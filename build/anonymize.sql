UPDATE `core_users` SET `name` = CONCAT(SUBSTRING(`name`, 1, 1), '*****************');

UPDATE `eshop_customers` SET
  `email` = CONCAT(
    SUBSTRING(`email`, 1, 1),
	  LEFT(MD5(`email`), 10),
	  SUBSTRING(`email`, INSTR(`email`, '@'))
  ),
  `first_name` = CONCAT(SUBSTRING(`first_name`, 1, 1), '*****************'),
  `last_name` = CONCAT(SUBSTRING(`last_name`, 1, 1), '*****************'),
  `phone` = CONCAT(SUBSTRING(`phone`, 1, 1), '************'),
  `address` = CONCAT(SUBSTRING(`address`, 1, 1), '*****************'),
  `city` = CONCAT(SUBSTRING(`city`, 1, 1), '*****************'),
  `post_code` = CONCAT(SUBSTRING(`post_code`, 1, 1), '0000'),
  `company` = CONCAT(SUBSTRING(`company`, 1, 1), '*****************'),
  `ic` = CONCAT(SUBSTRING(`ic`, 1, 1), '*******'),
  `dic` = CONCAT(SUBSTRING(`dic`, 1, 1), '***********');

TRUNCATE TABLE `eshop_customers_reset_passwords`;
TRUNCATE TABLE `eshop_customers_tokens`;

UPDATE `eshop_orders` SET
  `first_name` = CONCAT(SUBSTRING(`first_name`, 1, 1), '*****************'),
  `last_name` = CONCAT(SUBSTRING(`last_name`, 1, 1), '*****************'),
  `company` = CONCAT(SUBSTRING(`company`, 1, 1), '*****************'),
  `email` = CONCAT('*****************', SUBSTRING(`email`, INSTR(`email`, '@'))),
  `phone` = CONCAT(SUBSTRING(`phone`, 1, 1), '************'),
  `address` = CONCAT(SUBSTRING(`address`, 1, 1), '*****************'),
  `city` = CONCAT(SUBSTRING(`city`, 1, 1), '*****************'),
  `post_code` = CONCAT(SUBSTRING(`post_code`, 1, 1), '0000'),
  `company` = CONCAT(SUBSTRING(`company`, 1, 1), '*****************'),
  `del_address` = CONCAT(SUBSTRING(`del_address`, 1, 1), '*****************'),
  `del_city` = CONCAT(SUBSTRING(`del_city`, 1, 1), '*****************'),
  `del_post_code` = CONCAT(SUBSTRING(`del_post_code`, 1, 1), '0000'),
  `ic` = CONCAT(SUBSTRING(`ic`, 1, 1), '*******'),
  `dic` = CONCAT(SUBSTRING(`dic`, 1, 1), '***********'),
  `note` = CONCAT(SUBSTRING(`note`, 1, 1), '*****************');