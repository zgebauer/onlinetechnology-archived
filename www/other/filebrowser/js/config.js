'use strict';

//var baseUrl = window.location.href.replace('/other/filebrowser/browser.htm', '');

var GstFileBrowserConfig = {
    'default': {
        /** url to server connector */
        connector:'connector/index.php',
        /** base url to root folder with files, relative or absolute without trailing slash */
        baseUrl:window.location.href.replace('/other/filebrowser/browser.htm', '')+'/data/soubory',
        configServer:'default',
        lang:'cs',
        /**
         * function pass url of selected file back to editor
         * @param {String} url of selected file
         */
        callbackSubmit: function(url) {
            var dialogArguments = top.tinymce.activeEditor.windowManager.getParams();
            dialogArguments.window.document.getElementById(dialogArguments.input).value = url;
            top.tinymce.activeEditor.windowManager.close();
        },
        /**
         * function called when filebrowser closed by button storno
         */
        callbackStorno: function() {
            top.tinymce.activeEditor.windowManager.close();
        }
    }

};
