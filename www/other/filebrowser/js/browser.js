'use strict';

$(document).ready(function(){
    /* @var {String} section in config.js */
    var configSection = 'default',
    /* @var {String} section in config on server */
    configServer = 'default',
    /* @var {String} url of server connector */
    connector = GstFileBrowserConfig[configSection].connector,
    /* @var {String} language code */
    lang = 'en',
    /* @var {String} url of folder with files */
    baseUrl = window.location.protocol+'//'+window.location.host+'/',
    /* @var {Function} pass selected file to editor */
    callbackSubmit = null,
    /* @var {Function} invoked on filebrowser close */
    callbackStorno = null,
    /* @var {Function} invoked on filebrowser initialization */
    callbackInit = null,
    match, langUrl = '';
    if (GstFileBrowserConfig[configSection].baseUrl !== undefined) {
        baseUrl = GstFileBrowserConfig[configSection].baseUrl;
    }

    // parse parameters from url
    match = /[\?&]config=([^&]+)/i.exec(window.location.search);
    configSection = (match && match.length > 1 ? decodeURIComponent(match[1]) : 'default');
    match = /[\?&]lang=([^&]+)/i.exec(window.location.search);
    langUrl = (match && match.length > 1 ? decodeURIComponent(match[1]) : '');

    // get parameters from configuration
    if (GstFileBrowserConfig[configSection] === undefined) {
        alert('config '+configSection+' not found');
    } else {
        if (GstFileBrowserConfig[configSection].connector !== undefined) {
            connector = GstFileBrowserConfig[configSection].connector;
        }
        if (GstFileBrowserConfig[configSection].lang !== undefined) {
            lang = GstFileBrowserConfig[configSection].lang.substr(0, 2);
        }
        lang = ((langUrl !== undefined && langUrl !== '') ? langUrl.substr(0, 2) : lang);
        if (GstFileBrowserConfig[configSection].baseUrl !== undefined) {
            baseUrl = GstFileBrowserConfig[configSection].baseUrl;
        }
        if (GstFileBrowserConfig[configSection].configServer !== undefined) {
            configServer = GstFileBrowserConfig[configSection].configServer;
        }
        if ($.isFunction(GstFileBrowserConfig[configSection].callbackSubmit)) {
            callbackSubmit = GstFileBrowserConfig[configSection].callbackSubmit;
        }
        if ($.isFunction(GstFileBrowserConfig[configSection].callbackStorno)) {
            callbackStorno = GstFileBrowserConfig[configSection].callbackStorno;
        }
        if ($.isFunction(GstFileBrowserConfig[configSection].callbackInit)) {
            callbackInit = GstFileBrowserConfig[configSection].callbackInit;
        }
    }

    if (connector === undefined) {
        alert('undefined connector in section '+configSection);
    }

    // load language
    $.ajaxSetup({cache: true });
    $.getScript('js/lang/'+lang+'.js')
        .done(function() {
            $.ajaxSetup({cache: false });
            // init file browser
            var filebrowser = new GstFileBrowser(connector, configServer, baseUrl, callbackSubmit, callbackStorno, callbackInit);
            filebrowser.init();
        })
        .fail(function() {
            $.ajaxSetup({cache: false });
            alert('cannot load language '+lang);
        });

});

/**
 * @class GstFileBrowser javascript function related to browser window
 * @param {String} connector url of server side connector
 * @param {String} configServer section in configuration file
 * @param {String} baseUrl url to root folder with files
 * @param {Function} callbackSubmit pass selected file to editor
 * @param {Function} callbackStorno close filebrowser
 * @param {Function} callbackInit invoked on filebrowser initialization
 */
function GstFileBrowser(connector, configServer, baseUrl, callbackSubmit, callbackStorno, callbackInit) {
    /** @var {String} url of server connector */
    this.connector = connector;
    /** @var {String} url of server connector */
    this.configServer = configServer;
    /** @var {Function} pass selected file to editor */
    this.callbackSubmit = callbackSubmit;
    /** @var {Function} close filebrowser */
    this.callbackStorno = callbackStorno;
    /** @var {Function} invoked on filebrowser initialization */
    this.callbackInit = callbackInit;

    /** @var {String} selected path relative to root folder */
    this.baseUrl = baseUrl.toString();
    /** @var {String} selected path relative to root folder */
    this.selectedPath = '';
    /** @var {String} selected file or directory in current selected path */
    this.selectedFile = null;

    /** @var {String} selected view list/thumbs */
    this.view = 'thumbs';
    /** @var {String} string or regexp to filter list of files */
    this.filter = '';

    /** @var {Array} list of files and dirs in current selected path */
    this._files = new Array();
    /** @var {Array} list of all directories */
    this._tree = new Array();

    /** @var {String} custom event "directory tree loaded from server" */
    this.EVENT_TREE_LOAD = 'tree_load';
    /** @var {String} custom event "content of folder loaded from server" */
    this.EVENT_FOLDER_LOAD = 'folder_load';

    this._uploadSet = false;

    if (connector !== undefined) {
        this.connector = connector;
    }

    /**
     * initialization
     */
    this.init = function() {
        if (this.connector === null) {
            return;
        };
        if (this.callbackInit !== null) {
            callbackInit();
        }

        this._translatePage();

        $(document).ajaxStart(function() {
            $('#ajaxIndicator').removeClass('hidden');
        }).ajaxComplete(function(){
            $('#ajaxIndicator').addClass('hidden');
        });

        // bind actions to controls
        $('#tree').on('mousedown', 'li', {obj:this}, function(event){
            event.stopPropagation();
            var path = '', currentItem = $(this);
            while (currentItem.parent().closest('li[data-path]').length > 0) {
               currentItem = currentItem.parent().closest('li[data-path]');
               path = currentItem.data('path')+'/'+path;
            }
            path += $(this).data('path');
            event.data.obj.setPath(path);
        });

        $('#tree').on('mousedown', 'span.expand', {obj:this}, function(event){
            event.stopPropagation();
            $(this).closest('li').toggleClass('collapsed');
        });

        $('#tree').on('dragover', 'span.folder', {obj:this}, function(event){
            event.originalEvent.dataTransfer.dropEffect = (event.ctrlKey ? 'copy' : 'move'); // dont work in msie10
            return false;
        });
        $('#tree').on('drop', 'span.folder', {obj:this}, function(event){
            event.stopPropagation();
            event.preventDefault();
            var filename = event.originalEvent.dataTransfer.getData('text'), path = '', currentItem = $(this);
            if (filename === '') { // ignore drag from filesystem
                return false;
            }
            while (currentItem.parent().closest('li[data-path]').length > 0) {
               currentItem = currentItem.parent().closest('li[data-path]');
               path = currentItem.data('path')+'/'+path;
            }

           $('#panelCopy [name="new"]').val(path+filename);
           event.originalEvent.dataTransfer.dropEffect = (event.ctrlKey ? 'copy' : 'move'); // due msie10
           $('#panelCopy [name="action"]').val(event.originalEvent.dataTransfer.dropEffect).trigger('change');
           $('#butCopy').trigger('mousedown');
            return false;
        });

        $('#folder').on('mousedown', '[data-file]', {obj:this}, function(event){
            event.stopPropagation();
            event.data.obj.setFile($(this).data('file'));
        }).on('dblclick', '[data-file]', {obj:this}, function(event){
            $(this).trigger('mousedown');
            if (event.data.obj.selectedFile.type === 'dir') {
                event.data.obj.setPath(event.data.obj.selectedPath+'/'+event.data.obj.selectedFile.name);
                return;
            }
            $('#butOk').trigger('mousedown');
        }).on('dragstart', function(event) {
            event.originalEvent.dataTransfer.setData('text', event.target.getAttribute('data-file'));
            return true;
        });

        $('#butOk').mousedown({obj:this}, function(event) {
            event.stopPropagation();
            if ($(this).prop('disabled')) {
                return false;
            }
            var url = event.data.obj.baseUrl + event.data.obj.selectedPath+'/'+event.data.obj.selectedFile.name;
            if (event.data.obj.callbackSubmit !== null) {
                event.data.obj.callbackSubmit(url);
            }
        });

        $('#butStorno').mousedown({obj:this}, function(event) {
            if (event.data.obj.callbackStorno !== null) {
                event.data.obj.callbackStorno();
            }
        });

        $('#butMkdir').mousedown(function() {
            $('#panelMkdir').removeClass('hidden');
        });
        $('#butSubmitMkdir').mousedown({obj:this}, function(event) {
            event.data.obj.mkDir();
        });

        $('#butDelete').mousedown({obj:this}, function(event) {
            if (event.data.obj.selectedFile === '') {
                return;
            }
            $('#panelDelete').removeClass('hidden');
        });
        $('#butSubmitDelete').mousedown({obj:this}, function(event) {
            event.data.obj.del();
        });

        $('#butRename').mousedown({obj:this}, function(event) {
            if (event.data.obj.selectedFile === '') {
                return;
            }
            $('#panelRename').removeClass('hidden');
        });
        $('#butSubmitRename').mousedown({obj:this}, function(event) {
            event.data.obj.rename();
        });

        $('#butUpload').mousedown({obj:this}, function(event) {
            if (!this._uploadSet) {
                $('#dropzone').dropzone({
                    url: event.data.obj.connector,
                    paramName: "file", // The name that will be used to transfer the file
                    sending: function(file, xhr, formData) {
                        formData.append("filesize", file.size); // Will send the filesize along with the file as POST data.
                        formData.append('action', 'upload');
                        formData.append('config', event.data.obj.configServer);
                        formData.append('path', event.data.obj.selectedPath);
                    },
                    success: function(file, response) {
                        if (response.status === 'ERR') {
                            var token = 'ErrUpload' + response.err, message;
                            if (GstFileBrowserTranslations[token] === undefined) {
                                token = 'ErrUpload';
                            }
                            message = GstFileBrowserTranslations[token];
                            $('#panelUpload .msg').html(message);
                            return file.previewElement.classList.add("dz-error");
                        }
                        $('#panelUpload .msg').empty();
                        event.data.obj._files = response.files;
                        event.data.obj._refreshFiles();
                        return file.previewElement.classList.add("dz-success");
                    },
                    fallback: function() {
                        $('#dropzoneHolder').addClass('hidden');
                        var frameId = 'uploadframe'+Math.round(new Date().getTime() / 1000);
                        $('#frmUpload')
                            .attr('action', event.data.obj.connector)
                            .attr('target', frameId)
                            .append('<iframe width="0" height="0" class="hidden" name="'+frameId+'" id="'+frameId+'"></iframe>')
                            .removeClass('hidden');
                        $('#frmUpload input[name="config"]').val(event.data.obj.configServer);
                        $('#frmUpload input[name="path"]').val(event.data.obj.selectedPath);
                        $('#'+frameId).load(function() {
                            event.data.obj.handleUploadIframe($(this));
                        });
                      }
                });
                this._uploadSet = true;
            }

            $('#panelUpload').removeClass('hidden');
        });

        $('#panelUpload form').submit({obj:this}, function (event) {
            if ($(this).find('input:file').val() === '') {
                return false;
            }
            $('#panelUpload .msg').html(event.data.obj._translate('Processing...'));
            return true;
        });

        $('#butCopy').mousedown(function() {
            $('#panelCopy select').trigger('change');
            $('#panelCopy').removeClass('hidden');
        });
        $('#panelCopy select[name="action"]').change({obj:this}, function(event) {
            $('#butSubmitCopy').html(event.data.obj._translate($(this).val() === 'move' ? 'Move' : 'Copy'));
        });
        $('#butSubmitCopy').mousedown({obj:this}, function(event) {
            event.data.obj.copy();
        });

        $('.panel .close').mousedown(function(){
            $(this).closest('.panel').find('.msg').empty();
            $(this).closest('.panel').find('.dz-preview').remove();
            $(this).closest('.panel input:text').val('');
            $(this).closest('.panel').addClass('hidden');
        });

        $('#fldView').change({obj:this}, function(event) {
            event.data.obj.setView($(this).val());
        });

        $('#fldFilter').keyup({obj:this}, function(event) {
            $(this).val($(this).val().toLowerCase());
            if ($(this).val() === '') {
                $('#fldFilterResetButton').addClass('hidden');
            } else {
                $('#fldFilterResetButton').removeClass('hidden');
            }
            event.data.obj.setFilter($(this).val());
        });
        $('#fldFilterSelect').change(function() {
            $('#fldFilter').val($(this).val()).trigger('keyup');
            $(this).addClass('hidden');
        });
        $('#fldFilterButton').mousedown(function() {
            $('#fldFilterSelect').toggleClass('hidden');
        });
        $('#fldFilterResetButton').mousedown(function() {
            $('#fldFilter').val('').trigger('keyup');
        });

        // params je filebrowser object
        $(document).bind(this.EVENT_TREE_LOAD, function(event, params){
            params._refreshTree();
            params._loadFiles();
        });

        $(document).bind(this.EVENT_FOLDER_LOAD, function(event, params){
            params._refreshFiles();
        });

        $('#fldView').val(this.view);
        this._loadTree();
    };

    // reload tree of folders from server
    this._loadTree = function() {
        $.ajax({
            type: 'GET',
            url: this.connector,
            data: { config: this.configServer, action: 'tree' },
            dataType: 'json',
            timeout: 6000,
            context: this,
            success: function(response) {
                if (response.status === 'OK') {
                    $(this)[0]._tree = response.tree;
                    $(this)[0].setFile('');
                    $(document).trigger(this.EVENT_TREE_LOAD, $(this));
                }
                if (response.status === 'ERR') {
                    $('#tree').html($(this)[0]._translate('ErrLoadTree' + response.err));
                }
            },
            error: function() {
                $('#tree').html($(this)[0]._translate('ErrLoadTree'));
            }
        });
    };

    // redraw box with tree of folders
    this._refreshTree =  function() {
        var hasChildren = (this._tree[0].children !== undefined), i, html;
        html = '<ul><li data-path="" class="collapsed'+(!hasChildren ? 'noSubfolder' : '')+'">'+(hasChildren ? '<span class="expand">+</span>' : '')
                +'<span class="folder">'+this._tree[0].name+'</span>';
        if (hasChildren) {
            for (i=0; i<this._tree[0].children.length; i++) {
                html += this._formatTreeItem(this._tree[0].children[i]);
            }
        }
        html += '</li></ul>';
        $('#tree').html(html);
    };
    // returns item in tree of folders
    this._formatTreeItem = function(treeItem) {
        var hasChildren = (treeItem.children !== undefined), y, ret;
        ret = '<ul><li data-path="'+treeItem.name+'" class="collapsed'+(!hasChildren ? ' noSubfolder' : '')+'">';
        if (hasChildren) {
            ret += '<span class="expand">+</span>';
        }
        ret += '<span class="folder">'+treeItem.name+'</span>';
        if (hasChildren) {
            ret += '<ul>';
            for (y=0;y<treeItem.children.length; y++) {
                ret += this._formatTreeItem(treeItem.children[y]);
            }
            ret += '</ul>';
        }
        ret += '</li></ul>';
        return ret;
    };

    this._loadFiles = function() {
        $.ajax({
            type: 'GET',
            url: this.connector,
            data: { config: this.configServer, action: 'files', path: this.selectedPath },
            dataType: 'json',
            timeout: 6000,
            context: this,
            success: function(data) {
                if (data.status === 'OK') {
                    $(this)[0]._files = data.files;
                    $(document).trigger(this.EVENT_FOLDER_LOAD, $(this));
                }
                if (data.status === 'ERR') {
                    $('#folder').html($(this)[0]._translate('ErrLoadFiles' + data.err));
                }
            },
            error: function() {
                $('#folder').html($(this)[0]._translate('ErrLoadFiles'));
            }
        });
    };

    this._refreshFiles = function() {
        var displayed = [], html = '', i, y, cssType, date, title = '', filterStrings, menuItems, menuOpts, draggable;

        this.selectedFile = '';

        // apply filter
        if (this.filter === '') {
            displayed = this._files;
        } else {
            filterStrings = this.filter.split('|');
            for (i=0;i<this._files.length;i++) {
                for (y=0;y<filterStrings.length;y++) {
                    if (this._files[i].name.toLowerCase().indexOf(filterStrings[y]) !== -1) {
                        displayed.push(this._files[i]);
                        break;
                    }
                }
            }
        }

        // sort by dir and name
        displayed.sort(function(obj1, obj2) {
            if (obj1.type === 'dir' && obj2.type !== 'dir') {
                return  -1;
            }
            if (obj1.type !== 'dir' && obj2.type === 'dir') {
                return 1;
            }
            return obj1.name < obj2.name ? -1 : 1;
        });

        if (this.view === 'thumbs') {
            for (i=0;i<displayed.length;i++) {

                cssType = (displayed[i].type === 'dir' ? 'dir' : this._getFileExtension(displayed[i].name));
                draggable = (displayed[i].type === 'file' ? 'true' : 'false');

                html += '<div data-file="'+displayed[i].name+'" data-type="'+displayed[i].type+'" class="thumb thumb-'+cssType+'" draggable="'+draggable+'">';
                if (displayed[i].imgsize !== null) {
                    title = ' title="'+displayed[i].imgsize[0]+' x '+displayed[i].imgsize[1]+'"';
                }
                if (displayed[i].thumbnail !== null && displayed[i].thumbnail !== false && displayed[i].thumbnail.indexOf('data:image') !== -1) {
                    html +=  '<span class="image"><img src="'+displayed[i].thumbnail+'" alt="'+displayed[i].name+'" '+title+' draggable="false" /></span>';
                }
                html += '<span class="name">'+displayed[i].name+'</span>';
                html += '</div>';
            }
        } else {
            html = '<table><thead><tr><th colspan="2">'+this._translate('Filename')+'</th><th>'+this._translate('Size')+'</th><th>'
                    +this._translate('Dimensions')+'</th><th>'+this._translate('Date')+'</th></tr></thead><tbody>';
            for (i=0;i<displayed.length;i++) {
                cssType = (displayed[i].type === 'dir' ? 'dir' : this._getFileExtension(displayed[i].name));
                draggable = (displayed[i].type === 'file' ? 'true' : 'false');
                date = new Date(displayed[i].date);
                html += '<tr data-file="'+displayed[i].name+'" data-type="'+displayed[i].type+'" draggable="'+draggable+'">'
                    +'<td class="icon icon-'+cssType+'"></td><td class="name">'+displayed[i].name +'</td><td>';
                if (displayed[i].size !== null) {
                    html += this.formatFilesize(displayed[i].size);
                }
                html += '</td><td>';
                if (displayed[i].imgsize !== null) {
                    html += displayed[i].imgsize[0]+' x '+displayed[i].imgsize[0];
                }
                html += '</td><td>'+date.toLocaleString()+'</td></tr>';
            }
            html += '</tbody></table>';
        }

        $('#folder').html(html);

        menuItems = [
            {'Insert': {
                onclick: function(menuItem, menu) { $('#butOk').trigger('mousedown');; },
                className: 'relFile',
                itemLabel: this._translate('Insert')
                }
            },
            {'Open': {
                onclick: function(menuItem, menu) { $(menu.target).trigger('dblclick'); },
                className: 'relDir',
                itemLabel: this._translate('Open')
                }
            },
            {'Rename': {
                onclick: function(menuItem, menu) { $('#butRename').trigger('mousedown'); },
                itemLabel: this._translate('Rename')
                }
            },
            $.contextMenu.separator,
            {'Delete': {
                onclick: function(menuItem, menu) { $('#butDelete').trigger('mousedown'); },
                itemLabel: this._translate('Delete')
                }
            }
        ];
        menuOpts = {
            beforeShow: function() {
                // hide items related to dir/file
                var itemType = $(this.target).data('type');
                $(this.menu).find('.relFile, .relDir').removeClass('hidden');
                if (itemType === 'dir') {
                    $(this.menu).find('.relFile').addClass('hidden');
                }
                if (itemType === 'file') {
                    $(this.menu).find('.relDir').addClass('hidden');
                }
            }
        };
        $('#folder [data-file]').contextMenu(menuItems, menuOpts);
    };

    this._getFileExtension = function(filename) {
        return filename.split('.').pop().toLowerCase();
    };

    this.setPath = function(path) {
        this.selectedPath = path;
        $('[data-showpath]').html(path);
        this.setFile('');
        this._loadFiles();
    };

    this.setFile = function(filename) {
        var cssType, html, i, date;
        this.selectedFile = null;
        for (i=0;i<this._files.length;i++) {
            if (this._files[i].name === filename) {
                this.selectedFile = this._files[i];
                break;
            }
        }

        $('#butDelete, #butRename').prop('disabled', this.selectedFile === null);

        $('#butOk, #butCopy').prop('disabled', this.selectedFile === null || this.selectedFile.type !== 'file');

        if (this.selectedFile === null) {
            $('[data-showfile]').html('');
            $('#folder [data-file]').removeClass('selected');
            $('#selectedFile').empty();
        } else {
            $('[data-showfile]').html(this.selectedFile.name);
            $('#folder [data-file="'+this.selectedFile.name+'"]').addClass('selected');
            $('#folder [data-file!="'+this.selectedFile.name+'"]').removeClass('selected');
            cssType = (this.selectedFile.type === 'dir' ? 'dir' : this._getFileExtension(this.selectedFile.name));
            html = '<div class="thumb thumb-'+cssType+'">';
            if (this.selectedFile.thumbnail !== null && this.selectedFile.thumbnail.indexOf('data:image') !== -1) {
                html += '<img src="'+this.selectedFile.thumbnail+'" alt="'+this.selectedFile.name+'" />';
            }
            html += '</div>';
            html += '<p>'+this.selectedFile.name+'<br />';
            if (this.selectedFile.size !== null) {
                html += this._translate('Size')+':'+this.formatFilesize(this.selectedFile.size)+'<br />';
            }
            if (this.selectedFile.imgsize !== null) {
                html += this._translate('Dimensions')+':'+this.selectedFile.imgsize[0]+' x '+this.selectedFile.imgsize[1]+'<br />';
            }
            date = new Date(this.selectedFile.date);
            html += this._translate('Date')+':'+date.toLocaleString()+'</p>';
            $('#selectedFile').html(html);
        }
    };

    this.setView = function(view) {
        if (view !== 'list' && view !== 'thumbs') {
            return;
        }
        if (view !== this.view) {
            this.view = view;
            this._refreshFiles();
        }
    };

    this.setFilter = function(filter) {
        this.filter = filter;
        this._refreshFiles();
    };

    this.mkDir = function() {
        $('#panelMkdir .msg').html(this._translate('Processing...'));
        $.ajax({
            type: 'POST',
            url: this.connector,
            data: { config: this.configServer, action: 'mkdir', path: this.selectedPath, dir: $('input[name="newdir"]').val() },
            dataType: 'json',
            timeout: 6000,
            context: this,
            success: function(data) {
                if (data.status === 'OK') {
                    $(this)[0]._tree = data.tree;
                    $(this)[0]._refreshTree();
                    $(this)[0]._files = data.files;
                    $(this)[0]._refreshFiles();
                    $('#panelMkdir .close').trigger('mousedown');
                }
                if (data.status === 'ERR') {
                    $('#panelMkdir .msg').html($(this)[0]._translate('ErrMkDir' + data.err));
                }
            },
            error: function() {
                $('#panelMkdir .msg').html($(this)[0]._translate('ErrMkDir'));
            }
        });
    };

    this.del = function() {
        $('#panelDelete .msg').html(this._translate('Processing...'));
        $.ajax({
            type: 'POST',
            url: this.connector,
            data: { config: this.configServer, action: 'delete', path: this.selectedPath, name : this.selectedFile.name },
            dataType: 'json',
            timeout: 6000,
            context: this,
            success: function(data) {
                if (data.status === 'OK') {
                    if (data.tree !== undefined) {
                        $(this)[0]._tree = data.tree;
                        $(this)[0]._refreshTree();
                    }
                    $(this)[0]._files = data.files;
                    $(this)[0]._refreshFiles();
                    $(this)[0].setFile('');
                    $('#panelDelete .close').trigger('mousedown');
                }
                if (data.status === 'ERR') {
                    $('#panelDelete .msg').html($(this)[0]._translate('ErrDelete' + data.err));
                }
            },
            error: function() {
                $('#panelDelete .msg').html($(this)[0]._translate('ErrDelete'));
            }

        });
    };

    this.rename  = function() {
        $('#panelRename .msg').html(this._translate('Processing...'));
        $.ajax({
            type: 'POST',
            url: this.connector,
            data: { config: this.configServer, action: 'rename', path: this.selectedPath, old: this.selectedFile.name, 'new' : $('#panelRename input[name="new"]').val() },
            dataType: 'json',
            timeout: 6000,
            context: this,
            success: function(data) {
                if (data.status === 'OK') {
                    if (data.tree !== undefined) {
                        $(this)[0]._tree = data.tree;
                        $(this)[0]._refreshTree();
                    }
                    $(this)[0]._files = data.files;
                    $(this)[0]._refreshFiles();
                    $('#panelRename .close').trigger('mousedown');
                }
                if (data.status === 'ERR') {
                    $('#panelRename .msg').html($(this)[0]._translate('ErrRename' + data.err));
                }
            },
            error: function() {
                $('#panelRename .msg').html($(this)[0]._translate('ErrRename'));
            }
        });
    };

    /**
     * process response from upload placed in iframe
     * @param {jQuery} frame iframe with reluts
     */
    this.handleUploadIframe = function(frame) {
        var responseStr = $.trim(frame[0].contentWindow.document.body.innerHTML), response;
        if (responseStr === '') { // FF fire load event when append iframe to page
            return;
        }
        responseStr = responseStr.replace('<pre>', '');
        responseStr = responseStr.replace('</pre>', '');

        try {
            response = $.parseJSON($.trim(responseStr));
            if (response.status === 'OK') {
                $('#panelUpload .msg').empty();
                this._files = response.files;
                this._refreshFiles();
            }
            if (response.status === 'ERR') {
                $('#panelUpload .msg').html(this._translate('ErrUpload' + response.err));
                return;
            }
        } catch(e) {
        }
        $('#panelUpload form')[0].reset();
        $('#panelUpload iframe').remove();
        $('#panelUpload').addClass('hidden');
    };

    this.copy = function() {
        $('#panelCopy .msg').html(this._translate('Processing...'));
        $.ajax({
            type: 'POST',
            url: this.connector,
            data: { config: this.configServer, action: $('#panelCopy [name="action"]').val(), path: this.selectedPath, old: this.selectedFile.name,  'new': $('#panelCopy input[name="new"]').val() },
            dataType: 'json',
            timeout: 6000,
            context: this,
            success: function(data) {
                if (data.status === 'OK') {
                   if (data.tree !== undefined) {
                        $(this)[0]._tree = data.tree;
                        $(this)[0]._refreshTree();
                    }
                    if (data.files !== undefined) {
                        $(this)[0]._files = data.files;
                        $(this)[0]._refreshFiles();
                    }
                    $('#panelCopy .close').trigger('mousedown');
                }
                if (data.status === 'ERR') {
                    $('#panelCopy .msg').html($(this)[0]._translate('ErrCopy' + data.err));
                }
            },
            error: function() {
                $('#panelCopy .msg').html($(this)[0]._translate('ErrCopy'));
            }
        });
    };

    this.formatFilesize = function(bytes) {
        var sizes = ['B', 'kB', 'MB', 'GB', 'TB'], i;
        if (bytes <= 0) {
            return '0 B';
        }
        i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[[i]];
    };

    this._translate = function(token) {
        return (GstFileBrowserTranslations[token] === undefined ? token : GstFileBrowserTranslations[token]);
    };

    this._translatePage = function() {
        var obj = this;
        $('.i18n').each(function(){
            $(this).text(obj._translate($.trim($(this).text())));
        });
    };

}
