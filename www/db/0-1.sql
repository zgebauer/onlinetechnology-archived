CREATE TABLE IF NOT EXISTS `core_permissions` (
  `user_id` int(10) unsigned NOT NULL,
  `module` varchar(20) NOT NULL,
  `permission` text NOT NULL,
  KEY `module` (`module`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `core_sitemap` (
  `module` varchar(20) NOT NULL,
  `last_refresh` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `url` varchar(255) NOT NULL,
  `last_mod` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `change_freq` varchar(7) NOT NULL,
  `priority` decimal(2,1) unsigned NOT NULL DEFAULT '0.0',
  KEY `module` (`module`),
  KEY `last_refresh` (`last_refresh`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `core_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(10) NOT NULL,
  `pwd` char(60) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `last_log` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `block` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/* default user with password 'ergo' */
INSERT IGNORE INTO `core_users` (`id`, `login`, `pwd`, `name`) VALUES
(1, 'admin', '$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm', 'Administrator');
INSERT IGNORE INTO `core_permissions` (`user_id`, `module`, `permission`) VALUES
(1, 'admins', '');
