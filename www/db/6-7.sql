ALTER TABLE `eshop_products`
	ADD COLUMN `stock_info` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci' AFTER `programy_en`,
	ADD COLUMN `stock_info_en` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci' AFTER `stock_info`;
