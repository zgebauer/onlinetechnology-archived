CREATE TABLE `eshop_orders_payments` (
	`id` TINYINT(4) UNSIGNED NOT NULL,
	`price` DECIMAL(6,2) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `eshop_orders` ADD COLUMN `price_payment` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `price_delivery`;
