ALTER TABLE `articles`
	ADD COLUMN `lang` CHAR(2) NOT NULL DEFAULT 'cs' AFTER `seo`,
	ADD INDEX `lang` (`lang`);

ALTER TABLE `menu_items`
	ADD COLUMN `lang` CHAR(2) NOT NULL DEFAULT 'cs' AFTER `target_type`,
	ADD INDEX `lang` (`lang`);

ALTER TABLE `eshop_products`
	ADD COLUMN `price_eur` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00' AFTER `price_vat`,
	ADD COLUMN `price_eur_vat` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00' AFTER `price_eur`;


CREATE TABLE IF NOT EXISTS `eshop_exchange_rates` (
	`currency` CHAR(3) NOT NULL,
	`rate` DECIMAL(6,3) UNSIGNED NOT NULL,
	PRIMARY KEY (`currency`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `eshop_categories`
	ADD COLUMN `name_en` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `name`,
	ADD COLUMN `text_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `text`;

ALTER TABLE `eshop_products`
	ADD COLUMN `name_en` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `name`,
	ADD COLUMN `text_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `text`,
	ADD COLUMN `popis_modulu_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `popis_modulu`,
	ADD COLUMN `zapojeni_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `zapojeni`,
	ADD COLUMN `dokumentace_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `dokumentace`,
	ADD COLUMN `firmware_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `firmware`,
	ADD COLUMN `programy_en` TEXT NOT NULL COLLATE 'utf8_general_ci' AFTER `programy`;


ALTER TABLE `eshop_orders_items`
	ADD COLUMN `price_unit_eur` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00' AFTER `price_row_vat`,
	ADD COLUMN `price_unit_eur_vat` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00' AFTER `price_unit_eur`,
	ADD COLUMN `price_row_eur` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0.00' AFTER `price_unit_eur_vat`,
	ADD COLUMN `price_row_eur_vat` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0.00' AFTER `price_row_eur`;


ALTER TABLE `eshop_orders`
	ADD COLUMN `subtotal_eur` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `subtotal_vat`,
	ADD COLUMN `subtotal_eur_vat` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `subtotal_eur`,
	ADD COLUMN `price_delivery_eur` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `price_delivery`,
	ADD COLUMN `price_payment_eur` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `price_payment`,
	ADD COLUMN `grandtotal_eur` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `grandtotal_vat`,
	ADD COLUMN `grandtotal_eur_vat` DECIMAL(10,2) UNSIGNED NOT NULL AFTER `grandtotal_eur`,
    ADD COLUMN `lang` CHAR(2) NOT NULL DEFAULT 'cs' AFTER `date_sent`;


CREATE TABLE IF NOT EXISTS `eshop_payment_fees` (
	`payment_type` INT(10) UNSIGNED NOT NULL,
	`country_code` CHAR(2) NOT NULL,
	`allow` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`fee` FLOAT(5,2) NOT NULL DEFAULT '0.00',
	`fee_eur` FLOAT(5,2) NOT NULL DEFAULT '0.00',
	`info` TEXT NOT NULL,
	PRIMARY KEY (`payment_type`, `country_code`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `eshop_delivery_fees` (
	`delivery_type` INT(10) UNSIGNED NOT NULL,
	`country_code` CHAR(2) NOT NULL,
	`allow` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`fee` FLOAT(5,2) NOT NULL DEFAULT '0.00',
	`fee_eur` FLOAT(5,2) NOT NULL DEFAULT '0.00',
	`free_limit` FLOAT(7,2) NOT NULL DEFAULT '0.00',
	`free_limit_eur` FLOAT(7,2) NOT NULL DEFAULT '0.00',
	`info` TEXT NOT NULL,
	PRIMARY KEY (`delivery_type`, `country_code`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `eshop_products_icons`
	ADD COLUMN `name_en` VARCHAR(100) NOT NULL AFTER `text`,
	ADD COLUMN `text_en` TEXT NOT NULL AFTER `name_en`;

ALTER TABLE `staticarticles`
	ADD COLUMN `name_en` VARCHAR(250) NOT NULL DEFAULT '' AFTER `meta_keys`,
	ADD COLUMN `text_en` TEXT NOT NULL DEFAULT '' AFTER `name_en`;
