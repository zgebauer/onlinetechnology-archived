CREATE TABLE IF NOT EXISTS `eshop_config` (
	`param` VARCHAR(25) NOT NULL,
	`value` TEXT NULL,
	PRIMARY KEY (`param`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_customers` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50) NOT NULL,
	`pwd` CHAR(64) NOT NULL DEFAULT '',
	`first_name` VARCHAR(20) NOT NULL DEFAULT '',
	`last_name` VARCHAR(40) NOT NULL DEFAULT '',
	`phone` VARCHAR(13) NOT NULL DEFAULT '',
	`address` VARCHAR(50) NOT NULL DEFAULT '',
	`city` VARCHAR(50) NOT NULL DEFAULT '',
	`post_code` VARCHAR(10) NOT NULL DEFAULT '',
	`country` CHAR(2) NOT NULL DEFAULT '',
	`company` VARCHAR(50) NOT NULL DEFAULT '',
	`ic` VARCHAR(8) NOT NULL DEFAULT '',
	`dic` VARCHAR(12) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `email` (`email`),
	INDEX `last_name` (`last_name`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_customers_reset_passwords` (
	`token` CHAR(32) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`valid_to` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`token`),
	UNIQUE INDEX `token` (`token`),
	INDEX `valid_to` (`valid_to`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_customers_tokens` (
	`token` CHAR(32) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`valid_to` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`token`),
	UNIQUE INDEX `token` (`token`),
	INDEX `valid_to` (`valid_to`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_products` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT '',
	`seo` VARCHAR(52) NOT NULL DEFAULT '',
	`last_change` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`text` TEXT NOT NULL,
	`meta_desc` VARCHAR(255) NOT NULL DEFAULT '',
	`meta_keys` VARCHAR(255) NOT NULL DEFAULT '',
	`price` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`price_vat` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`vat` DECIMAL(3,1) UNSIGNED NOT NULL DEFAULT '0.0',
	`bestseller` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`featured` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `seo` (`seo`),
	INDEX `bestseller` (`bestseller`),
	INDEX `name` (`name`),
	INDEX `featured` (`featured`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_products_images` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`last_change` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`parent_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`ordering` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`description` VARCHAR(150) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`),
	INDEX `ordering` (`ordering`),
	INDEX `publish` (`publish`),
	INDEX `parent_id` (`parent_id`),
	CONSTRAINT `FK_eshop_products_images_eshop_products` FOREIGN KEY (`parent_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `eshop_products_icons` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) NOT NULL COLLATE 'utf8_czech_ci',
	`text` TEXT NOT NULL COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`),
	INDEX `name` (`name`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_categories` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`parent_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`ordering` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`seo` VARCHAR(52) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`publish` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`meta_desc` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`meta_keys` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`text` TEXT NOT NULL COLLATE 'utf8_czech_ci',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `seo` (`seo`),
	INDEX `parent_id` (`parent_id`),
	INDEX `ordering` (`ordering`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_rel_product_category` (
	`product_id` INT(10) UNSIGNED NOT NULL,
	`category_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`product_id`, `category_id`),
	INDEX `FK_categories` (`category_id`),
	CONSTRAINT `FK_categories` FOREIGN KEY (`category_id`) REFERENCES `eshop_categories` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_rel_product_cross` (
	`product_id` INT(10) UNSIGNED NOT NULL,
	`cross_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`product_id`, `cross_id`),
	INDEX `FK_eshop_rel_product_cross_eshop_products_2` (`cross_id`),
	CONSTRAINT `FK_eshop_rel_product_cross_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_eshop_rel_product_cross_eshop_products_2` FOREIGN KEY (`cross_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_rel_product_variant` (
	`product_id` INT(10) UNSIGNED NOT NULL,
	`variant_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`product_id`, `variant_id`),
	INDEX `FK_eshop_rel_product_variant_eshop_products_2` (`variant_id`),
	CONSTRAINT `FK_eshop_rel_product_variant_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_eshop_rel_product_variant_eshop_products_2` FOREIGN KEY (`variant_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_rel_product_icon` (
	`product_id` INT(10) UNSIGNED NOT NULL,
	`icon_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`product_id`, `icon_id`),
	INDEX `FK_eshop_rel_product_icon_eshop_products_icons` (`icon_id`),
	CONSTRAINT `FK_eshop_rel_product_icon_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_eshop_rel_product_icon_eshop_products_icons` FOREIGN KEY (`icon_id`) REFERENCES `eshop_products_icons` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_orders` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`customer_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`first_name` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`last_name` VARCHAR(40) NOT NULL COLLATE 'utf8_general_ci',
	`company` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`email` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`phone` VARCHAR(13) NOT NULL COLLATE 'utf8_general_ci',
	`address` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`city` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`post_code` VARCHAR(10) NOT NULL COLLATE 'utf8_general_ci',
	`country` CHAR(2) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`del_address` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`del_city` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`del_post_code` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`del_country` CHAR(2) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`ic` VARCHAR(8) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`dic` VARCHAR(12) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`note` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`deliverer_id` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`subtotal` DECIMAL(10,2) UNSIGNED NOT NULL,
	`subtotal_vat` DECIMAL(10,2) UNSIGNED NOT NULL,
	`price_delivery` DECIMAL(10,2) UNSIGNED NOT NULL,
	`grandtotal` DECIMAL(10,2) UNSIGNED NOT NULL,
	`grandtotal_vat` DECIMAL(10,2) UNSIGNED NOT NULL,
	`status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`parcel_number` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`),
	INDEX `status` (`status`),
	INDEX `customer_id` (`customer_id`)
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `eshop_orders_items` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`order_id` INT(10) UNSIGNED NOT NULL,
	`product_id` INT(10) UNSIGNED NOT NULL,
	`product` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`quantity` SMALLINT(5) UNSIGNED NOT NULL,
	`price_unit` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`price_unit_vat` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`vat` DECIMAL(3,1) UNSIGNED NOT NULL DEFAULT '0.0',
	`price_row` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`price_row_vat` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
	PRIMARY KEY (`id`),
	INDEX `order_id` (`order_id`),
	CONSTRAINT `FK_eshop_orders_items_eshop_orders` FOREIGN KEY (`order_id`) REFERENCES `eshop_orders` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_czech_ci'
ENGINE=InnoDB;
