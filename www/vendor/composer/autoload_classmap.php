<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Admins\\Admin\\Controller' => $baseDir . '/modules/admins/admin/controller.php',
    'Admins\\Admin\\ControllerAuth' => $baseDir . '/modules/admins/admin/controllerauth.php',
    'Admins\\Data' => $baseDir . '/modules/admins/data.php',
    'Admins\\Module' => $baseDir . '/modules/admins/module.php',
    'Admins\\User' => $baseDir . '/modules/admins/user.php',
    'Articles\\Admin\\Controller' => $baseDir . '/modules/articles/admin/controller.php',
    'Articles\\Article' => $baseDir . '/modules/articles/article.php',
    'Articles\\Config' => $baseDir . '/modules/articles/config.php',
    'Articles\\Data' => $baseDir . '/modules/articles/data.php',
    'Articles\\DataSitemap' => $baseDir . '/modules/articles/data_sitemap.php',
    'Articles\\Gui' => $baseDir . '/modules/articles/gui.php',
    'Articles\\Image' => $baseDir . '/modules/articles/image.php',
    'Articles\\Module' => $baseDir . '/modules/articles/module.php',
    'Core\\DataSitemap' => $baseDir . '/modules/core/data_sitemap.php',
    'Core\\Gui' => $baseDir . '/modules/core/gui.php',
    'Core\\GuiAdmin' => $baseDir . '/modules/core/admin/gui.php',
    'Core\\Module' => $baseDir . '/modules/core/module.php',
    'Dice\\Dice' => $vendorDir . '/level-2/dice/Dice.php',
    'Dice\\Loader\\Json' => $vendorDir . '/level-2/dice/Loader/Json.php',
    'Dice\\Loader\\Xml' => $vendorDir . '/level-2/dice/Loader/Xml.php',
    'Ergo\\Application' => $baseDir . '/core/app.php',
    'Ergo\\ApplicationInterface' => $baseDir . '/core/app.php',
    'Ergo\\Component' => $baseDir . '/core/Component.php',
    'Ergo\\Config' => $baseDir . '/core/config.php',
    'Ergo\\ConfigInterface' => $baseDir . '/core/config_interface.php',
    'Ergo\\Csrf' => $baseDir . '/core/csrf.php',
    'Ergo\\DbMapperAbstract' => $baseDir . '/core/DbMapperAbstract.php',
    'Ergo\\GuiAbstract' => $baseDir . '/core/gui_abstract.php',
    'Ergo\\ModuleAbstract' => $baseDir . '/core/module_abstract.php',
    'Ergo\\ModuleManager' => $baseDir . '/core/module_manager.php',
    'Ergo\\Pagination' => $baseDir . '/core/pagination.php',
    'Ergo\\Request' => $baseDir . '/core/request.php',
    'Ergo\\Response' => $baseDir . '/core/response.php',
    'Ergo\\Router' => $baseDir . '/core/router.php',
    'Ergo\\Tracy\\ApplicationPanel' => $baseDir . '/modules/core/Tracy/ApplicationPanel.php',
    'Ergo\\Tracy\\DatabasePanel' => $baseDir . '/modules/core/Tracy/DatabasePanel.php',
    'Eshop\\Admin\\ControllerCategories' => $baseDir . '/modules/eshop/admin/controllercategories.php',
    'Eshop\\Admin\\ControllerConfig' => $baseDir . '/modules/eshop/admin/controllerconfig.php',
    'Eshop\\Admin\\ControllerCustomers' => $baseDir . '/modules/eshop/admin/controllercustomers.php',
    'Eshop\\Admin\\ControllerIcons' => $baseDir . '/modules/eshop/admin/controllericons.php',
    'Eshop\\Admin\\ControllerIconsImages' => $baseDir . '/modules/eshop/admin/controllericonsimages.php',
    'Eshop\\Admin\\ControllerOrders' => $baseDir . '/modules/eshop/admin/controllerorders.php',
    'Eshop\\Admin\\ControllerProducts' => $baseDir . '/modules/eshop/admin/controllerproducts.php',
    'Eshop\\Admin\\ControllerProductsCross' => $baseDir . '/modules/eshop/admin/controllerproductscross.php',
    'Eshop\\Admin\\ControllerProductsGallery' => $baseDir . '/modules/eshop/admin/controllerproductsgallery.php',
    'Eshop\\Admin\\ControllerProductsImages' => $baseDir . '/modules/eshop/admin/controllerproductsimages.php',
    'Eshop\\Admin\\ControllerProductsVariants' => $baseDir . '/modules/eshop/admin/controllerproductsvariants.php',
    'Eshop\\Cnb\\Exception' => $baseDir . '/modules/eshop/Cnb/Exception.php',
    'Eshop\\Cnb\\Mapper' => $baseDir . '/modules/eshop/Cnb/Mapper.php',
    'Eshop\\Cnb\\Repository' => $baseDir . '/modules/eshop/Cnb/Repository.php',
    'Eshop\\Config' => $baseDir . '/modules/eshop/config.php',
    'Eshop\\ControllerCarts' => $baseDir . '/modules/eshop/controllercarts.php',
    'Eshop\\ControllerCustomer' => $baseDir . '/modules/eshop/controllercustomer.php',
    'Eshop\\ControllerOrders' => $baseDir . '/modules/eshop/controllerorders.php',
    'Eshop\\ControllerProducts' => $baseDir . '/modules/eshop/controllerproducts.php',
    'Eshop\\Customers\\Customer' => $baseDir . '/modules/eshop/customers/customer.php',
    'Eshop\\Customers\\DataCustomer' => $baseDir . '/modules/eshop/customers/datacustomer.php',
    'Eshop\\Customers\\DataResetPassword' => $baseDir . '/modules/eshop/customers/dataresetpassword.php',
    'Eshop\\Customers\\Discount' => $baseDir . '/modules/eshop/customers/discount.php',
    'Eshop\\Customers\\ResetPassword' => $baseDir . '/modules/eshop/customers/resetpassword.php',
    'Eshop\\DataConfig' => $baseDir . '/modules/eshop/dataconfig.php',
    'Eshop\\DataSearch' => $baseDir . '/modules/eshop/datasearch.php',
    'Eshop\\DataSitemap' => $baseDir . '/modules/eshop/datasitemap.php',
    'Eshop\\Module' => $baseDir . '/modules/eshop/module.php',
    'Eshop\\Orders\\Cart' => $baseDir . '/modules/eshop/orders/cart.php',
    'Eshop\\Orders\\CartItem' => $baseDir . '/modules/eshop/orders/cart.php',
    'Eshop\\Orders\\DataCart' => $baseDir . '/modules/eshop/orders/datacart.php',
    'Eshop\\Orders\\DataDeliverer' => $baseDir . '/modules/eshop/orders/datadeliverer.php',
    'Eshop\\Orders\\DataOrder' => $baseDir . '/modules/eshop/orders/dataorder.php',
    'Eshop\\Orders\\DataPayment' => $baseDir . '/modules/eshop/orders/datapayment.php',
    'Eshop\\Orders\\Deliverer' => $baseDir . '/modules/eshop/orders/delivererslist.php',
    'Eshop\\Orders\\DeliverersList' => $baseDir . '/modules/eshop/orders/delivererslist.php',
    'Eshop\\Orders\\DiscountSolver' => $baseDir . '/modules/eshop/orders/discountsolver.php',
    'Eshop\\Orders\\Fees\\Country' => $baseDir . '/modules/eshop/orders/Fees/Country.php',
    'Eshop\\Orders\\Fees\\CountryList' => $baseDir . '/modules/eshop/orders/Fees/CountryList.php',
    'Eshop\\Orders\\Fees\\DeliveryCountryFee' => $baseDir . '/modules/eshop/orders/Fees/DeliveryCountryFee.php',
    'Eshop\\Orders\\Fees\\DeliveryList' => $baseDir . '/modules/eshop/orders/Fees/DeliveryList.php',
    'Eshop\\Orders\\Fees\\DeliveryType' => $baseDir . '/modules/eshop/orders/Fees/DeliveryType.php',
    'Eshop\\Orders\\Fees\\Exception' => $baseDir . '/modules/eshop/orders/Fees/Exception.php',
    'Eshop\\Orders\\Fees\\MapperDelivery' => $baseDir . '/modules/eshop/orders/Fees/MapperDelivery.php',
    'Eshop\\Orders\\Fees\\MapperPayment' => $baseDir . '/modules/eshop/orders/Fees/MapperPayment.php',
    'Eshop\\Orders\\Fees\\PaymentCountryFee' => $baseDir . '/modules/eshop/orders/Fees/PaymentCountryFee.php',
    'Eshop\\Orders\\Fees\\PaymentList' => $baseDir . '/modules/eshop/orders/Fees/PaymentList.php',
    'Eshop\\Orders\\Fees\\PaymentType' => $baseDir . '/modules/eshop/orders/Fees/PaymentType.php',
    'Eshop\\Orders\\Fees\\RepositoryDelivery' => $baseDir . '/modules/eshop/orders/Fees/RepositoryDelivery.php',
    'Eshop\\Orders\\Fees\\RepositoryPayment' => $baseDir . '/modules/eshop/orders/Fees/RepositoryPayment.php',
    'Eshop\\Orders\\Fees\\Service' => $baseDir . '/modules/eshop/orders/Fees/Service.php',
    'Eshop\\Orders\\Order' => $baseDir . '/modules/eshop/orders/order.php',
    'Eshop\\Orders\\OrderItem' => $baseDir . '/modules/eshop/orders/order.php',
    'Eshop\\Orders\\Payment' => $baseDir . '/modules/eshop/orders/paymentslist.php',
    'Eshop\\Orders\\PaymentsList' => $baseDir . '/modules/eshop/orders/paymentslist.php',
    'Eshop\\Products\\Categories' => $baseDir . '/modules/eshop/products/categories.php',
    'Eshop\\Products\\Category' => $baseDir . '/modules/eshop/products/category.php',
    'Eshop\\Products\\DataCategory' => $baseDir . '/modules/eshop/products/datacategory.php',
    'Eshop\\Products\\DataGalleryImage' => $baseDir . '/modules/eshop/products/datagalleryimage.php',
    'Eshop\\Products\\DataIcon' => $baseDir . '/modules/eshop/products/dataicon.php',
    'Eshop\\Products\\DataIconImage' => $baseDir . '/modules/eshop/products/dataiconimage.php',
    'Eshop\\Products\\DataImage' => $baseDir . '/modules/eshop/products/dataimage.php',
    'Eshop\\Products\\DataProduct' => $baseDir . '/modules/eshop/products/dataproduct.php',
    'Eshop\\Products\\GalleryImage' => $baseDir . '/modules/eshop/products/galleryimage.php',
    'Eshop\\Products\\Icon' => $baseDir . '/modules/eshop/products/icon.php',
    'Eshop\\Products\\IconImage' => $baseDir . '/modules/eshop/products/iconimage.php',
    'Eshop\\Products\\Image' => $baseDir . '/modules/eshop/products/image.php',
    'Eshop\\Products\\Mapper' => $baseDir . '/modules/eshop/products/Mapper.php',
    'Eshop\\Products\\Models3d\\ControllerBackend' => $baseDir . '/modules/eshop/products/Models3d/ControllerBackend.php',
    'Eshop\\Products\\Models3d\\File' => $baseDir . '/modules/eshop/products/Models3d/File.php',
    'Eshop\\Products\\Models3d\\Mapper' => $baseDir . '/modules/eshop/products/Models3d/Mapper.php',
    'Eshop\\Products\\Models3d\\Service' => $baseDir . '/modules/eshop/products/Models3d/Service.php',
    'Eshop\\Products\\Product' => $baseDir . '/modules/eshop/products/product.php',
    'Eshop\\Products\\Repository' => $baseDir . '/modules/eshop/products/Repository.php',
    'Eshop\\Products\\ServicePrices' => $baseDir . '/modules/eshop/products/ServicePrices.php',
    'GstBrowserCacheDir' => $baseDir . '/other/filebrowser/connector/connector.php',
    'GstBrowserConnector' => $baseDir . '/other/filebrowser/connector/connector.php',
    'GstBrowserFile' => $baseDir . '/other/filebrowser/connector/connector.php',
    'GstLib\\Cnb\\Exception' => $baseDir . '/core/GstLib/Cnb/Exception.php',
    'GstLib\\Cnb\\Rate' => $baseDir . '/core/GstLib/Cnb/Rate.php',
    'GstLib\\Cnb\\Rates' => $baseDir . '/core/GstLib/Cnb/Rates.php',
    'GstLib\\Date' => $baseDir . '/core/GstLib/Date.php',
    'GstLib\\Db\\Criteria' => $baseDir . '/core/GstLib/Db/Criteria.php',
    'GstLib\\Db\\DatabaseErrorException' => $baseDir . '/core/GstLib/Db/Abstract.php',
    'GstLib\\Db\\DriverAbstract' => $baseDir . '/core/GstLib/Db/Abstract.php',
    'GstLib\\Db\\DriverMysqli' => $baseDir . '/core/GstLib/Db/Mysqli.php',
    'GstLib\\FileCache' => $baseDir . '/core/GstLib/FileCache.php',
    'GstLib\\Filesystem' => $baseDir . '/core/GstLib/Filesystem.php',
    'GstLib\\Filesystem\\IdFile' => $baseDir . '/core/GstLib/Filesystem/IdFile.php',
    'GstLib\\Filesystem\\Utils' => $baseDir . '/core/GstLib/Filesystem/Utils.php',
    'GstLib\\Html' => $baseDir . '/core/GstLib/Html.php',
    'GstLib\\Html\\Url' => $baseDir . '/core/GstLib/Html/Url.php',
    'GstLib\\ImageSharpen' => $baseDir . '/core/GstLib/Resizer.php',
    'GstLib\\Resizer' => $baseDir . '/core/GstLib/Resizer.php',
    'GstLib\\RichEdit' => $baseDir . '/core/GstLib/RichEdit.php',
    'GstLib\\Sitemap' => $baseDir . '/core/GstLib/Sitemap.php',
    'GstLib\\SitemapIndexItem' => $baseDir . '/core/GstLib/Sitemap.php',
    'GstLib\\SitemapItem' => $baseDir . '/core/GstLib/Sitemap.php',
    'GstLib\\Template' => $baseDir . '/core/GstLib/Template.php',
    'GstLib\\Translator' => $baseDir . '/core/GstLib/Translator.php',
    'GstLib\\Tree' => $baseDir . '/core/GstLib/Tree.php',
    'GstLib\\Upload' => $baseDir . '/core/GstLib/Upload.php',
    'GstLib\\UploadException' => $baseDir . '/core/GstLib/Upload.php',
    'GstLib\\Upload\\Constraints' => $baseDir . '/core/GstLib/Upload/Constraints.php',
    'GstLib\\Upload\\ConstraintsFile' => $baseDir . '/core/GstLib/Upload/ConstraintsFile.php',
    'GstLib\\Upload\\ConstraintsImage' => $baseDir . '/core/GstLib/Upload/ConstraintsImage.php',
    'GstLib\\Upload\\Exception' => $baseDir . '/core/GstLib/Upload/Exception.php',
    'GstLib\\Upload\\Uploader' => $baseDir . '/core/GstLib/Upload/Uploader.php',
    'GstLib\\UserFile' => $baseDir . '/core/GstLib/UserFile.php',
    'GstLib\\Vars' => $baseDir . '/core/GstLib/Vars.php',
    'Includes\\Gui' => $baseDir . '/modules/includes/gui.php',
    'Includes\\Module' => $baseDir . '/modules/includes/module.php',
    'Menu\\Admin\\ControllerItems' => $baseDir . '/modules/menu/admin/controlleritems.php',
    'Menu\\Controller' => $baseDir . '/modules/menu/controller.php',
    'Menu\\DataItem' => $baseDir . '/modules/menu/dataitem.php',
    'Menu\\Menu' => $baseDir . '/modules/menu/menu.php',
    'Menu\\MenuItem' => $baseDir . '/modules/menu/menuitem.php',
    'Menu\\MenuItems' => $baseDir . '/modules/menu/menuitems.php',
    'Menu\\Module' => $baseDir . '/modules/menu/module.php',
    'Onlinetechnology\\Application' => $baseDir . '/modules/app.php',
    'PHPMailer\\PHPMailer\\Exception' => $vendorDir . '/phpmailer/phpmailer/src/Exception.php',
    'PHPMailer\\PHPMailer\\OAuth' => $vendorDir . '/phpmailer/phpmailer/src/OAuth.php',
    'PHPMailer\\PHPMailer\\PHPMailer' => $vendorDir . '/phpmailer/phpmailer/src/PHPMailer.php',
    'PHPMailer\\PHPMailer\\POP3' => $vendorDir . '/phpmailer/phpmailer/src/POP3.php',
    'PHPMailer\\PHPMailer\\SMTP' => $vendorDir . '/phpmailer/phpmailer/src/SMTP.php',
    'Search\\Controller' => $baseDir . '/modules/search/controller.php',
    'Search\\Data' => $baseDir . '/modules/search/data.php',
    'Search\\Module' => $baseDir . '/modules/search/module.php',
    'Staticarticles\\Admin\\Controller' => $baseDir . '/modules/staticarticles/admin/controller.php',
    'Staticarticles\\Article' => $baseDir . '/modules/staticarticles/article.php',
    'Staticarticles\\Config' => $baseDir . '/modules/staticarticles/config.php',
    'Staticarticles\\Data' => $baseDir . '/modules/staticarticles/data.php',
    'Staticarticles\\DataSitemap' => $baseDir . '/modules/staticarticles/data_sitemap.php',
    'Staticarticles\\Gui' => $baseDir . '/modules/staticarticles/gui.php',
    'Staticarticles\\Image' => $baseDir . '/modules/staticarticles/image.php',
    'Staticarticles\\Module' => $baseDir . '/modules/staticarticles/module.php',
    'Tracy\\Bar' => $vendorDir . '/tracy/tracy/src/Tracy/Bar.php',
    'Tracy\\BlueScreen' => $vendorDir . '/tracy/tracy/src/Tracy/BlueScreen.php',
    'Tracy\\Bridges\\Nette\\Bridge' => $vendorDir . '/tracy/tracy/src/Bridges/Nette/Bridge.php',
    'Tracy\\Bridges\\Nette\\MailSender' => $vendorDir . '/tracy/tracy/src/Bridges/Nette/MailSender.php',
    'Tracy\\Bridges\\Nette\\TracyExtension' => $vendorDir . '/tracy/tracy/src/Bridges/Nette/TracyExtension.php',
    'Tracy\\Debugger' => $vendorDir . '/tracy/tracy/src/Tracy/Debugger.php',
    'Tracy\\DefaultBarPanel' => $vendorDir . '/tracy/tracy/src/Tracy/DefaultBarPanel.php',
    'Tracy\\Dumper' => $vendorDir . '/tracy/tracy/src/Tracy/Dumper.php',
    'Tracy\\FireLogger' => $vendorDir . '/tracy/tracy/src/Tracy/FireLogger.php',
    'Tracy\\Helpers' => $vendorDir . '/tracy/tracy/src/Tracy/Helpers.php',
    'Tracy\\IBarPanel' => $vendorDir . '/tracy/tracy/src/Tracy/IBarPanel.php',
    'Tracy\\ILogger' => $vendorDir . '/tracy/tracy/src/Tracy/ILogger.php',
    'Tracy\\Logger' => $vendorDir . '/tracy/tracy/src/Tracy/Logger.php',
    'Tracy\\OutputDebugger' => $vendorDir . '/tracy/tracy/src/Tracy/OutputDebugger.php',
);
