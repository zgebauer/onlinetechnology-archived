<?php

/**
 * frontend entry page
 *
 * @package Ergo
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 */
require_once __DIR__ . '/config/config.php';
require ERGO_ROOT_DIR . 'vendor/autoload.php';
require_once ERGO_ROOT_DIR.'core/base.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';

ergoSetEnvironment();
ergoSetErrHandler();

$config = new Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
$config->setUrlRewriteEnabled(ERGO_REWRITE_ENABLED);
$config->mode(ERGO_RUN_MODE);
$config->defaultLanguage('cs');
$config->useSession(true);
$config->facade('onlinetech');
$config->isAdmin(false);
$config->adminPath('admin');
$config->translate(true);
$config->moduleAliases(
    array(
        'articles' => 'clanky',
        'staticarticles' => 'stranky',
        'search' => 'hledani'
    )
);
$config->pageCacheEnabled(ERGO_PAGE_CACHE_ENABLE);
$config->pageCacheSkip(array('ajax-', '/profil', '/objednavka', '/kosik', '/hledani'));
$config->pageCacheExpire(36000);

$lang = 'cs';
$allowLang = array('cs', 'en');
session_start();
if (isset($_GET['lang']) && in_array($_GET['lang'], $allowLang)) {
    $lang = $_SESSION['lang'] = $_GET['lang'];
	die(header('Location: ' . ERGO_ROOT_URL));
} else if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
}
$config->setLanguage($lang);

$app = new Onlinetechnology\Application();
$app->run($config);

