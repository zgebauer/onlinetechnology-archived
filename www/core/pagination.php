<?php
/**
 * @package Ergo
 */

namespace Ergo;

use GstLib\Html;

/**
 * count positions for navigation through list of records
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
class Pagination
{
    /** @var type of pagination (?pos=10) */
    const POS_AS_PARAM = 0;
    /** @var type of pagination (/pos-10) */
    const POS_AS_DIR = 1;
    /** @var type of pagination (/pos/10) */
    const POS_AS_SUBDIR = 2;
    /** @var name of wariable holding position */
    const POS_VARIABLE = 'pos';
    /** @var int total number of records in list */
    private $_totalRows;
    /** @var int current positions in list */
    private $_currentPos;
    /* @var int $rowsPage number of records per page */
    private $_rowsPage;
    /** @var array array with positions for each page */
    private $_pages;
    /** @var int current position */
    private $_currentPage;
    /** @var int current page number */
    private $_currentPageNo;
    /** @var int position of previous page */
    private $_prev;
    /** @var int position of next page */
    private $_next;
    /** @var int way of including position in url */
    private $_posType;
    /** @var array additional parameters to url */
    private $_queryParams;

    /**
     * constructor
     * @param int $totalRows total number of records
     * @param int $currentPos current position in list
     * @param int $rowsPage records per page [1-1000]
     */
    public function __construct($totalRows, $currentPos, $rowsPage)
    {
        $this->_totalRows = $this->_currentPos = 0;
        $this->_rowsPage = 10;

        $this->_pages = array();
        $this->_currentPage = $this->_currentPageNo = 0;
        $this->_prev = $this->_next = 0;
        $this->_posType = self::POS_AS_PARAM;
        $this->_queryParams = array();

        if ((int) $totalRows >= 0) {
            $this->_totalRows = (int) $totalRows;
        } else {
            user_error('incorrect total number:'.$totalRows, E_USER_NOTICE);
        }

        if ((int) $currentPos >= 0) {
            $this->_currentPos = (int) $currentPos;
        } else {
            user_error('incorrect position:'.$currentPos, E_USER_NOTICE);
        }

        $rowsPage = (int) $rowsPage;
        if ($rowsPage > 0 && $rowsPage <= 1000) {
            $this->_rowsPage = $rowsPage;
        } else {
            user_error('incorrect rows per page:'.$rowsPage, E_USER_NOTICE);
        }

        $this->recalc();
    }

    /**
     * returns total number of records
     * @return int
     */
    public function getTotalRows()
    {
        return $this->_totalRows;
    }

    /**
     * returns current position in list
     * @return int
     */
    public function getCurrentPos()
    {
        return $this->_currentPos;
    }

    /**
     * sets additional parameters for including in url
     * @param array $params assoc array with parameters
     * @return \Ergo\Pagination
     */
    public function setQueryParams(array $params)
    {
        $this->_queryParams = array();
        foreach ($params as $key => $value) {
            $this->_queryParams[trim($key)] = trim($value);
        }
        return $this;
    }

    /**
     * sets type of pagination in url. Affects url in output of subsequent methods getUrl
     * @param int $type type of pagination in url
     * @return \Ergo\Pagination
     */
    public function setPosType($type)
    {
        if (in_array($type, array(self::POS_AS_PARAM, self::POS_AS_DIR, self::POS_AS_SUBDIR))) {
            $this->_posType = $type;
        }
        return $this;
    }

    /**
     * returns array of positions for pages
     * @return array
     */
    public function getPages()
    {
        if (!isset($this->_pages[0])) {
            $this->recalc();
        }
        return $this->_pages;
    }

    /** recalculate positions */
    public function recalc()
    {
        $this->_pages = array();
        $pageCounter = 0;
        for ($i = 1; $i<=$this->_totalRows; $i+=$this->_rowsPage) {
            $this->_pages[] = $i;
            if ($i <= $this->_currentPos) {
                $this->_currentPage = $i;
                $this->_currentPageNo = $pageCounter;
            }
            $pageCounter++;
        }

        $this->_prev = $this->_currentPage - $this->_rowsPage;
        if ($this->_prev < 1) {
            $this->_prev = 0;
        }
        $this->_next = $this->_currentPage + $this->_rowsPage;
        if ($this->_next > $this->_totalRows) {
            $this->_next = 0;
        }
    }

    /**
     * $options can contains indices:
     * 'cssSelected' - css class of current page
     * 'descPageNum' - if set, show page numbers as link description
     * 'delimiter' - string used as delimiter between links
     *
     * @param string $url base url without position variable
     * @param string $posVar name of variable holding position
     * @param int $range max number of visible links before/after current position
     * @param array $options
     * @return string links to pages, delimited with space
     */
    public function getLinks($url, $posVar = self::POS_VARIABLE, $range = null, array $options = array())
    {
        $options['delimiter'] = (isset($options['delimiter']) ? $options['delimiter'] : '');

        $start = 0;
        $end = count($this->_pages);
        if (!is_null($range) && $end > 2*$range) {
            $start = $this->_currentPageNo - $range;
            $start = ($start < 0 ? 0 : $start);
            $end = $this->_currentPageNo + $range + 1;
            $end = (($end > count($this->_pages)) ? count($this->_pages) : $end);
        }

        $links = array();
        for ($i = $start; $i < $end; $i++) {
            $css = '';
            if (isset($options['cssSelected']) && $this->_currentPageNo == $i) {
                $css = ' class="'.$options['cssSelected'].'"';
            }
            $desc = $this->_pages[$i];
            if (isset($options['descPageNum'])) {
                $desc = $i+1;
            }

            $links[] = '<a href="'.
                self::createUrl($url, $posVar, $this->_pages[$i], $this->_posType, $this->_queryParams).
                '"'.$css.'>'.$desc.'</a>';
        }
        return join($options['delimiter'], $links);
    }

    /**
     * returns url with position
     * @param string $url
     * @param string $posVar
     * @param int $pos
     * @param int $posType type of pagination in url
     * @param array $queryParams additional parameters for including in url
     * @return string
     */
    public static function createUrl($url, $posVar, $pos, $posType = self::POS_AS_PARAM, array $queryParams = array())
    {
        if ($pos > 1) {
            switch ($posType) {
            case self::POS_AS_DIR:
                $url .= '/'.$posVar.'-'.$pos;
                break;
            case self::POS_AS_SUBDIR:
                $url .= '/'.$posVar.'/'.$pos;
                break;
            default:
                $url = Html::queryUrl($url, array($posVar=>$pos));
                break;
            }
        }
        return Html::queryUrl($url, $queryParams);
    }

    /**
     * $options can contains indices:
     * 'cssSelected' - css class of current page
     * 'descPageNum' - if set, show page numbers as link description
     *
     * @param string $url base url without position variable
     * @param string $posVar name of variable holding position
     * @param int $range max number of visible options before/after current position
     * @return string html tag options with links to pages
     */
    public function getOptions($url, $posVar = self::POS_VARIABLE, $range = null)
    {
        $url = Html::fixUrl($url);
        $ret = '';
        $start = 0;
        $end = count($this->_pages);
        if (!is_null($range) && $end > (2*$range)) {
            $start = $this->_currentPageNo - $range;
            $start = ($start < 0 ? 0 : $start);
            $end = $this->_currentPageNo + $range + 1;
            $end = (($end > count($this->_pages)) ? $end = count($this->_pages) : $end);
        }

        for ($i = $start; $i < $end; $i++) {
            $selected = Html::isSelected($this->_currentPageNo == $i);
            $ret .= '<option value="'.
                Html::queryUrl($url, array($posVar=>$this->_pages[$i])).'"'.$selected.'>'.($i+1).'</option>';
        }
        return $ret;
    }

    /**
     * returns url to previous page of list or empty string
     * @param string $url base url without position variable
     * @param string $posVar name of variable holding position
     * @return string
     */
    public function getUrlPrev($url, $posVar = self::POS_VARIABLE)
    {

        if ($this->_prev == 0) {
            return '';
        }
        return self::createUrl(Html::fixUrl($url), $posVar, $this->_prev, $this->_posType, $this->_queryParams);
    }

    /**
     * returns url to next page of list or empty string
     * @param string $url base url without position variable
     * @param string $posVar name of variable holding position
     * @return string
     */
    public function getUrlNext($url, $posVar = self::POS_VARIABLE)
    {
        if ($this->_next == 0) {
            return '';
        }
        return self::createUrl(Html::fixUrl($url), $posVar, $this->_next, $this->_posType, $this->_queryParams);
    }

    /**
     * returns string url to first page of list
     * @param string $url base url without position variable
     * @param string $posVar name of variable holding position
     * @return string
     */
    public function getUrlFirst($url, $posVar = self::POS_VARIABLE)
    {
        if (count($this->_pages) <= 1) {
            return '';
        }
        if ($this->_currentPageNo == 0) {
            return '';
        }
        return self::createUrl($url, $posVar, 1, $this->_posType, $this->_queryParams);
    }

    /**
     * returns url to last page of list
     * @param string $url base url without position variable
     * @param string $posVar name of variable holding position
     * @return string
     */
    public function getUrlLast($url, $posVar = self::POS_VARIABLE)
    {
        if (count($this->_pages) <= 1) {
            return '';
        }
        $last = count($this->_pages)-1;
        if ($this->_currentPageNo == $last) {
            return '';
        }
        return self::createUrl($url, $posVar, $this->_pages[$last], $this->_posType, $this->_queryParams);
    }

    /**
     * returns array with positions of current page (first, last record)
     * @return array
     */
    public function getRecords()
    {
        $begin = $this->_currentPage;
        $end = $begin + $this->_rowsPage - 1;
        $end = (($end > $this->_totalRows) ? $this->_totalRows : $end);
        return array($begin, $end);
    }

    /**
     * returns number of all pages
     * @return int
     */
    public function getTotalPages()
    {
        return count($this->_pages);
    }

    /**
     * chek if position is out of list (next page not exists)
     * @param int $pos
     * @param int $rowsPage
     * @param int $sum
     * @return bool
     */
    public static function isOut($pos, $rowsPage, $sum)
    {
        return (($pos + $rowsPage - 1) > (ceil($sum/$rowsPage)*$rowsPage));
    }

    /**
     * parse value of position from url
     *
     * @param string $url url with position, if not set use current url
     * @param int $posType type of pagination in url
     * @param string $posVar name of varieble holding position
     * @return int position, 0 if position is not recognized
     */
    public static function parsePosFromUrl($url = null, $posType = self::POS_AS_PARAM, $posVar = self::POS_VARIABLE)
    {
        $ret = 0;
        $url = is_null($url) ? Html::currentUrl() : trim($url);

        $posVar = trim($posVar);
        $posVar = ($posVar === '' ? self::POS_VARIABLE : $posVar);

        if (!in_array($posType, array(self::POS_AS_PARAM, self::POS_AS_DIR, self::POS_AS_SUBDIR))) {
            $posType = self::POS_AS_PARAM;
        }

        $parts = parse_url($url);
        switch ($posType) {
        case self::POS_AS_DIR:
            if (preg_match('/'.$posVar.'-([0-9]+)/s', $parts['path'], $matches)) {
                $ret = (int) $matches[1];
            }
            break;
        case self::POS_AS_SUBDIR:
            if (preg_match('/'.$posVar.'\/([0-9]+)/s', $parts['path'], $matches)) {
                $ret = (int) $matches[1];
            }
            break;
        case self::POS_AS_PARAM:
        default:
            if (isset($parts['query'])) {
                $tmp = explode('&', Html::fixAmp($parts['query']));
                foreach ($tmp as $param) {
                    if (strpos($param, '=') !== false) {
                        list($key, $value) = explode('=', $param);
                        if (trim($key) === $posVar) {
                            $ret = (int) $value;
                            break;
                        }
                    }
                }
            }
        }
        return $ret;
    }

}
