<?php
/**
 * @package Ergo
 */

namespace Ergo;

use GstLib\Html;

/**
 * holds current response data
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
class Response
{
	/** @var http status code */
	const HTTP_OK = 200;
	/** @var http status code */
	const HTTP_NO_CONTENT = 204;
	/** @var http status code */
	const HTTP_MOVED_PERMANENTLY = 301;
	/** @var http status code */
	const HTTP_FOUND = 302;
	/** @var http status code */
	const HTTP_SEE_OTHER = 303;
	/** @var http status code */
	const HTTP_NOT_MODIFIED = 304;
	/** @var http status code */
	const HTTP_TEMPORARY_REDIRECT= 307;
	/** @var http status code */
	const HTTP_BAD_REQUEST = 400;
	/** @var http status code */
	const HTTP_UNAUTHORIZED = 401;
	/** @var int http status code */
	const HTTP_FORBIDDEN = 403;
	/** @var http status code */
	const HTTP_NOT_FOUND = 404;
	/** @var http status code */
	const HTTP_INTERNAL_SERVER_ERROR = 500;

	/** @var string title of page */
	protected $_title;
	/** @var string meta description of page */
	protected $_metaDescription;
	/** @var string meta keywords of page */
	protected $_metaKeywords;
	/** @var array meta property tags */
	protected $_metaProperties;
	/** @var string canonical url of page */
	protected $_canonicalUrl;
	/** @var string content of main part of page */
	protected $_content;
	/** @var int current http code */
	protected $_httpCode;
	/** @var array of header */
	protected $_headers;
	/** @var string type of output */
	protected $_outputType;
	/** @var string target url for redirect */
	protected $_redirectUrl;

	/**
	 * constructor
	 */
	public function __construct()
	{
		$this->_title = $this->_metaDescription = $this->_metaKeywords = '';
		$this->_metaProperties = array();
		$this->_canonicalUrl = '';
		$this->_content = '';
		$this->_httpCode = self::HTTP_OK;
		$this->_headers = array();
		$this->_outputType = 'default';
		$this->_redirectUrl = '';
	}

	/**
	 * sets and return HTTP response code
	 * @param int
	 * @return int
	 */
	public function httpCode($httpCode = null)
	{
		if (!is_null($httpCode)) {
			$allow = array(200, 204, 301, 302, 303, 304, 307, 400, 401, 403, 404, 500);
			if (in_array($httpCode, $allow)) {
				$this->_httpCode = $httpCode;
			} else {
				user_error('invalid code'.$httpCode, E_USER_WARNING);
			}
		}
		return $this->_httpCode;
	}

	/**
	 * sets and returns title of page
	 * @param string
	 * @return string
	 */
	public function pageTitle($value = null)
	{
		if (!is_null($value)) {
			$this->_title = $value;
			$this->setMetaProperty('og:title', $value);
		}
		return $this->_title;
	}

	/**
	 * sets and returns meta description of page
	 * @param string
	 * @return string
	 */
	public function metaDescription($value = null)
	{
		if (!is_null($value)) {
			$this->_metaDescription = $value;
			$this->setMetaProperty('og:description', $value);
		}
		return $this->_metaDescription;
	}

	/**
	 * sets and returns meta keywords of page
	 * @param string
	 * @return string
	 */
	public function metaKeywords($value = null)
	{
		if (!is_null($value)) {
			$this->_metaKeywords = $value;
		}
		return $this->_metaKeywords;
	}

	/**
	 * sets and returns canonical url of page
	 * @param string
	 * @return string
	 */
	public function canonicalUrl($value = null)
	{
		if (!is_null($value)) {
			$this->_canonicalUrl = $value;
			$this->setMetaProperty('og:url', $value);
		}
		return $this->_canonicalUrl;
	}

	/**
	 * sets and returns content of main part of page
	 * @param string
	 * @return string
	 */
	public function pageContent($value = null)
	{
		if (!is_null($value)) {
			$this->_content = $value;
		}
		return $this->_content;
	}

	/**
	 * returns html fragment for head of page
	 * @return string
	 */
	public function pageHeader()
	{
		$ret = '';
		if ($this->_metaDescription !== '') {
			$ret .= '<meta name="Description" content="'.Html::escape($this->_metaDescription)."\" />\n";
		}
		if ($this->_metaKeywords !== '') {
			$ret .= '<meta name="Keywords" content="'.Html::escape($this->_metaKeywords)."\" />\n";
		}
		if ($this->_canonicalUrl !== '') {
			$ret .= '<link rel="canonical" href="'.$this->_canonicalUrl."\" />\n";
		}
		foreach ($this->_metaProperties as $name=>$value) {
			if ($value !== '') {
				$ret .= '<meta property="'.Html::escape($name).'" content="'.Html::escape($value)."\" />\n";
			}
		}
		return trim($ret);
	}

	/**
	 * add or replace meta header
	 * @param string header name
	 * @param string header value
	 * @return \Ergo\Response
	 */
	public function setHeader($name, $value)
	{
		$this->_headers[$name] = $value;
		return $this;
	}

	/**
	 * returns headers
	 * @return array
	 */
	public function headers()
	{
		return $this->_headers;
	}

	/**
	 * sets Content-Type header
	 * @param string mime
	 * @param string charset
	 */
	public function setContentType($type, $charset = null)
	{
		$this->setHeader('Content-Type', $type.($charset ? '; charset='.$charset : ''));
	}

	/**
	 * redirect user to another url with specified http code (301=moved permanently, 302=found, 303=see other).
	 * If $_SERVER['HTTP_HOST'] is not set, print target url and code to stdout
	 *
	 * @param string $url
	 * @param int $httpCode http response code (3xx)
	 */
	public function redirect($url, $httpCode = self::HTTP_FOUND)
	{
		$this->_redirectUrl = $url;
		$this->httpCode($httpCode);

		if (!isset($_SERVER['HTTP_HOST'])) {
			// script was run from command line (phpunit?)
			return;
		}
		// @codeCoverageIgnoreStart
		if (headers_sent($file, $line)) {
			user_error('Headers already sent in '.$file.' on line '.$line);
		}
		$url = str_replace('&amp;', '&', $url);
		die(header('Location: '.$url, true, $this->httpCode()));
		// @codeCoverageIgnoreEnd
	}

	/**
	 * returns target url for redirect
	 * @return string
	 */
	public function redirectUrl()
	{
		return $this->_redirectUrl;
	}

	/**
	 * set output type as JSON
	 * @param mixed $value
	 * @param int $httpCode
	 */
	public function setOutputJson($value, $httpCode = self::HTTP_OK)
	{
		$this->_outputType = 'json';
		$this->httpCode($httpCode);
		$this->pageContent(json_encode($value));
	}

	/**
	 * set output type as "none" with http status code
	 * @param int $httpCode
	 */
	public function setOutputNone($httpCode)
	{
		$this->_outputType = 'none';
		$this->httpCode($httpCode);
		$this->pageContent('');
	}

	/**
	 * offers file for download
	 * download file will stop executing of script!
	 *
	 * @param string $fullPath absolute path of file for download
	 * @param string $mime mime type of file
	 * @param bool $forceDownload force download dialog
	 * @param string $saveAsFileName offer filename (default real filename)
	 * @return bool false on error
	 */
	public function download($fullPath, $mime, $forceDownload = true, $saveAsFileName = '')
	{
		$fullPath = trim($fullPath);
		$saveAsFileName = trim($saveAsFileName);

		if ($saveAsFileName === '') {
			$saveAsFileName = basename($fullPath);
		}

		if (!is_file($fullPath)) {
			user_error('file_not_found:'.$fullPath, E_USER_WARNING);
			return false;
		}

		$size = filesize($fullPath);

		if (ERGO_TESTING) {
			 $data = array(
				'filename' => $saveAsFileName,
				'mime' => $mime,
				'size' => $size
			);
			$GLOBALS['phpunit_download'] = $data;
			return;
		}

		// @codeCoverageIgnoreStart
		if ($forceDownload) {
			header('Content-Type: application/force-download');
			header('Content-Type: application/download');
		}
		header('Content-Type: '.$mime);
		header('Content-Disposition: attachment; filename='.$saveAsFileName."\n");
		header("Content-Transfer-encoding: binary\n");
		header('Content-length: '.$size."\n");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		readfile($fullPath);
		die();
		// @codeCoverageIgnoreEnd
	}

	/**
	 * returns http code as string incl protocol
	 * @param int $httpCode
	 * @return string
	 */
	protected function _headersHttpCode($httpCode)
	{
		$headers = array(
			200 => 'HTTP/1.0 200 OK',
			204 => 'HTTP/1.0 204 No Content',
			400 => 'HTTP/1.0 400 Bad Request',
			403 => 'HTTP/1.0 403 Forbidden',
			404 => 'HTTP/1.0 404 Not Found'
		);
		return (isset($headers[$httpCode]) ? ($headers[$httpCode]) : $httpCode);
	}

	/**
	 * send response data to stdout
	 */
	public function output()
	{
		if ($this->_outputType === 'json') {
			header('Content-type: application/json');
			die($this->_content);
		}
		if ($this->_outputType === 'none') {
			$this->sendHeaders();
			die($this->_content);
		}
		die();
	}

	/**
	 * send http headers
	 */
	public function sendHeaders()
	{
		$statusHeader = $this->_headersHttpCode($this->_httpCode);
		if ($statusHeader !== '') {
			header($this->_headersHttpCode($this->_httpCode));
		}
		foreach ($this->_headers as $name=>$value) {
			header($name.': '.$value);
		}
	}

	/**
	 * check if output type is other than default
	 * @return bool
	 */
	public function isSpecialOutput()
	{
		return ($this->_outputType !== 'default');
	}

	/**
	 * sets tag  meta property
	 * @param string $name name of property
	 * @param string $value value of property
	 */
	public function setMetaProperty($name, $value)
	{
		$this->_metaProperties[trim($name)] = trim($value);
	}

	/**
	 * sets tag  meta property only if property has not been set yet
	 * @param string $name name of property
	 * @param string $value value of property
	 */
	public function setMetaPropertyIfEmpty($name, $value)
	{
		if (!isset($this->_metaProperties[trim($name)]) || $this->_metaProperties[trim($name)] === '') {
			$this->setMetaProperty($name, $value);
		}
	}
}
