<?php
/**
 * @package Ergo
 */

namespace Ergo;

/**
 * interface for configuration object
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
interface ConfigInterface
{
    /**
     * constructor
     * @param string $baseUrl base url of web
     * @param string $baseDir base directory of web
     */
    public function __construct($baseUrl, $baseDir);

    /**
     * sets flag "mod_rewrite enabled"
     * @param bool $value
     */
    public function setUrlRewriteEnabled($value);

    /**
     * sets language, expect 2char language code
     * @param string $value
     */
    public function setLanguage($value);

    /**
     * returns and sets facade
     * @param string $value
     * @return string
     */
    public function facade($value);

}