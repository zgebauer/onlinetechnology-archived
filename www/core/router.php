<?php
/**
 * @package Ergo
 */

namespace Ergo;

/**
 * parse url into module and parameter
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
class Router
{
    /** @var string base url */
    private $_baseUrl;
    /** @var string current module */
    private $_module;
    /** @var string current parameter */
    private $_parameter;
    /** @var array allowed modules */
    private $_allowModules;
    /** @var string default module */
    private $_defaultModule;
    /** @var array associative array of modules aliases */
    private $_moduleAliases;

    /**
     * constructor
     * @param string base url, root of web
     * @param arry aliases of modules on format modulename=>alias
     */
    public function __construct($baseUrl, array $moduleAliases = array())
    {
        $this->_baseUrl = $baseUrl;
        $this->_module = $this->_parameter = '';
        $this->_allowModules = array();
        $this->_defaultModule = '';
        $this->_moduleAliases = $moduleAliases;
    }

    /**
     * returns current module
     * @return string
     */
    public function getModule()
    {
        return $this->_module;
    }

    /**
     * returns current parameter
     * @return string
     */
    public function getParameter()
    {
        return $this->_parameter;
    }

    /**
     * sets allowed modules
     * @param array $allowModules array of allowed modules
     * @param string $defaultModule name of default module
     * @return Gst_Router $instance
     */
    public function setModules(array $allowModules, $defaultModule = '')
    {
        $this->_allowModules = $allowModules;
        $this->_defaultModule = $defaultModule;
        return $this;
    }

    /**
     * parse url into module and parameter
     * @param string $string
     */
    public function parseUrl($url)
    {
        $queryString = str_replace($this->_baseUrl, '', $url);
        // remove other parameters
        $parts = explode('&', str_replace('?', '&', $queryString));
        $queryString = reset($parts);
        $parts = array_map('urldecode', explode('/', $queryString));
        // exists alias of module?
        $key = array_search($parts[0], $this->_moduleAliases);
        if ($key) {
            $parts[0] = $key;
        }

        if (isset($parts[0]) && in_array($parts[0], $this->_allowModules)) {
            $this->_module = $parts[0];
            $this->_parameter = join('/', array_slice($parts, 1));
        } else {
            $this->_module = $this->_defaultModule;
            $this->_parameter = join('/', $parts);
        }
    }

    /**
     * returns complete url combined from base url, language, module and parameter
     * @param string $baseUrl
     * @param string $module name of module
     * @param string $parameter parameter for module
     * @return string
     */
    public function createUrl($baseUrl, $module, $parameter = null)
    {
        $parts = array();
        if ($module != '' && $module != $this->_defaultModule) {
            if (isset($this->_moduleAliases[$module])) {
                $module = $this->_moduleAliases[$module];
            }
            $parts[] = $module;
        }
        if ($parameter != '') {
            $parts[] = str_replace('%2F', '/', urlencode($parameter));
        }
        return $baseUrl.join('/', $parts);
    }

}