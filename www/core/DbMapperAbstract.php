<?php

namespace Ergo;

abstract class DbMapperAbstract extends \Ergo\Component {

	/** @var \GstLib\Db\DriverAbstract $connection */
	protected $con;

	/**
	 * constructor
	 * @param \Ergo\ApplicationInterface $application
	 */
	public function __construct(\Ergo\ApplicationInterface $application) {
		parent::__construct($application);
		$this->con = $this->app->getInstance('GstLib\Db\DriverMysqli');
	}

}
