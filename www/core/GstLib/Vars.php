<?php
/**
 * class Gst_Var
 *
 * @package GstLib
 */

namespace GstLib;

/**
 * various functions for manipulations with variables
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   GstLib
 */
class Vars
{
    /**
     * returns value cast to boolean
     * @param int $value
     * @return bool
     */
    public static function int2bool($value)
    {
        return (intval($value) != 0);
    }

    /**
     * returns value cast to integer
     * @param bool $value
     * @return int
     */
    public static function bool2int($value)
    {
        return ($value ? 1 : 0);
    }

    /**
     * returns value as human-readable number
     * @param float $value number to convert
     * @param int $decimals number of digits after decimal point
     * @param string $decPoint string representing decimal point
     * @param string $thousandSeparator string representing thousands separator
     * @return string
     */
    public static function formatNumberHuman($value, $decimals, $decPoint = '.', $thousandSeparator = ' ')
    {
        return number_format($value, $decimals, $decPoint, $thousandSeparator);
    }

    /**
     * returns value converted to float
     * @param string $number
     * @return float
     */
    public static function floatval($number)
    {
        $ret = trim(str_replace(' ', '', $number));
        $ret = str_replace(',', '.', $ret);
        return floatval($ret);
    }

    /**
     * returns string padded and cropped to exact length
     * @param string $string
     * @param int $length length of returned string
     * @param string $padString padding character
     * @param int $padType one of STR_PAD_* constants
     * @return string
     */
    public static function strPadCut($string, $length, $padString, $padType)
    {
        $ret = str_pad($string, $length, $padString, $padType);
        return substr($ret, 0, $length);
    }

    /**
     * returns random alphanumeric string (only ascii chars and digits)
     * @param int $length of required string
     * @return string
     */
    public static function randomString($length)
    {
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjklmnpqrstuvwxyz';
        mt_srand((double)microtime()*1000000);
        $ret = '';
        for ($i = 0; $i<$length; $i++) {
            $ret .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $ret;
    }

    /**
     * wraper for functions substr() and mb_substr)()
     * @param string $string input string
     * @param int $start  first position used in $string
     * @param int $length max length of the returned string, null=return rest of string
     * @param string $encoding encoding of string, null=mb_internal_encoding()
     * @return string
     */
    public static function substr($string, $start, $length = null, $encoding = null)
    {
        if (function_exists('mb_substr')) {
            if (is_null($length)) $length = mb_strlen($string);
            if (is_null($encoding)) $encoding = mb_internal_encoding();
            return mb_substr($string, $start, $length, $encoding);
        }
        // @codeCoverageIgnoreStart
        return substr($string, $start, $length);
        // @codeCoverageIgnoreEnd
    }

    /**
     * $options allow indices:
     * 'add' - string added to shortened $string
     *
     * @param string $string input string
     * @param int $length length of the returned string
     * @param int $over maximal additional length of the returned string
     * @param array $options
     * @return string first part of string with specified length with wordwrap
     */
    public static function perex($string, $length, $over = null, array $options = array())
    {
        $tmp = wordwrap(trim($string), $length, '[DELIMITER]');
        $tmp = explode('[DELIMITER]', $tmp);
        $ret = $tmp[0];
        if (!is_null($over) && (strlen($ret) > $length + $over)) {
            $ret = self::substr($ret, 0, $length + $over);
        }
        if (isset($options['add']) && strlen($ret) < strlen($string)) {
            $ret .= $options['add'];
        }
        return $ret;
    }

    /**
     * returns amount and currency by local settings
     * @param float $amount
     * @param string $currency ISO currency code
     * @param int $decimals number of digits after decimal point
     * @param string $decPoint string representing decimal point
     * @param string $thousandSeparator string representing thousands separator
     * @return string amount and currency by local settings
     */
    public static function formatCurrency($amount, $currency, $decimals = 2, $decPoint = '.', $thousandSeparator = ' ')
    {
        $retCurrency = $currency;
        $currencies = array(
        'CZK'=>'Kč',
        'EUR'=>'&euro;',
        'PLN'=>'zł',
        'SKK'=>'Sk',
        'UAH'=>'&#8372;',
        'USD'=>"\$");
        if (isset($currencies[$currency])) {
            $retCurrency = $currencies[$currency];
        }
        $retAmount = self::formatNumberHuman($amount, $decimals, $decPoint, $thousandSeparator);
        switch($currency) {
            case 'USD':
                return $retCurrency.' '.$retAmount;
            case 'EUR':
                return '<span class="currency">' . $retCurrency. '</span>' . $retAmount;
            default:
                break;
        }
        return $retAmount . ' <span class="currency">' . $retCurrency . '</span>';
    }

    public static function formatCurrencyPlain($amount, $currency, $decimals = 2, $decPoint = '.', $thousandSeparator = ' ')
    {
        $retCurrency = $currency;
        $currencies = array(
        'CZK'=>'Kč',
        'EUR'=>'EUR',
        'PLN'=>'zł',
        'SKK'=>'Sk',
        'UAH'=>'UAH',
        'USD'=>"\$");
        if (isset($currencies[$currency])) {
            $retCurrency = $currencies[$currency];
        }
        $retAmount = self::formatNumberHuman($amount, $decimals, $decPoint, $thousandSeparator);
        switch($currency) {
            case 'USD':
                return $retCurrency . ' ' . $retAmount;
            default:
                break;
        }
        return $retAmount . ' ' .$retCurrency;
    }

    /**
     * returns cyrilic string converted to latin alphabet
     * @param string $string
     * @return string
     * @see    http://en.wikipedia.org/wiki/GOST_16876-71
     */
    public static function cyrilic2Latin($string)
    {
        $convert = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
            'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'й' => 'jj', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '',
            'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D',
            'Е' => 'E', 'Ё' => 'JO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'JJ', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ф' => 'F', 'Х' => 'KH', 'Ц' => 'C', 'Ч' => 'CH',
            'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '',
            'Э' => 'EH', 'Ю' => 'JU', 'Я' => 'JA',
        );
        return strtr($string, $convert);
    }

    /**
     * from given text parse words with minimal length
     *
     * @param string $string
     * @param int $minLength
     * @return array
     */
    public static function parseKeywords($string, $minLength = 5)
    {
        $words = array();
        $minLength = ($minLength > 2 && $minLength < 50 ? $minLength : 5);
        $tmp = explode(' ', $string);
        foreach ($tmp as $word) {
            $trans = array(','=>'', '"' =>'');
            $word = trim(strtr($word, $trans));
            if (strlen($word) >= $minLength) {
                $words[] = $word;
            }
        }
        return $words;
    }

    /**
     * return hash of password
     * @param int $userId
     * @param string $password
     * @return string
     */
    public static function hashPassword($salt, $password)
    {
        return hash('sha256', $salt.$salt.$password);
    }

    /**
     * calculate price with VAT
     * @param float $basePrice base price without VAT
     * @param float $vat VAT percent
     * @param bool $round true=round result
     * @return float
     */
    public static function calcPriceVat($basePrice, $vat, $round = false)
    {
        $ret = $basePrice + ($vat/100*$basePrice);
        if ($round) {
            $ret = round($ret, 0, PHP_ROUND_HALF_UP);
        }
        return $ret;
    }
}