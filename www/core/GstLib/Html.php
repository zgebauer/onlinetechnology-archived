<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once __DIR__.'/Vars.php';

/**
 * library of common HTML related functions
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   GstLib
 */
class Html
{

    /**
     * returns escaped and fixed string for javascript
     * @param string $str string to escape and fix
     * @return string
     */
    public static function escapeForJs($str)
    {
        return str_replace(array('\\', "'", '"', "\t"), array("\\\\", "\\'", '', ''), $str);
    }

    /**
     * returns escaped and fixed string for html
     * @param string $str string to escape and fix
     * @return string
     */
    public static function escape($str)
    {
        return htmlspecialchars($str, ENT_QUOTES);
    }

    /**
     * use Gst_Html:escape()
     * @deprecated
     */
    public static function escapeForHtml($str)
    {
        return self::escape($str);
    }

    /**
     * returns string given email address as html link
     * @param string $email email address
     * @param string $description description of link
     * @param bool $obfuscate true=obfuscate e-mail address
     * @return string
     */
    public static function getEmailLink($email, $description = '', $obfuscate = false)
    {
        $description = trim($description);
        if ($description == '') {
            $description = ($obfuscate ? self::obfuscate($email) : $email);
        }
        if ($obfuscate) {
            return ('<a href="'.self::obfuscate('mailto:'.$email).'">'.$description.'</a>');
        }
        return self::getLink('mailto:'.$email, $description);
    }

    /**
     * returns obfuscated text
     * @param string $text
     * @return string
     */
    public static function obfuscate($text)
    {
        $res = '';
        for ($i = 0; $i < strlen($text); $i++) {
            $rand = mt_rand(0, 1);
            if ($rand) {
                $res .= substr($text, $i, 1);
            } else {
                $rand = mt_rand(0, 1);
                if ($rand) {
                    $res .= '&#'.ord(substr($text, $i, 1)).';';
                } else {
                    $res .= '&#x'.sprintf("%x", ord(substr($text, $i, 1))).';';
                }
            }
        }
        $replaceAt = (mt_rand(0, 1) ? '&#64;' : '&#x40;');
        return str_replace('@', $replaceAt, $res);
    }

    /**
     * returns html <option> tags created from given array. Expects array in format array[$key] = $value
     *
     * @param array $array
     * @param mixed $selectedKey selected key(s)
     * @return string
     */
    public static function getOptions(array $array, $selectedKey = '')
    {
        $ret = '';
        if (!is_array($selectedKey)) {
            $selectedKey = array($selectedKey);
        }
        foreach ($array as $key=>$value) {
            $ret .= '<option value="'.self::escapeForHtml($key).'"'.
                self::isSelected(in_array($key, $selectedKey)).'>'.$value.'</option>';
        }
        return $ret;
    }

    /**
     * returns html hyperlink
     * @param string $url address
     * @param string $description description of link
     * @param string $title title of link
     * @return string
     */
    public static function getLink($url, $description = '', $title = '')
    {
        $href = self::fixUrl($url);
        $description = trim($description);
        $description = $description == '' ? $url : $description;
        $title = trim($title);
        if ($title != '') {
            $title = ' title="'.$title.'"';
        }
        return '<a href="'.$href.'"'.$title.'>'.$description.'</a>';
    }

    /**
     * return string with removed accents and all chars except [a-z0-9_] replaced with hyphen
     * @param string $string
     * @return string
     */
    public static function fixForUrl($string)
    {
         $convert = array(
            'á'=>'a', 'Á'=>'a', 'ä'=>'a', 'Ä'=>'a','ą'=>'a', 'Ą'=>'a',
            'č'=>'c', 'Č'=>'c',
            'ď'=>'d', 'Ď'=>'d',
            'é'=>'e', 'É'=>'e', 'ě'=>'e', 'Ě'=>'e', 'ę'=>'e', 'Ę'=>'e', 'ē'=>'e', 'Ē'=>'e',
            'ë'=>'e', 'Ë'=>'e',
            'í'=>'i', 'Í'=>'i',
            'ľ'=>'l', 'Ľ'=>'l', 'ł'=>'l', 'Ł'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l',
            'ň'=>'n', 'Ň'=>'n',
            'ó'=>'o', 'Ó'=>'o', 'ö'=>'o', 'Ö'=>'o', 'ő'=>'o', 'Ő'=>'o',
            'ř'=>'r', 'Ř'=>'r',
            'š'=>'s', 'Š'=>'s',
            'ť'=>'t', 'Ť'=>'t',
            'ú'=>'u', 'Ú'=>'u', 'ů'=>'u', 'Ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'ü'=>'u', 'Ü'=>'u',
            'ý'=>'y', 'Ý'=>'y',
            'ž'=>'z', 'Ž'=>'z'
        );
        $string = strtolower(strtr($string, $convert));
        $string = preg_replace('~[^a-z0-9_]+~', '-', $string);
        return trim($string, '-');
    }

    /**
     * build query string from given arrays
     *
     * Returns $baseUrl with query string. Query string  will contain all
     * parameters from arrays given as arguments after $baseUrl.
     * Parameters with value null will be ignored.
     * Parameters with numeric indexes will be ignored.
     *
     * @param string $baseUrl
     * @param array $parameters assoc arrays with parameter for url
     * @return string
     */
    public static function queryUrl($baseUrl, $parameters = null)
    {
        $tmp = array();
        $args = func_get_args();

        if (func_num_args() == 1) {
            return self::fixUrl($baseUrl);
        }

        for ($i = 1; $i < func_num_args(); $i++) {
            if (!is_array($args[$i])) {
                continue;
            }
            $tmp = array_merge($tmp, $args[$i]);
        }

        $parameters = array();
        foreach ($tmp as $key=>$value) {
            if (is_null($value)) {
                continue;
            }
            if (is_numeric($key)) {
                continue;
            }
            $parameters[$key]=$value;
        }

        $query = http_build_query($parameters, '', '&amp;');
        if ($query != '') {
            $baseUrl .= (strpos($baseUrl, '?') === false ? '?' : '&amp;');
        }
        return self::fixUrl($baseUrl).$query;
    }

    /**
     * returns html attribute ' checked="checked"' or empty string (for input checkbox and radio)
     * @param bool $condition
     * @return string
     */
    public static function isChecked($condition)
    {
        return ($condition ? ' checked="checked"' : '');
    }

    /**
     * returns html attribute ' selected="selected"' or empty string for <option>
     * @param bool $condition
     * @return string
     */
    public static function isSelected($condition)
    {
        return ($condition ? ' selected="selected"' : '');
    }

    /**
     * try find variable in global arrays Env, Get, Post, Cookie, Session and returns its value
     * $type can contain single string or array of strings get, post, cook, cookie, sess, session, env
     * $returnAs can contain int, string, bool, html, date, datetime
     *
     * @param string $name variable name
     * @param mixed $types where search variable, string or array of strings
     * @param string $returnAs data type of returned value
     * @param string $retType prefered global var, one option from $types
     * @return mixed
     */
//    public static function getGlobal($name, $types, $returnAs = 'string', $retType = null)
//    {
//        $ret = null;
//        if (!is_array($types)) {
//            $types = array($types);
//        }
//
//        foreach ($types as $type) {
//            switch ($type) {
//                case 'get':
//                    if (!isset($_GET[$name])) {
//                        continue;
//                    }
//                    $ret = trim(urldecode($_GET[$name]));
//                    break;
//                case 'post':
//                    if (!isset($_POST[$name])) {
//                        continue;
//                    }
//                    $ret = trim($_POST[$name]);
//                    // checkbox
//                    if ($ret == 'on' && $returnAs == 'bool') {
//                        $ret = true;
//                    }
//                    break;
//                case 'cook':
//                case 'cookie':
//                    if (!isset($_COOKIE[$name])) {
//                        continue;
//                    }
//                    $ret = trim($_COOKIE[$name]);
//                    break;
//                case 'sess':
//                case 'session':
//                    if (!isset($_SESSION[$name])) {
//                        continue;
//                    }
//                    $ret = trim($_SESSION[$name]);
//                    break;
//                case 'env':
//                    if (!isset($_ENV[$name])) {
//                        continue;
//                    }
//                    $ret = trim($_ENV[$name]);
//                    break;
//            }
//            if (!is_null($ret) && $retType == $type) {
//                break;
//            }
//        }
//
//        // magic quotes deprecated
//        // @codeCoverageIgnoreStart
//        if (get_magic_quotes_gpc()) {
//            $ret = stripslashes($ret);
//        }
//        // @codeCoverageIgnoreEnd
//
//        switch($returnAs) {
//            case 'int':
//                return (int) $ret;
//            case 'float':
//                return Vars::floatval($ret);
//            case 'bool':
//                return (bool) $ret;
//            case 'html':
//                return iconv('UTF-8', 'UTF-8//IGNORE', (string) $ret);
//            case 'date':
//                return Gst_Date::dateTime2Iso($ret);
//            case 'datetime':
//                return Gst_Date::dateTime2Iso($ret, true);
//            default:
//                break;
//        }
//        return strip_tags(iconv('UTF-8', 'UTF-8//IGNORE', (string) $ret));
//    }

   /**
    * check if email is valid
    * @param string $email email address
    * @return bool
    */
    public static function isEmail($email)
    {
        if ($email == '' || is_array($email)) {
            return false;
        }
        // period (".") may not be used to start or end the
        // local part, nor may two or more consecutive periods appear
        if ($email{0} === '.') {
            return false;
        }
        if (strpos($email, '.@') !== false) {
            return false;
        }
        if (strpos($email, '..') !== false) {
            return false;
        }
        if (strpos($email, ':') !== false) {
            return false;
        }
        if (strpos($email, ',') !== false) {
            return false;
        }
        // _ may not be used to start
        if ($email{0} == '_') {
            return false;
        }

        $regexp = '/^[a-z0-9!#$%&*+-=?^_`{|}~]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*';
        $regexp .= '@([-a-z0-9]+\.)+([a-z]{2,3}';
        // TLD with more than 3 chars
        $regexp .= '|aero|arpa|asia|coop|info|jobs|mobi|museum|name|travel)$/ix';
        return (bool) preg_match($regexp, $email);
    }

    /**
     * returns url with &amp; replaced by single &
     * @param string $url
     * @return string
     */
    public static function fixAmp($url)
    {
        return str_replace('&amp;', '&', $url);
    }

    /**
     * remove problematic html tags and invisible content
     * @param string $text
     * @param bool $removeWhitespaces
     * @return string
     * @see http://nadeausoftware.com/articles/2007/09/php_tip_how_strip_html_tags_web_page#Downloads
     */
    public static function stripHtml($text, $removeWhitespaces = false)
    {
        $text = preg_replace(
            array(
                // Remove invisible content
                '@<head[^>]*?>.*?</head>@siu',
                '@<style[^>]*?>.*?</style>@siu',
                '@<script[^>]*?.*?</script>@siu',
                '@<object[^>]*?.*?</object>@siu',
                '@<embed[^>]*?.*?</embed>@siu',
                '@<applet[^>]*?.*?</applet>@siu',
                '@<noframes[^>]*?.*?</noframes>@siu',
                '@<noscript[^>]*?.*?</noscript>@siu',
                '@<noembed[^>]*?.*?</noembed>@siu',
                // Add line breaks before & after blocks
                '@<((br)|(hr))@iu',
                '@</?((address)|(blockquote)|(center)|(del))@iu',
                '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
                '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
                '@</?((table)|(th)|(td)|(caption))@iu',
                '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
                '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
                '@</?((frameset)|(frame)|(iframe))@iu',
            ),
            array(
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
                "\n\$0", "\n\$0",
            ), $text
        );

        $text = strip_tags($text);
        $text = html_entity_decode($text, ENT_QUOTES);
        if ($removeWhitespaces) {
//            $text = str_replace('&nbsp;', ' ', $text);
//            $text = str_replace('&#160;', ' ', $text);
            $text = str_replace("\n", ' ', $text);
            $text = str_replace("\r", ' ', $text);
            $text = str_replace("\t", ' ', $text);
            while (strpos($text, '  ') !== false) {
                $text = str_replace('  ', ' ', $text); // remove double spaces
            }
        }
        return trim($text);
    }

    /**
     * returns full url with protocol
     * @param string $url
     * @return string
     */
    public static function fixUrl($url)
    {
        if ($url != '' && substr(strtolower($url), 0, 7) != 'mailto:' && strpos($url, '://') === false) {
            $url = 'http://'.$url;
        }
        return $url;
    }

    /**
     * returns text with stripped html tags
     * @param string $text
     * @return string
     */
    public static function stripTags($text)
    {
        $spaces = array('<br />', '<br>');
        $text = str_ireplace($spaces, ' ', $text);
        return strip_tags($text);
    }

    /**
     * In given html replace relative url in src and href attributes with absolute urls starting with $baseUrl
     * @param string $text html
     * @param string $baseUrl
     * @return string
     */
    public static function urlToAbs($text, $baseUrl)
    {
        // href|src)="(?!#) - do not convert links to anchors
        return preg_replace('/(href|src)="(?!#)([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))/', '$1="'.$baseUrl.'$2$3', $text);
    }

    /**
     * returns minified html with removed white chracters
     * @param string $html
     * @return string
     */
    public static function minifyHtml($html)
    {
        $search = array(
            '/\>[^\S ]+/s', // strip whitespaces after tags, except space
            '/[^\S ]+\</s', // strip whitespaces before tags, except space
            '/(\s)+/s'  // shorten multiple whitespace sequences
        );
        $replace = array(
            '>',
            '<',
            '\\1'
        );
        return trim(preg_replace($search, $replace, $html));
    }

}