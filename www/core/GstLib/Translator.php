<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once __DIR__.'/Filesystem.php';

/**
 * Translate texts depending on selected language
 *
 * Requires language files with the same extension as language shortcut.
 * Language files must have structure 'token;value' and comments starting with #.
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package GstLib
 */
class Translator
{
    /** @var array translations in format token=>translation */
    private $_strings;
    /** @var string language */
    private $_lang;

    /**
     * Constructor, fill instance from language file(s) by given param.
     * Expects language files with the same extensions as parameter $lang or 'txt' or 'tmp'.
     *
     * @param string $lang  shortcut of selected language
     * @param array $files array of language files
     */
    public function __construct($lang, array $files)
    {
        $this->_strings = array();

        $this->_lang = 'en';
        $lang = strtolower(trim($lang));
        if (strlen($lang) == 2) {
            $this->_lang = $this->_strings['LANG'] = $lang;
        } else {
            user_error('lang have to be valid ISO 639-1 code', E_USER_WARNING);
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        $files = (is_array($files) ? $files : array($files));

        foreach ($files as $file) {
            $this->_readFile($file);
        }
    }

    /**
     * returns translated text by given token
     * If translation of $token not found, raise warning error and returns untranslated token
     *
     * @param string $token key in language file (first column)
     * @return string translated text
     */
    public function getText($token)
    {
        $token = trim($token);
        if ($token == '') {
            return '';
        }
        if (isset($this->_strings[$token])) {
            return $this->_strings[$token];
        }
        user_error('translation not found: '.$token.' (lang: '.$this->_lang.')', E_USER_NOTICE);
        // @codeCoverageIgnoreStart
        return $token;
        // @codeCoverageIgnoreEnd
    }

    /**
     * replace pseudotags (for example [%LG_SOMETHING%]), with items from language file(s)
     *
     * @param string $string text with items for translations ([%LG_TOKEN%])
     * @return string text with translated pseudotags
     */
    public function translate($string)
    {
        $occurences = array();
        preg_match_all('/\[%LG_(.*)%\]/U', $string, $occurences, PREG_PATTERN_ORDER);
        $occurences = array_unique($occurences[1]);
        foreach ($occurences as $token) {
            $string = str_replace('[%LG_'.$token.'%]', $this->getText($token), $string);
        }
        return $string;
    }

    /**
     * read language strings from given language file
     * @param string $fullPath
     */
    private function _readFile($fullPath)
    {
        if (!is_file($fullPath)) {
            user_error('cannot read file '.$fullPath, E_USER_WARNING);
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }
        $lines = file($fullPath);
        if ($lines === false) {
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }

        foreach ($lines as $line) {
            $line = trim($line);
            // ignore empty lines and comments
            if ($line == '') {
                continue;
            }
            if ($line{0} == '#') {
                continue;
            }
            // parse line, assume only first semicolon as delimiter
            $string = explode(';', $line, 2);
            if (!isset($string[1])) {
                continue;
            }
            $this->_strings[trim($string[0])] = trim($string[1]);
        }
    }

    /**
     * returns translations as array in format token=>translation
     * @return array
     */
    public function getStrings()
    {
        return $this->_strings;
    }

}