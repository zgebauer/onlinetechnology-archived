<?php
/**
 * @package GstLib
 */

namespace GstLib;

/**
 * library of filesystem related functions
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package GstLib
 */
class Filesystem
{
    /**
     * recursive version of function glob
     * @param string $dir start directory without trailing slash
     * @param string $pattern pattern to glob
     * @param int $flags flags sent to glob
     * @return array of all pattern-matched files
     */
    public static function globr($dir, $pattern, $flags = null)
    {
        $files = glob($dir.'/'.$pattern, $flags);
        $files = (is_array($files) ? $files : array());
        $dirs = glob($dir.'/*', GLOB_ONLYDIR);
        $dirs = (is_array($dirs) ? $dirs : array());
        foreach ($dirs as $subDir) {
            $files = array_merge($files, self::globr($subDir, $pattern, $flags));
        }
        return $files ;
    }

    /**
     * recursively create directory (directories) specified by path
     * @param string $path
     * @param int $mode unix rights
     * @return bool false on error
     */
    public static function mkDir($path, $mode = 0777)
    {
        $current = '';
        foreach (explode('/', $path) as $item) {
            $current .= $item.'/';
            if (@is_dir($current)) {
                continue;
            } else {
                if (!@is_readable(dirname($current))) {
                    // @codeCoverageIgnoreStart
                    continue; // parent directory outside open_basedir
                    // @codeCoverageIgnoreEnd
                }
            }
            @umask(000);
            if (!mkdir($current, $mode)) {
                // @codeCoverageIgnoreStart
                return false;
                // @codeCoverageIgnoreEnd
            }
        }
        return true;
    }

    /**
     * returns extension of file
     * @param string $file filename
     * @return string
     */
    public static function getFileExtension($file)
    {
        if (strpos($file, '.') === false) {
            return '';
        }
        if (strtolower(substr($file, -7)) === '.tar.gz') {
            return substr($file, -6);
        }
        $tmp = explode('.', $file);
        return end($tmp);
    }

    /**
     * returns name of file without extension
     * @param string $file filename
     * @return string
     */
    public static function getFilenameWoExtension($file)
    {
        $ext = self::getFileExtension($file);
        if ($ext === '') {
            return $file;
        }
        return basename($file, '.'.$ext);
    }

    /**
     * similar to unix logrotate, copy specified file with timestamp and delete older versions
     *
     * @param string $fullPath file for rotate
     * @param int $history how many older versions to keep
     */
    public static function logrotate($fullPath, $history)
    {
        if (!is_file($fullPath)) {
            return;
        }

        @umask(000);
        copy($fullPath, $fullPath.'.'.date('Y-m-d-H-i-s'));
        file_put_contents($fullPath, '');
        // delete older versions
        $oldLogs = glob($fullPath.'*');
        unset($oldLogs[array_search($fullPath, $oldLogs)]);
        sort($oldLogs);
        $count = count($oldLogs);
        for ($i = 0; $i < $count - $history; $i++) {
            unlink($oldLogs[$i]);
        }
    }

    /**
     * recursively delete directory with content
     * @param string $dir $directory
     * @return bool false on error
     */
    public static function rmDir($dir)
    {
        if (is_dir($dir)) {
            $dirHandler = opendir($dir);
            if ($dirHandler !== false) {
                while (($item = readdir($dirHandler)) !== false) {
                    if ($item == '.' || $item == '..') {
                        continue;
                    }
                    $fullpath = $dir.'/'.$item;
                    if (is_file($fullpath)) {
                        unlink($fullpath);
                    }
                    if (is_dir($fullpath)) {
                        self::rmDir($fullpath);
                    }
                }
                closedir($dirHandler);
            }
            rmdir($dir);
        }
        return true;
    }

    /**
     * recursively set file mode on directory
     * @param dir $dir
     * @param int $modeDir mode for directories, null = remain unchanged
     * @param int $modeFile mode for files, null = remain unchanged
     */
    public static function chmodr($dir, $modeDir = 0777, $modeFile = 0666)
    {
        foreach (self::globr($dir, '*') as $item) {
            if (is_dir($item) && !is_null($modeDir)) {
                @chmod($dir, $modeDir);
                continue;
            }
            if (is_file($item) && !is_null($modeFile)) {
                @chmod($dir, $modeFile);
                continue;
            }
        }
    }

    /**
     * gzip file
     * @param string $source source file
     * @param string $target target file
     */
    public static function gZipFile($source, $target)
    {
        $fileHandler = fopen($source, 'r');
        $data = fread($fileHandler, filesize($source));
        fclose($fileHandler);

        $zipHandler = gzopen($target, 'wb9');
        gzwrite($zipHandler, $data);
        gzclose($zipHandler);
    }

}
