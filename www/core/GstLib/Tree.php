<?php
/**
 * @package GstLib
 */

namespace GstLib;

/**
 * common tree structure
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@centrum.cz>
 * @package   GstLib
 */
class Tree
{
    /** @var array array of items in tree
     */
    private $_cache;

    /**
     * constructor, create instance and fill it with data by given identificator
     * @param array $items each item must be an object with properties id, parentId
     */
    public function __construct(array $items)
    {
        $this->_cache = $items;
    }

    /**
     * returns children items of node with given id
     * @param int $itemId
     * @return array
     */
    public function getChildren($itemId)
    {
        $ret = array();
        foreach ($this->_cache as $item) {
            if ($item->parentId() == $itemId) {
                $ret[] = $item;
            }
        }
        return $ret;
    }

    /**
     * returns parent items of item with given id
     * @param int $itemId identificator of item
     * @return array
     */
    public function getParents($itemId)
    {
        $ret = array();
        $tmp = $this->_getParent($itemId);
        while (!is_null($tmp)) {
            if ($tmp->parentId() == 0) {
                break;
            }
            $tmp = $this->_getParent($tmp->parentId());
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns parent object
     * @param int $itemId identificator of item
     * @return object parent item or null
     */
    private function _getParent($itemId)
    {
        foreach ($this->_cache as $item) {
            if ($item->instanceId() == $itemId) {
                return $item;
            }
        }
        return null;
    }

}