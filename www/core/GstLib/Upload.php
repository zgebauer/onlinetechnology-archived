<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once dirname(__FILE__).'/UserFile.php';

use GstLib\UserFile;

/**
 * handler for uploading files
 *
 * sample usage:
 * <code>
 * $upload = new Gst_Upload();
 * // limits for upload
 * $upload->maxUploadSize   = 10000;
 * $upload->_allowExtensions = array('jpg');
 * $upload->_allowTypes      = array('image/jpeg')
 * $upload->_allowNames      = array('foto.jpg', 'nextfoto.jpg');
 * // or for example
 * $upload->_maxImgWidth     = 200;
 * $upload->_maxImgHeight    = 300;
 * if (!$opload->save('/upload/folder/', 'formfield', true)) {
 *     echo $upload->_err);
 * }
 * // or
 * if (!$upload->saveAs('foto.jpg', '/upload/folder/', 'formfield', true)) {
 *     echo $upload->_err);
 * }
 * unset ($upload);
 * </code>
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   GstLib
 * @property int   $errCode
 * @property int   $maxImgHeight     max height of uploaded image (0=allow any)
 * @property int   $maxImgWidth      max width of uploaded image (0=allow any)
 * @property bool  $allowCmyk        true=allow upload CMYK jpg
 */
class Upload
{
    /**
     * @var error - already exists file with the same name as uploaded file
     * @see errCode
     */
    const ERR_FILE_ALREADY_EXISTS = 1;
    /**
     * @var error - file was not uploaded
     * @see errCode
     */
    const ERR_FILE_NOT_UPLOADED = 2;
    /**
     * @var error - file was too big
     * @see errCode
     */
    const ERR_FILE_TOO_BIG = 3;
    /**
     * @var error - unallowed extension of uploaded file
     * @see errCode
     * @see allowExtensions
     */
    const ERR_UNALLOWED_EXTENSION = 4;
    /**
     * @var error - unallowed MIME type of uploaded file
     * @see errCode
     * @see _allowTypes
     */
    const ERR_UNALLOWED_MIME = 5;
    /**
     * @var error - unallowed name of uploaded file
     * @see errCode
     * @see _allowNames
     */
    const ERR_UNALLOWED_NAME = 6;
    /**
     * @var error - uploaded image was too wide
     * @see errCode
     * @see maxImgWidth
     */
    const ERR_IMG_TOO_WIDE = 7;
    /**
     * @var error - uploaded image was too high
     * @see errCode
     * @see maxImgHeight
     */
    const ERR_IMG_TOO_HIGH = 8;
    /**
     * @var error - incorrect height of uploaded image
     * @see errCode
     * @see imgHeight
     */
    const ERR_IMG_INCORRECT_HEIGHT = 9;
    /**
     * @var error - incorrect width of uploaded image
     * @see errCode
     * @see imgWidth
     */
    const ERR_IMG_INCORRECT_WIDTH = 10;
    /**
     * @var error - CMYK JPG not allowed
     * @see errCode
     * @see allowCmyk
     */
    const ERR_IMG_CMYK = 11;

    /** @var string last error message */
    protected $_err;
    /** @var array allowed extensions of uploaded file */
    protected $_allowExtensions;
    /** @var array allowed extensions of uploaded file */
    protected $_allowTypes;
    /** @var int max filesize for upload */
    protected $_maxUploadSize;
    /** @var int required image width */
    protected $_imgHeight;
    /** @var int required image height */
    protected $_imgWidth;

    /** constructor */
    public function __construct()
    {
        $this->_err = '';
        $this->_errCode = 0;
        $this->_allowExtensions = array();
        $this->_allowNames = array();
        $this->_allowTypes = array();
        $this->_maxUploadSize = UserFile::bytes(ini_get('upload_max_filesize'));
        $this->_imgHeight = 0;
        $this->_imgWidth = 0;
        $this->_maxImgHeight = 0;
        $this->_maxImgWidth = 0;
        $this->_allowCmyk = false;
    }

    /**
     * sets value of a property
     * @param  string $name  name of property
     * @param  mixed  $value value of property
     */
//    public function __set($name,$value)
//    {
//        switch ($name) {
//            case 'allowExtensions':
//            case '_allowNames':
//            case '_allowTypes':
//                if (!is_array($value)) {
//                    user_error(
//                        'property '.$name.' must be array', E_USER_WARNING
//                    );
//                    return;
//                }
//                break;
//            case 'maxUploadSize':
//            case 'imgHeight':
//            case 'imgWidth':
//            case 'maxImgHeight':
//            case 'maxImgWidth':
//                $value = (int) $value;
//                break;
//            default:
//                break;
//        }
//        try {
//            parent::__set($name, $value);
//        } catch (Exception $e) {
//            user_error($e->getMessage(), E_USER_WARNING);
//        }
//    }

    /**
     * returns and sets file extensions allowed for upload
     * @param array $value
     * @return array
     * @throws \InvalidArgumentException
     */
    public function allowExtensions($value = null)
    {
        if (!is_null($value)) {
            if (!is_array($value)) {
                throw new \InvalidArgumentException('incorrect parameter '.print_r($value, true));
            }
            $this->_allowExtensions = $value;
        }
        return $this->_allowExtensions;
    }

    /**
     * returns and sets MIME types allowed for upload
     * @param array $value
     * @return array
     * @throws \InvalidArgumentException
     */
    public function allowMimeTypes($value = null)
    {
        if (!is_null($value)) {
            if (!is_array($value)) {
                throw new \InvalidArgumentException('incorrect parameter '.print_r($value, true));
            }
            $this->_allowTypes = $value;
        }
        return $this->_allowTypes;
    }

    /**
     * returns and sets MIME types allowed for upload
     * @param int $value
     * @return int
     * @throws \InvalidArgumentException
     */
    public function maxUploadFileSize($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value <= 0) {
                throw new \InvalidArgumentException('incorrect parameter '.$value);
            }
            $this->_maxUploadSize = $value;
        }
        return $this->_maxUploadSize;
    }

    /**
     * returns and sets image width
     * @param int $value
     * @return int
     */
    public function imgWidth($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_imgWidth = $value;
            }
        }
        return $this->_imgWidth;
    }

    /**
     * returns and sets image width
     * @param int $value
     * @return int
     */
    public function imgHeight($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_imgHeight = $value;
            }
        }
        return $this->_imgHeight;
    }

    /**
     * check if exist uploaded file
     * @param string $field name of form field (input file) with uploaded file
     * @return bool
     */
    public static function isUploaded($field)
    {
        if (!isset($_FILES[$field]) || $_FILES[$field]['name'] == '' || $_FILES[$field]['name'] == 'none') {
            return false;
        }
        return true;
    }

    /**
     * save uploaded file to given directory
     * @param string $dir directory to save
     * @param string $field name of form field with uploaded file
     * @param bool $overwrite true = overwrite file if exists
     * @param int $mode unix file permissions
     * @return bool false on error
     */
    public function save($dir, $field, $overwrite, $mode = 0664)
    {
        return $this->saveAs($_FILES[$field]['name'], $dir, $field, $overwrite, $mode);
    }

    /**
     * save uploaded file to given directory with given filename
     * @param string $fileName save as filename
     * @param string $dir directory to save
     * @param string $field name of form field with uploaded file
     * @param bool $overwrite true = overwrite file if exists
     * @param int $mode unix file permissions
     * @return bool false on error
     * @throws \GstLib\UploadException
     */
    public function saveAs($fileName, $dir, $field, $overwrite, $mode = 0664)
    {
        try {
            if (!$this->_isValid($field)) {
                return false;
            }
        } catch (UploadException $e) {
            throw $e;
        }
        $tempName = $_FILES[$field]['tmp_name'];
        $fullPath = $dir.$fileName;

        if (is_file($fullPath) && !$overwrite) {
            throw new UploadException('File already exists', self::ERR_FILE_ALREADY_EXISTS);
        }
        if (is_file($fullPath) && $overwrite && !@unlink($fullPath)) {
            user_error('Cannot overwrite file '.$fullPath, E_USER_WARNING);
            return false;
        }
        if (!isset($_SERVER['HTTP_HOST'])) {
            // script was run from command line (phpunit?)
            if (!rename($tempName, $fullPath)) {
                user_error('cannot write file '.$fullPath, E_USER_WARNING);
                return FALSE;
            }
        } else {
            if (!move_uploaded_file($tempName, $fullPath)) {
                user_error('cannot write file '.$fullPath, E_USER_WARNING);
                return FALSE;
            }
        }
        @umask(0000);
        if (!@chmod($fullPath, $mode)) {
            user_error('cannot set permissions on '.$fullPath, E_USER_NOTICE);
        }
        return true;
    }

    /**
     * returns lowercased extension of uploaded file or empty string if file not exists
     * @param string $field name of form field with uploaded file
     * @return string
     */
    public static function getExtension($field)
    {
        if (!isset($_FILES[$field]['name'])) {
            return '';
        }
        $tmp = explode('.', $_FILES[$field]['name']);
        return strtolower(end($tmp));
    }

    /**
     * returns mime type of uploaded file or empty string if file not exists
     * @param string $field name of form field with uploaded file
     * @return string
     */
    public static function getMime($field)
    {
        if (!isset($_FILES[$field]['type'])) {
            return '';
        }
        return $_FILES[$field]['type'];
    }

    /**
     * returns file name of uploaded file or empty string if file not exists
     * @param string $field name of form field with uploaded file
     * @return string
     */
    public static function getFileName($field)
    {
        if (!isset($_FILES[$field]['name'])) {
            return '';
        }
        return $_FILES[$field]['name'];
    }

    /**
     * check uploaded file
     * @access private
     * @param  string  $field name of form field with uploaded file
     * @return bool    false if file is not valid and correct
     */
    private function _isValid($field)
    {
        if (!isset($_FILES[$field])) {
            $this->_err = 'File was not uploaded';
            $this->_errCode = self::ERR_FILE_NOT_UPLOADED;
            user_error('Form field '.$field.' not found', E_USER_WARNING);
            return false;
        }
        // check error constants
        if (isset($_FILES[$field]['error'])) {
            switch($_FILES[$field]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $this->_err = 'Uploaded file was too big';
                $this->_errCode = self::ERR_FILE_TOO_BIG;
                return false;
            case UPLOAD_ERR_PARTIAL:
                $this->_err = 'File was not completely uploaded';
                $this->_errCode = self::ERR_FILE_NOT_UPLOADED;
                return FALSE;
            case UPLOAD_ERR_NO_FILE:
                $this->_err = 'File was not uploaded, maybe was too big';
                $this->_errCode = self::ERR_FILE_NOT_UPLOADED;
                return false;
            default:
                user_error('upload error '.$_FILES[$field]['error'], E_USER_WARNING);
                return false;
            }
        }

        if (!isset($_SERVER['HTTP_HOST'])) {
            // script was run from command line (phpunit?)
            if (!is_file($_FILES[$field]['tmp_name'])) {
                throw new UploadException('File was not uploaded', self::ERR_FILE_NOT_UPLOADED);
            }
        } else {
            if (!is_uploaded_file($_FILES[$field]['tmp_name'])) {
                throw new UploadException('File was not uploaded', self::ERR_FILE_NOT_UPLOADED);
            }
        }
        if ($_FILES[$field]['name'] == '') {
            return false;
        }
        if ($_FILES[$field]['size'] == 0 || $_FILES[$field]['name'] == 'none') {
            $this->_err = 'File was not uploaded, maybe was too big';
            $this->_errCode = self::ERR_FILE_TOO_BIG;
            return false;
        }
        if ($_FILES[$field]['size'] > $this->_maxUploadSize) {
            $this->_err = 'Uploaded file was too big';
            $this->_errCode = self::ERR_FILE_TOO_BIG;
            return false;
        }
        if (isset($this->_allowExtensions[0])) {
            if (!in_array(self::getExtension($field), $this->_allowExtensions)) {
                throw new UploadException('Unallowed extension of uploaded file', self::ERR_UNALLOWED_EXTENSION);
            }
        }
        if (isset($this->_allowTypes[0])) {
            if (!in_array($_FILES[$field]['type'], $this->_allowTypes)) {
                $this->_err = 'Unallowed MIME type of uploaded file';
                $this->_errCode = self::ERR_UNALLOWED_MIME;
                user_error('unallowed MIME '.$_FILES[$field]['type'].' in '.$field, E_USER_NOTICE);
                return false;
            }
        }
        if (isset($this->_allowNames[0])) {
            if (!in_array($_FILES[$field]['name'], $this->_allowedNames)) {
                $this->_err = 'Unallowed name uploaded file';
                $this->_errCode = self::ERR_UNALLOWED_NAME;
                return false;
            }
        }
        // check image
        if (substr($_FILES[$field]['type'], 0, 5) == 'image' ||
                $_FILES[$field]['type'] == 'application/x-shockwave-flash') {
            $imgInfo = @getimagesize($_FILES[$field]['tmp_name']);
            if (is_array($imgInfo)) {
                $width = $imgInfo[0];
                $height = $imgInfo[1];
                if ($this->_maxImgWidth > 0 && $width > $this->_maxImgWidth) {
                    $this->_err = 'Uploaded image was wider than '.$this->_maxImgWidth.'px';
                    $this->_errCode = self::ERR_IMG_TOO_WIDE;
                    return false;
                }
                if ($this->_maxImgHeight > 0 && $height > $this->_maxImgHeight) {
                    $this->_err = 'Uploaded image was higher than '.$this->_maxImgHeight.'px';
                    $this->_errCode = self::ERR_IMG_TOO_HIGH;
                    return false;
                }
                if ($this->_imgWidth > 0 && $width !== $this->_imgWidth) {
                    throw new UploadException(
                        'Required image width '.$this->_imgWidth.'px', self::ERR_IMG_INCORRECT_WIDTH
                    );
                }
                if ($this->_imgHeight > 0 && $height !== $this->_imgHeight) {
                    throw new UploadException(
                        'Required image height '.$this->_imgHeight.'px', self::ERR_IMG_INCORRECT_HEIGHT
                    );
                }
                if (!$this->_allowCmyk
                        && in_array($_FILES[$field]['type'], array('image/jpg', 'image/jpeg', 'image/pjpeg'))
                        && $imgInfo['channels'] == 4) {
                    $this->_err = 'CMYK not allowed';
                    $this->_errCode = self::ERR_IMG_CMYK;
                    return false;
                }
            }
        }
        return true;
    }

}


/**
 * exception throwed from GstLib\Upload
 * @package GstLib
 */
class UploadException extends \Exception
{
}
