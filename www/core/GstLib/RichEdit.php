<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once 'Html.php';

/**
 * wrapper for creating rich editors
 *
 * create code for rich editor
 * supports TinyMCE (http://tinymce.moxiecode.com/),
 * FCKeditor (http://www.fckeditor.net/)
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package GstLib
 */
class RichEdit
{
    /**
     * create html code of rich editor
     *
     * possible configuration parameters:
     * textarea_cols
     * textarea_rows
     * textarea_css_class
     *
     * tinymce_url - url of tiny_mce.js (tiny_mce_gzip.js). Only for first
     *  editor in page
     * tinymce_gzip - (bool) use gzip. In this case should be set parameters
     *  tinymce_gzip_themes, tinymce_gzip_languages, tinymce_gzip_disk_cache
     * tinymce_plugins (and other tinymce_* parameters) - other tinymce
     *  parameters described on
     *  http://wiki.moxiecode.com/index.php/TinyMCE:Configuration
     *
     * fckeditor_file - absolute path to file fckeditor.php
     * fckeditor_BasePath - path form root to fckeditor dir
     * fckeditor_Width - width of editor in px (default) or %
     * fckeditor_Height - height of editor in px (default) or %
     * fckeditor_CustomConfigurationsPath - url of additional configuration file
     *  of FCKeditor. This file is preferable way to customize FCKeditor, but
     *  for special cases some paramaters should be set there (see
     *  http://docs.fckeditor.net/FCKeditor_2.x/Developers_Guide/Configuration/
     *  Configuration_Options)
     * fckeditor_EditorAreaCSS - url of css file for editor area
     *
     * ibrowser_config_file - absolute path to additional config file for
     *  ibrowser (or imanager) plugin from http://j-cons.com/downloads/
     *
     * @param  string $editor     [tinymce|fckeditor|textarea (default)]
     * @param  string $fieldName  name of form field
     * @param  string $fieldValue value of form field
     * @param  array  $config     configuration
     * @param  string $fieldId    id of form field
     * @return string html code of editor
     */
    public static function render($editor, $fieldName, $fieldValue,
        array $config, $fieldId = null)
    {
        if ($editor == 'tinymce') {
            return self::_renderTinyMCE(
                $fieldName, $fieldValue, $config, $fieldId
            );
        }
        if ($editor == 'tinymce-jquery') {
            return self::_renderTinyMCEJquery(
                $fieldName, $fieldValue, $config, $fieldId
            );
        }
        if ($editor == 'fckeditor') {
            return self::_renderFCKEditor(
                $fieldName, $fieldValue, $config, $fieldId
            );
        }
        return self::_renderTextarea(
            $fieldName, $fieldValue, $config, $fieldId
        );
    }

    /**
     * render common textarea tag
     * @access private
     * @param  string $fieldName  name of form field
     * @param  string $fieldValue value of form field
     * @param  array  $config     configuration
     * @param  string $fieldId    id of form field
     * @return string html textarea tag
     */
    private static function _renderTextarea($fieldName, $fieldValue,
        array $config, $fieldId = null)
    {
        if (!is_null($fieldId)) {
            $fieldId = ' id="'.$fieldId.'"';
        }
        $cols = 40;
        if (isset($config['textarea_cols'])) {
            $cols = intval($config['textarea_cols']);
            $cols = $cols <= 0 ? $cols = 40 : $cols;
        }
        $rows = 5;
        if (isset($config['textarea_rows'])) {
            $rows = intval($config['textarea_rows']);
            $rows = $rows <= 0 ? $rows = 5 : $rows;
        }
        $cssClass = '';
        if (!empty($config['textarea_css_class'])) {
            $cssClass = ' class="'.$config['textarea_css_class'].'"';
        }
        return '<textarea'.$fieldId.' name="'.$fieldName.'" cols="'.$cols.
            '" rows="'.$rows.'"'.$cssClass.'>'.Html::escape($fieldValue).'</textarea>';
    }

    /**
     * render code for TinyMCE editor
     * @access private
     * @param  string $fieldName  name of form field
     * @param  string $fieldValue value of form field
     * @param  array  $config     configuration
     * @param  string $fieldId    id of form field
     * @return string html
     */
    private static function _renderTinyMCE($fieldName, $fieldValue,
        array $config, $fieldId = null)
    {
        if (is_null($fieldId)) {
            $fieldId = uniqid('richedit');
        }
        $ret = self::_renderTextarea(
            $fieldName, $fieldValue, $config, $fieldId
        );

        //default paramaters
        if (!isset($config['tinymce_dialog_type'])) {
            $config['tinymce_dialog_type'] = 'modal';
        }
        if (!isset($config['tinymce_entity_encoding'])) {
            $config['tinymce_entity_encoding'] = 'raw';
        }
        if (!isset($config['tinymce_relative_urls'])) {
            $config['tinymce_relative_urls'] = 'false';
        }
        if (!isset($config['tinymce_theme_advanced_toolbar_location'])) {
            $config['tinymce_theme_advanced_toolbar_location'] = 'top';
        }
        if (!isset($config['tinymce_theme_advanced_toolbar_align'])) {
            $config['tinymce_theme_advanced_toolbar_align'] = 'left';
        }
        if (!isset($config['tinymce_remove_script_host'])) {
            $config['tinymce_remove_script_host'] = 'false';
        }

        $settings = '';
        $settingsGzip = '';
        foreach ($config as $key=>$value) {
            switch ($key) {
                case 'tinymce_plugins':
                    $settings .= 'plugins:"'.$value.'",';
                    $settingsGzip .= 'plugins:"'.$value.'",';
                    break;
                case 'tinymce_theme':
                    $settings .= 'theme:"'.$value.'",';
                    $settingsGzip .= 'themes:"'.$value.'",';
                    break;
                case 'tinymce_language':
                    $settings .= 'language:"'.$value.'",';
                    $settingsGzip .= 'languages:"'.$value.'",';
                    break;
                case 'tinymce_gzip_disk_cache':
                    $settingsGzip .= 'disk_cache:'.$value.',';
                    break;
                case 'tinymce_gzip':
                case 'tinymce_url':
                    break;
                default:
                    if (substr($key, 0, 9) == 'ibrowser_') {
                        @session_start();
                        $_SESSION[$key] = $value;
                    }
                    if (substr($key, 0, 8) == 'tinymce_') {
                        if (!in_array($value, array('true', 'false'))) {
                            $value = '"'.$value.'"';
                        }
                        // remove 'tinymce_
                        $settings .= substr($key, 8).':'.$value.',';
                    }
                    break;
            }
        }
        $settings = substr($settings, 0, -1);
        $settingsGzip = substr($settingsGzip, 0, -1);
        if (isset($config['tinymce_gzip'])) {
            $config['tinymce_url'] = str_replace(
                'tiny_mce.js', 'tiny_mce_gzip.js', $config['tinymce_url']
            );
        }
        if (isset($config['tinymce_url'])) {
            $ret .= '<script type="text/javascript" src="'.
                $config['tinymce_url'].'"></script>'."\n";
        }

        if (isset($config['tinymce_gzip'])) {
            $ret .= "<script type=\"text/javascript\">\n";
            $ret .= 'tinyMCE_GZ.init({'.$settingsGzip."});\n";
            $ret .= '</script>'."\n";
        }
        $ret .= "<script type=\"text/javascript\">\n";
        $ret .= 'tinyMCE.init({mode:"exact", elements:"'.$fieldId.'",'.
            $settings.'})';
        $ret .= '</script>';
        return $ret;
    }

    /**
     * render code for FCKeditor editor
     * @access private
     * @param  string $fieldName  name of form field
     * @param  string $fieldValue value of form field
     * @param  array  $config     configuration
     * @param  string $fieldId    id of form field
     * @return string html
     */
    private static function _renderFCKEditor($fieldName, $fieldValue,
        array $config, $fieldId = null)
    {
        if (!isset($config['fckeditor_file'])) {
            user_error(
                'missing configuration parameter fckeditor_file', E_USER_WARNING
            );
            return self::_renderTextarea(
                $fieldName, $fieldValue, $config, $fieldId
            );
        }
        if (!is_file($config['fckeditor_file'])) {
            user_error(
                'cannot find FCKeditor: '.$config['fckeditor_file'],
                E_USER_WARNING
            );
            return self::_renderTextarea(
                $fieldName, $fieldValue, $config, $fieldId
            );
        }
        require_once $config['fckeditor_file'];
        $editor = new FCKeditor($fieldName);
        // @codingStandardsIgnoreStart
        $editor->Value = $fieldValue;
        // @codingStandardsIgnoreEnd

        foreach (arry_keys($config) as $key) {
            switch ($key) {
                case 'fckeditor_BasePath':
                    // @codingStandardsIgnoreStart
                    $editor->BasePath = $config[$key];
                    // @codingStandardsIgnoreEnd
                    break;
                case 'fckeditor_Width':
                    // @codingStandardsIgnoreStart
                    $editor->Width = $config['fckeditor_Width'];
                    // @codingStandardsIgnoreEnd
                    break;
                case 'fckeditor_Height':
                    // @codingStandardsIgnoreStart
                    $editor->Height = $config['fckeditor_Height'];
                    // @codingStandardsIgnoreEnd
                    break;
                case 'fckeditor_file':
                    break;
                default:
                    if (substr($key, 0, 10) == 'fckeditor_') {
                        // @codingStandardsIgnoreStart
                        $editor->Config[(substr($key, 10))] = $config[$key];
                        // @codingStandardsIgnoreEnd
                    }
                    break;
            }
        }
        return $editor->CreateHTML();
    }

    /**
     * render code for TinyMCE editor
     * @access private
     * @param  string $fieldName  name of form field
     * @param  string $fieldValue value of form field
     * @param  array  $config     configuration
     * @param  string $fieldId    id of form field
     * @return string html
     */
    private static function _renderTinyMCEJquery($fieldName, $fieldValue,
        array $config, $fieldId = null)
    {
        if (is_null($fieldId)) {
            $fieldId = uniqid('richedit');
        }
        $ret = self::_renderTextarea(
            $fieldName, $fieldValue, $config, $fieldId
        );

        //default paramaters
        if (!isset($config['tinymce_dialog_type'])) {
            $config['tinymce_dialog_type'] = 'modal';
        }
        if (!isset($config['tinymce_entity_encoding'])) {
            $config['tinymce_entity_encoding'] = 'raw';
        }
        if (!isset($config['tinymce_relative_urls'])) {
            $config['tinymce_relative_urls'] = 'false';
        }
        if (!isset($config['tinymce_theme_advanced_toolbar_location'])) {
            $config['tinymce_theme_advanced_toolbar_location'] = 'top';
        }
        if (!isset($config['tinymce_theme_advanced_toolbar_align'])) {
            $config['tinymce_theme_advanced_toolbar_align'] = 'left';
        }
        if (!isset($config['tinymce_remove_script_host'])) {
            $config['tinymce_remove_script_host'] = 'false';
        }

        if (!isset($config['tinymce_no_include_script'])) {
            $ret .= '<script type="text/javascript" src="'.
                $config['tinymce_url'].'jquery.tinymce.js"></script>';
        }
        if (isset($config['tinymce_tinybrowser_js'])) {
            $ret .= '<script type="text/javascript" src="'.
                $config['tinymce_tinybrowser_js'].'"></script>';
        }
        $ret .= '<script type="text/javascript">'."\n";
        $ret .= "$(function() {\n";
        $ret .= "$('textarea#".$fieldId."').tinymce({\n";
        // settings
        $settings = '';
        if (isset($config['tinymce_gzip']) && $config['tinymce_gzip']) {
            $settings .=
                "script_url:'".$config['tinymce_url']."tiny_mce_gzip.php',\n";
        } else {
            $settings .=
                "script_url:'".$config['tinymce_url']."tiny_mce.js',\n";
        }

        foreach ($config as $key=>$value) {
            switch ($key) {
//                case 'tinymce_plugins':
//                 $ret .= "plugins:'".$config['tinymce_url']."tiny_mce.js'\n";
//                    $settings .= 'plugins:"'.$value.'",';
//                    $settingsGzip .= 'plugins:"'.$value.'",';
//                    break;
//                case 'tinymce_theme':
//                    $settings .= 'theme:"'.$value.'",';
//                    $settingsGzip .= 'themes:"'.$value.'",';
//                    break;
//                case 'tinymce_language':
//                    $settings .= 'language:"'.$value.'",';
//                    $settingsGzip .= 'languages:"'.$value.'",';
//                    break;
//                case 'tinymce_gzip_disk_cache':
//                    $settingsGzip .= 'disk_cache:'.$value.',';
//                    break;
                case 'tinymce_gzip':
                case 'tinymce_url':
                case 'tinymce_no_include_script':
                    break;
                default:
                    if (substr($key, 0, 8) == 'tinymce_') {
//                        if (!in_array($value, array('true', 'false'))) {
//                            $value = '"'.$value.'"';
//                        }
                        // remove 'tinymce_
                        $settings .= substr($key, 8).':"'.$value.'",';
                    }
                    break;
            }
        }
        $ret .= substr($settings, 0, -1);
        $ret .= "});\n";
        $ret .= "});\n";
        $ret .= "</script>\n";
        return $ret;
    }
}
