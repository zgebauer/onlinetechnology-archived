<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once __DIR__.'/Filesystem.php';
require_once __DIR__.'/UserFile.php';

/**
 * resize images
 * require GD extension
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   GstLib
 */
class Resizer
{
    /**
     * create file with thumbnail image from specifired file. Resize only if source image is larger than thumbnail size.
     * Thumbnail image can be "sharpened" by given instance ImageSharpen
     *
     * @param string $source absolute path of source file
     * @param string $target absolute path of target file
     * @param int $width max width of thumbnail
     * @param int $height max height of thumbnail
     * @param int $quality quality of thumbnail(%)
     * @param ImageSharpen $sharpen
     * @return bool false on error
     */
    public static function createThumbnailMax($source, $target, $width, $height, $quality = 85,
            ImageSharpen $sharpen = null)
    {
        $size = self::_getThumbnailSize($source, $width, $height);

        if ($size === false) {
            return false;
        }
        if ($size['resize']) {
            return self::createThumbnail($source, $target, $size['width'], $size['height'], $quality, $sharpen);
        } else {
            if (is_a($sharpen, 'GstLib\ImageSharpen')) {
                return self::sharpen($source, $target, $sharpen, array('quality'=>$quality));
            }
            if ($source === $target) {
                return true;
            }
            return copy($source, $target);
        }
    }

    /**
     * create file with thumbnail image from specified file.
     * Thumbnail image can be "sharpened" by given instance ImageSharpen
     *
     * @param string $source absolute path of source file
     * @param string $target absolute path of target file
     * @param int $width width of thumbnail
     * @param int $height height of thumbnail
     * @param int $quality quality of thumbnail(%)
     * @param ImageSharpen $sharpen
     * @return bool false on error
     */
    public static function createThumbnail($source, $target, $width, $height, $quality = 85, $sharpen = null)
    {
        if (!self::_checkGd()) {
            return false;
        }
        if ($width <= 0 || $height <= 0) {
            user_error('invalid size:'.$width.'x'.$height, E_USER_WARNING);
            return false;
        }
        if (!self::_checkOriginalFile($source)) {
            return false;
        }

        $needMemory = self::_mallocImageFile($source) + self::_mallocImage($width, $height);
        if (!self::_checkMemory($needMemory)) {
            return false;
        }

        $imgOrig = self::_openImage($source);
        if ($imgOrig === false) {
            return false;
        }

        $origTop = $origLeft = 0;
        $origWidth = imagesx($imgOrig);
        $origHeight = imagesy($imgOrig);
        $thumbTop = $thumbLeft = 0;

        if (function_exists('imagecreatetruecolor')) { // GD v2
            $imgThumb = imagecreatetruecolor($width, $height);
        } else {
            $imgThumb = imagecreate($width, $height);
        }
        if (!$imgThumb) {
            return false;
        }
        $ext = strtolower(Filesystem::getFileExtension($source));
        if ($ext == 'gif') {
            $transparentIndex = imagecolortransparent($imgOrig);
            if ($transparentIndex >= 0) {
                // get original image's transparent color's RGB values
                @$transparentColor = imagecolorsforindex($imgOrig, $transparentIndex);
                // allocate the same color in the new image
                $transparentIndex = imagecolorallocate(
                    $imgThumb, $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']
                );
                // fill the background of the new image with allocated color
                imagefill($imgThumb, 0, 0, $transparentIndex);
                // set the background color to transparent
                imagecolortransparent($imgThumb, $transparentIndex);
            }
        }
        if ($ext == 'png') {
            // temporarily turn off transparency blending
            imagealphablending($imgThumb, false);
            imagesavealpha($imgThumb, true);
            // create a new transparent color for image
            $transparent = imagecolorallocatealpha($imgThumb, 0, 0, 0, 127);
            // fill the background of the new image with allocated color
            imagefilledrectangle($imgThumb, 0, 0, $width, $height, $transparent);
            // restore transparency blending
            imagesavealpha($imgThumb, true);
        }

        if (function_exists('imagecopyresampled')) { // GD v2
            imagecopyresampled(
                $imgThumb, $imgOrig, $origTop, $origLeft, $thumbTop, $thumbLeft,
                $width, $height, $origWidth, $origHeight
            );
        } else {
            imagecopyresized(
                $imgThumb, $imgOrig, $origTop, $origLeft, $thumbTop, $thumbLeft,
                $width, $height, $origWidth, $origHeight
            );
        }

        // sharpen image
        if (is_a($sharpen, '\GstLib\ImageSharpen')) {
            $sharpen->sharpen($imgThumb);
        }

        if (!self::_saveImage($imgThumb, $target, $quality)) {
            return false;
        }

        imagedestroy($imgOrig);
        imagedestroy($imgThumb);
        return true;
    }

    /**
     * create thumbnail with specified max height
     * Thumbnail image can be "sharpened" by given instance ImageSharpen
     *
     * @param string $source absolute path of source file
     * @param string $target absolute path of target file
     * @param int $height height of thumbnail
     * @param int $quality quality of thumbnail(%)
     * @param ImageSharpen $sharpen
     * @return bool false on error
     */
    public static function createThumbnailMaxHeight($source, $target, $height, $quality = 85, $sharpen = null)
    {
        $size = self::_getThumbnailWidth($source, $height);
        if ($size === false) {
            return false;
        }
        if ($size['resize']) {
            return self::createThumbnail($source, $target, $size['width'], $size['height'], $quality, $sharpen);
        } else {
            if (is_a($sharpen, 'GstLib\ImageSharpen')) {
                return self::sharpen($source, $target, $sharpen, array('quality'=>$quality));
            }
            return copy($source, $target);
        }
    }

    /**
     * create file with thumbnail image from specified file
     * Thumbnail image can be "sharpened" by given instance ImageSharpen
     *
     * @param string $source absolute path of source file
     * @param string $target absolute path of target file
     * @param int $width width of thumbnail
     * @param int $height height of thumbnail
     * @param int $quality quality of thumbnail(%)
     * @param ImageSharpen $sharpen
     * @return bool false on error
     */
    public static function createCenterThumbnail($source, $target, $width, $height, $quality = 85,
        ImageSharpen $sharpen = null)
    {
        if (!self::_checkGd()) {
            return false;
        }

        if ($width <= 0 || $height <= 0) {
            user_error('invalid size:'.$width.'x'.$height, E_USER_WARNING);
            return false;
        }
        if (!self::_checkOriginalFile($source)) {
            return false;
        }

        $needMemory = self::_mallocImageFile($source) + self::_mallocImage($width, $height);
        if (!self::_checkMemory($needMemory)) {
            return false;
        }

        $imgOrig = self::_openImage($source);
        if ($imgOrig === false) {
            return false;
        }

        // size of original image
        $origWidth = imagesx($imgOrig);
        $origHeight = imagesy($imgOrig);

        // calc thumbnail size
        if ($width > $height) {
            // landscape
            $calcWidth = $origWidth;
            $calcHeight = ceil($height*($origWidth/$width));
            $offsetTop = ceil(($origHeight-$calcHeight)/2);
            $offsetLeft = 0;
            // thumbnail cannot be higher than original
            if ($calcHeight > $origHeight) {
                $calcHeight = $origHeight;
                $calcWidth = ceil($width*($origHeight/$height));
                $offsetLeft = ceil(($origWidth-$calcWidth)/2);
                $offsetTop = 0;
            }
        } else {
            // portrait
            $calcHeight = $origHeight;
            $calcWidth = ceil($width*($origHeight/$height));
            $offsetTop = 0;
            $offsetLeft = ceil(($origWidth-$calcWidth)/2);
            // thumbnail cannot be wider than original
            if ($calcWidth > $origWidth) {
                $calcWidth = $origWidth;
                $calcHeight = ceil($height*($origWidth/$width));
                $offsetTop = ceil(($origHeight-$calcHeight)/2);
                $offsetLeft = 0;
            }
        }

        if (function_exists('imagecreatetruecolor')) { // GD v2
            $imgThumb = imagecreatetruecolor($width, $height);
        } else {
            $imgThumb = imagecreate($width, $height);
        }
        if (!$imgThumb) {
            return false;
        }

        $ext = strtolower(Filesystem::getFileExtension($source));
        if ($ext == 'gif') {
            $transparentIndex = imagecolortransparent($imgOrig);
            if ($transparentIndex >= 0) {
                // get original image's transparent color's RGB values
                @$transparentColor = imagecolorsforindex($imgOrig, $transparentIndex);
                // allocate the same color in the new image
                $transparentIndex = imagecolorallocate(
                    $imgThumb, $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']
                );
                // fill the background of the new image with allocated color
                imagefill($imgThumb, 0, 0, $transparentIndex);
                // set the background color to transparent
                imagecolortransparent($imgThumb, $transparentIndex);
            }
        }
        if ($ext == 'png') {
            // temporarily turn off transparency blending
            imagealphablending($imgThumb, false);
            imagesavealpha($imgThumb, true);
            // create a new transparent color for image
            $transparent = imagecolorallocatealpha($imgThumb, 0, 0, 0, 127);
            // fill the background of the new image with allocated color
            imagefilledrectangle($imgThumb, 0, 0, $width, $height, $transparent);
            // restore transparency blending
            imagesavealpha($imgThumb, true);
        }

        if (function_exists('imagecopyresampled')) { // GD v2
            imagecopyresampled(
                $imgThumb, $imgOrig, 0, 0, $offsetLeft, $offsetTop, $width, $height, $calcWidth, $calcHeight
            );
        } else {
            imagecopyresized(
                $imgThumb, $imgOrig, 0, 0, $offsetLeft, $offsetTop, $width, $height, $calcWidth, $calcHeight
            );
        }

        // sharpen image
        if (is_a($sharpen, 'GstLib\ImageSharpen')) {
            $sharpen->sharpen($imgThumb);
        }

        if (!self::_saveImage($imgThumb, $target, $quality)) {
            return false;
        }

        imagedestroy($imgOrig);
        imagedestroy($imgThumb);
        return true;
    }

    /**
     * calculate new size of image if given image is larger than required size
     *
     * Returns associative array (indices width, height, resize). Returned value
     * resize means thar new size differs from original size. If source file is
     * smaller than required size, returns the same size as original image and
     * resize false.
     *
     * @access private
     * @param  string $source  absolute path of source file
     * @param  int    $width
     * @param  int    $height
     * @return array|false array with new width and height or false on error
     */
    private static function _getThumbnailSize($source, $width, $height)
    {
        if ($width <= 0 || $height <= 0) {
            user_error('invalid size:'.$width.'x'.$height, E_USER_WARNING);
            return false;
        }
        $size = @getimagesize($source);
        if (!is_array($size)) {
            user_error('cannot read image size of '.$source, E_USER_WARNING);
            return false;
        }

        list($origWidth, $origHeight) = $size;
        if ($origWidth <= $width && $origHeight <= $height) {
            return (array('width'=>$origWidth, 'height'=>$origHeight, 'resize'=>false)) ; // do not need resizing
        }

        if ($origWidth > $width) {
            $thumbWidth = $width;
            $thumbHeight = ceil($origHeight/$origWidth * $thumbWidth);
            if ($thumbHeight > $height) {
                $thumbHeight = $height;
                $thumbWidth = ceil($origWidth/$origHeight * $thumbHeight);
            }
        } else {
            $thumbHeight = $height;
            $thumbWidth = ceil($origWidth/$origHeight * $thumbHeight);
            if ($thumbWidth > $width) {
                $thumbWidth = $width;
                $thumbHeight = ceil($origHeight/$origWidth * $thumbWidth);
            }
        }
        return array('width'=>$thumbWidth, 'height'=>$thumbHeight,
            'resize'=>true);
    }

    private static function _getThumbnailWidth($source, $maxHeight)
    {
        if ($maxHeight <= 0) {
            user_error('invalid params $maxHeight='.$maxHeight, E_USER_WARNING);
            return false;
        }
        $size = getimagesize($source);
        if (!is_array($size)) {
            user_error(
                'cannot read image size of '.$source, E_USER_WARNING
            );
            return false;
        }

        list($origWidth, $origHeight) = $size;
        if ($origHeight <= $maxHeight) {
            return array('width'=>$origWidth, 'height'=>$origHeight,
            'resize'=>false); // do not need resizing
        }

        $thumbHeight = $maxHeight;
        $thumbWidth = ceil($origWidth/$origHeight * $thumbHeight);
        return array('width'=>$thumbWidth, 'height'=>$thumbHeight,
            'resize'=>true);
    }

    /**
     * calculate thumbnail size. Returns array with indices 'width, 'height'
     *
     * @param int $origWidth width of original image
     * @param int $origHeight height of original image
     * @param int $maxWidth max width of thumbnail
     * @param int $maxHeight max height of thumbnail
     * @return array false on error
     */
    public static function calcThumbSize($origWidth, $origHeight, $maxWidth, $maxHeight)
    {
        if ($maxWidth <= 0 || $maxHeight <= 0) {
            user_error('invalid params '.$maxWidth.','.$maxHeight, E_USER_WARNING);
            return false;
        }

        if ($origWidth <= $maxWidth && $origHeight <= $maxHeight) {
            return array('width'=>$origWidth, 'height'=>$origHeight);
        }

        if ($origWidth > $maxWidth) {
            $thumbWidth = $maxWidth;
            $thumbHeight = ceil($origHeight/$origWidth * $thumbWidth);
            if ($thumbHeight > $maxHeight) {
                $thumbHeight = $maxHeight;
                $thumbWidth = ceil($origWidth/$origHeight * $thumbHeight);
            }
        } else {
            $thumbHeight = $maxHeight;
            $thumbWidth = ceil($origWidth/$origHeight * $thumbHeight);
            if ($thumbWidth > $maxWidth) {
                $thumbWidth = $maxWidth;
                $thumbHeight = ceil($origHeight/$origWidth * $thumbWidth);
            }
        }
        return array('width'=>$thumbWidth, 'height'=>$thumbHeight);
    }

    /**
     * @access private
     * @return bool   false if GD extension not found
     */
    private static function _checkGd()
    {
        if (!extension_loaded('gd')) {
            user_error('GD extension is not loaded!', E_USER_ERROR);
            return false;
        }
        return true;
    }

    /**
     * check source file
     * @access private
     * @param  string $fullPath
     * @return bool   false on error
     */
    private static function _checkOriginalFile($fullPath)
    {
        if (!is_file($fullPath)) {
            user_error('file '.$fullPath.' not found', E_USER_WARNING);
            return false;
        }

        // check extension
        $tmp = explode('.', $fullPath);
        $ext = strtolower(end($tmp));
        if (!in_array($ext, array('jpg', 'jpeg', 'png','gif'))) {
            user_error(
                'unsupported or unknown extension of '.$fullPath, E_USER_WARNING
            );
            return false;
        }
        return true;
    }

    /**
     * save image to file
     * @access private
     * @param  resource $img
     * @param  string   $fullpath
     * @param  int      $quality
     * @return bool     false on error
     */
    private static function _saveImage($img, $fullpath, $quality)
    {
        $tmp = explode('.', $fullpath);
        $ext = strtolower(end($tmp));

        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                $save = imagejpeg($img, $fullpath, $quality);
                break;
            case 'png':
                $save = imagepng($img, $fullpath);
                break;
            case 'gif':
                $save = imagegif($img, $fullpath);
                break;
            default:
                $save = false;
                break;
        }
        return $save;
    }

    /**
     * read image from file
     * @access private
     * @param  string   $fullpath
     * @return resource false on error
     */
    private static function _openImage($fullpath)
    {
        $tmp = explode('.', $fullpath);
        $ext = strtolower(end($tmp));
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                $img = imagecreatefromjpeg($fullpath);
                break;
            case 'png':
                $img = imagecreatefrompng($fullpath);
                break;
            case 'gif':
                $img = imagecreatefromgif($fullpath);
                break;
            default:
                $img = false;
                break;
        }
        if (!$img) {
            user_error('cannot read image from '.$fullpath, E_USER_WARNING);
            return false;
        }
        return $img;
    }

    /**
     * create image file with watermark image
     *
     * $option can have indices:
     * 'transparency' - (1-100), default 80;
     * 'offsetX', 'offsetY' - offset of watermark from left-top/right-bottom
     * corner
     * 'alignX' - alignment [center|left|right], default center
     * 'alignY' - alignment [center|top|bottom], default center
     * 'quality' - quality of jpg, default 85
     *
     * @static array  $watermarks        cache with watermarks
     * @param  string $originalFullPath  absolute path of source image
     * @param  string $thumbnailFullPath absolute path of target image
     * @param  string $watermarkFullPath absolute path of watermark image
     * @param  array  $options
     * @return bool   false on error
     */
    public static function watermark($originalFullPath, $thumbnailFullPath,
            $watermarkFullPath, array $options = array())
    {
        static $watermarks;

        if (!is_file($originalFullPath)) {
            user_error('file '.$originalFullPath.' not found', E_USER_WARNING);
            return false;
        }

        if (!isset($options['transparency'])) {
            $options['transparency'] = 80;
        }
        if (!isset($options['offsetX'])) {
            $options['offsetX'] = 0;
        }
        if (!isset($options['offsetY'])) {
            $options['offsetY'] = 0;
        }
        if (!isset($options['alignX'])) {
            $options['alignX'] = 'center';
        }
        if (!isset($options['alignY'])) {
            $options['alignY'] = 'center';
        }
        if (!isset($options['quality'])) {
            $options['quality'] = 85;
        }

        $imgOriginal = self::_openImage($originalFullPath);
        if ($imgOriginal === false) {
            return false;
        }

        if (isset($watermarks[$watermarkFullPath])) {
            $imgWatermark = $watermarks[$watermarkFullPath];
        } else {
            $imgWatermark = self::_openImage($watermarkFullPath);
            if ($imgWatermark === false) {
                return false;
            }
            $watermarks[$watermarkFullPath] = $imgWatermark;
        }

        list($originalWidth, $originalHeight) = getimagesize($originalFullPath);
        list($watermarkWidth, $watermarkHeight) =
            getimagesize($watermarkFullPath);

        $posX = $posY = 0;
        switch($options['alignX']) {
            case 'left':
                $posX = $options['offsetX'];
                break;
            case 'right':
                $posX = $originalWidth - $watermarkWidth -
                    $options['offsetX'];
                break;
            default: // 'center'
                $posX = $originalWidth/2 - $watermarkWidth/2 +
                    $options['offsetX'];
                break;
        }
        switch ($options['alignY']) {
            case 'top':
                $posY = $options['offsetY'];
                break;
            case 'bottom':
                $posY = $originalHeight - $watermarkHeight -
                    $options['offsetY'];
                break;
            default: // 'center'
                $posY = $originalHeight/2 - $watermarkHeight/2 +
                    $options['offsetY'];
                break;
        }

        // imagecopymerge maybe doesn't work correctly with jpg+png watermark?
        imagecopymerge(
            $imgOriginal, $imgWatermark, $posX, $posY, 0, 0,
            $watermarkWidth, $watermarkHeight, $options['transparency']
        );

        if (!self::_saveImage(
            $imgOriginal, $thumbnailFullPath, $options['quality']
        )) {
            return false;
        }

        imagedestroy($imgOriginal);
        return true;
    }

    /**
     * check memory_limit and set new higher limit if required
     * @access private
     * @param  int  $needMemory required memory
     * @return bool false on error
     */
    private static function _checkMemory($needMemory)
    {
        $usedMemory = memory_get_usage();
        $memoryLimit = UserFile::bytes(ini_get('memory_limit'));
        if (($usedMemory + $needMemory) > $memoryLimit) {
            // +2MB for sure
            $newLimit = $memoryLimit + $usedMemory + $needMemory + pow(2048, 2);
            $newLimit = (int) UserFile::humanFilesize($newLimit, 0).'M';
            ini_set('memory_limit', $newLimit);
            $memoryLimit = UserFile::bytes(ini_get('memory_limit'));
            if (($usedMemory + $needMemory) > $memoryLimit) {
                user_error(
                    'not enough memory, need:'.$needMemory.', used:'.
                    $usedMemory, E_USER_WARNING
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @access private
     * @param  string $file
     * @return int memory in bytes required for opening image from file
     */
    private static function _mallocImageFile($file)
    {
        $size = getimagesize($file);
        if (!is_array($size)) {
            user_error('cannot read image size of '.$file, E_USER_WARNING);
            return 0;
        }
        // 3 bits per pixel, 0.1% just for sure
        return (int) ($size[0] * $size[1] * 3.001);
    }

    /**
     * @access private
     * @param  int $width
     * @param  int $height
     * @return int memory in bytes required for creating image in memory
     */
    private static function _mallocImage($width, $height)
    {
        // 3 bits per pixel, 0.1% just for sure
        return $width * $height * 3.001;
    }

    /**
     * convert image to grayscale
     *
     * $option can have indices:
     * 'quality' - quality of jpg, default 85
     *
     * @param  string $originalFullPath absolute path of source image
     * @param  string $targetFullPath   absolute path of target image
     * @param  array  $options
     * @return bool   false on error
     */
    public static function grayscale($originalFullPath, $targetFullPath,
            array $options = array())
    {
        if (!is_file($originalFullPath)) {
            user_error('file '.$originalFullPath.' not found', E_USER_WARNING);
            return false;
        }

        if (!isset($options['quality'])) {
            $options['quality'] = 85;
        }

        $imgOriginal = self::_openImage($originalFullPath);
        if ($imgOriginal === false) {
            return false;
        }

        if (!imagefilter($imgOriginal, IMG_FILTER_GRAYSCALE)) {
            return false;
        }

        if (!self::_saveImage(
            $imgOriginal, $targetFullPath, $options['quality']
        )) {
            return false;
        }

        imagedestroy($imgOriginal);
        return true;
    }

    /**
     * sharpen image
     *
     * $option can have indices:
     * 'quality' - quality of jpg, default 85
     *
     * @param string $originalFullPath absolute path of source image
     * @param string $targetFullPath absolute path of target image
     * @param ImageSharpen $sharpen
     * @param array $options
     * @return bool false on error
     */
    public static function sharpen($originalFullPath, $targetFullPath, ImageSharpen $sharpen,
        array $options = array())
    {
        if (!is_file($originalFullPath)) {
            user_error('file '.$originalFullPath.' not found', E_USER_WARNING);
            return false;
        }

        $options['quality'] = (isset($options['quality']) ? (int) $options['quality'] : 85);
        $options['quality'] = ($options['quality'] < 0 || $options['quality'] > 100 ? 85 : $options['quality']);

        $imgOriginal = self::_openImage($originalFullPath);
        if ($imgOriginal === false) {
            return false;
        }

        if (!$sharpen->sharpen($imgOriginal)) {
            return false;
        }

        if (!self::_saveImage($imgOriginal, $targetFullPath, $options['quality'])) {
            return false;
        }

        imagedestroy($imgOriginal);
        return true;
    }

}

/**
 * sharpen image
 * require GD extension
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package GstLib
 */
class ImageSharpen
{
    /** @var array $matrix 3x3 matrix, an array of three arrays of three floats */
    private $_matrix = array();

    /**
     * constructor
     * @param array $matrix array with 9 floats (3x3 matix)
     */
    public function __construct(array $matrix = array())
    {
        $this->_matrix = array(
            array(0.0, -1.0, 0.0),
            array(-1.0, 16.0, -1.0),
            array(0.0, -1.0, 0.0)
        );

        $count = 0;
        foreach ($matrix as $value) {
            $x = (int) ($count/3);
            $y = (int) ($count-($x*3));
            $this->_matrix[$x][$y] = (float) $value;
            $count++;
            if ($count > 8) {
                break;
            }
        }
    }

    /**
     * sharpen given image
     * @param resource $image
     * @return bool false on error
     */
    public function sharpen($image)
    {
        $divisor = array_sum(array_map('array_sum', $this->_matrix));
        return imageconvolution($image, $this->_matrix, $divisor, 0);
    }

}