<?php
/**
 * class Gst_UserFile
 *
 * @package GstLib
 */

namespace GstLib;

require_once __DIR__.'/Html.php';
require_once __DIR__.'/Filesystem.php';

/**
 * library of functions for manipulating with user's files (uploaded photos and the like)
 * Assumes files with names in format id_<filename>.<extension>
 * stored in 'base_dir' and subdirectories. Names of subdirectories are created from id of file.
 * For example:
 * /some_dir/images/1_image.jpg<br />
 * /some_dir/images/1/12_another-image.gif<br />
 * Number of files in each subdir can be set from unlimited or 10,100 or 100
 * files in directory.
 *
 * @version	  0.0.1
 * @since	  26.1.2007
 * @author	  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @copyright 2007 GST
 * @package	  GstLib
 */
class UserFile
{
    /** @var special case, means 'try to search' */
    const FILES_PER_DIR_UNKNOWN = -1;
    /** @var store all files in one directory (in base directory) */
    const FILES_PER_DIR_UNLIMITED = 0;
    /** @var store max 10 files in one directory, more files store in subdirectories */
    const FILES_PER_DIR_10 = 1;
    /** @var store max 100 files in one directory, more files store in subdirectories */
    const FILES_PER_DIR_100 = 2;
    /** @var store max 1000 files in one directory, more files store in subdirectories */
    const FILES_PER_DIR_1000 = 3;

    /**
     * return relative directory path created from specified numeric ID and number of files per directory
     *
     * @param int $fileId
     * @param enum|int $filesPerDir 0|1|2|3 = files per dir:unlimited|10|100|100
     * @return string
     */
    public static function idToDir($fileId, $filesPerDir = self::FILES_PER_DIR_UNLIMITED)
    {
        if ($fileId == 0 || $filesPerDir == self::FILES_PER_DIR_UNLIMITED) {
            return '';
        }

        $tmp = array();
        $strId  = strval($fileId);
        $files = self::FILES_PER_DIR_UNLIMITED;
        $allow = array(self::FILES_PER_DIR_10, self::FILES_PER_DIR_100, self::FILES_PER_DIR_1000);
        if (in_array($filesPerDir, $allow)) {
            $files = $filesPerDir;
        }

        for ($x = 0; $x<strlen($fileId)-$files; $x++) {
            $tmp[] = $strId{$x};
        }
        $dir = implode('/', $tmp);
        if ($dir != '') {
            $dir .= '/';
        }
        return $dir;
    }

    /**
     * returns fixed filename: remove accents, replace all chars except [a-z0-9_.] with hyphen
     * @param string $filename
     * @return string
     */
    public static function fixFileName($filename)
    {
        $filename = trim($filename);
        if ($filename ===  '') {
            return '';
        }
        $name = Filesystem::getFilenameWoExtension($filename);
        $ext = strtolower(Filesystem::getFileExtension($filename));
        return Html::fixForUrl($name).'.'.$ext;
    }

    /**
     * delete file(s) specified by $fileId
     *
     * $ignoreFileName - name of file that do not be deleted, even if id of file match given $fileId.
     * Parameter $filesPerDir should be FILES_PER_DIR* value, default value is
     * FILES_PER_DIR_UNLIMITED. Specifies subdirectories where file should be
     * stored. Special value FILES_PER_DIR_UNKNOWN means 'search in basedir and
     * all possible subdirs' (slower, use with caution).
     *
     * @param int $fileId
     * @param string $baseDir
     * @param string $ignoreFileName
     * @param int $filesPerDir
     * @return bool false on error
     */
    public static function delete($fileId, $baseDir, $ignoreFileName = '', $filesPerDir = self::FILES_PER_DIR_UNLIMITED)
    {
        static $allowedOptions = array(
            self::FILES_PER_DIR_1000,
            self::FILES_PER_DIR_100,
            self::FILES_PER_DIR_10,
            self::FILES_PER_DIR_UNLIMITED
        );

        $options = array(self::FILES_PER_DIR_UNLIMITED);
        if (in_array($filesPerDir, $allowedOptions)) {
            $options = array($filesPerDir);
        } else {
            if ($filesPerDir == self::FILES_PER_DIR_UNKNOWN) {
                $options = $allowedOptions;
            }
        }

        foreach ($options as $option) {
            $dir = $baseDir.self::idToDir($fileId, $option);
            $files = glob($dir.self::_fileId($fileId).'*');
            $files = ($files === false ? array() : $files); // ignore errors
            foreach ($files as $fullPath) {
                if ($ignoreFileName != '' && basename($fullPath) == $ignoreFileName) {
                    continue;
                }
                if (!unlink($fullPath)) {
                    // @codeCoverageIgnoreStart
                    return false;
                    // @codeCoverageIgnoreEnd
                }
            }
        }

        return true;
    }

    /**
     * returns full filename with specified id
     *
     * Returns empty string if file is not found.
     * Parameter $eFilesPerDir should be FILES_PER_DIR* value, default value is
     * FILES_PER_DIR_UNLIMITED. Specifies subdirectories where file should be
     * stored. Special value FILES_PER_DIR_UNKNOWN means 'search in basedir and
     * all possible subdirs' (slower, use with caution).
     *
     * @param int $fileId id of file
     * @param string $baseDir base directory where files are stored
     * @param enum|int $filesPerDir
     * @return string
     */
    public static function getFullPath($fileId, $baseDir, $filesPerDir = self::FILES_PER_DIR_UNLIMITED)
    {
        static $allowedOptions = array(
            self::FILES_PER_DIR_1000,
            self::FILES_PER_DIR_100,
            self::FILES_PER_DIR_10,
            self::FILES_PER_DIR_UNLIMITED
        );

        $options = array(self::FILES_PER_DIR_UNLIMITED);
        if (in_array($filesPerDir, $allowedOptions)) {
            $options = array($filesPerDir);
        } else {
            if ($filesPerDir == self::FILES_PER_DIR_UNKNOWN) {
                $options = $allowedOptions;
            }
        }
        foreach ($options as $option) {
            $files = glob($baseDir.self::idToDir($fileId, $option).self::_fileId($fileId).'*');
            if (isset($files[0])) {
                return $files[0];
            }
        }
        return '';
    }

    /**
     * create new filename from specified id and original filename
     * @param int $fileId
     * @param string $fileName
     * @return string
     */
    public static function createFileName($fileId, $fileName)
    {
        return self::_fileId($fileId).self::fixFileName($fileName);
    }

    /**
     * returns a human readable filesize with binary or decimal sufix. Doesnt work correctly for filesize > 2GiB
     * @param int $filesize
     * @param bool $decimalSufix true|false - return decimal|binary sufix
     * @param int $decimals number of digits after decimal point
     * @param string $decPoint string representing decimal point
     * @param string $thousandSeparator string representing thousands separator
     * @return string
     */
    public static function humanFilesize($filesize, $decimalSufix = false, $decimals = 2, $decPoint = '.',
            $thousandSeparator = '')
    {
        $filesize = ($filesize < 0 ? 0 : $filesize);

        $prefixes = array('B', 'KiB', 'MiB', 'GiB');
        $divider = 1024;
        if ($decimalSufix) {
            $prefixes = array('B', 'kB', 'MB', 'GB');
            $divider = 1000;
        }

        $iter = 0;
        while (($filesize/$divider)>=1) {
            $filesize = $filesize/$divider;
            $iter++;
        }

        $ret = Vars::formatNumberHuman($filesize, $decimals, $decPoint, $thousandSeparator);
        return $ret.' '.$prefixes[$iter];
    }

    /**
     * return mime of file by extension. For unknown extensions returns 'application/octet-stream'
     * @param string $fileName
     * @return string
     */
    public static function getMime($fileName)
    {
        $tmp = explode('.', $fileName);
        $extension = strtolower(end($tmp));

        static $mimeTypes = array(
            'ogg'=>'application/ogg',
            'pdf'=>'application/pdf',
            'rtf'=>'application/rtf',
            'zip'=>'application/zip',
            'ai'=>'application/postscript', 'eps'=>'application/postscript', 'ps'=>'application/postscript',
            'xls'=>'application/vnd.ms-excel',
            'ppt'=>'application/vnd.ms-powerpoint',
            'sxw'=>'application/vnd.sun.xml.writer',
            'sxc'=>'application/vnd.sun.xml.calc',
            'torrent'=>'application/x-bittorrent',
            'gz'=>'application/x-gzip', 'tgz'=>'application/x-gzip',
            'js'=>'application/x-javascript',
            'swf'=>'application/x-shockwave-flash',
            'rpm'=>'application/x-rpm',
            'xhtml'=>'application/xhtml+xml', 'xht'=>'application/xhtml+xml',
            'mid'=>'audio/midi', 'midi'=>'audio/midi',
            'mp2'=>'audio/mpeg', 'mp3'=>'audio/mpeg',
            'ram'=>'audio/x-pn-realaudio', 'rm' => 'audio/x-pn-realaudio',
            'ra'=>'audio/x-realaudio',
            'wav'=>'audio/x-wav',
            'bmp'=>'image/bmp',
            'gif'=>'image/gif',
            'jpg'=>'image/jpeg', 'jpeg'=>'image/jpeg', 'jpe'=>'image/jpeg',
            'png'=>'image/png',
            'tiff'=>'image/tiff', 'tif'=>'image/tiff',
            'wmf'=>'image/wmf',
            'wrl'=>'model/vrml', 'vrml'=>'model/vrml',
            'css'=>'text/css',
            'html'=>'text/html', 'htm'=>'text/html',
            'xml'=>'text/xml', 'xsl'=>'text/xml',
            'txt'=>'text/plain', 'asc'=>'text/plain',
            'mpg'=>'video/mpeg', 'mpeg'=>'video/mpeg', 'mpe'=>'video/mpeg',
            'mov'=>'video/quicktime', 'qt'=>'video/quicktime',
            'avi'=>'video/x-msvideo',
            'wmv'=>'video/x-ms-wmv'
        );
        if (isset($mimeTypes[$extension])) {
            return $mimeTypes[$extension];
        }
        return 'application/octet-stream';
    }

    /**
     * converts giga/mega/kilobytes to bytes.
     * Assume given param ending with 'G', 'M' or 'K'
     * @param string $size
     * @return int
     */
    public static function bytes($size)
    {
        $size = trim($size);
        $lastChar = strtoupper(substr($size, -1));
        if (in_array($lastChar, array('G', 'M', 'K'))) {
            $bytes = intval(substr($size, 0, strlen($size)-1));
            // The 'G' modifier is available since PHP 5.1.0
            if ($lastChar == 'G') {
                $bytes = $bytes * 1024 * 1024 * 1024;
            }
            if ($lastChar == 'M') {
                $bytes = $bytes * 1024 * 1024;
            }
            if ($lastChar == 'K') {
                $bytes = $bytes * 1024;
            }
        } else {
            $bytes = intval($size);
        }
        return $bytes;
    }

    /**
     * returns filename without id prefix (assume filename in format id_filename.ext)
     * @param string $fileName
     * @return string
     */
    public static function stripId($fileName)
    {
        $pos = strpos($fileName, '_');
        $pos = ($pos === false ? 0 : $pos+1);
        return substr($fileName, $pos);
    }

    /**
     * returns with identifier, filename have to start with this string
     * @param int $fileId
     * @return string
     */
    private static function _fileId($fileId)
    {
        return $fileId.'_';
    }

}