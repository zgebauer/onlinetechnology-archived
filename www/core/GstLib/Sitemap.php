<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once __DIR__.'/Date.php';
require_once __DIR__.'/Html.php';

/**
 * generate sitemaps for search engines
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   GstLib
 * @see       http://www.sitemaps.org/
 */
class Sitemap
{
    /** @var array of urls in sitemaps */
    private $_items;
    /** @var array of url of sitemaps for sitemap index */
    private $_sitemaps;
    /** @var option for change frequency */
    const CHANGE_ALWAYS = 'always';
    /** @var option for change frequency */
    const CHANGE_HOURLY = 'hourly';
    /** @var option for change frequency */
    const CHANGE_DAILY = 'daily';
    /** @var option for change frequency */
    const CHANGE_WEEKLY = 'weekly';
    /** @var option for change frequency */
    const CHANGE_MONTHLY = 'monthly';
    /** @var option for change frequency */
    const CHANGE_YEARLY = 'yearly';
    /** @var option for change frequency */
    const CHANGE_NEVER = 'never';

    /** constructor */
    public function  __construct()
    {
        $this->_items = array();
        $this->_sitemaps = array();
    }

    /**
     * add url to sitemap
     * @param url $url
     * @param date $lastMod last modification of url
     * @param string $changeFreq how frequently changes content of url
     * @param float $priority relative priority [0.0-1.0]
     * @return bool false on error
     */
    public function addItem($url, $lastMod = null, $changeFreq = null, $priority = null)
    {
        $item = new SitemapItem($url, $lastMod, $changeFreq, $priority);
        if (!$item->isValid()) {
            return false;
        }
        $this->_items[] = $item;
        return true;
    }

    /**
     * add sitemap to sitemap index
     * @param string $url url of sitemap file
     * @param date $lastMod last modification of sitemap file
     * @return bool false on error
     */
    public function addSitemap($url, $lastMod = null)
    {
        $item = new SitemapIndexItem($url, $lastMod);
        if (!$item->isValid()) {
            return false;
        }
        $this->_sitemaps[] = $item;
        return true;
    }

    /**
     * returns XML document with sitemap
     * @return string
     */
    public function getXml()
    {
        $ret = '<?xml version="1.0" encoding="UTF-8"?>'."\n".
                '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        /** @var $item SitemapItem */
        foreach ($this->_items as $item) {
            $ret .= $item->getXml();
        }
        $ret .= '</urlset>';
        return $ret;
    }

    /**
     * returns XML document with sitemap index
     * @return string
     */
    public function getXmlSitemap()
    {
        $ret = '<?xml version="1.0" encoding="UTF-8"?>'."\n".
            '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        /** @var $item SitemapIndexItem */
        foreach ($this->_sitemaps as $item) {
            $ret .= $item->getXml();
        }
        $ret .= '</sitemapindex>';
        return $ret;
    }

}


/**
 * url in sitemap
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package GstLib
 */
class SitemapItem
{
    /** @var string url of page */
    private $_url;
    /** @var string last modification of page */
    private $_lastMod;
    /** @var string change frequency of page */
    private $_changeFreq;
    /** @var float priority of page */
    private $_priority;
    /** @var bool flag "parameters are valid" */
    private $_isValid;

    /**
     * constructor
     * @param url $url url of page
     * @param date $lastMod last modification of page
     * @param string $changeFreq how frequently changes content of page
     * @param float $priority relative priority [0.0-1.0]
     */
    public function  __construct($url, $lastMod = null, $changeFreq = null, $priority = null)
    {
        $this->_url = Html::fixUrl(trim($url));
        $this->_lastMod = Date::EMPTY_DATETIME;
        $this->_changeFreq = $this->_priority = null;
        $this->_isValid = true;

        $lastMod = (($lastMod == Date::EMPTY_DATE || $lastMod == Date::EMPTY_DATETIME) ? null : $lastMod);
        if (is_null($lastMod) || Date::isIsoDate($lastMod) ) {
            $this->_lastMod = $lastMod;
        } else {
            $this->_isValid = false;
            user_error('invalid date:'.$lastMod, E_USER_NOTICE);
            return;
        }

        $allow = array(null, Sitemap::CHANGE_ALWAYS, Sitemap::CHANGE_HOURLY, Sitemap::CHANGE_DAILY,
            Sitemap::CHANGE_WEEKLY, Sitemap::CHANGE_MONTHLY, Sitemap::CHANGE_YEARLY,
            Sitemap::CHANGE_NEVER);

        if (in_array($changeFreq, $allow)) {
            $this->_changeFreq = $changeFreq;
        } else {
            $this->_isValid = false;
            user_error('invalid frequency:'.$changeFreq, E_USER_NOTICE);
            return;
        }

        if (!is_null($priority)) {
            $priority = Vars::floatval($priority);
            if ($priority < 0 || $priority > 1) {
                $this->_isValid = false;
                user_error('invalid priority:'.$priority, E_USER_NOTICE);
                return;
            } else {
                $this->_priority = $priority;
            }
        }

    }

    /**
     * check if values of item are valid
     * @return bool
     */
    public function isValid()
    {
        return $this->_isValid;
    }

    /**
     * returns item as XML
     * @return string
     */
    public function getXml()
    {
        $ret = '';
        if ($this->_isValid) {
            $ret = '<url><loc>'.$this->_url.'</loc>';
            if (!is_null($this->_lastMod)) {
                $ret .= '<lastmod>'.$this->_lastMod.date('P').'</lastmod>';
            }
            if (!is_null($this->_changeFreq)) {
                $ret .= '<changefreq>'.$this->_changeFreq.'</changefreq>';
            }
            if (!is_null($this->_priority)) {
                $ret .= '<priority>'.$this->_priority.'</priority>';
            }
            $ret .= '</url>';
        }
        return $ret;
    }

}


/**
 * url in sitemap index
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package GstLib
 */
class SitemapIndexItem
{
    /** @var string url of page */
    private $_url;
    /** @var string last modification of page */
    private $_lastMod;
    /** @var bool flag "parameters are valid" */
    private $_isValid;

    /**
     * constructor
     * @param url $url url of sitemap
     * @param date $lastMod last modification of sitemap
     */
    public function  __construct($url, $lastMod = null)
    {
        $this->_url = Html::fixUrl(trim($url));
        $this->_lastMod = Date::EMPTY_DATETIME;
        $this->_isValid = true;

        $lastMod = (($lastMod == Date::EMPTY_DATE || $lastMod == Date::EMPTY_DATETIME) ? null : $lastMod);
        if (is_null($lastMod) || Date::isIsoDate($lastMod) ) {
            $this->_lastMod = $lastMod;
        } else {
            $this->_isValid = false;
            user_error('invalid date:'.$lastMod, E_USER_NOTICE);
            return;
        }
    }

    /**
     * check if values item are valid
     * @return bool
     */
    public function isValid()
    {
        return $this->_isValid;
    }

    /**
     * returns item as XML
     * @return string
     */
    public function getXml()
    {
        $ret = '';
        if ($this->_isValid) {
            $ret = '<sitemap><loc>'.$this->_url.'</loc>';
            if (!is_null($this->_lastMod)) {
                $ret .= '<lastmod>'.$this->_lastMod.'</lastmod>';
            }
            $ret .= '</sitemap>';
        }
        return $ret;
    }

}