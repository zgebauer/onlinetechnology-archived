<?php
/**
 * class DriverAbstract, DatabaseErrorException
 *
 * @package GstLib
 */

namespace GstLib\Db;

/**
 * abstract database driver
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Db
 */
abstract class DriverAbstract
{
    /**
     * returns last error message
     * @return string
     */
    abstract public function getErr();

    /**
     * open connection to database
     * @return bool false on error
     */
    abstract public function connect();

    /**
     * close current connection
     * @return bool false on error
     */
    abstract public function close();

    /**
     * executes SQL query
     * @param string $sql SQL query
     * @return bool false on error
     */
    abstract public function query($sql);

    /**
     * returns one row as associative array. If param is empty use recordset from previous fetchArray()
     * @param string $sql SQL query for new recordset
     * @return array record on success, false on error or empty result
     */
    abstract public function fetchArray($sql = '');

    /**
     * returns all rows as array of associative arrays
     * @param string $sql SQL query for new recordset
     * @return array record on success, false on error or empty result
     */
    abstract public function fetchArrayAll($sql);

    /**
     * returns array values of only one column
     * @param string $column name of required column
     * @param string $sql SQL query for new recordset
     * @return array array on success, false on error or empty result
     */
    abstract public function fetchColAll($column, $sql);

    /**
     * returns id of new record on success, false on error
     * @param string $sql SQL INSERT query
     * @return int
     */
    abstract public function insert($sql);

    /**
     * returns number of rows on success, false on error
     * @param string $sql SQL SELECT COUNT query
     * @return int
     */
    abstract public function count($sql);

    /**
     * free current recordset
     * @return bool false on error
     */
    abstract public function free();

    /**
     * executes array of SQL queries as single transtaction
     * @param array $sql array of SQL queries
     * @return bool false on error
     */
    abstract public function transaction(array $sql);

    /**
     * begin transaction
     * @return bool false on error
     */
    abstract public function beginTrans();

    /**
     * commit transaction
     * @return bool false on error
     */
    abstract public function commit();

    /**
     * rollback transaction
     * @return bool false on error
     */
    abstract public function rollback();

    /**
     * returns escaped and quoted string for sql according to current database
     * @param string $value unescaped string
     * @return string
     */
    abstract public function escape($value);

    /**
     * returns SQL representation of $value according to current database
     * @param date $value ISO-8601 date or datetime
     * @return mixed
     */
    abstract public function datetimeToSql($value);

    /**
     * returns SQL representation of boolean $value according to current database
     * @param bool $value
     * @return mixed
     */
    abstract public function boolToSql($value);

    /**
     * sets type casting for returned records
     * @param string $columnName name of column
     * @param string $dataType required data type [bool|int|float|date|time|iso-datetime]
     */
    abstract public function setType($columnName, $dataType);

    /** resets type casting for returned records to default data types */
    abstract public function resetTypes();

    /**
     * returns string formatted sql
     * @param string $sql sql query
     * @return string
     */
    abstract public function showSql($sql);

}


/**
 * exception about database error
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Db
 */
class DatabaseErrorException extends \ErrorException
{
    /**
     * constructor 
     * @param string $message exception message
     * @param int $code user defined exception code
     * @param type $severity severity of the exception
     * @param type $filename source file of exception
     * @param type $lineno source line of exception
     * @param \GstLib\Db\Exception $previous previous exception if nested exception
     */
    public function __construct($message, $code = 0, $severity = 1, $filename = __FILE__, $lineno = __LINE__,
            Exception $previous = NULL)
    {
        parent::__construct($message, $code, $severity, $filename, $lineno, $previous);
    }
}