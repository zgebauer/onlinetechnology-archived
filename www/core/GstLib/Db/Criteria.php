<?php

namespace GstLib\Db;

/**
 * auxiliary object holding database criteria
 *
 * @package GstLib
 */
class Criteria {

	/** @var string */
	protected $column;

	/** @var string */
	protected $value;

	/** @var string */
	protected $operator;

	/**
	 * condition for column, ie:
	 * new Criteria('id', '123')
	 * new Criteria('name', '%some%', 'LIKE')
	 * new Criteria('id', '(1,2,3)', 'IN')
	 * new Criteria('id', '1 AND 10', 'BETWEEN')
	 * new Criteria('column', 'IS NULL')
	 * new Criteria('name', '^[a-d]', 'REGEXP')
	 *
	 * @param string $column
	 * @param string $value
	 * @param string $operator
	 */
	public function __construct($column, $value, $operator = '=') {
		$this->column = $column;
		$this->value = $value;
		$this->operator = $operator;
	}

	/**
	 * returns condition formated for WHERE clause
	 * @return string
	 */
	public function getWhere(DriverAbstract $con = NULL) {
		$escapedValue = (is_null($con) ? "'" . mysqli_real_escape_string($this->value) . "'"
			: $con->escape($this->value));
		return '`' . $this->column . '`' . $this->operator . $escapedValue;
	}

}
