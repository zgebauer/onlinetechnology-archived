<?php
/**
 * class Gst_Db_DriverMysqli
 *
 * @package GstLib
 */

namespace GstLib\Db;

require_once 'Abstract.php';

/**
 * Database driver for mysqli extension
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Db
 */
class DriverMysqli extends DriverAbstract
{
    /** @var array configuration parameters */
    private $_configuration;
    /** @var int id of last inserted record */
    private $_lastInsertId;
    /** @var resource database connection */
    private $_connection;
    /** @var resource current recordset */
    private $_recordset;
    /** @var array conversion table */
    private $_typeConversion;
    /** @var string last error message */
    private $_err;

	/**
	 * @var callable query logger
	 */
	protected $logger;

    /**
     * constructor
     * @param array $options
     */
    public function __construct(array $options)
    {

        $this->_configuration = array(
            'server' => '127.0.0.1',
            'user' => 'root',
            'password' => '',
            'database' => 'test',
            'persistent' => false,
            'table_prefix' => '',
            'charset' => 'utf8'
        );
        if (isset($options['server'])) {
            $this->_configuration['server'] = $options['server'];
        }
        if (isset($options['user'])) {
            $this->_configuration['user'] = $options['user'];
        }
        if (isset($options['password'])) {
            $this->_configuration['password'] = $options['password'];
        }
        if (isset($options['database'])) {
            $this->_configuration['database'] = $options['database'];
        }
        if (isset($options['persistent'])) {
            $this->_configuration['persistent'] = (bool) $options['persistent'];
        }
        if (isset($options['table_prefix'])) {
            $this->_configuration['table_prefix'] = $options['table_prefix'];
        }
        if (isset($options['charset'])) {
            $this->_configuration['charset'] = $options['charset'];
        }

        $this->_lastInsertId = false;
        $this->_connection = null;
        $this->_recordset = null;
        $this->_typeConversion = null;
        $this->_err = '';
    }

    /**
     * returns last error message
     * @return string
     */
    public function getErr()
    {
        return $this->_err;
    }

    /**
     * open connection to mysql with mysqli driver
     * @return bool false on error
     * @throws \Exception
     */
    public function connect()
    {
        $this->_err = '';
        $this->_connection = mysqli_connect(
            $this->_configuration['server'], $this->_configuration['user'],
            $this->_configuration['password'], $this->_configuration['database']
        );
        if (!$this->_connection) {
            $this->_err = mysqli_connect_errno().':'.mysqli_connect_error();
            throw new DatabaseErrorException(mysqli_connect_errno().':'.mysqli_connect_error());
            return false;
        }

        if ($this->_configuration['charset'] != '') {
            $this->query('SET CHARACTER SET '.$this->_configuration['charset']);
            $this->query('SET NAMES '.$this->_configuration['charset']);
        }
        return true;
    }

    /**
     * close current connection
     * @return bool false on error
     */
    public function close()
    {
        if (!is_null($this->_connection)) {
            $this->free();
            // kill connection
            $this->_connection->kill($this->_connection->thread_id);
            mysqli_close($this->_connection);
        }
        $this->_connection = null;
        return true;
    }

    /**
     * executes SQL query
     * @param string $sql SQL query
     * @return bool false on error
     * @throws DatabaseErrorException
     */
    public function query($sql)
    {
        if (is_null($this->_connection)) {
            $this->connect();
        }
        if (is_null($this->_connection)) {
            return false;
        }

        $this->_err = '';
        $this->_lastInsertId = false;
		$logQueryEnabled = is_callable($this->logger);

        $sql = $this->_formatSQLQuery($sql);
		$timeStart = microtime(true);
        $this->_recordset = mysqli_query($this->_connection, $sql);
		$duration = microtime(true) - $timeStart;
        if ($this->_recordset === false) {
            $this->_recordset = null;
            $this->_err = 'cannot execute: '.$sql.'-'.
            mysqli_errno($this->_connection).'-'.mysqli_error($this->_connection);
            throw new DatabaseErrorException(
                mysqli_error($this->_connection).':'.$sql, mysqli_errno($this->_connection)
            );
            return false;
        }

		if ($logQueryEnabled) {
			call_user_func($this->logger, $sql, $duration, $this->_connection->affected_rows);
		}

        $this->_lastInsertId = mysqli_insert_id($this->_connection);
        if ($this->_lastInsertId < 1) {
            $this->_lastInsertId = false;
        }

        return true;
    }

    /**
     * returns one row as associative array. If param is empty use recordset from previous fetchArray()
     * @param string $sql SQL query for new recordset
     * @return array record on success, false on error or empty result
     */
    public function fetchArray($sql = '')
    {
        if ($sql != '' && !$this->query($sql)) {
            return false;
        }

        $ret = mysqli_fetch_assoc($this->_recordset);
        if (is_null($ret)) {
            return false;
        }

        if (!is_null($this->_typeConversion)) {
            $this->_typeCasting($ret);
        }
        return $ret;
    }

    /**
     * returns all rows as array of associative arrays
     * @param string $sql SQL query for new recordset
     * @return array record on success, false on error or empty result
     */
    public function fetchArrayAll($sql)
    {
        if ($sql != '' && !$this->query($sql)) {
            return false;
        }
        $ret = array();
        while (($row = mysqli_fetch_assoc($this->_recordset)) !== null) {
           if (!is_null($this->_typeConversion)) {
               $this->_typeCasting($row);
           }
           $ret[] = $row;
        }
        return $ret;
    }

    /**
     * returns array values of only one column
     * @param string $column name of required column
     * @param string $sql SQL query for new recordset
     * @return array array on success, false on error or empty result
     */
    public function fetchColAll($column, $sql)
    {
        if ($sql != '' && !$this->query($sql)) {
            return false;
        }

        $ret = array();
        while (($row = mysqli_fetch_assoc($this->_recordset)) !== null) {
            if (!isset($row[$column])) {
                return false;
            }
            if (isset($this->_typeConversion[$column])) {
                $this->_typeCasting($row);
            }
            $ret[] = $row[$column];
        }
        return $ret;
    }

    /**
     * returns id of new record on success, false on error
     * @param string $sql SQL INSERT query
     * @return int
     */
    public function insert($sql)
    {
        if (!$this->query($sql)) {
            return false;
        }
        return $this->_lastInsertId;
    }

    /**
     * returns number of rows on success, false on error
     * @param string $sql SQL SELECT COUNT query
     * @return int
     */
    public function count($sql)
    {
        if (!$this->query($sql)) {
            return false;
        }
        $row = mysqli_fetch_row($this->_recordset);
        return (isset($row[0]) ? (int) $row[0] : false);
    }

    /**
     * free current recordset
     * @return bool false on error
     */
    public function free()
    {
        @mysqli_free_result($this->_recordset);
        $this->_recordset = null;
        return true;
    }

    /**
     * executes array of SQL queries as single transtaction
     * @param array $sql array of SQL queries
     * @return bool false on error
     * @throws DatabaseErrorException
     */
    public function transaction(array $sql)
    {
        if (is_null($this->_connection)) {
            $this->connect();
        }
        if (is_null($this->_connection)) {
            return false;
        }

        $this->_err = '';
        $this->_lastInsertId = false;

        mysqli_autocommit($this->_connection, false); // disable autocommit
        $count = count($sql);
        for ($i = 0; $i < $count; $i++) {
            if (mysqli_query($this->_connection, $this->_formatSQLQuery($sql[$i])) == false) {
                $this->_err = mysqli_errno($this->_connection).'-'.mysqli_error($this->_connection);
                mysqli_rollback($this->_connection);
                throw new DatabaseErrorException($this->_err);
                return false;
            }
        }
        mysqli_commit($this->_connection);
        mysqli_autocommit($this->_connection, true); // enable autocommit
        return true;
    }

    /**
     * begin transaction
     * @return bool false on error
     */
    public function beginTrans()
    {
        $this->_lastInsertId = false;
        // disable autocommit
        return mysqli_autocommit($this->_connection, false);
    }

    /**
     * commit transaction
     * @return bool false on error
     */
    public function commit()
    {
        $ret = mysqli_commit($this->_connection);
        mysqli_autocommit($this->_connection, true); // enable autocommit
        return $ret;
    }

    /**
     * rollback transaction
     * @return bool false on error
     */
    public function rollback()
    {
        $ret = mysqli_rollback($this->_connection);
        mysqli_autocommit($this->_connection, true); // enable autocommit
        return $ret;
    }

    /**
     * returns escaped and quoted string for sql according to current database
     * @param string $value unescaped string
     * @return string
     */
    public function escape($value)
    {
        if (is_null($this->_connection)) {
            $this->connect();
        }
        if (is_null($value)) {
            return "NULL";
        }
        return "'".mysqli_real_escape_string($this->_connection, $value)."'";
    }

    /**
     * returns SQL representation of $value according to current database
     * @param date $value ISO-8601 date or datetime
     * @return mixed
     */
    public function datetimeToSql($value)
    {
        return "'".str_replace('T', ' ', $value)."'";
    }

    /**
     * returns SQL representation of boolean $value according to current database
     * @param bool $value
     * @return mixed
     */
    public function boolToSql($value)
    {
        return ((bool) $value ? 1 : 0);
    }

    /**
     * sets type casting for returned records
     * @param string $columnName name of column
     * @param string $dataType required data type [bool|int|float|date|time|iso-datetime]
     */
    public function setType($columnName, $dataType)
    {
        $this->_typeConversion[$columnName] = $dataType;
    }

    /** resets type casting for returned records to default data types */
    public function resetTypes()
    {
        $this->_typeConversion = null;
    }

    /**
     * returns part of WHERE clause combined from parameters
     * $operator can contains common operator and special operators:
     * 'like' - returns "column LIKE %value%"
     * 'like-left' - returns "column LIKE value%"
     * 'like-right' - returns "column LIKE %value"
     * 'in' - returns "column IN (values)", expects $value as list of values delimited by colon
     *
     * @param string $column
     * @param string $value
     * @param string $operator
     * @return string
     */
//    public function getCriteria($column, $value, $operator = '=')
//    {
//        switch ($operator) {
//        case 'like':
//            $condition = " LIKE '%".mysqli_real_escape_string($this->_connection, $value)."%'";
//            break;
//        case 'like-left':
//            $condition = " LIKE '".mysqli_real_escape_string($this->_connection, $value)."%'";
//            break;
//        case 'like-right':
//            $condition = " LIKE '%".mysqli_real_escape_string($this->_connection, $value)."'";
//            break;
//        case 'in':
//            $tmp = explode(',', $value);
//            $count = count($tmp);
//            for ($i = 0; $i < $count; $i++) {
//                $tmp[$i] = trim($tmp[$i]);
//                $tmp[$i] = $this->escape($tmp[$i]);
//            }
//            $condition = " IN (".join(',', $tmp).')';
//            break;
//        default:
//            if (is_null($value)) {
//                return $column;
//            } else {
//                $condition = $operator.$this->escape($value);
//            }
//        }
//        //return '`'.$column.'`'.$condition;
//        return $column.$condition;
//    }

    /**
     * returns string formatted sql
     * @param string $sql sql query
     * @return string
     */
    public function showSql($sql)
    {
        return $this->_formatSqlQuery($sql);
    }

    /**
     * execute type casting on record according internal conversion table
     * @param array $row record
     */
    private function _typeCasting(array &$row)
    {
        foreach ($this->_typeConversion as $colName=>$dataType) {
            if (isset($row[$colName])) {
                if ($dataType == 'bool') {
                    $row[$colName]= ($row[$colName] == 0 ? false : true);
                }
                if ($dataType == 'int') {
                    $row[$colName] = (int) $row[$colName];
                }
                if ($dataType == 'float') {
                    $row[$colName] = (float) $row[$colName];
                }
                if ($dataType == 'date') {
                    // first 10 YYYY-MM-DD
                    $row[$colName] = substr($row[$colName], 0, 10);
                }
                if ($dataType == 'time') {
                    $row[$colName] = substr($row[$colName], -8); // last 8 hh:mm:ss
                }
                // date and time in ISO8601 format
                if ($dataType == 'iso-datetime') {
                    $row[$colName] = str_replace(' ', 'T', $row[$colName]);
                }
            }
        }
    }

    /**
     * @param string $sql
     * @return string formatted sql query
     */
    private function _formatSqlQuery($sql)
    {
        // replace [%table%] with table prefix
        $sql = str_replace('[%]', '[###]', $sql);
        $sql = str_replace('[%', '`'.$this->_configuration['table_prefix'], $sql);
        $sql = str_replace('%]', '`', $sql);
        $sql = str_replace('[###]', '[%]', $sql);
        return $sql;
    }

    /**
     * returns SQL clause WHERE created from $criteria
     * @param array $criteria array of parts of WHERE clause
     * @param string $join jouning criteria  [AND|OR]
     * @return string
     */
    public function where(array $criteria, $join = 'AND')
    {
        if (!isset($criteria[0])) {
            return '';
        }

        if (is_null($this->_connection)) {
            $this->connect();
        }
        if (is_null($this->_connection)) {
            return '';
        }

        $parts = array();
        foreach ($criteria as $item) {
			if ($item instanceof Criteria) {
				$parts[] = $item->getWhere($this);
				continue;
			}
            if (isset($item[1])) {
                $value = $item[1];
                $operator = strtolower(isset($item[2]) ? $item[2] : '=');
                switch ($operator) {
                case 'like':
                    $condition = " LIKE '%".mysqli_real_escape_string($this->_connection, $value)."%'";
                    break;
                case 'like%':
                    $condition = " LIKE '".mysqli_real_escape_string($this->_connection, $value)."%'";
                    break;
                case '%like':
                    $condition = " LIKE '%".mysqli_real_escape_string($this->_connection, $value)."'";
                    break;
                case 'in':
                    $tmp = explode(',', $value);
                    $count = count($tmp);
                    for ($i = 0; $i < $count; $i++) {
                        $tmp[$i] = trim($tmp[$i]);
                        $tmp[$i] = $this->escape($tmp[$i]);
                    }
                    $condition = " IN (".join(',', $tmp).')';
                    break;
                default:
                    if (is_null($value)) {
                        $condition = '';
                    } else {
                        $condition = $operator.$this->escape($value);
                    }
                }
                $parts[] = $item[0].$condition;
            } else {
                $parts[] = $item[0];
            }
        }

        $join = trim(strtoupper($join));
        $join = ($join !== 'AND' && $join !== 'OR') ? 'AND' : $join;
        return ' WHERE '.join(' '.$join.' ', $parts);
    }

    /**
     * returns clause ORDER BY  created from given parameters
     * @param string $order
     * @param bool $desc
     * @return string
     */
    public function orderBy($order, $desc = false)
    {
        if ($order == '') {
            return '';
        }
        return ' ORDER BY '.$order.($desc ? ' DESC' : ' ASC');
    }

    /**
     * returns clause LIMIT created from given parameters
     * @param int $start start position
     * @param int $rows number of returned rows (null = return all)
     * @return string
     */
    public function limit($start, $rows = null)
    {
        if (!is_null($rows)) {
            return ' LIMIT '.$start.','.$rows;
        }
        return '';
    }

    /**
     * check if table exists
     * @param string $tableName
     * @return bool
     */
    public function tableExists($tableName)
    {

        $tableName = str_replace('[%', $this->_configuration['table_prefix'], $tableName);
        $tableName = str_replace('%]', '', $tableName);

        $sql = "SHOW TABLES LIKE '".$tableName."'";
        return $this->fetchArray($sql);
    }

	/**
	 * register logger for executed query
	 * logger must accept parameters $sql, $time, $affectedRows
	 *
	 * @param callable $logger
	 */
	public function setQueryLogger(callable $logger) {
		$this->logger = $logger;
	}


}