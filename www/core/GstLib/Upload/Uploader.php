<?php

namespace GstLib\Upload;

use GstLib\Upload\Exception;
use GstLib\Upload\Constraints;

/**
 * handler for uploading files
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Upload
 */
class Uploader {

	/** @var bool */
	private $isCli;

	/**
	 * check if exist uploaded file
	 * @param string $field name of input with uploaded file
	 * @return bool
	 * @SuppressWarnings(PHPMD.Superglobals)
	 */
	public static function isUploaded($field) {
		return (isset($_FILES[$field]) && $_FILES[$field]['name'] !== '');
	}

	/**
	 * @SuppressWarnings(PHPMD.Superglobals)
	 */
	public function __construct() {
		$this->isCli = php_sapi_name() === 'cli' || PHP_SAPI === 'cli' || !isset($_SERVER['REMOTE_ADDR']);
	}

	/**
	 * save uploaded file
	 * @param string $target $path and name of saved file
	 * @param string $field name of input with uploaded file
	 * @param Constraints $constraints for uploaded file
	 * @param bool $overwrite overwrite existing file
	 * @param int $mode file permissions
	 * @throws Exception
	 * @SuppressWarnings(PHPMD.Superglobals)
	 */
	public function upload($target, $field, Constraints $constraints, $overwrite = true, $mode = 0664) {
		if (!isset($_FILES[$field])) {
			throw new Exception('Form field not found (' . $field . ')', Exception::ERR_FILE_NOT_UPLOADED);
		}
		$uploadedFile = $_FILES[$field];
		$this->checkUploadedFile($uploadedFile);

		$tempName = $uploadedFile['tmp_name'];
		$fullPath = is_dir($target) ? $target . $uploadedFile['name'] : $target;

		$constraints->validate($uploadedFile);

		if (!$overwrite && is_file($fullPath)) {
			throw new Exception('File already exists', Exception::ERR_FILE_ALREADY_EXISTS);
		}
		// @codingStandardsIgnoreStart
		if ($overwrite && is_file($fullPath) && !@unlink($fullPath)) {
			// @codingStandardsIgnoreEnd
			throw new Exception('Cannot overwrite file ' . basename($fullPath), Exception::ERR_WRITE_FILE);
		}
		$this->moveUploadedFile($tempName, $fullPath, $mode);
	}

	/**
	 * check if file is really uploaded
	 * @param array $uploadedFile
	 * @throws Exception
	 */
	protected function checkUploadedFile(array $uploadedFile) {
		if ($uploadedFile['size'] === 0 || $uploadedFile['name'] === '') {
			throw new Exception('File was not uploaded', Exception::ERR_FILE_NOT_UPLOADED);
		}
		if ($this->isCli) {
			if (!is_file($uploadedFile['tmp_name'])) {
				throw new Exception('File was not uploaded', Exception::ERR_FILE_NOT_UPLOADED);
			}
		} else {
			if (!is_uploaded_file($uploadedFile['tmp_name'])) {
				throw new Exception('File was not uploaded', Exception::ERR_FILE_NOT_UPLOADED);
			}
		}
	}

	/**
	 * save uploaded file to target location
	 * @param string $tempFile
	 * @param string $targetFile
	 * @param int $mode
	 * @throws Exception
	 */
	protected function moveUploadedFile($tempFile, $targetFile, $mode) {
		if ($this->isCli) {
			if (!rename($tempFile, $targetFile)) {
				throw new Exception('Cannot write file ' . $targetFile, Exception::ERR_WRITE_FILE);
			}
		} else {
			if (!move_uploaded_file($tempFile, $targetFile)) {
				throw new Exception('Cannot write file ' . $targetFile, Exception::ERR_WRITE_FILE);
			}
		}
		@umask(0000);
		@chmod($targetFile, $mode);
	}

	/**
	 * returns file name of uploaded file or empty string if file not exists
	 * @param string $field name of form field with uploaded file
	 * @return string
	 * @SuppressWarnings(PHPMD.Superglobals)
	 */
	public function getFileName($field) {
		return (isset($_FILES[$field]['name']) ? $_FILES[$field]['name'] : 'xx');
	}
}
