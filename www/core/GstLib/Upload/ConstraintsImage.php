<?php

namespace GstLib\Upload;

use GstLib\Upload\Exception;

/**
 * constraints for uploaded image
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Upload
 */
class ConstraintsImage {

	/** @var int required image width */
	protected $height;

	/** @var int required image height */
	protected $width;

	/** @var int max image width */
	protected $maxHeight;

	/** @var int max image height */
	protected $maxWidth;

	/** @var bool allow CMYK image */
	protected $allowCmyk;

	public function __construct() {
		$this->height = 0;
		$this->width = 0;
		$this->maxHeight = 0;
		$this->maxWidth = 0;
		$this->allowCmyk = false;
	}

	/**
	 * sets required width of image
	 * @param int $width
	 * @return ConstraintsImage
	 * @throws \InvalidArgumentException
	 */
	public function allowWidth($width) {
		$value = (int)$width;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected integer greater or equals zero');
		}
		$this->width = $value;
		return $this;
	}

	/**
	 * sets required height of image
	 * @param int $height
	 * @return ConstraintsImage
	 * @throws \InvalidArgumentException
	 */
	public function allowHeight($height) {
		$value = (int)$height;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected integer greater or equals zero');
		}
		$this->height = $value;
		return $this;
	}

	/**
	 * sets allowed max width of image
	 * @param int $width
	 * @return ConstraintsImage
	 * @throws \InvalidArgumentException
	 */
	public function allowMaxWidth($width) {
		$value = (int)$width;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected integer greater or equals zero');
		}
		$this->maxWidth = $value;
		return $this;
	}

	/**
	 * sets allowed max height of image
	 * @param int $height
	 * @return ConstraintsImage
	 * @throws \InvalidArgumentException
	 */
	public function allowMaxHeight($height) {
		$value = (int)$height;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected integer greater or equals zero');
		}
		$this->maxHeight = $value;
		return $this;
	}

	/**
	 * allow CMYK image
	 * @param bool $allow
	 * @return ConstraintsImage
	 */
	public function allowCmyk($allow) {
		$this->allowCmyk = (bool)$allow;
		return $this;
	}

	/**
	 * validate uploaded image
	 * @param array $uploadFile
	 * @throws Exception
	 */
	public function validate($uploadFile) {
		// @codingStandardsIgnoreStart
		$imgInfo = @getimagesize($uploadFile['tmp_name']);
		// @codingStandardsIgnoreEnd
		if (!is_array($imgInfo)) {
			throw new Exception('Image not recognized (' . $uploadFile['name'] . ')', Exception::ERR_UNALLOWED_MIME);
		}
		$width = $imgInfo[0];
		$height = $imgInfo[1];
		$this->validateSize($width, $height);
		$this->validateMaxSize($width, $height);

		if (!$this->allowCmyk && $imgInfo['channels'] === 4) {
			throw new Exception('CMYK is not allowed (' . $uploadFile['name'] . ')', Exception::ERR_IMG_CMYK);
		}
	}

	/**
	 * validate required size of image
	 * @param array  $uploadFile
	 * @throws Exception
	 */
	protected function validateSize($width, $height) {
		if ($this->width > 0 && $width !== $this->width) {
			throw new Exception('Incorrect image width (' . $width . ')', Exception::ERR_IMG_INCORRECT_WIDTH);
		}
		if ($this->height > 0 && $height !== $this->height) {
			throw new Exception('Incorrect image height (' . $height . ')', Exception::ERR_IMG_INCORRECT_HEIGHT);
		}
	}

	/**
	 * validate max size of image
	 * @param array  $uploadFile
	 * @throws Exception
	 */
	protected function validateMaxSize($width, $height) {
		if ($this->maxWidth > 0 && $width > $this->maxWidth) {
			throw new Exception('Image width exceeds limit (' . $width . ')', Exception::ERR_IMG_INCORRECT_WIDTH);
		}
		if ($this->maxHeight > 0 && $height > $this->maxHeight) {
			throw new Exception('Image height exceeds limit (' . $height . ')', Exception::ERR_IMG_INCORRECT_HEIGHT);
		}
	}

}
