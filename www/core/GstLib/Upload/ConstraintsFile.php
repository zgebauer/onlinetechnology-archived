<?php

namespace GstLib\Upload;

use GstLib\Upload\Exception;
use GstLib\Filesystem\Utils;

/**
 * constraints for uploaded file
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Upload
 */
class ConstraintsFile {

	/** @var array allowed extensions */
	protected $allowExtensions;

	/** @var array allowed mime types */
	protected $allowMimeTypes;

	/** @var int max allowed filesize */
	protected $maxUploadSize;

	public function __construct() {
		$this->allowExtensions = [];
		$this->allowMimeTypes = [];
		$this->maxUploadSize = Utils::bytes(trim(ini_get('upload_max_filesize')));
	}

	/**
	 * sets allowed file extensions
	 * @param array $extensions
	 * @return ConstraintsFile
	 */
	public function allowExtensions(array $extensions) {
		$this->allowExtensions = array_filter(array_map('trim', $extensions));
		return $this;
	}

	/**
	 * sets allowed MIME types
	 * @param array $mimeTypes
	 * @return ConstraintsFile
	 */
	public function allowMimeTypes(array $mimeTypes) {
		$this->allowMimeTypes = array_filter(array_map('trim', $mimeTypes));
		return $this;
	}

	/**
	 * sets max allowed filesize
	 * @param int $filesize
	 * @return ConstraintsFile
	 * @throws \InvalidArgumentException
	 */
	public function allowMaxFileSize($filesize) {
		$value = (int)$filesize;
		if ($value <= 0) {
			throw new \InvalidArgumentException('expected integer greater than zero');
		}
		$this->maxUploadSize = $value;
		return $this;
	}

	/**
	 * validate uploaded file
	 * @param array $uploadFile
	 * @throws Exception
	 */
	public function validate(array $uploadFile) {
		if ($uploadFile['size'] > $this->maxUploadSize) {
			throw new Exception('File is too large (' . $uploadFile['size'] . ')', Exception::ERR_FILE_TOO_LARGE);
		}
		if (isset($this->allowMimeTypes[0]) && !in_array($uploadFile['type'], $this->allowMimeTypes)) {
			throw new Exception(
				'Unallowed MIME type of file (' . $uploadFile['type'] . ')', Exception::ERR_UNALLOWED_MIME
			);
		}

		if (isset($this->allowExtensions[0])) {
			$tmp = explode('.', $uploadFile['name']);
			$extension = strtolower(end($tmp));
			if (!in_array($extension, $this->allowExtensions)) {
				throw new Exception(
					'Unallowed extension of file (' . $extension . ')', Exception::ERR_UNALLOWED_EXTENSION
				);
			}
		}
	}

}
