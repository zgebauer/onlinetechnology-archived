<?php

namespace GstLib\Upload;

use GstLib\Upload\Exception;

/**
 * constraints for uploaded file
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Upload
 */
class Constraints {

	/** @var ConstraintsFile */
	protected $constraintsFile;

	/** @var ConstraintsImage */
	protected $constraintsImage;

	public function __construct() {
		$this->constraintsFile = new ConstraintsFile();
		$this->constraintsImage = new ConstraintsImage();
	}

	/**
	 * sets allowed file extensions
	 * @param array $extensions
	 * @return Constraints
	 */
	public function allowExtensions(array $extensions) {
		$this->constraintsFile->allowExtensions($extensions);
		return $this;
	}

	/**
	 * sets allowed MIME types
	 * @param array $mimeTypes
	 * @return Constraints
	 */
	public function allowMimeTypes(array $mimeTypes) {
		$this->constraintsFile->allowMimeTypes($mimeTypes);
		return $this;
	}

	/**
	 * sets max allowed filesize
	 * @param int $filesize
	 * @return Constraints
	 */
	public function allowMaxFileSize($filesize) {
		$this->constraintsFile->allowMaxFileSize($filesize);
		return $this;
	}

	/**
	 * sets required size of image
	 * @param int $width 0=any size
	 * @param int $height 0=any size
	 * @return Constraints
	 */
	public function allowImageSize($width, $height) {
		$this->constraintsImage
			->allowWidth($width)
			->allowHeight($height);
		return $this;
	}

	/**
	 * sets max allowed size of image
	 * @param int $width 0=any size
	 * @param int $height 0=any size
	 * @return Constraints
	 */
	public function allowMaxImageSize($width, $height) {
		$this->constraintsImage
			->allowMaxWidth($width)
			->allowMaxHeight($height);
		return $this;
	}

	/**
	 * allow CMYK image
	 * @param bool $allow
	 * @return Constraints
	 */
	public function allowCmyk($allow) {
		$this->constraintsImage->allowCmyk($allow);
		return $this;
	}

	/**
	 * check uploaded file
	 * @param array $uploadFile
	 */
	public function validate(array $uploadFile) {
		$this->validateUpload($uploadFile);
		$this->constraintsFile->validate($uploadFile);
		if (substr($uploadFile['type'], 0, 5) === 'image' || $uploadFile['type'] === 'application/x-shockwave-flash') {
			$this->constraintsImage->validate($uploadFile);
		}
	}

	/**
	 * check error code of uploaded file
	 * @param array $uploadFile
	 * @throws Exception
	 */
	protected function validateUpload(array $uploadFile) {
		if (!isset($uploadFile['error']) || $uploadFile['error'] === UPLOAD_ERR_OK) {
			return;
		}
		switch ($uploadFile['error']) {
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				throw new Exception('Uploaded file was too big', Exception::ERR_FILE_TOO_LARGE);
			case UPLOAD_ERR_PARTIAL:
				throw new Exception('File was not completely uploaded', Exception::ERR_FILE_NOT_UPLOADED);
			case UPLOAD_ERR_NO_FILE:
				throw new Exception('File was not uploaded', Exception::ERR_FILE_NOT_UPLOADED);
			case UPLOAD_ERR_CANT_WRITE:
				throw new Exception('Cannot wwrite file', Exception::ERR_WRITE_FILE);
			default:
				throw new Exception('Upload error', $uploadFile['error']);
		}
	}

}
