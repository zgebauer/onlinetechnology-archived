<?php

namespace GstLib\Upload;

/**
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage Upload
 */
class Exception extends \Exception {

	/** @var already exists file with the same name as uploaded file */
	const ERR_FILE_ALREADY_EXISTS = 1;

	/** @var file was not uploaded */
	const ERR_FILE_NOT_UPLOADED = 2;

	/** @var file was too big */
	const ERR_FILE_TOO_LARGE = 3;

	/** @var unallowed extension of uploaded file */
	const ERR_UNALLOWED_EXTENSION = 4;

	/** unallowed MIME type of uploaded file */
	const ERR_UNALLOWED_MIME = 5;

	/** @var uploaded image was too wide */
	const ERR_IMG_TOO_WIDE = 10;

	/** @var uploaded image was too high */
	const ERR_IMG_TOO_HIGH = 11;

	/** @var incorrect height of uploaded image */
	const ERR_IMG_INCORRECT_HEIGHT = 12;

	/** @var incorrect width of uploaded image */
	const ERR_IMG_INCORRECT_WIDTH = 13;

	/** @var CMYK JPG not allowed */
	const ERR_IMG_CMYK = 14;

	/** @var cannot write */
	const ERR_WRITE_FILE = 20;

}
