<?php

namespace GstLib\Cnb;

class Rate {

    /** @var string */
    private $countryName;

    /** @var string */
    private $currencyName;

    /** @var int */
    private $quantity;

    /** @var string */
    private $currencyCode;

    /** @var float */
    private $rate;

    /**
     * @param string $countryName
     * @param string $currencyName
     * @param float $quantity
     * @param string $currencyCode
     * @param float $rate
     */
    public function __construct($countryName, $currencyName, $quantity, $currencyCode, $rate) {
        $this->countryName = $countryName;
        $this->currencyName = $currencyName;
        $this->quantity = (int) $quantity;
        $this->currencyCode = $currencyCode;
        $this->rate = (float) $rate;
    }

    /**
     * @return string
     */
    public function getCountryName() {
        return $this->countryName;
    }

    /**
     * @return string
     */
    public function getCurrencyName() {
        return $this->currencyName;
    }

    /**
     * @return int
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getCurrencyCode() {
        return $this->currencyCode;
    }

    /**
     * @return float
     */
    public function getRate() {
        return $this->rate;
    }

}
