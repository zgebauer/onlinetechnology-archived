<?php

namespace GstLib\Cnb;

class Rates {

    /** @var string */
    private static $url = 'http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt';

    /** @var array */
    private $cache;

    public function __construct() {
        $this->cache = array();
    }

    /**
     * @param string $currencyCode
     * @param \DateTime $date
     * @return Rate
     * @throws Exception
     */
    public function getRate($currencyCode, \DateTime $date = null) {
        if (is_null($date)) {
            $date = new \DateTime('now', new \DateTimeZone('UTC'));
        }
        $datestring = $date->format('Y-m-d');

        if (!isset($this->cache[$datestring])) {
            $this->loadRates($date);
        }
        if (isset($this->cache[$datestring][$currencyCode])) {
            return $this->cache[$datestring][$currencyCode];
        }
        throw new Exception('not found exchange rate for ' . $currencyCode . ' on ' . $datestring);
    }

    /**
     * @param string $currencyCode
     * @param float $amount
     * @param \DateTime $date
     * @return float
     */
    public function calcToCzk($currencyCode, $amount, \DateTime $date = null) {
        $rate = $this->getRate($currencyCode, $date);
        return $amount * $rate->getRate() / $rate->getQuantity();
    }

    /**
     * @param stgring $currencyCode
     * @param float $amountCzk
     * @param \DateTime $date
     * @return float
     */
    public function calcToForeign($currencyCode, $amountCzk, \DateTime $date = null) {
        $rate = $this->getRate($currencyCode, $date);
        return $amountCzk / $rate->getRate() * $rate->getQuantity();
    }

    private function loadRates(\DateTime $date) {
        $now = new \DateTime('now');
        $url = self::$url
                . ($now->format('Y-m-d') === $date->format('Y-m-d') ? '' : '?date=' . $date->format('d.m.Y'));

        $lines = file($url, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        if (!is_array($lines)) {
            return;
        }

        $lines = array_slice($lines, 2); // skip header
        foreach ($lines as $line) {
            $values = explode('|', $line);
            if (isset($values[4])) {
                $values = array_map('trim', $values);
                $values[4] = str_replace(',', '.', $values[4]);
                $this->cache[$date->format('Y-m-d')][$values[3]]
                    = new Rate($values[0], $values[1], $values[2], $values[3], $values[4]);
            }
        }
    }

}
