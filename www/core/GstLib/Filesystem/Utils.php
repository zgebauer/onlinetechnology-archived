<?php

namespace GstLib\Filesystem;

class Utils {

	/**
	 * returns extension of file
	 * @param string $file filename
	 * @return string
	 */
	public static function getFileExtension($file) {
		if (strpos($file, '.') === false) {
			return '';
		}
		$tmp = explode('.', $file);
		return end($tmp);
	}

	/**
	 * returns a human readable filesize with binary or decimal sufix. Doesnt work correctly for filesize > 2GiB
	 * @param int $filesize
	 * @param bool $decimalSufix TRUE|FALSE - return decimal|binary sufix
	 * @param int $decimals number of digits after decimal point
	 * @param string $decPoint string representing decimal point
	 * @param string $thousandSeparator string representing thousands separator
	 * @return string
	 */
	public static function humanFilesize($filesize, $decimalSufix = false, $decimals = 2, $decPoint = '.',
			$thousandSeparator = '') {
		$filesize = ($filesize < 0 ? 0 : $filesize);

		$prefixes = ['B', 'KiB', 'MiB', 'GiB'];
		$divider = 1024;
		if ($decimalSufix) {
			$prefixes = ['B', 'kB', 'MB', 'GB'];
			$divider = 1000;
		}

		$iter = 0;
		while (($filesize / $divider) >= 1) {
			$filesize = $filesize / $divider;
			$iter++;
		}

		$ret = number_format($filesize, $decimals, $decPoint, $thousandSeparator);
		return $ret . ' ' . $prefixes[$iter];
	}

	/**
	 * return mime of file by extension. For unknown extensions returns 'application/octet-stream'
	 * @param string $fileName
	 * @return string
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public static function getMime($fileName) {
		$tmp = explode('.', $fileName);
		$extension = strtolower(end($tmp));

		static $mimeTypes = [
			'ogg' => 'application/ogg',
			'pdf' => 'application/pdf',
			'rtf' => 'application/rtf',
			'zip' => 'application/zip',
			'ai' => 'application/postscript', 'eps' => 'application/postscript', 'ps' => 'application/postscript',
			'xls' => 'application/vnd.ms-excel',
			'ppt' => 'application/vnd.ms-powerpoint',
			'sxw' => 'application/vnd.sun.xml.writer',
			'sxc' => 'application/vnd.sun.xml.calc',
			'torrent' => 'application/x-bittorrent',
			'gz' => 'application/x-gzip', 'tgz' => 'application/x-gzip',
			'js' => 'application/x-javascript',
			'swf' => 'application/x-shockwave-flash',
			'rpm' => 'application/x-rpm',
			'xhtml' => 'application/xhtml+xml', 'xht' => 'application/xhtml+xml',
			'mid' => 'audio/midi', 'midi' => 'audio/midi',
			'mp2' => 'audio/mpeg', 'mp3' => 'audio/mpeg',
			'ram' => 'audio/x-pn-realaudio', 'rm' => 'audio/x-pn-realaudio',
			'ra' => 'audio/x-realaudio',
			'wav' => 'audio/x-wav',
			'bmp' => 'image/bmp',
			'gif' => 'image/gif',
			'jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'jpe' => 'image/jpeg',
			'png' => 'image/png',
			'tiff' => 'image/tiff', 'tif' => 'image/tiff',
			'wmf' => 'image/wmf',
			'wrl' => 'model/vrml', 'vrml' => 'model/vrml',
			'css' => 'text/css',
			'html' => 'text/html', 'htm' => 'text/html',
			'xml' => 'text/xml', 'xsl' => 'text/xml',
			'txt' => 'text/plain', 'asc' => 'text/plain',
			'mpg' => 'video/mpeg', 'mpeg' => 'video/mpeg', 'mpe' => 'video/mpeg',
			'mov' => 'video/quicktime', 'qt' => 'video/quicktime',
			'avi' => 'video/x-msvideo',
			'wmv' => 'video/x-ms-wmv'
		];
		return (isset($mimeTypes[$extension]) ? $mimeTypes[$extension] : 'application/octet-stream');
	}

	/**
	 * converts filesize with suffix 'G', 'M' or 'K' to bytes
	 * @param string $size
	 * @return int
	 */
	public static function bytes($size) {
		$size = trim($size);
		$lastChar = strtoupper(substr($size, -1));
		if (in_array($lastChar, ['G', 'M', 'K'])) {
			$bytes = (int)(substr($size, 0, strlen($size) - 1));
			// The 'G' modifier is available since PHP 5.1.0
			if ($lastChar === 'G') {
				$bytes = $bytes * 1024 * 1024 * 1024;
			}
			if ($lastChar === 'M') {
				$bytes = $bytes * 1024 * 1024;
			}
			if ($lastChar === 'K') {
				$bytes = $bytes * 1024;
			}
		} else {
			$bytes = (int)$size;
		}
		return $bytes;
	}

	/**
	 * @param string $source source file
	 * @param string $target target file
	 */
	public static function gZipFile($source, $target) {
		$fileHandler = fopen($source, 'r');
		$data = fread($fileHandler, filesize($source));
		fclose($fileHandler);

		$zipFileHandler = gzopen($target, 'wb9');
		gzwrite($zipFileHandler, $data);
		gzclose($zipFileHandler);
	}

}
