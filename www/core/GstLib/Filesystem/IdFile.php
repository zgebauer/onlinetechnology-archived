<?php

namespace GstLib\Filesystem;

use \GstLib\Html\Url;

/**
 * library of functions for manipulating with files assigned to some instance with numeric identifier.
 * Assumes files with names in format id_<filename>.<extension>
 * stored in 'base_dir' and subdirectories. Names of subdirectories are created from id of file.
 * For example:
 * /some_dir/images/1_image.jpg<br />
 * /some_dir/images/1/1234_another-image.gif<br />
 * Number of files in each subdir can be set from unlimited or 10, 100 or 100 files in directory.
 */
class IdFile {

	/** @var unknown number of files per directory */
	const FILES_PER_DIR_UNKNOWN = -1;

	/** @var all files in base directory */
	const FILES_PER_DIR_UNLIMITED = 0;

	/** @var max 10 files per directory */
	const FILES_PER_DIR_10 = 1;

	/** @var max 100 files per directory */
	const FILES_PER_DIR_100 = 2;

	/** @var max 1000 files per directory */
	const FILES_PER_DIR_1000 = 3;

	/**
	 * returns relative directory path created from id by number of files per directory
	 * @param int $fileId
	 * @param int $filesPerDir 0|1|2|3 = files per dir:unlimited|10|100|100
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public static function idToDir($fileId, $filesPerDir = self::FILES_PER_DIR_UNLIMITED) {
		$fileId = (int)$fileId;
		if ($fileId <= 0) {
			throw new \InvalidArgumentException('expected id greater than zero');
		}
		if ($filesPerDir === self::FILES_PER_DIR_UNLIMITED) {
			return '';
		}

		$allow = [self::FILES_PER_DIR_10, self::FILES_PER_DIR_100, self::FILES_PER_DIR_1000];
		if (!in_array($filesPerDir, $allow)) {
			throw new \InvalidArgumentException('unallowed value files per directory:' . $filesPerDir);
		}

		$tmp = [];
		$strId = strval($fileId);
		$files = self::FILES_PER_DIR_UNLIMITED;
		if (in_array($filesPerDir, $allow)) {
			$files = $filesPerDir;
		}

		$length = strlen($fileId) - $files;
		for ($x = 0; $x < $length; $x++) {
			$tmp[] = $strId{$x};
		}
		$dir = join('/', $tmp);
		return $dir . ($dir === '' ? '' : '/');
	}

	/**
	 * delete file(s) specified by $fileId
	 *
	 * $ignoreFileName - name of file that do not be deleted, even if id of file match given $fileId.
	 * Parameter $filesPerDir should be FILES_PER_DIR* value, default value is
	 * FILES_PER_DIR_UNLIMITED. Specifies subdirectories where file should be
	 * stored. Special value FILES_PER_DIR_UNKNOWN means 'search in basedir and
	 * all possible subdirs' (slower, use with caution).
	 *
	 * @param int $fileId
	 * @param string $baseDir
	 * @param string $ignoreFileName
	 * @param int $filesPerDir
	 * @return bool FALSE on error
	 */
	public static function delete($fileId, $baseDir, $ignoreFileName = '', $filesPerDir = self::FILES_PER_DIR_UNLIMITED) {
		$options = [self::FILES_PER_DIR_UNLIMITED];
		if (in_array($filesPerDir, self::allowedFilesPerDir())) {
			$options = [$filesPerDir];
		} else {
			if ($filesPerDir === self::FILES_PER_DIR_UNKNOWN) {
				$options = self::allowedFilesPerDir();
			}
		}

		foreach ($options as $option) {
			$dir = $baseDir . self::idToDir($fileId, $option);
			$files = glob($dir . self::fileId($fileId) . '*');
			$files = ($files === false ? [] : $files); // ignore errors
			foreach ($files as $fullPath) {
				if ($ignoreFileName !== '' && basename($fullPath) === $ignoreFileName) {
					continue;
				}
				if (!unlink($fullPath)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * returns full filename with specified id
	 *
	 * Returns empty string if file is not found.
	 * Parameter $eFilesPerDir should be FILES_PER_DIR* value, default value is
	 * FILES_PER_DIR_UNLIMITED. Specifies subdirectories where file should be
	 * stored. Special value FILES_PER_DIR_UNKNOWN means 'search in basedir and
	 * all possible subdirs' (slower, use with caution).
	 *
	 * @param int $fileId id of file
	 * @param string $baseDir base directory where files are stored
	 * @param enum|int $filesPerDir
	 * @return string
	 */
	public static function getFullPath($fileId, $baseDir, $filesPerDir = self::FILES_PER_DIR_UNLIMITED) {
		$options = [self::FILES_PER_DIR_UNLIMITED];
		if (in_array($filesPerDir, self::allowedFilesPerDir())) {
			$options = [$filesPerDir];
		} else {
			if ($filesPerDir == self::FILES_PER_DIR_UNKNOWN) {
				$options = self::allowedFilesPerDir();
			}
		}
		foreach ($options as $option) {
			$files = glob($baseDir . self::idToDir($fileId, $option) . self::fileId($fileId) . '*');
			if (isset($files[0])) {
				return $files[0];
			}
		}
		return '';
	}

	/**
	 * create new filename from specified id and original filename
	 * @param int $fileId
	 * @param string $fileName
	 * @return string
	 */
	public static function createFileName($fileId, $fileName) {
		$parts = explode('.', $fileName);
		if (isset($parts[1])) {
			if (strtolower(substr($fileName, -7)) === '.tar.gz') {
				return self::fileId($fileId) . Url::fixForUrl(join('.', array_slice($parts, 0, -2))) . substr($fileName, -7);
			}
			return self::fileId($fileId) . Url::fixForUrl(join('.', array_slice($parts, 0, -1))) . '.' . end($parts);
		}
		return self::fileId($fileId) . Url::fixForUrl($fileName);
	}

	/**
	 * returns filename without id prefix (assume filename in format id_filename.ext)
	 * @param string $fileName
	 * @return string
	 */
	public static function stripId($fileName) {
		$pos = strpos($fileName, '_');
		$pos = ($pos === false ? 0 : $pos + 1);
		return substr($fileName, $pos);
	}

	/**
	 * returns with identifier, filename have to start with this string
	 * @param int $fileId
	 * @return string
	 */
	private static function fileId($fileId) {
		return $fileId . '_';
	}

	private static function allowedFilesPerDir() {
		return [
			self::FILES_PER_DIR_1000,
			self::FILES_PER_DIR_100,
			self::FILES_PER_DIR_10,
			self::FILES_PER_DIR_UNLIMITED
		];
	}

}
