<?php
/**
 * @package GstLib
 */

namespace GstLib;

require_once __DIR__.'/Html.php';

/**
 * simple template, create output based on template
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   GstLib
 * @property  bool $checkSub true=if subtemplate not exists, raise error
 */
class Template
{
    /** @var string left delimiter for 'subtemplate pseudotags' */
    protected static $_leftDelimiter = '<!-- <';
    /** @var string right delimiter for 'subtemplate pseudotags' */
    protected static $_rightDelimiter = '> -->';
    /** @var string left delimiter for 'normal pseudotags' */
    protected static $_leftTxtDelimiter = '[%';
    /** @var string right delimiter for 'normal pseudotags' */
    protected static $_rightTxtDelimiter = '%]';
    /** @var string part of subtemplate description (for example: SUB_PART) */
    protected static $_subTag = 'SUB_';
    /** @var string content of template */
    private $_content;

    /**
     * create instance and fill it from given file
     * @param string $fullPath fill content from this file
     * @param bool $stripHtmlHeader true=read only part between tags <body>, false=read entire file
     */
    public function __construct($fullPath = '', $stripHtmlHeader = false)
    {
        $this->_content = '';

        if ($fullPath != '') {
            $this->readFile($fullPath, $stripHtmlHeader);
        }
    }

    /**
     * remove entire subtemplate (for example <!-- <SUB_PART> -->content<!-- </SUB_PART> -->)
     * @param string $tag subtemplate description without delimiters
     */
    public function delSub($tag)
    {
        $this->replaceSub($tag, '');
    }

    /**
     * replace subtemplate (for example <!-- <SUB_PART> -->) and inner
     * pseudotags (for example [%TXT_VALUE%], [%TXT_OTHER_VALUE%]) with given array of values.
     * Without parameter $map, method replace pseudotags with values in same order.
     * Optional parameter $map can rewrite order, have to contains
     * associative array with pseudotags and index of value in row
     * (for example 'TXT_VALUE'=>1, 'TXT_OTHER_VALUE'=>0)
     *
     * @param string $tagSubtemplate subtemplate description without delimiters
     * @param array $values two-dimensional array of values
     * @param array $map associative array, template tags as indices
     */
    public function processSub($tagSubtemplate, array $values, array $map = array())
    {
        $cnt = '';
        $tplRow = new self();
        $rowTemplate = $this->getSub($tagSubtemplate);
        if (empty($map)) {
            preg_match_all('/\[%(.*)%\]/U', $rowTemplate, $map, PREG_PATTERN_ORDER);
            $map = array_flip($map[1]);
        }

        foreach ($values as $row) {
            $tplRow->set($rowTemplate);
            foreach ($map as $tag=>$index) {
                if (!isset($row[$index])) {
                    user_error('undefined index of values '.$index.' ('.$tag.')', E_USER_NOTICE);
                    // @codeCoverageIgnoreStart
                    continue;
                    // @codeCoverageIgnoreEnd
                }
                $tplRow->replace($tag, $row[$index]);
            }
            $cnt .= $tplRow->get();
        }
        unset($tplRow);
        $this->replaceSub($tagSubtemplate, $cnt);
    }

    /**
     * fill template from file
     * @param string $fullPath fill content from this file
     * @param bool $stripHtmlHeader true=read only part between tags <body>, false=read entire file
     */
    public function readFile($fullPath, $stripHtmlHeader = false)
    {
        if (!is_file($fullPath)) {
            user_error('file '.$fullPath.' not found', E_USER_WARNING);
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }
        $this->_content = file_get_contents($fullPath);
        if ($this->_content === false) {
            // @codeCoverageIgnoreStart
            $this->_content = '';
            user_error('cannot read file '.$fullPath, E_USER_WARNING);
            return;
            // @codeCoverageIgnoreEnd
        }

        if (get_magic_quotes_runtime() == 1) {
            // @codeCoverageIgnoreStart
            $this->_content = stripslashes($this->_content);
        }
        // @codeCoverageIgnoreEnd

        if ($stripHtmlHeader) {
            $this->_content = preg_replace('/^(.*)<body>/sm', '', $this->_content);
            $this->_content = preg_replace('/<\/body>(.*)$/sm', '', $this->_content);
        }
    }

    /**
     * replace subtemplate (for example <!-- <SUB_PART> -->) with given value
     * @param string $tag subtemplate tag without delimiters
     * @param string $value
     */
    public function replaceSub($tag,$value)
    {
        $this->_content =  preg_replace(self::_getRegexp($tag), $value, $this->_content);
    }

    /**
     * strip or delete subtemplate
     * @param string $tag
     * @param bool $condition true|false = strip|delete subtemplate
     */
    public function showSub($tag, $condition)
    {
        if ($condition) {
            $this->stripSub($tag);
        } else {
            $this->delSub($tag);
        }
    }

    /**
     * replace subtemplate (for example <!-- <SUB_PART> -->) with content of subtemplate itself
     * @param string $tag subtemplate tag without delimiters
     */
    public function stripSub($tag)
    {
        $this->replaceSub($tag, $this->getSub($tag));
    }

    /**
     * print content to standard output
     * @param bool $addCacheHeader true = add headers for disable cache
     */
    public function display($addCacheHeader = true)
    {
        if ($addCacheHeader && !headers_sent()) {
            // @codeCoverageIgnoreStart
            header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
            header('Last-Modified: '.gmdate("D, d M Y H:i:s").' GMT');
            header('Cache-Control: no-store, no-cache, must-revalidate');
            header('Cache-Control: post-check=0, pre-check=0', false);
        }
        // @codeCoverageIgnoreEnd
        echo $this->_content;
    }

    /**
     * replace normal pseudotag (for example [%TXT_VALUE%]) with specified value
     * @param string $tag normal pseudotag without delimiters
     * @param string $value
     * @param bool $quiet true = do not raise eror on missing tag
     */
    public function replace($tag, $value, $options = array())
    {
        if ($options === 'raw') {
            $options = array('output' => 'raw');
        }
        if (!isset($options['output'])) {
            $options['output'] = 'html';
        }
        if (!isset($options['quiet'])) {
            $options['quiet'] = false;
        }

        $pseudoTag = self::$_leftTxtDelimiter.$tag.self::$_rightTxtDelimiter;
        $count = 0;
        if ($options['output'] === 'html') {
            $value = Html::escape($value);
        }
        $this->_content = str_replace($pseudoTag, $value, $this->_content, $count);
        if (!$options['quiet'] && $count == 0) {
            user_error('pseudotag '.$tag.' not found', E_USER_NOTICE);
            // @codeCoverageIgnoreStart
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * fill template with specified value
     * @param string $value content of template
     */
    public function set($value)
    {
        $this->_content = trim($value);
    }

    /**
     * returns content of template
     * @return string
     */
    public function get()
    {
        return $this->_content;
    }

    /**
     * returns subtemplate of current template, empty string if not found
     * @param string $tag subtemplate tag without delimiters
     * @return string
     */
    public function getSub($tag)
    {
        $ret = array();
        if (preg_match($this->_getRegexp($tag), $this->_content, $ret)) {
            return $ret[1]; // middle part
        } else {
            return '';
        }
    }

    /**
     * @param string $tag subtemplate description without delimiters
     * @return string regular expression for subtemplate
     */
    private static function _getRegexp($tag)
    {
        return '/'.self::$_leftDelimiter.self::$_subTag.$tag.
            self::$_rightDelimiter.'(.*)'.self::$_leftDelimiter.'\/'.
            self::$_subTag.$tag.self::$_rightDelimiter.'/sm';  // any char, multiline
    }

}