<?php
/**
 * @package GstLib
 */

namespace GstLib;

/**
 * manipulate date in format ISO-8601, ie: 2006-12-11T10:47Z
 *
 * @version   0.0.1
 * @since     11.12.2006
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @copyright 2006 GST
 * @package   GstLib
 */
class Date
{
    /** @var string recognized as empty date */
    const EMPTY_DATE = '0000-00-00';
    /** @var string recognized as empty datetime */
    const EMPTY_DATETIME = '0000-00-00T00:00:00';

    /**
     * function like a php native function getdate()
     * returns associative array with indices 'seconds','minutes','hours',
     * 'mday','mon','year'
     * returns false if date is not valid
     *
     * @param date $datetime date in format ISO8601
     * @return array false if $datetime is not recognized as valid date
     */
    public static function getDateFromIso($datetime)
    {
        if (!self::isIsoDate($datetime)) {
            return false;
        }

        $date = '';
        $time = '';
        $zone = '';
        $year = 0;
        $mon = 1;
        $mday = 1;
        $hours = 0;
        $minutes = 0;
        $seconds = 0;

        $tmp = explode('T', $datetime);
        if (isset($tmp[0])) {
            $date = $tmp[0];
        }
        if (isset($tmp[1])) {
            $time = $tmp[1];
            $tmp = explode('Z', $time);
            if (isset($tmp[1])) {
                $zone = $tmp[1];
                $time = $tmp[0];
            }
        }

        if ($date != '') {
            $tmp = explode('-', $date);
            if (isset($tmp[0])) {
                $year = intval($tmp[0]);
            }
            if (isset($tmp[1])) {
                $mon = intval($tmp[1]);
            }
            if (isset($tmp[2])) {
                $mday = intval($tmp[2]);
            }
        }
        if ($time != '') {
            $tmp = explode(':', $time);
            if (isset($tmp[0])) {
                $hours = intval($tmp[0]);
            }
            if (isset($tmp[1])) {
                $minutes = intval($tmp[1]);
            }
            if (isset($tmp[2])) {
                $seconds = intval($tmp[2]);
            }
        }

        return array(
            'seconds'=>$seconds,
            'minutes'=>$minutes,
            'hours'=>$hours,
            'mday'=>$mday,
            'mon'=>$mon,
            'year'=>$year
       );
    }

    /**
     * validate given ISO-8601 datetime (Y-m-dTH:i:s)
     * Date part is required, time is not required.
     *
     * @param date $datetime
     * @return bool false if date is not valid
     */
    public static function isIsoDate($datetime)
    {
        $parts = array();
        $regexp = '/^(\d{4})-(\d{2})-(\d{2})[T]{0,1}';
        $regexp .= '(\d{0,2})[:]{0,1}(\d{0,2})[:]{0,1}(\d{0,2})[Z|+]{0,1}';
        $regexp .= '(\d{0,2})[:]{0,1}(\d{0,2})$/';
        if (!preg_match($regexp, $datetime, $parts)) {
            return false;
        }
        if (intval($parts[2]) != 0 || intval($parts[3]) != 0 ||
                intval($parts[1]) != 0) {
            if (!checkdate(
                intval($parts[2]), intval($parts[3]), intval($parts[1])
            )) {
                return false;
            }
        }
        if (intval($parts[4]) < 0 || intval($parts[4]) > 23) {
            return false;
        }
        if (intval($parts[5]) < 0 || intval($parts[5]) > 59) {
            return false;
        }
        if (intval($parts[6]) < 0 || intval($parts[6]) > 59) {
            return false;
        }
        return true;
    }

    /**
     * return difference between dates in specified units
     *
     * Specified dates should be in ISO 8601 format, unit can be:<br />
     * m - number of full months (30 days/month)<br />
     * d - number of full days<br />
     * h - number of full hours<br />
     * i - number of full minutes<br />
     * s - number of full seconds (default)<br />
     *
     * @param date $dateFrom start date (the sooner one)
     * @param date $dateTo end date
     * @param enum $unit
     * @return int
     */
    public static function dateDiff($dateFrom, $dateTo, $unit = 's')
    {
        // convert to seconds
        $diff = strtotime($dateTo) - strtotime($dateFrom);
        switch($unit) {
            case 'm': // months
                return floor($diff/(30*86400));
            case 'd': // days
                return floor($diff/86400);
            case 'h': // hours
                return floor($diff/3600);
            case 'i': // minutes
                return floor($diff/60);
            default:
                break;
        }
        return $diff;
    }

    /**
     * converts date and/or time from ISO format to specified format. For invalid date returns empty string
     *
     * @param date $datetime
     * @param string $format
     * @return string
     */
    public static function formatDateTime($datetime, $format = 'j. n. Y')
    {
        $errMsg = 'invalid date '.$datetime;
        $datetime = trim($datetime);
        $empty = array('', '0000-00-00T00:00:00', '0000-00-00', '0000-00-00T00:00');
        if (in_array($datetime, $empty)) {
            return '';
        }

        $parts = array();
        $regexp = '/([0-9]{4})-([0-9]{2})-([0-9]{2})T';
        $regexp .= '([0-9]{2}):([0-9]{2}):([0-9]{2})/';
        if (preg_match($regexp, $datetime, $parts)) {
            $year = intval($parts[1]);
            $month = intval($parts[2]);
            $day = intval($parts[3]);
            $hour = intval($parts[4]);
            $min = intval($parts[5]);
            $sec = intval($parts[6]);
        } else { // try only date
            if (preg_match(
                '/([0-9]{4})-([0-9]{2})-([0-9]{2})/', $datetime, $parts
            )) {
                $year = intval($parts[1]);
                $month = intval($parts[2]);
                $day = intval($parts[3]);
                $hour = 0;
                $min = 0;
                $sec = 0;
            } else {
                user_error($errMsg, E_USER_NOTICE);
                return '';
            }
        }

        if (!checkdate($month, $day, $year)) {
            user_error($errMsg, E_USER_NOTICE);
            return '';
        }
        if ($hour < 0 || $hour > 23) {
            user_error($errMsg, E_USER_NOTICE);
            return '';
        }
        if ($min < 0 || $min > 59) {
            user_error($errMsg, E_USER_NOTICE);
            return '';
        }
        if ($sec < 0 || $sec > 59) {
            user_error($errMsg, E_USER_NOTICE);
            return '';
        }

        switch($format) {
            case 'm.d.Y':
                return str_pad($month, 2, '0', STR_PAD_LEFT).'.'.
                    str_pad($day, 2, '0', STR_PAD_LEFT).'.'.$year;
            case 'Y/m/d':
                return $year.'/'.str_pad($month, 2, '0', STR_PAD_LEFT).'/'.
                    str_pad($day, 2, '0', STR_PAD_LEFT);
            case 'j. n. Y H:i':
            case 'cz-datetime':
                return $day.'. '.$month.'. '.$year.' '.
                    str_pad($hour, 2, '0', STR_PAD_LEFT).':'.
                    str_pad($min, 2, '0', STR_PAD_LEFT);
            case 'time':
                return str_pad($hour, 2, '0', STR_PAD_LEFT).':'.
                    str_pad($min, 2, '0', STR_PAD_LEFT);
            case 'cz-datetimesec':
                return $day.'. '.$month.'. '.$year.' '.
                    str_pad($hour, 2, '0', STR_PAD_LEFT).':'.
                    str_pad($min, 2, '0', STR_PAD_LEFT).':'.
                    str_pad($sec, 2, '0', STR_PAD_LEFT);
            case 'timestamp':
                return mktime($hour, $min, $sec, $month, $day, $year);
            case 'rss':
                return date(
                    'D, d M Y H:i:s O',
                    mktime($hour, $min, $sec, $month, $day, $year)
                );
            case 'atom':
                return date(
                    DATE_ATOM,
                    mktime($hour, $min, $sec, $month, $day, $year)
                );
            default:
                break;
        }
        return $day.'. '.$month.'. '.$year;
    }

    /**
     * converts date from human readable format to ISO format yyyy-mm-ddThh:mm:ss
     * @param date $date date
     * @param bool $withTime true = return date and time
     * @param bool $addCurrentTime
     * @return date false if $date is not recognized as valid date
     */
    public static function dateTime2Iso($date, $withTime = false, $addCurrentTime = false)
    {
        $date = str_replace('. ', '.', trim($date));

        if ($date == '') {
            return ($withTime ? self::EMPTY_DATETIME : self::EMPTY_DATE);
        }
        if ($date == self::EMPTY_DATE) {
            return self::EMPTY_DATE;
        }

        $hour = ($addCurrentTime ? date('H') : 0);
        $minutes = ($addCurrentTime ? date('i') : 0);
        $seconds = ($addCurrentTime ? date('s') : 0);

        $tmp = explode(' ', $date, 2);
        $tmpDate = array();
        // date in Y-m-d
        if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $tmp[0], $tmpDate)) {
            $day = $tmpDate[3];
            $month = $tmpDate[2];
            $year = $tmpDate[1];
        }

        // date in m.d.Y
        if (preg_match('/^(\d{1,2})[.,-](\d{1,2})[.,-](\d{2,4})$/', $tmp[0], $tmpDate)) {
            $day = $tmpDate[1];
            $month = $tmpDate[2];
            $year = $tmpDate[3];
            if (strlen($year) == 2) {
                $year = (intval($year) > 50 ? '19' : '20').$year;
            }
        }

        if ($withTime && isset($tmp[1])) {
            if (preg_match('/^(\d{1,2}):(\d{1,2})[:]+(\d{0,2})$/', $tmp[1], $tmpDate)) {
                $hour = $tmpDate[1];
                $minutes = $tmpDate[2];
                $seconds = $tmpDate[3];
            } elseif (preg_match('/^(\d{1,2}):(\d{1,2})$/', $tmp[1], $tmpDate)) {
                $hour = $tmpDate[1];
                $minutes = $tmpDate[2];
            }
        }
        if (!isset($day) || !isset($month) || !isset($year)) {
            return false;
        }

        if (!checkdate($month, $day, $year)) {
            return false;
        }
        // check time
        if ($hour < 0 || $hour > 23) {
            return false;
        }
        if ($minutes < 0 || $minutes > 59) {
            return false;
        }
        if ($seconds < 0 || $seconds > 59) {
            return false;
        }

        // add leading zeros
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $day = str_pad($day, 2, '0', STR_PAD_LEFT);
        $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
        $minutes = str_pad($minutes, 2, '0', STR_PAD_LEFT);
        $seconds = str_pad($seconds, 2, '0', STR_PAD_LEFT);

        if ($withTime) {
            return $year.'-'.$month.'-'.$day.'T'.$hour.':'.$minutes.':'.$seconds;
        }
        return $year.'-'.$month.'-'.$day;
    }

    /**
     * convert unix timestamp to date in ISO format yyyy-mm-ddThh:mm:ss
     * @param int $timestamp
     * @param bool $with time true|false=return datetime|date
     * @return date
     */
    public static function unix2Iso($timestamp, $withTime = false)
    {
        $ret = date('c', $timestamp);
        if ($withTime) {
            return substr($ret, 0, 19);
        }
        return substr($ret, 0, 10);
    }

    /**
     * returns current datetime in ISO format
     * @return date
     */
    public static function currentDate()
    {
        return date('Y-m-d');
    }

    /**
     * returns current datetime in ISO format
     * @return date
     */
    public static function currentDateTime()
    {
        return date('Y-m-d\TH:i:s');
    }

    /**
     * return array of yers and months in range
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public static function getMonthsRange($startDate, $endDate)
    {
        $time1 = strtotime($startDate);//absolute date comparison needs to be done here
        $time2 = strtotime($endDate);
        $year1 = date('Y', $time1);
        $year2 = date('Y', $time2);
        $years = range($year1, $year2);

        foreach ($years as $year) {
            $months[$year] = array();
            while ($time1 < $time2) {
                if (date('Y', $time1) == $year) {
                    $months[$year][] = date('m', $time1);
                    $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
                } else {
                    break;
                }
            }
            continue;
        }
        return $months;
    }

}