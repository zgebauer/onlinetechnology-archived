<?php
/**
 * @package GstLib
 */

namespace GstLib;

/**
 * Store pieces of text to files in cache directory.
 *
 * @author	  Zdenek Gebauer <zdenek.gebauer@centrum.cz>
 * @package	  GstLib
 */
class FileCache
{
    /** @var string directory cache */
    protected $_cacheDir = null;

    /**
     * sets cache directory to dir
     * @param string $dir
     * @return bool false on error
     */
    public function __construct($dir)
    {
        if (!is_dir($dir)) {
            user_error($dir.' is not a directory', E_USER_WARNING);
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
        // on windows this function incorrectly returns false
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
            // @codeCoverageIgnoreStart
            if (!is_writable($dir)) {
                user_error($dir.' is not writeable', E_USER_WARNING);
                return false;
            }
        }
        // @codeCoverageIgnoreEnd
        $this->_cacheDir = $dir;

        // @codeCoverageIgnoreStart
        if (rand(0, 100) < 2) {
            $this->emptyCache(array('modified'=>86400));
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * delete cached content by given token
     * @param string $token token (identificator of cached content)
     * @return bool false on error
     */
    public function delete($token)
    {
        $token = strtolower($token);
        if (!self::_checkToken($token)) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
        $fullpath = $this->_getFullPath($token);
        if (!is_file($fullpath)) {
            return true;
        }
        return @unlink($fullpath);
    }

    /**
     * empty cache directory
     * Paramater $config can have indices:
     * 'pattern' - left part of filename to delete, if empty delete all files
     * 'tmp' - [true|false] delete [only *.tmp|all extensions]
     * 'modified' - timeout of last modified time in seconds
     *
     * @param array $config
     * @return bool false on error
     */
    public function emptyCache(array $config = array())
    {
//        if ($self::$_cacheDir == '') {
//            user_error('cache directory is not set yet', E_USER_WARNING);
//            // @codeCoverageIgnoreStart
//            return false;
//            // @codeCoverageIgnoreEnd
//        }

        $pattern = (isset($config['pattern']) ? (string) $config['pattern'] : '');
        $length  = strlen($pattern);
        $tmpOnly = (isset($config['tmp']) ? (bool) $config['tmp'] : false);
        $modified = (isset($config['modified']) ? (int) $config['modified'] : 0);

        $glob = $this->_cacheDir.$pattern.($tmpOnly ? '*.tmp' : '*');
        $files = glob($glob);
        if (is_array($files)) {
            foreach (glob($glob) as $file) {
                if (!is_file($file) || $file === '.htaccess') {
                    continue;
                }
                if ($modified > 0 && filemtime($file) > (time() - $modified)) {
                    continue;
                }
                if ($pattern != '' && substr(basename($file), 0, $length) != $pattern) {
                    continue;
                }
                unlink($file);
            }
        }
        return true;
    }

    /**
     * store given content to cache by token, return false on error
     * @param string $token token (identificator of cached content)
     * @param string $content text to store
     * @return bool false on error
     */
    public function store($token, $content)
    {
        $token = strtolower($token);
        if (!$this->_checkToken($token)) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
        $fullpath = $this->_getFullPath($token);
        if (file_put_contents($fullpath, $content, LOCK_EX) === false) {
            user_error('cannot write cache '.$fullpath, E_USER_WARNING);
            return false;
        }
        return true;
    }

    /**
     * read cached content by given token
     * If cached content not found or is older than given timeout, returns
     * empty string.
     * Timeout 0 disable check time
     *
     * @param string $token token (identificator of cached content)
     * @param int $timeout expiration of content (seconds)
     * @return string cached content
     */
    public function read($token, $timeout = 0)
    {
        $token = strtolower($token);
        if (!self::_checkToken($token)) {
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
        $fullpath = $this->_getFullPath($token);
        if (!is_file($fullpath)) {
            return '';
        }
        // check last modified time
        if ($timeout > 0 && filemtime($fullpath) < (time()-$timeout)) {
            return '';
        }
        $cnt = file_get_contents($fullpath);
        if ($cnt === false) {
            // @codeCoverageIgnoreStart
            user_error('cannot read from cache '.$fullpath, E_USER_WARNING);
            return '';
            // @codeCoverageIgnoreEnd
        }
        if (get_magic_quotes_runtime() == 1) {
            // @codeCoverageIgnoreStart
            $cnt = stripslashes($cnt);
        }
        // @codeCoverageIgnoreEnd
        return trim($cnt);
    }

    /**
     * check given token, allow only lowercase letters and numbers
     * @access private
     * @param string $token token (identificator of cached content)
     * @return bool false on error
     */
    protected function _checkToken($token)
    {
        if (!preg_match('/^[a-z0-9_]+$/', $token)) {
            user_error('invalid token '.$token.', only a-z0-9_ allowed', E_USER_WARNING);
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
        return true;
    }

    /**
     * returns path and filename of cached content by given token
     * @param string $token token (identificator of cached content)
     * @return string false on error
     */
    protected function _getFullPath($token)
    {
        if ($this->_cacheDir == '') {
            user_error('cache directory is not set yet', E_USER_WARNING);
            // @codeCoverageIgnoreStart
            return false;
            // @codeCoverageIgnoreEnd
        }
        return $this->_cacheDir.$token.'.tmp';
    }

    /**
     * get last modified time of cached file
     * @param string $token token (identificator of cached content)
     * @return int last modified time of cached file (unix timestamp) or null if file not found
     */
    protected function _getLastModified($token)
    {
        $fullpath = $this->_getFullPath($token);
        if (!is_file($fullpath)) {
            return null;
        }
        return filemtime($fullpath);
    }

}