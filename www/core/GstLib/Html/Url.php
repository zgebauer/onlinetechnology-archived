<?php

namespace GstLib\Html;

/**
 * library of functions for url
 */
class Url {

	/**
	 * return string with removed accents and all chars except [a-z0-9_] and additional allowed chars replaced
	 * with hyphen
	 * @param string $string
	 * @param string $allowChars
	 * @return string
	 */
	public static function fixForUrl($string, $allowChars = '') {
		$convert = [
			'á' => 'a', 'Á' => 'a', 'ä' => 'a', 'Ä' => 'a', 'ą' => 'a', 'Ą' => 'a',
			'č' => 'c', 'Č' => 'c',
			'ď' => 'd', 'Ď' => 'd',
			'é' => 'e', 'É' => 'e', 'ě' => 'e', 'Ě' => 'e', 'ę' => 'e', 'Ę' => 'e', 'ē' => 'e', 'Ē' => 'e',
			'ë' => 'e', 'Ë' => 'e',
			'í' => 'i', 'Í' => 'i',
			'ľ' => 'l', 'Ľ' => 'l', 'ł' => 'l', 'Ł' => 'l', 'ĺ' => 'l', 'Ĺ' => 'l',
			'ň' => 'n', 'Ň' => 'n',
			'ó' => 'o', 'Ó' => 'o', 'ö' => 'o', 'Ö' => 'o', 'ő' => 'o', 'Ő' => 'o',
			'ř' => 'r', 'Ř' => 'r',
			'š' => 's', 'Š' => 's',
			'ť' => 't', 'Ť' => 't',
			'ú' => 'u', 'Ú' => 'u', 'ů' => 'u', 'Ů' => 'u', 'ű' => 'u', 'Ű' => 'u', 'ü' => 'u', 'Ü' => 'u',
			'ý' => 'y', 'Ý' => 'y',
			'ž' => 'z', 'Ž' => 'z'
		];
		$string = strtolower(strtr($string, $convert));
		$string = preg_replace('/[^a-z0-9_' . preg_quote($allowChars) . ']+/', '-', $string);
		return trim($string, '-');
	}

	/**
	 * build query string from given arrays
	 *
	 * Returns $baseUrl with query string. Query string  will contain all
	 * parameters from arrays given as arguments after $baseUrl.
	 * Parameters with value NULL will be ignored.
	 * Parameters with numeric indexes will be ignored.
	 *
	 * @param string $baseUrl
	 * @param array $parameters assoc arrays with parameter for url
	 * @return string
	 */
	public static function queryUrl($baseUrl, $parameters = null) {
		$tmp = [];
		$args = func_get_args();

		if (func_num_args() === 1) {
			return self::fixUrl($baseUrl);
		}

		$length = count($args);
		for ($i = 1; $i < $length; $i++) {
			if (is_array($args[$i])) {
				$tmp = array_merge($tmp, $args[$i]);
			}
		}

		$parameters = [];
		foreach ($tmp as $key => $value) {
			if (is_null($value) || is_numeric($key)) {
				continue;
			}
			$parameters[$key] = $value;
		}

		$query = http_build_query($parameters, '', '&amp;');
		if ($query !== '') {
			$baseUrl .= (strpos($baseUrl, '?') === false ? '?' : '&amp;');
		}
		return self::fixUrl($baseUrl) . $query;
	}

	/**
	 * returns url with &amp; replaced by single &
	 * @param string $url
	 * @return string
	 */
	public static function fixAmp($url) {
		return str_replace('&amp;', '&', $url);
	}

	/**
	 * returns full url with protocol
	 * @param string $url
	 * @return string
	 */
	public static function fixUrl($url) {
		if ($url !== '' && substr(strtolower($url), 0, 7) !== 'mailto:' && strpos($url, '://') === false) {
			$url = 'http://' . $url;
		}
		return $url;
	}

	/**
	 * In given html replace relative url in src and href attributes with absolute urls starting with $baseUrl
	 * @param string $text html
	 * @param string $baseUrl
	 * @return string
	 */
	public static function urlToAbs($text, $baseUrl) {
		return preg_replace('#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#', '$1="' . $baseUrl . '$2$3', $text);
	}

}
