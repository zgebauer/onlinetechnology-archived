'use strict';

(function($){

    $('#dialog').dialog({
        autoOpen: false,
        modal: true
    });

    Ergo.RecordsList = function (table) {
        this.url = null;
        this.table = table;
        this.itemTemplate = null;

        this.position = 1;
        this.rowsPerPage = 20;
        this.total = 0;
        this.positionEnd = 0;
        this.pages = new Array();

        this.orderBy = '';
        this.orderDesc = false;
        this.extraParams = new Array();

        this.callbackLoaded = null;

        this.table.find('[data-orderby]').mousedown({recordsList:this}, function(event) {
            event.data.recordsList.orderBy = $(this).data('orderby');
            event.data.recordsList.orderDesc = ($(this).data('orderdesc') > 0);
            event.data.recordsList.load();
        });

        this.table.on('dblclick', 'tbody td', function() {
            if (!$(this).hasClass('edit') && !$(this).hasClass('del')) {
                $(this).closest('tr').find('td.edit').trigger('mousedown');
            }
        });

        this.load = function() {
            var query = {
                pos:this.position,
                rows_page:this.rowsPerPage,
                order:this.orderBy,
                desc:(this.orderDesc ? 1 : 0)
            };
            for(var index in this.extraParams) {
                query[index] = this.extraParams[index];
            }

            var url = this.url + (this.url.indexOf('?') === -1 ? '?' : '&') + $.param(query);
            $.ajax({
                url: url,
                cache: false,
                dataType: 'json',
                context: this,
            }).done(function(data, textStatus, jqXHR) {
                var html = '';
                for(var i=0; i<data.items.length; i++) {
                    var row = this.itemTemplate;
                    for (var key in data.items[0]) {
                        row = row.replace(new RegExp('%'+key+'%', 'g'), data.items[i][key]);
                    }
                    html += row;
                }
                this.table.find('tbody').html(html);
                this.position = data.pos;
                this.positionEnd = data.posEnd;
                this.total = data.total;
                this.pages = data.pages;

                if ($.isFunction(this.callbackLoaded)) {
                    this.callbackLoaded(this, data);
                }
            });
        };
    };

    Ergo.RecordsList.Filter = function(nodeForm, recordsList) {
        this.node = nodeForm;
        this.recordsList = recordsList;

        this.node.submit({recordsList:this.recordsList}, function(event) {
            var filters = new Array();
            $(this).find('input, select').each(function() {
                if ($(this).prop('name') !== '') {
                    filters[$(this).prop('name')] = $(this).val();
                }
            });
            event.data.recordsList.extraParams = filters;
            event.data.recordsList.load();
            return false;
        }).bind('reset', function(e) {
            e.preventDefault();
            $(this).find('input:text').val('');
            $(this).submit();
        });

    };

    Ergo.RecordsList.RowsPage = function(nodeSelect, recordsList) {
        this.node = nodeSelect;
        this.recordsList = recordsList;
        this.node.val(this.recordsList.rowsPerPage);

        this.node.change({recordsList:this.recordsList}, function(event) {
            event.data.recordsList.rowsPerPage = $(this).val();
            event.data.recordsList.position = 1;
            event.data.recordsList.load();
        });
    };

    Ergo.RecordsList.Pagination = function(nodeSelect, recordsList) {
        this.node = nodeSelect;
        this.recordsList = recordsList;
        this.node.change({recordsList:this.recordsList}, function(event) {
            event.data.recordsList.position = $(this).val();
            event.data.recordsList.load();
        });

        this.setPages = function() {
            var pages = this.recordsList.pages;
            var html = '';
            for (var i=0; i<pages.length; i++) {
                html += '<option value="'+pages[i]+'">Strana '+(i+1)+'</option>';
            }
            this.node.html(html);
            this.node.val(this.recordsList.position);
        };
    };

    Ergo.RecordsList.Positions = function(recordsList, nodeStart, nodeEnd, nodeTotal) {
        this.recordsList = recordsList;
        this.nodeStart = nodeStart;
        this.nodeEnd = nodeEnd;
        this.nodeTotal = nodeTotal;

        this.update = function() {
            this.nodeStart.html(this.recordsList.position);
            this.nodeEnd.html(this.recordsList.positionEnd);
            this.nodeTotal.html(this.recordsList.total);
        };
    };

})(jQuery);
