<?php
/**
 * @package Ergo
 */

namespace Ergo;

/**
 * base class for modules
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
abstract class ModuleAbstract
{
    /** @var array meta info */
    protected $_meta;
    /** @var \Ergo\ApplicationInterface application */
    protected $_app;

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application
     * @throws \Exception
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        if (!defined('static::NAME')) {
            throw new \Exception('Constant NAME is not defined on subclass '.get_class($this));
        }

        $this->_meta = array(
            'title' => '',
            'description' => '',
            'keywords' => '',
            'canonical'=>''
        );

        $this->_app = $application;
    }

    /**
     * returns menu items as array of arrays with indices
     * 'name' - text of link
     * 'url' - url of link
     * 'title' - title attribute of link
     * 'highlight' - flag "highlight link", bool
     *
     * if modules has no menu, returns null
     *
     * @return array|null
     */
    public function menuItems()
    {
        return null;
    }

    /**
     * returns permissions as
     * - null if module do not require any permission
     * - empty array if whole module has only one permission
     * - array in format [number]=>value  id module requires different permiisions
     *
     * @return null|array
     */
    public function permissions()
    {
        return null;
    }

    /**
     * returns name of module
     * @return string
     */
    abstract public function name();

    /** run scheduled tasks */
    public function cron()
    {
    }

    /**
     * returns output of module for web by given parameter:<br />
     * @param string $parameter
     * @return string
     */
    public function output($parameter = '')
    {
        switch($parameter) {
            case '':
                return $this->_app->render404();
            default:
                if (substr($parameter, 0, 1) === '_') {
                    return null;
                }
                return $this->_app->render404();
        }
        return null;
    }

    /**
     * returns output of module for admin by given parameter:<br />
     * @param string $parameter
     * @return string
     */
    public function outputAdmin($parameter = '')
    {
        switch($parameter) {
            case '':
                return $this->_app->render404();
            default:
                if (substr($parameter, 0, 1) === '_') {
                    return null;
                }
                return $this->_app->render404();
        }
        return null;
    }

    /**
     * refresh list of links related to module in table core_sitemap
     */
    public function refreshSitemapLinks()
    {
    }



    /**
     * returns menu items as array of arrays with indices
     * 'name' - text of link
     * 'url' - url of link
     * 'title' - title attribute of link
     * 'highlight' - flag "highlight link", bool
     *
     * if modules has no menu, returns null
     *
     * @return array|null
     */
    public function navigationLinks()
    {
        return NULL;
    }
}