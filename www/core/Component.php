<?php

namespace Ergo;

/**
 * base class for components
 */
abstract class Component {

    /** @var \Ergo\ApplicationInterface current application */
    protected $app;

    /**
     * @param \Ergo\ApplicationInterface $application
     */
    public function __construct(\Ergo\ApplicationInterface $application) {
        $this->app = $application;
    }

    /**
     * @return string
     */
    public static function getName() {
        return get_called_class();
    }

}
