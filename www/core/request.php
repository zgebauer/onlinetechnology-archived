<?php
/**
 * @package Ergo
 */
namespace Ergo;

/**
 * holds current request data
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
class Request
{
    /** @var variable type POST */
    const POST = 'post';
    /** @var variable type GET */
    const GET = 'get';
    /** @var variable type COOKIE */
    const COOKIE = 'cookie';
    /** @var variable type SESSION */
    const SESSION = 'session';
    /** @var variable type ENVIRONMENT */
    const ENVIRONMENT = 'env';

    /**
     * returns true if current request comes from browser speed dial
     * ($_SERVER['HTTP_X_PURPOSE']) is 'preview')
     * @return bool
     */
    public function isPreviewRequest()
    {
        return (isset($_SERVER['HTTP_X_PURPOSE']) && (strtolower($_SERVER['HTTP_X_PURPOSE']) === 'preview'));
    }

    /**
     * returns url of current page
     * @return string
     */
    public static function getCurrentUrl()
    {
        $ret = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http')
            .'://'.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');

        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '443') {
                $ret .= ':'.$_SERVER['SERVER_PORT'];
            }
        } else {
            if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '80') {
                $ret .= ':'.$_SERVER['SERVER_PORT'];
            }
        }
        $ret .= (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');
        return $ret;
    }

    /**
     * check if exists any data in $_POST
     * @return bool
     */
    public function isPost()
    {
        return (isset($_POST) && count($_POST) > 0);
    }

    /**
     * try find variable in global arrays Env, Get, Post, Cookie, Session and returns its value
     * $type can contain single string or array of strings get, post, cook, cookie, sess, session, env
     * $returnAs can contain int, string, bool, html, date, datetime
     *
     * @param string $name variable name
     * @param mixed $types where search variable, string or array of strings
     * @param string $returnAs data type of returned value
     * @param string $retType prefered global var, one option from $types
     * @return mixed
     */
    public function getValue($name, $types, $returnAs = 'string', $retType = null)
    {
        $ret = null;
        if (!is_array($types)) {
            $types = array($types);
        }

        foreach ($types as $type) {
            switch ($type) {
                case self::GET:
                    if (!isset($_GET[$name])) {
                        continue;
                    }
                    $ret = $_GET[$name];
                    if (is_array($ret)) {
                        $ret = array_map('urldecode', $ret);
                    } else {
                        $ret = urldecode($ret);
                    }


//                    $ret = urldecode($_GET[$name]);
                    break;
                case self::POST:
                    if (!isset($_POST[$name])) {
                        continue;
                    }
                    $ret = $_POST[$name];
                    break;
                case 'cook':
                case 'cookie':
                    if (!isset($_COOKIE[$name])) {
                        continue;
                    }
                    $ret = $_COOKIE[$name];
                    break;
                case 'sess':
                case 'session':
                    if (!isset($_SESSION[$name])) {
                        continue;
                    }
                    $ret = $_SESSION[$name];
                    break;
                case 'env':
                    if (!isset($_ENV[$name])) {
                        continue;
                    }
                    $ret = $_ENV[$name];
                    break;
            }
            if (!is_null($ret) && $retType == $type) {
                break;
            }
        }

        if (is_array($ret)) {
            $ret = array_map('trim', $ret);
        } else {
            $ret = trim($ret);
        }

        // magic quotes deprecated
        // @codeCoverageIgnoreStart
        if (get_magic_quotes_gpc()) {
            $ret = stripslashes($ret);
        }
        // @codeCoverageIgnoreEnd

        switch($returnAs) {
            case 'int':
                return (int) $ret;
            case 'float':
                return \GstLib\Vars::floatval($ret);
            case 'bool':
                // checkbox
                if ($ret === 'on' ) {
                    $ret = TRUE;
                }
                if ($ret === 'false' ) {
                    $ret = false;
                }
                return (bool) $ret;
            case 'html':
                return iconv('UTF-8', 'UTF-8//IGNORE', (string) $ret);
            case 'date':
                return \GstLib\Date::dateTime2Iso($ret);
            case 'datetime':
                return \GstLib\Date::dateTime2Iso($ret, true);
            case 'array':
                return $ret === '' ? array() : (array) $ret;
            default:
                break;
        }
        return strip_tags(iconv('UTF-8', 'UTF-8//IGNORE', (string) $ret));
    }

    /**
     * Check if request is from AJAX
     * @return bool
     */
    public function isAjax()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
            return true;
        }
        if ($this->getValue('isAjax', array(self::GET, self::POST)) !== '') {
            return true;
        }
        return false;
    }

    /**
     * get value from global $_POST
     *
     * @param string $name variable name
     * @param string $returnAs data type of returned value
     * @return mixed
     * @see $this->getValue()
     */
    public function getPost($name, $returnAs = 'string')
    {
        return self::getValue($name, self::POST, $returnAs);
    }

    /**
     * get value from global $_GET
     *
     * @param string $name variable name
     * @param string $returnAs data type of returned value
     * @return mixed
     * @see $this->getValue()
     */
    public function getQuery($name, $returnAs = 'string')
    {
        return self::getValue($name, self::GET, $returnAs);
    }

    /**
     * get value from global $_COOKIE
     *
     * @param string $name variable name
     * @param string $returnAs data type of returned value
     * @return mixed
     * @see $this->getValue()
     */
    public function getCookie($name, $returnAs = 'string')
    {
        if (ERGO_TESTING) {
            // fake cookie for testing
            return isset($GLOBALS[$name]) ? $GLOBALS[$name] : '';
        }
        return self::getValue($name, self::COOKIE, $returnAs);
    }

}