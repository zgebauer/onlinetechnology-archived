<?php
/**
 * @package Ergo
 */

namespace Ergo;

require_once __DIR__.'/config_interface.php';

/**
 * configuration
 */
class Config implements ConfigInterface
{
    /** @var int run mode - development */
    const MODE_DEVELOPMENT = 0;
    /** @var int run mode - testing */
    const MODE_TESTING = 1;
    /** @var int run mode - production */
    const MODE_PRODUCTION = 2;
    /** @var string base url of web */
    protected $_baseUrl;
    /** @var string base directory of web */
    protected $_baseDir;
    /** @var bool flag "mod_rewrite enabled" */
    protected $_urlRewriteEnabled;
    /** @var string name of default module */
    protected $_defaultModule;
    /** @var string ISO 639 code of default language */
    protected $_defaultLanguage;
    /** @var string ISO 639 code of current language */
    protected $_language;
    /** @var string name of current facade */
    protected $_facade;
    /** @var string name of default facade */
    protected $_defaultFacade;
    /** @var bool flag "translate content" */
    protected $_translate;
    /** @var bool flag "use session" */
    protected $_session;
    /** @var int production mode */
    protected $_mode;
    /** @var bool flag 'administration environment' */
    protected $_isAdmin;
    /** @var string path of admin directory */
    protected $_adminPath;
    /** @var array module aliases */
    protected $_moduleAliases;
    /** @var boolean flag page cache enabled */
    protected $_pageCacheEnabled;
    /** @var array strings in urls which disable page cache */
    protected $_pageCacheSkip;
    /** @var int page cache expiration */
    protected $_pageCacheExpire;

    /**
     * constructor
     * @param string base url of web
     * @param string base directory of web
     */
    public function __construct($baseUrl, $baseDir)
    {
        $this->_baseUrl = $baseUrl;
        $this->_baseDir = $baseDir;
        $this->_urlRewriteEnabled = false;
        $this->_defaultModule = 'core';
        $this->_defaultLanguage = 'en';
        $this->_language = null;
        $this->_facade = $this->_defaultFacade =  'default';
        $this->_translate = false;
        $this->_session = false;
        $this->_mode = self::MODE_DEVELOPMENT;
        $this->_isAdmin = false;
        $this->_adminPath = null;
        $this->_moduleAliases = array();
        $this->_pageCacheEnabled = false;
        $this->_pageCacheSkip = array();
        $this->_pageCacheExpire = 0;
    }

    /**
     * sets flag "mod_rewrite enabled"
     * @param bool $value
     * @return \Ergo\Config
     */
    public function setUrlRewriteEnabled($value)
    {
        $this->_urlRewriteEnabled = (bool) $value;
        return $this;
    }

    /**
     * sets language
     * @param string $value
     * @return \Ergo\Config
     */
    public function setLanguage($value)
    {
        $this->_language = $value;
        return $this;
    }

    /**
     * returns and sets facade
     * @param string $value
     * @return string
     */
    public function facade($value = null)
    {
        if (!is_null($value)) {
            $this->_facade = $value;
        }
        return $this->_facade;
    }

    /**
     * returns base url of web
     * @return string
     */
    public function baseUrl()
    {
        return $this->_baseUrl;
    }

    /**
     * returns base directory of web
     * @return string
     */
    public function baseDir()
    {
        return $this->_baseDir;
    }

    /**
     * returns directory of modules
     * @return string
     */
    public function baseDirModules()
    {
        return $this->_baseDir.'modules/';
    }

    /**
     * returns directory for cached files
     * @return string
     */
    public function baseDirCache()
    {
        return $this->_baseDir.'cache/';
    }

    /**
     * returns directory for log files from cron task
     * @return string
     */
    public function baseDirLogCron()
    {
        return $this->_baseDir.'log/cron/';
    }

    /**
     * returns and sets name of default module
     * @param string $value
     * @return string
     */
    public function defaultModule($value = null)
    {
        if (!is_null($value)) {
            $this->_defaultModule = $value;
        }
        return $this->_defaultModule;
    }

    /**
     * returns and sets code of default language
     * @param string $value
     * @return string
     */
    public function defaultLanguage($value = null)
    {
        if (!is_null($value)) {
            $this->_defaultLanguage = strtolower(substr($value, 0, 2));
        }
        return $this->_defaultLanguage;
    }

    /**
     * return directory of facade
     * @param type $facade
     * @return string
     */
    public function getDirFacade($facade = null)
    {
        $ret = $this->_baseDir.'facades/';
        if (!is_null($facade)) {
            $ret .= $facade.'/';
        }
        return $ret;
    }

    /**
     * returns name of default facade
     * @return string
     */
    public function getDefaultFacade()
    {
        return $this->_defaultFacade;
    }

    /**
     * returns current language code
     * @return string
     */
    public function getCurrentLanguage()
    {
        if (is_null($this->_language)) {
            return $this->_defaultLanguage;
        }
        return $this->_language;
    }

    /**
     * return and optionally sets flag "translate content"
     * @param bool $value
     * @return bool
     */
    public function translate($value = null)
    {
        if (!is_null($value)) {
            $this->_translate = (bool) $value;
        }
        return $this->_translate;
    }

    /**
     * return and optionally sets flag "use session"
     * @param bool $value
     * @return bool
     */
    public function useSession($value = null)
    {
        if (!is_null($value)) {
            $this->_session = (bool) $value;
        }
        return $this->_session;
    }

    /**
     * return and sets production mode
     * @param int $value
     * @return int
     * @throws \InvalidArgumentException
     */
    public function mode($value = null)
    {
        if (!is_null($value)) {
            if ($value !== self::MODE_DEVELOPMENT && $value !== self::MODE_TESTING
                    && $value !== self::MODE_PRODUCTION) {
                throw new \InvalidArgumentException('Invalid mode');
            }
            $this->_mode = $value;
        }
        return $this->_mode;
    }

    /**
     * returns abs path to directory for log files related to cron jobs
     * @return string
     */
    public function baseDirCron()
    {
        return $this->_baseDir.'log/cron/';
    }

    /**
     * return and optionally sets flag "administartion"
     * @param bool $value
     * @return bool
     */
    public function isAdmin($value = null)
    {
        if (!is_null($value)) {
            $this->_isAdmin = (bool) $value;
        }
        return $this->_isAdmin;
    }

    /**
     * returns and sets flag admin
     * @param string $value
     * @return string
     */
    public function adminPath($value = null)
    {
        if (!is_null($value)) {
            $value = trim($value);
            if ($value !== '') {
                $this->_adminPath = $value;
            }
        }
        return $this->_adminPath;
    }

    /**
     * returns and sets aliases of modules in format modulename=>alias
     * @param array $value
     * @return array
     */
    public function moduleAliases($value = null)
    {
        if (is_array($value)) {
            $this->_moduleAliases = $value;
        }
        return $this->_moduleAliases;
    }

    /**
     * returns and sets flag enable page cache
     * @param boolean $value
     * @return boolean
     */
    public function pageCacheEnabled($value = null)
    {
        if (!is_null($value)) {
            $this->_pageCacheEnabled = (bool) $value;
        }
        return $this->_pageCacheEnabled;
    }

    /**
     * sets array of strings. If these sstrings  found in current url, page wont be stored in page cache
     * @param array $value
     */
    public function pageCacheSkip(array $values)
    {
        array_walk(
            $values, function(&$value) {
                $value = trim($value);
            }
        );
        $this->_pageCacheSkip = $values;
    }

    /**
     * check if url can be stored in page cache
     * @param string $url
     * @return boolean
     */
    public function isPageCacheAllowed($url)
    {
        foreach ($this->_pageCacheSkip as $string) {
            if (strpos($url, $string) !== false) {
                return false;
            }
        }
        return true;
    }

    /**
     * returns and sets page cache expiration in seconds
     * @param int $value
     * @return int
     */
    public function pageCacheExpire($value = null)
    {
        if (!is_null($value)) {
            $value = ((int) $value < 0 ? 0 : (int) $value);
            $this->_pageCacheExpire =  $value;
        }
        return $this->_pageCacheExpire;
    }

}