<?php
/**
 * @package Ergo
 */

namespace Ergo;

require_once 'pagination.php';

use GstLib\Template;

/**
 * base class of objects for rendering GUI
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
abstract class GuiAbstract
{
    /** @var \Ergo\ApplicationInterface $application */
    protected $_app;

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * returns html code with pagination for administration
     * @param string $templatePath absolute path to template for pagination
     * @param int $sumRows number of all records in list
     * @param int $pos current position in list
     * @param string $url url of page
     * @param int $rowsPage number of records per one page
     * @param string $posVar name of variable holding position
     * @return string
     */
    public function renderPagination($templatePath, $sumRows, $pos, $url, $rowsPage, $posVar = Pagination::POS_VARIABLE)
    {
        $pager = new Pagination($sumRows, $pos, $rowsPage);

        $tpl = new Template($templatePath, true);
 
        $tmp = $pager->getUrlFirst($url, $posVar);
        $tpl->replace('URL_FIRST', $tmp, 'raw');
        $tpl->showSub('FIRST', $tmp != '');
        $tmp = $pager->getUrlPrev($url, $posVar);
        $tpl->replace('URL_PREV', $tmp, 'raw');
        $tpl->showSub('PREV', $tmp != '');
        $tmp = $pager->getUrlNext($url, $posVar);
        $tpl->replace('URL_NEXT', $tmp, 'raw');
        $tpl->showSub('NEXT', $tmp != '');
        $tmp = $pager->getUrlLast($url, $posVar);
        $tpl->replace('URL_LAST', $tmp, 'raw');
        $tpl->showSub('LAST', $tmp != '');

        $tmp = $pager->getOptions($url, $posVar, 100);
        $tpl->replace('INPVAL_PAGES', $tmp, array('output' => 'raw', 'quiet' => true));
        $tpl->showSub('PAGES', count($pager->getPages()) > 1);
        $options['cssSelected'] = 'selected';
        $options['descPageNum'] = true;
        $tmp = $pager->getLinks($url, $posVar, 20, $options);
        $tpl->replace('LINKS_PAGES', $tmp, array('output' => 'raw', 'quiet' => true));

        $tmp = $pager->getRecords();
        $tpl->replace('TXT_CURRENT_POS', $tmp[0]);
        $tpl->replace('TXT_END_POS', $tmp[1]);
        $tpl->replace('TXT_SUM', $pager->getTotalRows());
        unset($pager);

        return $tpl->get();
    }

}
