//'use strict'; // problem with loading via getScript()

var Ergo = {};

/**
 * @class auxiliary functions
 */
Ergo.Utils = {
    /**
     *@param {String} name of cookie
     *@return {String} value of cookie
     */
    getCookie : function(name) {
        var nameEQ = name + '=', ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c. length);
            }
            if (c.indexOf(nameEQ) === 0) {
                return decodeURIComponent(c.substring(nameEQ.length,c.length));
            }
        }
        return null;
    },

    /**
     * @param {String} name of cookie
     * @param {String} value of cookie
     * @param {int} expireTime expiration of cookie in miliseconds, default 7 days
     */
    setCookie : function(name, value, expireTime) {
        if (expireTime === undefined) {
            expireTime = 3600000*24*7;
        }
        var today = new Date();
        var expire = new Date();
        expire.setTime(today.getTime() + expireTime);
        document.cookie = escape(name)+"="+encodeURIComponent(value)+";expires="+expire.toGMTString()+";path=/;";
    },

    /**
     * @param {String} email
     * @return {Bool}
     */
    isEmail : function(email) {
        if (email.substr(0, 1) === '.' || email.substr(0, 1) === '_') {
            return false;
        }
        if (email.indexOf(':') !== -1 || email.indexOf('..') !== -1 || email.indexOf('.@') !== -1) {
            return false;
        }
        var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return filter.test(email);
    },

    /**
     * check date in format d.m.Y
     * @param {String} value
     * @return {Bool}
     */
    isDate : function(value) {
        if (value === '') {
           return false;
        }

        var parts = value.match(/^(\d{1,2})\.(\d{1,2})\.(\d{4})$/);
        if (parts === null) {
            return false;
        }

        var day = parseInt(parts[1]);
        var month = parseInt(parts[2]);
        var year = parseInt(parts[3]);

        if (month < 1 || month > 12) {
            return false;
        }
        if (day < 1 || day > 31) {
            return false;
        }
        if ((month === 4 || month === 6 || month === 9 || month === 11) && day === 31) {
            return false;
        }
        if (month === 2) {
            var isleap = (year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0));
            if (day > 29 || (day === 29 && !isleap)) {
                return false;
            }
        }
        return true;
    },

    translate : function(token) {
        return (ErgoTranslations[token] === undefined ? token : ErgoTranslations[token]);
    }

};


(function($){

    /**
     * after click show print dialog
     * @returns jQuery element(s)
     */
    $.fn.ergoPrintPage = function() {
        return this.each(function() {
            // allow mock window
            $(this).click(function(e, currentWindow) {
                var currentWindow = currentWindow || window;
                e.preventDefault();
                currentWindow.print();
            });
        });
    };

    /**
     * after click navigate back to previous page
     * @returns jQuery element(s)
     */
    $.fn.ergoGoBack = function() {
        return this.each(function() {
            // allow mock window
            $(this).click(function(e, currentWindow) {
                var currentWindow = currentWindow || window;
                e.preventDefault();
                currentWindow.history.back();
            });
        });
    };

    /**
     * open url from href attribute in new window on click
     * @returns jQuery element(s)
     */
    $.fn.ergoOpenWindow = function() {
        return this.each(function() {
            // allow mock window
            $(this).click(function(e, currentWindow) {
                e.preventDefault();
                var url = $(this).attr('href');
                if (url !== '') {
                   var currentWindow = currentWindow || window;
                   currentWindow.open(url);
                }
            });
        });
    };

    /**
     * after click show dialog "add bookmark"
     * @param {Array} options
     * @returns jQuery element(s)
     */
    $.fn.ergoAddBookmark = function(options) {

        this.ergoAddBookmark.settings = $.extend({}, this.ergoAddBookmark.defaults, options);

        var plugin = this;

        return this.each(function() {
            // allow mock window
            $(this).click(function(e, currentWindow) {
                var currentWindow = currentWindow || window;
                var title = $.trim($(this).attr('title'));
                title = (title === '' ? $.trim(document.title) : title);
                var url = $(this).attr('href');
                url = (url === '' ? location.href : url);

                if (currentWindow.sidebar) {
                    e.preventDefault();
                    currentWindow.sidebar.addPanel(title, url, '');
                    return false;
                } else if (currentWindow.external && ('AddFavorite' in currentWindow.external)) {
                    currentWindow.external.AddFavorite(url, title);
                    return false;
                } else if( currentWindow.opera && currentWindow.print ) {
                    $(this).attr('rel', 'sidebar');
                    return true;
                } else if(currentWindow.chrome){
                    currentWindow.alert(plugin.ergoAddBookmark.settings.message);
                }
                return false;
            });
        });
    };

    $.fn.ergoAddBookmark.defaults = {
        message: 'Press Ctrl+D to bookmark (Command+D for macs) after you click OK'
    };
    $.fn.ergoAddBookmark.settings = {};

    /**
     * after click navigate back to previous page
     * @returns jQuery element(s)
     */
    $.fn.ergoNavigate = function() {
        return this.each(function() {
            // allow mock window
            $(this).change(function(e, currentWindow) {
                var currentWindow = currentWindow || window;
                var url = $.trim($(this).find('option:selected:first').attr('value'));
                if (url !== '') {
                    currentWindow.location.href = url;
                }
            });
        });
    };

    $.fn.ergoSetLanguage = function(options) {
        var defaults = {
            cookieName: 'ergo_lang',
            cookieExpire: 3600000
        };
        var options = $.extend(defaults, options);

        return this.each(function() {
            var o = options;

            $(this).val(Ergo.Utils.getCookie(o.cookieName));

            // allow mock window
            $(this).change(function(e, currentWindow) {
                var currentWindow = currentWindow || window;
                var lang = $.trim($(this).find('option:selected:first').attr('value'));
                Ergo.Utils.setCookie(o.cookieName, lang, o.cookieExpire);
                currentWindow.location.reload(true);

            });
        });
    };

})(jQuery);





function number_format(number, decimals, dec_point, thousands_sep) {
	// phpjs.org
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function(n, prec) {
			var k = Math.pow(10, prec);
			return '' + (Math.round(n * k) / k).toFixed(prec);
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}


function formatCurrency(amount, currency, decimals) {
	amount = number_format(amount, decimals, ',', '');
	if (currency === 'EUR') {
		return '&euro;' + amount;
	}
	return amount + ' Kč';
}