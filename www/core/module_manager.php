<?php
/**
 * @package Ergo
 */

namespace Ergo;

/**
 * manage modules of Ergo
 *
 * Expect modules as classes with name Module.
 * - class have to be stored in file module.php in base directory of module
 * - class have to extends \Ergo\ModuleAbstract
 * - module have to has uniquie namespace
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
class ModuleManager
{
    /** @var string base directory with modules */
    protected $_dir;
    /** @var array of registered modules */
    protected $_modules;
    /** @var array cache of instances */
    protected $_cacheModules;
    /** @var \Ergo\Application application */
    protected $_app;

    /**
     * constructor
     * @param \Ergo\Application $application
     * @param string $dir full path to base directory with modules, including trailing slash
     * @throws \InvalidArgumentException
     */
    public function  __construct(Application $application, $dir)
    {
        $this->_app = $application;
        $this->_modules = array();
        $this->_cacheModules = array();
        if (!is_dir($dir)) {
            throw new \InvalidArgumentException('directory '.$dir.' not found');
        }
        $this->_dir = $dir;

        // register all modules
        foreach (glob($this->_dir.'*', GLOB_ONLYDIR) as $dir) {
            $this->_registerModule(basename($dir));
        }
    }

    /**
     * check if module exists
     * @param string $name name of module
     * @param bool $quiet true=do not raise error if module not found
     * @return bool
     */
    public function isModule($name, $quiet = false)
    {
        $this->_registerModule($name, $quiet);
        return in_array($name, $this->_modules);
    }

    /**
     * register module
     *
     * @param string $name name of module
     * @param bool $quiet true=do not raise error
     * @throws Exception
     */
    protected function _registerModule($name, $quiet = false)
    {
        if (!in_array($name, $this->_modules)) {
            $fullPath = $this->_getModuleFullpath($name);
            if (!is_file($fullPath) && !$quiet) {
                throw new \Exception('expected module file not found:'.$fullPath);
            }
            $this->_modules[] = $name;
        }
    }

    /**
     * @param string $name name of module
     * @return string full path and filename of module file
     */
    private function _getModuleFullpath($name)
    {
        return $this->_dir.$name.'/module.php';
    }

    /**
     * return instance of module, null if module not found
     * @param string $name name of module
     * @static array $cache
     * @return object
     */
    public function getModule($name)
    {
        if (isset($this->_cacheModules[$name])) {
            return $this->_cacheModules[$name];
        }

        if (!$this->isModule($name)) {
            // @codeCoverageIgnoreStart
            return null;
            // @codeCoverageIgnoreEnd
        }
        $fullPath = $this->_getModuleFullpath($name);
        require_once $fullPath;
        //$class = 'Ergo_Module_'.$name;
        $class = $name.'\Module';
        if (!class_exists($class)) {
            user_error('class '.$class.' not found', E_USER_WARNING);
            return null;
        }
        $ret = new $class($this->_app);
        $this->_cacheModules[$name] = &$ret;
        return $ret;
    }

    /**
     * in given string replace module pseudotags with output of getOutput or getAdminOutput methods of modules.
     * Pseudotags have to be in format [%MOD_module%] or [%MOD_module#param%]
     *
     * @param string $string
     * @param bool $admin replace with output for admin or public web
     * @return string
     */
    public function replaceModules($string, $admin = false)
    {
        // look for all [%MOD_some%]
        $occurences = array();
        preg_match_all('/\[%MOD_(.*)%\]/U', $string, $occurences, PREG_SET_ORDER);

        foreach ($occurences as $item) {
            // module name and param
            $tag = $item[0] ;
            $tmp = explode('#', $item[1]);
            $name = $tmp[0];
            $parameter = (isset($tmp[1]) ? $tmp[1] : '');

            $replacement = '';
            $module = $this->getModule($name);
            if (!$module) {
                user_error($name.' is not a module', E_USER_WARNING);
                continue;
            }

            if ($admin) {
                $replacement = $module->outputAdmin($parameter);
            } else {
                $replacement = $module->output($parameter);
            }
            $string = str_replace($tag, $replacement, $string);
        }
        return $string;
    }

    /**
     * returns registered modules
     * @return array
     */
    public function getModules()
    {
        return $this->_modules;
    }

}