<?php
/**
 * base functions
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */

use Ergo\Tracy\ApplicationPanel;
use Ergo\Tracy\DatabasePanel;
use Tracy\Debugger;

// @codingStandardsIgnoreStart
/**
 * sets php environment
 */
function ergoSetEnvironment()
{
    if (strpos(ini_get('disable_functions'), 'ini_set') === false) {
        ini_set('session.auto_start', 0);
        ini_set('session.use_cookies', 1);
        ini_set('session.cookie_lifetime', 0);
        ini_set('session.use_only_cookies', 1);
        ini_set('session.cookie_httponly', 1);
        @ini_set('session.use_trans_sid', 0);
        ini_set('url_rewriter.tags', '');
        ini_set('zend.ze1_compatibility_mode', 'off');
        ini_set('magic_quotes_gpc', 'off');
    }
    date_default_timezone_set(ERGO_TIMEZONE);
}

/**
 * register error and exception handler
 */
function ergoSetErrHandler()
{
	Debugger::$strictMode = true;
	Debugger::enable(ERGO_DEVEL_ADDRESS,ERGO_ROOT_DIR.'log/err/', ERGO_ERR_MAIL);
	Debugger::getBar()->addPanel(new DatabasePanel())
		->addPanel(new ApplicationPanel());

}
// @codingStandardsIgnoreEnd

