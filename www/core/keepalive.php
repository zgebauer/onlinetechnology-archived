<?php
/**
 * keep alive current session
 *
 * Should be used on servers where session.cookie_lifetime is not set to zero
 * to prevent logout after longer inactivity
 * Script have to be called repeatedly from javascript ie:
 * setInterval('keepAlive()', 60000);
 * function keepAlive() {
 *     $.get('keepalive.php?'+Math.random());
 * }
 *
 *
 * @package Ergo
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 */

if (strpos(ini_get('disable_functions'), 'ini_set') === false) {
    ini_set('session.auto_start', 0);
    ini_set('session.use_cookies', 1);
    ini_set('session.cookie_lifetime', 0);
    ini_set('session.use_only_cookies', 1);
    ini_set('session.cookie_httponly', 1);
    @ini_set('session.use_trans_sid', 0);
}

session_id($_COOKIE[session_name()]);
session_start();
die((string) rand(0, 9999));