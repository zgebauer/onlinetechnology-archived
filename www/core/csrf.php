<?php
/**
 * @package Ergo
 */

namespace Ergo;

use GstLib\Html;

require_once 'GstLib/Html.php';

/**
 * create and check CSRF tokens
 *
 * @author    Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package   Ergo
 */
class Csrf
{
    /** @var name of variable with csrf auth token (session, post,get) */
    const CSRF_VAR = 'ergo_csrf';
    /** @var \Ergo\ApplicationInterface application */
    protected $_app;

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application
     */
    public function __construct(ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * returns tag <input type="hidden" with auth token
     * @param string $token auth token, if not set function create new one
     * @return string
     */
    public function renderCsrfHidden($token = null)
    {
        if (is_null($token)) {
            $token = $this->getCsrf();
        }
        return '<input type="hidden" name="'.self::CSRF_VAR.'" value="'.Html::escape($token).'" />';
    }

    /**
     * returns new auth token, token is also stored in session
     * @return string
     */
    public function getCsrf()
    {
        if (isset($_SESSION[self::CSRF_VAR])) {
            return $_SESSION[self::CSRF_VAR];
        }
        return $this->refreshCsrf();
    }

    /**
     * returns new auth token, token is also stored in session
     * @return string
     */
    public function refreshCsrf()
    {
        $_SESSION[self::CSRF_VAR] = md5(uniqid());
        return $_SESSION[self::CSRF_VAR];
    }

    /**
     * check auth token
     * @return bool false if token is not valid
     */
    public function checkCsrf()
    {
        $csrf = $this->_app->request()
            ->getValue(self::CSRF_VAR, array(Request::POST, Request::GET), 'string', Request::GET);
        if (!isset($_SESSION[self::CSRF_VAR]) || $csrf == '' || $_SESSION[self::CSRF_VAR] != $csrf) {
            return false;
        }
        return true;
    }

}