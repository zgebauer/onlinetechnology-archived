<?php
/**
 * @package Ergo
 */

namespace Ergo;

require_once __DIR__.'/GstLib/Html.php';
require_once __DIR__.'/GstLib/Vars.php';
require_once __DIR__.'/GstLib/Date.php';
require_once __DIR__.'/GstLib/Filesystem.php';
require_once __DIR__.'/GstLib/Template.php';
require_once __DIR__.'/GstLib/Translator.php';
require_once __DIR__.'/GstLib/Sitemap.php';
require_once __DIR__.'/GstLib/FileCache.php';
require_once 'module_manager.php';
require_once 'module_abstract.php';
require_once 'router.php';
require_once 'request.php';
require_once 'response.php';
require_once 'csrf.php';
require_once 'gui_abstract.php';

use GstLib\Filesystem;
use GstLib\Html;
use GstLib\Template;
use GstLib\FileCache;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * interface of application object
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
interface ApplicationInterface
{
    /** @var name of session variable with url to redirect after login to admin */
    const REDIRECT_SESSION_VAR = 'ergo_login_redirect';
    /**
     * returns current request object
     * @return \Ergo\Request
     */
    public function request();
    /**
     * returns current response object
     * @return \Ergo\Response
     */
    public function response();

}

/**
 * application
 *
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package Ergo
 */
class Application implements ApplicationInterface
{
    /** @var ConfigInterface configuration */
    protected $_config;
    /** @var string current url */
    protected $_currentUrl;
    /** @var Request current request */
    protected $_request;
    /** @var Response current response */
    protected $_response;
    /** @var string current main module */
    protected $_currentModule;
    /** @var string current language */
    protected $_currentLanguage;
    /** @var Csrf csrf helper */
    protected $_csrf;
    /** @var bool flag "current request is for admin" */
    protected $_isAdmin;
    /** @var \GstLib\FileCache page cache instance */
    protected $_pageCache;
    /** @var \Dice DI container */
    protected $_dic;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->_config = null;
        $this->_currentUrl = null;
        $this->_currentModule = null;
        $this->_currentLanguage = null;
        $this->_response = null;
        $this->_csrf = null;
        $this->_isAdmin = false;
        $this->_pageCache = null;
        $this->_dic = null;
    }

    /**
     * process request and display result
     *
     * @param \Ergo\ConfigInterface $config
     * @param string $currentUrl
     * @param bool $admin administration or public environment
     */
    public function run(/*ConfigInterface*/ $config, $currentUrl = null, $admin = false)
    {
        $this->init($config, $currentUrl, $admin);

        $isPageCacheEnabled = !$admin &&
            $this->_config->pageCacheEnabled() && $this->_config->isPageCacheAllowed($this->_currentUrl);

        if ($isPageCacheEnabled) {
            $pageCacheToken = $this->_currentModule.'_'. md5(Html::fixForUrl($this->_currentUrl)) . '_'
                . $this->currentLanguage();
            $cachedPage = $this->pageCache()->read($pageCacheToken, $this->_config->pageCacheExpire());
            if ($cachedPage !== '') {
                if (strpos($cachedPage, '[%NOCACHE_MOD_') !== false) {
                    $cachedPage = str_replace('[%NOCACHE_MOD_', '[%MOD_', $cachedPage);
                    while (strpos($cachedPage, '[%MOD_') !== false) {
                        $cachedPage = $this->_moduleManager->replaceModules($cachedPage);
                    }
                }
                die($cachedPage);
            }
        }

        if (($this->_config->useSession() || $this->isAdmin()) && !isset($_SESSION)) {
            session_start();
        }

        // create output
        $this->_response = new Response();


        $module = $this->_moduleManager->getModule($this->_currentModule);

        if (strpos($this->_currentModuleParameter, 'ajax-') !== false
                && strpos($this->_currentModuleParameter, '_url_') === false
                && !$this->request()->isAjax()) {
            $this->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
        } else {
            if ($this->isAdmin()) {
                $module->outputAdmin($this->_currentModuleParameter);
            } else {
                $module->output($this->_currentModuleParameter);
            }
        }

        if ($this->_response->isSpecialOutput()) {
            $this->_response->output();
        }

        if ($this->isAdmin()) {
            $this->_response->setMetaProperty('og:title', '');
        } else {
            $this->_response->setMetaProperty('og:site_name', '[%LG_SITENAME%]');
            $this->_response->setMetaPropertyIfEmpty('og:type', 'website');
            $this->_response
            ->setMetaPropertyIfEmpty('og:image', $this->baseUrl().'facades/'.$this->currentFacade().'/opengraph.jpg');
        }

        $pageTitle = $this->_response->pageTitle();
        $pageHeader = $this->_response->pageHeader();
        $pageContent = $this->_response->pageContent();

        $template = ($this->request()->isPreviewRequest() ? 'preview.htm' : 'main.htm');
        $tpl = new Template($this->getTemplatePath($template, '', $this->isAdmin()));

        $tpl->replace('ERGO_MAIN_OUTPUT', $pageContent, 'raw');
        while (strpos($tpl->get(), '[%MOD_') !== false) {
            $tpl->set($this->_moduleManager->replaceModules($tpl->get(), $this->isAdmin()));
        }
        $tpl->replace('ERGO_HEADER_OUTPUT', $pageHeader, 'raw');
        $tpl->replace('TXT_TITLE', $pageTitle);
        $tpl->replace('URL_ROOT', $this->baseUrl(), array('quiet'=>true));

        if ($this->_config->translate()) {
            $tpl->set($this->_getTranslator()->translate($tpl->get()));
        }

        if ($this->isAdmin()) {
            $this->response()->setHeader('X-Frame-Options', 'deny');
        }

        $this->_response->sendHeaders();

        if ($isPageCacheEnabled) {
            $cachedPage = Html::minifyHtml($tpl->get());
            $this->pageCache()->store($pageCacheToken, $cachedPage);
//            $cachedPage = str_replace('[%NOCACHE_MOD_', '[%MOD_', $cachedPage);
//            while (strpos($cachedPage, '[%MOD_') !== false) {
//                $cachedPage = $this->_moduleManager->replaceModules($cachedPage);
//            }
            die($cachedPage);
        }

        $tpl->display();
    }

    /**
     * initialize application
     *
     * @param \Ergo\ConfigInterface $config
     * @param string $currentUrl
     * @param bool $admin administration or public environment
     */
    public function init(/* ConfigInterface */ $config, $currentUrl = null, $admin = false)
    {
        $this->_config = $config;
        $this->_currentUrl = (is_null($currentUrl) ? Request::getCurrentUrl() : $currentUrl);
        $this->_isAdmin = (bool) $admin;

        $this->_moduleManager = new ModuleManager($this, $this->_config->baseDirModules());

        // parse module and parameter from url
        $this->_router = new Router($this->baseUrl($this->isAdmin()), $this->_config->moduleAliases());
        $this->_router->setModules($this->_moduleManager->getModules(), $this->_config->defaultModule());
        $this->_router->parseUrl($this->_currentUrl);

        $this->_currentModule = $this->_router->getModule() == ''
                 ? $this->_config->getDefaultModule() : $this->_router->getModule();

        $this->_currentModuleParameter = $this->_router->getParameter();

        // parameters starting with _ are not main content of page
        if (substr($this->_currentModuleParameter, 0, 1) === '_') {
            $this->_currentModule = null;
        }
    }

    /**
     * returns string with translated [%LG_%] strings
     * @param string $string
     * @return string
     */
    public function translate($string)
    {
        return $this->_getTranslator()->translate($string);
    }

    /**
     * look for specified file in directory of module. If module is empty look for file in directory facades
     *
     * @param string $filename template filename
     * @param string $module name of module, empty means core
     * @param bool $admin true|false = template for admin|public web
     * @return string full path to template file
     */
    public function getTemplatePath($filename, $module = '', $admin = false)
    {
        $defaultFacade = 'default';
        $facade = ($this->_config->facade() === '' ? $defaultFacade : $this->_config->facade());
        $langDir = ($this->currentLanguage() === $this->config()->defaultLanguage() ? '' : $this->currentLanguage());

        $fullpathRet = ERGO_ROOT_DIR.
            ($module === '' ? 'facades/'.$facade.'/' : 'facades/'.$facade.'/modules/'.$module.'/').
            ($admin ? 'admin/' : '').
            ($langDir === '' ? '' : $langDir.'/').
            $filename;
        if (is_file($fullpathRet)) {
            return $fullpathRet;
        }
        $fullpathRet = ERGO_ROOT_DIR.
            ($module === '' ? 'facades/'.$facade.'/' : 'modules/'.$module.'/').
            ($admin ? 'admin/' : '').
            ($langDir === '' ? '' : $langDir.'/').
            $filename;
        if (is_file($fullpathRet)) {
            return $fullpathRet;
        }
        // not found file for specified language? try without language
        $fullpathRet = ERGO_ROOT_DIR.
            ($module === '' ? 'facades/'.$facade.'/' : 'modules/'.$module.'/').
            ($admin ? 'admin/' : '').
            $filename;
        if (is_file($fullpathRet)) {
            return $fullpathRet;
        }
        // file not found? try default location
        $fullpathRet = ERGO_ROOT_DIR.
            ($module === '' ? 'facades/'.$defaultFacade.'/' : 'modules/'.$module.'/').
            ($admin ? 'admin/' : '').
            ($langDir === '' ? '' : $langDir.'/').
            $filename;
        if (is_file($fullpathRet)) {
            return $fullpathRet;
        }
        $fullpathRet = ERGO_ROOT_DIR.
            ($module === '' ? 'facades/'.$defaultFacade.'/' : 'modules/'.$module.'/').
            ($admin ? 'admin/' : '').
            $filename;
        return $fullpathRet;
    }

    /**
     * run psedocron method of each module
     * @param int $timeout minimal timeout between run
     */
    public function cron($timeout = 600)
    {
        $logDir = $this->_config->baseDirCron();
        if (!is_dir($logDir)) {
            Filesystem::mkDir($logDir);
        }

        $runFile = $logDir.'cron.lastrun';
        $logFile = $logDir.'cron.log';
        $timeout = $timeout < 600 ? 600 : $timeout;

        // get time of last run
        $lastRun = 0;
        if (is_file($runFile)) {
            $lastRun = intval(file_get_contents($runFile));
        }
        if ($lastRun + $timeout > time()) {
            return;
        }

        file_put_contents($logFile, 'cron start:'.date('c')."\n", FILE_APPEND);
        foreach ($this->_moduleManager->getModules() as $moduleName) {
            $module = $this->_moduleManager->getModule($moduleName);
            if (is_null($module)) {
                continue;
            }
            $module->cron();
        }
        file_put_contents($logFile, 'cron end:'.date('c'). "\n", FILE_APPEND);

        // once a day rotate logs
        $fullPath = $this->_config->baseDirCron().'logrotate.lastrun';
        $lastRun = '';
        if (is_file($fullPath)) {
            $lastRun = trim(file_get_contents($fullPath));
        }
        if ($lastRun != date('Y-m-d')) {
            Filesystem::logrotate($logFile, 5);
            Filesystem::logrotate(ERGO_ROOT_DIR.'log/err/error.log', 10);
            file_put_contents($fullPath, date('Y-m-d'));
        }

        // generate sitemap.xml
        $fullPath = $this->_config->baseDirCron().'sitemap.lastrun';
        $lastRun = '';
        if (is_file($fullPath)) {
            $lastRun = trim(file_get_contents($fullPath));
        }

        if ($lastRun != date('Y-m-d')) {
            foreach ($this->_moduleManager->getModules() as $moduleName) {
                $module = $this->_moduleManager->getModule($moduleName);
                if (!is_null($module)) {
                    $module->refreshSitemapLinks();
                }
            }
            file_put_contents($fullPath, date('Y-m-d'));
        }

        // save time of last run
        file_put_contents($runFile, time());
    }

    /**
     * returns code of current language
     * @return string
     */
    public function currentLanguage()
    {
        if (is_null($this->_currentLanguage)) {
            return $this->_config->getCurrentLanguage();
        }
        return $this->_currentLanguage;
    }

    /**
     * returns array of allowed languages
     * @return array
     */
    public static function getLanguages()
    {
        return self::$_languages;
    }

    /**
     * returns root url of application
     * @param bool returns url for admin or public web
     * @return string
     */
    public function baseUrl($admin = false)
    {
        if ($admin) {
            return $this->_config->baseUrl().$this->_config->adminPath()
                .(is_null($this->_config->adminPath()) ? '' : '/');
        }
        return $this->_config->baseUrl();
    }

    /**
     * returns abs path to dirtectory for modules incl. trailing slash
     * @return string
     */
    public function baseDirModules()
    {
        return $this->_config->baseDirModules();
    }

    /**
     * return object with configuration
     * @return \Ergo\ConfigInterface
     */
    public function config()
    {
        return $this->_config;
    }

    /**
     * return current url
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_currentUrl;
    }

    /**
     * check app is in production mode
     * @return bool
     */
    public function isProductionMode()
    {
        return $this->_config->mode() == Config::MODE_PRODUCTION;
    }

    /**
     * returns page 404
     * @return string
     */
    public function render404()
    {
        return $this->_moduleManager->getModule('core')->output('404');
    }

    /**
     * returns current request
     * @return \Ergo\Request
     */
    public function request()
    {
        if (is_null($this->_request)) {
            $this->_request = new \Ergo\Request();
        }
        return $this->_request;
    }

    /**
     * returns current request
     * @return \Ergo\Response
     */
    public function response()
    {
        if (is_null($this->_response)) {
            $this->_response = new \Ergo\Response();
        }
        return $this->_response;
    }

    /**
     * returns factory - DI container
     * @return \Dice
     * @see http://r.je/dice.html
     */
    public function dice()
    {
        return $this->dic;
    }

    /**
     * returns shared instance of class
     * @param string $className
     * @return object
     */
	public function getInstance($className) {
		return $this->dic->create($className);
	}

    /**
     * register class as "shared",
     * so create instance through dice() factory will return the same instance
     * @param class $className
     */
    public function setShared($className)
    {
        $this->dice()->addRule($className, ['shared' => true]);
    }

    /**
     * returns complete url combined from module and parameter
     * @param string $module name of module
     * @param string $parameter parameter for module
     * @param bool $admin force url for admin or public web
     * @return string
     */
    public function getUrl($module, $parameter = null, $admin = null)
    {
        if (is_null($admin)) {
            $admin = $this->isAdmin();
        }
        return $this->_router->createUrl($this->baseUrl($admin), $module, $parameter);
    }

    /**
     * returns registered modules
     * @return array
     */
    public function getModules()
    {
        return $this->_moduleManager->getModules();
    }

    /**
     * return instance of module, null if module not found
     * @param string $name name of module
     * @return \Ergo\ModuleAbstract
     */
    public function getModule($name)
    {
        return $this->_moduleManager->getModule($name);
    }

    /**
     * returns current main module
     * @return string
     */
    public function getCurrentModule()
    {
        return $this->_currentModule;
    }

    /**
     * returns current parameter for main module
     * @return string
     */
    public function currentModuleParameter()
    {
        return $this->_currentModuleParameter;
    }

    /**
     * returns current facade
     * @return string
     */
    public function currentFacade()
    {
        return $this->_config->facade();
    }

    /**
     * returns object which creta and check CSRF tokens
     * @return \Ergo\Csrf
     */
    public function csrf()
    {
        if (is_null($this->_csrf)) {
            $this->_csrf = new Csrf($this);
        }
        return $this->_csrf;
    }

    /**
     * send email
     * @param string|array $from
     * @param array $recipients
     * @param string $subject
     * @param string $bodyText
     * @param string $bodyHtml
     * @param array $options
     * @param array $attachments
     * @return bool false on error
     */
    public function sendMail($from, array $recipients, $subject, $bodyText, $bodyHtml = '',
            array $options = array(), array $attachments = array())
    {
        static $mailer;
        if (!isset($mailer)) {
            $mailer = new PHPMailer(true);
            // @codingStandardsIgnoreStart
            $mailer->Host = PHPMAILER_SMTP_HOST;
            $mailer->SMTPAuth = PHPMAILER_SMTP_AUTH;
            $mailer->Username = PHPMAILER_SMTP_USERNAME;
            $mailer->Password = PHPMAILER_SMTP_PASSWORD;
            $mailer->Mailer = PHPMAILER_MAILER;
            // @codingStandardsIgnoreEnd
        }

        $charset = 'UTF-8';
        $encoding = '8bit';

        if (!is_array($from)) {
            $from = array($from);
        }
        if (!isset($from[1])) {
            $from[1] = '';
        }
        try {
            $mailer->SetFrom($from[0], $from[1]);
        } catch (\phpmailerException $e) {
            user_error($e->getMessage(), E_USER_WARNING);
            return false;
        }
        $mailer->ClearAddresses();
        foreach ($recipients as $item) {
            $address = $item;
            $name = '';
            if (is_array($item) && isset($item[1])) {
                $address = $item[0];
                $name = $item[1];
            }
            try {
                $mailer->AddAddress($address, $name);
            } catch (phpmailerException $e) {
                user_error($e->getMessage(), E_USER_WARNING);
                return false;
            }
        }
        // @codingStandardsIgnoreStart
        $mailer->Subject = $subject;
        // @codingStandardsIgnoreEnd
        if ($bodyHtml == '') {
            // @codingStandardsIgnoreStart
            $mailer->Body = $bodyText;
            $mailer->AltBody = '';
            // @codingStandardsIgnoreEnd
            $mailer->IsHTML(false);
        } else {
            // @codingStandardsIgnoreStart
            $mailer->Body = $bodyHtml;
            $mailer->AltBody = $bodyText;
            // @codingStandardsIgnoreEnd
            $mailer->IsHTML(true);
        }
        $mailer->IsHTML($bodyHtml != '');
        if (isset($options['charset'])) {
            $charset = $options['charset'];
        }
        if (isset($options['encoding'])) {
            $encoding = $options['encoding'];
        }
        // @codingStandardsIgnoreStart
        $mailer->CharSet = $charset;
        $mailer->Encoding = $encoding;
        foreach ($attachments as $attachment) {
            $mailer->AddAttachment($attachment);
        }
        // @codingStandardsIgnoreEnd

        if (ERGO_TESTING) {
            // in testmode store info about email to global variable
            $tmpRecipients = array();
            foreach ($recipients as $item) {
                $address = $item;
                $name = '';
                if (is_array($item) && isset($item[1])) {
                    $address = $item[0];
                    $name = $item[1];
                }
                $tmpRecipients[] = trim($address.' '.$name);
            }
            $GLOBALS['testing_emails'][] = array(
                // @codingStandardsIgnoreStart
                'subject' => $mailer->Subject,
                'from' => $mailer->From,
                'fromName' => $mailer->FromName,
                // @codingStandardsIgnoreEnd
                'to' => join(',', $tmpRecipients),
                'bodyText' => $bodyText,
                'bodyHtml' => $bodyHtml,
                'attachments' => join(',', $attachments)
            );
        } else {
            try {
                $mailer->Send();
            } catch (\phpmailerException $e) {
                // @codingStandardsIgnoreStart
                //user_error($->ErrorInfo, E_USER_NOTICE);
                // @codingStandardsIgnoreEnd
                return false;
            }
        }
        return true;
    }

    /**
     * returns instance of translator
     * @staticvar \GstLib\Translator $translator
     * @return \GstLib\Translator
     */
    protected function _getTranslator()
    {
        static $translator;
        if (!isset($translator)) {
            $langFile = $this->_config->baseDir().'lang/'
                .($this->isAdmin() ? 'admin/' : '').$this->currentLanguage().'.txt';

            $translator = new \GstLib\Translator($this->currentLanguage(), array($langFile));
        }
        return $translator;
    }

    /**
     * returns true if run in administration environment
     * @return bool
     */
    public function isAdmin()
    {
        return $this->_config->isAdmin();
    }

    /**
     * returns path od admin directory
     * @return string
     */
    public function adminPath()
    {
        if (is_null($this->_config->adminPath())) {
            return '';
        }
        return (string) $this->_config->adminPath();

    }

    /**
     * remember current url in session $_SESSION[self::REDIRECT_SESSION_VAR]
     */
    public function rememberRedirectLogin()
    {
        $_SESSION[self::REDIRECT_SESSION_VAR] = $this->_currentUrl;
    }

    /**
     * returns url remembered before login
     * @return string
     */
    public function targetUrlAfterLogin()
    {
        $ret = $this->baseUrl(true);
        if (isset($_SESSION[self::REDIRECT_SESSION_VAR])) {
            $ret = $_SESSION[self::REDIRECT_SESSION_VAR];
            unset($_SESSION[self::REDIRECT_SESSION_VAR]);
        }
        return $ret;
    }

    /**
     * returns page cache instance
     * @return \GstLib\FileCache
     */
    public function pageCache()
    {
        if (is_null($this->_pageCache)) {
            $cacheDir = $this->_config->baseDirCache().'page/';
            if (!is_dir($cacheDir)) {
                Filesystem::mkDir($cacheDir);
            }
            $this->_pageCache = new FileCache($cacheDir);
        }
        return $this->_pageCache;
    }

    /**
     * handle exception without displaying and stopping script
     * @staticvar \Ergo\ErrHandler $errHandler
     * @param \Exception $exception
     */
    public static function logException(\Exception $exception)
    {
        static $errHandler;
        if (!isset($errHandler)) {
            $errHandler = new \Ergo\ErrHandler(
                array(
                    'logDir' => ERGO_ROOT_DIR.'log/err/',
                    'displayErrors' => FALSE,
                    'haltOnError' => FALSE,
                    'email' => ERGO_ERR_MAIL
                )
            );
        }
        $errHandler->exceptionHandler($exception);
    }

    /**
     * returns navigation links from all modules
     * @return array
     */
    public function getOptionsModuleLinks()
    {
        $ret = array();
        foreach ($this->getModules() as $moduleName) {
            $module = self::getModule($moduleName);
            $links = $module->navigationLinks();
            if (is_null($links)) {
                continue;  // no menu links
            }
            $modName = constant(get_class($module).'::NAME');
            foreach ($links as $url=>$link) {
                $link = ($link === '' ? $link : ':'.$link);
                $ret[$modName.'#'.$url] = $module->name().$link;
            }
        }
        return $ret;
    }

}