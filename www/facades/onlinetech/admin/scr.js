'use strict';

$(document).ready(function(){

    $('select.redirect').ergoNavigate();

    // fix for msie
    $('.buttons a input').click(function() {
        $(this).closest('a').trigger('click');
    });

    if ($('#adminsList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/admins/admin/admins.js'))
        .done(function() { Ergo.Admins.Admin(); });
    }

    if ($('#productsList').length > 0) {
        $.when(
            $.getScript(ERGO_ROOT_URL+'other/jquery/jquery.dropUpload.js'),
            $.getScript(ERGO_ROOT_URL+'modules/eshop/admin/products.js?160130')
        )
        .done(function() { Ergo.Eshop.Products.Admin(); });
    }
    if ($('#categoriesList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/eshop/admin/categories.js'))
        .done(function() { Ergo.Eshop.Categories.Admin(); });
    }
    if ($('#ordersList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/eshop/admin/orders.js?1'))
        .done(function() { Ergo.Eshop.Orders.Admin(); });
    }
    if ($('#customersList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/eshop/admin/customers.js'))
        .done(function() { Ergo.Eshop.Customers.Admin(); });
    }
    if ($('#iconsList').length > 0) {
        $.when(
            $.getScript(ERGO_ROOT_URL+'other/jquery/jquery.dropUpload.js'),
            $.getScript(ERGO_ROOT_URL+'modules/eshop/admin/icons.js')
        )
        .done(function() { Ergo.Eshop.Icons.Admin(); });
    }
    if ($('#frmEshopSettings').length > 0) {
        $.when(
            $.getScript(ERGO_ROOT_URL+'modules/eshop/admin/config.js')
        )
        .done(function() { Ergo.Eshop.Config.Admin(); });
    }
    if ($('#staticarticlesList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/staticarticles/admin/scr.js'))
        .done(function() { Ergo.Staticarticles.Admin(); });
    }
    if ($('#articlesList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/articles/admin/scr.js'))
        .done(function() { Ergo.Articles.Admin(); });
    }
    if ($('#menuitemsList').length > 0) {
        $.when($.getScript(ERGO_ROOT_URL+'modules/menu/admin/items.js'))
        .done(function() { Ergo.Menu.Items.Admin(); });
    }

    // keep session alive
    setInterval(function() { $.get(ERGO_ROOT_URL+'core/keepalive.php?'+Math.random()); }, 600000);

});

