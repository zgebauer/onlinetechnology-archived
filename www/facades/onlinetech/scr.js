'use strict';

$(document).ready(function(){

	$('.printPage').ergoPrintPage();
	$('.addBookmark').ergoAddBookmark();

	$('#prodDetail dl.ergoTabs').each(function() {
		$(this).find('dt').mousedown(function() {
			$(this).siblings().removeClass('selected').end().next('dd').andSelf().addClass('selected');
		});
	});

	$('#menu a').each(function() {
		if ($(this).attr('href') !== ERGO_ROOT_URL && window.location.href.indexOf($(this).attr('href')) !== -1) {
			$(this).parents('li').addClass('selected');
		}
	});
	if ($('#menu li.selected').length === 0) {
		$('#menu li:first').addClass('selected');
	}

	$("#slider").easySlider({
		auto: true,
		continuous: true
	});

	setSlider(Ergo.Utils.getCookie('slider'));

	$('#banHider').mousedown(function() {
		Ergo.Utils.setCookie('slider', 1, (3600000*24));
		setSlider(1);
	});
	$('#banShow').mousedown(function() {
		Ergo.Utils.setCookie('slider', 0, (3600000*24));
		setSlider(0);
	});

	function setSlider(minified){
		if (minified == 1) {
			$("#sliderHolder").addClass('hidden');
			$("#sliderHolderMin").removeClass('hidden');
		} else {
			$("#sliderHolder").removeClass('hidden');
			$("#sliderHolderMin").addClass('hidden');
		}
	};

	$('.colorbox').colorbox({
		rel:'colorbox',
		current:'obrázek {current} z {total}'
	});

	$('.autocolorbox a:has(img)').colorbox({
		maxWidth:'90%',
		maxHeight:'90%',
		rel:'colorbox2'
	});

	$('[data-action="show3d"]').colorbox({
		width:1030,
		height:750,
		iframe:true
	});

	$('#crossSell').bxSlider({
		maxSlides: 3,
		slideWidth: 244,
		pager: false,
		nextSelector: '#nextCross',
		prevSelector: '#prevCross',
		nextText: '',
		prevText: ''
	});
	$('#variants').bxSlider({
		maxSlides: 3,
		slideWidth: 244,
		pager: false,
		nextSelector: '#nextVariant',
		prevSelector: '#prevVariant',
		nextText: '',
		prevText: ''
	});

	$('.triz a').each(function() {
		if (document.location.href.indexOf($(this).prop('href')) !== -1) {
			$(this).addClass('selected');
			return;
		}
	});
	if ($('.triz a.selected').length === 0) {
		$('.triz a:first').addClass('selected');
	}

});

