<?php
header('HTTP/1.1 503 Service Temporarily Unavailable');
header('Status: 503 Service Temporarily Unavailable');
header('Retry-After: 60');
// @codingStandardsIgnoreStart
?>
<!DOCTYPE html>
<html lang="cs">
<head>
<meta charset="utf-8" />
<title>Údržba webu</title>
<meta name="robots" content="noindex" />
</head>
<body>
<h1>Probíhá aktualizace webu</h1>
<p>Probíhá aktualizace a údržba webu, brzy bude web opět fungovat.</p>
<p>Omlouváme se a těšíme se na váš brzký návrat.</p>
</body>
</html>