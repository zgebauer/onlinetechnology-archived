<?php

use Ergo\Environment;

error_reporting(E_ALL);
ini_set('display_errors', 1);
header('Content-Type: text/plain', true);

$allowEnvironment = array('localhost', 'devkobylanet', 'wwwonlinetechnologycz');

$environment = $_SERVER['QUERY_STRING'];
if (!in_array($environment, $allowEnvironment)) {
	die('unallowed environment');
}


require_once dirname(__DIR__) . '/config/config.php';
require_once ERGO_ROOT_DIR . 'vendor/autoload.php';

ergoSetEnvironment();
error_reporting(E_ALL);
ini_set('display_errors', 1);

try {
	$output = '';

	$output .= "delete setup\n" . __DIR__;
	if ($environment !== 'localhost') {
		GstLib\Filesystem::rmDir(__DIR__);
	}

	echo "OK\n", $output;
} catch (Exception $exception) {
	header('HTTP/1.0 500 Internal Server Error');
	echo "ERROR\n", $exception->getMessage(), "\n", $output;
}
