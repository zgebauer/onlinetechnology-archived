<?php
/**
 * sample of main configuration file
 *
 * @package Ergo
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 */

/** database type - null=no database/mysqli */
define('ERGO_DB_TYPE', 'mysqli');
/** name or IP address of database server */
define('ERGO_DB_SERVER', '127.0.0.1');
/** username for database */
define('ERGO_DB_USER', 'root');
/** password for database */
define('ERGO_DB_PWD', '');
/** database name, path to database file */
define('ERGO_DB_NAME', 'onlinetechnology');
/** true|false = use persistent|normal connection */
define('ERGO_DB_PERSISTENT', false);
/** prefix of table names */
define('ERGO_DB_PREFIX', '');
/** character set of database */
define('ERGO_DB_CHARSET', 'utf8');

/** base directory of website with trailing slash */
define('ERGO_ROOT_DIR', str_replace('\\', '/', dirname(__DIR__)) . '/');
/** base url of website with trailing slash */
define('ERGO_ROOT_URL', 'http://'.$_SERVER['SERVER_NAME'].'/');

/** base directory for users data with trailing slash */
define('ERGO_DATA_DIR', ERGO_ROOT_DIR.'data/');

/** base directory for log files with trailing slash */
define('ERGO_LOG_DIR', ERGO_ROOT_DIR.'log/');
/** base directory for cached files files with trailing slash */
define('ERGO_CACHE_DIR', ERGO_ROOT_DIR.'cache/');
/** true|false, true enable full page cache */
define('ERGO_PAGE_CACHE_ENABLE', false);

/** e-mail address for error messages, null=do not send */
define('ERGO_ERR_MAIL', null);
/** ip address of developer, null = none. Multiple addresses as string delimited by coma */
define('ERGO_DEVEL_ADDRESS', null);

/** true|false = server support|do not support mod_rewrite */
define('ERGO_REWRITE_ENABLED', true);
/** time zone, http://php.net/manual/en/function.date-default-timezone-set.php */
define('ERGO_TIMEZONE', 'Europe/Prague');

/** sender of automatically sent e-mails */
define('ERGO_EMAIL_ROBOT', 'ergo@onlinetechnology.cz');
/**
 * send mail via [mail|smtp]
 * @see http://phpmailer.codeworxtech.com/
 */
define('PHPMAILER_MAILER', 'mail');
/** smtp server, if use smtp */
define('PHPMAILER_SMTP_HOST', '');
/** authorize to smtp server, if use smtp */
define('PHPMAILER_SMTP_AUTH', false);
/** username for smtp server, if PHPMAILER_SMTP_AUTH is true */
define('PHPMAILER_SMTP_USERNAME', '');
/** password for smtp server, if PHPMAILER_SMTP_AUTH is true */
define('PHPMAILER_SMTP_PASSWORD', '');

/**run mode - 0=development, 1=testing, 2=production */
define('ERGO_RUN_MODE', 2);
/** true|false = run in test(phpunit)|normal mode */
define('ERGO_TESTING', false);
