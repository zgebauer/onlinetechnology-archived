<?php
/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu;

use GstLib\Html;

/**
 * main class of module Menu
 * Management of menu and menu items
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'menu';

    public function __construct(\Ergo\ApplicationInterface $application)
    {
        parent::__construct($application);
    }

    public function menuItems()
    {
        $user = $this->_app->getUser();
        if (is_null($user) || !$user->hasPermission(self::NAME)) {
            return null;
        }

        return array(
            array(
                'name' => 'Menu CS',
                'url' => Html::queryUrl($this->_app->getUrl(self::NAME, 'items')),
                'title' => 'Menu české verze',
                'highlight' => ($this->_app->getCurrentModule() === self::NAME)
            ),
            array(
                'name' => 'Menu EN',
                'url' => Html::queryUrl($this->_app->getUrl(self::NAME, 'items'), array('lang'=>'en')),
                'title' => 'Menu anglické verze',
                'highlight' => ($this->_app->getCurrentModule() === self::NAME)
            )
        );
    }

    public function permissions()
    {
        return array();
    }

    public function output($parameter = '')
    {
        $map = array(
            '_box_' => array('\Menu\Controller', 'renderBox'),
        );

        if (isset($map[$parameter])) {
            return call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
        }
        switch ($parameter) {

//            case '__links__':
//                return array('_url_root_' => 'Homepage');
//            case '_box_':
//                return Ergo_Core::factory('Menu_Gui')->renderBox();
//            case '_box_footer_':
//                return Ergo_Core::factory('Menu_Gui')->renderBoxFooter();
        case '_url_root_':
            return ERGO_ROOT_URL;
//            default:
//                if (substr($parameter, 0, 5) == '_url_') {
//                    return Gst_Router::getUrl(
//                        self::NAME, substr($parameter, 5)
//                    );
//                }
//                $alias = $this->getAliasUrl('record').'/';
//                if (substr($parameter, 0, strlen($alias)) == $alias) {
//                    $seo = substr($parameter, strlen($alias));
//                    if ($seo == '') {
//                        Gst_Html::redirect(ERGO_ROOT_URL);
//                    }
        }

        return parent::output($parameter);
    }

    public function outputAdmin($parameter = '')
    {
        $map = array(
            'items' => array('\Menu\Admin\ControllerItems', 'renderList'),
            'ajax-listitems' => array('\Menu\Admin\ControllerItems', 'handleList'),
            'ajax-edititem' => array('\Menu\Admin\ControllerItems', 'handleEdit'),
            'ajax-saveitem' => array('\Menu\Admin\ControllerItems', 'handleSave'),
            'ajax-deleteitem' => array('\Menu\Admin\ControllerItems', 'handleDelete'),
            'ajax-moveitem' => array('\Menu\Admin\ControllerItems', 'handleMove'),
//            'ajax-publishproduct' => array('\Eshop\Admin\ControllerProducts', 'handlePublish'),

        );

        if (isset($map[$parameter])) {
            call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
            return;
        }
        return parent::outputAdmin($parameter);
    }

    public function name()
    {
        return 'Menu';
    }

    public function cron()
    {
    }

    public function navigationLinks()
    {
        return array('_url_root_' => 'Homepage');
    }



}