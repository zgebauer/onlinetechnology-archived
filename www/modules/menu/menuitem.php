<?php
/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu;

use GstLib\Vars;

/**
 * Item in menu
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class MenuItem
{
    /** @var int type of target - module */
    const TARGET_MODULE = 0;
    /** @var int type of target - url */
    const TARGET_URL = 1;

    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var bool flag "loaded from data tier" */
    protected $_loaded;
    /** @var string name */
    protected $_name;
    /** @var date date of last change of record */
    //protected $_lastChange;
    /** @var int menu identifier */
    protected $_menuId;
    /** @var int id of parent item */
    protected $_parentId;
    /** @var int ordering in parent */
    protected $_ordering;
    /** @var bool flag publish record */
    protected $_publish;
    /** @var string target module or url  */
    protected $_target;
    /** @var int type of target */
    protected $_targetType;
    /** @var string */
    private $lang;

    /** @var \Menu\DataItem data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Menu\DataItem $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(\Menu\DataItem $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_name = '';
        $this->_menuId = 0;
        $this->_parentId = 0;
        $this->_ordering = 0;
        $this->_publish = false;
        $this->_lang = '';
        $this->_target = '';
        $this->_targetType = self::TARGET_MODULE;
        $this->lang = 'cs';

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded()
    {
        return $this->_loaded;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null)
    {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 50);
        }
        return $this->_name;
    }

    /**
     * returns date of last change
     * @return date
     */
//    public function getLastChange()
//    {
//        return $this->_lastChange;
//    }

    /**
     * sets date of last change
     * @param date $value
     * @return MenuItem instance
     */
//    public function setLastChange($value)
//    {
//        if (Gst_Date::isIsoDate($value)) {
//            $this->_lastChange = $value;
//        } else {
//            user_error('incorrect parameter '.$value, E_USER_WARNING);
//        }
//        return $this;
//    }

    /**
     * returns and sets identifier of menu
     * @param int $value
     * @return int
     */
    public function menuId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_menuId = $value;
            }
        }
        return $this->_menuId;
    }

    /**
     * returns and sets identifier of parent id
     * @param int $value
     * @return int
     */
    public function parentId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_parentId = $value;
            }
        }
        return $this->_parentId;
    }

    /**
     * returns and sets ordering
     * @param int $value
     * @return int
     */
    public function ordering($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_ordering = $value;
            }
        }
        return $this->_ordering;
    }

     /**
     * returns and sets flag "publish record"
     * @param bool $value
     * @return bool
     */
    public function published($value = null)
    {
        if (!is_null($value)) {
            $this->_publish = (bool) $value;
        }
        return $this->_publish;
    }

    /**
     * returns and sets target
     * @param string $value
     * @return string
     */
    public function target($value = null)
    {
        if (!is_null($value)) {
            $this->_target = Vars::substr($value, 0, 255);
        }
        return $this->_target;
    }

    /**
     * returns and sets target type
     * @param int $value
     * @return int
     */
    public function targetType($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value === self::TARGET_MODULE || $value === self::TARGET_URL) {
                $this->_targetType = $value;
            }
        }
        return $this->_targetType;
    }

    /**
     * returns target url
     * @return string
     */
    public function getTargetUrl()
    {
        if ($this->_targetType === self::TARGET_URL) {
            return $this->_target;
        }
        return ('[%MOD_'.$this->_target.'%]');
    }

    /**
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return MenuItem
     */
    public function setLang($lang) {
        $this->lang = Vars::substr($lang, 0, 2);
        return $this;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load()
    {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save()
    {
        $this->_err = '';
        $required = array();
        if ($this->_name === '') {
            $required[] = '[%LG_NAME%]';
        }
        if ($this->_target == '') {
            $required[] = 'Cíl menu';
        }
        if (isset($required[0])) {
            $this->_err = '[%LG_ERR_MISSING_VALUES%] '.join(', ', $required);
            return false;
        }

        return $this->_dataLayer->save($this);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        return $this->_dataLayer->delete($this);
    }

    /**
     * move item up
     * @return bool false on error
     */
    public function moveUp()
    {
        return $this->_dataLayer->moveItem($this, 'up');
    }

    /**
     * move item down
     * @return bool false on error
     */
    public function moveDown()
    {
        return $this->_dataLayer->moveItem($this, 'down');
    }

}