'use strict';

Ergo.Menu = Ergo.Menu || {};
Ergo.Menu.Items = Ergo.Menu.Items || {};

Ergo.Menu.Items.Admin = function() {
    var nodeList = $('#menuitemsList'),
        nodeDialog = $('#dialog'),
        recordsList = new Ergo.RecordsList(nodeList),
		lang = nodeList.data('lang').toLowerCase();

    recordsList.url = ERGO_ROOT_URL+'admin/menu/ajax-listitems?lang='+lang;
    recordsList.itemTemplate = '<tr data-parentid="%parent_id%"><td class="level%level%">%name%</td>'
      + '<td data-item-up="%id%" class="clickable">Nahoru</td><td data-item-down="%id%" class="clickable">Dolů</td>'
      + '<td data-item-edit="%id%" class="clickable edit">Upravit</td><td data-item-del="%id%" class="clickable del">Smazat</td></tr>';
    recordsList.callbackLoaded = function(list, data) {
        var parentIds = [];
        nodeList.find('tr[data-parentid]').each(function() {
            var id = $(this).data('parentid');
            if ($.inArray(id, parentIds) === -1) {
                nodeList.find('tr[data-parentid="'+id+'"]:first td[data-item-up]').empty();
                nodeList.find('tr[data-parentid="'+id+'"]:last td[data-item-down]').empty();
                parentIds.push(id);
            }
        });

        var optionsItems = '<option value="0">(hlavní menu)</option>';
        for (var i = 0; i < data.items.length; i++) {
            optionsItems += '<option value="'+data.items[i].id+'" class="level'+data.items[i].level+'">'+data.items[i].name+'</option>';
        }
        $('#detailEdit select[name="parent"]').html(optionsItems);

    };
    recordsList.load();


    var renderEditForm = function(record) {
        var html = $('#detailEdit').html();
        nodeDialog.html(html);

        if (record !== undefined) {
            nodeDialog.find('input[name="id"]').val(record.id);
            nodeDialog.find('input[name="name"]').val(record.name);
            nodeDialog.find('input[name="publish"]').prop('checked', record.publish);
            nodeDialog.find('select[name="parent"]').val(record.parent_id);
            nodeDialog.find('input[name="target_type"][value="'+record.target_type+'"]').prop('checked', true);
            if (record.target_type === 0) {
                nodeDialog.find('select[name="target"]').val(record.target);
            } else {
                nodeDialog.find('input[name="target"]').val(record.target);
            }
        } else {
            nodeDialog.find('input[name="target_type"]:first').prop('checked', true);
        }

        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');
    };

    var saveItem = function(closeAfterSave) {
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        var postdata = {
            id: nodeDialog.find('input[name="id"]').val(),
            name: nodeDialog.find('input[name="name"]').val(),
            publish: nodeDialog.find('input[name="publish"]').prop('checked'),
            parent: nodeDialog.find('select[name="parent"]').val(),
            target_type: nodeDialog.find('input[name="target_type"]:checked').val(),
            target: (nodeDialog.find('input[name="target_type"]:checked').val() === '0'
                    ? nodeDialog.find('select[name="target"]').val()
                    : nodeDialog.find('input[name="target"]').val()),
        };
        $.ajax({
            url: ERGO_ROOT_URL+'admin/menu/ajax-saveitem?lang='+lang,
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
			nodeDialog.find('.msg').empty();
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            }
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/menu/ajax-edititem?id='+id+'&lang='+lang,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            renderEditForm(data.record);
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var confirmDeleteItem = function(id, name) {
        nodeDialog.html('<p>Opravdu chcete vymazat '+name+'?</p>').dialog({
            buttons: [
                { text: "Ok", click: function() { deleteItem(id) } },
                { text: "Storno", click: function() { nodeDialog.dialog('close') } }
            ],
            width: '40%'
            }).dialog('open');
    };

    var deleteItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/menu/ajax-deleteitem?id='+id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDeleteItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDeleteItem( { msg: errorThrown } );
        });
    };
    var handleResponseDeleteItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
            nodeDialog.dialog('close');
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var moveItem = function(id, direction) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/menu/ajax-moveitem?id='+id+'&direction='+direction,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseMoveItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseMoveItem( { msg: errorThrown } );
        });
    };
    var handleResponseMoveItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
//        } else {
//            nodeDialog.html('<p>'+data.msg+'</p>')
//                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); })
        .on('mousedown', '[data-item-add]', function() { renderEditForm(); })
        .on('mousedown', '[data-item-del]', function() {
            confirmDeleteItem($(this).data('item-del'), $(this).closest('tr').find('td').eq(0).html());
        }).on('mousedown', '[data-item-up]', function() { moveItem($(this).data('item-up'), 'up'); })
        .on('mousedown', '[data-item-down]', function() { moveItem($(this).data('item-down'), 'down'); });

};
