<?php
/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu\Admin;

use GstLib\Html;
use GstLib\Template;
use Ergo\Response;
use Menu\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class ControllerItems
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render page with list of records
     */
    public function renderList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->rememberRedirectLogin();
            $this->_app->response()->redirect($this->_app->baseUrl(true));
            return;
        }
        $lang = (isset($_GET['lang']) && $_GET['lang'] === 'en' ? 'en' : 'cs');

        $tpl = new Template($this->_app->getTemplatePath('list_items.htm', Module::NAME, true), true);
        $tpl->replace('TXT_LANG', strtoupper($lang));
        $tpl->replace('INPVAL_TARGET_MODULE', \Menu\MenuItem::TARGET_MODULE);
        $tpl->replace('INPVAL_TARGET_URL', \Menu\MenuItem::TARGET_URL);
        $tpl->replace('INPVAL_MODULE', Html::getOptions($this->_app->getOptionsModuleLinks()), 'raw');
        $this->_app->response()->pageContent($tpl->get());

        $this->_app->response()->pageTitle('Menu | [%LG_SITENAME%]');
    }

    /**
     * render json with list of records
     */
    public function handleList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }
        $lang = (isset($_GET['lang']) && $_GET['lang'] === 'en' ? 'en' : 'cs');

        $list = new \Menu\MenuItems($this->_app->dice()->create('\Menu\DataItem'), FALSE, $lang);

        $ret = new \stdClass();
        $ret->items = $list->listItems();
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render json with single record
     */
    public function handleEdit()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $record = $this->_app->dice()->create('\Menu\MenuItem', array($this->_app->request()->getQuery('id', 'int')));

        $ret = new \stdClass();
        if ($record->loaded()) {
            $ret->record = (object) array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
                'publish' => $record->published(),
                'menu_id' => $record->menuId(),
                'parent_id' => $record->parentId(),
                'target' => $record->target(),
                'target_type' => $record->targetType()
            );
        }

        $ret->status = ($record->loaded() ? 'OK' : 'ERR');
        $ret->msg = ($record->loaded() ? '' : 'Record not found');
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle save record
     */
    public function handleSave()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        $lang = (isset($_GET['lang']) && $_GET['lang'] === 'en' ? 'en' : 'cs');


        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Menu\MenuItem', array($request->getPost('id', 'int')));

        $record->name($request->getPost('name'));
        $record->published($request->getPost('publish', 'bool'));
        $record->parentId($request->getPost('parent', 'int'));
        $record->target($request->getPost('target'));
        $record->targetType($request->getPost('target_type', 'int'));
        $record->setLang($lang);
        if (!$record->save()) {
            $err = ($record->err() === '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete record
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $record = $this->_app->dice()->create('\Menu\MenuItem', array($request->getQuery('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $err = ($record->delete() ? '' : $record->err());
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle move item. Expects
     * #_GET['id'] - record id
     * #_GET['direction'] - 'up'|'down'
     */
    public function handleMove()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $direction = $this->_app->request()->getQuery('direction');
        $record = $this->_app->dice()->create('\Menu\MenuItem', array($request->getQuery('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $ret = $this->_app->dice()->create('\Menu\DataItem', array($this->_app))
                ->moveItem($record, $direction);
            $err = $ret ? '' : 'something wrong';
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

}