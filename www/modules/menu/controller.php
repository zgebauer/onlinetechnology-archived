<?php

/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu;

//use Ergo\Request;
//use Ergo\Response;
//use GstLib\Vars;
use GstLib\Html;
use GstLib\Template;

//use Ergo\Pagination;

/**
 * controller
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class Controller {

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application) {
        $this->_app = $application;
    }

    /**
     * returns box with with tree of categories
     * @return string
     */
    public function renderBox() {
        $tpl = new Template($this->_app->getTemplatePath('box.htm', Module::NAME), true);
        $cnt = $this->_getTree(0);
        if ($cnt === '') {
            return '';
        }
        $tpl->replaceSub('LEVEL', $cnt);
        return $tpl->get();
    }

    /**
     * recursive function, create tree of items
     *
     * @param int $itemId id of current tree node
     * @return string
     */
    private function _getTree($lang, $itemId = 0) {
        static $levelTemplate;
        static $itemTemplate;

        $tplLevel = new Template();
        $tplItem = new Template();

        if (!isset($levelTemplate) || !isset($itemTemplate)) {
            $tpl = new Template($this->_app->getTemplatePath('box.htm', Module::NAME), true);
            $levelTemplate = $tpl->getSub('LEVEL');
            $itemTemplate = $tpl->getSub('ITEM');
        }

        static $list;
        if (!isset($list) || defined('ERGO_TESTING')) {
            //$list = $this->_app->dice()->create('\Menu\MenuItems');
            $list = new \Menu\MenuItems($this->_app->dice()->create('\Menu\DataItem'), true, $this->_app->currentLanguage());
        }

        $cnt = '';
        $tplLevel->set($levelTemplate);
        $counter = 0;
        foreach ($list->getItems($itemId) as $item) {
            if (!$item->published()) {
                continue;
            }
            $tplItem->set($itemTemplate);
            $tplItem->replace('TXT_ITEM', $item->name());
            //$tplItem->replace('TXT_ITEM_ID', $item->instanceId());
            //$url = Ergo_Core::replaceModules($item->getTargetUrl());
            $tplItem->replace('URL_ITEM', $item->getTargetUrl(), 'raw');
            //
//            $url = $this->_app->getUrl(Module::NAME, 'kategorie/'.$item->seo());
//            $tplItem->replace('URL_ITEM', $url);
//            $cssSelected = $this->_app->getCurrentUrl() === $url ? ' selected' : '';
//            $tplItem->replace('CSS_SELECTED', $cssSelected);
            $tplItem->replace('INC_SUBLEVEL', $this->_getTree($lang, $item->instanceId()), 'raw');
            $cnt .= $tplItem->get();
            $counter++;
        }
        if ($counter === 0) {
            return '';
        }
        $tplLevel->replaceSub('ITEM', $cnt);
        //return $this->_app->replaceModules($tplLevel->get());
        return $tplLevel->get();
    }

}
