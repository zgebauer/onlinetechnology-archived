<?php

/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu;

use GstLib\Html;
use GstLib\Tree;

/**
 * tree of items
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class MenuItems {

    /** @var \GstLib\Tree tree of items */
    private $_tree;

    /** @var \Menu\DataItem data layer */
    protected $_dataLayer;

    /** @var bool flag "load only published items" */
    private $_publishedOnly = false;

    /** @var string */
    private $lang;

    /**
     * constructor
     * @param \Menu\DataItem $dataLayer
     * @param bool $publishedOnly true=load only published items
     * @param string $lang
     */
    public function __construct(\Menu\DataItem $dataLayer, $publishedOnly = false, $lang = 'cs') {
        $this->_tree = null;
        $this->_dataLayer = $dataLayer;
        $this->_publishedOnly = (bool) $publishedOnly;
        $allowLang = array('cs', 'en');
        $this->lang = in_array($lang, $allowLang) ? $lang : reset($allowLang);
    }

    /**
     * returns array of items in format [id]=>name with indent
     * @param int $itemId id of current item
     * @param int $level level of tree
     * @return array
     */
    public function options($itemId = 0, $level = 0) {
        $ret = array();
        foreach (self::getItems($itemId) as $item) {
            if ($item->parentId() == $itemId) {
                $name = str_repeat('&nbsp;&nbsp;', $level) . Html::escape($item->name());
                $ret[$item->instanceId()] = $name;
                foreach ($this->options($item->instanceId(), $level + 1) as $key => $value) {
                    $ret[$key] = $value;
                }
            }
        }
        return $ret;
    }

    /**
     * returns array of items as objects with properties id, name, parent_id,level
     * @param int $itemId id of current item
     * @param int $level level of tree
     * @return array
     */
    public function listItems($itemId = 0, $level = 0) {
        $ret = array();
        foreach ($this->getItems($itemId) as $item) {
            if ($item->parentId() === $itemId) {
                $ret[] = array(
                    'id' => $item->instanceId(),
                    'name' => $item->name(),
                    'parent_id' => $item->parentId(),
                    'level' => $level
                );
                foreach ($this->listItems($item->instanceId(), $level + 1) as $value) {
                    $ret[] = $value;
                }
            }
        }
        return $ret;
    }

    /**
     * returns children items
     * @param int $parentId
     * @return array
     */
    public function getItems($parentId = 0) {
        if (is_null($this->_tree)) {
            $criteria = array(array('lang', $this->lang));
            if ($this->_publishedOnly) {
                $criteria[] = array('publish', '1');
            }
            $items = $this->_dataLayer->getRecords(0, null, 'ordering', false, $criteria);
            $this->_tree = new Tree($items);
        }
        return $this->_tree->getChildren($parentId);
    }

    /**
     * returns ids of children items
     * @param int $parentId
     * @return array
     */
    public function getChildrenIds($parentId = 0) {
        $ret = array();
        foreach ($this->getMenuItems($parentId) as $item) {
            $ret[] = $item->instanceId();
        }
        return $ret;
    }

    /**
     * returns parent items of item with given id
     * @param int $itemId identificator of item
     * @return array
     */
    public function getParentItems($itemId) {
        if (is_null($this->_tree)) {
            $criteria = array();
            if ($this->_publishedOnly) {
                $criteria = array(array('publish', '1'));
            }
            $items = $this->_dataLayer->getRecords(0, null, 'ordering', false, $criteria);
            $this->_tree = new Tree($items);
        }
        return $this->_tree->getParents($itemId);
    }

}
