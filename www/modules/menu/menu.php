<?php
/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu;

use GstLib\Vars;

/**
 * menu with items
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class Menu
{
    /** @var menu definovane v layoutu */
    const MAIN_MENU = 1;
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var bool flag "loaded from data tier" */
    protected $_loaded;
    /** @var string name */
    protected $_name;
    /** @var array menu items */
    protected $_items;

    /** @var \Menu\DataMenu data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Menu\DataMenu $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(\Menu\DataMenu $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_name = '';
        $this->_items = null;

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded()
    {
        return $this->_loaded;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null)
    {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 50);
        }
        return $this->_name;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load()
    {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save()
    {
        $this->_err = '';
        $required = array();
        if ($this->_name === '') {
            $required[] = '[%LG_NAME%]';
        }
        if (isset($required[0])) {
            $this->_err = '[%LG_ERR_MISSING_VALUES%] '.join(', ', $required);
            return false;
        }
        return $this->_dataLayer->save($this);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        return $this->_dataLayer->delete($this);
    }

    /**
     * @param int $parentId
     * @return array of menu items
     */
//    public function getItems($parentId = 0)
//    {
//        if (is_null($this->_items)) {
//            $criteria = array(
//                Gst_Db::getCriteria('menu_id', $this->_id)
//            );
//            $this->_items = Ergo_Core::factory('MenuDataItem')
//                ->getItems(0, null, 'ordering', false, $criteria);
//        }
//        $tree = new Gst_Tree($this->_items);
//        return $tree->getChildren($parentId);
//    }

    /**
     * returns  array of items in format [id]=>name with indent
     * @param int $menuId id of current item
     * @param int $level level of tree
     * @return  array
     */
    public function getOptionsItems($menuId = 0, $level = 0)
    {
        $ret = array();
        foreach ($this->getItems($menuId) as $item) {
            if ($item->getParentId() != $menuId) {
                continue;
            }
            $name = str_repeat('&nbsp;&nbsp;', $level).Gst_Html::escape($item->getName());
            $ret[$item->getId()] = $name;
            foreach ($this->getOptionsItems($item->getId(), $level+1) as $key=>$value) {
                $ret[$key] = $value;
            }
        }
        return $ret;
    }

}