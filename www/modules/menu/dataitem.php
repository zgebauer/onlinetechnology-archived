<?php
/**
 * @package    Ergo
 * @subpackage Menu
 */

namespace Menu;

/**
 * data tier for module - menu items
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Menu
 */
class DataItem
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%menu_items%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * fill given instance from database
     * @param \Menu\MenuItem $record
     * @return bool false on error
     */
    public function load(MenuItem $record)
    {
        $sql = 'SELECT id,name,menu_id,parent_id,ordering,publish,target,target_type,`lang`
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->menuId($row['menu_id']);
        $record->published($row['publish']);
        $record->parentId($row['parent_id']);
        $record->ordering($row['ordering']);
        $record->target($row['target']);
        $record->targetType($row['target_type']);
        $record->setLang($row['lang']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Menu\MenuItem $record
     * @return bool false on error
     */
    public function save(MenuItem $record)
    {
        $items = array();
        if ($record->instanceId() === 0) {
            $row = $this->_con->fetchArray(
                'SELECT IFNULL(MAX(ordering),0)+1 AS new_order
                FROM '.self::$_tableName.' WHERE parent_id='.$record->parentId()
            );
            $record->ordering($row['new_order']);
            $items['name'] = $this->_con->escape($record->name());
            $items['publish'] = $this->_con->boolToSql($record->published());
            $items['menu_id'] = $record->menuId();
            $items['parent_id'] = $record->parentId();
            $items['ordering'] = $record->ordering();
            $items['target'] = $this->_con->escape($record->target());
            $items['target_type'] = $record->targetType();
            $items['`lang`'] = $this->_con->escape($record->getLang());
            $sql = 'INSERT INTO '.self::$_tableName.' ('
                .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            try {
                $record->instanceId($this->_con->insert($sql));
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        } else {
            // check previous parent
            $row = $this->_con->fetchArray(
                'SELECT parent_id FROM '.self::$_tableName.' WHERE id='.$record->instanceId()
            );
            $oldParent = (int) $row['parent_id'];

            $items[] = 'name='.$this->_con->escape($record->name());
            $items[] = 'publish='.$this->_con->boolToSql($record->published());
            $items[] = 'menu_id='.$record->menuId();
            $items[] = 'parent_id='.$record->parentId();
            $items[] = 'target='.$this->_con->escape($record->target());
            $items[] = 'target_type='.$record->targetType();
            $sql = array(
                'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId(),
                //'UPDATE '.self::$_tableName.' SET parent_id='.$oldParent.' WHERE parent_id='.$record->instanceId()
            );

            try {
                $this->_con->transaction($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        }
        $this->_fixOrder($record->parentId());
        return TRUE;
    }

    /**
     * delete given instance from database
     * @param \Menu\Products\MenuItem $record
     * @return bool false on error
     */
    public function delete(MenuItem $record)
    {
        try {
            $sql = 'SELECT parent_id FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
            $row = $this->_con->fetchArray($sql);

            $sql = array(
                'DELETE FROM '.self::$_tableName.' WHERE id='.$record->instanceId(),
                'UPDATE '.self::$_tableName.' SET parent_id='.$row['parent_id']
                    .' WHERE parent_id='.$record->instanceId()
            );
            $this->_con->transaction($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_DELETE_FAILED%]');
            \Ergo\Application::logException($exception);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,name,menu_id,parent_id,ordering,publish,target,target_type,`lang`
            FROM '.self::$_tableName.
            $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new MenuItem($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->published($row['publish']);
            $tmp->menuId($row['menu_id']);
            $tmp->parentId($row['parent_id']);
            $tmp->ordering($row['ordering']);
            $tmp->target($row['target']);
            $tmp->targetType($row['target_type']);
            $tmp->setLang($row['lang']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * move item
     * @param \Menu\MenuItem $record
     * @param string $direction [up|down]
     * @return bool false on error
     */
    public function moveItem(MenuItem $record, $direction = 'down')
    {
        switch($direction) {
        case 'up' :
            $sql = 'SELECT id,ordering FROM '.self::$_tableName
            .' WHERE parent_id='.$record->parentId().' AND ordering<'.$record->ordering()
            . ' AND lang=' . $this->_con->escape($record->getLang())
            . ' ORDER BY ordering DESC LIMIT 0,1';
            break;
        case 'down' :
        default :
            $sql = 'SELECT id,ordering FROM '.self::$_tableName
            .' WHERE parent_id='.$record->parentId().' AND ordering>'.$record->ordering()
            . ' AND lang=' . $this->_con->escape($record->getLang())
            . ' ORDER BY ordering ASC LIMIT 0,1';
        }
        $row = $this->_con->fetchArray($sql);
        if (!$row) {
            return true;
        }

        $switchId = $row['id'];
        $switchPos = $row['ordering'];
        $this->_con->free();

        $sql = array(
            'UPDATE '.self::$_tableName.' SET ordering='.$record->ordering().' WHERE id='.$switchId,
            'UPDATE '.self::$_tableName.' SET ordering='.$switchPos.' WHERE id='.$record->instanceId()
        );
        return $this->_con->transaction($sql);
    }

    /**
     * fix duplicate positions of items
     * @param int $parentId
     */
    private function _fixOrder($parentId)
    {
        do {
            $orderOk = true;
            $sql = 'SELECT id,ordering,COUNT(ordering) AS dup
            FROM '.self::$_tableName.' WHERE parent_id='.$parentId.' GROUP BY ordering HAVING dup>1 ORDER BY id';
            foreach ($this->_con->fetchArrayAll($sql) as $row) {
                $rowFix = $this->_con->fetchArray(
                    'SELECT IFNULL(MAX(ordering),0)+1 AS pos FROM '.self::$_tableName.' WHERE parent_id='.$parentId
                );
                $this->_con->query(
                    'UPDATE '.self::$_tableName.' SET ordering='.$rowFix['pos'].' WHERE id='.$row['id']
                );
                $orderOk = false;
            }
        } while (!$orderOk);
    }

}