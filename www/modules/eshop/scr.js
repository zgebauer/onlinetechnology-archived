'use strict';

var EVENT_USER_LOAD = 'user_loaded';
var EVENT_CART_LOAD = 'cart_loaded';

var Users = Users || {};
var Cart = Cart || {};

Users.user = null;
Cart.cart = null;
Users.init = function() {
    $.ajax({
        url: ERGO_ROOT_URL+'eshop/ajax-userinfo',
        dataType: 'json',
        timeout: 6000,
        cache: false
    }).done(function(data, textStatus, jqXHR) {
        if (data.status === 'OK') {
            Users.user = data.user;
            Cart.cart = data.cart;
            $(document).triggerHandler(EVENT_USER_LOAD);
            $(document).triggerHandler(EVENT_CART_LOAD);
        }
    });
};

Users.logout = function() {
    $.ajax({
        url: ERGO_ROOT_URL+'eshop/ajax-logout',
        dataType: 'json',
        timeout: 6000
    }).done(function(data, textStatus, jqXHR) {
        if (data.status === 'OK') {
            if (window.location.href.indexOf('/kosik') !== -1
                    || window.location.href.indexOf('/objednavka') !== -1
                    || window.location.href.indexOf('/profil') !== -1) {
                window.location.reload(true);
            }

            Users.user = null;
            $(document).triggerHandler(EVENT_USER_LOAD);
        }
    });
};

Users.init();

// box about looged user
Users.Box = function() {
    var node = $('#boxUserInfo');

    var showForgotForm = function() {
        node.html('<div id="boxCustomer"><form action="#" class="forgot">'
            + '<p><input type="email" name="email" required /></p>'
            + '<p>' + Ergo.Utils.translate('Forgot password info') + '</p>'
            + '<p><input type="submit" value="' + Ergo.Utils.translate('send') + '" class="buton" /></p></form></div>'
            + '<div class="overlay hidden"></div>');
    };
    var hideOverlay = function() {
        $('#boxUserInfo .overlay').addClass('hidden').removeClass('loading warning').empty();
    };

    var login = function() {
        $('#boxUserInfo .overlay').removeClass('hidden').addClass('loading');
        $.ajax({
            type: 'POST',
            url: ERGO_ROOT_URL+'eshop/ajax-login',
            data: node.find('form').serialize(),
            dataType: 'json',
            timeout: 6000
        }).done(function(data, textStatus, jqXHR) {
            handleResponseLogin(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseLogin( { msg: errorThrown } );
        });
    };
    var handleResponseLogin = function(data) {
        var nodeOverlay = $('#boxUserInfo .overlay');
        nodeOverlay.removeClass('loading');
        if (data.status === 'OK') {
            if (window.location.href.indexOf('users/register') !== -1) {
                window.location.href = ERGO_ROOT_URL;
            }
            if (window.location.href.indexOf('/kosik') !== -1
                    || window.location.href.indexOf('/objednavka') !== -1) {
                window.location.reload(true);
            }
            nodeOverlay.addClass('hidden');
            Users.user = data.user;
            $(document).triggerHandler(EVENT_USER_LOAD);
        } else {
            nodeOverlay.addClass('warning')
                .html('<div class="vert-center"><p>'+data.msg+'</p><p><span class="buton close">' + Ergo.Utils.translate('close') + '</span></p></div>');
        }
    };

    var submitResetPassword = function() {
        $('#boxUserInfo .overlay').removeClass('hidden').addClass('loading');
        $.ajax({
            type: 'POST',
            url: ERGO_ROOT_URL+'eshop/ajax-resetpassword',
            data: node.find('form').serialize(),
            dataType: 'json',
            timeout: 6000
        }).done(function(data, textStatus, jqXHR) {
            handleResponseResetPassword(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseResetPassword( { msg: errorThrown } );
        });
    };
    var handleResponseResetPassword = function(data) {
        var nodeOverlay = $('#boxUserInfo .overlay');
        nodeOverlay.removeClass('loading');
        if (data.status === 'OK') {
            node.find('form.forgot').replaceWith(data.msg);
        } else {
            nodeOverlay.addClass('warning')
                .html('<div class="vert-center"><p>'+data.msg+'</p><p><span class="buton close">' + Ergo.Utils.translate('close') + '</span></p></div>');
        }
    };

    $(document).bind(EVENT_USER_LOAD, node, function(event) {
        if (Users.user === null) {
            node.html('<div id="boxCustomer"><p><strong>' + Ergo.Utils.translate('login') + '</strong> ' + Ergo.Utils.translate('registered users') + '</p>'
                + '<form class="login" method="post" action="#">'
                + '<p><input type="email" title="' + Ergo.Utils.translate('user Login info') + '" id="fldLogin" name="login" placeholder="E-mail" required></p>'
                + '<p><input type="password" title="' + Ergo.Utils.translate('user Login info') + '" id="fldPwd" name="pwd" placeholder="' + Ergo.Utils.translate('Password') + '" required></p>'
                + '<p><input type="submit" class="buton" value="' + Ergo.Utils.translate('user Login') + '"></p>'
                + '<p><a href="'+ERGO_ROOT_URL+'eshop/registrace">' + Ergo.Utils.translate('register') + '</a> | <a href="" data-showforgot="" class="clickable">' + Ergo.Utils.translate('Forgot your password?') + '</a></p>'
                + '</form></div>'
                + '<div class="overlay hidden"></div>');
        } else {
            node.html('<div id="boxCustomer">'
                + '<p>' + Ergo.Utils.translate('user') + ': <span>'+Users.user.name+'</span></p>'
                + '<p><a href="'+ERGO_ROOT_URL+'eshop/profil/objednavky" class="buton">' + Ergo.Utils.translate('My Orders') + '</a></p>'
                + '<p><a href="'+ERGO_ROOT_URL+'eshop/profil" class="buton">' + Ergo.Utils.translate('My profile data') + '</a></p>'
                + '<p><a href="" class="logout clickable buton">' + Ergo.Utils.translate('logout user') + '</a></p></div>'
                + '<div class="overlay hidden"></div>');
        }
    });
    node.on('mousedown', '.logout', function() { Users.logout() });
    node.on('mousedown', '.close', function() { hideOverlay(); });
    node.on('submit', 'form.login', function() { login(); return false; });
    node.on('mousedown', '[data-showforgot]', function() { showForgotForm(); });
    node.on('submit', 'form.forgot', function() { submitResetPassword(); return false; });
    node.on('mousedown', '.close', function() {
        $(this).closest('.overlay').addClass('hidden').removeClass('warning info loading');
    });
};

Users.Box();



Cart.Box = function() {

    $(document).bind(EVENT_CART_LOAD, function() {
        if (Cart.cart === null || Cart.cart.count <= 0) {
            var html = '<div id="kosikTop"><h3>' + Ergo.Utils.translate('Shopping cart') + '</h3><p>' + Ergo.Utils.translate('You have no products selected') + '<br /> <strong>0 ' + Ergo.Utils.translate('items') + '</strong><br>'
                 + ' ' + Ergo.Utils.translate('Choose from our wide selection') + '.</p><h4>0.00<span> ' + Ergo.Utils.translate('w VAT') + '</span></h4><p><a href="'+ERGO_ROOT_URL+'eshop/kosik" class="buton">' + Ergo.Utils.translate('go to shopping cart') + '</a></p></div>';
        } else {
             var html = '<div id="kosikTop"><h3>' + Ergo.Utils.translate('Shopping cart') + '</h3>'
                + '<p>' + Ergo.Utils.translate('Your cart') + ' <br><strong>'+Cart.cart.count+' ' + Ergo.Utils.translate('items') + '</strong><br>'
                + ' ' + Ergo.Utils.translate('the total value') + '</p>'
                + '<h4>'+Cart.cart.priceVat+'<span> ' + Ergo.Utils.translate('w VAT') + '</span></h4>'
                + '<p><a href="'+ERGO_ROOT_URL+'eshop/kosik" class="buton">' + Ergo.Utils.translate('go to shopping cart') + '</a></p></div>';
			setInterval(function() { $.get(ERGO_ROOT_URL+'core/keepalive.php?'+Math.random()); }, 600000);
        }
        $('#boxCart').html(html);
    });
};

Cart.Box();


$('.frmAddToCart').submit(function(event) {
    event.preventDefault();


    $(this).closest('.listItem').animate_from_to('#kosikTop', {
        pixels_per_second: 500,
        shadow_class: 'cart-animated'
    });


    $(this).closest('#prodDetail').animate_from_to('#kosikTop', {
        pixels_per_second: 500,
        shadow_class: 'cart-animated'
    });

    var postdata = $(this).serialize();
    //$(this).find('input').attr('disabled', 'disabled');
    $.ajax({
        type: 'POST',
        url: ERGO_ROOT_URL+'eshop/ajax-cartadd',
        data: postdata,
        dataType: 'json',
        timeout: 6000,
        cache: false
    }).done(function(data, textStatus, jqXHR) {
        if (data.status === 'OK') {
            Cart.cart = data.cart;
            $(document).triggerHandler(EVENT_CART_LOAD);
        }
    });
    return false;
});



// discounts
$(document).bind(EVENT_USER_LOAD, function() {
    if (Users.user === null) {
        $('[data-discount-category]').addClass('hidden');
        $('[data-discount-category] [data-discount-base]').empty();
    } else {
        for (var i=0; i < Users.user.discounts.length; i++) {
            var categoryId = Users.user.discounts[i].category_id;
            var discount = Users.user.discounts[i].discount;
            $('[data-discount-category="'+categoryId+'"]').each(function() {
                var nodePrice = $(this).find('[data-discount-base]');
                var basePrice = nodePrice.data('discount-base');
                var currency = nodePrice.data('discount-currency');
                var discountedPrice = basePrice - (basePrice*discount/100);
				var html = discountedPrice.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				if (currency === 'EUR') {
					html = '<span class="currency">&euro;</span>' + html;
				} else {
					html += ' <span class="currency">Kč</span>';
				}
                nodePrice.html(html);
                $(this).removeClass('hidden');
            });

        }
    }

});





var FormOrder = function(form) {
    var country = '',
		submitAction = null,
		nodeOverlay = form.find('.overlay'),
		$boxPayment = form.find('[data-payment]'),
		$boxDelivery = form.find('[data-delivery]');

   form.on('change', 'input[data-switchdisplay]', function (){
        var fldId = $(this).data('switchdisplay');
        if ($(this).is(':checked')) {
            $('#'+fldId).removeClass('hidden');
        } else {
            $('#'+fldId).addClass('hidden');
        }
    });

    // set payment block
    form.on('change', '#fldCountry, #fldCountry2, #fldAnotherAddress', function (){
		var country = $('#fldAnotherAddress').prop('checked') ? $('#fldCountry2').val() : $('#fldCountry').val();
		$boxPayment.find('p[data-country!="'+country+'"]').addClass('hidden');
		$boxPayment.find('p[data-country="'+country+'"]').removeClass('hidden');
		$boxPayment.find('p[data-country="'+country+'"]:first input:radio:first').prop('checked', true);

		$boxDelivery.find('p[data-country!="'+country+'"]').addClass('hidden');
		$boxDelivery.find('p[data-country="'+country+'"]').removeClass('hidden');
		$boxDelivery.find('p[data-country="'+country+'"]:first input:radio:first').prop('checked', true);
    });

    var recalcOrder = function() {
       var subtotal = parseFloat(form.find('[data-subtotal]').data('subtotal'));
       var subtotalVat = parseFloat(form.find('[data-subtotal-vat]').data('subtotal-vat'));
       var pricePayment = parseFloat($boxPayment.find('input[name="payment"]:checked').data('fee'));
       var priceDelivery = parseFloat($boxDelivery.find('input[name="delivery"]:checked').data('fee'));
       var freeDelivery = parseFloat(form.find('input[data-free-delivery]:checked').data('free-delivery'));
       priceDelivery = (subtotalVat >= freeDelivery ? 0 : priceDelivery);
       var grandtotal = formatCurrency((subtotal + pricePayment + priceDelivery), form.data('currency'), 2);
       var grandtotalVat = formatCurrency((subtotalVat + pricePayment + priceDelivery), form.data('currency'), 2);
       form.find('[data-grandtotal]').html(grandtotal);
       form.find('[data-grandtotal-vat]').html(grandtotalVat);
    };

    form.on('change', 'input:radio', recalcOrder);
    $('#fldCountry').trigger('change');
    recalcOrder();

    form.on('mousedown', 'button[type="submit"], input[type="submit"]', function (){
        submitAction = $(this).data('action');
    });

    form.submit(function (){
        switch(submitAction) {
            case 'save':
                var subtotalVat = parseFloat(form.find('[data-subtotal-vat]').data('subtotal-vat'));
                var pricePayment = parseFloat(form.find('input[name="payment"]:checked').data('fee'));
                var priceDelivery = parseFloat(form.find('input[name="delivery"]:checked').data('fee'));
                var freeDelivery = parseFloat(form.find('input[name="delivery"]:checked').data('free-delivery'));
                priceDelivery = (subtotalVat >= freeDelivery ? 0 : priceDelivery);
                var grandtotalVat = (subtotalVat + pricePayment + priceDelivery);
                var html = '<h1>' + Ergo.Utils.translate('Recapitulation of goods before ordering') + '</h1><div class="stepBYstep3"></div>'
                    + form.find('fieldset:first')[0].outerHTML
                    + '<fieldset><legend>' + Ergo.Utils.translate('Client') + '</legend><table>'
                    + '<tr><th>' + Ergo.Utils.translate('Name') + ':</th><td>'+form.find('[name="first_name"]').val()+'</td>'
                    + '<th>' + Ergo.Utils.translate('Surname') + ':</th><td>'+form.find('[name="last_name"]').val()+'</td></tr>'
                    + '<tr><th>E-mail:</th><td>'+form.find('[name="email"]').val()+'</td>'
                    + '<th>' + Ergo.Utils.translate('Phone') + ':</th><td>'+form.find('[name="phone"]').val()+'</td></tr>'
                    + '<tr><th>' + Ergo.Utils.translate('Firm') + ':</th><td colspan="3">'+form.find('[name="company"]').val()+'</td></tr>'
                    + '<tr><th>' + Ergo.Utils.translate('IR') + ':</th><td>'+form.find('[name="ic"]').val()+'</td>'
                    + '<th>' + Ergo.Utils.translate('TAX no') + ':</th><td>'+form.find('[name="dic"]').val()+'</td></tr></table></fieldset>'
                    + '<fieldset><legend>' + Ergo.Utils.translate('Billing address') + '</legend><table>'
                    + '<tr><th>' + Ergo.Utils.translate('Street no') + ':</th><td>'+form.find('[name="address"]').val()+'</td>'
                    + '<th>' + Ergo.Utils.translate('City') + ':</th><td>'+form.find('[name="city"]').val()+'</td></tr>'
                    + '<tr><th>' + Ergo.Utils.translate('ZIP') + ':</th><td>'+form.find('[name="post_code"]').val()+'</td>'
                    + '<th>' + Ergo.Utils.translate('Country') + ':</th><td>'+form.find('[name="country"] option:selected').text()+'</td></tr></table></fieldset>';
                    if ($('[name="use_delivery_address"]').prop('checked')) {
                        html += '<fieldset><legend>' + Ergo.Utils.translate('Delivery address') + '</legend><table>'
                        + '<tr><th>' + Ergo.Utils.translate('Street no') + ':</th><td>'+form.find('[name="delivery_address"]').val()+'</td>'
                        + '<th>' + Ergo.Utils.translate('City') + ':</th><td>'+form.find('[name="delivery_city"]').val()+'</td></tr>'
                        + '<tr><th>' + Ergo.Utils.translate('ZIP') + ':</th><td>'+form.find('[name="delivery_post_code"]').val()+'</td>'
                        + '<th>' + Ergo.Utils.translate('Country') + ':</th><td>'+form.find('[name="delivery_country"] option:selected').text()+'</td></tr></table></fieldset>';
                    }
                    html += '<fieldset><legend>' + Ergo.Utils.translate('Method of payment') + '</legend>'
                        + '<p>'+form.find('[name="payment"]:checked').siblings('label').text()+'</p></fieldset>'
                        + '<fieldset><legend>' + Ergo.Utils.translate('Delivery Method') + '</legend>'
                        + '<p>'+form.find('[name="delivery"]:checked').siblings('label').text()+'</p></fieldset>'
                        + '<fieldset><legend>' + Ergo.Utils.translate('Your comment') + '</legend>'
                        + '<p>'+form.find('[name="note"]').val().replace(/\n/g, '<br>')+'</p></fieldset>';
                    html += '<fieldset class="ordersPrices"><p> ' + Ergo.Utils.translate('price with WAT') + ': <strong>' + formatCurrency(subtotalVat, form.data('currency'), 2) + '</strong> + ' + Ergo.Utils.translate('payment') + ': <strong>'
                        + formatCurrency(pricePayment, form.data('currency'), 2) + '</strong> + ' + Ergo.Utils.translate('Delivery') + ': <strong>' + formatCurrency(priceDelivery, form.data('currency'), 2) + '</strong> = ' + Ergo.Utils.translate('Total price with WAT') + ': <strong> '
                        + formatCurrency(grandtotalVat, form.data('currency'), 2) + '</strong> </p></fieldset>';
                html += '<p> <input type="submit" value="' + Ergo.Utils.translate('Modify') + '" class="buton" /> <input type="submit" value="' + Ergo.Utils.translate('Binding order') + '" data-action="save-force"  class="buton" /></p>';

                nodeOverlay.html(html).removeClass('hidden').addClass('confirm');
                break;
            case 'save-force':
                nodeOverlay.empty().removeClass('confirm').addClass('loading');
                $.ajax({
                    url: ERGO_ROOT_URL+'eshop/ajax-saveorder',
                    method: 'post',
                    data: form.serialize(),
                    dataType: 'json',
                    cache: false
                }).done(function(data, textStatus, jqXHR) {
                    handleResponseSave(data);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    handleResponseSave( { msg: errorThrown } );
                });
                break;
            default:
                form.find('.overlay').empty().addClass('hidden');
                break;
        }
        return false;
    });

    var handleResponseSave = function(data) {
        if (data.status === 'OK') {
            var html = '<h1>' + Ergo.Utils.translate('Finishing order') + '</h1><div class="stepBYstep4"></div><p>' + Ergo.Utils.translate('ordered info') + ' '+data.id+'.<br />'
                + ' ' + Ergo.Utils.translate('Thx') + '</p>';
            nodeOverlay.html(html).removeClass('loading').addClass('order-ok');
            Cart.cart = null;
            $(document).triggerHandler(EVENT_CART_LOAD);
        } else {
            nodeOverlay.html('<h1>' + Ergo.Utils.translate('Finishing order') + '</h1><div class="stepBYstep4"></div><p>'+data.msg+'</p><input type="submit" value="' + Ergo.Utils.translate('close') + '" class="buton" />').removeClass('loading').addClass('warning');;
        }
    };

};

if ($('#frmOrder').length > 0) {
   FormOrder($('#frmOrder'));
}


Users.FormRegister = function(form) {

    form.submit(function() {
        var postdata = form.serialize(); // must be serialized here
        form.find('.msg').html(' ' + Ergo.Utils.translate('sendingForm') + ' ');
        form.find('input').prop('disabled', true);
        $.ajax({
            type: 'POST',
            url: ERGO_ROOT_URL+'eshop/ajax-register',
            data: postdata,
            dataType: 'json',
            timeout: 6000
        }).done(function(data, textStatus, jqXHR) {
            handleResponseRegister(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseRegister( { msg: errorThrown } );
        });
        return false;
    });

    var handleResponseRegister = function(data) {
        if (data.status === 'OK') {
            window.location.href = ERGO_ROOT_URL;
        } else {
            form.find('.msg').html(data.msg);
            form.find('input').prop('disabled', false);
        }
    };

};

if ($('#frmRegister').length > 0) {
    Users.FormRegister($('#frmRegister'));
}


Users.FormNewPassword = function(form) {

    form.submit(function() {
        var postdata = form.serialize(); // must be serialized here
        form.find('.msg').html(' ' + Ergo.Utils.translate('sendingForm') + ' ');
        form.find('input').prop('disabled', true);
        $.ajax({
            type: 'POST',
            url: ERGO_ROOT_URL+'eshop/ajax-newpassword',
            data: postdata,
            dataType: 'json',
            timeout: 6000,
        }).done(function(data, textStatus, jqXHR) {
            handleResponse(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponse( { msg: errorThrown } );
        });
        return false;
    });

    var handleResponse = function(data) {
        if (data.status === 'OK') {
            window.location.href = ERGO_ROOT_URL;
        } else {
            form.find('.msg').html(data.msg);
            form.find('input').prop('disabled', false);
        }
        form.find('input').prop('disabled', false);
    };
};

if ($('#frmNewPassword').length > 0) {
    Users.FormNewPassword($('#frmNewPassword'));
};

Users.FormProfile = function(form) {

    form.submit(function() {
        var postdata = form.serialize(); // must be serialized here
        form.find('.msg').html(' ' + Ergo.Utils.translate('sendingForm') + ' ');
        form.find('input').prop('disabled', true);
        $.ajax({
            type: 'POST',
            url: ERGO_ROOT_URL+'eshop/ajax-save',
            data: postdata,
            dataType: 'json',
            timeout: 6000
        }).done(function(data, textStatus, jqXHR) {
            handleResponse(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponse( { msg: errorThrown } );
        });
        return false;
    });

    var handleResponse = function(data) {
        if (data.status === 'OK') {
            form.find('.msg').html('Udaje byly ulozeny');
        } else {
            form.find('.msg').html(data.msg);
        }
        form.find('input').prop('disabled', false);
    };

};

if ($('#frmProfile').length > 0) {
    Users.FormProfile($('#frmProfile'));
}

Users.FormChangePassword = function(form) {

    form.submit(function() {
        var postdata = form.serialize(); // must be serialized here
        form.find('.msg').html(' ' + Ergo.Utils.translate('sendingForm') + ' ');
        form.find('input').prop('disabled', true);
            $.ajax({
                type: 'POST',
                url: ERGO_ROOT_URL+'eshop/ajax-changepassword',
                data: postdata,
                dataType: 'json',
                timeout: 6000
            }).done(function(data, textStatus, jqXHR) {
                handleResponse(data);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                handleResponse( { msg: errorThrown } );
            });
            return false;
        });

        var handleResponse = function(data) {
        if (data.status === 'OK') {
            form.find('.msg').html('Heslo bylo nastaveno, použijte ho při dalším přhlášení')
                .addClass(data.status === 'OK' ? 'success' : 'error');
            form[0].reset();
        } else {
            form.find('.msg').html(data.msg);
        }
        form.find('input').prop('disabled', false);
    };

};

if ($('#frmChangePassword').length > 0) {
    Users.FormChangePassword($('#frmChangePassword'));
}



var FormProfileOrders = function(node) {
    var node = node;

    node.find('[data-showorder]').mousedown(function() {
        var orderId = $(this).data('showorder');
        var boxOrder = $('#boxOrderDetail'+orderId);
        if (boxOrder.is(':empty')) {
            boxOrder.addClass('loading').removeClass('hidden');
            $.ajax({
                type: 'GET',
                url: ERGO_ROOT_URL+'eshop/ajax-orderdetail?id='+orderId,
                dataType: 'json',
                timeout: 6000
            }).done(function(data, textStatus, jqXHR) {
                handleResponse(data);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                handleResponse( { msg: errorThrown } );
            });
        } else {
            if (boxOrder.hasClass('hidden')) {
                boxOrder.removeClass('hidden');
                $(this).html(' ' + Ergo.Utils.translate('hide details') + ' ');
            } else {
                boxOrder.addClass('hidden');
                $(this).html('' + Ergo.Utils.translate('show details') + ' ');
            }
        }
    });

    var handleResponse = function(data) {
        if (data.status === 'OK') {
            var order, i, html, boxOrder;

            order = data.order;
            boxOrder = $('#boxOrderDetail'+order.id);
            $('#boxProfileOrders [data-showorder="'+order.id+'"]').html(' ' + Ergo.Utils.translate('hide details') + ' ');
            html = '<div class="orderedItems"><fieldset><legend>' + Ergo.Utils.translate('Client') + '</legend><table><tr><td>' + Ergo.Utils.translate('Name') + ':</td><td>'+order.firstName+'</td>'
                + '<td>' + Ergo.Utils.translate('Surname') + ':</td><td>'+order.lastName+'</td></tr><tr>'
                + '<td>E-mail:</td><td>-</td>'
                + '<td>' + Ergo.Utils.translate('Phone') + ':</td><td>'+order.phone+'</td></tr><tr>'
                + '<td>' + Ergo.Utils.translate('Firm') + ':</td><td colspan="3">'+order.company+'</td></tr>'
                + '<tr><td>' + Ergo.Utils.translate('IR') + ':</td><td>'+order.ic+'</td>'
                + '<td>' + Ergo.Utils.translate('TAX no') + ':</td><td>'+order.dic+'</td></tr></table></fieldset>'
                + '<fieldset><legend>' + Ergo.Utils.translate('Billing address') + '</legend><table><tr><td>' + Ergo.Utils.translate('Street no') + ':</td><td>'+order.address+'</td>'
                + '<td>' + Ergo.Utils.translate('City') + ':</td><td>'+order.city+'</td></tr>'
                + '<tr><td>' + Ergo.Utils.translate('ZIP') + ':</td><td>'+order.postCode+'</td>'
                + '<td>' + Ergo.Utils.translate('Country') + ':</td><td>'+order.country+'</td></tr></table></fieldset>'
                + '<fieldset><legend>' + Ergo.Utils.translate('Delivery address') + '</legend><table><tr><td>' + Ergo.Utils.translate('Street no') + ':</td><td>'+order.deliveryAddress+'</td>'
                + '<td>' + Ergo.Utils.translate('City') + ':</td><td>'+order.deliveryCity+'</td></tr><tr>'
                + '<td>' + Ergo.Utils.translate('ZIP') + ':</td><td>'+order.deliveryPostCode+'</td>'
                + '<td>' + Ergo.Utils.translate('Country') + ':</td><td>'+order.deliveryCountry+'</td></tr></table></fieldset>'
                + '<fieldset><legend>' + Ergo.Utils.translate('Your comment') + '</legend>'+order.note.replace(/\n/g, '<br>')+'</fieldset>'
                + '<table><thead><tr><th>' + Ergo.Utils.translate('Name') + '</th><th>' + Ergo.Utils.translate('pieces') + '</th><th>' + Ergo.Utils.translate('Price pieces with WAT') + '</th><th>' + Ergo.Utils.translate('Total price without WAT') + '</th></tr></thead><tbody>';
                for (i = 0;  i < order.items.length; i++) {
                    html += '<tr><td>'+order.items[i].product+'</td><td>'+order.items[i].quantity+'</td>'
                        + '<td class="number">'+order.items[i].priceUnit+'</td>'
                        + '<td class="number">'+order.items[i].priceRow+'</td></tr>';
                };
                html += '</tbody></table>'
                + '<p class="ordersPrices">' + Ergo.Utils.translate('price with WAT') + ': <strong>'+order.subtotalVat+'</strong> Kč + '
                + ' ' + Ergo.Utils.translate('payment') + ': <strong>'+order.pricePayment+'</strong> Kč + '
                + ' ' + Ergo.Utils.translate('Delivery') + ': <strong>'+order.priceDelivery+'</strong> Kč = '
                + ' ' + Ergo.Utils.translate('Total price with WAT') + ': <strong>'+order.grandTotalVat+'</strong> Kč</p></div>';
                if (order.parcel !== '') {
                    html += '<p>' + Ergo.Utils.translate('Your shipment can') + ' <a href="'
                    + 'http://www.ceskaposta.cz/cz/nastroje/sledovani-zasilky.php?barcode='+order.parcel+'&go=ok">' + Ergo.Utils.translate('watch online') + '</a> ' + Ergo.Utils.translate('Web Czech Post') + '</p>';
                }
            boxOrder.html(html).removeClass('loading');
        };
    }
}

if ($('#boxProfileOrders').length > 0) {
   FormProfileOrders($('#boxProfileOrders'));
}