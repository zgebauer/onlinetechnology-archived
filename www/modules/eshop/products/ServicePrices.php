<?php

namespace Eshop\Products;


class ServicePrices extends \Ergo\Component {

    /** @var Cnb\Repository $repository */
    private $repository;

    /**
     * @param \Ergo\ApplicationInterface $application
     * @param Repository $repository
     */
    public function __construct(\Ergo\ApplicationInterface $application, Repository $repository) {
        parent::__construct($application);
        $this->repository = $repository;
    }

//    public function getAmountEurPerCzk() {
//		return $this->repository->getAmountEurPerCzk();
//	}
//
//	public function saveAmountEurPerCzk($amount) {
//		$this->repository->saveAmountEurPerCzk($amount);
//	}

    public function recalcPricesEur() {

        $repositoryCnb = $this->app->getInstance(\Eshop\Cnb\Repository::getName());

        try {
            $prevRate = $repositoryCnb->getRateEur();
        } catch (\Eshop\Cnb\Exception $exc) {
            $prevRate = 0;
        }

        $cnb = new \GstLib\Cnb\Rates();
        $newRate = $cnb->getRate('EUR');
        $repositoryCnb->saveRateEur($newRate->getRate());
        if ($prevRate !== $newRate->getRate()) {
            $this->repository->calcPricesEur($newRate->getRate());
        }
    }

}

