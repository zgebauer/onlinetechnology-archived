<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Vars;
use GstLib\Html;

/**
 * item in list of products
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Product {

    /** @var string last error message */
    protected $_err;

    /** @var int instance identifier */
    protected $_id;

    /** @var bool flag "loaded from data tier" */
    protected $_loaded;

    /** @var string name */
    protected $_name;

    /** @var string */
    private $nameEn;

    /** @var string identifier for url */
    protected $_seo;

    /** @var bool flag publish record */
    protected $_publish;

    /** @var string text of record */
    protected $_text;

    /** @var string */
    private $textEn;

    /** @var string content for meta tag description */
    protected $_metaDescription;

    /** @var string content for meta tag keywords */
    protected $_metaKeywords;

    /** @var float price without VAT in CZK */
    protected $_price;

    /** @var float price with VAT in CZK */
    protected $_priceVat;

    /** @var float percent of VAT */
    protected $_vat;

    /** @var float price without VAT in EUR */
    protected $priceEur;

    /** @var float price with VAT in EUR */
    protected $priceEurVat;

    /** @var bool flag "bestseller" */
    protected $_bestseller;

    /** @var bool flag "featured" */
    protected $_featured;

    /** @var \Eshop\Products\Image main image assigned to instance */
    private $_mainImage;

    /** @var array gallery images */
    private $_galleryImages;

    /** @var array icons */
    private $_icons;

    /** @var array */
    private $_categoriesIds;

    /** @var array */
    private $_iconsIds;

    /** @var array */
    private $_crossSellProducts;

    /** @var array */
    private $_variantProducts;

    /** @var int */
    private $_categoryId;
    private $_popisModulu;
    /** @var string */
    private $popisModuluEn;
    private $_zapojeni;
    /** @var string */
    private $zapojeniEn;
    private $_dokumentace;
    /** @var string */
    private $dokumentaceEn;
    private $_firmware;
    /** @var string */
    private $firmwareEn;
    private $_programy;
    /** @var string */
    private $programyEn;

	/** @var string */
	private $stockInfo = '';

	/** @var string */
	private $stockInfoEn = '';

    /** @var \Eshop\Products\DataProduct data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Eshop\Products\Data $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(\Eshop\Products\DataProduct $dataLayer, $instanceId = 0) {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_name = $this->_seo = '';
        $this->nameEn = '';
        $this->_publish = false;
        $this->_text = '';
        $this->textEn = '';
        $this->_metaDescription = $this->_metaKeywords = '';
        $this->_price = $this->_priceVat = $this->_vat = 0;
        $this->priceEur = $this->priceEurVat = 0;
        $this->_bestseller = $this->_featured = false;
        $this->_mainImage = null;
        $this->_galleryImages = null;
        $this->_icons = null;
        $this->_categoriesIds = null;
        $this->_iconsIds = null;
        $this->_crossSellProducts = null;
        $this->_variantProducts = null;
        $this->_categoryId = 0;

        $this->_popisModulu = $this->_zapojeni = $this->_dokumentace = $this->_firmware = $this->_programy = '';
        $this->popisModuluEn = '';
        $this->zapojeniEn = '';
        $this->dokumentaceEn = '';
        $this->firmwareEn = '';
        $this->programyEn = '';

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null) {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null) {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded() {
        return $this->_loaded;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null) {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 50);
        }
        return $this->_name;
    }

    /**
     * @return string
     */
    public function getNameEn() {
        return $this->nameEn;
    }

    /**
     * @param string $nameEn
     * @return Product
     */
    public function setNameEn($nameEn) {
        $this->nameEn = Vars::substr($nameEn, 0, 50);
        return $this;
    }

    /**
     * returns and sets seo
     * @param string $value
     * @return string
     */
    public function seo($value = null) {
        if (!is_null($value)) {
            $this->_seo = Vars::substr(Html::fixForUrl($value), 0, 52);
        }
        return $this->_seo;
    }

    /**
     * returns and sets description text
     * @param string $value
     * @return string text
     */
    public function text($value = null) {
        if (!is_null($value)) {
            $this->_text = Vars::substr($value, 0, 65535);
        }
        return $this->_text;
    }

    /**
     * @return string
     */
    public function getTextEn() {
        return $this->textEn;
    }

    /**
     * @param string $textEn
     * @return Product
     */
    public function setTextEn($textEn) {
        $this->textEn = $textEn;
        return $this;
    }

        /**
     * returns and sets flag "publish record"
     * @param bool $value
     * @return bool
     */
    public function published($value = null) {
        if (!is_null($value)) {
            $this->_publish = (bool) $value;
        }
        return $this->_publish;
    }

    /**
     * returns and sets meta description
     * @param string $value
     * @return string
     */
    public function metaDescription($value = null) {
        if (!is_null($value)) {
            $this->_metaDescription = Vars::substr($value, 0, 255);
        }
        return $this->_metaDescription;
    }

    /**
     * returns and sets meta keywords
     * @param string $value
     * @return string
     */
    public function metaKeywords($value = null) {
        if (!is_null($value)) {
            $this->_metaKeywords = Vars::substr($value, 0, 255);
        }
        return $this->_metaKeywords;
    }

    /**
     * returns and sets price without VAT in CZK
     * @param float $value
     * @return float
     */
    public function price($value = null) {
        if (!is_null($value)) {
            $this->_price = (float) $value;
        }
        return $this->_price;
    }

    /**
     * returns and sets price with VAT in CZK
     * @param float $value
     * @return float
     */
    public function priceVat($value = null) {
        if (!is_null($value)) {
            $this->_priceVat = (float) $value;
        }
        return $this->_priceVat;
    }

    /**
     * returns and sets VAT
     * @param float $value
     * @return float
     */
    public function vat($value = null) {
        $value = (float) $value;
        if ($value > 0 && $value < 100) {
            $this->_vat = (float) $value;
        }
        return $this->_vat;
    }

    /**
     * @return float price in EUR without VAT
     */
    public function getPriceEur() {
        return $this->priceEur;
    }

    /**
     * @param float $priceEur price in EUR without VAT
     * @return Product
     */
    public function setPriceEur($priceEur) {
        $this->priceEur = (float) $priceEur;
        return $this;
    }

    /**
     * @return float price in EUR with VAT
     */
    public function getPriceEurVat() {
        return $this->priceEurVat;
    }

    /**
     *
     * @param type $priceEurVat price in EUR with VAT
     * @return Product
     */
    public function setPriceEurVat($priceEurVat) {
        $this->priceEurVat = (float) $priceEurVat;
        return $this;
    }

    /**
     * returns and sets flag "bestseller"
     * @param bool $value
     * @return bool
     */
    public function bestseller($value = null) {
        if (!is_null($value)) {
            $this->_bestseller = (bool) $value;
        }
        return $this->_bestseller;
    }

    /**
     * returns and sets flag "featured" (nejprodavanejsi)
     * @param bool $value
     * @return bool
     */
    public function featured($value = null) {
        if (!is_null($value)) {
            $this->_featured = (bool) $value;
        }
        return $this->_featured;
    }

    /**
     * returns and sets identifier of category
     * @param int $value
     * @return int
     */
    public function categoryId($value = null) {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_categoryId = $value;
            }
        }
        return $this->_categoryId;
    }

    /**
     * returns and sets "popis modulu"
     * @param string $value
     * @return string
     */
    public function popisModulu($value = null) {
        if (!is_null($value)) {
            $this->_popisModulu = Vars::substr($value, 0, 65535);
        }
        return $this->_popisModulu;
    }

    /**
     * @return string
     */
    public function getPopisModuluEn() {
        return $this->popisModuluEn;
    }

    /**
     * @param string $popisModuluEn
     * @return Product
     */
    public function setPopisModuluEn($popisModuluEn) {
        $this->popisModuluEn = $popisModuluEn;
        return $this;
    }

    /**
     * returns and sets "zapojeni"
     * @param string $value
     * @return string
     */
    public function zapojeni($value = null) {
        if (!is_null($value)) {
            $this->_zapojeni = Vars::substr($value, 0, 65535);
        }
        return $this->_zapojeni;
    }

    /**
     * @return string
     */
    public function getZapojeniEn() {
        return $this->zapojeniEn;
    }

    /**
     * @param string $zapojeniEn
     * @return Product
     */
    public function setZapojeniEn($zapojeniEn) {
        $this->zapojeniEn = $zapojeniEn;
        return $this;
    }

    /**
     * returns and sets "zapojeni"
     * @param string $value
     * @return string
     */
    public function dokumentace($value = null) {
        if (!is_null($value)) {
            $this->_dokumentace = Vars::substr($value, 0, 65535);
        }
        return $this->_dokumentace;
    }

    /**
     * @return string
     */
    public function getDokumentaceEn() {
        return $this->dokumentaceEn;
    }

    /**
     * @param string $dokumentaceEn
     * @return Product
     */
    public function setDokumentaceEn($dokumentaceEn) {
        $this->dokumentaceEn = $dokumentaceEn;
        return $this;
    }

    /**
     * returns and sets "firmware"
     * @param string $value
     * @return string
     */
    public function firmware($value = null) {
        if (!is_null($value)) {
            $this->_firmware = Vars::substr($value, 0, 65535);
        }
        return $this->_firmware;
    }

    /**
     * @return string
     */
    public function getFirmwareEn() {
        return $this->firmwareEn;
    }

    /**
     *
     * @param string $firmwareEn
     * @return Product
     */
    public function setFirmwareEn($firmwareEn) {
        $this->firmwareEn = $firmwareEn;
        return $this;
    }

    /**
     * returns and sets "programy"
     * @param string $value
     * @return string
     */
    public function programy($value = null) {
        if (!is_null($value)) {
            $this->_programy = Vars::substr($value, 0, 65535);
        }
        return $this->_programy;
    }

    /**
     * @return string
     */
    public function getProgramyEn() {
        return $this->programyEn;
    }

    /**
     * @param string $programyEn
     * @return Product
     */
    public function setProgramyEn($programyEn) {
        $this->programyEn = $programyEn;
        return $this;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load() {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save() {
        $this->_err = '';
        $required = array();
        if ($this->_name === '') {
            $required[] = '[%LG_NAME%]';
        }
//        if ($this->_text === '') {
//            $required[] = '[%LG_TEXT%]';
//        }
        if ($this->_price < 0) {
            $required[] = '[%LG_PRICE%]';
        }
        if (isset($required[0])) {
            $this->_err = '[%LG_ERR_MISSING_VALUES%] ' . join(', ', $required);
            return false;
        }
        if ($this->_seo === '') {
            $this->seo($this->_name);
        }
        if ($this->_metaDescription === '') {
            $this->metaDescription($this->_name);
        }
        if ($this->_metaKeywords === '') {
            $this->metaKeywords(join(', ', Vars::parseKeywords($this->_name)));
        }

        return $this->_dataLayer->save($this);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete() {
        if (!$this->getMainImage()->delete()) {
            return false;
        }
        foreach ($this->getImages() as $image) {
            $image->delete();
        }
        return $this->_dataLayer->delete($this);
    }

    /**
     * returns main image assigned to instance
     * @return \Eshop\Products\Image
     */
    public function getMainImage() {
        if (is_null($this->_mainImage)) {
            $this->_mainImage = $this->_dataLayer->getImage($this->instanceId());
        }
        return $this->_mainImage;
    }

    /**
     * returns ids of icons assigned to instance
     * @return array
     */
    public function getIconsIds() {
        if (is_null($this->_iconsIds)) {
            $this->_iconsIds = $this->_dataLayer->getIconIdsByProduct($this);
        }
        return $this->_iconsIds;
    }

    /**
     * sets ids of icons assigned to instance
     * @param array $ids
     */
    public function setIconsIds(array $ids) {
        $tmp = array();
        foreach ($ids as $iconId) {
            $iconId = (int) $iconId;
            if ($iconId > 0) {
                $tmp[] = $iconId;
            }
        }
        $this->_iconsIds = $tmp;
    }

    /**
     * returns gallery images assigned to instance
     * @return array of instances \Eshop\Products\GalleryImage
     */
    public function getImages() {
        if ($this->_id == 0) {
            return array();
        }
        if (is_null($this->_galleryImages)) {
            $this->_galleryImages = $this->_dataLayer->getGalleryImages($this);
        }
        return $this->_galleryImages;
    }

    /**
     * returns icons assigned to instance
     * @return array of instances \Eshop\Products\Icon
     */
    public function getIcons() {
        if ($this->_id == 0) {
            return array();
        }
        if (is_null($this->_icons)) {
            $this->_icons = $this->_dataLayer->getIcons($this);
        }
        return $this->_icons;
    }

    /**
     * calculate price with VAT in CZK
     * @return float
     */
    public function calcPriceVat() {
        $this->_priceVat = Vars::calcPriceVat($this->price(), $this->vat(), true);
        return $this->priceVat();
    }

    /**
     * calculate price with VAT in EUR
     * @return float
     */
    public function calcPriceEurVat() {
        $this->priceEurVat = Vars::calcPriceVat($this->getPriceEur(), $this->vat(), true);
        return $this->getPriceEurVat();
    }

    /**
     * returns cross-sell products assigned to instance
     * @return array
     */
    public function getCrossSellProducts() {
        if ($this->_id === 0) {
            return array();
        }
        if (is_null($this->_crossSellProducts)) {
            $this->_crossSellProducts = $this->_dataLayer->getCrossSellProducts($this->instanceId());
        }
        return $this->_crossSellProducts;
    }

    /**
     * returns variant products assigned to instance
     * @return array
     */
    public function getVariantProducts() {
        if ($this->_id === 0) {
            return array();
        }
        if (is_null($this->_variantProducts)) {
            $this->_variantProducts = $this->_dataLayer->getVariantProducts($this->instanceId());
        }
        return $this->_variantProducts;
    }

    /**
     * save instance to data tier as new record
     * @return bool false on error
     */
    public function saveCopy() {
        return $this->_dataLayer->saveCopy($this);
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getNameByLang($lang) {
        return ($lang === 'en' ? $this->getNameEn() : $this->name());
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getTextByLang($lang) {
        return ($lang === 'en' ? $this->getTextEn() : $this->text());
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getPopisModuluByLang($lang) {
        return ($lang === 'en' ? $this->getPopisModuluEn() : $this->popisModulu());
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getFirmwareByLang($lang) {
        return ($lang === 'en' ? $this->getFirmwareEn() : $this->firmware());
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getProgramyByLang($lang) {
        return ($lang === 'en' ? $this->getProgramyEn() : $this->programy());
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getZapojeniByLang($lang) {
        return ($lang === 'en' ? $this->getZapojeniEn() : $this->zapojeni());
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getDokumentaceByLang($lang) {
        return ($lang === 'en' ? $this->getDokumentaceEn() : $this->dokumentace());
    }

    /**
     * @param string $lang
     * @return float
     */
    public function getPriceByLang($lang) {
        return ($lang === 'en' ? $this->getPriceEur() : $this->price());
    }

    /**
     * @param string $lang
     * @return float
     */
    public function getPriceVatByLang($lang) {
        return ($lang === 'en' ? $this->getPriceEurVat() : $this->priceVat());
    }

	/**
	 * @return string
	 */
	public function getStockInfo() {
		return $this->stockInfo;
	}

	/**
	 * @param string $stockInfo
	 * @return \Eshop\Products\Product
	 */
	public function setStockInfo($stockInfo) {
		$this->stockInfo = Vars::substr($stockInfo, 0, 50);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStockInfoEn() {
		return $this->stockInfoEn;
	}

	/**
	 * @param string $stockInfoEn
	 * @return \Eshop\Products\Product
	 */
	public function setStockInfoEn($stockInfoEn) {
		$this->stockInfoEn = Vars::substr($stockInfoEn, 0, 50);
		return $this;
	}

    /**
     * @param string $lang
     * @return string
     */
    public function getStockInfoByLang($lang) {
        return ($lang === 'en' ? $this->getStockInfoEn() : $this->getStockInfo());
    }
}
