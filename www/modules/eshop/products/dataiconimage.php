<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Upload;
use GstLib\UploadException;
use GstLib\UserFile;
use GstLib\Resizer;
use GstLib\ImageSharpen;
use GstLib\Filesystem;

/**
 * data tier for module - icons
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataIconImage
{
    /** @var \Eshop\Config configuration */
    private $_config;
    /** @var int number of files per directory */
    private static $_filesPerDir = UserFile::FILES_PER_DIR_1000;

    /**
     * constructor
     * @param \Eshop\Config $config
     */
    public function __construct(\Eshop\Config $config)
    {
        $this->_config = $config;
    }

    /**
     * delete given instance
     * @param \Eshop\Products\IconImage $record
     * @return bool false on error
     */
    public function delete(IconImage $record)
    {
        $ret = $this->deleteAttachment($record, Image::IMG_THUMB);
        if ($ret) {
            $ret = $this->deleteAttachment($record, Image::IMG_ORIG);
        }
        return $ret;
    }

    /**
     * save given instance and upload file
     * @param \Eshop\Products\IconImage $record
     * @param string $field with uploaded file
     * @return bool false on error
     */
    public function save(IconImage $record, $field)
    {
        if (!Upload::isUploaded($field)) {
            return true;
        }
        $uploader = new Upload();
        $uploader->allowExtensions(array('jpg', 'jpeg', 'gif', 'png'));
        $uploader->allowMimeTypes(array('image/jpeg', 'image/gif', 'image/png', 'image/x-png'));
        $uploader->maxUploadFileSize($this->_config->maxUploadImageFileSize());

        $baseDir = $this->_config->dirIcons(IconImage::IMG_ORIG);
        $targetDir = $baseDir.UserFile::idToDir($record->instanceId(), self::$_filesPerDir);
        if (!is_dir($targetDir)) {
            Filesystem::mkDir($targetDir);
        }
        if (!is_dir($targetDir)) {
            user_error('directory '.$targetDir.' not found ', E_USER_WARNING);
            return false;
        }
        $fileName = UserFile::createFileName($record->instanceId(), $uploader->getFileName($field));
        try {
            $uploader->saveAs($fileName, $targetDir, $field, true);
        } catch (UploadException $e) {
            $record->err('[%LG_ERR_UPLOAD_'.$e->getCode().'%]');
            return false;
        }

        UserFile::delete($record->instanceId(), $baseDir, $fileName, self::$_filesPerDir);
        return true;
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,name,text
            FROM '.self::$_tableName.
            $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new IconImage($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->text($row['text']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns full path and name of file attached tu given instance or empty string if file nor found
     * @param \Eshop\Products\IconImage $record
     * @param enum $type type of attached file
     * @return string
     */
    public function getAttachmentFullPath(IconImage $record, $type)
    {
        $ret = UserFile::getFullPath($record->instanceId(), $this->_config->dirIcons($type), self::$_filesPerDir);

        // thumbnail
        if ($type !== IconImage::IMG_ORIG) {
            $source = UserFile::getFullPath(
                $record->instanceId(), $this->_config->dirImages(IconImage::IMG_ORIG), self::$_filesPerDir
            );
            if ($ret != '') {
                // check filemtime, get filemtime of local file from name in format id_timestamp_name.ext
                $tmp = explode('_', basename($ret));
                if ($tmp[1] != filemtime($source)) {
                    unlink($ret);
                    $ret = '';
                }
            }
            if ($ret == '') {
                if (!is_file($source)) {
                    return '';
                }
                // try make new image
                $dir = $this->_config->dirImages($type).UserFile::idToDir($record->instanceId(), self::$_filesPerDir);
                if (!is_dir($dir)) {
                    Filesystem::mkDir($dir);
                }
                if (!is_dir($dir)) {
                    return '';
                }
                $target = $dir.$record->instanceId().'_'.filemtime($source).'_'.UserFile::stripId(basename($source));
                $size = $this->_config->sizeImage($type);
                Resizer::createThumbnailMaxHeight($source, $target, $size[1], 95, new ImageSharpen());
                $ret = UserFile::getFullPath(
                    $record->instanceId(), $this->_config->dirImages($type), self::$_filesPerDir
                );
            }
        }
        return $ret;
    }

    /**
     * returns url of file attached to given instance or empty string if file is not found
     * @param \Eshop\Products\IconImage $record
     * @param enum $type type of atached file
     * @return string
     */
    public function getAttachmentUrl(IconImage $record, $type)
    {
        $fullPath = $this->getAttachmentFullPath($record, $type);
        if ($fullPath === '') {
            return '';
        }
        return str_replace(ERGO_ROOT_DIR, ERGO_ROOT_URL, $fullPath)
            .($type === Image::IMG_ORIG ? '?'.filemtime($fullPath) : '');
    }

    /**
     * delete file attached to given instance
     * @param \Eshop\Products\IconImage $record
     * @param enum $type type of attached file
     * @return bool false on error
     */
    public function deleteAttachment(IconImage $record, $type)
    {
        $fullPath = $this->getAttachmentFullPath($record, $type);
        if ($fullPath !== '') {
            $pattern = dirname($fullPath).'/'.$record->instanceId().'_*.*';
            $files = glob($pattern);
            if (is_array($files)) {
                foreach ($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
            }
        }
        return true;
    }

}