<?php

namespace Eshop\Products;

class Mapper {

    /**
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application) {
        $this->con = $connection;
        $this->app = $application;
    }

    /**
     * @param float $amountPerEur amount of CZK per 1 EUR
     */
    public function calcPricesEur($amountPerEur) {
    	$amountPerEur = (float)$amountPerEur;
		$sql = "UPDATE eshop_products SET price_eur = CONVERT(price/". $amountPerEur . ", DECIMAL(8,2)),
			price_eur_vat = CONVERT(price/". $amountPerEur . "*(1+(vat/100)), DECIMAL(8,2))";
		$this->con->query($sql);
    }

}
