<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\UserFile;

/**
 * data tier for module - product icons
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataIcon
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_products_icons%]';
    private static $_tableNameRelProductIcon = '[%eshop_rel_product_icon%]';
    private static $_tableNameProducts = '[%eshop_products%]';
    /** @var \Eshop\Config configuration */
    private $_config;

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Eshop\Config $config module configuration
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Eshop\Config $config,
        \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_config = $config;
        $this->_app = $application;
    }

    /**
     * fill given instance from database
     * @param \Eshop\Products\Icon $record
     * @return bool false on error
     */
    public function load(Icon $record)
    {
        $sql = 'SELECT `id`,`name`,`text`,`name_en`,`text_en`
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->text($row['text']);
        $record->setNameEn($row['name_en'])
			->setTextEn($row['text_en']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Eshop\Products\Icon $record
     * @return bool false on error
     */
    public function save(Icon $record)
    {
        $items = array();
        if ($record->instanceId() === 0) {
            $items['name'] = $this->_con->escape($record->name());
            $items['text'] = $this->_con->escape($record->text());
            $items['`name_en`'] = $this->_con->escape($record->getNameEn());
            $items['`text_en`'] = $this->_con->escape($record->getTextEn());
            $sql = 'INSERT INTO '.self::$_tableName.' ('.
                implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            $newId = $this->_con->insert($sql);
            if (!$newId) {
                return false;
            }
            $record->instanceId($newId);
        } else {
            $items[] = 'name='.$this->_con->escape($record->name());
            $items[] = 'text='.$this->_con->escape($record->text());
            $items[] = '`name_en`='.$this->_con->escape($record->getNameEn());
            $items[] = '`text_en`='.$this->_con->escape($record->getTextEn());
            $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
            if (!$this->_con->query($sql)) {
                return false;
            };
        }

        return true;
    }

    /**
     * delete given instance from database
     * @param \Eshop\Products\Icon $record
     * @return bool false on error
     */
    public function delete(Icon $record)
    {
        self::deleteAttachment($record, Icon::IMG_ORIG);
        $sql = 'DELETE FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        return $this->_con->query($sql);
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT `id`,`name`,`text`,`name_en`,`text_en` FROM '.self::$_tableName.
            $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Icon($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->text($row['text']);
            $tmp->setNameEn($row['name_en'])
				->setTextEn($row['text_en']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns icons which has at least one product assigned in category
     * @param int $categoryId id of category
     * @return array of instances \Eshop\Products\Icon
     */
    public function getNonEmptyIcons($categoryId)
    {
        $sql = 'SELECT DISTINCT i.id,i.name,i.text FROM '.self::$_tableName.' AS i
            INNER JOIN '.self::$_tableNameRelProductIcon.' AS r ON r.icon_id=i.id
            INNER JOIN '.self::$_tableNameProducts.' AS p ON r.product_id=p.id
            WHERE p.category_id='.$categoryId.' AND p.publish='.$this->_con->boolToSql(TRUE);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Icon($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->text($row['text']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array())
    {
        $sql = 'SELECT COUNT(*) FROM '.self::$_tableName.$this->_con->where($criteria);
        return $this->_con->count($sql);
    }

    /**
     * returns image by id
     * @param int $instanceId
     * @return \Eshop\Products\IconImage
     */
    public function getImage($instanceId)
    {
        return new IconImage($this->_app->dice()->create('\Eshop\Products\DataIconImage'), $instanceId);
    }

    /**
     * returns icons assigned to product
     * @param int $productId
     * @return array of instances \Eshop\Products\Icon
     */
    public function getImagesByProductId($productId)
    {
        $sql = 'SELECT id,name,text FROM '.self::$_tableName.' AS i
            INNER JOIN eshop_rel_product_icon AS r ON i.id=r.icon_id WHERE r.product_id='.$productId
            .' ORDER BY i.name';
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Icon($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->text($row['text']);
            $ret[] = $tmp;
        }
        return $ret;
    }

}