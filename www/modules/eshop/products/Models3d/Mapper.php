<?php

namespace Eshop\Products\Models3d;

use Eshop\Config;
use GstLib\Filesystem\IdFile;
use GstLib\Upload\Constraints;
use GstLib\Upload\Uploader;

class Mapper {

	/**
	 * @var Config
	 */
	private $config;

	public function __construct(Config $config) {
		$this->config = $config;
	}

	/**
	 * @param int $productId
	 * @return File
	 */
	public function getFile($productId) {
		$file = new File($productId);
		$file->setFullPath($this->getAttachmentFullPath($file))
			->setUrl($this->getAttachmentUrl($file));
		return $file;
	}

	/**
	 * @param File $file
	 * @param string $field with uploaded file
	 * @throws \RuntimeException
	 */
	public function upload(File $file, $field) {
		if (!Uploader::isUploaded($field)) {
			return;
		}

		$constraints = (new Constraints())
			->allowExtensions(Config::allowedExtensions3d())
			->allowMimeTypes(Config::allowedMime3d());

		$uploader = new Uploader();
		$dir = $this->config->dirModels3d() . IdFile::idToDir($file->getProductId(), $this->config->getFilesPerDir());
		$old = @umask(0);
		if (!is_dir($dir) && !mkdir($dir, 0777, true) && !is_dir($dir)) {
			throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
		}
		@umask($old);

		$fileName = IdFile::createFileName($file->getProductId(), $uploader->getFileName($field));
		$uploader->upload($dir . $fileName, $field, $constraints);
		IdFile::delete($file->getProductId(), $dir, $fileName, $this->config->getFilesPerDir());
	}

	/**
	 * @return bool false on error
	 */
	public function delete(File $file) {
		$fullPath = $this->getAttachmentFullPath($file);
		return $fullPath === '' ? true : unlink($fullPath);
	}

	/**
	 * @return string
	 */
	private function getAttachmentFullPath(File $file) {
		return IdFile::getFullPath($file->getProductId(), $this->config->dirModels3d(), $this->config->getFilesPerDir());
	}

	/**
	 * @return string
	 */
	public function getAttachmentUrl(File $record) {
		$fullPath = $this->getAttachmentFullPath($record);
		if ($fullPath === '') {
			return '';
		}
		return str_replace(ERGO_ROOT_DIR, ERGO_ROOT_URL, $fullPath) . '?' . filemtime($fullPath);
	}

}
