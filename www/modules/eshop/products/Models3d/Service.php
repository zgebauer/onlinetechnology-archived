<?php

namespace Eshop\Products\Models3d;

class Service {

	/**
	 * @var Mapper
	 */
	private $mapper;

	public function __construct(Mapper $mapper) {
		$this->mapper = $mapper;
	}

	/**
	 * @param int $productId
	 * @return File
	 */
	public function getFile($productId) {
		return $this->mapper->getFile($productId);
	}

	/**
	 * @param File $file
	 * @param string $field
	 * @throws \GstLib\Upload\Exception
	 */
	public function upload(File $file, $field) {
		$this->mapper->upload($file, $field);
		$file->setUrl($this->mapper->getAttachmentUrl($file));
	}

	public function delete(File $file) {
		$this->mapper->delete($file);
	}
}
