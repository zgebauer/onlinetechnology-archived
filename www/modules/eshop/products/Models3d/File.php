<?php

namespace Eshop\Products\Models3d;

class File {

	/**
	 * @var int
	 */
	private $productId;

	/**
	 * @var string
	 */
	private $fullPath = '';

	/**
	 * @var string
	 */
	private $url = '';

	/**
	 * @param int $productId
	 */
	public function __construct($productId) {
		$this->productId = (int)$productId;
	}

	/**
	 * @return int
	 */
	public function getProductId() {
		return $this->productId;
	}

	/**
	 * @return string
	 */
	public function getFullPath() {
		return $this->fullPath;
	}

	/**
	 * @param string $fullPath
	 * @return $this
	 */
	public function setFullPath($fullPath) {
		$this->fullPath = (string)$fullPath;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @param string $url
	 * @return File
	 */
	public function setUrl($url) {
		$this->url = (string)$url;
		return $this;
	}

}