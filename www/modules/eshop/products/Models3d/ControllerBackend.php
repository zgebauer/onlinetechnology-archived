<?php

namespace Eshop\Products\Models3d;

use Ergo\ApplicationInterface;
use Ergo\Response;
use Eshop\Module;
use Eshop\Products\Product;

use Tracy\Debugger;

class ControllerBackend {

	/**
	 * @var Service
	 */
	private $service;

	/**
	 * @var ApplicationInterface
	 */
	private $app;

	public function __construct(ApplicationInterface $application, Service $service) {
		$this->app = $application;
		$this->service = $service;
	}

	public function handleUpload() {
		if ($this->app->getUser() === null || !$this->app->getUser()->hasPermission(Module::NAME)) {
			$this->app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$ret = new \stdClass();
		$ret->status = 'ERR';

		try {
			$request = $this->app->request();
			$productId = $request->getPost('id', 'int');
			/** @var Product $product */
			$product = $this->app->dice()->create(Product::class, [$productId]);
			if (!$product->loaded()) {
				throw new \RuntimeException('product not found ID:' . $productId);
			}
			$file = new File($productId);
			$this->service->upload($file, 'file');
			$ret->url_model3d = $file->getUrl();
			$ret->status = 'OK';
		} catch (\Exception $exception) {
			Debugger::log($exception);
			$ret->msg = $exception->getMessage();
		}
		$this->app->response()->setOutputJson($ret);
	}

	public function handleDelete() {
		if ($this->app->getUser() === null || !$this->app->getUser()->hasPermission(Module::NAME)) {
			$this->app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$ret = new \stdClass();
		$ret->status = 'ERR';

		try {
			$request = $this->app->request();
			$productId = $request->getQuery('id', 'int');
			$this->service->delete(new File($productId));
			$ret->status = 'OK';
		} catch (\Exception $exception) {
			Debugger::log($exception);
			$ret->msg = $exception->getMessage();
		}
		$this->app->response()->setOutputJson($ret);
	}

}