<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Vars;
use GstLib\Html;

/**
 * icon with name and text assgned to products
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 * @see        \Eshop\Products\Product
 */
class Icon {

	/** @var string last error message */
	protected $_err;

	/** @var int instance identifier */
	protected $_id;

	/** @var bool flag "loaded from data tier" */
	protected $_loaded;

	/** @var string name */
	protected $_name;

	/** @var string text of record */
	protected $_text;

	/** @var string */
	private $nameEn;

	/** @var string */
	private $textEn;

	/** @var \Eshop\Products\IconImage main image assigned to instance */
	private $_mainImage;

	/** @var \Eshop\Products\DataIcon data layer */
	protected $_dataLayer;

	/**
	 * create instance and fill it with data by given identifier
	 * @param \Eshop\Products\DataIcon $dataLayer
	 * @param int $instanceId identifier of instance
	 */
	public function __construct(DataIcon $dataLayer, $instanceId = 0) {
		$this->_err = '';
		$this->_id = 0;
		$this->_loaded = false;
		$this->_name = '';
		$this->_text = '';
		$this->_mainImage = null;
		$this->nameEn = '';
		$this->textEn = '';
		$this->_mainImage = null;

		$this->_dataLayer = $dataLayer;

		$this->instanceId($instanceId);
		if ($this->_id != 0) {
			$this->load();
		}
	}

	/**
	 * returns and sets last error message
	 * @param string $value
	 * @return string
	 */
	public function err($value = null) {
		if (!is_null($value)) {
			$this->_err = $value;
		}
		return $this->_err;
	}

	/**
	 * returns and sets instance identifier
	 * @param int $value
	 * @return int
	 */
	public function instanceId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_id = $value;
			}
		}
		return $this->_id;
	}

	/**
	 * returns flag "record loaded"
	 * @return bool
	 */
	public function loaded() {
		return $this->_loaded;
	}

	/**
	 * returns and sets name
	 * @param string $value
	 * @return string
	 */
	public function name($value = null) {
		if (!is_null($value)) {
			$this->_name = Vars::substr($value, 0, 100);
		}
		return $this->_name;
	}

	/**
	 * returns and sets text of article
	 * @param string $value
	 * @return string text
	 */
	public function text($value = null) {
		if (!is_null($value)) {
			$this->_text = $value;
		}
		return $this->_text;
	}

	/**
	 * @return string
	 */
	public function getNameEn() {
		return $this->nameEn;
	}

	/**
	 * @param string$nameEn
	 * @return Icon
	 */
	public function setNameEn($nameEn) {
		$this->nameEn = Vars::substr($nameEn, 0, 100);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTextEn() {
		return $this->textEn;
	}

	/**
	 * @param string $textEn
	 * @return Icon
	 */
	public function setTextEn($textEn) {
		$this->textEn = $textEn;
		return $this;
	}

	/**
	 * fill instance from data tier
	 * @return bool false on error
	 */
	public function load() {
		$this->_loaded = $this->_dataLayer->load($this);
		return $this->_loaded;
	}

	/**
	 * save instance to data tier
	 * @return bool false on error
	 */
	public function save() {
		$this->_err = '';
		$required = array();
		if ($this->_name === '') {
			$required[] = '[%LG_NAME%]';
		}
		if (isset($required[0])) {
			$this->_err = '[%LG_ERR_MISSING_VALUES%] ' . join(', ', $required);
			return false;
		}

		return $this->_dataLayer->save($this);
	}

	/**
	 * delete instance from data tier
	 * @return bool false on error
	 */
	public function delete() {
		$this->_err = '';
		return $this->_dataLayer->delete($this);
	}

	/**
	 * returns main image assigned to instance
	 * @return \Eshop\Products\Image
	 */
	public function getMainImage() {
		if (is_null($this->_mainImage)) {
			$this->_mainImage = $this->_dataLayer->getImage($this->instanceId());
		}
		return $this->_mainImage;
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public function getNameByLang($lang) {
		return $lang === 'en' ? $this->getNameEn() : $this->name();
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public function getTextByLang($lang) {
		return $lang === 'en' ? $this->getTextEn() : $this->text();
	}

}
