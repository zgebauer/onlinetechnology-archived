<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Upload;
use GstLib\UploadException;
use GstLib\UserFile;
use GstLib\Resizer;
use GstLib\ImageSharpen;
use GstLib\Filesystem;

/**
 * data tier for module - images
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataImage
{
    /** @var \Eshop\Config configuration */
    private $_config;
    /** @var int number of files per directory */
    private static $_filesPerDir = UserFile::FILES_PER_DIR_1000;

    /**
     * constructor
     * @param \Eshop\Config $config
     */
    public function __construct(\Eshop\Config $config)
    {
        $this->_config = $config;
    }

    /**
     * delete given instance
     * @param \Eshop\Products\Image $record
     * @return bool false on error
     */
    public function delete(Image $record)
    {
        $ret = $this->deleteAttachment($record, Image::IMG_THUMB);
        $ret2 = $this->deleteAttachment($record, Image::IMG_THUMB2);
        if ($ret && $ret2) {
            $ret = $this->deleteAttachment($record, Image::IMG_ORIG);
        }
        return $ret;
    }

    /**
     * save given instance and upload file
     * @param \Eshop\Products\Image $record
     * @param string $field with uploaded file
     * @return bool false on error
     */
    public function save(Image $record, $field)
    {
        if (!Upload::isUploaded($field)) {
            return true;
        }
        $uploader = new Upload();
        $uploader->allowExtensions(array('jpg', 'jpeg', 'gif', 'png'));
        $uploader->allowMimeTypes(array('image/jpeg', 'image/gif', 'image/png', 'image/x-png'));
        $uploader->maxUploadFileSize($this->_config->maxUploadImageFileSize());
//        $imgSize = $this->_config->sizeImage(Image::IMG_ORIG);
//        $uploader->imgWidth($imgSize[0]);
//        $uploader->imgHeight($imgSize[1]);

        $baseDir = $this->_config->dirImages(Image::IMG_ORIG);
        $targetDir = $baseDir.UserFile::idToDir($record->instanceId(), self::$_filesPerDir);
        if (!is_dir($targetDir)) {
            Filesystem::mkDir($targetDir);
        }
        if (!is_dir($targetDir)) {
            user_error('directory '.$targetDir.' not found ', E_USER_WARNING);
            return false;
        }
        $fileName = UserFile::createFileName($record->instanceId(), $uploader->getFileName($field));
        try {
            $uploader->saveAs($fileName, $targetDir, $field, true);
        } catch (UploadException $e) {
            $record->err('[%LG_ERR_UPLOAD_'.$e->getCode().'%]');
            return false;
        }

        UserFile::delete($record->instanceId(), $baseDir, $fileName, self::$_filesPerDir);
        // delete thumbnail and create new one
        $this->deleteAttachment($record, Image::IMG_THUMB);
        $this->getAttachmentFullPath($record, Image::IMG_THUMB);
        $this->deleteAttachment($record, Image::IMG_THUMB2);
        $this->getAttachmentFullPath($record, Image::IMG_THUMB2);
        return true;
    }

    /**
     * update
     * @param \Eshop\Products\Image $record
     * @return bool false on error
     */
    public function update(Image $record)
    {
        $items = array();
        $items[] = 'description='.$this->_con->escape($record->description());
        $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
        return $this->_con->query($sql);
    }

    /**
     * returns full path and name of file attached tu given instance or empty string if file nor found
     * @param \Eshop\Products\Image $record
     * @param enum $type type of attached file
     * @return string
     */
    public function getAttachmentFullPath(Image $record, $type)
    {
        $ret = UserFile::getFullPath($record->instanceId(), $this->_config->dirImages($type), self::$_filesPerDir);

        // thumbnail
        if ($type !== Image::IMG_ORIG) {
            $source = UserFile::getFullPath(
                $record->instanceId(), $this->_config->dirImages(Image::IMG_ORIG), self::$_filesPerDir
            );
            if ($ret != '') {
                // check filemtime, get filemtime of local file from name in format id_timestamp_name.ext
                $tmp = explode('_', basename($ret));
                if ($tmp[1] != filemtime($source)) {
                    unlink($ret);
                    $ret = '';
                }
            }
            if ($ret == '') {
                if (!is_file($source)) {
                    return '';
                }
                // try make new image
                $dir = $this->_config->dirImages($type).UserFile::idToDir($record->instanceId(), self::$_filesPerDir);
                if (!is_dir($dir)) {
                    Filesystem::mkDir($dir);
                }
                if (!is_dir($dir)) {
                    return '';
                }
                $target = $dir.$record->instanceId().'_'.filemtime($source).'_'.UserFile::stripId(basename($source));
                list($width, $height) = $this->_config->sizeImage($type);
                Resizer::createThumbnailMax($source, $target, $width, $height, 95, new ImageSharpen());
                $ret = UserFile::getFullPath(
                    $record->instanceId(), $this->_config->dirImages($type), self::$_filesPerDir
                );
            }
        }
        return $ret;
    }

    /**
     * returns url of file attached to given instance or empty string if file is not found
     * @param \Eshop\Products\Image $record
     * @param enum $type type of atached file
     * @return string
     */
    public function getAttachmentUrl(Image $record, $type)
    {
        $fullPath = $this->getAttachmentFullPath($record, $type);
        if ($fullPath === '') {
            return '';
        }
        return str_replace(ERGO_ROOT_DIR, ERGO_ROOT_URL, $fullPath)
            .($type === Image::IMG_ORIG ? '?'.filemtime($fullPath) : '');
    }

    /**
     * delete file attached to given instance
     * @param \Eshop\Products\Image $record
     * @param enum $type type of attached file
     * @return bool false on error
     */
    public function deleteAttachment(Image $record, $type)
    {
        $fullPath = $this->getAttachmentFullPath($record, $type);
        if ($fullPath !== '') {
            $pattern = dirname($fullPath).'/'.$record->instanceId().'_*.*';
            $files = glob($pattern);
            if (is_array($files)) {
                foreach ($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
            }
        }
        return true;
    }
    /**
     * save given instance and upload file
     * @param \Eshop\Products\Image $record
     * @param string $field with uploaded file
     * @return bool false on error
     */
    public function saveCopy(Image $record, $newId)
    {
        $baseDir = $this->_config->dirImages(Image::IMG_ORIG);
        $targetDir = $baseDir.UserFile::idToDir($newId, self::$_filesPerDir);
        if (!is_dir($targetDir)) {
            Filesystem::mkDir($targetDir);
        }
        if (!is_dir($targetDir)) {
            user_error('directory '.$targetDir.' not found ', E_USER_WARNING);
            return FALSE;
        }
        $src = $this->getAttachmentFullPath($record, Image::IMG_ORIG);
        if ($src === '') {
            return TRUE;
        }
        $srcName = UserFile::stripId(basename($src));
        $target = $targetDir.UserFile::createFileName($newId, $srcName);
        if (!copy($src, $target)) {
            $record->err('cannot copy image');
            return FALSE;
        }
        return TRUE;
    }

}