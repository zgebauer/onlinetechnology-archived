<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

/**
 * main image assigned to product
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 * @see        \Eshop\Products\Product
 */
class Image
{
    /** @var type of image - original image */
    const IMG_ORIG = 0;
    /** @var type of image - thumbnail in list of product */
    const IMG_THUMB = 1;
    /** @var type of image - thumbnail in product detail */
    const IMG_THUMB2 = 2;
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;

    /** @var \Eshop\Products\DataImage data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Eshop\Products\DataImage $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(\Eshop\Products\DataImage $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * save instance to data tier
     * @param string name of input with uploaded file
     * @return bool false on error
     */
    public function save($field)
    {
        $this->_err = '';
        return $this->_dataLayer->save($this, $field);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        $this->_err = '';
        return $this->_dataLayer->delete($this);
    }

    /**
     * return url of attached file or empty string if file not found
     * @param enum $type type of attached file
     * @return string
     */
    public function getAttachmentUrl($type)
    {
        return $this->_dataLayer->getAttachmentUrl($this, $type);
    }

    /**
     * save instance to data tier as new record
     * @param int $productId id of product which new item is related to
     * @return bool false on error
     */
    public function saveCopy($productId)
    {
        return $this->_dataLayer->saveCopy($this, $productId);
    }

}