<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Vars;

/**
 * image in gallery assigned to product
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 * @see        \Eshop\Products\Product
 */
class GalleryImage
{
    /** @var type of image - original image */
    const IMG_ORIG = 0;
    /** @var type of image - thumbnail */
    const IMG_THUMB = 1;
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var bool flag "loaded from data tier" */
    protected $_loaded;
    /** @var string name */
    protected $_name;
    /** @var bool flag publish record */
    protected $_publish;
    /** @var int product id */
    protected $_parentId;
    /** @var int ordering */
    protected $_ordering;
    /** @var string description */
    protected $_description;
    /** @var \Eshop\Products\DataGalleryImage data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Eshop\Products\DataGalleryImage $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(DataGalleryImage $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_name = '';
        $this->_publish = false;
        $this->_parentId = 0;
        $this->_ordering = 0;
        $this->_description = '';

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded()
    {
        return $this->_loaded;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null)
    {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 255);
        }
        return $this->_name;
    }

    /**
     * returns and sets flag "publish record"
     * @param bool $value
     * @return bool
     */
    public function published($value = null)
    {
        if (!is_null($value)) {
            $this->_publish = (bool) $value;
        }
        return $this->_publish;
    }

    /**
     * return and sets product id
     * @param int $value
     * @return int
     */
    public function parentId($value = 0)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_parentId = $value;
            }
        }
        return $this->_parentId;
    }

    /**
     * returns and sets ordering
     * @param int $value
     * @return int
     */
    public function ordering($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_ordering = $value;
            }
        }
        return $this->_ordering;
    }

    /**
     * returns and sets description
     * @param string $value
     * @return string
     */
    public function description($value = null)
    {
        if (!is_null($value)) {
            $this->_description = Vars::substr($value, 0, 150);
        }
        return $this->_description;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load()
    {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @param string name of input with uploaded file
     * @return bool false on error
     */
    public function save($field)
    {
        $this->_err = '';
        return $this->_dataLayer->save($this, $field);
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function update()
    {
        $this->_err = '';
        if ($this->_id < 0) {
            return false;
        }
        return $this->_dataLayer->update($this);
    }
    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        $this->_err = '';
        return $this->_dataLayer->delete($this);
    }

    /**
     * return url of attached file or empty string if file not found
     * @param enum $type type of attached file
     * @return string
     */
    public function getAttachmentUrl($type)
    {
        return $this->_dataLayer->getAttachmentUrl($this, $type);
    }

    /**
     * move image up
     * @return bool false on error
     */
    public function moveUp()
    {
        return $this->_dataLayer->move($this, 'up');
    }

    /**
     * move image down
     * @return bool false on error
     */
    public function moveDown()
    {
        return $this->_dataLayer->move($this, 'down');
    }

    /**
     * return parent product of instance
     * @staticvar \Eshop\Products\Product $product
     * @return \Eshop\Products\Product
     */
    public function getProduct()
    {
        static $product;
        if (!isset($product)) {
            $product = $this->_dataLayer->getProductByImage($this);
        }
        return $product;
    }

    /**
     * save instance to data tier as new record
     * @param int $productId id of product which new item is related to
     * @return bool false on error
     */
    public function saveCopy($productId)
    {
        return $this->_dataLayer->saveCopy($this, $productId);
    }

}