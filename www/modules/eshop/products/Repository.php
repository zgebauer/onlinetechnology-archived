<?php

namespace Eshop\Products;

class Repository extends \Ergo\Component {

	/** @var Mapper $mapper */
	private $mapper;

    /**
     * @param \Ergo\ApplicationInterface $application
     * @param Mapper $mapper
     */
	public function __construct(\Ergo\ApplicationInterface $application, Mapper $mapper) {
		parent::__construct($application);
		$this->mapper = $mapper;
	}

    /**
     * @param float $amountPerEur amoun of CZK per 1 EUR
     */
    public function calcPricesEur($amountPerEur) {
        $this->mapper->calcPricesEur($amountPerEur);
    }

}
