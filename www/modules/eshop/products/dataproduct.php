<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

/**
 * data tier for module - products
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataProduct
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_products%]';
    private static $_tableNameRelProductIcon = '[%eshop_rel_product_icon%]';
    private static $_tableNameRelProductCross = '[%eshop_rel_product_cross%]';
    private static $_tableNameRelProductVariant = '[%eshop_rel_product_variant%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_app = $application;
    }

    /**
     * fill given instance from database
     * @param \Eshop\Products\Product $record
     * @return bool false on error
     */
    public function load(Product $record)
    {
        $sql = 'SELECT id,name,`name_en`,seo,publish,category_id,text,`text_en`,meta_desc,meta_keys,price,price_vat,vat,
            `price_eur`,`price_eur_vat`,
            bestseller,featured,popis_modulu,`popis_modulu_en`,zapojeni,`zapojeni_en`,dokumentace,`dokumentace_en`,
            firmware,`firmware_en`,programy,`programy_en`,`stock_info`,`stock_info_en`
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->setNameEn($row['name_en']);
        $record->seo($row['seo']);
        $record->published($row['publish']);
        $record->categoryId($row['category_id']);
        $record->text($row['text']);
        $record->setTextEn($row['text_en']);
        $record->metaDescription($row['meta_desc']);
        $record->metaKeywords($row['meta_keys']);
        $record->price($row['price']);
        $record->priceVat($row['price_vat']);
        $record->vat($row['vat']);
        $record->setPriceEur($row['price_eur']);
        $record->setPriceEurVat($row['price_eur_vat']);
        $record->bestseller($row['bestseller']);
        $record->featured($row['featured']);
        $record->popisModulu($row['popis_modulu']);
        $record->setPopisModuluEn($row['popis_modulu_en']);
        $record->zapojeni($row['zapojeni']);
        $record->setZapojeniEn($row['zapojeni_en']);
        $record->dokumentace($row['dokumentace']);
        $record->setDokumentaceEn($row['dokumentace_en']);
        $record->firmware($row['firmware']);
        $record->setFirmwareEn($row['firmware_en']);
        $record->programy($row['programy']);
        $record->setProgramyEn($row['programy_en']);
        $record->setStockInfo($row['stock_info']);
        $record->setStockInfoEn($row['stock_info_en']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Eshop\Products\Product $record
     * @return bool false on error
     */
    public function save(Product $record)
    {
        // seo have to be unique
        $uniq = false;
        $count = 1;
        $uniqSeo = '';
        do {
            $sql = 'SELECT COUNT(*) FROM '.self::$_tableName
            .' WHERE seo='.$this->_con->escape($record->seo().$uniqSeo).' AND id<>'.$record->instanceId();
            if ($this->_con->count($sql) === 0) {
                $uniq = true ;
            } else {
                $uniqSeo = '-'.($count++);
            }
        } while (!$uniq);
        $record->seo($record->seo().$uniqSeo);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $items = array();
        $this->_con->beginTrans();
        if ($record->instanceId() === 0) {
            $items['name'] = $this->_con->escape($record->name());
            $items['`name`'] = $this->_con->escape($record->getNameEn());
            $items['last_change'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
            $items['publish'] = $this->_con->boolToSql($record->published());
            $items['category_id'] = $record->categoryId();
            $items['seo'] = $this->_con->escape($record->seo());
            $items['text'] = $this->_con->escape($record->text());
            $items['`text_en`'] = $this->_con->escape($record->getTextEn());
            $items['meta_desc'] = $this->_con->escape($record->metaDescription());
            $items['meta_keys'] = $this->_con->escape($record->metaKeywords());
            $items['price'] = $record->price();
            $items['price_vat'] = $record->calcPriceVat();
            $items['vat'] = $record->vat();
            $items['price_eur'] = $record->getPriceEur();
            $items['price_eur_vat'] = $record->calcPriceEurVat();
            $items['bestseller'] = $this->_con->boolToSql($record->bestseller());
            $items['featured'] = $this->_con->boolToSql($record->featured());
            $items['popis_modulu'] = $this->_con->escape($record->popisModulu());
            $items['`popis_modulu_en`'] = $this->_con->escape($record->getPopisModuluEn());
            $items['zapojeni'] = $this->_con->escape($record->zapojeni());
            $items['`zapojeni_en`'] = $this->_con->escape($record->getZapojeniEn());
            $items['dokumentace'] = $this->_con->escape($record->dokumentace());
            $items['`dokumentace_en`'] = $this->_con->escape($record->getDokumentaceEn());
            $items['firmware'] = $this->_con->escape($record->firmware());
            $items['`firmware_en`'] = $this->_con->escape($record->getFirmwareEn());
            $items['programy'] = $this->_con->escape($record->programy());
            $items['`programy_en`'] = $this->_con->escape($record->getProgramyEn());
            $items['`stock_info`'] = $this->_con->escape($record->getStockInfo());
            $items['`stock_info_en`'] = $this->_con->escape($record->getStockInfoEn());
            $sql = 'INSERT INTO '.self::$_tableName.' ('
                .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            try {
                $productId = $this->_con->insert($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                $this->_con->rollback();
                return FALSE;
            }
        } else {
            $items[] = 'name='.$this->_con->escape($record->name());
            $items[] = '`name_en`='.$this->_con->escape($record->getNameEn());
            $items[] = 'last_change='.$this->_con->escape($date->format('Y-m-d H:i:s'));
            $items[] = 'publish='.$this->_con->boolToSql($record->published());
            $items[] = 'category_id='.$record->categoryId();
            $items[] = 'seo='.$this->_con->escape($record->seo());
            $items[] = 'text='.$this->_con->escape($record->text());
            $items[] = '`text_en`='.$this->_con->escape($record->getTextEn());
            $items[] = 'meta_desc='.$this->_con->escape($record->metaDescription());
            $items[] = 'meta_keys='.$this->_con->escape($record->metaKeywords());
            $items[] = 'price='.$record->price();
            $items[] = 'price_vat='.$record->calcPriceVat();
            $items[] = 'vat='.$record->vat();
            $items[] = 'price_eur='.$record->getPriceEur();
            $items[] = 'price_eur_vat='.$record->calcPriceEurVat();
            $items[] = 'bestseller='.$this->_con->boolToSql($record->bestseller());
            $items[] = 'featured='.$this->_con->boolToSql($record->featured());
            $items[] = 'popis_modulu='.$this->_con->escape($record->popisModulu());
            $items[] = '`popis_modulu_en`='.$this->_con->escape($record->getPopisModuluEn());
            $items[] = 'zapojeni='.$this->_con->escape($record->zapojeni());
            $items[] = '`zapojeni_en`='.$this->_con->escape($record->getZapojeniEn());
            $items[] = 'dokumentace='.$this->_con->escape($record->dokumentace());
            $items[] = '`dokumentace_en`='.$this->_con->escape($record->getDokumentaceEn());
            $items[] = 'firmware='.$this->_con->escape($record->firmware());
            $items[] = '`firmware_en`='.$this->_con->escape($record->getFirmwareEn());
            $items[] = 'programy='.$this->_con->escape($record->programy());
            $items[] = '`programy_en`='.$this->_con->escape($record->getProgramyEn());
            $items[] = '`stock_info`='.$this->_con->escape($record->getStockInfo());
            $items[] = '`stock_info_en`='.$this->_con->escape($record->getStockInfoEn());
            $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
            try {
                $this->_con->query($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                $this->_con->rollback();
                return FALSE;
            }
            $productId = $record->instanceId();
        }
        // icons
        $sql = 'DELETE FROM '.self::$_tableNameRelProductIcon.' WHERE product_id='.$productId;
        if (!$this->_con->query($sql)) {
            $this->_con->rollback();
            return false;
        }
        foreach ($record->getIconsIds() as $iconId) {
            $sql = 'INSERT INTO '.self::$_tableNameRelProductIcon
                .' (product_id,icon_id) VALUES ('.$productId.','.$iconId.')';
            if (!$this->_con->query($sql)) {
                $this->_con->rollback();
                return false;
            }
        }
        $this->_con->commit();
        $record->instanceId($productId);
        return TRUE;
    }

    /**
     * delete given instance from database
     * @param \Eshop\Products\Product $record
     * @return bool false on error
     */
    public function delete(Product $record)
    {
        $sql = 'DELETE FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        return $this->_con->query($sql);
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT DISTINCT id,name,`name_en`,seo,publish,category_id,text,meta_desc,meta_keys,
            price,price_vat,vat,`price_eur`,`price_eur_vat`,bestseller,featured
        FROM '.self::$_tableName.' AS p
        LEFT JOIN '.self::$_tableNameRelProductIcon.' AS ri ON p.id=ri.product_id'.
        $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Product($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->setNameEn($row['name_en']);
            $tmp->seo($row['seo']);
            $tmp->published($row['publish']);
            $tmp->categoryId($row['category_id']);
            $tmp->text($row['text']);
            $tmp->metaDescription($row['meta_desc']);
            $tmp->metaKeywords($row['meta_keys']);
            $tmp->price($row['price']);
            $tmp->priceVat($row['price_vat']);
            $tmp->vat($row['vat']);
            $tmp->setPriceEur($row['price_eur']);
            $tmp->setPriceEurVat($row['price_eur_vat']);
            $tmp->bestseller($row['bestseller']);
            $tmp->featured($row['featured']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array())
    {
        $sql = 'SELECT COUNT(DISTINCT p.id) FROM '.self::$_tableName.' AS p
        LEFT JOIN '.self::$_tableNameRelProductIcon.' AS ri ON p.id=ri.product_id'.$this->_con->where($criteria);
        return $this->_con->count($sql);
    }

    /**
     * returns instance by seo
     * @param string $string
     * @return \Eshop\Products\Product null if seo not found
     */
    public function getRecordBySeo($string)
    {
        $sql = 'SELECT id FROM '.self::$_tableName.' WHERE seo='.$this->_con->escape($string);
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        return (isset($row['id']) ? new Product($this, $row['id']) : null);
    }

    /**
     * change flag "published"
     * @param int $instanceId
     * @param bool current value of flag
     * @return bool false on error
     */
    public function changePublish($instanceId, &$publish)
    {
        $ret = true;
        $sql = 'SELECT publish FROM '.self::$_tableName.' WHERE id='.$instanceId;
        $row = $this->_con->fetchArray($sql);
        if (isset($row['publish'])) {
            $sql = 'UPDATE '.self::$_tableName.' SET publish='.$this->_con->boolToSql(!$row['publish'])
                .' WHERE id='.$instanceId;
            $ret = $this->_con->query($sql);
            if ($ret) {
                $publish = !$row['publish'];
            }
        }
        return $ret;
    }

    /**
     * return image assigned to article
     * @param int $instanceId
     * @return \Eshop\Products\Image
     */
    public function getImage($instanceId)
    {
        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataImage');
        return new Image($dataLayer, $instanceId);
    }

    /**
     * returns gallery images assigned to product
     * @param \Eshop\Products\Product $product
     * @return array
     */
    public function getGalleryImages(Product $product)
    {
        return $this->_app->dice()->create('\Eshop\Products\DataGalleryImage', array($this->_app))
            ->getImagesByProductId($product->instanceId());
    }

    /**
     * returns icosns assigned to product
     * @param \Eshop\Products\Product $product
     * @return array
     */
    public function getIcons(Product $product)
    {
        return $this->_app->dice()->create('\Eshop\Products\DataIcon', array($this->_app))
            ->getImagesByProductId($product->instanceId());
    }

    /**
     * returns ids of icons assigned to given product
     * @param \Eshop\Products\Product $record
     * @return array
     */
    public function getIconIdsByProduct(Product $record)
    {
        $ret = $this->_con->fetchColAll(
            'icon_id',
            'SELECT icon_id FROM '.self::$_tableNameRelProductIcon.' WHERE product_id='.$record->instanceId()
        );
        return ($ret === false ? array(): $ret);
    }

    /**
     * returns array of suggestions with cross-sell products
     * Retturn array if item with indices 'value' and 'label'
     * @param string $text
     * @param int $excludeId id of product
     * @param int $rows max rows
     * @return array
     */
    public function getSuggestCrossSell($text, $excludeId, $rows = 10)
    {
        $text = trim($text);
        if ($text === '') {
            return array();
        }
        $sql = 'SELECT id AS value,name AS label FROM '.self::$_tableName
            .' WHERE name LIKE '.$this->_con->escape('%'.$text.'%').' AND id<>'.$excludeId
            .' AND id NOT IN (SELECT cross_id FROM '.self::$_tableNameRelProductCross
                .' WHERE product_id='.$excludeId.') ORDER BY name '.$this->_con->limit(0, $rows);
        return $this->_con->fetchArrayAll($sql);
    }

    /**
     * returns cross-sell products assigned to given instance
     * @param \Eshop\Products\Product $product
     * @return array
     */
    public function getCrossSellProducts($productId)
    {
        $sql = 'SELECT id,name,seo,publish,text,price,price_vat,vat,bestseller
        FROM '.self::$_tableName
        .' WHERE id IN (SELECT cross_id FROM '.self::$_tableNameRelProductCross
            .' WHERE product_id='.$productId.')'.$this->_con->orderBy('name', false);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Product($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->seo($row['seo']);
            $tmp->published($row['publish']);
            $tmp->text($row['text']);
            $tmp->price($row['price']);
            $tmp->priceVat($row['price_vat']);
            $tmp->vat($row['vat']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * add relation between product and cross-sell product
     * @param int $productId
     * @param int $crossId
     * @return bool false on error
     */
    public function addCrossSell($productId, $crossId)
    {
        if ($productId === $crossId || $productId === 0 || $crossId === 0) {
            return true;
        }
        $sql = 'INSERT IGNORE INTO '.self::$_tableNameRelProductCross
            .' (product_id,cross_id) VALUES('.$productId.','.$crossId.')';
        return $this->_con->query($sql);
    }

    /**
     * remove relation between product and assigned cross-sell product
     * @param int $productId
     * @param int $crossId
     * @return bool false on error
     */
    public function removeCrossSell($productId, $crossId)
    {
        $sql = 'DELETE FROM '.self::$_tableNameRelProductCross
            .' WHERE product_id='.$productId.' AND cross_id='.$crossId;
        return $this->_con->query($sql);
    }

    /**
     * returns variant products assigned to given instance
     * @param \Eshop\Products\Product $product
     * @return array
     */
    public function getVariantProducts($productId)
    {
        $sql = 'SELECT id,name,seo,publish,text,price,price_vat,vat
        FROM '.self::$_tableName
        .' WHERE id IN (SELECT variant_id FROM '.self::$_tableNameRelProductVariant
            .' WHERE product_id='.$productId.')'.$this->_con->orderBy('name', false);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Product($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->seo($row['seo']);
            $tmp->published($row['publish']);
            $tmp->text($row['text']);
            $tmp->price($row['price']);
            $tmp->priceVat($row['price_vat']);
            $tmp->vat($row['vat']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * add relation between product and variant product
     * @param int $productId
     * @param int $variantId
     * @return bool false on error
     */
    public function addVariant($productId, $variantId)
    {
        if ($productId === $variantId || $productId === 0 || $variantId === 0) {
            return true;
        }
        $sql = 'INSERT IGNORE INTO '.self::$_tableNameRelProductVariant
            .' (product_id, variant_id) VALUES('.$productId.','.$variantId.')';
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            if ($exception->getCode() === 1062) {
                return TRUE; // ignore insert duplicite records
            }
            \Ergo\Application::logException($exception);
            $this->_con->rollback();
            return FALSE;
        }
        return TRUE;
    }

    /**
     * remove relation between product and assigned variant product
     * @param int $productId
     * @param int $variantId
     * @return bool false on error
     */
    public function removeVariant($productId, $variantId)
    {
        $sql = 'DELETE FROM '.self::$_tableNameRelProductVariant
            .' WHERE product_id='.$productId.' AND variant_id='.$variantId;
        return $this->_con->query($sql);
    }

    /**
     * save given instance to database as new record
     * @param \Eshop\Products\Product $record
     * @return int id of new record, false on error
     */
    public function saveCopy(Product $record)
    {
        // seo have to be unique
        $uniq = FALSE;
        $count = 1;
        $uniqSeo = '';
        do {
            $sql = 'SELECT COUNT(*) FROM '.self::$_tableName
            .' WHERE seo='.$this->_con->escape($record->seo().$uniqSeo);
            if ($this->_con->count($sql) === 0) {
                $uniq = TRUE;
            } else {
                $uniqSeo = '-'.($count++);
            }
        } while (!$uniq);
        $record->seo($record->seo().$uniqSeo);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $items = array();
        $this->_con->beginTrans();

        $items['name'] = $this->_con->escape($record->name().' KOPIE');
        $items['last_change'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
        $items['publish'] = $this->_con->boolToSql(FALSE);
        $items['category_id'] = $record->categoryId();
        $items['seo'] = $this->_con->escape($record->seo());
        $items['text'] = $this->_con->escape($record->text());
        $items['meta_desc'] = $this->_con->escape($record->metaDescription());
        $items['meta_keys'] = $this->_con->escape($record->metaKeywords());
        $items['price'] = $record->price();
        $items['price_vat'] = $record->calcPriceVat();
        $items['vat'] = $record->vat();
        $items['bestseller'] = $this->_con->boolToSql($record->bestseller());
        $items['featured'] = $this->_con->boolToSql($record->featured());
        $items['popis_modulu'] = $this->_con->escape($record->popisModulu());
        $items['zapojeni'] = $this->_con->escape($record->zapojeni());
        $items['dokumentace'] = $this->_con->escape($record->dokumentace());
        $items['firmware'] = $this->_con->escape($record->firmware());
        $items['programy'] = $this->_con->escape($record->programy());
        $sql = 'INSERT INTO '.self::$_tableName.' ('
            .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        try {
            $productId = $this->_con->insert($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_SAVE_FAILED%]');
            \Ergo\Application::logException($exception);
            $this->_con->rollback();
            return FALSE;
        }

        // icons
        $sql = 'INSERT INTO '.self::$_tableNameRelProductIcon.' (product_id,icon_id) '
            .'SELECT '.$productId.',icon_id FROM '.self::$_tableNameRelProductIcon
            .' WHERE product_id='.$record->instanceId();
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_SAVE_FAILED%]');
            \Ergo\Application::logException($exception);
            $this->_con->rollback();
            return FALSE;
        }
        // cross-sell products
        $sql = 'INSERT INTO '.self::$_tableNameRelProductCross.' (product_id, cross_id) '
            .'SELECT '.$productId.',cross_id FROM '.self::$_tableNameRelProductCross
            .' WHERE product_id='.$record->instanceId();
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_SAVE_FAILED%]');
            \Ergo\Application::logException($exception);
            $this->_con->rollback();
            return FALSE;
        }
        // variant products
        $sql = 'INSERT INTO '.self::$_tableNameRelProductVariant.' (product_id, variant_id) '
            .'SELECT '.$productId.',variant_id FROM '.self::$_tableNameRelProductVariant
            .' WHERE product_id='.$record->instanceId();
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_SAVE_FAILED%]');
            \Ergo\Application::logException($exception);
            $this->_con->rollback();
            return FALSE;
        }

        // main image
        $image = $this->_app->dice()->create('\Eshop\Products\Image', array($record->instanceId()));
        if (!$image->saveCopy($productId)) {
            $record->err($image->err());
            $this->_con->rollback();
            return FALSE;
        }

        // gallery
        foreach ($record->getImages() as $image) {
            if (!$image->saveCopy($productId)) {
                $record->err($image->err());
                $this->_con->rollback();
                return FALSE;
            }
        }

        $this->_con->commit();
        $record->instanceId($productId);
        return $productId;
    }

}