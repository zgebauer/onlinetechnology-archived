<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Html;
use GstLib\Tree;

/**
 * tree of categories
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Categories
{
    /** @var \GstLib\Tree tree of categories */
    private $_tree;
    /** @var \Eshop\Products\DataCategory data layer */
    protected $_dataLayer;
    /** @var bool flag "load only published categories" */
    private $_publishedOnly = false;

    /**
     * constructor
     * @param \Eshop\Categories\Data $dataLayer
     * @param bool $publishedOnly true=load only published categories
     */
    public function __construct(\Eshop\Products\DataCategory $dataLayer, $publishedOnly = false)
    {
        $this->_tree = null;
        $this->_dataLayer = $dataLayer;
        $this->_publishedOnly = (bool) $publishedOnly;
    }

    /**
     * returns array of categories in format [id]=>name with indent
     * @param int $categoryId id of current item
     * @param int $level level of tree
     * @return array
     */
    public function optionsCategory($categoryId = 0, $level = 0)
    {
        $ret = array();
        foreach (self::getCategories($categoryId) as $item) {
            if ($item->parentId() == $categoryId) {
                $name = str_repeat('&nbsp;&nbsp;', $level).Html::escape($item->name());
                $ret[$item->instanceId()] = $name;
                foreach ($this->optionsCategory($item->instanceId(), $level+1) as $key=>$value) {
                    $ret[$key] = $value;
                }
            }
        }
        return $ret;
    }

    /**
     * returns array of categories as objects with properties id, name, parent_id,level
     * @param int $categoryId id of current item
     * @param int $level level of tree
     * @return array
     */
    public function listCategories($categoryId = 0, $level = 0)
    {
        $ret = array();
        foreach (self::getCategories($categoryId) as $item) {
            if ($item->parentId() === $categoryId) {
                $ret[] = array (
                    'id'=>$item->instanceId(),
                    'name'=>$item->name(),
                    'parent_id'=>$item->parentId(),
                    'level'=>$level
                );
                foreach ($this->listCategories($item->instanceId(), $level+1) as $value) {
                    $ret[] = $value;
                }
            }
        }
        return $ret;
    }

    /**
     * returns children categories
     * @param int $parentId
     * @return array
     */
    public function getCategories($parentId = 0)
    {
        if (is_null($this->_tree)) {
            $criteria = array();
            if ($this->_publishedOnly) {
                $criteria = array(array('publish', '1'));
            }
            $categories = $this->_dataLayer->getRecords(0, null, 'ordering', false, $criteria);
            $this->_tree = new Tree($categories);
        }
        return $this->_tree->getChildren($parentId);
    }

    /**
     * returns ids of children items
     * @param int $parentId
     * @return array
     */
    public function getChildrenIds($parentId = 0)
    {
        $ret = array();
        foreach ($this->getCategories($parentId) as $category) {
            $ret[] = $category->instanceId();
        }
        return $ret;
    }

    /**
     * returns parent categories of category item given id
     * @param int $categoryId identificator of item
     * @return array
     */
    public function getParentCategories($categoryId)
    {
        if (is_null($this->_tree)) {
            $criteria = array();
            if ($this->_publishedOnly) {
                $criteria = array(array('publish', '1'));
            }
            $categories = $this->_dataLayer->getRecords(0, null, 'ordering', false, $criteria);
            $this->_tree = new Tree($categories);
        }
        return $this->_tree->getParents($categoryId);
    }
}