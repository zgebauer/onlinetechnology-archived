<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

/**
 * data tier for module - categories
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataCategory
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_categories%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * fill given instance from database
     * @param \Eshop\Products\Category $record
     * @return bool false on error
     */
    public function load(Category $record)
    {
        $sql = 'SELECT id,name,`name_en`,seo,parent_id,ordering,publish,meta_desc,meta_keys,text,`text_en`
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->setNameEn($row['name_en']);
        $record->seo($row['seo']);
        $record->published($row['publish']);
        $record->parentId($row['parent_id']);
        $record->ordering($row['ordering']);
        $record->metaDescription($row['meta_desc']);
        $record->metaKeywords($row['meta_keys']);
        $record->text($row['text']);
        $record->setTextEn($row['text_en']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Eshop\Categories\Category $record
     * @return bool false on error
     */
    public function save(Category $record)
    {
        // seo have to be unique
        $uniq = false;
        $count = 1;
        $uniqSeo = '';
        do {
            $sql = 'SELECT COUNT(*) FROM '.self::$_tableName
            .' WHERE seo='.$this->_con->escape($record->seo().$uniqSeo).' AND id<>'.$record->instanceId();
            if ($this->_con->count($sql) === 0) {
                $uniq = true ;
            } else {
                $uniqSeo = '-'.($count++);
            }
        } while (!$uniq);
        $record->seo($record->seo().$uniqSeo);

        $items = array();
        if ($record->instanceId() === 0) {
            $row = $this->_con->fetchArray(
                'SELECT IFNULL(MAX(ordering),0)+1 AS new_order
                FROM '.self::$_tableName.' WHERE parent_id='.$record->parentId()
            );
            $record->ordering($row['new_order']);
            $items['name'] = $this->_con->escape($record->name());
            $items['`name_en`'] = $this->_con->escape($record->getNameEn());
            $items['publish'] = $this->_con->boolToSql($record->published());
            $items['seo'] = $this->_con->escape($record->seo());
            $items['parent_id'] = $record->parentId();
            $items['ordering'] = $record->ordering();
            $items['meta_desc'] = $this->_con->escape($record->metaDescription());
            $items['meta_keys'] = $this->_con->escape($record->metaKeywords());
            $items['text'] = $this->_con->escape($record->text());
            $items['`text_en`'] = $this->_con->escape($record->getTextEn());
            $sql = 'INSERT INTO '.self::$_tableName.' ('
                .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            try {
                $record->instanceId($this->_con->insert($sql));
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        } else {
            $items[] = 'name='.$this->_con->escape($record->name());
            $items[] = '`name_en`='.$this->_con->escape($record->getNameEn());
            $items[] = 'publish='.$this->_con->boolToSql($record->published());
            $items[] = 'seo='.$this->_con->escape($record->seo());
            $items[] = 'parent_id='.$record->parentId();
            $items[] = 'meta_desc='.$this->_con->escape($record->metaDescription());
            $items[] = 'meta_keys='.$this->_con->escape($record->metaKeywords());
            $items[] = 'text='.$this->_con->escape($record->text());
            $items[] = '`text_en`='.$this->_con->escape($record->getTextEn());
            $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
            try {
                $this->_con->query($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        }
        $this->_fixOrder($record->parentId());
        return TRUE;
    }

    /**
     * delete given instance from database
     * @param \Eshop\Products\Category $record
     * @return bool false on error
     */
    public function delete(Category $record)
    {
        try {
            $sql = 'SELECT parent_id FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
            $row = $this->_con->fetchArray($sql);

            $sql = array(
                'DELETE FROM '.self::$_tableName.' WHERE id='.$record->instanceId(),
                'UPDATE '.self::$_tableName.' SET parent_id='.$row['parent_id']
                .' WHERE parent_id='.$record->instanceId()
            );
            $this->_con->transaction($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_DELETE_FAILED%]');
            \Ergo\Application::logException($exception);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,name,`name_en`,seo,parent_id,ordering,publish,meta_desc,meta_keys
            FROM '.self::$_tableName.
            $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Category($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->setNameEn($row['name_en']);
            $tmp->seo($row['seo']);
            $tmp->published($row['publish']);
            $tmp->parentId($row['parent_id']);
            $tmp->ordering($row['ordering']);
            $tmp->metaDescription($row['meta_desc']);
            $tmp->metaKeywords($row['meta_keys']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * move item
     * @param \Eshop\Products\Category $record
     * @param string $direction [up|down]
     * @return bool false on error
     */
    public function moveItem(Category $record, $direction = 'down')
    {
        switch($direction) {
        case 'up' :
            $sql = 'SELECT id,ordering FROM '.self::$_tableName
            .' WHERE parent_id='.$record->parentId().' AND ordering<'.$record->ordering().
            ' ORDER BY ordering DESC LIMIT 0,1';
            break;
        case 'down' :
        default :
            $sql = 'SELECT id,ordering FROM '.self::$_tableName
            .' WHERE parent_id='.$record->parentId().' AND ordering>'.$record->ordering().
            ' ORDER BY ordering ASC LIMIT 0,1';
        }
        $row = $this->_con->fetchArray($sql);
        if (!$row) {
            return true;
        }

        $switchId = $row['id'];
        $switchPos = $row['ordering'];
        $this->_con->free();

        $sql = array(
            'UPDATE '.self::$_tableName.' SET ordering='.$record->ordering().' WHERE id='.$switchId,
            'UPDATE '.self::$_tableName.' SET ordering='.$switchPos.' WHERE id='.$record->instanceId()
        );
        return $this->_con->transaction($sql);
    }

    /**
     * fix duplicate positions of items
     * @param int $instanceId
     */
    private function _fixOrder($parentId)
    {
        do {
            $orderOk = true;
            $sql = 'SELECT id,ordering,COUNT(ordering) AS dup
            FROM '.self::$_tableName.' WHERE parent_id='.$parentId.' GROUP BY ordering HAVING dup>1 ORDER BY id';
            foreach ($this->_con->fetchArrayAll($sql) as $row) {
                $rowFix = $this->_con->fetchArray(
                    'SELECT IFNULL(MAX(ordering),0)+1 AS pos FROM '.self::$_tableName.' WHERE parent_id='.$parentId
                );
                $this->_con->query(
                    'UPDATE '.self::$_tableName.' SET ordering='.$rowFix['pos'].' WHERE id='.$row['id']
                );
                $orderOk = false;
            }
        } while (!$orderOk);
    }

    /**
     * returns instance by seo
     * @param string $string
     * @return \Eshop\Categories\Category null if seo not found
     */
    public function getRecordBySeo($string)
    {
        $sql = 'SELECT id FROM '.self::$_tableName.' WHERE seo='.$this->_con->escape($string);
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        return (isset($row['id']) ? new Category($this, $row['id']) : null);
    }

}