<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Vars;
use GstLib\Html;

/**
 * category of sortiment, item in tree of categories
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Category
{
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var bool flag "loaded from data tier" */
    protected $_loaded;
    /** @var string */
    protected $_name;
    /** @var string */
    protected $nameEn;
    /** @var string identifier for url */
    protected $_seo;
    /** @var bool flag publish record */
    protected $_publish;
    /** @var string */
    protected $_text;
    /** @var string */
    protected $textEn;
    /** @var string content for meta tag description */
    protected $_metaDescription;
    /** @var string content for meta tag keywords */
    protected $_metaKeywords;
    /** @var int id of parent item */
    protected $_parentId;
    /** @var int ordering in parent record */
    protected $_ordering;
    /** @var \Eshop\Products\DataCategory data layer */
    protected $_dataLayer;
    /** @var string alt of menu  */

    /**
     * create instance and fill it with data by given identifier
     * @param \Eshop\Products\DataCategory $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(\Eshop\Products\DataCategory $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_name = $this->_seo = '';
        $this->_publish = false;
        $this->_text = '';
        $this->_metaDescription = $this->_metaKeywords = '';
        $this->_parentId = $this->_ordering = 0;

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded()
    {
        return $this->_loaded;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null)
    {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 50);
        }
        return $this->_name;
    }

    /**
     * @return string
     */
    public function getNameEn() {
        return $this->nameEn;
    }

    /**
     *
     * @param string $nameEn
     * @return Category
     */
    public function setNameEn($nameEn) {
        $this->nameEn = Vars::substr($nameEn, 0, 50);
        return $this;
    }

    /**
     * returns and sets seo
     * @param string $value
     * @return string
     */
    public function seo($value = null)
    {
        if (!is_null($value)) {
            $this->_seo = Vars::substr(Html::fixForUrl($value), 0, 52);
        }
        return $this->_seo;
    }

    /**
     * returns and sets description text
     * @param string $value
     * @return string text
     */
    public function text($value = null)
    {
        if (!is_null($value)) {
            $this->_text = $value;
        }
        return $this->_text;
    }

    /**
     * @return string
     */
    public function getTextEn() {
        return $this->textEn;
    }

    /**
     * @param string $textEn
     * @return Category
     */
    public function setTextEn($textEn) {
        $this->textEn = $textEn;
        return $this;
    }

    /**
     * returns and sets flag "publish record"
     * @param bool $value
     * @return bool
     */
    public function published($value = null)
    {
        if (!is_null($value)) {
            $this->_publish = (bool) $value;
        }
        return $this->_publish;
    }

    /**
     * returns and sets meta description
     * @param string $value
     * @return string
     */
    public function metaDescription($value = null)
    {
        if (!is_null($value)) {
            $this->_metaDescription = Vars::substr($value, 0, 255);
        }
        return $this->_metaDescription;
    }

    /**
     * returns and sets meta keywords
     * @param string $value
     * @return string
     */
    public function metaKeywords($value = null)
    {
        if (!is_null($value)) {
            $this->_metaKeywords = Vars::substr($value, 0, 255);
        }
        return $this->_metaKeywords;
    }

    /**
     * returns and sets value of parent id
     * @param int $value
     * @return int
     */
    public function parentId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_parentId = $value;
            }
        }
        return $this->_parentId;
    }

    /**
     * returns and sets value of ordering
     * @param int $value
     * @return int
     */
    public function ordering($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_ordering = $value;
            }
        }
        return $this->_ordering;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load()
    {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save()
    {
        $this->_err = '';
        $required = array();
        if ($this->_name === '') {
            $required[] = '[%LG_NAME%] (CS)';
        }
        if ($this->getNameEn() === '') {
            $required[] = '[%LG_NAME%] (EN)';
        }
        if (isset($required[0])) {
            $this->_err = '[%LG_ERR_MISSING_VALUES%] '.join(', ', $required);
            return false;
        }
        if ($this->_seo === '') {
            $this->seo($this->_name);
        }
        if ($this->_metaDescription === '') {
            $this->metaDescription($this->_name);
        }
        if ($this->_metaKeywords === '') {
            $this->metaKeywords(join(', ', Vars::parseKeywords($this->_name)));
        }

        return $this->_dataLayer->save($this);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        return $this->_dataLayer->delete($this);
    }

    /**
     * move item up
     * @return bool false on error
     */
    public function moveUp()
    {
        return $this->_dataLayer->moveItem($this, 'up');
    }

    /**
     * move item down
     * @return bool false on error
     */
    public function moveDown()
    {
        return $this->_dataLayer->moveItem($this, 'down');
    }

    /**
     * @param string $lang
     * @return string
     */
    public function getNameByLang($lang) {
        return ($lang === 'en' ? $this->getNameEn() : $this->name());
    }

}