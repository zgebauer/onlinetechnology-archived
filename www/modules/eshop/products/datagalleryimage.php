<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Products;

use GstLib\Upload;
use GstLib\UploadException;
use GstLib\UserFile;
use GstLib\Resizer;
use GstLib\ImageSharpen;
use GstLib\Filesystem;

/**
 * data tier for module - images gallery
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataGalleryImage
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_products_images%]';
    /** @var \Eshop\Config configuration */
    private $_config;
    /** @var int number of files per directory */
    private static $_filesPerDir = UserFile::FILES_PER_DIR_1000;

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Eshop\Config $config module configuration
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Eshop\Config $config,
            \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_config = $config;
        $this->_app = $application;
    }

    /**
     * fill given instance from database
     * @param \Eshop\Products\GalleryImage $record
     * @return bool false on error
     */
    public function load(GalleryImage $record)
    {
        $sql = 'SELECT id,name,publish,parent_id,ordering,description
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->published($row['publish']);
        $record->parentId($row['parent_id']);
        $record->ordering($row['ordering']);
        $record->description($row['description']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Eshop\Products\GalleryImage $record
     * @param string $field with uploaded file
     * @return bool false on error
     */
    public function save(GalleryImage $record, $field)
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $items = array();
        if ($record->instanceId() === 0) {
            $row = $this->_con->fetchArray(
                'SELECT IFNULL(MAX(ordering),0)+1 AS new_order
                FROM '.self::$_tableName.' WHERE parent_id='.$record->parentId()
            );
            $record->ordering($row['new_order']);
            $items['name'] = $this->_con->escape($record->name());
            $items['last_change'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
            $items['publish'] = $this->_con->boolToSql($record->published());
            $items['parent_id'] = $record->parentId();
            $items['ordering'] = $record->ordering();
            $sql = 'INSERT INTO '.self::$_tableName.' ('.
                implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            $newId = $this->_con->insert($sql);
            if (!$newId) {
                return false;
            }
            $record->instanceId($newId);
        } else {
            $items[] = 'name='.$this->_con->escape($record->name());
            $items[] = 'last_change='.$this->_con->escape($date->format('Y-m-d H:i:s'));
            $items[] = 'publish='.$this->_con->boolToSql($record->published());
            $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
            if (!$this->_con->query($sql)) {
                return false;
            };
        }
        self::_fixOrder($record->parentId());

        if (!Upload::isUploaded($field)) {
            return true;
        }
        $uploader = new Upload();
        $uploader->allowExtensions($this->_config->allowedImageExtensions());
        $uploader->allowMimeTypes($this->_config->allowedImageMimeTypes());
        $uploader->maxUploadFileSize($this->_config->maxUploadImageFileSize());

        $baseDir = $this->_config->dirGalleryImages(GalleryImage::IMG_ORIG);
        $targetDir = $baseDir.UserFile::idToDir($record->instanceId(), self::$_filesPerDir);
        if (!is_dir($targetDir)) {
            Filesystem::mkDir($targetDir);
        }
        if (!is_dir($targetDir)) {
            user_error('directory '.$targetDir.' not found ', E_USER_WARNING);
            return false;
        }
        $fileName = UserFile::createFileName($record->instanceId(), $uploader->getFileName($field));
        try {
            $uploader->saveAs($fileName, $targetDir, $field, true);
        } catch (UploadException $e) {
            $record->err('[%LG_ERR_UPLOAD_'.$e->getCode().'%]');
            return false;
        }

        UserFile::delete($record->instanceId(), $baseDir, $fileName, self::$_filesPerDir);
        // delete thumbnail and create new one
        $this->deleteAttachment($record, GalleryImage::IMG_THUMB);
        $this->getAttachmentFullPath($record, GalleryImage::IMG_THUMB);

        return true;
    }

    /**
     * delete given instance from database
     * @param \Eshop\Products\GalleryImage $record
     * @return bool false on error
     */
    public function delete(GalleryImage $record)
    {
        self::deleteAttachment($record, GalleryImage::IMG_ORIG);
        self::deleteAttachment($record, GalleryImage::IMG_THUMB);
        return $this->_con->query('DELETE FROM '.self::$_tableName.' WHERE id='.$record->instanceId());
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,name,parent_id,publish,ordering,description
            FROM '.self::$_tableName.
            $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new GalleryImage($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->published($row['publish']);
            $tmp->parentId($row['parent_id']);
            $tmp->ordering($row['ordering']);
            $tmp->description($row['description']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**

    /**
     * returns full path and name of file attached tu given instance or empty string if file nor found
     * @param \Eshop\Products\GalleryImage $record
     * @param enum $type type of attached file
     * @return string
     */
    public function getAttachmentFullPath(GalleryImage $record, $type)
    {
        $ret = UserFile::getFullPath(
            $record->instanceId(), $this->_config->dirGalleryImages($type), self::$_filesPerDir
        );

        // thumbnail
        if ($type !== GalleryImage::IMG_ORIG) {
            $source = UserFile::getFullPath(
                $record->instanceId(), $this->_config->dirGalleryImages(GalleryImage::IMG_ORIG), self::$_filesPerDir
            );
            if ($ret != '') {
                // check filemtime, get filemtime of local file from name in format id_timestamp_name.ext
                $tmp = explode('_', basename($ret));
                if ($tmp[1] != filemtime($source)) {
                    unlink($ret);
                    $ret = '';
                }
            }
            if ($ret == '') {
                if (!is_file($source)) {
                    return '';
                }
                // try make new image
                $dir = $this->_config->dirGalleryImages($type)
                    .UserFile::idToDir($record->instanceId(), self::$_filesPerDir);
                if (!is_dir($dir)) {
                    Filesystem::mkDir($dir);
                }
                if (!is_dir($dir)) {
                    return '';
                }
                $target = $dir.$record->instanceId().'_'.filemtime($source).'_'.UserFile::stripId(basename($source));
                list($width, $height) = $this->_config->sizeGalleryImage($type);
                Resizer::createCenterThumbnail($source, $target, $width, $height, 85, new ImageSharpen());
                $ret = UserFile::getFullPath(
                    $record->instanceId(), $this->_config->dirGalleryImages($type), self::$_filesPerDir
                );
            }
        }
        return $ret;
    }

    /**
     * returns url of file attached to given instance or empty string if file is not found
     * @param \Eshop\Products\GalleryImage $record
     * @param enum $type type of atached file
     * @return string
     */
    public function getAttachmentUrl(GalleryImage $record, $type)
    {
        $fullPath = $this->getAttachmentFullPath($record, $type);
        if ($fullPath === '') {
            return '';
        }
        return str_replace(ERGO_ROOT_DIR, ERGO_ROOT_URL, $fullPath)
            .($type === GalleryImage::IMG_ORIG ? '?'.filemtime($fullPath) : '');
    }

    /**
     * delete file attached to given instance
     * @param \Eshop\Products\GalleryImage $record
     * @param enum $type type of attached file
     * @return bool false on error
     */
    public function deleteAttachment(GalleryImage $record, $type)
    {
        $fullPath = self::getAttachmentFullPath($record, $type);
        if ($fullPath == '') {
            return true;
        }
        return unlink($fullPath);
    }

    /**
     * move image
     * @param \Eshop\Products\GalleryImage $record
     * @param string $direction [up|down]
     * @return bool false on error
     */
    public function move(GalleryImage $record, $direction)
    {
        switch($direction) {
        case 'up' :
            $sql = 'SELECT id,ordering FROM [%eshop_products_images%]
            WHERE product_id='.$record->getProductId().
            ' AND ordering<'.$record->getOrdering().
            ' ORDER BY ordering DESC LIMIT 0,1';
            break;
        case 'down' :
            $sql = 'SELECT id,ordering FROM [%eshop_products_images%]
            WHERE product_id='.$record->getProductId().
            ' AND ordering>'.$record->getOrdering().
            ' ORDER BY ordering ASC LIMIT 0,1';
            break;
        default :
            user_error('unknown direction '.$direction, E_USER_NOTICE);
            return false;
            break;
        }
        $row = Gst_Db::fetchArray($sql);
        if (!$row) {
            return true;
        }

        $switchId = $row['id'];
        $switchPos = $row['ordering'];
        Gst_Db::free();

        $sql = array('UPDATE [%eshop_products_images%] SET ordering='.
        $record->getOrdering().' WHERE id='.$switchId);
        $sql[] = 'UPDATE [%eshop_products_images%] SET ordering='.$switchPos.
        ' WHERE id='.$record->getId();
        return Gst_Db::transaction($sql);
    }

    /**
     * fix duplicate positions of items
     * @param int $instanceId
     */
    private function _fixOrder($productId)
    {
        do {
            $orderOk = true;
            $sql = 'SELECT id,ordering,COUNT(ordering) AS dup
            FROM '.self::$_tableName.' WHERE parent_id='.$productId.' GROUP BY ordering HAVING dup>1 ORDER BY id';
            foreach ($this->_con->fetchArrayAll($sql) as $row) {
                $rowFix = $this->_con->fetchArray(
                    'SELECT IFNULL(MAX(ordering),0)+1 AS pos FROM '.self::$_tableName.' WHERE parent_id='.$productId
                );
                $this->_con->query(
                    'UPDATE '.self::$_tableName.' SET ordering='.$rowFix['pos'].' WHERE id='.$row['id']
                );
                $orderOk = false;
            }
        } while (!$orderOk);
    }

    /**
     * returns images assigned to product parent product pf image
     * @param int $productId
     * @return array of instances \Eshop\Products\GalleryImage
     */
    public function getImagesByProductId($productId)
    {
        $criteria = array(array('parent_id', $productId));
        return $this->getRecords(0, null, 'ordering', FALSE, $criteria);
    }

    /**
     * update
     * @param \Eshop\Products\GalleryImage $record
     * @return bool false on error
     */
    public function update(\Eshop\Products\GalleryImage $record)
    {
        $items = array();
        $items[] = 'description='.$this->_con->escape($record->description());
        $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
        return $this->_con->query($sql);
    }

    /**
     * save given instance to database as new record
     * @param \Eshop\Products\GalleryImage $record
     * @param int $productId id of product which new image is related to
     * @return bool false on error
     */
    public function saveCopy(GalleryImage $record, $productId)
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $items = array();
        $row = $this->_con->fetchArray(
            'SELECT IFNULL(MAX(ordering),0)+1 AS new_order
            FROM '.self::$_tableName.' WHERE parent_id='.$productId
        );
        $record->ordering($row['new_order']);
        $items['name'] = $this->_con->escape($record->name());
        $items['last_change'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
        $items['publish'] = $this->_con->boolToSql($record->published());
        $items['parent_id'] = $productId;
        $items['ordering'] = $record->ordering();
        $items['description'] = $this->_con->escape($record->description());
        $sql = 'INSERT INTO '.self::$_tableName.' ('.
            implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $newId = $this->_con->insert($sql);
        if (!$newId) {
            return false;
        }
        $baseDir = $this->_config->dirGalleryImages(GalleryImage::IMG_ORIG);
        $targetDir = $baseDir.UserFile::idToDir($newId, self::$_filesPerDir);
        if (!is_dir($targetDir)) {
            Filesystem::mkDir($targetDir);
        }
        if (!is_dir($targetDir)) {
            user_error('directory '.$targetDir.' not found ', E_USER_WARNING);
            return FALSE;
        }
        $src = $this->getAttachmentFullPath($record, GalleryImage::IMG_ORIG);
        if ($src === '') {
            return TRUE;
        }
        $srcName = UserFile::stripId(basename($src));
        $target = $targetDir.UserFile::createFileName($newId, $srcName);
        if (!copy($src, $target)) {
            $record->err('cannot copy image');
            return FALSE;
        }
        return TRUE;
    }

}