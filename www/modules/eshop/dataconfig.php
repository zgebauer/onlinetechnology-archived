<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

/**
 * data tier for module - config
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataConfig
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_config%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }


    /**
     * fill given instance from given xml file
     * @param \Eshop\Config $record
     */
    public function load(Config $record)
    {
        $sql = 'SELECT param,value FROM '.self::$_tableName;
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $record->setParam($row['param'], $row['value']);
        }
    }

    /**
     * save given instance to database
     * @param \Eshop\Config $record
     * @return bool false on error
     */
    public function save(Config $record)
    {
        $paramsSave = array('email_order');
        foreach ($paramsSave as $param) {
            $sql = 'SELECT COUNT(*) FROM '.self::$_tableName.' WHERE param='.$this->_con->escape($param);
            if ($this->_con->count($sql) > 0) {
                $sql = 'UPDATE '.self::$_tableName.' SET value='.$this->_con->escape($record->getParam($param))
                    .' WHERE param='.$this->_con->escape($param);
            } else {
                $sql = 'INSERT INTO '.self::$_tableName.' (param,value)
                VALUES ('.$this->_con->escape($param).','.$this->_con->escape($record->getParam($param)).')';
            }
            try {
                $this->_con->query($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        }
        return true;
    }

}