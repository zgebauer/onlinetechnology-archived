<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

use GstLib\Vars;
use GstLib\Template;
use Ergo\Request;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for shopping cart
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerCarts
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render page with current shopping cart
     */
    public function renderCart()
    {
        $cart = $this->_app->getModule('eshop')->getCart();

        $quantity = $this->_app->request()->getValue('qty', array(Request::GET, Request::POST), 'int');
        $productId = $this->_app->request()->getValue('id', array(Request::GET, Request::POST), 'int');

        // change quantity
        if (isset($_POST['recalc']) && isset($_POST['quantity'])) {
            foreach ($_POST['quantity'] as $productId=>$quantity) {
                $cart->changeQuantity($productId, $quantity);
            }
        }

        // remove items from cart
        if (isset($_POST['remove'])) {
            $productId = (int) key($_POST['remove']);
            $cart->changeQuantity($productId, 0);
        }

        $lang = $this->_app->currentLanguage();
        $tpl = new Template($this->_app->getTemplatePath('frm_cart.htm', Module::NAME), true);

        if (!is_null($cart)) {
            $cnt = '';
            $tplRow = new Template();
            $rowTemplate = $tpl->getSub('ITEMS');
            foreach ($cart->getItems() as $item) {
                $tplRow->set($rowTemplate);
                $tplRow->replace('INPVAL_ITEM_ID', $item->instanceId());
                $tplRow->replace('TXT_ITEM', $item->name());
                $tplRow->replace('URL_ITEM', $this->_app->getUrl(Module::NAME, 'zbozi/'.$item->seo()));
                $tplRow->replace('INPVAL_ITEM_QUANTITY', $item->quantity());
                $tplRow->replace(
                    'TXT_ITEM_PRICE_UNIT_VAT', Vars::formatNumberHuman($item->priceUnit(true, $lang), 2, ',')
                );
                $tplRow->replace(
                    'TXT_ITEM_PRICE_ROW_VAT', Vars::formatNumberHuman($item->getPriceRow(true, $lang), 2, ',')
                );
                $cnt .= $tplRow->get();
            }
            $tpl->replaceSub('ITEMS', $cnt);
            $tpl->replace('TXT_TOTAL_PRICE', Vars::formatNumberHuman($cart->price(false, $lang), 2, ','));
            $tpl->replace('TXT_TOTAL_PRICE_VAT', Vars::formatNumberHuman($cart->price(true, $lang), 2, ','));
        }

        $tpl->showSub('CART', !is_null($cart) && !$cart->isEmpty());
        $tpl->showSub('CART_EMPTY', is_null($cart) || $cart->isEmpty());

        $this->_app->response()->pageTitle('Košík | [%LG_SITENAME%]');
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * add product to cart. Expects
     * $_GET[qty] - quantity
     * $_GET[id] - cart id
     * @return void
     */
    public function handleAdd()
    {
        $cart = $this->_app->getModule('eshop')->getCart(true);
        $err = '';
        $request = $this->_app->request();

        $quantity = $request->getValue('qty', Request::POST, 'int');
        $productId = $request->getValue('id', Request::POST, 'int');
        if ($quantity < 1 || $productId < 1) {
            $this->_app->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
            return;
        }
        if (!$cart->add($productId, $quantity)) {
            $err = ($cart->err() == '' ? 'something is wrong' : $cart->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->err = $err;
        $ret->cart = null;
        if (!is_null($cart)) {
            $ret->cart = new \stdClass();
            $ret->cart->count = count($cart->getItems());
            $ret->cart->priceVat = Vars::formatNumberHuman($cart->price(true, $this->_app->currentLanguage()), 2);
        }
        $this->_app->response()->setOutputJson($ret);
    }

}