<?php

namespace Eshop\Cnb;

class Repository extends \Ergo\Component {

	/** @var Mapper $mapper */
	private $mapper;

    /**
     * @param \Ergo\ApplicationInterface $application
     * @param Mapper $mapper
     */
	public function __construct(\Ergo\ApplicationInterface $application, Mapper $mapper) {
		parent::__construct($application);
		$this->mapper = $mapper;
	}

	public function getRateEur() {
		return $this->mapper->getRateEur();
	}

	public function saveRateEur($amount) {
		$this->mapper->saveRateEur($amount);
	}


}
