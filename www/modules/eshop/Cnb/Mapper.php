<?php

namespace Eshop\Cnb;

use \GstLib\Db\Criteria;

class Mapper extends \Ergo\DbMapperAbstract {

	/** @var string name of database table */
	private static $tableName = '`eshop_exchange_rates`';

    /**
     * @return float
     * @throws Exception
     */
	public function getRateEur() {
		$criteria = array(new Criteria('currency', 'EUR'));
		$sql = 'SELECT `rate` FROM ' . self::$tableName . $this->con->where($criteria) ;
		$row = $this->con->fetchArray($sql);
        if (isset($row['rate'])) {
            return (float) $row['rate'];
		}
        throw new Exception('EUR rate not found');
	}

    /**
     * @param float $rate
     */
	public function saveRateEur($rate) {
        $itemsInsert = array();
        $itemsInsert['currency'] = $this->con->escape('EUR');
        $itemsInsert['rate'] = (float) $rate;

        $itemsUpdate = array();
        $itemsUpdate[] = 'rate=' . (float) $rate;

        $sql = 'INSERT INTO '.self::$tableName.' ('.
            implode(',', array_keys($itemsInsert)).') VALUES ('.implode(',', $itemsInsert).')
            ON DUPLICATE KEY UPDATE '.join(',', $itemsUpdate);
        $this->con->query($sql);
	}

}