<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

use GstLib\Vars;
use GstLib\Html;
use GstLib\Template;
use Ergo\Request;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for shopping cart
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerOrders {

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application) {
        $this->_app = $application;
    }

    /**
     * render page with order form
     * @return void
     */
    public function renderFormOrder() {
        $cart = $this->_app->getModule('eshop')->getCart(true);
        if ($cart->isEmpty()) {
            $this->_app->response()->redirect($this->_app->getUrl(\Eshop\Module::NAME, 'kosik'));
            return;
        }

		$lang = $this->_app->currentLanguage();
		$currency = $this->_app->getCurrency();

        $customer = $this->_app->getVisitor();

        $order = new \Eshop\Orders\Order($this->_app->dice()->create('\Eshop\Orders\DataOrder'));
        $order->setLang($this->_app->currentLanguage());
        $order->fill($cart);
        if (!is_null($customer)) {
            $order->customerId($customer->instanceId());
            $order->email($customer->email());
            $order->firstName($customer->firstName());
            $order->lastName($customer->lastName());
            $order->company($customer->company());
            $order->phone($customer->phone());
            $order->ic($customer->ic());
            $order->dic($customer->dic());
            $order->address($customer->address());
            $order->city($customer->city());
            $order->postCode($customer->postCode());
            $order->country($customer->country());
        }

        $tpl = new Template($this->_app->getTemplatePath('frm_order.htm', Module::NAME), true);
        $tpl->replace('INP_CURRENCY', $this->_app->getCurrency());
        $tpl->replace('INPVAL_COMPANY', $order->company());
        $tpl->replace('INPVAL_FIRST_NAME', $order->firstName());
        $tpl->replace('INPVAL_LAST_NAME', $order->lastName());
        $tpl->replace('INPVAL_EMAIL', $order->email());
        $tpl->replace('INPVAL_PHONE', $order->phone());
        $tpl->replace('INPVAL_ADDRESS', $order->address());
        $tpl->replace('INPVAL_CITY', $order->city());
        $tpl->replace('INPVAL_POST_CODE', $order->postCode());
        $tpl->replace('INPVAL_COUNTRY', Html::getOptions($this->_app->countries(), $order->country()), 'raw');
        $tpl->replace('INPVAL_ADDRESS2', $order->deliveryAddress());
        $tpl->replace('INPVAL_CITY2', $order->deliveryCity());
        $tpl->replace('INPVAL_POST_CODE2', $order->deliveryPostCode());
        $tpl->replace('INPVAL_COUNTRY2', Html::getOptions($this->_app->countries()), 'raw');
        $tpl->replace('INPVAL_IC', $order->ic());
        $tpl->replace('INPVAL_DIC', $order->dic());
        $tpl->replace('INPVAL_NOTE', $order->note());
        $tpl->showSub('REGISTRACE', is_null($customer));

        // items
        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('ITEMS');
        foreach ($order->getItems() as $item) {
            $tplRow->set($rowTemplate);
            $tplRow->replace('TXT_ITEM', $item->product());
            $tplRow->replace('TXT_ITEM_QUANTITY', $item->quantity());
            $tplRow->replace(
                    'TXT_ITEM_PRICE_UNIT_VAT', Vars::formatCurrency($item->getPriceUnitVatByLang(), $this->_app->getCurrency(), 2, ','), 'raw'
            );
            $tplRow->replace(
                    'TXT_ITEM_PRICE_ROW_VAT', Vars::formatCurrency($item->calcPriceRowByLang(true), $this->_app->getCurrency(), 2, ','), 'raw'
            );
            $cnt .= $tplRow->get();
        }
        $tpl->replaceSub('ITEMS', $cnt);
        $tpl->replace(
                'TXT_SUBTOTAL', Vars::formatCurrency($order->calcSubtotalByLang(false), $this->_app->getCurrency(), 2, ','), 'raw'
        );
        $tpl->replace('TXT_SUBTOTAL_NR', Vars::formatNumberHuman($order->calcSubtotalByLang(false), 2, '.', ''));
        $tpl->replace('TXT_SUBTOTAL_VAT', Vars::formatCurrency($order->calcSubtotalByLang(true), $this->_app->getCurrency(), 2, ','), 'raw');
        $tpl->replace('TXT_SUBTOTAL_VAT_NR', Vars::formatNumberHuman($order->calcSubtotalByLang(true), 2, '.', ''));

		$serviceFees = $this->_app->getInstance(\Eshop\Orders\Fees\Service::getName());
		$countryList = new \Eshop\Orders\Fees\CountryList();
		$paymentList = $serviceFees->getPaymentList();
		$deliverryList = $serviceFees->getDeliveryList();

        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('PAYMENTS');
        foreach ($countryList->getCountries() as $country) {
			foreach ($paymentList->getFeesEnabledForCountry($country->getCode()) as $fee) {
				$tplRow->set($rowTemplate);
				$tplRow->replace('INPVAL_ITEM_TYPE', $fee->getPaymentType());
				$tplRow->replace('INPVAL_ITEM_COUNTRY', $fee->getCountryCode());
				$tplRow->replace('TXT_ITEM', $paymentList->getTypeName($fee->getPaymentType()));
				$tplRow->replace('TXT_ITEM_FEE', Vars::formatCurrency($fee->getFeeByLang($lang), $currency, 2), 'raw');
				$tplRow->replace('TXT_ITEM_FEE_NR', Vars::formatNumberHuman($fee->getFeeByLang($lang), 2, '.', ''), 'raw');
				$tplRow->replace('TXT_ITEM_INFO', $fee->getInfo(), 'raw');
				$cnt .= $tplRow->get();
			}
		}
        $tpl->replaceSub('PAYMENTS', $cnt);

        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('DELIVERERS');
		foreach ($countryList->getCountries() as $country) {
			foreach ($deliverryList->getFeesEnabledForCountry($country->getCode()) as $fee) {
				$tplRow->set($rowTemplate);
				$tplRow->replace('INPVAL_ITEM_TYPE', $fee->getDeliveryType());
				$tplRow->replace('INPVAL_ITEM_COUNTRY', $fee->getCountryCode());
				$tplRow->replace('TXT_ITEM', $deliverryList->getTypeName($fee->getDeliveryType()));
				$tplRow->replace('TXT_ITEM_FEE', Vars::formatCurrency($fee->getFeeByLang($lang), $currency, 2), 'raw');
				$tplRow->replace('TXT_ITEM_FEE_NR', Vars::formatNumberHuman($fee->getFeeByLang($lang), 2, '.', ''), 'raw');
				$tplRow->replace('TXT_ITEM_INFO', $fee->getInfo(), 'raw');
				$tplRow->replace('TXT_ITEM_FREE', Vars::formatCurrency($fee->getFreeLimitByLang($lang), $currency, 2), 'raw');
				$tplRow->replace('TXT_ITEM_FREE_NR', Vars::formatNumberHuman($fee->getFreeLimitByLang($lang), 2, '.', ''));
				$tplRow->showSub('DELIVERY_FREE', $fee->getFreeLimitByLang($lang) > 0);

				$cnt .= $tplRow->get();
			}
		}
        $tpl->replaceSub('DELIVERERS', $cnt);

        $this->_app->response()->pageContent($tpl->get());
        $this->_app->response()->pageTitle('Objednávka  | [%LG_SITENAME%]');
    }

    /**
     * save order
     * @return void
     */
    public function handleSave() {
        $cart = $this->_app->getModule('eshop')->getCart(true);
        if ($cart->isEmpty()) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';

        $customer = $this->_app->getVisitor();

        $request = $this->_app->request();
        $register = $request->getPost('register', 'bool');

        $order = new \Eshop\Orders\Order($this->_app->dice()->create('\Eshop\Orders\DataOrder'));
        $order->fill($cart);

        // register new customer
        if (is_null($customer) && $register) {
            $customer = new Customers\Customer($this->_app->dice()->create('\Eshop\Customers\DataCustomer'));
            $customer->email($request->getPost('email'));
            $customer->setPassword($request->getPost('pwd'), $request->getPost('confirm'));
            $customer->firstName($request->getValue('first_name', Request::POST));
            $customer->lastName($request->getValue('last_name', Request::POST));
            $customer->company($request->getValue('company', Request::POST));
            $customer->phone($request->getValue('phone', Request::POST));
            $customer->ic($request->getValue('ic', Request::POST));
            $customer->dic($request->getValue('dic', Request::POST));
            $customer->address($request->getValue('address', Request::POST));
            $customer->city($request->getValue('city', Request::POST));
            $customer->postCode($request->getValue('post_code', Request::POST));
            if ($customer->save()) {
                $_SESSION[Module::SESSION_USER_ID] = $customer->instanceId();
            } else {
                $err = ($customer->err() == '' ? 'something wrong' : $customer->err());
            }
        }

        if ($err === '') {
            if (!is_null($customer)) {
                $order->customerId($customer->instanceId());
            }
            $order->email($request->getPost('email'));
            $order->firstName($request->getPost('first_name'));
            $order->lastName($request->getPost('last_name'));
            $order->company($request->getPost('company'));
            $order->phone($request->getPost('phone'));
            $order->ic($request->getPost('ic'));
            $order->dic($request->getPost('dic'));
            $order->address($request->getPost('address'));
            $order->city($request->getPost('city'));
            $order->postCode($request->getPost('post_code'));
            $order->country($request->getPost('country'));
            $order->deliveryAddress($request->getPost('delivery_address'));
            $order->deliveryCity($request->getPost('delivery_city'));
            $order->deliveryPostCode($request->getPost('delivery_post_code'));
            $order->deliveryCountry($request->getPost('delivery_country'));
            $order->paymentId($request->getPost('payment', 'int'));
            $order->delivererId($request->getPost('delivery', 'int'));
            $order->note($request->getPost('note'));
            $order->setLang($this->_app->currentLanguage());
            if ($order->save($register)) {
                $cart->emptyCart();
            } else {
                $err = ($order->err() == '' ? 'something wrong' : $order->err());
            }
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        if ($ret->status === 'OK') {
            $ret->id = $order->instanceId();
        }
        $ret->msg = $this->_app->translate($err);
        $this->_app->response()->setOutputJson($ret);
    }

}
