<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

/**
 * main class of module Eshop
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Module extends \Ergo\ModuleAbstract {

    /** @var module name */
    const NAME = 'eshop';

    /** @var name of session variable to store id of logged user */
    const SESSION_USER_ID = 'cid';

    /** @var name of session variable to store id of current cart */
    const SESSION_CART_ID = 'cart_id';

    /** @var \Eshop\Customers\Customer current logged customer */
    private $_user;

    public function __construct(\Ergo\ApplicationInterface $application) {
        parent::__construct($application);
        $this->_user = null;

        $this->_app->setShared('\Eshop\Products\DataProduct');
        $this->_app->setShared('\Eshop\Products\DataCategory');
        $this->_app->setShared('\Eshop\Products\DataGalleryImage');
        $this->_app->setShared('\Eshop\Products\DataImage');
        $this->_app->setShared('\Eshop\Products\DataIcon');
        $this->_app->setShared('\Eshop\Products\DataIconImage');
        $this->_app->setShared('\Eshop\Customers\DataCustomer');
        $this->_app->setShared('\Eshop\Customers\DataCart');
        $this->_app->setShared('\Eshop\DataSearch');
        $this->_app->setShared('\Eshop\DataConfig');
        $this->_app->setShared('\Eshop\Config');
        $this->_app->setShared('\Eshop\Products\Categories');
        $this->_app->setShared('\Eshop\Orders\DeliverersList');
        $this->_app->setShared('\Eshop\Orders\DataDeliverer');
    }

    public function menuItems() {
        $user = $this->_app->getUser();
        if (is_null($user) || !$user->hasPermission(self::NAME)) {
            return null;
        }
        $selected = 'products';
        if (strpos($this->_app->currentModuleParameter(), 'orders') !== false) {
            $selected = 'orders';
        }
        if (strpos($this->_app->currentModuleParameter(), 'categories') !== false) {
            $selected = 'categories';
        }
        if (strpos($this->_app->currentModuleParameter(), 'icons') !== false) {
            $selected = 'icons';
        }
        if (strpos($this->_app->currentModuleParameter(), 'config') !== false) {
            $selected = 'config';
        }

        return array(
            array(
                'name' => $this->name() . ': Zboží',
                'url' => $this->_app->getUrl(self::NAME, 'products'),
                'title' => 'Seznam zboží',
                'highlight' => ($this->_app->getCurrentModule() == self::NAME && $selected === 'products')
            ),
            array(
                'name' => $this->name() . ': Objednávky',
                'url' => $this->_app->getUrl(self::NAME, 'orders'),
                'title' => 'Seznam objednávek',
                'highlight' => ($this->_app->getCurrentModule() == self::NAME && $selected === 'orders')
            ),
            array(
                'name' => $this->name() . ': Zákazníci',
                'url' => $this->_app->getUrl(self::NAME, 'customers'),
                'title' => 'Seznam registrovaných zákazníků',
                'highlight' => ($this->_app->getCurrentModule() == self::NAME && $selected === 'customers')
            ),
            array(
                'name' => $this->name() . ': Kategorie zboží',
                'url' => $this->_app->getUrl(self::NAME, 'categories'),
                'title' => 'Seznam kategorií zboží',
                'highlight' => ($this->_app->getCurrentModule() == self::NAME && $selected === 'categories')
            ),
            array(
                'name' => $this->name() . ': Ikony',
                'url' => $this->_app->getUrl(self::NAME, 'icons'),
                'title' => 'Ikony vlastností produktů',
                'highlight' => ($this->_app->getCurrentModule() == self::NAME && $selected === 'icons')
            ),
            array(
                'name' => $this->name() . ': [%LG_SETTINGS%]',
                'url' => $this->_app->getUrl(self::NAME, 'config'),
                'title' => 'Nastavení e-shopu',
                'highlight' => ($this->_app->getCurrentModule() == self::NAME && $selected === 'config')
            )
        );
    }

    public function permissions() {
        return array();
    }

    public function output($parameter = '') {
        $map = array(
            '_box_home_' => array('\Eshop\ControllerProducts', 'renderBoxHome'),
            'ajax-userinfo' => array('\Eshop\ControllerCustomer', 'renderUserInfo'),
            'ajax-login' => array('\Eshop\ControllerCustomer', 'handleLogin'),
            'ajax-logout' => array('\Eshop\ControllerCustomer', 'handleLogout'),
            'profil' => array('\Eshop\ControllerCustomer', 'renderFormProfile'),
            'profil/objednavky' => array('\Eshop\ControllerCustomer', 'renderProfileOrders'),
            'ajax-orderdetail' => array('\Eshop\ControllerCustomer', 'handleOrderDetail'),
            'registrace' => array('\Eshop\ControllerCustomer', 'renderFormRegister'),
            'ajax-register' => array('\Eshop\ControllerCustomer', 'handleRegister'),
            'ajax-save' => array('\Eshop\ControllerCustomer', 'handleSave'),
            'ajax-changepassword' => array('\Eshop\ControllerCustomer', 'handleChangePassword'),
            'ajax-resetpassword' => array('\Eshop\ControllerCustomer', 'handleResetPassword'),
            'new-password' => array('\Eshop\ControllerCustomer', 'renderFormNewPassword'),
            'ajax-newpassword' => array('\Eshop\ControllerCustomer', 'handleNewPassword'),
            '_box_categories_' => array('\Eshop\ControllerProducts', 'renderBoxCategories'),
            'kosik' => array('\Eshop\ControllerCarts', 'renderCart'),
            'ajax-cartadd' => array('\Eshop\ControllerCarts', 'handleAdd'),
            'ajax-cartremove' => array('\Eshop\ControllerCarts', 'handleRemove'),
            'objednavka' => array('\Eshop\ControllerOrders', 'renderFormOrder'),
            'ajax-saveorder' => array('\Eshop\ControllerOrders', 'handleSave'),
        );

        if (isset($map[$parameter])) {
            return call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
        }

        if (substr($parameter, 0, 5) === '_url_') {
            return $this->_app->getUrl(self::NAME, substr($parameter, 5));
        }
        if (substr($parameter, 0, 10) === 'kategorie/') {
            $this->_app->dice()->create('\Eshop\ControllerProducts')->renderCategory(substr($parameter, 10));
            return;
        }
        if (substr($parameter, 0, 6) === 'zbozi/') {
            $this->_app->dice()->create('\Eshop\ControllerProducts')->renderProduct(substr($parameter, 6));
            return;
        }
        if (substr($parameter, 0, 6) === 'ikona/') {
            $this->_app->dice()->create('\Eshop\ControllerProducts')->renderIcon(substr($parameter, 6));
            return;
        }

        return parent::output($parameter);
    }

    public function outputAdmin($parameter = '') {
        $map = array(
            'products' => array('\Eshop\Admin\ControllerProducts', 'renderList'),
            'ajax-listproducts' => array('\Eshop\Admin\ControllerProducts', 'handleList'),
            'ajax-editproduct' => array('\Eshop\Admin\ControllerProducts', 'handleEdit'),
            'ajax-saveproduct' => array('\Eshop\Admin\ControllerProducts', 'handleSave'),
            'ajax-copyproduct' => array('\Eshop\Admin\ControllerProducts', 'handleCopy'),
            'ajax-deleteproduct' => array('\Eshop\Admin\ControllerProducts', 'handleDelete'),
            'ajax-publishproduct' => array('\Eshop\Admin\ControllerProducts', 'handlePublish'),
            'ajax-uploadproductimage' => array('\Eshop\Admin\ControllerProductsImages', 'handleUpload'),
            'ajax-deleteproductimage' => array('\Eshop\Admin\ControllerProductsImages', 'handleDelete'),
            'ajax-uploadproductgalleryimage' => array('\Eshop\Admin\ControllerProductsGallery', 'handleUpload'),
            'ajax-deleteproductgalleryimage' => array('\Eshop\Admin\ControllerProductsGallery', 'handleDelete'),
            'ajax-updateproductgalleryimage' => array('\Eshop\Admin\ControllerProductsGallery', 'handleUpdate'),
            'ajax-suggestproductvariant' => array('\Eshop\Admin\ControllerProductsVariants', 'handleSuggest'),
            'ajax-addproductvariant' => array('\Eshop\Admin\ControllerProductsVariants', 'handleAdd'),
            'ajax-removeproductvariant' => array('\Eshop\Admin\ControllerProductsVariants', 'handleRemove'),
            'ajax-suggestproductcross' => array('\Eshop\Admin\ControllerProductsCross', 'handleSuggest'),
            'ajax-addproductcross' => array('\Eshop\Admin\ControllerProductsCross', 'handleAdd'),
            'ajax-removeproductcross' => array('\Eshop\Admin\ControllerProductsCross', 'handleRemove'),
			'ajax-uploadproduct3d' => ['\Eshop\Products\Models3d\ControllerBackend', 'handleUpload'],
			'ajax-deleteproduct3d' => ['\Eshop\Products\Models3d\ControllerBackend', 'handleDelete'],
            'categories' => array('\Eshop\Admin\ControllerCategories', 'renderList'),
            'ajax-listcategories' => array('\Eshop\Admin\ControllerCategories', 'handleList'),
            'ajax-editcategory' => array('\Eshop\Admin\ControllerCategories', 'handleEdit'),
            'ajax-savecategory' => array('\Eshop\Admin\ControllerCategories', 'handleSave'),
            'ajax-deletecategory' => array('\Eshop\Admin\ControllerCategories', 'handleDelete'),
            'ajax-movecategory' => array('\Eshop\Admin\ControllerCategories', 'handleMove'),
            'icons' => array('\Eshop\Admin\ControllerIcons', 'renderList'),
            'ajax-listicons' => array('\Eshop\Admin\ControllerIcons', 'handleList'),
            'ajax-editicon' => array('\Eshop\Admin\ControllerIcons', 'handleEdit'),
            'ajax-saveicon' => array('\Eshop\Admin\ControllerIcons', 'handleSave'),
            'ajax-deleteicon' => array('\Eshop\Admin\ControllerIcons', 'handleDelete'),
            'ajax-uploadiconimage' => array('\Eshop\Admin\ControllerIconsImages', 'handleUpload'),
            'ajax-deleteiconimage' => array('\Eshop\Admin\ControllerIconsImages', 'handleDelete'),
            'orders' => array('\Eshop\Admin\ControllerOrders', 'renderList'),
            'ajax-listorders' => array('\Eshop\Admin\ControllerOrders', 'handleList'),
            'ajax-editorder' => array('\Eshop\Admin\ControllerOrders', 'handleEdit'),
            'ajax-saveorder' => array('\Eshop\Admin\ControllerOrders', 'handleSave'),
            'ajax-saveordersent' => array('\Eshop\Admin\ControllerOrders', 'handleSaveSent'),
            'customers' => array('\Eshop\Admin\ControllerCustomers', 'renderList'),
            'ajax-listcustomers' => array('\Eshop\Admin\ControllerCustomers', 'handleList'),
            'ajax-editcustomer' => array('\Eshop\Admin\ControllerCustomers', 'handleEdit'),
//            'ajax-savecustomer' => array('\Eshop\Admin\ControllerCustomers', 'handleSave'),
            'ajax-addcustomerdiscount' => array('\Eshop\Admin\ControllerCustomers', 'handleAddDiscount'),
            'ajax-removecustomerdiscount' => array('\Eshop\Admin\ControllerCustomers', 'handleRemoveDiscount'),
            'config' => array('\Eshop\Admin\ControllerConfig', 'renderFormConfig'),
            'ajax-saveconfig' => array('\Eshop\Admin\ControllerConfig', 'handleSave')
        );

        if (isset($map[$parameter])) {
//        	var_dump($this->_app->dice()->create('\Eshop\Products\Models3d\ControllerBackend'));
//        	die('EE');
//        	var_dump($map[$parameter][1]);
            call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
            return;
        }

//        if (substr($parameter, 0, 5) === '_url_') {
//            return $this->_app->getUrl(Module::NAME, substr($parameter, 5));
//        }
        return parent::outputAdmin($parameter);
    }

    public function name() {
        return 'E-shop';
    }

    public function cron() {
        // once a day recalc prices in eur
        $fullPath = $this->_app->config()->baseDirLogCron() . self::NAME . '_recalc_prices.lastrun';
        $lastRun = '';
        if (is_file($fullPath)) {
            $lastRun = trim(file_get_contents($fullPath));
        }
        if ($lastRun != date('Y-m-d')) {
            $this->_app->getInstance(Products\ServicePrices::getName(), array($this->_app))->recalcPricesEur();
            file_put_contents($fullPath, date('Y-m-d'));
        }

        // once a day refresh search index
        $fullPath = $this->_app->config()->baseDirLogCron() . self::NAME . '_search.lastrun';
        $lastRun = '';
        if (is_file($fullPath)) {
            $lastRun = trim(file_get_contents($fullPath));
        }
        if ($lastRun != date('Y-m-d')) {
            $this->_app->dice()->create('\Eshop\DataSearch', array($this->_app))->refreshSearchIndex();
            file_put_contents($fullPath, date('Y-m-d'));
        }
    }

    public function refreshSitemapLinks() {
        $this->_app->dice()->create('\Eshop\DataSitemap')->refreshLinks();
    }

    /**
     * returns current customer
     * @return \Eshop\Customers\Customer|null
     */
    public function getCustomer() {
        if (!is_null($this->_user)) {
            return $this->_user;
        }
        $userId = $this->_app->request()->getValue(self::SESSION_USER_ID, 'sess', 'int');
        if ($userId > 0) {
            $this->_user = $this->_app->dice()->create('\Eshop\Customers\Customer', array($userId));
            if (!$this->_user->loaded()) {
                $this->_user = null;
            }
        }
        return $this->_user;
    }

    /**
     * returns hash of salted password
     * @param string $password
     * @param string $salt
     * @return string
     */
    public static function passwordHash($password, $salt = null) {
        $password = urlencode($password);
        if (is_null($salt)) {
            // '$2a$07$' - blowfish
            $salt = '$2a$07$' . \GstLib\Vars::randomString(22);
        }
        return crypt($password, $salt);
    }

    /**
     * return current shopping cart
     * @staticvar \Eshop\Products\Cart $cart
     * @param boolean $createNew true=create new cart if not exists
     * @return null|\Eshop\Products\Cart
     */
    public function getCart($createNew = false) {
        static $cart;
        if (!isset($cart) || defined('ERGO_TESTING')) {
            $cartId = $this->_app->request()->getValue(self::SESSION_CART_ID, 'sess');
            if ($cartId === '' && $createNew) {
                $cartId = md5(uniqid(rand(), true));
                $_SESSION[self::SESSION_CART_ID] = $cartId;
            }
            if ($cartId === '') {
                return null;
            }
            //$cart = new \Eshop\Products\Cart($this->_app->factory()->shared('\Eshop\Products\DataCart'), $cartId);
            $cart = $this->_app->dice()->create('\Eshop\Orders\Cart', array($cartId));
        }
        return $cart;
    }

    /* private function _renderFormConfig()
      {
      if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(self::NAME)) {
      $this->_app->rememberRedirectLogin();
      $this->_app->response()->redirect($this->_app->baseUrl(true));
      return;
      }

      $record = new Config($this->_app->factory()->shared('\Eshop\DataConfig'));

      $this->_app->response()->pageTitle('Nastavení eshopu');
      $this->_app->response()
      ->pageContent($this->_app->factory()->shared('\Eshop\GuiAdminConfig')->renderFormConfig($record));
      } */
}
