<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use GstLib\Html;
use GstLib\Template;
use Ergo\Pagination;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerIcons
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render page with list of records
     */
    public function renderList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->rememberRedirectLogin();
            $this->_app->response()->redirect($this->_app->baseUrl(true));
            return;
        }

        $this->_app->response()->pageTitle('Ikony produktů | [%LG_SITENAME%]');
        $tpl = new Template($this->_app->getTemplatePath('list_icons.htm', Module::NAME, true), true);
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $tpl->replace('INPVAL_ROWS', Html::getOptions($options), 'raw');

        // categories
        //$selectedItems = $record->getCategoryIds();
        //$tpl->replaceSub('ITEMS_CATEGORIES', $this->_renderProductCategories(0));


        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * render json with list of records
     */
    public function handleList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataIcon', array($this->_app));
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $pos = max(array(1, $request->getQuery(Pagination::POS_VARIABLE, 'int')));
        $order = array_intersect(array('id'), array($request->getQuery('order')));
        $order = (isset($order[0]) ? $order[0] : 'id');
        $desc = $request->getQuery('desc', 'bool');
        $rowsPage = max(array(10, $request->getQuery('rows_page', 'int')));

        $filterName = $request->getQuery('f_name');
        $criteria = array();
        if ($filterName !== '') {
            $criteria[] = array('name', $filterName, 'like');
        }

        $items = array();
        /** @var $record \Eshop\Products\Icon */
        foreach ($dataLayer->getRecords($pos-1, $rowsPage, $order, $desc, $criteria) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'name' => $record->name()
            );
        }

        $ret = new \stdClass();
        $ret->items = $items;
        $ret->total = $dataLayer->count($criteria);

        $pager = new Pagination($ret->total, $pos, $rowsPage);
        $ret->pages = $pager->getPages();

        $tmp = $pager->getRecords();
        $ret->pos = $tmp[0];
        $ret->posEnd = $tmp[1];
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render json with single record
     */
    public function handleEdit()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $record = $this->_app->dice()
            ->create('\Eshop\Products\Icon', array($this->_app->request()->getQuery('id', 'int')));

        $ret = new \stdClass();
        if ($record->loaded()) {
            $ret->record = (object) array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
                'text' => $record->text(),
                'name_en' => $record->getNameEn(),
                'text_en' => $record->getTextEn(),
                'imageUrl'=>$record->getMainImage()->getAttachmentUrl(\Eshop\Products\IconImage::IMG_ORIG),
            );
        }

        $ret->status = ($record->loaded() ? 'OK' : 'ERR');
        $ret->msg = ($record->loaded() ? '' : 'Record not found');
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle save record
     */
    public function handleSave()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Products\Icon', array($request->getPost('id', 'int')));

        $record->name($request->getPost('name'));
        $record->text($request->getPost('text', 'html'));
		$record->setNameEn($request->getPost('name_en'))
			->setTextEn($request->getPost('text_en', 'html'));

        if (!$record->save()) {
            $err = ($record->err() === '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        $ret->id = $record->instanceId();
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete record
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $record = $this->_app->dice()->create('\Eshop\Products\Icon', array($request->getQuery('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $err = ($record->delete() ? '' : $record->err());
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

}