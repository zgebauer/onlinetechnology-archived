'use strict';

Ergo.Eshop = Ergo.Eshop || {};
Ergo.Eshop.Orders = Ergo.Eshop.Orders || {};

Ergo.Eshop.Orders.Admin = function() {
    var nodeList = $('#ordersList');
    var nodeDialog = $('#dialog');
    var editRecord;
    var nodeMsg;

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/eshop/ajax-listorders';
    recordsList.itemTemplate = '<tr class="status%status_id%"><td>%id%</td><td>%date%</td><td>%name%</td><td>%company%</td><td>%status%</td>'
      + '<td data-item-edit="%id%" class="clickable edit">Upravit</td></tr>';
    recordsList.orderBy = 'id';
    recordsList.orderDesc = 1;
    recordsList.callbackLoaded = function(list, data) {
        pagination.setPages(list.pages);
        positions.update(list.pages);
        // parse id url and edit
        var match;
        match = /[\?&]id=([^&]+)/i.exec(window.location.search);
        var showId = (match && match.length > 1 ? decodeURIComponent(match[1]) : '0');
        if ($('[data-item-edit="'+showId+'"]').length > 0) {
            editItem(showId);
        }
    };
    recordsList.load();

    var pagination = new Ergo.RecordsList.Pagination($('[data-pos]'), recordsList);
    var positions = new Ergo.RecordsList.Positions(recordsList, $('[data-pos-start]'), $('[data-pos-end]'), $('[data-pos-total]'));
    var rowsPage = new Ergo.RecordsList.RowsPage($('.pagination [data-rows-page]'), recordsList);
    var formFilter = new Ergo.RecordsList.Filter($('.frmFilter'), recordsList);

    var renderEditForm = function() {
        var html = $('#detailEdit').html();
        html = html.replace(/%id%/g, editRecord.id);
        var d = new Date(editRecord.date);
        html = html.replace(/%date%/g, d.getHours()+':'+('0'+d.getMinutes()).slice(-2)+' '+d.getDate()+'.'+(d.getMonth()+1)+'.'+d.getFullYear());
        html = html.replace(/%first_name%/g, editRecord.first_name);
        html = html.replace(/%last_name%/g, editRecord.last_name);
        html = html.replace(/%email%/g, editRecord.email);
        html = html.replace(/%phone%/g, editRecord.phone);
        html = html.replace(/%address%/g, editRecord.address);
        html = html.replace(/%city%/g, editRecord.city);
        html = html.replace(/%post_code%/g, editRecord.post_code);
        html = html.replace(/%delivery_address%/g, editRecord.delivery_address);
        html = html.replace(/%delivery_city%/g, editRecord.delivery_city);
        html = html.replace(/%delivery_post_code%/g, editRecord.delivery_post_code);
        html = html.replace(/%company%/g, editRecord.company);
        html = html.replace(/%ic%/g, editRecord.ic);
        html = html.replace(/%dic%/g, editRecord.dic);
        html = html.replace(/%note%/g, editRecord.note);
        html = html.replace(/%payment%/g, editRecord.payment);
        html = html.replace(/%deliverer%/g, editRecord.deliverer);
        html = html.replace(/%price_payment%/g, editRecord.price_payment);
        html = html.replace(/%price_payment_eur%/g, editRecord.price_payment_eur);
        html = html.replace(/%price_delivery%/g, editRecord.price_delivery);
        html = html.replace(/%price_delivery_eur%/g, editRecord.price_delivery_eur);
        html = html.replace(/%subtotal%/g, editRecord.subtotal);
        html = html.replace(/%subtotal_eur%/g, editRecord.subtotal_eur);
        html = html.replace(/%subtotal_vat%/g, editRecord.subtotal_vat);
        html = html.replace(/%subtotal_eur_vat%/g, editRecord.subtotal_eur_vat);
        html = html.replace(/%grandtotal%/g, editRecord.grandtotal);
        html = html.replace(/%grandtotal_eur%/g, editRecord.grandtotal_eur);
        html = html.replace(/%grandtotal_vat%/g, editRecord.grandtotal_vat);
        html = html.replace(/%grandtotal_eur_vat%/g, editRecord.grandtotal_eur_vat);
        if (editRecord.date_sent === '') {
            html = html.replace(/%date_sent%/g, '');
        } else {
            d = new Date(editRecord.date_sent);
            html = html.replace(/%date_sent%/g, d.getHours()+':'+('0'+d.getMinutes()).slice(-2)+' '+d.getDate()+'.'+(d.getMonth()+1)+'.'+d.getFullYear());
        }

        nodeDialog.html(html);
        if (editRecord.date_sent !== '') {
            nodeDialog.find('.but-sent').remove();
        }

        var htmlItems = '';
        for (var i=0; i < editRecord.items.length; i++)  {
            htmlItems += '<tr><td>'+editRecord.items[i].name+'</td>'
                + '<td class="number">'+editRecord.items[i].price_unit+' / '+editRecord.items[i].price_unit_vat+'</td>'
                + '<td class="number">'+editRecord.items[i].vat+'%</td><td class="number">'+editRecord.items[i].quantity+'</td>'
                + '<td class="number">'+editRecord.items[i].price_row+' / '+editRecord.items[i].price_row_vat+'</td></tr>';
        }
        nodeDialog.find('table.items tbody').html(htmlItems);

        nodeMsg = nodeDialog.find('.msg');

        nodeDialog.find('input[name="id"]').val(editRecord.id);
        nodeDialog.find('input[name="parcel"]').val(editRecord.parcel);
        nodeDialog.find('select[name="status"]').val(editRecord.status);

        nodeDialog.find('input[id], textarea[id]').each(function() {
            $(this).prop('id', 'edit_'+$(this).prop('id'));
        });
        nodeDialog.find('label[for]').each(function() {
            $(this).prop('for', 'edit_'+$(this).prop('for'));
        });

        nodeDialog.on('mousedown', '.but-sent', function(event) {
            event.stopPropagation();
            if (!$(this).prop('disabled')) {
                if (confirm('Opravdu chcete odeslat zákazníkovi e-mail o odeslání zboží?')) {
                    saveItemsSent();
                } else {
                    $(this).prop('disabled', false);
                }
            }
            return false;
        });

        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');

    };

    var saveItem = function(closeAfterSave) {
        var postdata = nodeDialog.find('form').serialize();
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-saveorder',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            nodeDialog.find('.msg').html('');
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            } else {
                editItem(data.id);
            }
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-editorder?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            editRecord = data.record;
            renderEditForm();
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var saveItemsSent = function() {
        var parcel = $.trim(nodeDialog.find('[name="parcel"]').val());
        if (parcel === '') {
            nodeDialog.find('.msg').html('Vyplňte číslo zásilky');
            nodeDialog.find('[name="parcel"]').focus();
            return false;
        }

        nodeDialog.find('.but-sent').prop('disabled', true);
        var postdata = { id: editRecord.id, parcel: parcel};
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-saveordersent',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItemSent(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItemSent( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItemSent = function(data) {
        nodeDialog.find('.but-sent').prop('disabled', false);
        if (data.status === 'OK') {
            nodeDialog.find('.msg').html('');
            var d = new Date(data.dateSent);
            nodeDialog.find('.but-sent').replaceWith(d.getHours()+':'+('0'+d.getMinutes()).slice(-2)+' '+d.getDate()+'.'+(d.getMonth()+1)+'.'+d.getFullYear());
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); });

};