<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use Ergo\ApplicationInterface;
use Eshop\Products\Models3d\Service as ServiceModels3d;
use GstLib\Html;
use GstLib\Template;
use Ergo\Pagination;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerProducts
{
	/**
	 * @var ServiceModels3d
	 */
	private $serviceModels3d;

	/**
	 * @param ApplicationInterface $application current application
	 * @param ServiceModels3d $serviceModels3d
	 */
	public function __construct(ApplicationInterface $application, ServiceModels3d $serviceModels3d) {
		$this->_app = $application;
		$this->serviceModels3d = $serviceModels3d;
	}

    /**
     * render page with list of records
     */
    public function renderList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->rememberRedirectLogin();
            $this->_app->response()->redirect($this->_app->baseUrl(true));
            return;
        }

        $this->_app->response()->pageTitle('Zboží | [%LG_SITENAME%]');
        $tpl = new Template($this->_app->getTemplatePath('list_products.htm', Module::NAME, true), true);
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $tpl->replace('INPVAL_ROWS', Html::getOptions($options), 'raw');

        // categories
        //$selectedItems = $record->getCategoryIds();
        //$tpl->replaceSub('ITEMS_CATEGORIES', $this->_renderProductCategories(0));

        $categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');
        $tpl->replace('INPVAL_CATEGORY_ID', Html::getOptions($categoriesList->optionsCategory()), 'raw');

        // icons
        $cnt = '';
        $rowTemplate = $tpl->getSub('ITEMS_ICONS');
        $tplRow = new Template();
        foreach ($this->_app->dice()->create('\Eshop\Products\DataIcon', array($this->_app))->getRecords() as $item) {
            $tplRow->set($rowTemplate);
            $tplRow->replace('TXT_ITEM', $item->name());
            $tplRow->replace('INPVAL_ITEM_ID', $item->instanceId());
            $cnt .= $tplRow->get();
        }
        unset($tplRow);
        $tpl->replaceSub('ITEMS_ICONS', $cnt);


        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * render json with list of records
     */
    public function handleList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app));
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $pos = max(array(1, $request->getQuery(Pagination::POS_VARIABLE, 'int')));
        $order = array_intersect(array('id'), array($request->getQuery('order')));
        $order = (isset($order[0]) ? $order[0] : 'id');
        $desc = $request->getQuery('desc', 'bool');
        $rowsPage = max(array(10, $request->getQuery('rows_page', 'int')));

        $filterName = $request->getQuery('f_name');
        $criteria = array();
        if ($filterName !== '') {
            $criteria[] = array('name', $filterName, 'like');
        }

        $items = array();
        /** @var $record \Eshop\Products\Product */
        foreach ($dataLayer->getRecords($pos-1, $rowsPage, $order, $desc, $criteria) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
                'publish' => ($record->published() ? 'Ano' : 'Ne'),
            );
        }

        $ret = new \stdClass();
        $ret->items = $items;
        $ret->total = $dataLayer->count($criteria);

        $pager = new Pagination($ret->total, $pos, $rowsPage);
        $ret->pages = $pager->getPages();

        $tmp = $pager->getRecords();
        $ret->pos = $tmp[0];
        $ret->posEnd = $tmp[1];
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render json with single record
     */
    public function handleEdit()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $record = $this->_app->dice()
            ->create('\Eshop\Products\Product', array($this->_app->request()->getQuery('id', 'int')));

        $ret = new \stdClass();
        if ($record->loaded()) {
            $images = array();
            foreach ($record->getImages() as $image) {
                $url = $image->getAttachmentUrl(\Eshop\Products\GalleryImage::IMG_THUMB);
                $images[] = array(
                    'id' => $image->instanceId(),
                    'description' => $image->description(),
                    'url' => $url
                );
            }
            $variants = array();
            foreach ($record->getVariantProducts() as $variant) {
                $variants[] = array(
                    'id' => $variant->instanceId(),
                    'name' => $variant->name(),
                );
            }
            $cross = array();
            foreach ($record->getCrossSellProducts() as $crosssell) {
                $cross[] = array(
                    'id' => $crosssell->instanceId(),
                    'name' => $crosssell->name(),
                );
            }
            $ret->record = (object) array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
                'name_en' => $record->getNameEn(),
                'seo' => $record->seo(),
                'publish' => $record->published(),
                'category_id' => $record->categoryId(),
                'meta_desc' => $record->metaDescription(),
                'meta_keys' => $record->metaKeywords(),
                'text' => $record->text(),
                'text_en' => $record->getTextEn(),
                'bestseller'=>$record->bestseller(),
                'featured'=>$record->featured(),
                'price'=>$record->price(),
                'price_vat'=>$record->priceVat(),
                'vat'=>$record->vat(),
                'price_eur'=>$record->getPriceEur(),
                'price_eur_vat'=>$record->getPriceEurVat(),
                'imageUrl'=>$record->getMainImage()->getAttachmentUrl(\Eshop\Products\Image::IMG_ORIG),
                'images' => $images,
                'variants' => $variants,
                'cross' => $cross,
                'icons' => $record->getIconsIds(),
                'popis_modulu' => $record->popisModulu(),
                'popis_modulu_en' => $record->getPopisModuluEn(),
                'zapojeni' => $record->zapojeni(),
                'zapojeni_en' => $record->getZapojeniEn(),
                'dokumentace' => $record->dokumentace(),
                'dokumentace_en' => $record->getDokumentaceEn(),
                'firmware' => $record->firmware(),
                'firmware_en' => $record->getFirmwareEn(),
                'programy' => $record->programy(),
                'programy_en' => $record->getProgramyEn(),
				'stock_info' => $record->getStockInfo(),
				'stock_info_en' => $record->getStockInfoEn()
            );
			$ret->record->url_model3d = $this->serviceModels3d->getFile($record->instanceId())->getUrl();
        }

        $ret->status = ($record->loaded() ? 'OK' : 'ERR');
        $ret->msg = ($record->loaded() ? '' : 'Record not found');
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle save record
     */
    public function handleSave()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

		/* @var $record \Eshop\Products\Product */
        $record = $this->_app->dice()->create('\Eshop\Products\Product', array($request->getPost('id', 'int')));

        $record->name($request->getPost('name'));
        $record->setNameEn($request->getPost('name_en'));
        $record->seo($request->getPost('seo'));
        $record->text($request->getPost('text', 'html'));
        $record->setTextEn($request->getPost('text_en', 'html'));
        $record->published($request->getPost('publish', 'bool'));
        $record->metaDescription($request->getPost('meta_desc'));
        $record->metaKeywords($request->getPost('meta_keys'));
        $record->bestseller($request->getPost('bestseller', 'bool'));
        $record->featured($request->getPost('featured', 'bool'));
        $record->price($request->getPost('price', 'float'));
        $record->vat($request->getPost('vat', 'float'));
        $record->setPriceEur($request->getPost('price_eur', 'float'));
        $record->categoryId($request->getPost('category_id', 'int'));
        $record->setIconsIds(isset($_POST['icons']) ? $_POST['icons'] : array());
        $record->popisModulu($request->getPost('popis_modulu', 'html'));
        $record->setPopisModuluEn($request->getPost('popis_modulu_en', 'html'));
        $record->zapojeni($request->getPost('zapojeni', 'html'));
        $record->setZapojeniEn($request->getPost('zapojeni_en', 'html'));
        $record->dokumentace($request->getPost('dokumentace', 'html'));
        $record->setDokumentaceEn($request->getPost('dokumentace_en', 'html'));
        $record->firmware($request->getPost('firmware', 'html'));
        $record->setFirmwareEn($request->getPost('firmware_en', 'html'));
        $record->programy($request->getPost('programy', 'html'));
        $record->setProgramyEn($request->getPost('programy_en', 'html'));
        $record->setStockInfo($request->getPost('stock_info'));
        $record->setStockInfoEn($request->getPost('stock_info_en'));

        if (!$record->save()) {
            $err = ($record->err() === '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        $ret->id = $record->instanceId();
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle save record as new
     */
    public function handleCopy()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $record = $this->_app->dice()->create('\Eshop\Products\Product', array($request->getPost('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $newId = $record->saveCopy();
            $err = ($newId ? '' : $record->err());
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }


        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        if ($err === '') {
            $ret->id = $newId;
        }
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete record
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $record = $this->_app->dice()->create('\Eshop\Products\Product', array($request->getQuery('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $err = ($record->delete() ? '' : $record->err());
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle change flag "publish"
     */
    public function handlePublish()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        $instanceId = $this->_app->request()->getQuery('id', 'int');

        $publish = null;
        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app));
        $err = ($dataLayer->changePublish($instanceId, $publish) ? '' : 'something wrong');

        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->id = $instanceId;
        $ret->publish = (is_null($publish) ? '' : ($publish ? 'Ano' : 'Ne'));
        $this->_app->response()->setOutputJson($ret);
    }

}