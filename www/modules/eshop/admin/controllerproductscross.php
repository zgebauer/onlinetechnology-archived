<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerProductsCross
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * renders product names for autocomplete. Expects
     * $_GET[name] - part of product name
     * @return void
     */
    public function handleSuggest()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $filterName = $request->getQuery('name');
        if ($filterName === '') {
            $this->_app->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
            return;
        }

        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app));

        $items = array();
        $criteria = array();
        $criteria[] = array('name', $filterName, 'like');
        /** @var $record \Eshop\Products\Product */
        foreach ($dataLayer->getRecords(0, 10, 'name', FALSE, $criteria) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
            );
        }

        $ret = new \stdClass();
        $ret->status = 'OK';
        $ret->items = $items;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * add relation between product and cross-sell product. Expects
     * $_GET[id] - product id
     * $_GET[add] - cross-sell product id
     * @return void
     */
    public function handleAdd()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $productId = $request->getQuery('id', 'int');
        $variantId = $request->getQuery('add', 'int');

        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app));

        $ret = $dataLayer->addCrossSell($productId, $variantId);

        $ret = new \stdClass();
        $ret->status = $ret ? 'OK' : 'ERR';

        $items = array();
        /** @var $record \Eshop\Products\Product */
        foreach ($dataLayer->getCrossSellProducts($productId) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
            );
        }

        $ret->items = $items;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * remove relation between product and cross-sell product. Expects
     * $_GET[id] - product id
     * $_GET[add] - cross-sell product id
     * @return void
     */
    public function handleRemove()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $productId = $request->getQuery('id', 'int');
        $variantId = $request->getQuery('remove', 'int');

        $dataLayer = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app));

        $ret = $dataLayer->removeCrossSell($productId, $variantId);

        $ret = new \stdClass();
        $ret->status = $ret ? 'OK' : 'ERR';

        $items = array();
        /** @var $record \Eshop\Products\Product */
        foreach ($dataLayer->getCrossSellProducts($productId) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
            );
        }

        $ret->items = $items;
        $this->_app->response()->setOutputJson($ret);
    }

}