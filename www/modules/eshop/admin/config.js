'use strict';

Ergo.Eshop = Ergo.Eshop || {};
Ergo.Eshop.Config = Ergo.Eshop.Config || {};

Ergo.Eshop.Config.Admin = function() {
    var nodeForm = $('#frmEshopSettings');

    nodeForm.submit(function(event) {
        event.preventDefault();

        var postdata = nodeForm.serialize();
        nodeForm.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-saveconfig',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSave(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSave( { msg: errorThrown } );
        });
        return false;
    });

    var handleResponseSave = function(data) {
        nodeForm.find('.msg').html(data.msg);
    };

};