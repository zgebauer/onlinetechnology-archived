<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use GstLib\Html;
use GstLib\Vars;
use GstLib\Template;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerConfig {

	/**
	 * constructor
	 * @param \Ergo\ApplicationInterface $application current application
	 */
	public function __construct(\Ergo\ApplicationInterface $application) {
		$this->_app = $application;
	}

	/**
	 * render configuration form
	 */
	public function renderFormConfig() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->rememberRedirectLogin();
			$this->_app->response()->redirect($this->_app->baseUrl(true));
			return;
		}

		$config = $this->_app->dice()->create('\Eshop\Config');

		/* @var $serviceFees \Eshop\Orders\Fees\Service */
		$serviceFees = $this->_app->getInstance(\Eshop\Orders\Fees\Service::getName());

		$paymentList = $serviceFees->getPaymentList();
		$deliveryList = $serviceFees->getDeliveryList();

		$countryList = new \Eshop\Orders\Fees\CountryList();

		$tpl = new Template($this->_app->getTemplatePath('config.htm', Module::NAME, true), true);
		$tpl->replace('INPVAL_EMAIL', $config->getParam('email_order'));

		$cnt = '';
		$rowTemplate = $tpl->getSub('ITEMS_PAYMENT');
		$rowTemplateCountry = $tpl->getSub('ITEMS_PAYMENT_COUNTRY');
		$tplRow = new Template();
		$tplRowCountry = new Template();
		foreach ($paymentList->getAvailablePayments() as $paymentType) {
			$tplRow->set($rowTemplate);
			$tplRow->replace('TXT_PAYMENT_NAME', $paymentType->getName());
			$cntCountry = '';
			foreach ($countryList->getCountries() as $country) {
				$fee = $paymentList->getFee($paymentType->getType(), $country->getCode());
				$tplRowCountry->set($rowTemplateCountry);
				$tplRowCountry->replace('VAL_TYPE', $paymentType->getType());
				$tplRowCountry->replace('VAL_COUNTRY', $country->getCode());
				$tplRowCountry->replace('TXT_COUNTRY', $country->getName());
				$tplRowCountry->replace('INPCHECK_ALLOW', Html::isChecked($fee->isAllowed()));
				$tplRowCountry->replace('INPVAL_FEE', Vars::formatNumberHuman($fee->getFee(), 2));
				$tplRowCountry->replace('INPVAL_FEE_EUR', Vars::formatNumberHuman($fee->getFeeEur(), 2));
				$tplRowCountry->replace('INPVAL_INFO', $fee->getInfo());
				$cntCountry .= $tplRowCountry->get();
			}
			$tplRow->replaceSub('ITEMS_PAYMENT_COUNTRY', $cntCountry);
			$cnt .= $tplRow->get();
		}
		$tpl->replaceSub('ITEMS_PAYMENT', $cnt);

		$cnt = '';
		$rowTemplate = $tpl->getSub('ITEMS_DELIVERY');
		$rowTemplateCountry = $tpl->getSub('ITEMS_DELIVERY_COUNTRY');
		$tplRow = new Template();
		$tplRowCountry = new Template();
		foreach ($deliveryList->getAvailableDeliveryTypes() as $deliveryType) {
			$tplRow->set($rowTemplate);
			$tplRow->replace('TXT_DELIVERY_NAME', $deliveryType->getName());
			$cntCountry = '';
			foreach ($countryList->getCountries() as $country) {
				$fee = $deliveryList->getFee($deliveryType->getType(), $country->getCode());
				$tplRowCountry->set($rowTemplateCountry);
				$tplRowCountry->replace('VAL_TYPE', $deliveryType->getType());
				$tplRowCountry->replace('VAL_COUNTRY', $country->getCode());
				$tplRowCountry->replace('TXT_COUNTRY', $country->getName());
				$tplRowCountry->replace('INPCHECK_ALLOW', Html::isChecked($fee->isAllowed()));
				$tplRowCountry->replace('INPVAL_FEE', Vars::formatNumberHuman($fee->getFee(), 2));
				$tplRowCountry->replace('INPVAL_FEE_EUR', Vars::formatNumberHuman($fee->getFeeEur(), 2));
				$tplRowCountry->replace('INPVAL_FREE', Vars::formatNumberHuman($fee->getFreeLImit(), 2));
				$tplRowCountry->replace('INPVAL_FREE_EUR', Vars::formatNumberHuman($fee->getFreeLimitEur(), 2));
				$tplRowCountry->replace('INPVAL_INFO', $fee->getInfo());
				$cntCountry .= $tplRowCountry->get();
			}
			$tplRow->replaceSub('ITEMS_DELIVERY_COUNTRY', $cntCountry);
			$cnt .= $tplRow->get();
		}
		$tpl->replaceSub('ITEMS_DELIVERY', $cnt);

		$this->_app->response()->pageContent($tpl->get());
		$this->_app->response()->pageTitle('Nastavení e-shopu | [%LG_SITENAME%]');
	}

	/**
	 * handle save record
	 */
	public function handleSave() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$err = '';
		/** @var $request \Ergo\Request */
		$request = $this->_app->request();

		$config = $this->_app->dice()->create('\Eshop\Config');

		$config->setParam('email_order', $request->getPost('email'));

		/* @var $serviceFees \Eshop\Orders\Fees\Service */
		$serviceFees = $this->_app->getInstance(\Eshop\Orders\Fees\Service::getName());
		$paymentsList = $serviceFees->getPaymentList();
		$deliveryList = $serviceFees->getDeliveryList();

		foreach ($_POST['payment'] as $paymentType => $countryFees) {
			foreach ($countryFees as $countryCode => $params) {
				$fee = new \Eshop\Orders\Fees\PaymentCountryFee($paymentType, $countryCode);
				$fee->setAllowed(isset($params['allow']) && $params['allow'] === 'on')
					->setFee(Vars::floatval($params['fee']))
					->setFeeEur(Vars::floatval($params['fee_eur']))
					->setInfo($params['info']);
				$paymentsList->setFee($fee);
			}
		}

		foreach ($_POST['delivery'] as $paymentType => $countryFees) {
			foreach ($countryFees as $countryCode => $params) {
				$fee = new \Eshop\Orders\Fees\DeliveryCountryFee($paymentType, $countryCode);
				$fee->setAllowed(isset($params['allow']) && $params['allow'] === 'on')
					->setFee(Vars::floatval($params['fee']))
					->setFeeEur(Vars::floatval($params['fee_eur']))
					->setFreeLimit(Vars::floatval($params['free_limit']))
					->setFreeLimitEur(Vars::floatval($params['free_limit_eur']))
					->setInfo($params['info']);
				$deliveryList->setFee($fee);
			}
		}

		if (!$config->save()) {
			$err = ($config->err() == '' ? 'something wrong' : $config->err());
		}

		if ($err === '') {
			try {
				$serviceFees->savePaymentList($paymentsList);
				$serviceFees->saveDeliveryList($deliveryList);
			} catch (Exception $ex) {
				$err = ($ex->getMessage());
			}
		}

		$ret = new \stdClass();
		$ret->status = ($err === '' ? 'OK' : 'ERR');
		$ret->msg = $this->_app->translate(($err === '' ? '[%LG_MSG_SETTINGS_SAVED%]' : $err));
		$this->_app->response()->setOutputJson($ret);
	}

}
