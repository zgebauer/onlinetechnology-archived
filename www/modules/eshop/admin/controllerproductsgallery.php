<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerProductsGallery
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * handle upload image
     */
    public function handleUpload()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Products\Product', array($request->getPost('id', 'int')));
        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $image = $this->_app->dice()->create('\Eshop\Products\GalleryImage');
            $image->parentId($record->instanceId());
            if (!$image->save('img')) {
                $err = ($image->err() === '' ? 'something wrong' : $image->err());
            }
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        if ($ret->status === 'OK') {
            $images = array();
            foreach ($record->getImages() as $image) {
                $url = $image->getAttachmentUrl(\Eshop\Products\GalleryImage::IMG_THUMB);
                $images[] = array(
                    'id' => $image->instanceId(),
                    'description' => $image->description(),
                    'url' => $url
                );
            }
            $ret->images = $images;
        }
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete image
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $imageId = $request->getQuery('id', 'int');
        $image = $this->_app->dice()->create('\Eshop\Products\GalleryImage', array($imageId));

        if (!$image->delete()) {
            $err = ($image->err() === '' ? 'something wrong' : $image->err());
        }


        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $ret->id = $imageId;
        if ($ret->status === 'OK') {
            $record = $this->_app->dice()->create('\Eshop\Products\Product', array($image->parentId()));

            $images = array();
            foreach ($record->getImages() as $image) {
                $url = $image->getAttachmentUrl(\Eshop\Products\GalleryImage::IMG_THUMB);
                $images[] = array(
                    'id' => $image->instanceId(),
                    'description' => $image->description(),
                    'url' => $url
                );
            }
            $ret->images = $images;
        }
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle move image
     */
    public function handleMove()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $image = $this->_app->dice()->create('\Eshop\Image', array($request->getQuery('id', 'int')));
        $direction = array_intersect(array('up', 'down'), array($request->getQuery('dir')));
        $direction = reset($direction);
        if (!$image->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            if ($direction === 'up' && !$image->moveUp()) {
                $err = ($image->err() === '' ? 'something wrong' : $image->err());
            }
            if ($direction === 'down' && !$image->moveDown()) {
                $err = ($image->err() === '' ? 'something wrong' : $image->err());
            }
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;

        if ($image->loaded()) {
            $record = $this->_app->dice()->create('\Eshop\Eshop', array($image->parentId()));
            $ret->record = $this->_recordToJson($record);
        }

        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle update image
     */
    public function handleUpdate()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $image = $this->_app->dice()->create('\Eshop\Products\GalleryImage', array($request->getPost('id', 'int')));
        if (!$image->loaded()) {
            $err = 'Record not found';
        }
        $image->description($request->getPost('description'));
        if (!$image->update()) {
            $err = ($image->err() === '' ? 'something wrong' : $image->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;

        $this->_app->response()->setOutputJson($ret);
    }

}