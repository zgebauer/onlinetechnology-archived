<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use GstLib\Vars;
use GstLib\Date;
use GstLib\Html;
use GstLib\Template;
use Ergo\Pagination;
use Ergo\Response;
use Eshop\Module;
use Eshop\Orders\Order;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerOrders {

	private $serviceFees;

	/**
	 * constructor
	 * @param \Ergo\ApplicationInterface $application current application
	 */
	public function __construct(\Ergo\ApplicationInterface $application, \Eshop\Orders\Fees\Service $serviceFees) {
		$this->_app = $application;
		$this->serviceFees = $serviceFees;
	}

	/**
	 * render page with list of records
	 */
	public function renderList() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->rememberRedirectLogin();
			$this->_app->response()->redirect($this->_app->baseUrl(true));
			return;
		}

		$this->_app->response()->pageTitle('Objednávky | [%LG_SITENAME%]');
		$tpl = new Template($this->_app->getTemplatePath('list_orders.htm', Module::NAME, true), true);
		$options = array(10 => '10', 20 => '20', 50 => '50', 100 => '100');
		$tpl->replace('INPVAL_ROWS', Html::getOptions($options), 'raw');
		$tpl->replace('INPVAL_STATUS', Html::getOptions(Order::statuses()), 'raw');
		$this->_app->response()->pageContent($tpl->get());
	}

	/**
	 * render json with list of records
	 */
	public function handleList() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$dataLayer = $this->_app->dice()->create('\Eshop\Orders\DataOrder', array($this->_app));
		/** @var $request \Ergo\Request */
		$request = $this->_app->request();

		$pos = max(array(1, $request->getQuery(Pagination::POS_VARIABLE, 'int')));
		$order = array_intersect(array('id'), array($request->getQuery('order')));
		$order = (isset($order[0]) ? $order[0] : 'id');
		$desc = $request->getQuery('desc', 'bool');
		$rowsPage = max(array(10, $request->getQuery('rows_page', 'int')));

		$filterId = $request->getQuery('f_id', 'int');
		$filterStart = $request->getQuery('f_start', 'date');
		$filterEnd = $request->getQuery('f_end', 'date');

		$criteria = array();
		if ($filterId > 0) {
			$criteria[] = array('id', $filterId);
		}
		if ($filterStart != Date::EMPTY_DATE) {
			$criteria[] = array('date', $filterStart, '>=');
		}
		if ($filterEnd != Date::EMPTY_DATE) {
			$criteria[] = array('date', $filterEnd . ' 23:59:59', '<=');
		}

		$items = array();
		foreach ($dataLayer->getRecords($pos - 1, $rowsPage, $order, $desc, $criteria) as $record) {
			$items[] = array(
				'id' => $record->instanceId(),
				'date' => $record->date()->format('j.n.Y'),
				'name' => trim($record->lastName() . ' ' . $record->firstName()),
				'status' => $this->_app->translate($record->statusName()),
				'status_id' => $record->status(),
				'company' => $record->company()
			);
		}

		$ret = new \stdClass();
		$ret->items = $items;
		$ret->total = $dataLayer->count($criteria);

		$pager = new Pagination($ret->total, $pos, $rowsPage);
		$ret->pages = $pager->getPages();

		$tmp = $pager->getRecords();
		$ret->pos = $tmp[0];
		$ret->posEnd = $tmp[1];
		$this->_app->response()->setOutputJson($ret);
	}

	/**
	 * render json with single record
	 */
	public function handleEdit() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$record = $this->_app->dice()
			->create('\Eshop\Orders\Order', array($this->_app->request()->getQuery('id', 'int')));

//		$payments = $this->_app->dice()->create('\Eshop\Orders\PaymentsList');
//		$deliverers = $this->_app->dice()->create('\Eshop\Orders\DeliverersList');

		$ret = new \stdClass();
		if ($record->loaded()) {
			$ret->record = (object)array(
					'id' => $record->instanceId(),
					'date' => $record->date()->format('Y-m-d\TH:i:s\Z'),
					'first_name' => $record->firstName(),
					'last_name' => $record->lastName(),
					'address' => $record->address(),
					'city' => $record->city(),
					'post_code' => $record->postCode(),
					'delivery_address' => $record->deliveryAddress(),
					'delivery_city' => $record->deliveryCity(),
					'delivery_post_code' => $record->deliveryPostCode(),
					'email' => $record->email(),
					'phone' => $record->phone(),
					'note' => $record->note(),
					'company' => $record->company(),
					'ic' => $record->ic(),
					'dic' => $record->dic(),
					//'payment' => $payments->getPayment($record->paymentId())->name(),
					'payment' => $this->_app->translate($this->serviceFees->getPaymentList()->getTypeName($record->paymentId())),
					'price_payment' => Vars::formatCurrency($record->pricePayment(), 'CZK', 2),
					'price_payment_eur' => Vars::formatCurrency($record->getPricePaymentEur(), 'EUR', 2),
					//'deliverer' => $deliverers->getDeliverer($record->delivererId())->name(),
					'deliverer' => $this->_app->translate($this->serviceFees->getDeliveryList()->getTypeName($record->delivererId())),
					'price_delivery' => Vars::formatCurrency($record->priceDelivery(), 'CZK', 2),
					'price_delivery_eur' => Vars::formatCurrency($record->getPriceDeliveryEUR(), 'EUR', 2),
					'parcel' => $record->parcelNumber(),
					'status' => $record->status(),
					'subtotal' => Vars::formatCurrency($record->subtotal(), 'CZK', 2),
					'subtotal_eur' => Vars::formatCurrency($record->getSubtotalEur(), 'EUR', 2),
					'subtotal_vat' => Vars::formatCurrency($record->subtotalVat(), 'CZK', 2),
					'subtotal_eur_vat' => Vars::formatCurrency($record->getSubtotalEurVat(), 'EUR', 2),
					'grandtotal' => Vars::formatCurrency($record->grandtotal(), 'CZK', 2),
					'grandtotal_eur' => Vars::formatCurrency($record->getGrandtotalEur(), 'EUR', 2),
					'grandtotal_vat' => Vars::formatCurrency($record->grandtotalVat(), 'CZK', 2),
					'grandtotal_eur_vat' => Vars::formatCurrency($record->getGrandtotalEurVat(), 'EUR', 2),
					'date_sent' => is_null($record->dateSent()) ? '' : $record->dateSent()->format('Y-m-d\TH:i:s\Z')
			);
			$ret->record->items = array();
			foreach ($record->getItems() as $item) {
				$ret->record->items[] = array(
					'name' => $item->product(),
					'quantity' => Vars::formatNumberHuman($item->quantity(), 0),
					'price_unit' => Vars::formatCurrency($item->priceUnit(), 'CZK', 2),
					'price_unit_vat' => Vars::formatCurrency($item->priceUnitVat(), 'CZK', 2),
					'vat' => Vars::formatNumberHuman($item->vat(), 0),
					'price_row' => Vars::formatCurrency($item->priceRow(), 'CZK', 2),
					'price_row_vat' => Vars::formatCurrency($item->priceRowVat(), 'CZK', 2),
				);
			}
		}
		$ret->status = ($record->loaded() ? 'OK' : 'ERR');
		$ret->msg = ($record->loaded() ? '' : 'Record not found');
		$this->_app->response()->setOutputJson($ret);
	}

	/**
	 * handle save record
	 */
	public function handleSave() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$err = '';
		/** @var $request \Ergo\Request */
		$request = $this->_app->request();

		$record = $this->_app->dice()->create('\Eshop\Orders\Order', array($request->getPost('id', 'int')));
		if (!$record->loaded()) {
			$this->_app->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
			return;
		}

		$record->parcelNumber($request->getPost('parcel'));
		$record->status($request->getPost('status', 'int'));

		if (!$record->saveAdmin()) {
			$err = ($record->err() === '' ? 'something wrong' : $record->err());
		}

		$ret = new \stdClass();
		$ret->status = ($err === '' ? 'OK' : 'ERR');
		$ret->msg = $this->_app->translate($err);
		$ret->id = $record->instanceId();
		$this->_app->response()->setOutputJson($ret);
	}

	/**
	 * handle "ordered items sent" request
	 */
	public function handleSaveSent() {
		if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
			$this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
			return;
		}

		$err = '';
		/** @var $request \Ergo\Request */
		$request = $this->_app->request();

		$record = $this->_app->dice()->create('\Eshop\Orders\Order', array($request->getPost('id', 'int')));
		if (!$record->loaded()) {
			$this->_app->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
			return;
		}

		if (!is_null($record->dateSent())) {
			$err = 'Zásilka už byla odeslána';
		}

		$record->parcelNumber($request->getPost('parcel'));
		if ($err === '' && $record->parcelNumber() === '') {
			$err = 'Vyplňte číslo zásilky';
		}

		if ($err === '' && !$record->saveSent()) {
			$err = ($record->err() === '' ? 'something wrong' : $record->err());
		}

		$ret = new \stdClass();
		$ret->status = ($err === '' ? 'OK' : 'ERR');
		$ret->msg = $this->_app->translate($err);
		$ret->id = $record->instanceId();
		$ret->dateSent = is_null($record->dateSent()) ? '' : $record->dateSent()->format('Y-m-d\TH:i:s\Z');
		$this->_app->response()->setOutputJson($ret);
	}

}
