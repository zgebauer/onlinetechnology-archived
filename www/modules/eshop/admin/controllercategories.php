<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use GstLib\Template;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerCategories
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render page with list of records
     */
    public function renderList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->rememberRedirectLogin();
            $this->_app->response()->redirect($this->_app->baseUrl(true));
            return;
        }

        $this->_app->response()->pageTitle('Kategorie zboží | [%LG_SITENAME%]');
        $tpl = new Template($this->_app->getTemplatePath('list_categories.htm', Module::NAME, true), true);
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * render json with list of records
     */
    public function handleList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');

        $ret = new \stdClass();
        $ret->items = $categoriesList->listCategories();
        //$ret->total = $dataLayer->count($criteria);
      //  $ret->total = 2;

//        $pager = new Pagination($ret->total, $pos, $rowsPage);
//        $ret->pages = $pager->getPages();
 //       $ret->pages = array();
//
//        $tmp = $pager->getRecords();
//        $ret->pos = $tmp[0];
 //       $ret->pos = 1;
//        $ret->posEnd = $tmp[1];
        //$ret->posEnd = 2;
        $this->_app->response()->setOutputJson($ret);
    }


    /**
     * render json with single record
     */
    public function handleEdit()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $record = $this->_app->dice()
            ->create('\Eshop\Products\Category', array($this->_app->request()->getQuery('id', 'int')));

        $ret = new \stdClass();
        if ($record->loaded()) {
            $ret->record = (object) array(
                'id' => $record->instanceId(),
                'name' => $record->name(),
                'name_en' => $record->getNameEn(),
                'seo' => $record->seo(),
                'publish' => $record->published(),
                'parent_id' => $record->parentId(),
                'meta_desc' => $record->metaDescription(),
                'meta_keys' => $record->metaKeywords(),
                'text' => $record->text(),
                'text_en' => $record->getTextEn()
            );
        }

        $ret->status = ($record->loaded() ? 'OK' : 'ERR');
        $ret->msg = ($record->loaded() ? '' : 'Record not found');
        $this->_app->response()->setOutputJson($ret);
    }

   /**
     * handle save record
     */
    public function handleSave()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Products\Category', array($request->getPost('id', 'int')));

        $record->name($request->getPost('name'));
        $record->setNameEn($request->getPost('name_en'));
        $record->seo($request->getPost('seo'));
        $record->text($request->getPost('text', 'html'));
        $record->setTextEn($request->getPost('text_en', 'html'));
        $record->published($request->getPost('publish', 'bool'));
        $record->parentId($request->getPost('parent', 'int'));
        $record->metaDescription($request->getPost('meta_desc'));
        $record->metaKeywords($request->getPost('meta_keys'));
        if (!$record->save()) {
            $err = ($record->err() === '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete record
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $record = $this->_app->dice()->create('\Eshop\Products\Category', array($request->getQuery('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $err = ($record->delete() ? '' : $record->err());
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle move item. Expects
     * #_GET['id'] - record id
     * #_GET['direction'] - 'up'|'down'
     */
    public function handleMove()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $direction = $this->_app->request()->getQuery('direction');
        $record = $this->_app->dice()->create('\Eshop\Products\Category', array($request->getQuery('id', 'int')));

        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $ret = $this->_app->dice()->create('\Eshop\Products\DataCategory', array($this->_app))
                ->moveItem($record, $direction);
            $err = $ret ? '' : 'something wrong';
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

}