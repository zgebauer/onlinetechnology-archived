'use strict';

Ergo.Eshop = Ergo.Eshop || {};
Ergo.Eshop.Products = Ergo.Eshop.Products || {};

Ergo.Eshop.Products.Admin = function() {
    var nodeList = $('#productsList');
    var nodeDialog = $('#dialog');
    var editRecord;
    var nodeMsg;
    var editorInstances;
    var selectedTab = 0;

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/eshop/ajax-listproducts';
    recordsList.itemTemplate = '<tr><td>%name%</td><td data-item-publish="%id%" class="clickable">%publish%</td>'
        + '<td data-item-edit="%id%" class="clickable edit">Upravit</td><td data-item-del="%id%" class="clickable del">Smazat</td></tr>';
    recordsList.orderBy = 'id';
    recordsList.orderDesc = 1;
    recordsList.callbackLoaded = function(list, data) {
        pagination.setPages(list.pages);
        positions.update(list.pages);
    };
    recordsList.load();

    var pagination = new Ergo.RecordsList.Pagination($('[data-pos]'), recordsList);
    var positions = new Ergo.RecordsList.Positions(recordsList, $('[data-pos-start]'), $('[data-pos-end]'), $('[data-pos-total]'));
    var rowsPage = new Ergo.RecordsList.RowsPage($('.pagination [data-rows-page]'), recordsList);
    var formFilter = new Ergo.RecordsList.Filter($('.frmFilter'), recordsList);

    var renderEditForm = function() {

        nodeDialog.html($('#detailEdit').html());
        nodeMsg = nodeDialog.find('.msg');

        if (editRecord === undefined) {
            nodeDialog.find('.isNew').removeClass('hidden');
            nodeDialog.find('.isNotNew').addClass('hidden');
        } else {
            nodeDialog.find('.isNew').addClass('hidden');
            nodeDialog.find('.isNotNew').removeClass('hidden');
            nodeDialog.find('input[name="id"]').val(editRecord.id);
            nodeDialog.find('input[name="name"]').val(editRecord.name);
            nodeDialog.find('input[name="name_en"]').val(editRecord.name_en);
            nodeDialog.find('input[name="seo"]').val(editRecord.seo);
            nodeDialog.find('input[name="publish"]').prop('checked', editRecord.published);
            nodeDialog.find('textarea[name="text"]').val(editRecord.text);
            nodeDialog.find('textarea[name="text_en"]').val(editRecord.text_en);
            nodeDialog.find('input[name="meta_desc"]').val(editRecord.meta_desc);
            nodeDialog.find('input[name="meta_keys"]').val(editRecord.meta_keys);
            nodeDialog.find('input[name="publish"]').prop('checked', editRecord.publish);
            nodeDialog.find('input[name="bestseller"]').prop('checked', editRecord.bestseller);
            nodeDialog.find('input[name="featured"]').prop('checked', editRecord.featured);
            nodeDialog.find('input[name="price"]').val(editRecord.price);
            nodeDialog.find('input[name="price_vat"]').val(editRecord.price_vat);
            nodeDialog.find('input[name="vat"]').val(editRecord.vat);
			nodeDialog.find('input[name="price_eur"]').val(editRecord.price_eur);
            nodeDialog.find('input[name="price_eur_vat"]').val(editRecord.price_eur_vat);
            nodeDialog.find('select[name="category_id"]').val(editRecord.category_id);
            for (var i=0; i<editRecord.icons.length; i++) {
                nodeDialog.find('input[name="icons\[\]"][value="'+editRecord.icons[i]+'"]').prop('checked', true);
            }
            nodeDialog.find('img[data-main-image]').prop('src', editRecord.imageUrl);
            nodeDialog.find('textarea[name="popis_modulu"]').val(editRecord.popis_modulu);
            nodeDialog.find('textarea[name="popis_modulu_en"]').val(editRecord.popis_modulu_en);
            nodeDialog.find('textarea[name="zapojeni"]').val(editRecord.zapojeni);
            nodeDialog.find('textarea[name="zapojeni_en"]').val(editRecord.zapojeni_en);
            nodeDialog.find('textarea[name="dokumentace"]').val(editRecord.dokumentace);
            nodeDialog.find('textarea[name="dokumentace_en"]').val(editRecord.dokumentace_en);
            nodeDialog.find('textarea[name="firmware"]').val(editRecord.firmware);
            nodeDialog.find('textarea[name="firmware_en"]').val(editRecord.firmware_en);
            nodeDialog.find('textarea[name="programy"]').val(editRecord.programy);
            nodeDialog.find('textarea[name="programy_en"]').val(editRecord.programy_en);
            nodeDialog.find('input[name="stock_info"]').val(editRecord.stock_info);
            nodeDialog.find('input[name="stock_info_en"]').val(editRecord.stock_info_en);
            renderEditFormImage();
            renderEditFormGallery();
            renderEditFormVariants();
            renderEditFormCross();
			initForm3d();
			renderEditForm3d();
        }

        nodeDialog.find('input[id], textarea[id]').each(function() {
            $(this).prop('id', 'edit_'+$(this).prop('id'));
        });
        nodeDialog.find('label[for]').each(function() {
            $(this).prop('for', 'edit_'+$(this).prop('for'));
        });

        nodeDialog.on('mouseover', '.ui-dialog-buttonset button', function() {
            $(this).focus(); // grab focus from tinymce
        });

        var editorNames = ['text', 'text_en', 'popis_modulu', 'popis_modulu_en', 'zapojeni', 'zapojeni_en',
			'dokumentace', 'dokumentace_en', 'firmware', 'firmware_en', 'programy', 'programy_en'];
        editorInstances = [];
        for (var i=0; i<editorNames.length; i++) {

            var editorId = 'editor'+Math.random(); // tinymce requires unique id on textarea
            nodeDialog.find('textarea[name="'+editorNames[i]+'"]').prop('id', editorId);

            editorInstances[i] = new tinymce.Editor(editorId, {
                language : 'cs',
                plugins: "autolink charmap code contextmenu hr image link lists paste table textcolor",
                toolbar: "undo redo | styleselect bullist numlist | link image | charmap forecolor | preview",
                contextmenu: "link image inserttable | cell row column deletetable",
                paste_as_text: true,
                image_advtab: true,
                entity_encoding: 'raw',
                relative_urls: false,
                remove_script_host: false,
                height : (editorNames[i] === 'text' ? 400 : 100),
                file_browser_callback: function(field_name, url, type, win) {
                    tinyMCE.activeEditor.windowManager.open({
                        file : ERGO_ROOT_URL+'other/filebrowser/browser.htm',
                        title : 'Gst File Browser',
                        width : 600,
                        height : 500,
                        resizable : "yes",
                        inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
                        close_previous : "yes"
                    }, {
                        window : win,
                        input : field_name
                    });
                }
            }, tinymce.EditorManager);
            editorInstances[i].render();
        }

        nodeDialog.find('.tabs').tabs({
            active: selectedTab,
            activate: function(event, ui ) {
                selectedTab = ui.newTab.index();
            }
        });

        nodeDialog.find('.dropgalleryimage').dropUpload({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-uploadproductgalleryimage',
            // POST variables
            fileMeta: function() {
                return { id: editRecord.id };
            },
            fileParamName: 'img',
            fileSizeMax: 2*1024*1024,
            allowedfiletypes: ['image/jpeg','image/gif','image/png'],
            onDropFileError: function(file, message) {
                handleResponseUploadGalleryImage( { msg: message } );
            },
            onFileStarted: function(file) {
                nodeMsg.html('Nahrávání souboru '+file.name);
            },
            onQueueCompleted: function(xhr) {
                handleResponseUploadGalleryImage($.parseJSON(xhr.responseText));
            }
        });

        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Uložit kopii", click: function() { copyItem(); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');
    };

    var saveItem = function(closeAfterSave) {
        for (var i=0; i<editorInstances.length; i++) {
            editorInstances[i].save();
        }

        var postdata = nodeDialog.find('form').serialize();
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-saveproduct',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            nodeDialog.find('.msg').html('');
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            } else {
                editItem(data.id);
            }
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };
    var copyItem = function() {
        var postdata = { id: editRecord.id };
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-copyproduct',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseCopyItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseCopyItem( { msg: errorThrown } );
        });
    };
    var handleResponseCopyItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            nodeDialog.find('.msg').html('');
            recordsList.load();
            nodeDialog.dialog('close');
            editItem(data.id);
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var addItem = function() {
        editRecord = undefined;
        renderEditForm();
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-editproduct?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            editRecord = data.record;
            renderEditForm();
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var confirmDeleteItem = function(id, name) {
        nodeDialog.html('<p>Opravdu chcete vymazat '+name+'?</p>').dialog({
            buttons: [
                { text: "Ok", click: function() { deleteItem(id); } },
                { text: "Storno", click: function() { nodeDialog.dialog('close'); } }
            ],
            width: '40%'
            }).dialog('open');
    };

    var deleteItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-deleteproduct?id='+id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDeleteItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDeleteItem( { msg: errorThrown } );
        });
    };
    var handleResponseDeleteItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
            nodeDialog.dialog('close');
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var publishItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-publishproduct?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponsePublishItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponsePublishItem( { msg: errorThrown } );
        });
    };
    var handleResponsePublishItem = function(data) {
        if (data.status === 'OK') {
            $('[data-item-publish="'+data.id+'"]').html(data.publish);
        }
    };

    var renderEditFormImage = function() {
        if (editRecord !== undefined) {
            if (editRecord.imageUrl === '') {
                nodeDialog.find('[data-deleteimage]').addClass('hidden');
            } else {
                nodeDialog.find('[data-deleteimage]').removeClass('hidden');
                nodeDialog.on('mousedown', '[data-deleteimage]', function(event) {
                    event.preventDefault();
                    //var id = $(this).closest('form').find('input[name="id"]').val();
                    nodeMsg.html('Probíhá zpracování...');
                    $.ajax({
                        url: ERGO_ROOT_URL+'admin/eshop/ajax-deleteproductimage?id='+editRecord.id,
                        dataType: 'json'
                    }).done(function(data, textStatus, jqXHR) {
                        handleResponseDeleteImage(data);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        handleResponseDeleteImage( { msg: errorThrown } );
                    });
                });
            }

            nodeDialog.find('.dropimage').dropUpload({
                url: ERGO_ROOT_URL+'admin/eshop/ajax-uploadproductimage',
                // POST variables
                fileMeta: function() {
                    return { id: editRecord.id };
                },
                fileParamName: 'img',
                fileSizeMax: 2*1024*1024,
                allowedfiletypes: ['image/jpeg','image/gif','image/png'],
                onDropFileError: function(file, message) {
                    handleResponseUploadImage( { msg: message } );
                },
                onFileStarted: function(file) {
                    nodeMsg.html('Nahrávání souboru '+file.name);
                },
                onQueueCompleted: function(xhr) {
                    handleResponseUploadImage($.parseJSON(xhr.responseText));
                }
            });
        }
    };
    var handleResponseUploadImage = function(data) {
        if (data.status === 'OK') {
            editRecord.imageUrl = data.imageUrl;
            nodeMsg.html('');
            renderEditForm();
        } else {
            nodeMsg.html(data.msg);
        }
    };
    var handleResponseDeleteImage = function(data) {
        if (data.status === 'OK') {
            editRecord.imageUrl = '';
            nodeMsg.html('');
            renderEditForm();
        } else {
            nodeMsg.html(data.msg);
        }
    };

    var renderEditFormGallery = function() {
        var nodeGallery = nodeDialog.find('ul.gal');
        var htmlImages = '';
        for (var i=0; i < editRecord.images.length; i++)  {
            htmlImages += '<li data-image-id="'+editRecord.images[i].id+'"><img src="'+editRecord.images[i].url +'" alt="" />'
                + '<textarea>'+editRecord.images[i].description+'</textarea> <button type="button">Uložit</button>'
                + '<span data-image-delete="'+editRecord.images[i].id+'" class="clickable ui-icon ui-icon-circle-close" title="Smazat obrázek"></span>'
                + '<span data-image-move="'+editRecord.images[i].id+'" data-dir="up" class="clickable ui-icon ui-icon-circle-triangle-w" title="Přesunout dopředu"></span>'
                + '<span data-image-move="'+editRecord.images[i].id+'" data-dir="down" class="clickable ui-icon ui-icon-circle-triangle-e" title="Přesunout dozadu"></span></li>';
        }
        nodeGallery.html(htmlImages);
        nodeGallery.find('li:first [data-dir="up"]').remove();
        nodeGallery.find('li:last [data-dir="down"]').remove();

        nodeGallery.on('mousedown', 'button', function () {
            var postdata = {
                id: $(this).closest('[data-image-id]').data('image-id'),
                description: $(this).closest('[data-image-id]').find('textarea').val()
            };
            $.ajax({
                method: 'POST',
                url: ERGO_ROOT_URL + 'admin/eshop/ajax-updateproductgalleryimage',
                data: postdata
            }).done(function(data, textStatus, jqXHR) {
                handleResponseUpdateGalleryImage(data);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                handleResponseUpdateGalleryImage( { msg: errorThrown } );
           });
       });
    };

    nodeDialog.on('mousedown', '[data-image-delete]', function(event) {
        event.preventDefault();
        nodeMsg.html('Probíhá zpracování...');
        var id = $(this).data('image-delete');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-deleteproductgalleryimage?id='+id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDeleteGalleryImage(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDeleteGalleryImage( { msg: errorThrown } );
        });
        return false;
    });

    var handleResponseUploadGalleryImage = function(data) {
        if (data.status === 'OK') {
            editRecord.images = data.images;
            nodeMsg.html('');
            renderEditFormGallery();
        } else {
            nodeMsg.html(data.msg);
        }
    };
    var handleResponseDeleteGalleryImage = function(data) {
        if (data.status === 'OK') {
            editRecord.images = data.images;
            nodeMsg.html('');
            renderEditFormGallery();
        } else {
            nodeMsg.html(data.msg);
        }
    };
    var handleResponseUpdateGalleryImage = function(data) {
        if (data.status === 'OK') {
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    var renderEditFormVariants = function() {
        var nodeVariants = nodeDialog.find('ul.variants');
        var html = '';
        for (var i=0; i < editRecord.variants.length; i++)  {
            html += '<li data-variant-id="'+editRecord.variants[i].id+'">'+editRecord.variants[i].name
                + '<span data-variant-remove="'+editRecord.variants[i].id+'" class="clickable ui-icon ui-icon-circle-close" title="Odebrat variantu"></span></li>';
        }
        nodeVariants.html(html);
    };

    nodeDialog.on('keyup', 'input[name="variant-search"]', function() {
        var name = $.trim($(this).val());
        if (name === '') {
            return;
        }
        nodeMsg.html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-suggestproductvariant?name='+encodeURIComponent(name),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSuggestVariant(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSuggestVariant( { msg: errorThrown } );
        });
    });

    var handleResponseSuggestVariant = function(data) {
        if (data.status === 'OK') {
            var nodeVariants = nodeDialog.find('ul.suggestvariants');
            var html = '';
            for (var i=0; i < data.items.length; i++)  {
                html += '<li>'+data.items[i].name
                    + '<span data-variant-add="'+data.items[i].id+'" class="clickable ui-icon ui-icon-circle-plus" title="Přidat variantu"></span></li>';
            }
            nodeVariants.html(html);
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeDialog.on('mousedown', '[data-variant-add]', function() {
        nodeMsg.html('Probíhá zpracování...');
        var productId = $(this).closest('form').find('input[name="id"]').val();
        var variantId = $(this).data('variant-add');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-addproductvariant?id='+productId+'&add='+variantId,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseAddRemoveVariant(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseAddRemoveVariant( { msg: errorThrown } );
        });
    });

    var handleResponseAddRemoveVariant = function(data) {
        if (data.status === 'OK') {
            editRecord.variants = data.items;
            renderEditFormVariants();
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeDialog.on('mousedown', '[data-variant-remove]', function() {
        nodeMsg.html('Probíhá zpracování...');
        var productId = $(this).closest('form').find('input[name="id"]').val();
        var variantId = $(this).data('variant-remove');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-removeproductvariant?id='+productId+'&remove='+variantId,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseAddRemoveVariant(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseAddRemoveVariant( { msg: errorThrown } );
        });
    });


    var renderEditFormCross = function() {
        var html = '';
        for (var i=0; i < editRecord.cross.length; i++)  {
            html += '<li data-cross-id="'+editRecord.cross[i].id+'">'+editRecord.cross[i].name
                + '<span data-cross-remove="'+editRecord.cross[i].id+'" class="clickable ui-icon ui-icon-circle-close" title="Odebrat související produkt"></span></li>';
        }
        nodeDialog.find('ul.cross').html(html);
    };

    nodeDialog.on('keyup', 'input[name="cross-search"]', function() {
        var name = $.trim($(this).val());
        if (name === '') {
            return;
        }
        nodeMsg.html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-suggestproductcross?name='+encodeURIComponent(name),
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSuggestCross(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSuggestCross( { msg: errorThrown } );
        });
    });

    var handleResponseSuggestCross = function(data) {
        if (data.status === 'OK') {
            var html = '';
            for (var i=0; i < data.items.length; i++)  {
                html += '<li>'+data.items[i].name
                    + '<span data-cross-add="'+data.items[i].id+'" class="clickable ui-icon ui-icon-circle-plus" title="Přidat cross-sellu"></span></li>';
            }
            nodeDialog.find('ul.suggestcross').html(html);
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeDialog.on('mousedown', '[data-cross-add]', function() {
        nodeMsg.html('Probíhá zpracování...');
        var productId = $(this).closest('form').find('input[name="id"]').val();
        var crossId = $(this).data('cross-add');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-addproductcross?id='+productId+'&add='+crossId,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseAddRemoveCross(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseAddRemoveCross( { msg: errorThrown } );
        });
    });

    var handleResponseAddRemoveCross = function(data) {
        if (data.status === 'OK') {
            editRecord.cross = data.items;
            renderEditFormCross();
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeDialog.on('mousedown', '[data-cross-remove]', function() {
        nodeMsg.html('Probíhá zpracování...');
        var productId = $(this).closest('form').find('input[name="id"]').val();
        var crossId = $(this).data('cross-remove');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-removeproductcross?id='+productId+'&remove='+crossId,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseAddRemoveCross(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseAddRemoveCross( { msg: errorThrown } );
        });
    });

    var renderEditForm3d = function() {
        var $buttonDelete3d = nodeDialog.find('[data-action="delete3d"]');
        if (editRecord !== undefined) {
            nodeDialog.find('#3dmodel').prop('src', editRecord.url_model3d);
            if (editRecord.url_model3d === '') {
                $buttonDelete3d.addClass('hidden');
            } else {
                $buttonDelete3d.removeClass('hidden');
            }
        }
    }

    var initForm3d = function() {
        nodeDialog.find('[data-drop3d]').dropUpload({
            url: ERGO_ROOT_URL + 'admin/eshop/ajax-uploadproduct3d',
            fileMeta: function () {
                return {id: editRecord.id};
            },
            fileParamName: 'file',
            fileSizeMax: 3 * 1024 * 1024,
            allowedfiletypes: ['text/html'],
            onDropFileError: function (file, message) {
                handleResponseUpload3d({msg: message});
            },
            onFileStarted: function (file) {
                nodeMsg.html('Nahrávání souboru ' + file.name);
            },
            onQueueCompleted: function (xhr) {
                handleResponseUpload3d($.parseJSON(xhr.responseText));
            }
        });
    }

    var handleResponseUpload3d = function(data) {
        if (data.status === 'OK') {
            editRecord.url_model3d = data.url_model3d;
            renderEditForm3d();
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };
    var handleResponseDelete3d = function(data) {
        if (data.status === 'OK') {
            editRecord.url_model3d = '';
            renderEditForm3d();
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeDialog.on('mousedown', '[data-action="delete3d"]', function(event) {
        event.preventDefault();
        nodeMsg.html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-deleteproduct3d?id='+editRecord.id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDelete3d(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDelete3d( { msg: errorThrown } );
        });
    });

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); })
        .on('mousedown', '[data-item-add]', function() { addItem(); })
        .on('mousedown', '[data-item-del]', function() {
            confirmDeleteItem($(this).data('item-del'), $(this).closest('tr').find('td').eq(0).html());
        }).on('mousedown', '[data-item-publish]', function() { publishItem($(this).data('item-publish')); });

    // Prevent jQuery UI dialog from blocking focusin
    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });

};
