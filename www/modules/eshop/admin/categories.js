'use strict';

Ergo.Eshop = Ergo.Eshop || {};
Ergo.Eshop.Categories = Ergo.Eshop.Categories || {};

Ergo.Eshop.Categories.Admin = function() {
    var nodeList = $('#categoriesList');
    var nodeDialog = $('#dialog');
    var editorInstance, editorInstance2;

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/eshop/ajax-listcategories';
    recordsList.itemTemplate = '<tr data-parentid="%parent_id%"><td class="level%level%">%name%</td>'
      + '<td data-item-up="%id%" class="clickable">Nahoru</td><td data-item-down="%id%" class="clickable">Dolů</td>'
      + '<td data-item-edit="%id%" class="clickable edit">Upravit</td><td data-item-del="%id%" class="clickable del">Smazat</td></tr>';
    recordsList.callbackLoaded = function(list, data) {
        var parentIds = [];
        nodeList.find('tr[data-parentid]').each(function() {
            var id = $(this).data('parentid');
            if ($.inArray(id, parentIds) === -1) {
                nodeList.find('tr[data-parentid="'+id+'"]:first td[data-item-up]').empty();
                nodeList.find('tr[data-parentid="'+id+'"]:last td[data-item-down]').empty();
                parentIds.push(id);
            }
        });

        var optionsCategories = '<option value="0">(hlavní kategorie)</option>';
        for (var i = 0; i < data.items.length; i++) {
            optionsCategories += '<option value="'+data.items[i].id+'" class="level'+data.items[i].level+'">'+data.items[i].name+'</option>';
        }
        $('#detailEdit select[name="parent"]').html(optionsCategories);


    };
    recordsList.load();


    var renderEditForm = function(record) {
        var html = $('#detailEdit').html();
        nodeDialog.html(html);

        if (record !== undefined) {
            nodeDialog.find('input[name="id"]').val(record.id);
            nodeDialog.find('input[name="name"]').val(record.name);
            nodeDialog.find('input[name="name_en"]').val(record.name_en);
            nodeDialog.find('input[name="seo"]').val(record.seo);
            nodeDialog.find('input[name="publish"]').prop('checked', record.publish);
            nodeDialog.find('select[name="parent"]').val(record.parent_id);
            nodeDialog.find('textarea[name="text"]').val(record.text);
            nodeDialog.find('textarea[name="text_en"]').val(record.text_en);
            nodeDialog.find('input[name="meta_desc"]').val(record.meta_desc);
            nodeDialog.find('input[name="meta_keys"]').val(record.meta_keys);
        }
        var editorId = 'editor'+Math.random(); // tinymce requires unique id on textarea
        nodeDialog.find('textarea[name="text"]').prop('id', editorId);
        var editor2Id = 'editor'+Math.random(); // tinymce requires unique id on textarea
        nodeDialog.find('textarea[name="text_en"]').prop('id', editor2Id);

        editorInstance = new tinymce.Editor(editorId, {
            language : 'cs',
            plugins: "autolink charmap code contextmenu hr image link lists paste table textcolor",
            toolbar: "undo redo | styleselect bullist numlist | link image | charmap forecolor | preview",
            contextmenu: "link image inserttable | cell row column deletetable",
            paste_as_text: true,
            image_advtab: true,
            entity_encoding: 'raw',
            relative_urls: false,
            remove_script_host: false,
            height : 600,
            file_browser_callback: function(field_name, url, type, win) {
                tinyMCE.activeEditor.windowManager.open({
                    file : ERGO_ROOT_URL+'other/filebrowser/browser.htm',
                    title : 'Gst File Browser',
                    width : 600,
                    height : 500,
                    resizable : "yes",
                    inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
                    close_previous : "yes"
                }, {
                    window : win,
                    input : field_name
                });
            }
        }, tinymce.EditorManager);
        editorInstance.render();

        editorInstance2 = new tinymce.Editor(editor2Id, {
            language : 'cs',
            plugins: "autolink charmap code contextmenu hr image link lists paste table textcolor",
            toolbar: "undo redo | styleselect bullist numlist | link image | charmap forecolor | preview",
            contextmenu: "link image inserttable | cell row column deletetable",
            paste_as_text: true,
            image_advtab: true,
            entity_encoding: 'raw',
            relative_urls: false,
            remove_script_host: false,
            height : 600,
            file_browser_callback: function(field_name, url, type, win) {
                tinyMCE.activeEditor.windowManager.open({
                    file : ERGO_ROOT_URL+'other/filebrowser/browser.htm',
                    title : 'Gst File Browser',
                    width : 600,
                    height : 500,
                    resizable : "yes",
                    inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
                    close_previous : "yes"
                }, {
                    window : win,
                    input : field_name
                });
            }
        }, tinymce.EditorManager);
        editorInstance2.render();

        nodeDialog.find('.tabs').tabs();

        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');
    };

    var saveItem = function(closeAfterSave) {
        editorInstance.save();
        editorInstance2.save();
        var postdata = nodeDialog.find('form').serialize();
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-savecategory',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            }
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-editcategory?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            renderEditForm(data.record);
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var confirmDeleteItem = function(id, name) {
        nodeDialog.html('<p>Opravdu chcete vymazat '+name+'?</p>').dialog({
            buttons: [
                { text: "Ok", click: function() { deleteItem(id) } },
                { text: "Storno", click: function() { nodeDialog.dialog('close') } }
            ],
            width: '40%'
            }).dialog('open');
    };

    var deleteItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-deletecategory?id='+id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDeleteItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDeleteItem( { msg: errorThrown } );
        });
    };
    var handleResponseDeleteItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
            nodeDialog.dialog('close');
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var moveItem = function(id, direction) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-movecategory?id='+id+'&direction='+direction,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseMoveItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseMoveItem( { msg: errorThrown } );
        });
    };
    var handleResponseMoveItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
//        } else {
//            nodeDialog.html('<p>'+data.msg+'</p>')
//                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); })
        .on('mousedown', '[data-item-add]', function() { renderEditForm(); })
        .on('mousedown', '[data-item-del]', function() {
            confirmDeleteItem($(this).data('item-del'), $(this).closest('tr').find('td').eq(0).html());
        }).on('mousedown', '[data-item-up]', function() { moveItem($(this).data('item-up'), 'up'); })
        .on('mousedown', '[data-item-down]', function() { moveItem($(this).data('item-down'), 'down'); });

    // Prevent jQuery UI dialog from blocking focusin
    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });

};
