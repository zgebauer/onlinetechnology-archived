'use strict';

Ergo.Eshop = Ergo.Eshop || {};
Ergo.Eshop.Customers = Ergo.Eshop.Customers || {};

Ergo.Eshop.Customers.Admin = function() {
    var nodeList = $('#customersList');
    var nodeDialog = $('#dialog');
    var editRecord;
    var nodeMsg;

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/eshop/ajax-listcustomers';
    recordsList.itemTemplate = '<tr><td>%last_name%</td><td>%first_name%</td><td>%company%</td><td>%address%</td>'
      + '<td data-item-edit="%id%" class="clickable edit">Upravit</td></tr>';
    recordsList.orderBy = 'last_name';
    recordsList.orderDesc = 1;
    recordsList.callbackLoaded = function(list, data) {
        pagination.setPages(list.pages);
        positions.update(list.pages);
    };
    recordsList.load();

    var pagination = new Ergo.RecordsList.Pagination($('[data-pos]'), recordsList);
    var positions = new Ergo.RecordsList.Positions(recordsList, $('[data-pos-start]'), $('[data-pos-end]'), $('[data-pos-total]'));
    var rowsPage = new Ergo.RecordsList.RowsPage($('.pagination [data-rows-page]'), recordsList);
    var formFilter = new Ergo.RecordsList.Filter($('.frmFilter'), recordsList);

    var renderEditForm = function() {
        var html = $('#detailEdit').html();
        html = html.replace(/%id%/g, editRecord.id);
        html = html.replace(/%first_name%/g, editRecord.first_name);
        html = html.replace(/%last_name%/g, editRecord.last_name);
        html = html.replace(/%email%/g, editRecord.email);
        html = html.replace(/%phone%/g, editRecord.phone);
        html = html.replace(/%address%/g, editRecord.address);
        html = html.replace(/%city%/g, editRecord.city);
        html = html.replace(/%post_code%/g, editRecord.post_code);
        html = html.replace(/%company%/g, editRecord.company);
        html = html.replace(/%ic%/g, editRecord.ic);
        html = html.replace(/%dic%/g, editRecord.dic);
        nodeDialog.html(html);

        nodeMsg = nodeDialog.find('.msg');

        nodeDialog.find('input[name="id"]').val(editRecord.id);
        renderEditFormDiscounts();

        nodeDialog.find('input[id], textarea[id]').each(function() {
            $(this).prop('id', 'edit_'+$(this).prop('id'));
        });
        nodeDialog.find('label[for]').each(function() {
            $(this).prop('for', 'edit_'+$(this).prop('for'));
        });

        nodeDialog.dialog({
            buttons : [
//                { text: "Uložit", click: function() { saveItem(); } },
//                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');
    };

//    var saveItem = function(closeAfterSave) {
//        var postdata = nodeDialog.find('form').serialize();
//        nodeDialog.find('.msg').html('Probíhá zpracování...');
//        $.ajax({
//            url: ERGO_ROOT_URL+'admin/eshop/ajax-savecustomer',
//            method:'post',
//            data: postdata,
//            dataType: 'json'
//        }).done(function(data, textStatus, jqXHR) {
//            handleResponseSaveItem(data, closeAfterSave);
//        }).fail(function(jqXHR, textStatus, errorThrown) {
//            handleResponseSaveItem( { msg: errorThrown } );
//        });
//    };
//    var handleResponseSaveItem = function(data, closeDialog) {
//        if (data.status === 'OK') {
//            nodeDialog.find('.msg').html('');
//            recordsList.load();
//            if (closeDialog) {
//                nodeDialog.dialog('close');
//            } else {
//                editItem(data.id);
//            }
//        } else {
//            nodeDialog.find('.msg').html(data.msg);
//        }
//    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-editcustomer?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            editRecord = data.record;
            renderEditForm();
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); });

    var renderEditFormDiscounts = function() {
        var nodeDiscounts = nodeDialog.find('table.discounts tbody');
        var html = '';
        for (var i=0; i < editRecord.discounts.length; i++)  {
            html += '<tr><td>'+editRecord.discounts[i].name+'</td><td>'+editRecord.discounts[i].discount+'</td>'
                + '<td><button data-discount-remove="'+editRecord.discounts[i].category_id
                    +'" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text">smazat</span></button></td></tr>';
        }
        nodeDiscounts.html(html);
    };


   nodeDialog.on('mousedown', '[data-discount-add]', function() {
        nodeMsg.html('Probíhá zpracování...');
        var customerId = $(this).closest('form').find('input[name="id"]').val();
        var categoryId = $(this).closest('form').find('select[name="category_id"]').val();
        var discount = $(this).closest('form').find('input[name="discount"]').val();
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-addcustomerdiscount?id='+customerId+'&add='+categoryId+'&discount='+discount,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseAddRemoveDiscount(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseAddRemoveDiscount( { msg: errorThrown } );
        });
    });

    var handleResponseAddRemoveDiscount = function(data) {
        if (data.status === 'OK') {
            editRecord.discounts = data.items;
            renderEditFormDiscounts();
            nodeMsg.html('');
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeDialog.on('mousedown', '[data-discount-remove]', function() {
        nodeMsg.html('Probíhá zpracování...');
        var customerId = $(this).closest('form').find('input[name="id"]').val();
        var categoryId = $(this).data('discount-remove');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-removecustomerdiscount?id='+customerId+'&remove='+categoryId,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseAddRemoveDiscount(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseAddRemoveDiscount( { msg: errorThrown } );
        });
    });

};