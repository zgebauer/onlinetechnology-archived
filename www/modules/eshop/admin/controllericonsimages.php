<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerIconsImages
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * handle upload image
     */
    public function handleUpload()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Products\Icon', array($request->getPost('id', 'int')));
        if (!$record->loaded()) {
            $err = 'Record not found';
        }

        if ($err === '') {
            $image = $this->_app->dice()->create('\Eshop\Products\IconImage', array($record->instanceId()));
            if (!$image->save('img')) {
                $err = ($image->err() === '' ? 'something wrong' : $image->err());
            }
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        if ($ret->status === 'OK') {
            $ret->imageUrl = $record->getMainImage()->getAttachmentUrl(\Eshop\Products\IconImage::IMG_ORIG);
        }
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete image
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        if ($err === '') {
            $image = $this->_app->dice()->create('\Eshop\Products\IconImage', array($request->getQuery('id', 'int')));
            if (!$image->delete()) {
                $err = ($image->err() === '' ? 'something wrong' : $image->err());
            }
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

}