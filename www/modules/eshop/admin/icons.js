'use strict';

Ergo.Eshop = Ergo.Eshop || {};
Ergo.Eshop.Icons = Ergo.Eshop.Icons || {};

Ergo.Eshop.Icons.Admin = function() {
    var nodeList = $('#iconsList');
    var nodeDialog = $('#dialog');
    var editRecord;
    var nodeMsg;
    var editorInstances;

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/eshop/ajax-listicons';
    recordsList.itemTemplate = '<tr><td>%name%</td>'
      + '<td data-item-edit="%id%" class="clickable edit">Upravit</td><td data-item-del="%id%" class="clickable del">Smazat</td></tr>';
    recordsList.orderBy = 'id';
    recordsList.orderDesc = 1;
    recordsList.callbackLoaded = function(list, data) {
        pagination.setPages(list.pages);
        positions.update(list.pages);
    };
    recordsList.load();

    var pagination = new Ergo.RecordsList.Pagination($('[data-pos]'), recordsList);
    var positions = new Ergo.RecordsList.Positions(recordsList, $('[data-pos-start]'), $('[data-pos-end]'), $('[data-pos-total]'));
    var rowsPage = new Ergo.RecordsList.RowsPage($('.pagination [data-rows-page]'), recordsList);
    var formFilter = new Ergo.RecordsList.Filter($('.frmFilter'), recordsList);

    var renderEditForm = function() {

        nodeDialog.html($('#detailEdit').html());
        nodeMsg = nodeDialog.find('.msg');

        if (editRecord === undefined) {
            nodeDialog.find('.isNew').removeClass('hidden');
            nodeDialog.find('.isNotNew').addClass('hidden');
        } else {
            nodeDialog.find('.isNew').addClass('hidden');
            nodeDialog.find('.isNotNew').removeClass('hidden');
            nodeDialog.find('input[name="id"]').val(editRecord.id);
            nodeDialog.find('input[name="name"]').val(editRecord.name);
            nodeDialog.find('input[name="name_en"]').val(editRecord.name_en);
            nodeDialog.find('textarea[name="text"]').val(editRecord.text);
            nodeDialog.find('textarea[name="text_en"]').val(editRecord.text_en);
            nodeDialog.find('img[data-main-image]').prop('src', editRecord.imageUrl);
            renderEditFormImage();
        }

        nodeDialog.find('input[id], textarea[id]').each(function() {
            $(this).prop('id', 'edit_'+$(this).prop('id'));
        });
        nodeDialog.find('label[for]').each(function() {
            $(this).prop('for', 'edit_'+$(this).prop('for'));
        });

        nodeDialog.on('mouseover', '.ui-dialog-buttonset button', function() {
            $(this).focus(); // grab focus from tinymce
        });

        var editorNames = ['text', 'text_en'];
        editorInstances = [];
        for (var i=0; i<editorNames.length; i++) {

            var editorId = 'editor'+Math.random(); // tinymce requires unique id on textarea
            nodeDialog.find('textarea[name="'+editorNames[i]+'"]').prop('id', editorId);

            editorInstances[i] = new tinymce.Editor(editorId, {
                language : 'cs',
                plugins: "autolink charmap code contextmenu hr image link lists paste table textcolor",
                toolbar: "undo redo | styleselect bullist numlist | link image | charmap forecolor | preview",
                contextmenu: "link image inserttable | cell row column deletetable",
                paste_as_text: true,
                image_advtab: true,
                entity_encoding: 'raw',
                relative_urls: false,
                remove_script_host: false,
                height : (editorNames[i] === 'text' ? 400 : 100),
                file_browser_callback: function(field_name, url, type, win) {
                    tinyMCE.activeEditor.windowManager.open({
                        file : ERGO_ROOT_URL+'other/filebrowser/browser.htm',
                        title : 'Gst File Browser',
                        width : 600,
                        height : 500,
                        resizable : "yes",
                        inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
                        close_previous : "yes"
                    }, {
                        window : win,
                        input : field_name
                    });
                }
            }, tinymce.EditorManager);
            editorInstances[i].render();
        }

        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');
    };

    var saveItem = function(closeAfterSave) {
        for (var i=0; i<editorInstances.length; i++) {
            editorInstances[i].save();
        }

        var postdata = nodeDialog.find('form').serialize();
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-saveicon',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            nodeDialog.find('.msg').html('');
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            } else {
                editItem(data.id);
            }
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var addItem = function() {
        editRecord = undefined;
        renderEditForm();
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-editicon?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            editRecord = data.record;
            renderEditForm();
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var confirmDeleteItem = function(id, name) {
        nodeDialog.html('<p>Opravdu chcete vymazat '+name+'?</p>').dialog({
            buttons: [
                { text: "Ok", click: function() { deleteItem(id); } },
                { text: "Storno", click: function() { nodeDialog.dialog('close'); } }
            ],
            width: '40%'
            }).dialog('open');
    };

    var deleteItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/eshop/ajax-deleteicon?id='+id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDeleteItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDeleteItem( { msg: errorThrown } );
        });
    };
    var handleResponseDeleteItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
            nodeDialog.dialog('close');
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    var renderEditFormImage = function() {
        if (editRecord !== undefined) {
            if (editRecord.imageUrl === '') {
                nodeDialog.find('[data-deleteimage]').addClass('hidden');
            } else {
                nodeDialog.find('[data-deleteimage]').removeClass('hidden');
                nodeDialog.on('mousedown', '[data-deleteimage]', function(event) {
                    event.preventDefault();
                    //var id = $(this).closest('form').find('input[name="id"]').val();
                    nodeMsg.html('Probíhá zpracování...');
                    $.ajax({
                        url: ERGO_ROOT_URL+'admin/eshop/ajax-deleteiconimage?id='+editRecord.id,
                        dataType: 'json'
                    }).done(function(data, textStatus, jqXHR) {
                        handleResponseDeleteImage(data);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        handleResponseDeleteImage( { msg: errorThrown } );
                    });
                });
            }

            nodeDialog.find('.dropimage').dropUpload({
                url: ERGO_ROOT_URL+'admin/eshop/ajax-uploadiconimage',
                // POST variables
                fileMeta: function() {
                    return { id: editRecord.id };
                },
                fileParamName: 'img',
                fileSizeMax: 2*1024*1024,
                allowedfiletypes: ['image/jpeg','image/gif','image/png'],
                onDropFileError: function(file, message) {
                    handleResponseUploadImage( { msg: message } );
//                var translations = {
//                    FILE_INVALID_TYPE : 'Soubor není platný obrázek',
//                    FILE_OVERSIZE : 'Soubor je příliš velký'
//                };
//                $('#dialogConfirm').html('<p>'+file.name+': '+translations[message]+'</p>').dialog({
//                    buttons: [
//                        { text: "OK", click: function() { $(this).dialog('close'); } }
//                    ]}).dialog('open');
                },
                onFileStarted: function(file) {
                    nodeMsg.html('Nahrávání souboru '+file.name);
                },
                onQueueCompleted: function(xhr) {
                    handleResponseUploadImage($.parseJSON(xhr.responseText));
                }
            });
        }
    };
    var handleResponseUploadImage = function(data) {
        if (data.status === 'OK') {
            editRecord.imageUrl = data.imageUrl;
            //nodeDialog.find('img[data-main-image]').prop('src', data.imageUrl);
            nodeMsg.html('');
            renderEditForm();
        } else {
            nodeMsg.html(data.msg);
        }
    };
    var handleResponseDeleteImage = function(data) {
        if (data.status === 'OK') {
            editRecord.imageUrl = '';
            //var image = nodeDialog.find('img[data-main-image]');
            //image.prop('src', '').replaceWith(image.clone());
            nodeMsg.html('');
            renderEditForm();
        } else {
            nodeMsg.html(data.msg);
        }
    };

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); })
        .on('mousedown', '[data-item-add]', function() { addItem(); })
        .on('mousedown', '[data-item-del]', function() {
            confirmDeleteItem($(this).data('item-del'), $(this).closest('tr').find('td').eq(0).html());
        });


    // Prevent jQuery UI dialog from blocking focusin
    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });

};