<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Admin;

use GstLib\Vars;
use GstLib\Html;
use GstLib\Template;
use Ergo\Pagination;
use Ergo\Response;
use Eshop\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerCustomers
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render page with list of records
     */
    public function renderList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->rememberRedirectLogin();
            $this->_app->response()->redirect($this->_app->baseUrl(true));
            return;
        }

        $this->_app->response()->pageTitle('Zákazníci | [%LG_SITENAME%]');
        $tpl = new Template($this->_app->getTemplatePath('list_customers.htm', Module::NAME, true), true);
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $tpl->replace('INPVAL_ROWS', Html::getOptions($options), 'raw');
        $categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');
        $tpl->replace('INPVAL_CATEGORIES', Html::getOptions($categoriesList->optionsCategory()), 'raw');
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * render json with list of records
     */
    public function handleList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $dataLayer = $this->_app->dice()->create('\Eshop\Customers\DataCustomer', array($this->_app));
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $pos = max(array(1, $request->getQuery(Pagination::POS_VARIABLE, 'int')));
        $order = array_intersect(array('last_name'), array($request->getQuery('order')));
        $order = (isset($order[0]) ? $order[0] : 'id');
        $desc = $request->getQuery('desc', 'bool');
        $rowsPage = max(array(10, $request->getQuery('rows_page', 'int')));

        $filterName = $request->getQuery('f_name');

        $criteria = array();
        if ($filterName !== '') {
            $criteria[] = array('last_name', $filterName, 'like');
        }

        $items = array();
        foreach ($dataLayer->getRecords($pos-1, $rowsPage, $order, $desc, $criteria) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'last_name' => $record->lastName(),
                'first_name' => $record->firstName(),
                'company' => $record->company(),
                'address' => $record->address() . ' ' . $record->city() . ' ' . $record->postCode(),

            );
        }

        $ret = new \stdClass();
        $ret->items = $items;
        $ret->total = $dataLayer->count($criteria);

        $pager = new Pagination($ret->total, $pos, $rowsPage);
        $ret->pages = $pager->getPages();

        $tmp = $pager->getRecords();
        $ret->pos = $tmp[0];
        $ret->posEnd = $tmp[1];
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render json with single record
     */
    public function handleEdit()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $record = $this->_app->dice()
            ->create('\Eshop\Customers\Customer', array($this->_app->request()->getQuery('id', 'int')));

        $ret = new \stdClass();
        if ($record->loaded()) {
            $ret->record = (object) array(
                'id' => $record->instanceId(),
                'first_name' => $record->firstName(),
                'last_name' => $record->lastName(),
                'address' => $record->address(),
                'city' => $record->city(),
                'post_code' => $record->postCode(),
                'email' => $record->email(),
                'phone' => $record->phone(),
                'company' => $record->company(),
                'ic' => $record->ic(),
                'dic' => $record->dic()
            );
            $ret->record->discounts = array();
            foreach ($record->getDiscounts() as $item) {
                $ret->record->discounts[] = array(
                    'category_id' => $item->categoryId(),
                    'name' => $item->categoryName(),
                    'discount' => Vars::formatNumberHuman($item->discountPercent(), 1)
                );
            }
        }
        $ret->status = ($record->loaded() ? 'OK' : 'ERR');
        $ret->msg = ($record->loaded() ? '' : 'Record not found');
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle save record
     */
    public function handleSave()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Customers\Customer', array($request->getPost('id', 'int')));
        if (!$record->loaded()) {
            $this->_app->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
            return;
        }

        $record->parcelNumber($request->getPost('parcel'));
        $record->status($request->getPost('status', 'int'));

        if (!$record->saveAdmin()) {
            $err = ($record->err() === '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $this->_app->translate($err);
        $ret->id = $record->instanceId();
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * add discount per category and customer. Expects
     * $_GET[id] - customer id
     * $_GET[add] - category id
     * $_GET[discount] - percent of discount
     * @return void
     */
    public function handleAddDiscount()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $customerId = $request->getQuery('id', 'int');
        $categoryId = $request->getQuery('add', 'int');
        $discount = $request->getQuery('discount', 'float');

        $dataLayer = $this->_app->dice()->create('\Eshop\Customers\DataCustomer', array($this->_app));

        $result = $dataLayer->addDiscount($customerId, $categoryId, $discount);

        $ret = new \stdClass();
        $ret->status = $result ? 'OK' : 'ERR';

        $items = array();
        foreach ($dataLayer->getDiscounts($customerId) as $item) {
            $items[] = array(
                'category_id' => $item->categoryId(),
                'name' => $item->categoryName(),
                'discount' => Vars::formatNumberHuman($item->discountPercent(), 1)
            );
        }

        $ret->items = $items;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * remove discount per category and customer. Expects
     * $_GET[id] - customer id
     * $_GET[remove] - category id
     * @return void
     */
    public function handleRemoveDiscount()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        /** @var $request \Ergo\Request */
        $request = $this->_app->request();
        $customerId = $request->getQuery('id', 'int');
        $categoryId = $request->getQuery('remove', 'int');

        $dataLayer = $this->_app->dice()->create('\Eshop\Customers\DataCustomer', array($this->_app));

        $result = $dataLayer->removeDiscount($customerId, $categoryId);

        $ret = new \stdClass();
        $ret->status = $result ? 'OK' : 'ERR';

        $items = array();
        foreach ($dataLayer->getDiscounts($customerId) as $item) {
            $items[] = array(
                'category_id' => $item->categoryId(),
                'name' => $item->categoryName(),
                'discount' => Vars::formatNumberHuman($item->discountPercent(), 1)
            );
        }
        $ret->items = $items;
        $this->_app->response()->setOutputJson($ret);
    }

}