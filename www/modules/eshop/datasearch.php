<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;


/**
 * data tier for module - search
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@centrum.cz>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataSearch
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_products%]';
    private static $_tableNameIndex = '[%core_search_index%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_app = $application;
    }

    /**
     * refresh urls of module in table sitemap
     */
    public function refreshSearchIndex()
    {
        $sql = 'SELECT id,name,seo FROM '.self::$_tableName.' WHERE publish='.$this->_con->boolToSql(true);
        $rows = $this->_con->fetchArrayAll($sql);

        $date =  new \DateTime('now', new \DateTimeZone('UTC'));
        $refresh = $date->format('Y-m-d H:i:s');
        foreach ($rows as $row) {
            $url = $this->_app->getUrl(Module::NAME, 'zbozi/'.$row['seo'], false);

            $itemsInsert = array();
            $itemsInsert['url'] = $this->_con->escape($url);
            $itemsInsert['name'] = $this->_con->escape($row['name']);
            $itemsInsert['text'] = $this->_con->escape('');
            $itemsInsert['module'] = $this->_con->escape(Module::NAME);
            $itemsInsert['last_refresh'] = $this->_con->escape($refresh);
            $itemsUpdate = array();
            $itemsUpdate[] = 'last_refresh='.$this->_con->escape($refresh);
            $sql = 'INSERT INTO '.self::$_tableNameIndex.' ('.implode(',', array_keys($itemsInsert))
                .') VALUES ('.implode(',', $itemsInsert).') ON DUPLICATE KEY UPDATE '.join(',', $itemsUpdate);
            $this->_con->query($sql);
        }
        $sql = 'DELETE FROM '.self::$_tableNameIndex
            .' WHERE module='.$this->_con->escape(Module::NAME).' AND last_refresh<>'.$this->_con->escape($refresh);
        $this->_con->query($sql);
    }

}