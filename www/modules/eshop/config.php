<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

use GstLib\Filesystem\IdFile;
use GstLib\Html;

/**
 * configuration of module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Config
{
	/** @var string last error message */
	protected $_err;

	/** @var \Eshop\DataConfig data layer */
	protected $_dataLayer;

	/**
	 * create instance and fill it with data by given identifier
	 * @param \Eshop\DataConfig $dataLayer
	 */
	public function __construct(DataConfig $dataLayer)
	{
		$this->_err = '';
		$this->_params = array('email_order'=>'',
//            'price_del_cp_balik_do_ruky'=>'', 'free_del_cp_balik_do_ruky'=>'',
//            'price_del_cp_balik_na_postu'=>'', 'free_del_cp_balik_na_postu'=>'',
//            'price_del_cp_balik_doporuceny'=>'', 'free_del_cp_balik_doporuceny'=>'',
//            'price_del_cp_balik_expres'=>'', 'free_del_cp_balik_doporuceny'=>'',
//            'price_delivery_express'=>'', 'free_delivery_express'=>''
		);

		$this->_dataLayer = $dataLayer;

		$this->load();
	}

	/**
	 * returns and sets last error message
	 * @param string $value
	 * @return string
	 */
	public function err($value = null)
	{
		if (!is_null($value)) {
			$this->_err = $value;
		}
		return $this->_err;
	}

	/**
	 * returns value of parameter
	 * @param string $key
	 * @return string
	 */
	public function getParam($key)
	{
		return (isset($this->_params[$key]) ? $this->_params[$key] : '');
	}

	/**
	 * sets value of parameter by key
	 * @param string $key
	 * @param string $value
	 */
	public function setParam($key, $value)
	{
		if (isset($this->_params[$key])) {
			$this->_params[$key] = $value;
		}
	}

	/**
	 * fill instance from data tier
	 * @return bool false on error
	 */
	public function load()
	{
		return $this->_dataLayer->load($this);
	}

	/**
	 * save instance to data tier
	 * @return bool false on error
	 */
	public function save()
	{
		return $this->_dataLayer->save($this);
	}

	/**
	 * returns valid emails for order notification
	 * @staticvar type $ret
	 * @return array
	 */
	public function getValidEmailOrder()
	{
		static $ret;
		if (!isset ($ret)) {
			$ret = array();
			$tmp = explode(',', $this->getParam('email_order'));
			foreach ($tmp as $email) {
				$email = trim($email);
				if (Html::isEmail($email)) {
					$ret[] = $email;
				}
			}
		}
		return $ret;
	}

	/**
	 * returns extensions allowed for images
	 * @return array
	 */
	public function allowedImageExtensions()
	{
		return array('jpg', 'jpeg', 'gif', 'png');
	}

	/**
	 * @return string[]
	 */
	public static function allowedExtensions3d() {
		return ['html', 'htm'];
	}

	/**
	 * returns mime types allowed for images
	 * @return array
	 */
	public function allowedImageMimeTypes()
	{
		return array('image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png');
	}

	/**
	 * @return string[]
	 */
	public static function allowedMime3d() {
		return ['text/html'];
	}

	/**
	 * returns max filesize of uploaded image
	 * @return int
	 */
	public function maxUploadImageFileSize()
	{
		return 1024*1024 * 3;
	}

	/**
	 * returns absolute path to directory for images
	 * @param int $type image type
	 * @return string
	 */
	public function dirImages($type = \Eshop\Products\Image::IMG_ORIG)
	{
		switch ($type){
		case \Eshop\Products\Image::IMG_THUMB:
			return ERGO_DATA_DIR.'eshop/thumb/';
		case \Eshop\Products\Image::IMG_THUMB2:
			return ERGO_DATA_DIR.'eshop/thumb2/';
		}
		return ERGO_DATA_DIR.'eshop/orig/';
	}

	/**
	 * returns absolute path to directory for gallery images
	 * @param int $type image type
	 * @return string
	 */
	public function dirGalleryImages($type = \Eshop\Products\GalleryImage::IMG_ORIG)
	{
		switch ($type){
		case \Eshop\Products\GalleryImage::IMG_THUMB:
			return ERGO_DATA_DIR.'eshop/gallery/thumb/';
		}
		return ERGO_DATA_DIR.'eshop/gallery/orig/';
	}

	/**
	 * returns absolute path to directory for product icons
	 * @return string
	 */
	public function dirIcons()
	{
		return ERGO_DATA_DIR.'eshop/icons/';
	}

	/**
	 * absolute path to base directory for product icons
	 * @return string
	 */
	public function dirModels3d() {
		return ERGO_DATA_DIR . 'eshop/3d/';
	}

	/**
	 * returns width and height of main product image
	 * @param int $type
	 * @return array
	 */
	public function sizeImage($type = \Eshop\Products\Image::IMG_ORIG)
	{
		if ($type === \Eshop\Products\Image::IMG_THUMB) {
			return array(238, 178);
		}
		if ($type === \Eshop\Products\Image::IMG_THUMB2) {
			return array(358, 268);
		}
		return array(490, 302);
	}

	/**
	 * returns width and height of gallery image
	 * @param int $type
	 * @return array
	 */
	public function sizeGalleryImage($type = \Eshop\Products\GalleryImage::IMG_ORIG)
	{
		if ($type === \Eshop\Products\GalleryImage::IMG_THUMB) {
			return array(166, 125);
		}
		return array(600, 600);
	}

	/**
	 * @return int IdFile::FILES_PER_DIR*
	 */
	public function getFilesPerDir() {
		return IdFile::FILES_PER_DIR_1000;
	}
}