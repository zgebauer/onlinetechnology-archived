<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

use Ergo\Response;
use GstLib\Vars;
use GstLib\Html;
use GstLib\Template;

/**
 * controller for authentification
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerCustomer
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * set json with current user
     */
    public function renderUserInfo()
    {
        $ret = new \stdClass();
        $ret->status = 'OK';
        $ret->user = null;
        $user = $this->_app->getVisitor();
        if (!is_null($user)) {
            $ret->user = new \stdClass();
            $ret->user->id = $user->instanceId();
            $ret->user->name = trim($user->firstName().' '.$user->lastName());

            $ret->user->discounts = array();
            foreach ($user->getDiscounts() as $item) {
                $ret->user->discounts[] = array(
                    'category_id' => $item->categoryId(),
                    'discount' => $item->discountPercent(),
                );
            }
        }
        $ret->cart = null;
        $cart = $this->_app->getModule('eshop')->getCart();
        if (!is_null($cart)) {
            $ret->cart = new \stdClass();
            $ret->cart->count = count($cart->getItems());
            $ret->cart->priceVat = Vars::formatNumberHuman($cart->price(true, $this->_app->currentLanguage()), 2);
        }

        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle request from login form
     */
    public function handleLogin()
    {
        $user = $this->_app->getVisitor();
        if (!is_null($user)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }
        $login = $this->_app->request()->getPost('login');
        $password = $this->_app->request()->getPost('pwd');
        $result = $this->_app->dice()->create('\Eshop\Customers\DataCustomer')->login($login, $password);
        $ret = new \stdClass();
        $ret->status = ($result ? 'OK' : 'ERR');
        $ret->msg = ($ret->status === 'OK' ? '' : 'Neplatný-email nebo heslo');
        $ret->user = null;
        $user = $this->_app->getVisitor();
        if (!is_null($user)) {
            $ret->user = new \stdClass();
            $ret->user->id = $user->instanceId();
            $ret->user->name = trim($user->firstName().' '.$user->lastName());

            $ret->user->discounts = array();
            foreach ($user->getDiscounts() as $item) {
                $ret->user->discounts[] = array(
                    'category_id' => $item->categoryId(),
                    'discount' => $item->discountPercent(),
                );
            }

        }
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * logout current user
     */
    public function handleLogout()
    {
        $result = $this->_app->dice()->create('\Eshop\Customers\DataCustomer')->logout();
        $ret = new \stdClass();
        $ret->status = ($result ? 'OK' : 'ERR');
        $this->_app->response()->setOutputJson($ret);
    }

 /**
     * render registration form
     */
    public function renderFormRegister()
    {
        $user = $this->_app->getVisitor();
        if (!is_null($user)) {
            $this->_app->response()->redirect($this->_app->baseUrl());
            return;
        }
        $this->_app->response()->pageTitle('Registrace | [%LG_SITENAME%]');
        $tpl = new Template($this->_app->getTemplatePath('frm_register.htm', Module::NAME), true);
        $tpl->replace('INPVAL_COUNTRY', Html::getOptions($this->_app->countries(), 'CZ'), 'raw');
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * handle submit registration form
     */
    public function handleRegister()
    {
        $user = $this->_app->getVisitor();
        if (!is_null($user)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Customers\Customer');
        $record->email($request->getPost('email'));
        //$record->setPassword($request->getPost('pwd'), $request->getPost('confirm'));
        $record->firstName($request->getPost('first_name'));
        $record->lastName($request->getPost('last_name'));

        $record->phone($request->getPost('phone'));
        $record->address($request->getPost('address'));
        $record->city($request->getPost('city'));
        $record->postCode($request->getPost('post_code'));
        $record->country($request->getPost('country'));
        $record->company($request->getPost('company'));
        $record->ic($request->getPost('ic'));
        $record->dic($request->getPost('dic'));

        if ($record->register($request->getPost('pwd'), $request->getPost('confirm'))) {
            $_SESSION[Module::SESSION_USER_ID] = $record->instanceId();
        } else {
            $err = ($record->err() == '' ? 'something wrong' : $record->err());
        }
        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render page with "edit profile" form
     * @return void
     */
    public function renderFormProfile()
    {
        $user = $this->_app->getVisitor();
        if (is_null($user)) {
            $this->_app->response()->redirect($this->_app->baseUrl());
            return;
        }

        $tpl = new Template($this->_app->getTemplatePath('frm_profile.htm', Module::NAME), true);
        $tpl->replace('TXT_EMAIL', $user->email());
        $tpl->replace('INPVAL_FIRST_NAME', $user->firstName());
        $tpl->replace('INPVAL_LAST_NAME', $user->lastName());
        $tpl->replace('INPVAL_PHONE', $user->phone());
        $tpl->replace('INPVAL_ADDRESS', $user->address());
        $tpl->replace('INPVAL_CITY', $user->city());
        $tpl->replace('INPVAL_POST_CODE', $user->postCode());
        $tpl->replace('INPVAL_COUNTRY', Html::getOptions($this->_app->countries(), $user->country()), 'raw');
        $tpl->replace('INPVAL_COMPANY', $user->company());
        $tpl->replace('INPVAL_IC', $user->ic());
        $tpl->replace('INPVAL_DIC', $user->dic());

        $this->_app->response()->pageTitle('Moje údaje | [%LG_SITENAME%]');
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * handle submit profile form
     * @return void
     */
    public function handleSave()
    {
        $user = $this->_app->getVisitor();
        if (is_null($user)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        $request = $this->_app->request();

        $user->firstName($request->getPost('first_name'));
        $user->lastName($request->getPost('last_name'));
        $user->phone($request->getPost('phone'));
        $user->address($request->getPost('address'));
        $user->city($request->getPost('city'));
        $user->postCode($request->getPost('post_code'));
        $user->country($request->getPost('country'));
        $user->company($request->getPost('company'));
        $user->ic($request->getPost('ic'));
        $user->dic($request->getPost('dic'));

        if (!$user->save()) {
            $err = ($user->err() == '' ? 'something wrong' : $user->err());
        }
        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

   /**
     * handle request from submit change password form
     */
    public function handleChangePassword()
    {
        $user = $this->_app->getVisitor();
        if (is_null($user)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $request = $this->_app->request();
        $ret = new \stdClass();
        $ret->status = ($user->updatePassword($request->getPost('pwd'), $request->getPost('confirm')) ? 'OK' : 'ERR');
        $ret->msg = ($ret->status === 'OK' ? '' : ($user->err() === '' ? 'something wrong' : $user->err()));
        $this->_app->response()->setOutputJson($ret);
    }

  /**
     * handle submit reset password form
     */
    public function handleResetPassword()
    {
        $user = $this->_app->getVisitor();
        if (!is_null($user)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Customers\ResetPassword');
        if (!$record->sendToken($request->getPost('email'))) {
            $err = ($record->err() == '' ? 'something wrong' : $record->err());
        }
        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = ($err === '' ? 'Na adresu '.$request->getPost('email')
            .' byl odeslán e-mail s dalšími pokyny.' : $this->_app->translate($err));
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render new password form
     */
    public function renderFormNewPassword()
    {
        if (!is_null($this->_app->getVisitor())) {
            $this->_app->response()->redirect($this->_app->baseUrl(), Response::HTTP_FORBIDDEN);
            return;
        }
        $token = $this->_app->request()->getQuery('token');
        $this->_meta['title'] = 'Nove heslo | [%LG_SITENAME%]';
        $tpl = new Template($this->_app->getTemplatePath('frm_new_password.htm', Module::NAME), true);
        $tpl->replace('INPVAL_TOKEN', $token);
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * handle submit new password form
     */
    public function handleNewPassword()
    {
        if (!is_null($this->_app->getVisitor())) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Eshop\Customers\ResetPassword');
        if ($record
                ->updatePassword($request->getPost('token'), $request->getPost('pwd'), $request->getPost('confirm'))) {

        } else {
            $err = ($record->err() == '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render page with order in customer profile
     * @return void
     */
    public function renderProfileOrders()
    {
        $customer = $this->_app->getVisitor();
        if (is_null($customer)) {
            $this->_app->response()->redirect($this->_app->baseUrl());
            return;
        }

        $tpl = new Template($this->_app->getTemplatePath('list_orders.htm', Module::NAME), true);

        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('ITEMS');
        $criteria = array(array('customer_id', $customer->instanceId()));
        foreach ($this->_app->dice()->create('\Eshop\Orders\DataOrder')
                ->getRecords(0, null, 'id', true, $criteria) as $record) {
            $tplRow->set($rowTemplate);
            $tplRow->replace('TXT_ITEM', $record->instanceId());
            $record->date()->setTimeZone(new \DateTimeZone(ERGO_TIMEZONE));
            $tplRow->replace('TXT_ITEM_DATE', $record->date()->format('j.n.Y H:i'));
            $tplRow->replace(
				'TXT_ITEM_GRANDTOTAL_VAT',
				Vars::formatCurrency($record->grandTotalVat(),  ($record->getLang() === 'en' ? 'EUR' : 'CZK'), 2, ','),
				'raw'
			);
            $tplRow->replace('TXT_ITEM_STATUS', $this->_app->translate($record->statusName(null, $this->_app->currentLanguage())));
            $cnt .= $tplRow->get();
        }
        unset($tplRow);
        $tpl->replaceSub('ITEMS', $cnt);

        $this->_app->response()->pageContent($tpl->get());
        $this->_app->response()->pageTitle('Moje objednávky | [%LG_SITENAME%]');
    }

    /**
     * render order data. Expects
     * $_GET[id] - order id
     * @return void
     */
    public function handleOrderDetail()
    {
        $user = $this->_app->getVisitor();
        if (is_null($user)) {
            $this->_app->response()->redirect($this->_app->baseUrl(), Response::HTTP_FORBIDDEN);
            return;
        }

        $ret = new \stdClass();
        $ret->status = 'OK';
        $ret->order = null;

        $order = new \Eshop\Orders\Order(
            $this->_app->dice()->create('\Eshop\Orders\DataOrder'),
            $this->_app->request()->getQuery('id', 'int')
        );

        if (!$order->loaded()) {
            $this->_app->response()->setOutputNone(Response::HTTP_BAD_REQUEST);
            return;
        }
        if ($user->instanceId() !== $order->customerId()) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $ret->order = new \stdClass();
        $ret->order->id = $order->instanceId();
        $ret->order->firstName = $order->firstName();
        $ret->order->lastName = $order->lastName();
        $ret->order->company = $order->company();
        $ret->order->phone = $order->phone();
        $ret->order->address = $order->address();
        $ret->order->city = $order->city();
        $ret->order->postCode = $order->postCode();
        $ret->order->country = $this->_app->translate($this->_app->countryName($order->country()));
        $ret->order->ic = $order->ic();
        $ret->order->dic = $order->dic();
        $ret->order->deliveryAddress = $order->deliveryAddress();
        $ret->order->deliveryCity = $order->deliveryCity();
        $ret->order->deliveryPostCode = $order->deliveryPostCode();
        $ret->order->deliveryCountry = $this->_app->translate($this->_app->countryName($order->deliveryCountry()));
        $ret->order->note = $order->note();
        $ret->order->subtotal = $order->subtotal();
        $ret->order->subtotalVat = $order->subtotalVat();
        $ret->order->priceDelivery = $order->priceDelivery();
        $ret->order->pricePayment = $order->pricePayment();
        $ret->order->grandTotal = $order->grandTotal();
        $ret->order->grandTotalVat = $order->grandTotalVat();
        $ret->order->parcel = $order->parcelNumber();

        // items
        $ret->order->items = array();
        foreach ($order->getItems() as $item) {
            $ret->order->items[] = array(
                'product' => $item->product(),
                'quantity' => $item->quantity(),
                'priceUnit' => Vars::formatCurrency($item->priceUnit(), 'CZK', 2, ','),
                'priceRow' => Vars::formatCurrency($item->priceRow(), 'CZK', 2, ',')
            );
        }

        $this->_app->response()->setOutputJson($ret);
    }

}