<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Customers;

use GstLib\Html;
use GstLib\Template;
use Eshop\Module;

/**
 * data tier for module - functions related to "reset password"
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataResetPassword
{
    /** @var \GstLib\Db\DriverMysqli $connection */
    private $_con;
    /** @var \Ergo\ApplicationInterface $application current application */
    private $_app;
    private static $_tableNameUsers = '[%eshop_customers%]';
    private static $_tableNameTokens = '[%eshop_customers_reset_passwords%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_app = $application;
    }

    /**
     * create new token, store it to db and send email with token to user
     * @param string $email
     * @param string $token
     * @return bool false on error
     */
    public function sendToken($email, $token)
    {
        $this->_deleteOldTokens();

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $date->add(new \DateInterval('PT1H'));

        $items = array();
        $items['token'] = $this->_con->escape($token);
        $items['email'] = $this->_con->escape($email);
        $items['valid_to'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
        $sql = 'INSERT INTO '.self::$_tableNameTokens.' ('.
            implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            \Ergo\Application::logException($exception);
            return false;
        }

		$template = ($this->_app->currentLanguage() === 'en' ? 'email_reset_password_en.txt' : 'email_reset_password.txt');
        $tplMail = new Template($this->_app->getTemplatePath($template, Module::NAME));
		$template = ($this->_app->currentLanguage() === 'en' ? 'email_reset_password_en.htm' : 'email_reset_password.htm');
        $tplMailHtml = new Template($this->_app->getTemplatePath($template, Module::NAME));
        $url = Html::queryUrl($this->_app->getUrl(Module::NAME, 'new-password'), array('token' => $token));
        $tplMail->replace('URL_FULL', $url);
        $tplMailHtml->replace('URL_FULL', $url);

        $subject = $tplMail->getSub('SUBJECT');
        $tplMail->delSub('SUBJECT');
        $from = array(ERGO_EMAIL_ROBOT, $this->_app->translate('[%LG_SITENAME%]'));
        $recipients[] = $email;
        return $this->_app->sendMail($from, $recipients, $subject, $tplMail->get(), $tplMailHtml->get());
    }

    /**
     * search for customer id by token
     * @param string $token
     * @return int id of customer ofalse if not found
     */
    public function getUserByToken($token)
    {
        $this->_deleteOldTokens();

        $sql = 'SELECT email FROM '.self::$_tableNameTokens.' WHERE token='.$this->_con->escape($token);
        $token = $this->_con->fetchArray($sql);
        if ($token === false) {
            return null;
        }
        $sql = 'SELECT id FROM '.self::$_tableNameUsers.' WHERE email='.$this->_con->escape($token['email']);
        $row = $this->_con->fetchArray($sql);
        return ($row === false ? null : $this->_app->dice()->create('\Eshop\Customers\Customer', array($row['id'])));
    }

    /**
     * check if email is already registered
     * @param string $email
     * @return bool
     */
    public function emailExists($email)
    {
        $sql = 'SELECT COUNT(*) FROM '.self::$_tableNameUsers.' WHERE email='.$this->_con->escape($email);
        return ($this->_con->count($sql) > 0);
    }

    /**
     * delete token from db
     * @param string $token
     * @return bool false on error
     */
    public function deleteToken($token)
    {
        $sql = 'DELETE FROM '.self::$_tableNameTokens.' WHERE token='.$this->_con->escape($token);
        return $this->_con->query($sql);
    }

    /**
     * delete expired tokens
     * @access private
     * @return bool false on error
     */
    private function _deleteOldTokens()
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $sql = 'DELETE FROM '.self::$_tableNameTokens
            .' WHERE valid_to<'.$this->_con->escape($date->format('Y-m-d H:i:s'));
        $this->_con->query($sql);
    }

}