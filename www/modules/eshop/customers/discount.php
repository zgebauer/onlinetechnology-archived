<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Customers;

/**
 * discount per customer and category
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Discount
{
    /** @var string last error message */
    protected $_err;
    /** @var int identifier of customer */
    protected $_customerId;
    /** @var int identifier of category */
    protected $_categoryId;
    /** @var float discount */
    protected $_discount;
    /** @var string category name */
    protected $_categoryName;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->_err = '';
        $this->_customerId = $this->_categoryId = $this->_discount = 0;
        $this->_categoryName = '';
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets identifier of customer
     * @param int $value
     * @return int
     */
    public function customerId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_customerId = $value;
            }
        }
        return $this->_customerId;
    }

    /**
     * returns and sets identifier of category
     * @param int $value
     * @return int
     */
    public function categoryId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_categoryId = $value;
            }
        }
        return $this->_categoryId;
    }

    /**
     * returns and sets discount percent
     * @param int $value
     * @return int
     */
    public function discountPercent($value = null)
    {
        if (!is_null($value)) {
            $value = (float) $value;
            if ($value > 0 && $value <= 100) {
                $this->_discount = $value;
            }
        }
        return $this->_discount;
    }

    /**
     * returns and sets category name
     * @param string $value
     * @return string
     */
    public function categoryName($value = null)
    {
        if (!is_null($value)) {
            $this->_categoryName = $value;
        }
        return $this->_categoryName;
    }

}