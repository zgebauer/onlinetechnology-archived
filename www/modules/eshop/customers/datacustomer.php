<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Customers;

use Eshop\Module;

/**
 * data tier for module - customers
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataCustomer
{
    /** @var \GstLib\Db\DriverMysqli $connection */
    private $_con;
    /** @var string name of table with customers */
    private static $_tableName = '[%eshop_customers%]';
    private static $_tableNameDiscounts = '[%eshop_customers_discounts%]';
    private static $_tableNameCategories = '[%eshop_categories%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * fill given instance from database
     * @param \Eshop\Customers\Customer $record
     * @return bool false on error
     */
    public function load(Customer $record)
    {
        $sql = 'SELECT email,first_name,last_name,phone,address,city,post_code,country,company,ic,dic
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->email($row['email']);
        $record->firstName($row['first_name']);
        $record->lastName($row['last_name']);
        $record->phone($row['phone']);
        $record->address($row['address']);
        $record->city($row['city']);
        $record->postCode($row['post_code']);
        $record->country($row['country']);
        $record->company($row['company']);
        $record->ic($row['ic']);
        $record->dic($row['dic']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Eshop\Customers\Customer $record
     * @param string $password password of new user
     * @return bool false on error
     */
    public function register(Customer $record, $password)
    {
        $items = array();
        $sql = 'SELECT COUNT(*) FROM '.self::$_tableName.' WHERE email='.$this->_con->escape($record->email());
        if ($this->_con->count($sql) !== 0) {
            $record->err('E-mail už je zaregistrovaný');
            return false;
        }
        if (!$this->_con->beginTrans()) {
            return false;
        }
        $items['email'] = $this->_con->escape($record->email());
        $items['pwd'] = $this->_con->escape(\Eshop\Module::passwordHash($password));
        $items['first_name'] = $this->_con->escape($record->firstName());
        $items['last_name'] = $this->_con->escape($record->lastName());
        $items['phone'] = $this->_con->escape($record->phone());
        $items['address'] = $this->_con->escape($record->address());
        $items['city'] = $this->_con->escape($record->city());
        $items['post_code'] = $this->_con->escape($record->postCode());
        $items['country'] = $this->_con->escape($record->country());
        $items['company'] = $this->_con->escape($record->company());
        $items['ic'] = $this->_con->escape($record->ic());
        $items['dic'] = $this->_con->escape($record->dic());
        $sql = 'INSERT INTO '.self::$_tableName.' ('
            .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $userId = $this->_con->insert($sql);
        if (!$userId) {
            $this->_con->rollback();
            return false;
        }
        if (!$this->_con->commit()) {
            return false;
        }
        $record->instanceId($userId);
        return true;
    }

    /**
     * save given instance to database
     * @param \Eshop\Customers\Customer $record
     * @return bool false on error
     */
    public function save(Customer $record)
    {
        $items = array();
        $userId = $record->instanceId();
        if ($userId === 0) {
            $sql = 'SELECT COUNT(*) FROM '.self::$_tableName.' WHERE email='.$this->_con->escape($record->email());
            if ($this->_con->count($sql) !== 0) {
                $record->err('E-mail už je zaregistrovaný');
                return false;
            }
//            if (!$this->_con->beginTrans()) {
//                return false;
//            }
            $items['email'] = $this->_con->escape($record->email());
            $items['pwd'] = $this->_con->escape(\Eshop\Module::passwordHash($record->password()));
            $items['first_name'] = $this->_con->escape($record->firstName());
            $items['last_name'] = $this->_con->escape($record->lastName());
            $items['phone'] = $this->_con->escape($record->phone());
            $items['address'] = $this->_con->escape($record->address());
            $items['city'] = $this->_con->escape($record->city());
            $items['post_code'] = $this->_con->escape($record->postCode());
            $items['country'] = $this->_con->escape($record->country());
            $items['company'] = $this->_con->escape($record->company());
            $items['ic'] = $this->_con->escape($record->ic());
            $items['dic'] = $this->_con->escape($record->dic());
            $sql = 'INSERT INTO '.self::$_tableName.' ('
                .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            $userId = $this->_con->insert($sql);
            if (!$userId) {
                //$this->_con->rollback();
                return false;
            }
            //  password
//            $sql = 'UPDATE '.self::$_tableName.' SET pwd='.
//                $this->_con->escape(self::_hash($userId, $record->password())).' WHERE id='.$userId;
//            if (!$this->_con->query($sql)) {
//                $this->_con->rollback();
//                return false;
//            }
//            if (!$this->_con->commit()) {
//                return false;
//            }
            $record->instanceId($userId);
        } else {
            $items[] = 'first_name='.$this->_con->escape($record->firstName());
            $items[] = 'last_name='.$this->_con->escape($record->lastName());
            $items[] = 'phone='.$this->_con->escape($record->phone());
            $items[] = 'address='.$this->_con->escape($record->address());
            $items[] = 'city='.$this->_con->escape($record->city());
            $items[] = 'post_code='.$this->_con->escape($record->postCode());
            $items[] = 'country='.$this->_con->escape($record->country());
            $items[] = 'company='.$this->_con->escape($record->company());
            $items[] = 'ic='.$this->_con->escape($record->ic());
            $items[] = 'dic='.$this->_con->escape($record->dic());
            $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
            return $this->_con->query($sql);
        }
        return true;
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,email,first_name,last_name,phone,address,city,post_code,company,ic,dic
            FROM '.self::$_tableName
            .$this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Customer($this);
            $tmp->instanceId($row['id']);
            $tmp->email($row['email']);
            $tmp->firstName($row['first_name']);
            $tmp->lastName($row['last_name']);
            $tmp->phone($row['phone']);
            $tmp->address($row['address']);
            $tmp->city($row['city']);
            $tmp->postCode($row['post_code']);
            $tmp->company($row['company']);
            $tmp->ic($row['ic']);
            $tmp->dic($row['dic']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array())
    {
        $sql = 'SELECT COUNT(*) FROM '.self::$_tableName.$this->_con->where($criteria);
        return $this->_con->count($sql);
    }

    /**
     * update password for given user
     * @param  \Eshop\Customer $record
     * @param  string $password new password
     * @return bool false on error
     */
    public function updatePassword(Customer $record, $password)
    {
        $sql = 'UPDATE '.self::$_tableName.' SET pwd='.
            $this->_con->escape(Module::passwordHash($password)).' WHERE id='.$record->instanceId();
        return $this->_con->query($sql);
    }

    /**
     * atempts login user with email and password. Store succesufuly logged user in session
     *
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function login($login, $password)
    {
        // profile exists in db?
        $sql = 'SELECT id,pwd FROM '.self::$_tableName.' WHERE email='.$this->_con->escape($login);
        $row = $this->_con->fetchArray($sql);
        if (!$row) {
            return false;
        }
        if ($row['pwd'] !== Module::passwordHash($password, $row['pwd'])) {
            return false;
        }

        $_SESSION[Module::SESSION_USER_ID] = $row['id'];
        return true;
    }

    /**
     * logout current customer and destroy session
     * @return bool true
     */
    public function logout()
    {
        unset($_SESSION[Module::SESSION_USER_ID]);
        session_unset();
        @session_destroy();
        return true;
    }

    /**
     * returns array od discounts of customer
     * @param id $customerId
     * @return array of instances \Eshop\Customers\Discount
     */
    public function getDiscounts($customerId)
    {
        $sql = 'SELECT d.category_id,c.name,d.discount
        FROM '.self::$_tableNameDiscounts.' AS d
        INNER JOIN '.self::$_tableNameCategories.' AS c ON d.category_id=c.id'
        .' WHERE customer_id='.$customerId.' '.$this->_con->orderBy('name', FALSE);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Discount();
            $tmp->customerId($customerId);
            $tmp->categoryId($row['category_id']);
            $tmp->categoryName($row['name']);
            $tmp->discountPercent($row['discount']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * add new discount for customer and category
     * @param int $customerId
     * @param int $categoryId
     * @param int $discount
     * @return bool false on error
     */
    public function addDiscount($customerId, $categoryId, $discount)
    {
        if ($categoryId === 0 || $discount <= 0) {
            return true;
        }
        $sql = 'INSERT INTO '.self::$_tableNameDiscounts
            .' (customer_id, category_id, discount) VALUES('.$customerId.','.$categoryId.','.$discount.')
                ON DUPLICATE KEY UPDATE discount='.$discount;
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            \Ergo\Application::logException($exception);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * remove discount for customer an category
     * @param int $customerId
     * @param int $categoryId
     * @return bool false on error
     */
    public function removeDiscount($customerId, $categoryId)
    {
        $sql = 'DELETE FROM '.self::$_tableNameDiscounts
            .' WHERE customer_id='.$customerId.' AND category_id='.$categoryId;
        try {
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            \Ergo\Application::logException($exception);
            return FALSE;
        }
        return TRUE;
    }

}