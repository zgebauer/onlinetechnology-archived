<?php
/**
 * class EshopResetPassword
 *
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Customers;

use GstLib\Vars;
use GstLib\Html;

/**
 * functions realted to reset password of customer
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ResetPassword
{
    /** @var string last error message */
    protected $_err;

    /** @var \Eshop\Customers\DataResetPassword $dataLayer */
    protected $_dataLayer;

    /**
     * constructor
     * @param \Eshop\Customers\DataResetPassword $dataLayer
     */
    public function __construct(DataResetPassword $dataLayer)
    {
        $this->_err = '';
        $this->_dataLayer = $dataLayer;
    }

    /**
     * returns last error message
     * @return string
     */
    public function err()
    {
        return $this->_err;
    }

    /**
     * create and send new token
     * @param string $email
     * @return bool
     */
    public function sendToken($email)
    {
        $this->_err = '';
        if (!Html::isEmail($email)) {
            $this->_err = '[%LG_ERR_INVALID_EMAIL_FORMAT%]';
            return false;
        }
        if (!$this->_dataLayer->emailExists($email)) {
            $this->_err = 'E-mail '.$email.' nenalezen';
            return false;
        }

        $prefix = mt_rand().microtime().getmypid();
        $token = md5(uniqid($prefix, true));
        $ret = $this->_dataLayer->sendToken($email, $token);
        if (!$ret) {
            $this->_err = '[%LG_ERR_SAVE_FAILED%]';
        }
        return $ret;
    }

    /**
     * update password of customer
     * @param string $token
     * @param string $password
     * @param string $confirm
     * @return bool false on error
     */
    public function updatePassword($token, $password, $confirm)
    {
        $this->_err = '';
        $required = array();
        if ($token === '') {
            $required[] = ' Kód';
        }
        if ($password === '') {
            $required[] = '[%LG_PASSWORD%]';
        }
        if ($confirm === '') {
            $required[] = '[%LG_PASSWORD_CONFIRM%]';
        }
        if (isset($required[0])) {
            $this->_err = '[%LG_ERR_MISSING_VALUES%]'.join(', ', $required);
            return false;
        }

        $user = $this->_dataLayer->getUserByToken($token);
        if (is_null($user)) {
            $this->_err = 'Kód není platný. Možná už vypršela jeho platnost nebo neodpovídá kódu v e-mailu';
            return false;
        }
        if (!$user->updatePassword($password, $confirm)) {
            $this->_err = $user->err();
            return false;
        }
        $this->_dataLayer->deleteToken($token);
        return true;
    }

}