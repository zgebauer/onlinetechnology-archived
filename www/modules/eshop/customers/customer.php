<?php
/**
 * @package    Ergo
 * @subpackage Customers
 */

namespace Eshop\Customers;

use GstLib\Vars;
use GstLib\Html;

/**
 * registered customer
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Customers
 */
class Customer
{
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var bool flag "loaded from data tier" */
    protected $_loaded;
    /** @var string username */
    protected $_email;
    /** @var string password */
    protected $_password;
    /** @var string confirm password */
    protected $_confirm;
    /** @var string first name */
    private $_firstName;
    /** @var string last name */
    private $_lastName;
    /** @var string */
    private $_phone;
    /** @var string address */
    private $_address;
    /** @var string city */
    private $_city;
    /** @var string post code */
    private $_postCode;
    /** @var string country code */
    private $_country;
    /** @var string company */
    private $_company;
    /** @var string IC */
    private $_ic;
    /** @var string DIC */
    private $_dic;
    /** @var array discounts */
    private $_discounts;


    /** @var \Eshop\Customers\Data $dataLayer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Eshop\Customers\DataCustomer $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(DataCustomer $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_email = $this->_password = $this->_confirm
            = $this->_firstName = $this->_lastName
            = $this->_phone = $this->_address = $this->_city = $this->_postCode
            = $this->_company = $this->_ic = $this->_dic = '';
        $this->_country = 'CZ';
        $this->_discounts = NULL;

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded()
    {
        return $this->_loaded;
    }

    /**
     * returns and sets email (unique username)
     * @param string $value
     * @return string
     */
    public function email($value = null)
    {
        if (!is_null($value)) {
            $value = substr($value, 0, 70);
            if (Html::isEmail($value)) {
                $this->_email = $value;
            }
        }
        return $this->_email;
    }

    /**
     * returns password
     * @return string
     */
    public function password()
    {
        return $this->_password;
    }

    /**
     * sets password
     * @param string $value
     * @param string $confirm confirmation of password
     */
    public function setPassword($value, $confirm)
    {
        $this->_password = $value;
        $this->_confirm = $confirm;
    }

    /**
     * returns and sets first name
     * @param string $value
     * @return string
     */
    public function firstName($value = null)
    {
        if (!is_null($value)) {
            $this->_firstName = Vars::substr($value, 0, 20);
        }
        return $this->_firstName;
    }

    /**
     * returns and sets last name
     * @param string $value
     * @return string
     */
    public function lastName($value = null)
    {
        if (!is_null($value)) {
            $this->_lastName = Vars::substr($value, 0, 40);
        }
        return $this->_lastName;
    }


    /**
     * returns and sets phone
     * @param string $value
     * @return string
     */
    public function phone($value = null)
    {
        if (!is_null($value)) {
            $this->_phone = Vars::substr($value, 0, 13);
        }
        return $this->_phone;
    }

    /**
     * returns and sets address
     * @param string $value
     * @return string
     */
    public function address($value = null)
    {
        if (!is_null($value)) {
            $this->_address = Vars::substr($value, 0, 50);
        }
        return $this->_address;
    }

    /**
     * returns and sets city
     * @param string $value
     * @return string
     */
    public function city($value = null)
    {
        if (!is_null($value)) {
            $this->_city = Vars::substr($value, 0, 50);
        }
        return $this->_city;
    }

    /**
     * returns and sets post code
     * @param string $value
     * @return string
     */
    public function postCode($value = null)
    {
        if (!is_null($value)) {
            $this->_postCode = Vars::substr($value, 0, 10);
        }
        return $this->_postCode;
    }

    /**
     * returns and sets country code
     * @param string $value
     * @return string
     */
    public function country($value = null)
    {
        if (!is_null($value)) {
            $this->_country = Vars::substr($value, 0, 2);
        }
        return $this->_country;
    }

    /**
     * returns and sets sets company
     * @param string $value
     * @return string
     */
    public function company($value = null)
    {
        if (!is_null($value)) {
            $this->_company = Vars::substr($value, 0, 50);
        }
        return $this->_company;
    }

    /**
     * returns and sets IC
     * @param string $value
     * @return string
     * @SuppressWarnings("short")
     */
    public function ic($value = null)
    {
        if (!is_null($value)) {
            $this->_ic = Vars::substr($value, 0, 8);
        }
        return $this->_ic;
    }

    /**
     * returns and sets DIC
     * @param string $value
     * @return string
     */
    public function dic($value = null)
    {
        if (!is_null($value)) {
            $this->_dic = Vars::substr($value, 0, 12);
        }
        return $this->_dic;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load()
    {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier as new record
     * @param string $password
     * @param string $confirm password
     * @return boolean false on error
     */
    public function register($password, $confirm)
    {
        $this->_err = '';

        $required = array();
        if ($this->_email === '') {
            $required[] = 'e-mail';
        }
        if ($password === '') {
            $required[] = 'heslo';
        }
        if ($confirm === '') {
            $required [] = 'potvzení hesla';
        }
        if ($this->_lastName === '') {
            $required[] = 'příjmení';
        }
        if (isset($required[0])) {
            $this->_err = 'Chybí údaje: '.join(', ', $required);
            return false;
        }
        if (!Html::isEmail($this->email())) {
            $this->_err = '[%LG_ERR_INVALID_EMAIL_FORMAT%]';
            return false;
        }
        if ($password != $confirm) {
            $this->_err = 'Heslo a potvrzení hesla není stejné!';
            return false;
        }

        return $this->_dataLayer->register($this, $password);
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save()
    {
        $this->_err = '';
        if ($this->_id === 0) {
            $required = array();
            if ($this->_email === '') {
                $required[] = 'e-mail';
            }
            if ($this->_password === '') {
                $required[] = 'heslo';
            }
            if ($this->_confirm === '') {
                $required [] = 'potvzení hesla';
            }
        }
        if ($this->_lastName === '') {
            $required[] = 'příjmení';
        }
        if (isset($required[0])) {
            $this->_err = 'Chybí údaje: '.join(', ', $required);
            return false;
        }
        if ($this->_id === 0) {
            if ($this->_password != $this->_confirm) {
                $this->_err = 'Heslo a potvrzení hesla není stejné!';
                return false;
            }
        }
        return $this->_dataLayer->save($this);
    }

    /**
     * save instance to data tier from admin
     * @return bool false on error
     */
    public function saveAdmin()
    {
        $this->_err = '';
        return $this->_dataLayer->saveAdmin($this);
    }

    /**
     * set new password
     * @param string $password
     * @param string $confirm
     * @return bool false on error
     */
    public function updatePassword($password, $confirm)
    {
        $this->_err = '';
        $required = array();
        if ($password === '') {
            $required[] = 'heslo';
        }
        if ($confirm === '') {
            $required[] = 'potvrzení hesla';
        }
        if (isset($required[0])) {
            $this->_err = 'Chybí údaje: '.join(', ', $required);
            return false;
        }
        if ($password !== $confirm) {
            $this->_err = 'Heslo a potvrzení hesla není stejné!';
            return false;
        }
        return $this->_dataLayer->updatePassword($this, $password);
    }

    /**
     * returns discountrs assigned to customer
     * @return array
     */
    public function getDiscounts()
    {
        if ($this->_id === 0) {
            return array();
        }
        if (is_null($this->_discounts)) {
            $this->_discounts = $this->_dataLayer->getDiscounts($this->instanceId());
        }
        return $this->_discounts;
    }
}