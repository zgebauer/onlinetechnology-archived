<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

use GstLib\Vars;
use GstLib\Html;
use GstLib\Template;
use Eshop\Module;
use Eshop\Orders\Fees\PaymentList;
use Eshop\Orders\Fees\DeliveryList;

/**
 * data tier for module - orders
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataOrder {

	/** @var \GstLib\Db\DriverAbstract $connection */
	private $_con;

	/** @var \Ergo\ApplicationInterface $application current application */
	private $_app;
	/** @var \Eshop\Orders\Fees\Service */
	private $serviceFees;

	private static $_tableName = '[%eshop_orders%]';
	private static $_tableNameItems = '[%eshop_orders_items%]';

	/**
	 * constructor
	 * @param \GstLib\Db\DriverMysqli $connection
	 * @param \Ergo\ApplicationInterface $application current application
	 * @param \Eshop\Orders\Fees\Service $serviceFees
	 */
	public function __construct(
		\GstLib\Db\DriverMysqli $connection,
		\Ergo\ApplicationInterface $application,
		\Eshop\Orders\Fees\Service $serviceFees
	) {
		$this->_con = $connection;
		$this->_app = $application;
		$this->serviceFees = $serviceFees;
	}

	/**
	 * fill given instance from database
	 * @param \Eshop\Orders\Order $record
	 * @return bool false on error
	 */
	public function load(Order $record) {
		$sql = 'SELECT date,customer_id,first_name,last_name,company,email,
            phone,address,city,post_code,country,del_address,del_city,del_post_code,del_country,ic,dic,note,
            payment_id,deliverer_id,subtotal,subtotal_vat,`subtotal_eur`,`subtotal_eur_vat`,
            price_delivery,`price_delivery_eur`,price_payment,`price_payment_eur`,parcel_number,
            grandtotal,grandtotal_vat,`grandtotal_eur`,`grandtotal_eur_vat`,status,date_sent,`lang`
            FROM ' . self::$_tableName . ' WHERE id=' . $record->instanceId();
		$row = $this->_con->fetchArray($sql);
		$this->_con->free();
		if (!$row) {
			return false;
		}
		$record->date($row['date']);
		$record->customerId($row['customer_id']);
		$record->email($row['email']);
		$record->firstName($row['first_name']);
		$record->lastName($row['last_name']);
		$record->company($row['company']);
		$record->phone($row['phone']);
		$record->address($row['address']);
		$record->city($row['city']);
		$record->postCode($row['post_code']);
		$record->country($row['country']);
		$record->ic($row['ic']);
		$record->dic($row['dic']);
		$record->deliveryAddress($row['del_address']);
		$record->deliveryCity($row['del_city']);
		$record->deliveryPostCode($row['del_post_code']);
		$record->deliveryCountry($row['del_country']);
		$record->note($row['note']);
		$record->paymentId($row['payment_id']);
		$record->delivererId($row['deliverer_id']);
		$record->subtotal($row['subtotal']);
		$record->subtotalVat($row['subtotal_vat']);
		$record->priceDelivery($row['price_delivery']);
		$record->pricePayment($row['price_payment']);
		$record->grandTotal($row['grandtotal']);
		$record->grandTotalVat($row['grandtotal_vat']);
		$record->status($row['status']);
		$record->parcelNumber($row['parcel_number']);
		$record->dateSent($row['date_sent']);
		$record->setSubtotalEur($row['subtotal_eur'])
			->setSubtotalEurVat($row['subtotal_eur_vat'])
			->setPriceDeliveryEur($row['price_delivery_eur'])
			->setPricePaymentEur($row['price_payment_eur'])
			->setGrandtotalEur($row['grandtotal_eur'])
			->setGrandtotalEurVat($row['grandtotal_eur_vat'])
			->setLang($row['lang']);
		return true;
	}

	/**
	 * save given instance to database
	 * @param \Eshop\Orders\Order $record
	 * @param boolean $registerCustomer flag "registered new customer"
	 * @return bool false on error
	 */
	public function save(Order $order, $registerCustomer = false) {
		if (!$this->_con->beginTrans()) {
			return false;
		}
		$date = new \DateTime('now', new \DateTimeZone('UTC'));
		$items = array();
		$items['date'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
		$items['customer_id'] = $order->customerId();
		$items['first_name'] = $this->_con->escape($order->firstName());
		$items['last_name'] = $this->_con->escape($order->lastName());
		$items['company'] = $this->_con->escape($order->company());
		$items['address'] = $this->_con->escape($order->address());
		$items['city'] = $this->_con->escape($order->city());
		$items['post_code'] = $this->_con->escape($order->postCode());
		$items['country'] = $this->_con->escape($order->country());
		$items['del_address'] = $this->_con->escape($order->deliveryAddress());
		$items['del_city'] = $this->_con->escape($order->deliveryCity());
		$items['del_post_code'] = $this->_con->escape($order->deliveryPostCode());
		$items['del_country'] = $this->_con->escape($order->deliveryCountry());
		$items['ic'] = $this->_con->escape($order->ic());
		$items['dic'] = $this->_con->escape($order->dic());
		$items['phone'] = $this->_con->escape($order->phone());
		$items['email'] = $this->_con->escape($order->email());
		$items['note'] = $this->_con->escape($order->note());
		$items['payment_id'] = $order->paymentId();
		$items['deliverer_id'] = $order->delivererId();
		$items['subtotal'] = $order->calcSubtotal(false);
		$items['subtotal_vat'] = $order->calcSubtotal(true);
		$items['subtotal_eur'] = $order->calcSubtotalEur(false);
		$items['subtotal_eur_vat'] = $order->calcSubtotalEur(true);
		$items['price_delivery'] = $order->calcDeliveryPrice();
		$items['price_delivery_eur'] = $order->calcDeliveryPriceEur();
		$items['price_payment'] = $order->calcPaymentPrice();
		$items['price_payment_eur'] = $order->calcPaymentPriceEur();
		$items['grandtotal'] = $order->calcGrandTotal(false);
		$items['grandtotal_vat'] = $order->calcGrandTotal(true);
		$items['grandtotal_eur'] = $order->calcGrandTotalEur(false);
		$items['grandtotal_eur_vat'] = $order->calcGrandTotalEur(true);
		$items['lang'] = $this->_con->escape($order->getLang());
		$sql = 'INSERT INTO ' . self::$_tableName . ' (' .
			implode(',', array_keys($items)) . ') VALUES (' . implode(',', $items) . ')';
		$orderId = $this->_con->insert($sql);
		if ($orderId === false) {
			$this->_con->rollback();
			return false;
		}
		//items
		foreach ($order->getItems() as $item) {
			$items = array();
			$items['order_id'] = $orderId;
			$items['product_id'] = $item->productId();
			$items['product'] = $this->_con->escape($item->product());
			$items['quantity'] = $item->quantity();
			$items['price_unit'] = $item->priceUnit();
			$items['price_unit_vat'] = $item->priceUnitVat();
			$items['vat'] = $item->vat();
			$items['price_row'] = $item->calcPriceRow(false);
			$items['price_row_vat'] = $item->calcPriceRow(true);
			$items['price_unit_eur'] = $item->getPriceUnitEur();
			$items['price_unit_eur_vat'] = $item->getPriceUnitEurVat();
			$items['price_row_eur'] = $item->calcPriceRowEur(false);
			$items['price_row_eur_vat'] = $item->calcPriceRowEur(true);

			$sql = 'INSERT INTO ' . self::$_tableNameItems . ' (' .
				implode(',', array_keys($items)) . ') VALUES (' . implode(',', $items) . ')';
			if (!$this->_con->query($sql)) {
				$this->_con->rollback();
				return false;
			}
			$item->orderId($orderId);
		}
		$this->_con->commit();
		$order->instanceId($orderId);

		$this->_sendOrderAdmin($order);
		$this->_sendOrderCustomer($order, $registerCustomer);
		return true;
	}

	/**
	 * save given instance to database
	 * @param \Eshop\Customers\Order $record
	 * @return bool false on error
	 */
	public function saveAdmin(Order $record) {
		$items = array();
		$items[] = 'parcel_number=' . $this->_con->escape($record->parcelNumber());
		$items[] = 'status=' . $record->status();
		$sql = 'UPDATE ' . self::$_tableName . ' SET ' . join(',', $items) . ' WHERE id=' . $record->instanceId();
		try {
			$this->_con->query($sql);
		} catch (\GstLib\Db\DatabaseErrorException $exception) {
			$record->err('[%LG_ERR_SAVE_FAILED%]');
			\Ergo\Application::logException($exception);
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * save info when ordered items has been sent
	 * @param \Eshop\Customers\Order $record
	 * @return bool false on error
	 */
	public function saveSent(Order $record) {
		if (!$this->_sendOrderCustomerSent($record)) {
			$record->err('[%LG_ERR_SAVE_FAILED%]');
			return FALSE;
		}

		$date = new \DateTime('now', new \DateTimeZone('UTC'));
		$items = array();
		$items[] = 'date_sent=' . $this->_con->escape($date->format('Y-m-d H:i:s'));
		$items[] = 'parcel_number=' . $this->_con->escape($record->parcelNumber());
		//$items[] = 'status='.$record->status();
		$sql = 'UPDATE ' . self::$_tableName . ' SET ' . join(',', $items) . ' WHERE id=' . $record->instanceId();
		try {
			$this->_con->query($sql);
			$record->dateSent($date->format('Y-m-d H:i:s'));
		} catch (\GstLib\Db\DatabaseErrorException $exception) {
			$record->err('[%LG_ERR_SAVE_FAILED%]');
			\Ergo\Application::logException($exception);
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * send new order to customer by email
	 * @param \Eshop\Customers\Order $order
	 * @return bool false on error
	 */
	private function _sendOrderCustomer(Order $order, $registerCustomer = false) {
		if (!Html::isEmail($order->email())) {
			return true;
		}

		$currency = ($order->getLang() === 'en' ? 'EUR' : 'CZK');
		$template = ($order->getLang() === 'en' ? 'email_order_confirm_en.txt' : 'email_order_confirm.txt');
		$tplMail = new Template($this->_app->getTemplatePath($template, Module::NAME));
		$tplMail->replace('TXT_NUMBER', $order->instanceId());
		$tplMail->replace('TXT_COMPANY', $order->company());
		$tplMail->replace('TXT_FIRST_NAME', $order->firstName());
		$tplMail->replace('TXT_LAST_NAME', $order->lastName());
		$tplMail->replace('TXT_ADDRESS', $order->address());
		$tplMail->replace('TXT_CITY', $order->city());
		$tplMail->replace('TXT_POST_CODE', $order->postCode());
		$tplMail->replace('TXT_COUNTRY', $this->_app->countryName($order->country()));
		$tplMail->replace('TXT_DELIVERY_ADDRESS', $order->deliveryAddress());
		$tplMail->replace('TXT_DELIVERY_CITY', $order->deliveryCity());
		$tplMail->replace('TXT_DELIVERY_POST_CODE', $order->deliveryPostCode());
		$tplMail->replace('TXT_DELIVERY_COUNTRY', $this->_app->countryName($order->deliveryCountry()));
		$tplMail->replace('TXT_EMAIL', $order->email());
		$tplMail->replace('TXT_PHONE', $order->phone());
		$tplMail->replace('TXT_NOTE', $order->note());
		$tplMail->replace('TXT_IC', $order->ic());
		$tplMail->replace('TXT_DIC', $order->dic());

		// items
		$cnt = '';
		$tplRow = new Template();
		$rowTemplate = $tplMail->getSub('ITEMS');
		foreach ($order->getItems() as $item) {
			$tplRow->set($rowTemplate);
			$tplRow->replace('TXT_ITEM', $item->product());
			$tplRow->replace('TXT_ITEM_QUANTITY', Vars::formatNumberHuman($item->quantity(), 0));
			$tplRow->replace('TXT_ITEM_PRICE_ROW', Vars::formatCurrencyPlain($item->calcPriceRowByLang(false), $currency, 2, ','));
			$tplRow->replace('TXT_ITEM_PRICE_ROW_VAT', Vars::formatCurrencyPlain($item->calcPriceRowByLang(true), $currency, 2, ','));
			$tplMail->showSub('ITEM_VAT', $order->isVatPricesRequired());
			$cnt .= $tplRow->get() . "\n";
		}
		$tplMail->replaceSub('ITEMS', trim($cnt));
		$tplMail->replace('TXT_TOTAL', Vars::formatCurrencyPlain($order->calcSubtotalByLang(false), $currency, 2, ','));
		$tplMail->replace('TXT_TOTAL_VAT', Vars::formatCurrencyPlain($order->calcSubtotalByLang(true), $currency, 2, ','));
		$tplMail->replace('TXT_PAYMENT_PRICE', Vars::formatCurrencyPlain($order->calcPaymentPrice(), $currency, 2, ','));
		$tplMail->replace('TXT_DELIVERY_PRICE', Vars::formatCurrencyPlain($order->calcDeliveryPrice(), $currency, 2, ','));
		$tplMail->replace('TXT_GRANDTOTAL', Vars::formatCurrencyPlain($order->calcGrandTotalByLang(false), $currency, 2, ','));
		$tplMail->replace('TXT_GRANDTOTAL_VAT', Vars::formatCurrencyPlain($order->calcGrandTotalByLang(true), $currency, 2, ','));
		$tplMail->showSub('SUBTOTAL_VAT', $order->isVatPricesRequired());
		$tplMail->showSub('GRANDTOTAL_VAT', $order->isVatPricesRequired());

//		$payments = $this->_app->dice()->create('\Eshop\Orders\PaymentsList');
//		$tplMail->replace('TXT_PAYMENT', $payments->getPayment($order->paymentId())->name());
		$tplMail->replace('TXT_PAYMENT', $this->serviceFees->getPaymentList()->getTypeName($order->paymentId()));
		$tplMail->showSub('ACCOUNT', $order->paymentId() === PaymentList::TRANSFER);
//		$deliverers = $this->_app->dice()->create('\Eshop\Orders\DeliverersList');
//		$tplMail->replace('TXT_DELIVERY', $deliverers->getDeliverer($order->delivererId())->name());
		$tplMail->replace('TXT_DELIVERY', $this->serviceFees->getDeliveryList()->getTypeName($order->delivererId()));

		$tplMail->showSub('REGISTER', $registerCustomer);

		$tplMail->set($this->_app->translate($tplMail->get()));
		$subject = $tplMail->getSub('SUBJECT');
		$tplMail->delSub('SUBJECT');
		$body = $tplMail->get();

		$from = array(ERGO_EMAIL_ROBOT, $this->_app->translate('[%LG_SITENAME%]'));
		$recipients[] = array($order->email(), trim($order->firstName() . ' ' . $order->lastName()));
		return $this->_app->sendMail($from, $recipients, $subject, $body);
	}

	/**
	 * send email about shipping
	 * @param \Eshop\Customers\Order $order
	 * @return bool false on error
	 */
	private function _sendOrderCustomerSent(Order $order) {
		if (!Html::isEmail($order->email())) {
			return true;
		}

		$template = ($order->getLang() === 'en' ? 'email_order_sent_en.txt' : 'email_order_sent.txt');
		$tplMail = new Template($this->_app->getTemplatePath($template, Module::NAME));
		$tplMail->replace('TXT_NUMBER', $order->instanceId());
		$tplMail->replace('TXT_PARCEL_NUMBER', $order->parcelNumber());
        $urlParcel = 'https://www.postaonline.cz/trackandtrace/-/zasilka/cislo?parcelNumbers=' . $order->parcelNumber();
        if ($order->delivererId() === DeliveryList::PPL) {
            $urlParcel = 'https://www.ppl.cz/main2.aspx?cls=Package&idSearch=' . $order->parcelNumber();
        }
        $tplMail->replace('URL_PARCEL', $urlParcel, 'raw');

		$tplMail->set($this->_app->translate($tplMail->get()));
		$subject = $tplMail->getSub('SUBJECT');
		$tplMail->delSub('SUBJECT');
		$body = $tplMail->get();

		$from = array(ERGO_EMAIL_ROBOT, $this->_app->translate('[%LG_SITENAME%]'));
		$recipients[] = array($order->email(), trim($order->firstName() . ' ' . $order->lastName()));
		return $this->_app->sendMail($from, $recipients, $subject, $body);
	}

	/**
	 * send new order to admin by email
	 * @param \Eshop\Customers\Order $order
	 * @return bool false on error
	 */
	private function _sendOrderAdmin(Order $order) {
		$emails = $this->_app->dice()->create('\Eshop\Config')->getValidEmailOrder();
		if (!isset($emails[0])) {
			return true;
		}

		$tplMail = new Template($this->_app->getTemplatePath('email_order_admin.txt', Module::NAME));
		$tplMail->replace('TXT_NUMBER', $order->instanceId());
		$tplMail->replace(
			'URL', Html::queryUrl($this->_app->getUrl(Module::NAME, 'orders', true), array('id' => $order->instanceId()))
		);
		$tplMail->set($this->_app->translate($tplMail->get()));
		$subject = $tplMail->getSub('SUBJECT');
		$tplMail->delSub('SUBJECT');

		$subject = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $subject);
		$body = iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $tplMail->get());

		$from = array(ERGO_EMAIL_ROBOT, $this->_app->translate('[%LG_SITENAME%]'));
		foreach ($emails as $email) {
			$recipients[] = $email;
		}
		return $this->_app->sendMail($from, $recipients, $subject, $body, '', array('charset' => 'ISO-8859-2'));
	}

	/**
	 * returns records as array of instances
	 * @param int $start start position
	 * @param int $rows number of returned instances (null = all)
	 * @param string $order name of column for sorting
	 * @param bool $desc true/false sort descending/ascending
	 * @param array $criteria array of parts of WHERE clause
	 * @return array
	 */
	public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array()) {

		$sql = 'SELECT id,date,last_name,first_name,grandtotal_vat,status,company
        FROM ' . self::$_tableName . $this->_con->where($criteria) .
			$this->_con->orderBy($order, $desc) . $this->_con->limit($start, $rows);
		$ret = array();
		foreach ($this->_con->fetchArrayAll($sql) as $row) {
			$tmp = new Order($this);
			$tmp->instanceId($row['id']);
			$tmp->date($row['date']);
			$tmp->lastName($row['last_name']);
			$tmp->firstName($row['first_name']);
			$tmp->grandTotalVat($row['grandtotal_vat']);
			$tmp->status($row['status']);
			$tmp->company($row['company']);
			$ret[] = $tmp;
		}
		return $ret;
	}

	/**
	 * returns number of records matching given criteria
	 * @param array $criteria array of parts of WHERE clause
	 * @return int
	 */
	public function count(array $criteria = array()) {
		$sql = 'SELECT COUNT(*) FROM [%eshop_orders%]' . $this->_con->where($criteria);
		return $this->_con->count($sql);
	}

	/**
	 * returns array with 2 items - id of prevous and next item. If there is no
	 * previous/next record, item in returned array contains null
	 *
	 * @param int $currentRow row number, position in list (0-first item)
	 * @param string $order name of column for orderi
	 * @param bool $desc true = order descending
	 * @param array $criteria array of parts of WHERE clause
	 * @return array
	 */
	public function getPrevNextId($currentRow, $order = '', $desc = false, array $criteria = array()) {
		$allowOrder = array('id');
		$order = in_array($order, $allowOrder) ? $order : reset($allowOrder);

		// previous
		if ($currentRow <= 1) {
			$prevId = null;
		} else {
			$sql = 'SELECT id FROM ' . self::$_tableName . $this->_con->where($criteria) .
				$this->_con->orderBy($order, $desc) . ' LIMIT ' . ($currentRow - 1) . ',1';
			$row = $this->_con->fetchArray($sql);
			$prevId = (isset($row['id']) ? $row['id'] : null);
		}

		// next
		$sql = 'SELECT id FROM ' . self::$_tableName . $this->_con->where($criteria) .
			$this->_con->orderBy($order, $desc) . ' LIMIT ' . ($currentRow + 1) . ',1';
		$row = $this->_con->fetchArray($sql);
		$nextId = (isset($row['id']) ? $row['id'] : null);

		return array($prevId, $nextId);
	}

	/**
	 * returns order items
	 * @param int $start start position
	 * @param int $rows number of returned instances (null = all)
	 * @param string $order name of column for sorting
	 * @param bool $desc true/false sort descending/ascending
	 * @param array $criteria array of instances Gst_Db_Criteria
	 * @return array of instances \Eshop\Customers\OrderItem
	 */
	public function getOrderItems($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array()) {
		$ret = array();
		$sql = 'SELECT id,order_id,product_id,product,quantity,price_unit,price_unit_vat,vat,price_row,price_row_vat
            FROM ' . self::$_tableNameItems . $this->_con->where($criteria) .
			$this->_con->orderBy($order, $desc) . $this->_con->limit($start, $rows);
		foreach ($this->_con->fetchArrayAll($sql) as $row) {
			$tmp = new OrderItem();
			$tmp->instanceId($row['id']);
			$tmp->orderId($row['order_id']);
			$tmp->productId($row['product_id']);
			$tmp->product($row['product']);
			$tmp->quantity($row['quantity']);
			$tmp->priceUnit($row['price_unit']);
			$tmp->priceUnitVat($row['price_unit_vat']);
			$tmp->vat($row['vat']);
			$tmp->priceRow($row['price_row']);
			$tmp->priceRowVat($row['price_row_vat']);
			$ret[] = $tmp;
		}
		return $ret;
	}

	/**
	 * calculate delivery price in CZK
	 * @param \Eshop\Customers\Order $order
	 * @return float
	 */
	public function calcPriceDelivery(Order $order, $currency) {
		$subtotal = $order->calcSubtotalByLang(TRUE);
		$country = $order->deliveryCountry() !== '' ? $order->deliveryCountry() : $order->country();
		$fee = $this->serviceFees->getDeliveryList()->getFee($order->delivererId(), $country);
		$limit = ($currency === 'EUR' ? $fee->getFreeLimitEur() : $fee->getFreeLimit());
		if ($subtotal >= $limit) {
			return 0;
		}
		return $currency === 'EUR' ? $fee->getFeeEur() : $fee->getFee();

//		$deliverers = $this->_app->dice()->create('\Eshop\Orders\DeliverersList');
//		$limit = $deliverers->getDeliverer($order->delivererId())->freeFrom();
//		if ($subtotal >= $limit) {
//			return 0;
//		}
//		return $deliverers->getDeliverer($order->delivererId())->price();
	}

	/**
	 * calculate payment fee in CZK
	 * @param Order $order
	 * @param string $currency
	 * @return float
	 */
	public function calcPricePayment(Order $order, $currency) {
		$country = $order->deliveryCountry() !== '' ? $order->deliveryCountry() : $order->country();
		return $this->serviceFees->getPaymentList()->getFeeAmnount($order->paymentId(), $country, $currency);
	}

}
