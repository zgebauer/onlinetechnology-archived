<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * list of available payment types
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class PaymentsList
{
    /** @var payment type */
    const UNKNOWN = 0;
    /** @var payment type - cash on delivery (dobirka v CR) */
    const COD = 1;
    /** @var payment type - bank transfer CR */
    const TRANSFER = 2;
    /** @var payment type - cash on delivery (dobirka Slovensko) */
    const COD_SK = 3;
    /** @var payment type - bank transfer Slovensko */
    const TRANSFER_SK = 4;
    /** @var array payments */
    private $_payments;

    /**
     * consructor
     * @param \Eshop\Orders\DataPayment $dataLayer
     */
    public function __construct(\Eshop\Orders\DataPayment $dataLayer)
    {
        $this->_dataLayer = $dataLayer;

        $this->_payments = array();
        $this->_payments[self::COD] = new Payment(self::COD, 'Dobírka Česká pošta do ČR');
        $this->_payments[self::TRANSFER] = new Payment(self::TRANSFER, 'Platba předem bankovním převodem z ČR');
        $this->_payments[self::COD_SK] = new Payment(self::COD_SK, 'Dobírka Česká pošta na Slovensko');
        $this->_payments[self::TRANSFER_SK]
            = new Payment(self::TRANSFER_SK, 'Platba předem bankovním převodem ze Slovenska');

        foreach ($this->_dataLayer->getRows() as $row) {
            $this->setPrice($row['id'], $row['price']);
        }
    }

    /**
     * retruns list of available payment types
     * @return array
     */
    public function getPayments()
    {
        return $this->_payments;
    }

    /**
     * returns payment type by id
     * @param type $paymentId
     * @return \Eshop\Orders\Payments
     */
    public function getPayment($paymentId)
    {
        return $this->_payments[$paymentId];
    }

    /**
     * set price of payment
     * @param int $paymentId
     * @param float $price
     */
    public function setPrice($paymentId, $price)
    {
        if (isset($this->_payments[$paymentId])) {
            $this->_payments[$paymentId]->price($price);
        }
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save()
    {
        return $this->_dataLayer->save($this);
    }

}

/**
 * payment type
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Payment
{
    /** @var int instance identifier */
    protected $_id;
    /** @var string name */
    private $_name;
    /** @var float fee */
    private $_price;


    /**
     * constructor
     * @param int $instanceId
     * @param string $name
     */
    public function __construct($instanceId, $name)
    {
        $this->_id = $instanceId;
        $this->_name = $name;
        $this->_price = 0;
    }

    /**
     * returns instance identifier
     * @return int
     */
    public function instanceId()
    {
        return $this->_id;
    }

    /**
     * returns name
     * @return string
     */
    public function name()
    {
        return $this->_name;
    }

    /**
     * returns and sets fee
     * @param int $value
     * @return int
     */
    public function price($value = null)
    {
        if (!is_null($value)) {
            $value = (float) $value;
            if ($value > 0) {
                $this->_price = $value;
            }
        }
        return $this->_price;
    }

}