<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * shopping cart
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Cart
{
    /** @var string last error message */
    protected $_err;
    /** @var string instance identifier */
    protected $_id;

    /** @var \Eshop\Customers\DataCart $dataLayer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data
     * @param \Eshop\Customers\DataCart $dataLayer
     * @param string $instanceId identifier of instance
     */
    public function __construct(DataCart $dataLayer, $instanceId)
    {
        $this->_err = '';
        $this->_id = '';

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
    }

    /**
     * returns last error message
     * @return string
     */
    public function err()
    {
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param string $value
     * @return string
     */
    public function instanceId($value)
    {
        //if (!is_null($value)) {
            $this->_id = $value;
        //}
        return $this->_id;
    }

    /**
     * returns items in cart
     * @return array
     */
    public function getItems()
    {
        static $ret;

        if (!isset($ret) || defined('ERGO_TESTING')) {
            $ret = $this->_dataLayer->getCartItems($this->_id);
        }
        return $ret;
    }

    /**
     * add item to cart
     * @param int $productId identificator of product
     * @param int $quantity
     * @return bool false on error
     */
    public function add($productId, $quantity)
    {
        if ($productId <= 0 || $quantity < 1) {
            return false;
        }
        return $this->_dataLayer->changeQuantityInCart($this->_id, $productId, $quantity, true);
    }

    /**
     * change quantity of item in cart
     * @param int $productId identificator of product
     * @param int $quantity
     * @return bool false on error
     */
    public function changeQuantity($productId, $quantity)
    {
        if ($productId == 0) {
            return false;
        }
        return $this->_dataLayer->changeQuantityInCart($this->_id, $productId, $quantity, false);
    }

    /**
     * remove item from cart
     * @param int $productId identificator of product
     * @return bool false on error
     */
    public function remove($productId)
    {
        if ($productId == 0) {
            return false;
        }
        return $this->_dataLayer->changeQuantityInCart($this->id, $productId, 0);
    }

    /**
     * empty shopping cart
     * @return boolean false on error
     */
    public function emptyCart()
    {
        return $this->_dataLayer->emptyCart($this->_id);
    }

    /**
     * returns total price of items in cart in CZK
     * @param boolean $withVat true=returns price with VAT, false=returns price without VAT
     * @return float
     */
    function price($withVat, $lang)
    {
        $ret = 0;
        foreach ($this->getItems() as $item) {
            $ret += $item->getPriceRow($withVat, $lang);
        }
        return $ret;
    }

    /**
     * check if cart is empty
     * @return boolean
     */
    public function isEmpty()
    {
        return (count($this->getItems()) == 0);
    }

}

/**
 * product in a shopping cart
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class CartItem extends \Eshop\Products\Product
{
    /** @var int quantity */
    private $_quantity;

    /**
     * create instance and fill it with data by given identificator
     * @param int $id identificator of instance
     */
    public function __construct(\Eshop\Products\DataProduct $dataLayer, \Ergo\ApplicationInterface $application)
    {
        $this->_quantity = 1;
        parent::__construct($dataLayer);
        $this->_app = $application;
    }

    /**
     * returns and sets quantity
     * @param int $value
     * @return int
     */
    public function quantity($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_quantity = $value;
            }
        }
        return $this->_quantity;
    }

    /**
     * returns price of one unit in CZK
     * @param boolean $withVat true=returns price with VAT, false=returns price without VAT
     * @return float
     */
    public function priceUnit($withVat, $lang)
    {
        if ($lang === 'en') {
            $basePrice = ($withVat ? $this->getPriceEurVat() : $this->getPriceEur());
        } else {
            $basePrice = ($withVat ? $this->priceVat() : $this->price());
        }
        return $this->_app->dice()->create('\Eshop\Orders\DiscountSolver')
            ->calcDiscountPrice($this->categoryId(), $basePrice);
    }

    /**
     * returns price of one row in CZK (price of unit * quantity
     * @param boolean $withVat true=returns price with VAT, false=returns price without VAT
     * @return float
     */
    public function getPriceRow($withVat, $lang)
    {
        return $this->priceUnit($withVat, $lang) * $this->quantity();
    }

}