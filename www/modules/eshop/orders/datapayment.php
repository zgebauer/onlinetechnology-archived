<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * data tier for module - payment types
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataPayment
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    /** @var string table name with records */
    private static $_tableName = '[%eshop_orders_payments%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * save given instance to database
     * @param \Eshop\Orders\PaymentsList $record
     * @return bool false on error
     */
    public function save(PaymentsList $record)
    {
        foreach ($record->getPayments() as $item) {
            if ($item->getCountryCode() !== 'CZ' && $item->getCountryCode() !== 'SK') {
                continue;
            }

            $items = array();
            $items['id'] = $item->getType();
            $items['price'] = $item->getFee();

            $itemsUpdate = array();
            $itemsUpdate[] = 'price='.$item->getFee();

            $sql = 'INSERT INTO '.self::$_tableName.' ('
                .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')
                    ON DUPLICATE KEY UPDATE '.join(',', $itemsUpdate);
            try {
                $this->_con->query($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * returns records
     * @return array
     */
    public function getRows()
    {
        $sql = 'SELECT id,price FROM '.self::$_tableName;
        return $this->_con->fetchArrayAll($sql);
    }

}