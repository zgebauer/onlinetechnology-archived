<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * data tier for module - deliverers
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataDeliverer
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    /** @var string table name with records */
    private static $_tableName = '[%eshop_orders_deliverers%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * save given instance to database
     * @param \Eshop\Orders\Category $record
     * @return bool false on error
     */
    public function save(DeliverersList $record)
    {
        foreach ($record->getDeliverers() as $item) {
            $items = array();
            $items['id'] = $item->instanceId();
            $items['price'] = $item->price();
            $items['free_from'] = $item->freeFrom();

            $itemsUpdate = array();
            $itemsUpdate[] = 'price='.$item->price();
            $itemsUpdate[] = 'free_from='.$item->freeFrom();

            $sql = 'INSERT INTO '.self::$_tableName.' ('
                .implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')
                    ON DUPLICATE KEY UPDATE '.join(',', $itemsUpdate);
            try {
                $this->_con->query($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * returns records
     * @return array
     */
    public function getRows()
    {
        $sql = 'SELECT id,price,free_from FROM '.self::$_tableName;
        return $this->_con->fetchArrayAll($sql);
    }

}