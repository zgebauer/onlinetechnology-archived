<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * rules to calculate delivery price
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@centrum.cz>
 * @package    Ergo
 * @subpackage Eshop
 */
class DiscountSolver
{

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * caculate price with discounbt for curent customer and given category
     * @param int $categoryId
     * @param float $basePrice
     * @return float
     */
    public function calcDiscountPrice($categoryId, $basePrice)
    {
        $customer = $this->_app->getVisitor();
        if (is_null($customer)) {
            return $basePrice;
        }

        $percent = NULL;
        foreach ($customer->getDiscounts() as $discount) {
            if ($discount->categoryId() === $categoryId) {
                $percent = $discount->discountPercent();
                break;
            }
        }
        if (is_null($percent)) {
            return $basePrice;
        }
        return $basePrice - ($basePrice*$percent/100);

    }

}