<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * data tier for module - shopping cart
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DataCart
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%eshop_carts%]';

    private $_app;

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_app = $application;
    }

    /**
     * returns items in shopping cart
     * @param string $cartId
     * @return array of instances \Eshop\Products\CartItem
     */
    public function getCartItems($cartId)
    {
        $sql = 'SELECT p.id,p.name,p.seo,p.price,p.price_vat,p.vat,c.quantity,p.category_id,p.`price_eur`,
            p.`price_eur_vat`
        FROM '.self::$_tableName.' AS c
        INNER JOIN eshop_products AS p ON c.product_id=p.id
        WHERE p.publish='.$this->_con->boolToSql(true).' AND c.id='.$this->_con->escape($cartId);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new CartItem(
                $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app)), $this->_app
            );
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->seo($row['seo']);
            $tmp->price($row['price']);
            $tmp->priceVat($row['price_vat']);
            $tmp->vat($row['vat']);
            $tmp->quantity($row['quantity']);
            $tmp->categoryId($row['category_id']);
            $tmp->setPriceEur($row['price_eur'])
                ->setPriceEurVat($row['price_eur_vat']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * add item to cart or change quantity of item in cart
     * @param string $cartId identificator of shopping cart
     * @param int $productId identificator of product
     * @param int $quantity quantity of product
     * @param bool $add true|false = add quantity|change quantity
     * @return bool false on error
     */
    public function changeQuantityInCart($cartId, $productId, $quantity, $add = false)
    {
        if ($quantity <= 0) {
            return $this->_con->query(
                'DELETE FROM '.self::$_tableName.' WHERE product_id='.$productId.' AND id='.$this->_con->escape($cartId)
            );
        }
        // productid already in cart?
        $sql = 'SELECT COUNT(*) FROM '.self::$_tableName.' WHERE product_id='.
            $productId.' AND id='.$this->_con->escape($cartId);
        $isInCart = ($this->_con->count($sql) > 0);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        if ($isInCart) {
            if ($add) {
                $sql = 'UPDATE '.self::$_tableName.'
                SET quantity=quantity+'.$quantity.',date='.$this->_con->escape($date->format('Y-m-d H:i:s'))
                .' WHERE product_id='.$productId.' AND id='.$this->_con->escape($cartId);
            } else {
                $sql = 'UPDATE '.self::$_tableName.'
                SET quantity='.$quantity.',date='.$this->_con->escape($date->format('Y-m-d H:i:s'))
                .' WHERE product_id='.$productId.' AND id='.$this->_con->escape($cartId);
            }
        } else {
            $items = array();
            $items['id'] = $this->_con->escape($cartId);
            $items['date'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
            $items['product_id'] = $productId;
            $items['quantity'] = $quantity;
            $sql = 'INSERT INTO '.self::$_tableName.' ('.
                implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        }
        return $this->_con->query($sql);
    }

    /**
     * empty shopping cart
     * @param string $cartId
     * @return bool
     */
    public function emptyCart($cartId)
    {
        $this->_deleteOrphanedCarts();
        return $this->_con->query('DELETE FROM '.self::$_tableName.' WHERE id='.$this->_con->escape($cartId));
    }

    /**
     * delete orphaned old carts
     * @return bool false on error
     */
    private function _deleteOrphanedCarts()
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $date->sub(new \DateInterval('P14D'));
        return $this->_con->query(
            'DELETE FROM '.self::$_tableName.' WHERE date<'.$this->_con->escape($date->format('Y-m-d H:i:s'))
        );
    }

}