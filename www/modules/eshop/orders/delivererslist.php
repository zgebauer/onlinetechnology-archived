<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

/**
 * list of available deliverers
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class DeliverersList
{
    /** @var delivery type */
    const UNKNOWN = 0;
    /** @var delivery type - Ceska posta Balik do ruky */
    const CP_BALIK_DO_RUKY = 1;
    /** @var delivery type - Ceska posta Balik na postu */
    const CP_BALIK_NA_POSTU = 2;
    /** @var delivery type - Ceska posta Balik doporuceny */
    const CP_BALIK_DOPORUCENY = 3;
    /** @var delivery type - Ceska posta Balik express */
    const CP_BALIK_EXPRES = 4;
    /** @var delivery type - Balik na Slovensko  */
    const SK_BALIK = 5;
    /** @var array deliverers */
    private $_deliverers;

    /**
     * consructor
     * @param \Eshop\Orders\DataDeliverer $dataLayer
     */
    public function __construct(\Eshop\Orders\DataDeliverer $dataLayer)
    {
        $this->_dataLayer = $dataLayer;

        $this->_deliverers = array();
        $this->_deliverers[self::CP_BALIK_DO_RUKY]
            = new Deliverer(self::CP_BALIK_DO_RUKY, 'Česká pošta - Balík do ruky');
        $this->_deliverers[self::CP_BALIK_NA_POSTU]
            = new Deliverer(self::CP_BALIK_NA_POSTU, 'Česká pošta - Balík na poštu');
        $this->_deliverers[self::CP_BALIK_DOPORUCENY]
            = new Deliverer(self::CP_BALIK_DOPORUCENY, 'Česká pošta - Doporučený balík');
        $this->_deliverers[self::CP_BALIK_EXPRES]
            = new Deliverer(self::CP_BALIK_EXPRES, 'Česká pošta - Balík Express');
        $this->_deliverers[self::SK_BALIK] = new Deliverer(self::SK_BALIK, 'Balík na Slovensko');

        foreach ($this->_dataLayer->getRows() as $row) {
            $this->setPrice($row['id'], $row['price']);
            $this->setFreeFrom($row['id'], $row['free_from']);
        }
    }

    /**
     * retruns list of available deliverers
     * @return array
     */
    public function getDeliverers()
    {
        return $this->_deliverers;
    }

    /**
     * returns deliverer by id
     * @param type $paymentId
     * @return \Eshop\Orders\Deliverer
     */
    public function getDeliverer($delivererId)
    {
        return $this->_deliverers[$delivererId];
    }

    /**
     * set delivery price
     * @param int $delivererId
     * @param float $price
     */
    public function setPrice($delivererId, $price)
    {
        if (isset($this->_deliverers[$delivererId])) {
            $this->_deliverers[$delivererId]->price($price);
        }
    }

    /**
     * set limit of price for free delivery
     * @param int $delivererId
     * @param float $price
     */
    public function setFreeFrom($delivererId, $price)
    {
        if (isset($this->_deliverers[$delivererId])) {
            $this->_deliverers[$delivererId]->freeFrom($price);
        }
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save()
    {
        return $this->_dataLayer->save($this);
    }

}

/**
 * delivery type
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Deliverer
{
    /** @var int instance identifier */
    protected $_id;
    /** @var string name */
    private $_name;
    /** @var float fee */
    private $_price;
    /** @var float limit for free delivery */
    private $_freeFrom;

    /**
     * constructor
     * @param int $instanceId
     * @param string $name
     */
    public function __construct($instanceId, $name)
    {
        $this->_id = $instanceId;
        $this->_name = $name;
        $this->_price = 0;
        $this->_freeFrom = 0;
    }

    /**
     * returns instance identifier
     * @return int
     */
    public function instanceId()
    {
        return $this->_id;
    }

    /**
     * returns name
     * @return string
     */
    public function name()
    {
        return $this->_name;
    }

    /**
     * returns and sets fee
     * @param int $value
     * @return int
     */
    public function price($value = null)
    {
        if (!is_null($value)) {
            $value = (float) $value;
            if ($value >= 0) {
                $this->_price = $value;
            }
        }
        return $this->_price;
    }

    /**
     * returns and sets limit for free delivery
     * @param int $value
     * @return int
     */
    public function freeFrom($value = null)
    {
        if (!is_null($value)) {
            $value = (float) $value;
            if ($value >= 0) {
                $this->_freeFrom = $value;
            }
        }
        return $this->_freeFrom;
    }

}