<?php

namespace Eshop\Orders\Fees;

class RepositoryDelivery extends \Ergo\Component {

	/** @var Mapper $mapper */
	private $mapper;

    /**
     * @param \Ergo\ApplicationInterface $application
     * @param MapperDelivery $mapper
     */
	public function __construct(\Ergo\ApplicationInterface $application, MapperDelivery $mapper) {
		parent::__construct($application);
		$this->mapper = $mapper;
	}

	/**
	 * @return DeliveryCountryFee[]
	 */
    public function getFees() {
        return $this->mapper->getFees();
    }

	/**
	 * @param DeliveryCountryFee[] $fees
	 */
    public function saveFees(array $fees) {
        $this->mapper->saveFees($fees);
    }

}
