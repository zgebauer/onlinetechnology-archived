<?php

namespace Eshop\Orders\Fees;

class DeliveryList {

	/** @var delivery type unknown */
    const UNKNOWN = 0;

    /** @var Ceska posta Balik do ruky */
    const CP_BALIK_DO_RUKY = 1;

    /** @var Ceska posta Balik na postu */
    const CP_BALIK_NA_POSTU = 2;

    /** @var Ceska posta Balik doporuceny */
    const CP_BALIK_DOPORUCENY = 3;

    /** @var Ceska posta Balik express */
    const CP_BALIK_EXPRES = 4;

    /** @var Ceska posta, balik do zahranici */
    const CP_ABROAD = 5;

	/**
	 * @var int
	 */
	const PPL = 6;

	/** @var DeliveryType[] */
	private $deliveryTypes;

	/** @var DeliveryCountryFees[] */
	private $countryFees;

	/**
	 * @param CountryList $countryList
	 */
	public function __construct(CountryList $countryList) {
		$this->deliveryTypes = array(
			new DeliveryType(self::CP_BALIK_DO_RUKY, '[%LG_POST_BALIK_DO_RUKY%]'),
			new DeliveryType(self::CP_BALIK_NA_POSTU, '[%LG_POST_BALIK_NA_POSTU%]'),
			new DeliveryType(self::CP_BALIK_DOPORUCENY, '[%LG_POST_DOPOR_BALIK%]'),
			new DeliveryType(self::CP_BALIK_EXPRES, '[%LG_POST_BALIK_EXPRESS%]'),
			new DeliveryType(self::CP_ABROAD, '[%LG_POST_BALIK%]'),
			new DeliveryType(self::PPL, 'PPL')
		);

		$this->countryFees = array();
		foreach ($countryList->getCountries() as $country) {
			$this->countryFees[self::CP_BALIK_DO_RUKY][$country->getCode()] = new DeliveryCountryFee(self::CP_BALIK_DO_RUKY, $country->getCode());
			$this->countryFees[self::CP_BALIK_NA_POSTU][$country->getCode()] = new DeliveryCountryFee(self::CP_BALIK_NA_POSTU, $country->getCode());
			$this->countryFees[self::CP_BALIK_DOPORUCENY][$country->getCode()] = new DeliveryCountryFee(self::CP_BALIK_DOPORUCENY, $country->getCode());
			$this->countryFees[self::CP_BALIK_EXPRES][$country->getCode()] = new DeliveryCountryFee(self::CP_BALIK_EXPRES, $country->getCode());
			$this->countryFees[self::CP_ABROAD][$country->getCode()] = new DeliveryCountryFee(self::CP_ABROAD, $country->getCode());
			$this->countryFees[self::PPL][$country->getCode()] = new DeliveryCountryFee(self::PPL, $country->getCode());
		}
	}

	/**
	 * @param DeliveryCountryFee[] $fees
	 */
	public function setFees(array $fees) {
		foreach ($fees as $fee) {
			$this->setFee($fee);
		}
	}

	/**
	 * @param DeliveryCountryFee $fee
	 */
	public function setFee(DeliveryCountryFee $fee) {
		$this->countryFees[$fee->getDeliveryType()][$fee->getCountryCode()] = $fee;
	}

	/**
	 * @return DeliveryType[]
	 */
	public function getAvailableDeliveryTypes() {
		return $this->deliveryTypes;
	}

	/**
	 * return country fees in format [delivery type][country code]=>DeliveryCountryFee
	 * @return array
	 */
	public function getFees() {
		return $this->countryFees;
	}

	/**
	 * @param int $deliveryType
	 * @param string $countryCode
	 * @return DeliveryCountryFee
	 */
	public function getFee($deliveryType, $countryCode) {
		return $this->countryFees[$deliveryType][$countryCode];
	}

	/**
	 * @param string $countryCode
	 * @return DeliveryCountryFee[]
	 */
	public function getFeesEnabledForCountry($countryCode) {
		$ret = array();
		foreach ($this->countryFees as $fees) {
			foreach ($fees as $fee) {
				if ($fee->getCountryCode() === $countryCode && $fee->isAllowed()) {
					$ret[] = $fee;
				}
			}
		}
		return $ret;
	}

	/**
	 * @param int $typeId
	 * @return string
	 */
	public function getTypeName($typeId) {
		foreach ($this->deliveryTypes as $type) {
			if ($type->getType() === $typeId) {
				return $type->getName();
			}
		}
		return '';
	}

	/**
	 * @param int $deliveryType
	 * @param string $countryCode
	 * @param string $currency
	 * @return float
	 */
	public function getFeeAmnount($deliveryType, $countryCode, $currency) {
		$fee = $this->getFee($deliveryType, $countryCode);
		return ($currency === 'EUR' ? $fee->getFeeEur() : $fee->getFee());
	}

}
