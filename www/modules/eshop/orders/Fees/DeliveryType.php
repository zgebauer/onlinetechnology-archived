<?php

namespace Eshop\Orders\Fees;

class DeliveryType {

    /** @var int */
    const UNKNOWN = 0;

    /** @var int Ceska posta Balik do ruky */
    const CP_BALIK_DO_RUKY = 1;

    /** @var int Ceska posta Balik na postu */
    const CP_BALIK_NA_POSTU = 2;

    /** @var int Ceska posta Balik doporuceny */
    const CP_BALIK_DOPORUCENY = 3;

    /** @var int Ceska posta Balik express */
    const CP_BALIK_EXPRES = 4;

    /** @var int  Balik na Slovensko  */
    const SK_BALIK = 5;

    /** @var int - Česká pošta balík do zahranici */
    const CZECH_POST = 6;

	/** @var int */
	private $type;

	/** @var string */
	private $name;

	/**
	 * @param int $type
	 * @param string $name
	 */
	public function __construct($type, $name) {
		$this->type = $type;
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

}
