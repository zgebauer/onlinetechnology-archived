<?php

namespace Eshop\Orders\Fees;

class PaymentList {

	/** @var payment type */
	const UNKNOWN = 0;

	/** @var payment type - cash on delivery (dobirka v CR) */
	const COD = 1;

	/** @var payment type - bank transfer CR */
	const TRANSFER = 2;

	/** @var PaymentType[] */
	private $paymentTypes;

	/** @var PaymentCountryFees[] */
	private $countryFees;

	/**
	 * @param CountryList $countryList
	 */
	public function __construct(CountryList $countryList) {
		$this->paymentTypes = array(
			new PaymentType(self::COD, '[%LG_PAYMENT_CASH_ON_DELIVERY%]'),
			new PaymentType(self::TRANSFER, '[%LG_PAYMENT_BANK_TRANSFER%]')
		);

		$this->countryFees = array();
		foreach ($countryList->getCountries() as $country) {
			$this->countryFees[self::COD][$country->getCode()] = new PaymentCountryFee(self::COD, $country->getCode());
			$this->countryFees[self::TRANSFER][$country->getCode()] = new PaymentCountryFee(self::TRANSFER, $country->getCode());
		}
	}

	/**
	 * @param PaymentCountryFee[] $fees
	 */
	public function setFees(array $fees) {
		foreach ($fees as $fee) {
			$this->setFee($fee);
		}
	}

	/**
	 * @param PaymentCountryFee $fee
	 */
	public function setFee(PaymentCountryFee $fee) {
		$this->countryFees[$fee->getPaymentType()][$fee->getCountryCode()] = $fee;
	}

	/**
	 * @return PaymentType[]
	 */
	public function getAvailablePayments() {
		return $this->paymentTypes;
	}

	/**
	 * return country fees in format [payment type][country code]=>PaymentCountryFee
	 * @return array
	 */
	public function getFees() {
		return $this->countryFees;
	}

	/**
	 * @param int $paymentType
	 * @param string $countryCode
	 * @return PaymentCountryFee
	 */
	public function getFee($paymentType, $countryCode) {
		return $this->countryFees[$paymentType][$countryCode];
	}

	/**
	 * @param string $countryCode
	 * @return PaymentCountryFee[]
	 */
	public function getFeesEnabledForCountry($countryCode) {
		$ret = array();
		foreach ($this->countryFees as $fees) {
			foreach ($fees as $fee) {
				if ($fee->getCountryCode() === $countryCode && $fee->isAllowed()) {
					$ret[] = $fee;
				}
			}
		}
		return $ret;
	}

	/**
	 * @param int $typeId
	 * @return string
	 */
	public function getTypeName($typeId) {
		foreach ($this->paymentTypes as $type) {
			if ($type->getType() === $typeId) {
				return $type->getName();
			}
		}
		return '';
	}

	/**
	 * @param int $paymentType
	 * @param string $countryCode
	 * @param string $currency
	 * @return float
	 */
	public function getFeeAmnount($paymentType, $countryCode, $currency) {
		$fee = $this->getFee($paymentType, $countryCode);
		return ($currency === 'EUR' ? $fee->getFeeEur() : $fee->getFee());
	}

}
