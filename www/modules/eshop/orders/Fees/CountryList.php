<?php

namespace Eshop\Orders\Fees;

class CountryList {

	/** @var array */
	private $countries;

	public function __construct() {
		$this->countries = array(
			'CZ' => new Country('CZ', 'Česká Republika'),
			'SK' => new Country('SK', 'Slovensko'),
			'PL' => new Country('PL', 'Polsko'),
			'DE' => new Country('DE', 'Německo'),
			'AT' => new Country('AT', 'Rakousko'),
			'FR' => new Country('FR', 'Francie'),
			'CH' => new Country('CH', 'Švýcarsko'),
			'PT' => new Country('PT', 'Portugalsko'),
			'ES' => new Country('ES', 'Španělsko'),
			'GB' => new Country('GB', 'Anglie')
		);
	}

	/**
	 * @return Country[]
	 */
	public function getCountries() {
		return $this->countries;
	}

	/**
	 * @param Country $country
	 * @return CountryList
	 * @throws Exception
	 */
	public function setCountry(Country $country) {
		if (!isset($this->countries[$country->getCountryCode()])) {
			throw new Exception('unknown country code ' . $country->getCountryCode());
		}
		$this->countries[$country->getCountryCode()]
			->setLimitFreeDeliveryEur($country->getLimitFreeDeliveryEur())
			->setFeeDeliveryCzechPostEur($country->getFeeDeliveryCzechPostEur())
			->setFeePaymentCashOnDeliveryEur($country->getFeePaymentCashOnDeliveryEur())
			->setFeePaymentBankTransferEur($country->getFeePaymentBankTransferEur());
		return $this;
	}

}
