<?php

namespace Eshop\Orders\Fees;

use GstLib\Vars;

class MapperDelivery extends \Ergo\DbMapperAbstract {

	/** @var string name of database table */
	private static $tableName = '`eshop_delivery_fees`';

	/**
	 * @return DeliveryCountryFee[]
	 */
	public function getFees() {
		$ret = array();
		$sql = 'SELECT `delivery_type`,`country_code`,`allow`,`fee`,`fee_eur`,`free_limit`,`free_limit_eur`,`info` 
			FROM ' . self::$tableName;
		$rows = $this->con->fetchArrayAll($sql);
		foreach ($rows as $row) {
			$fee = new DeliveryCountryFee($row['delivery_type'], $row['country_code']);
			$fee->setAllowed(Vars::int2bool($row['allow']))
				->setFee($row['fee'])
				->setFeeEur($row['fee_eur'])
				->setFreeLimit($row['free_limit'])
				->setFreeLimitEur($row['free_limit_eur'])
				->setInfo($row['info']);
			$ret[] = $fee;
		}
		return $ret;
	}

	/**
	 * @param array $fees in format [delivery type][country code]=>DeliveryCountryFee
	 */
	public function saveFees(array $fees) {
		$sqls = array();
		foreach ($fees as $countryFees) {
			foreach ($countryFees as $fee) {
				$itemsInsert = array();
				$itemsInsert['`delivery_type`'] = $fee->getDeliveryType();
				$itemsInsert['`country_code`'] = $this->con->escape($fee->getCountryCode());
				$itemsInsert['`allow`'] = Vars::bool2int($fee->isAllowed());
				$itemsInsert['`fee`'] = $fee->getFee();
				$itemsInsert['`fee_eur`'] = $fee->getFeeEur();
				$itemsInsert['`free_limit`'] = $fee->getFreeLimit();
				$itemsInsert['`free_limit_eur`'] = $fee->getFreeLimitEur();
				$itemsInsert['`info`'] = $this->con->escape($fee->getInfo());

				$itemsUpdate = array();
				$itemsUpdate[] = '`allow`=' . Vars::bool2int($fee->isAllowed());
				$itemsUpdate[] = '`fee`=' . $fee->getFee();
				$itemsUpdate[] = '`fee_eur`=' . $fee->getFeeEur();
				$itemsUpdate[] = '`free_limit`=' . $fee->getFreeLimit();
				$itemsUpdate[] = '`free_limit_eur`=' . $fee->getFreeLimitEur();
				$itemsUpdate[] = '`info`=' . $this->con->escape($fee->getInfo());

				$sqls[] = 'INSERT INTO ' . self::$tableName . ' ('
					. implode(',', array_keys($itemsInsert)) . ') VALUES (' . implode(',', $itemsInsert)
					. ') ON DUPLICATE KEY UPDATE ' . join(',', $itemsUpdate);
			}
		}
		$this->con->transaction($sqls);
	}

}
