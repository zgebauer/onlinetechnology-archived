<?php

namespace Eshop\Orders\Fees;

use GstLib\Vars;

class Country {

	/** @var string */
	private $code;

	/** @var string */
	private $name;

	/**
	 * @param string $code
	 * @param string $name
	 */
	public function __construct($code, $name) {
		$this->code = $code;
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return Country
	 */
	public function setName($name) {
		$this->name = Vars::substr($name, 0, 30);
		return $this;
	}

}
