<?php

namespace Eshop\Orders\Fees;

class RepositoryPayment extends \Ergo\Component {

	/** @var Mapper $mapper */
	private $mapper;

    /**
     * @param \Ergo\ApplicationInterface $application
     * @param MapperPayment $mapper
     */
	public function __construct(\Ergo\ApplicationInterface $application, MapperPayment $mapper) {
		parent::__construct($application);
		$this->mapper = $mapper;
	}

	/**
	 * @return PaymentCountryFee[]
	 */
    public function getFees() {
        return $this->mapper->getFees();
    }

	/**
	 * @param PaymentCountryFee[] $fees
	 */
    public function saveFees(array $fees) {
        $this->mapper->saveFees($fees);
    }

}
