<?php

namespace Eshop\Orders\Fees;

class PaymentCountryFee {

	/** @var int */
	private $paymentType;

	/** @var string */
	private $countryCode;

	/** @var bool */
	private $allowed;

	/** @var float */
	private $fee;

	/** @var float */
	private $feeEur;

	/** @var string */
	private $info;

	/**
	 * @param int $paymentType
	 * @param string $countryCode
	 */
	public function __construct($paymentType, $countryCode) {
		$this->paymentType = (int)$paymentType;
		$this->countryCode = $countryCode;
		$this->allowed = false;
		$this->fee = 0;
		$this->feeEur = 0;
		$this->info = '';
	}

	/**
	 * @return int
	 */
	public function getPaymentType() {
		return $this->paymentType;
	}

	/**
	 * @return string
	 */
	public function getCountryCode() {
		return $this->countryCode;
	}

	/**
	 * @return bool
	 */
	public function isAllowed() {
		return $this->allowed;
	}

	/**
	 * @param bool $allowed
	 * @return PaymentCountryFee
	 */
	public function setAllowed($allowed) {
		$this->allowed = (bool)$allowed;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFee() {
		return $this->fee;
	}

	/**
	 * @param float $fee
	 * @return PaymentCountryFees
	 */
	public function setFee($fee) {
		$this->fee = (float)$fee;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFeeEur() {
		return $this->feeEur;
	}

	/**
	 * @param float $feeEur
	 * @return PaymentCountryFees
	 */
	public function setFeeEur($feeEur) {
		$this->feeEur = (float)$feeEur;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getInfo() {
		return $this->info;
	}

	/**
	 * @param string $info
	 * @return PaymentCountryFee
	 */
	public function setInfo($info) {
		$this->info = $info;
		return $this;
	}

    /**
     * @param string $lang
     * @return float
     */
    public function getFeeByLang($lang) {
        return ($lang === 'en' ? $this->getFeeEur() : $this->getFee());
    }

}
