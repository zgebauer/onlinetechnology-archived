<?php

namespace Eshop\Orders\Fees;

class DeliveryCountryFee {

	/** @var int */
	private $deliveryType;

	/** @var string */
	private $countryCode;

	/** @var bool */
	private $allowed;

	/** @var float */
	private $fee;

	/** @var float */
	private $feeEur;

	/** @var float */
	private $freeLimit;

	/** @var float */
	private $freeLimitEur;

	/** @var string */
	private $info;

	/**
	 * @param int $deliveryType
	 * @param string $countryCode
	 */
	public function __construct($deliveryType, $countryCode) {
		$this->deliveryType = (int)$deliveryType;
		$this->countryCode = $countryCode;
		$this->allowed = false;
		$this->fee = 0;
		$this->feeEur = 0;
		$this->freeLimit = 0;
		$this->freeLimitEur = 0;
		$this->info = '';
	}

	/**
	 * @return int
	 */
	public function getDeliveryType() {
		return $this->deliveryType;
	}

	/**
	 * @return string
	 */
	public function getCountryCode() {
		return $this->countryCode;
	}

	/**
	 * @return bool
	 */
	public function isAllowed() {
		return $this->allowed;
	}

	/**
	 * @param bool $allowed
	 * @return DeliveryCountryFee
	 */
	public function setAllowed($allowed) {
		$this->allowed = (bool)$allowed;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFee() {
		return $this->fee;
	}

	/**
	 * @param float $fee
	 * @return DeliveryCountryFees
	 */
	public function setFee($fee) {
		$this->fee = (float)$fee;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFeeEur() {
		return $this->feeEur;
	}

	/**
	 * @param float $feeEur
	 * @return DeliveryCountryFees
	 */
	public function setFeeEur($feeEur) {
		$this->feeEur = (float)$feeEur;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFreeLimit() {
		return $this->freeLimit;
	}

	/**
	 * @param float $freeLimit
	 * @return DeliveryCountryFee
	 */
	public function setFreeLimit($freeLimit) {
		$this->freeLimit = (float)$freeLimit;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getFreeLimitEur() {
		return $this->freeLimitEur;
	}

	/**
	 * @param float $freeLimitEur
	 * @return DeliveryCountryFee
	 */
	public function setFreeLimitEur($freeLimitEur) {
		$this->freeLimitEur = (float)$freeLimitEur;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getInfo() {
		return $this->info;
	}

	/**
	 * @param string $info
	 * @return DeliveryCountryFee
	 */
	public function setInfo($info) {
		$this->info = $info;
		return $this;
	}

    /**
     * @param string $lang
     * @return float
     */
    public function getFeeByLang($lang) {
        return ($lang === 'en' ? $this->getFeeEur() : $this->getFee());
    }

    /**
     * @param string $lang
     * @return float
     */
    public function getFreeLimitByLang($lang) {
        return ($lang === 'en' ? $this->getFreeLimitEur() : $this->getFreeLimit());
    }

}
