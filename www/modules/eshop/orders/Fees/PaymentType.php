<?php

namespace Eshop\Orders\Fees;

class PaymentType {

	/** @var payment type */
	const UNKNOWN = 0;

	/** @var payment type - cash on delivery (dobirka v CR) */
	const COD = 1;

	/** @var payment type - bank transfer CR */
	const TRANSFER = 2;

	/** @var int */
	private $type;

	/** @var string */
	private $name;

	/**
	 * @param int $type
	 * @param string $name
	 */
	public function __construct($type, $name) {
		$this->type = $type;
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

}
