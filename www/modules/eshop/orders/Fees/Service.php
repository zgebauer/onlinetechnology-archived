<?php

namespace Eshop\Orders\Fees;

class Service extends \Ergo\Component {

	/** @var RepositoryPayment */
	private $repositoryPayment;

	/** @var PaymentList */
	private $paymentList;

	/** @var RepositoryDelivery */
	private $repositoryDelivery;

	/** @var DeliveryList */
	private $deliveryList;

	/**
	 *
	 * @param \Ergo\ApplicationInterface $application
	 * @param RepositoryPayment $repositoryPayment
	 * @param RepositoryDelivery $repositoryDelivery
	 */
	public function __construct(
		\Ergo\ApplicationInterface $application,
		RepositoryPayment $repositoryPayment,
		RepositoryDelivery $repositoryDelivery
	) {
		parent::__construct($application);
		$this->repositoryPayment = $repositoryPayment;
		$this->repositoryDelivery = $repositoryDelivery;
		$this->paymentList = $this->app->getInstance('\Eshop\Orders\Fees\PaymentList');
		$this->paymentList->setFees($this->repositoryPayment->getFees());
		$this->deliveryList = $this->app->getInstance('\Eshop\Orders\Fees\DeliveryList');
		$this->deliveryList->setFees($this->repositoryDelivery->getFees());
	}

	/**
	 * @return \Eshop\Orders\Fees\PaymentList
	 */
	public function getPaymentList() {
		return $this->paymentList;
	}

	/**
	 * @param PaymentList $paymentList
	 */
	public function savePaymentList(PaymentList $paymentList) {
		$this->repositoryPayment->saveFees($paymentList->getFees());
	}

	/**
	 * @return \Eshop\Orders\Fees\DeliveryList
	 */
	public function getDeliveryList() {
		return $this->deliveryList;
	}

	/**
	 * @param DeliveryList $deliveryList
	 */
	public function saveDeliveryList(DeliveryList $deliveryList) {
		$this->repositoryDelivery->saveFees($deliveryList->getFees());
	}

}
