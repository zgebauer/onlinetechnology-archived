<?php

/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop\Orders;

use GstLib\Vars;
use GstLib\Html;

use Eshop\Orders\Fees\PaymentList;
use Eshop\Orders\Fees\DeliveryList;

/**
 * order of products
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Order {

	/** @var default order status */
	const STATUS_RECEIVED = 0;

	/** @var order cancelled */
	const STATUS_STORNO = 1;

	/** @var order finished */
	const STATUS_FINISHED = 2;

	/** @var order finished */
	const STATUS_PAID = 3;

	/** @var string last error message */
	protected $_err;

	/** @var int instance identifier */
	protected $_id;

	/** @var bool flag "loaded from data tier" */
	protected $_loaded;

	/** @var date date of order */
	private $_date;

	/** @var int id of registered customer */
	private $_customerId;

	/** @var string first name */
	private $_firstName;

	/** @var string last name */
	private $_lastName;
	/*	 * @var string email */
	private $_email;

	/** @var string company */
	private $_company;

	/** @var string phone */
	private $_phone;

	/** @var string address */
	private $_address;

	/** @var string city */
	private $_city;

	/** @var string post code */
	private $_postCode;

	/** @var string country code */
	private $_country;

	/** @var bool */
	private $_useDeliveryAddress;

	/** @var string delivery address */
	private $_deliveryAddress;

	/** @var string delivery city */
	private $_deliveryCity;

	/** @var string delivery post code */
	private $_deliveryPostCode;

	/** @var string delivery country code */
	private $_deliveryCountry;

	/** @var string IC */
	private $_ic;

	/** @var string DIC */
	private $_dic;

	/** @var string note */
	private $_note;

	/** @var int payment type */
	private $_paymentId;

	/** @var int deliverer */
	private $_delivererId;

	/** @var float total price of products without VAT in CZK */
	private $_subtotal;

	/** @var float total price of products with VAT in CZK */
	private $_subtotalVat;

	/** @var float */
	private $subtotalEur;

	/** @var float */
	private $subtotalEurVat;

	/** @var float price of delivery */
	private $_priceDelivery;

	/** @var float */
	private $priceDeliveryEur;

	/** @var float payment fee */
	private $_pricePayment;

	/** @var float */
	private $pricePaymentEur;

	/** @var float total price incl delivery without VAT in CZK */
	private $_grandTotal;

	/** @var float total price incl delivery with VAT in CZK */
	private $_grandTotalVat;

	/** @var float */
	private $grandTotalEur;

	/** @var float */
	private $grandTotalEurVat;

	/** @var string parcel number */
	private $_parcelNr;

	/** @var array items in order */
	private $_items;

	/** @var int order status */
	private $_status;

	/** @var date when ordered items has been sent */
	private $_dateSent;

	/** @var string */
	private $lang;

	/** @var \Eshop\Orders\Data $dataLayer */
	protected $_dataLayer;

	/**
	 * create instance and fill it with data by given identifier
	 * @param \Eshop\Orders\Data $dataLayer
	 * @param int $instanceId identifier of instance
	 */
	public function __construct(DataOrder $dataLayer, $instanceId = 0) {
		$this->_err = '';
		$this->_id = 0;
		$this->_loaded = false;
		$this->_date = null;
		$this->_customerId = 0;
		$this->_firstName = $this->_lastName = '';
		$this->_email = $this->_phone = '';
		$this->_company = $this->_ic = $this->_dic = '';
		$this->_address = $this->_city = $this->_postCode = '';
		$this->_usedeliveryAddress = false;
		$this->_deliveryAddress = $this->_deliveryCity = $this->_deliveryPostCode = '';
		$this->_country = $this->_deliveryCountry = 'CZ';
		$this->_note = '';
		$this->_paymentId = $this->_delivererId = 0;
		$this->_subtotal = $this->_subtotalVat = 0;
		$this->subtotalEur = 0;
		$this->subtotalEurVat = 0;
		$this->_priceDelivery = $this->_pricePayment = 0;
		$this->priceDeliveryEur = 0;
		$this->pricePaymentEur = 0;
		$this->_grandTotal = $this->_grandTotalVat = 0;
		$this->grandTotalEur = 0;
		$this->grandTotalEurVat = 0;
		$this->_parcelNr = '';
		$this->_status = self::STATUS_RECEIVED;
		$this->lang = 'cs';

		$this->_dataLayer = $dataLayer;

		$this->instanceId($instanceId);
		if ($this->_id != 0) {
			$this->load();
		}
	}

	/**
	 * returns and sets last error message
	 * @param string $value
	 * @return string
	 */
	public function err($value = null) {
		if (!is_null($value)) {
			$this->_err = $value;
		}
		return $this->_err;
	}

	/**
	 * returns and sets instance identifier
	 * @param int $value
	 * @return int
	 */
	public function instanceId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_id = $value;
			}
		}
		return $this->_id;
	}

	/**
	 * returns flag "record loaded"
	 * @return bool
	 */
	public function loaded() {
		return $this->_loaded;
	}

	/**
	 * returns and sets date of order
	 * @param string $value
	 * @return Datetime|null
	 */
	public function date($value = null) {
		if (!is_null($value)) {
			$value = trim($value);
			if ($value === '' || (strpos($value, '0000') !== false)) {
				$this->_date = null;
			} else {
				$formattedDate = \GstLib\Date::dateTime2Iso($value, true);
				if ($formattedDate !== false) {
					$date = new \DateTime($formattedDate, new \DateTimeZone('UTC'));
					$this->_date = $date;
				}
			}
		}
		return $this->_date;
	}

	/**
	 * returns and sets identifier of customer wchich place order
	 * @param int $value
	 * @return int
	 */
	public function customerId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_customerId = $value;
			}
		}
		return $this->_customerId;
	}

	/**
	 * returns and sets email (unique username)
	 * @param string $value
	 * @return string
	 */
	public function email($value = null) {
		if (!is_null($value)) {
			$value = substr($value, 0, 50);
			if (Html::isEmail($value)) {
				$this->_email = $value;
			}
		}
		return $this->_email;
	}

	/**
	 * returns and sets first name
	 * @param string $value
	 * @return string
	 */
	public function firstName($value = null) {
		if (!is_null($value)) {
			$this->_firstName = Vars::substr($value, 0, 20);
		}
		return $this->_firstName;
	}

	/**
	 * returns and sets last name
	 * @param string $value
	 * @return string
	 */
	public function lastName($value = null) {
		if (!is_null($value)) {
			$this->_lastName = Vars::substr($value, 0, 40);
		}
		return $this->_lastName;
	}

	/**
	 * returns and sets sets company
	 * @param string $value
	 * @return string
	 */
	public function company($value = null) {
		if (!is_null($value)) {
			$this->_company = Vars::substr($value, 0, 50);
		}
		return $this->_company;
	}

	/**
	 * returns and sets phone
	 * @param string $value
	 * @return string
	 */
	public function phone($value = null) {
		if (!is_null($value)) {
			$this->_phone = Vars::substr($value, 0, 13);
		}
		return $this->_phone;
	}

	/**
	 * returns and sets address
	 * @param string $value
	 * @return string
	 */
	public function address($value = null) {
		if (!is_null($value)) {
			$this->_address = Vars::substr($value, 0, 50);
		}
		return $this->_address;
	}

	/**
	 * returns and sets city
	 * @param string $value
	 * @return string
	 */
	public function city($value = null) {
		if (!is_null($value)) {
			$this->_city = Vars::substr($value, 0, 50);
		}
		return $this->_city;
	}

	/**
	 * returns and sets post code
	 * @param string $value
	 * @return string
	 */
	public function postCode($value = null) {
		if (!is_null($value)) {
			$this->_postCode = Vars::substr($value, 0, 10);
		}
		return $this->_postCode;
	}

	/**
	 * returns and sets country code
	 * @param string $value
	 * @return string
	 */
	public function country($value = null) {
		if (!is_null($value)) {
			$this->_country = Vars::substr($value, 0, 2);
		}
		return $this->_country;
	}

	/**
	 * returns and sets IC
	 * @param string $value
	 * @return string
	 * @SuppressWarnings("short")
	 */
	public function ic($value = null) {
		if (!is_null($value)) {
			$this->_ic = Vars::substr($value, 0, 8);
		}
		return $this->_ic;
	}

	/**
	 * returns and sets DIC
	 * @param string $value
	 * @return string
	 */
	public function dic($value = null) {
		if (!is_null($value)) {
			$this->_dic = Vars::substr($value, 0, 12);
		}
		return $this->_dic;
	}

	/**
	 * returns and sets delivery address
	 * @param string $value
	 * @return string
	 */
	public function deliveryAddress($value = null) {
		if (!is_null($value)) {
			$this->_deliveryAddress = Vars::substr($value, 0, 50);
		}
		return $this->_deliveryAddress;
	}

	/**
	 * returns and sets delivery city
	 * @param string $value
	 * @return string
	 */
	public function deliveryCity($value = null) {
		if (!is_null($value)) {
			$this->_deliveryCity = Vars::substr($value, 0, 50);
		}
		return $this->_deliveryCity;
	}

	/**
	 * returns and sets delivery post code
	 * @param string $value
	 * @return string
	 */
	public function deliveryPostCode($value = null) {
		if (!is_null($value)) {
			$this->_deliveryPostCode = Vars::substr($value, 0, 10);
		}
		return $this->_deliveryPostCode;
	}

	/**
	 * returns and sets delivery country code
	 * @param string $value
	 * @return string
	 */
	public function deliveryCountry($value = null) {
		if (!is_null($value)) {
			$this->_deliveryCountry = Vars::substr($value, 0, 2);
		}
		return $this->_deliveryCountry;
	}

	/**
	 * returns anbd sets note
	 * @param string $value
	 * @return string
	 */
	public function note($value = null) {
		if (!is_null($value)) {
			$this->_note = $value;
		}
		return $this->_note;
	}

	/**
	 * returns and sets payment type id
	 * @param int $value
	 * @return int
	 */
	public function paymentId($value = null) {
		if (!is_null($value)) {
			$this->_paymentId = (int)$value;
		}
		return $this->_paymentId;
	}

	/**
	 * returns and sets delivererer id
	 * @param int $value
	 * @return int
	 */
	public function delivererId($value = null) {
		if (!is_null($value)) {
			$this->_delivererId = (int)$value;
		}
		return $this->_delivererId;
	}

	/**
	 * returns and sets price of products in order without VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function subtotal($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_subtotal = $value;
			}
		}
		return $this->_subtotal;
	}

	/**
	 * returns and sets price of products in order with VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function subtotalVat($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_subtotalVat = $value;
			}
		}
		return $this->_subtotalVat;
	}

	/**
	 * @return float
	 */
	public function getSubtotalEur() {
		return $this->subtotalEur;
	}

	/**
	 * @param type $subtotalEur
	 * @return Order
	 * @throws \InvalidArgumentException
	 */
	public function setSubtotalEur($subtotalEur) {
		$value = (float)$subtotalEur;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->subtotalEur = $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getSubtotalEurVat() {
		return $this->subtotalEurVat;
	}

	/**
	 * @param type $subtotalEurVat
	 * @return Order
	 * @throws \InvalidArgumentException
	 */
	public function setSubtotalEurVat($subtotalEurVat) {
		$value = (float)$subtotalEurVat;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->subtotalEurVat = $value;
		return $this;
	}

	/**
	 * returns and sets price of delivery
	 * @param float $value
	 * @return float
	 */
	public function priceDelivery($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_priceDelivery = $value;
			}
		}
		return $this->_priceDelivery;
	}

	/**
	 * @return float
	 */
	public function getPriceDeliveryEur() {
		return $this->priceDeliveryEur;
	}

	/**
	 * @param type $priceDeliveryEur
	 * @return \Eshop\Orders\Order
	 */
	public function setPriceDeliveryEur($priceDeliveryEur) {
		$value = (float)$priceDeliveryEur;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->priceDeliveryEur = $value;
		return $this;
	}

	/**
	 * returns and sets payment fee
	 * @param float $value
	 * @return float
	 */
	public function pricePayment($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_pricePayment = $value;
			}
		}
		return $this->_pricePayment;
	}

	/**
	 * @return float
	 */
	public function getPricePaymentEur() {
		return $this->pricePaymentEur;
	}

	/**
	 * @param type $pricePaymentEur
	 * @return \Eshop\Orders\Order
	 */
	public function setPricePaymentEur($pricePaymentEur) {
		$value = (float)$pricePaymentEur;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->pricePaymentEur = $value;
		return $this;
	}

	/**
	 * returns and sets grand total without VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function grandTotal($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_grandTotal = $value;
			}
		}
		return $this->_grandTotal;
	}

	/**
	 * returns and sets grand total with VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function grandTotalVat($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_grandTotalVat = $value;
			}
		}
		return $this->_grandTotalVat;
	}

	/**
	 * @return float
	 */
	public function getGrandtotalEur() {
		return $this->grandtotalEur;
	}

	/**
	 * @param type $grandtotalEur
	 * @return Order
	 * @throws \InvalidArgumentException
	 */
	public function setGrandtotalEur($grandtotalEur) {
		$value = (float)$grandtotalEur;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->grandtotalEur = $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getGrandtotalEurVat() {
		return $this->grandtotalEurVat;
	}

	/**
	 * @param type $grandtotalEurVat
	 * @return Order
	 * @throws \InvalidArgumentException
	 */
	public function setGrandtotalEurVat($grandtotalEurVat) {
		$value = (float)$grandtotalEurVat;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->grandtotalEurVat = $value;
		return $this;
	}

	/**
	 * return and sets parcel number
	 * @param string $value
	 * @return string
	 */
	public function parcelNumber($value = null) {
		if (!is_null($value)) {
			$this->_parcelNr = Vars::substr($value, 0, 20);
		}
		return $this->_parcelNr;
	}

	/**
	 * returns and sets date when ordered items has been sent
	 * @param string $value
	 * @return Datetime|null
	 */
	public function dateSent($value = null) {
		if (!is_null($value)) {
			$value = trim($value);
			if ($value === '' || (strpos($value, '0000') !== false)) {
				$this->_dateSent = null;
			} else {
				$formattedDate = \GstLib\Date::dateTime2Iso($value, true);
				if ($formattedDate !== false) {
					$date = new \DateTime($formattedDate, new \DateTimeZone('UTC'));
					$this->_dateSent = $date;
				}
			}
		}
		return $this->_dateSent;
	}

	/**
	 * returns and sets price of delivery
	 * @param float $value
	 * @return float
	 */
	public function status($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if (in_array($value, array_keys(self::statuses()))) {
				$this->_status = $value;
			}
		}
		return $this->_status;
	}

	/**
	 * @return string
	 */
	public function getLang() {
		return $this->lang;
	}

	/**
	 * @param string $lang
	 * @return Order
	 */
	public function setLang($lang) {
		$this->lang = Vars::substr($lang, 0, 2);
		return $this;
	}

	/**
	 * fill instance from data tier
	 * @return bool false on error
	 */
	public function load() {
		$this->_loaded = $this->_dataLayer->load($this);
		return $this->_loaded;
	}

	/**
	 * save instance to data tier
	 * @param boolean $registerCustomer flag "registered new customer"
	 * @return bool false on error
	 */
	public function save($registerCustomer = false) {
		$this->_err = '';
		$required = array();
		if ($this->_firstName === '') {
			$required[] = '[%LG_FIRST_NAME%]';
		}
		if ($this->_lastName === '') {
			$required[] = '[%LG_LAST_NAME%]';
		}
		if ($this->_email === '') {
			$required[] = '[%LG_EMAIL%]';
		}
		if ($this->_phone === '') {
			$required[] = '[%LG_PHONE%]';
		}
		if ($this->_address === '') {
			$required[] = '[%LG_ADDRESS%]';
		}
		if ($this->_city === '') {
			$required[] = '[%LG_CITY%]';
		}
		if ($this->_postCode === '') {
			$required[] = '[%LG_POST_CODE%]';
		}
		if ($this->_useDeliveryAddress) {
			if ($this->_address === '') {
				$required[] = 'Dodací adresa';
			}
			if ($this->_city === '') {
				$required[] = 'Dodací adresa:město';
			}
			if ($this->_postCode === '') {
				$required[] = 'Dodací adresa:PSČ';
			}
		}
		if ($this->_paymentId === PaymentList::UNKNOWN) {
			$required[] = 'Způsob doručení';
		}
		if ($this->_delivererId === DeliveryList::UNKNOWN) {
			$required[] = 'Způsob doručení';
		}
		if (isset($required[0])) {
			$this->_err = '[%LG_ERR_MISSING_VALUES%] ' . join(', ', $required);
			return false;
		}
		if (!Html::isEmail($this->_email)) {
			$this->_err = '[%LG_ERR_INVALID_EMAIL_FORMAT%]';
			return false;
		}
		return $this->_dataLayer->save($this, $registerCustomer);
	}

	/**
	 * save instance to data tier from admin
	 * @return bool false on error
	 */
	public function saveAdmin() {
		$this->_err = '';
		return $this->_dataLayer->saveAdmin($this);
	}

	/**
	 * save instance to data tier from admin
	 * @return bool false on error
	 */
	public function saveSent() {
		$this->_err = '';
		return $this->_dataLayer->saveSent($this);
	}

	/**
	 * return items in oder
	 * @return array of instances \Eshop\Orders\Item
	 */
	public function getItems() {
		if (is_null($this->_items)) {
			$this->_loadItems();
		}
		return $this->_items;
	}

	/**
	 * fill order items with content of shopping cart
	 * @param \Eshop\Orders\Cart $cart
	 * @return bool false on error
	 */
	public function fill(\Eshop\Orders\Cart $cart) {
		$this->_err = '';
		$this->_items = null;
		foreach ($cart->getItems() as $tmp) {
			$item = new OrderItem();
			$item->orderId($this->_id);
			$item->productId($tmp->instanceId());
			$item->product($tmp->name());
			$item->quantity($tmp->quantity());
			$item->priceUnit($tmp->priceUnit(false, 'cs'));
			$item->priceUnitVat($tmp->priceUnit(true, 'cs'));
			$item->setPriceUnitEur($tmp->priceUnit(false, 'en'));
			$item->setPriceUnitEurVat($tmp->priceUnit(true, 'en'));
			$item->vat($tmp->vat());
			$item->setLang($this->getLang());
			$this->_items[] = $item;
		}
		return true;
	}

	/**
	 * returns subtotal in CZK - price of all products in order
	 * @param bool $withVat true=return price with VAT, false=without VAT
	 * @return float
	 */
	public function calcSubtotal($withVat = false) {
		$ret = 0;
		if (is_null($this->_items)) {
			$this->_loadItems();
		}
		foreach ($this->_items as $item) {
			$ret += $item->calcPriceRow($withVat);
		}
		if ($withVat) {
			$this->_subtotalVat = $ret;
		} else {
			$this->_subtotal = $ret;
		}
		return $ret;
	}

	public function calcSubtotalEur($withVat = false) {
		$ret = 0;
		if (is_null($this->_items)) {
			$this->_loadItems();
		}
		foreach ($this->_items as $item) {
			$ret += $item->calcPriceRowEur($withVat);
		}
		if ($withVat) {
			$this->subtotalEurVat = $ret;
		} else {
			$this->subtotalEur = $ret;
		}
		return $ret;
	}

	public function calcSubtotalByLang($withVat = false) {
		return ($this->lang === 'en' ? $this->calcSubtotalEur($withVat) : $this->calcSubtotal($withVat));
	}

	/**
	 * returns grand total of order in CZK (subtotal incl delivery and discount)
	 * @param bool $withVat true=return price with VAT, false=without VAT
	 * @return float
	 */
	public function calcGrandTotal($withVat = false) {
		$ret = $this->calcSubtotal($withVat) + $this->calcDeliveryPrice() + $this->calcPaymentPrice();
		return round($ret, 0, PHP_ROUND_HALF_UP);
	}

	public function calcGrandTotalEur($withVat = false) {
		$ret = $this->calcSubtotalEur($withVat) + $this->calcDeliveryPriceEur() + $this->calcPaymentPriceEur();
		return round($ret, 0, PHP_ROUND_HALF_UP);
	}

	public function calcGrandTotalByLang($withVat = false) {
		return ($this->lang === 'en' ? $this->calcGrandTotalEur($withVat) : $this->calcGrandTotal($withVat));
	}

	/**
	 * load items of order
	 */
	private function _loadItems() {
		$this->_items = $this->_dataLayer
			->getOrderItems(0, null, 'product', false, array(array('order_id', $this->instanceId())));
	}

	/**
	 * calculate delivery fee in CZK
	 * @return float
	 */
	public function calcDeliveryPrice() {
		$this->_priceDelivery = $this->_dataLayer->calcPriceDelivery($this, 'CZK');
		return $this->_priceDelivery;
	}

	/**
	 * calculate delivery fee in EUR
	 * @return float
	 */
	public function calcDeliveryPriceEur() {
		$this->priceDeliveryEur = $this->_dataLayer->calcPriceDelivery($this, 'EUR');
		return $this->priceDeliveryEur;
	}

	/**
	 * calculate payment fee
	 * @return float
	 */
	public function calcPaymentPrice() {
		$this->_pricePayment = $this->_dataLayer->calcPricePayment($this, 'CZK');
		return $this->_pricePayment;
	}

	/**
	 * calculate payment fee
	 * @return float
	 */
	public function calcPaymentPriceEur() {
		$this->pricePaymentEur = $this->_dataLayer->calcPricePayment($this, 'EUR');
		return $this->pricePaymentEur;
	}

	/**
	 * returns list of statuses
	 * @return array
	 */
	public static function statuses() {
		return array(
			self::STATUS_RECEIVED => '[%LG_ORDER_STATUS_RECEIVED%]',
			self::STATUS_STORNO => '[%LG_ORDER_STATUS_STORNO%]',
			self::STATUS_FINISHED => '[%LG_ORDER_STATUS_FINISHED%]',
			self::STATUS_PAID => '[%LG_ORDER_STATUS_PAID%]'
		);
	}

	/**
	 * returns name of order status
	 * @param int $statusId
	 * @return string
	 */
	public function statusName($statusId = null, $lang = 'cs') {
		if (is_null($statusId)) {
			$statusId = $this->status();
		}
		$options = self::statuses();
		if (isset($options[$statusId])) {
			return $options[$statusId];
		}
		return '';
	}

	/**
	 * @return bool
	 */
	public function isVatPricesRequired() {
		return $this->dic() === '' || $this->country() === 'CZ';
	}

}

/**
 * item in order
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class OrderItem {

	/** @var int instance identifier */
	protected $_id;

	/** @var int id of parent order */
	private $_orderId;

	/** @var int id of product */
	private $_productId;

	/** @var string product name */
	private $_product;

	/** @var int quantity */
	private $_quantity;

	/** @var float price of one unit without VAT in CZK */
	private $_priceUnit;

	/** @var float price of unit with VAT in CZK */
	private $_priceUnitVat;

	/** @var float percent of VAT */
	private $_vat;

	/** @var float price without VAT per row (price * quantity) in CZK */
	private $_priceRow;

	/** @var float price with VAT per row (price with VAT * quantity) in CZK */
	private $_priceRowVat;

	/** @var float */
	private $priceUnitEur;

	/** @var float */
	private $priceUnitEurVat;

	/** @var float */
	private $priceRowEur;

	/** @var float */
	private $priceRowEurVat;

	/** @var stgring */
	private $lang;

	public function __construct() {
		$this->_id = 0;
		$this->_orderId = 0;
		$this->_productId = 0;
		$this->_product = '';
		$this->_quantity = 0;
		$this->_priceUnit = $this->_priceUnitVat = 0;
		$this->_vat = 0;
		$this->_priceRow = $this->_priceRowVat = 0;
		$this->priceUnitEur = 0;
		$this->priceUnitEurVat = 0;
		$this->priceRowEur = 0;
		$this->priceRowEurVat = 0;
		$this->lang = 'cs';
	}

	/**
	 * returns and sets instance identifier
	 * @param int $value
	 * @return int
	 */
	public function instanceId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_id = $value;
			}
		}
		return $this->_id;
	}

	/**
	 * returns and sets identifier of parent order
	 * @param int $value
	 * @return int
	 */
	public function orderId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_orderId = $value;
			}
		}
		return $this->_orderId;
	}

	/**
	 * returns and sets identifier of product
	 * @param int $value
	 * @return int
	 */
	public function productId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_productId = $value;
			}
		}
		return $this->_productId;
	}

	/**
	 * returns and sets product name
	 * @param string $value
	 * @return string
	 */
	public function product($value = null) {
		if (!is_null($value)) {
			$this->_product = Vars::substr($value, 0, 50);
		}
		return $this->_product;
	}

	/**
	 * returns and sets quantity
	 * @param int $value
	 * @return int
	 */
	public function quantity($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_quantity = $value;
			}
		}
		return $this->_quantity;
	}

	/**
	 * returns and sets price of unit without VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function priceUnit($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_priceUnit = $value;
			}
		}
		return $this->_priceUnit;
	}

	/**
	 * returns and sets price of unit with VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function priceUnitVat($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_priceUnitVat = $value;
			}
		}
		return $this->_priceUnitVat;
	}

	/**
	 * returns and sets VAT percent
	 * @param float $value
	 * @return float
	 */
	public function vat($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_vat = $value;
			}
		}
		return $this->_vat;
	}

	/**
	 * returns and sets price of row without VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function priceRow($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_priceRow = $value;
			}
		}
		return $this->_priceRow;
	}

	/**
	 * returns and sets price of row with VAT in CZK
	 * @param float $value
	 * @return float
	 */
	public function priceRowVat($value = null) {
		if (!is_null($value)) {
			$value = (float)$value;
			if ($value > 0) {
				$this->_priceRowVat = $value;
			}
		}
		return $this->_priceRowVat;
	}

	/**
	 * @return float
	 */
	public function getPriceUnitEur() {
		return $this->priceUnitEur;
	}

	/**
	 * @param float $priceUnitEur
	 * @return OrderItem
	 */
	public function setPriceUnitEur($priceUnitEur) {
		$value = (float)$priceUnitEur;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->priceUnitEur = $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPriceUnitEurVat() {
		return $this->priceUnitEurVat;
	}

	/**
	 *
	 * @param float $priceUnitEurVat
	 * @return OrderItem
	 */
	public function setPriceUnitEurVat($priceUnitEurVat) {
		$value = (float)$priceUnitEurVat;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->priceUnitEurVat = $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPriceRowEur() {
		return $this->priceRowEur;
	}

	/**
	 * @param float $priceRowEur
	 * @return OrderItem
	 */
	public function setPriceRowEur($priceRowEur) {
		$value = (float)$priceRowEur;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->priceRowEur = $value;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPriceRowEurVat() {
		return $this->priceRowEurVat;
	}

	/**
	 * @param float $priceRowEurVat
	 * @return OrderItem
	 */
	public function setPriceRowEurVat($priceRowEurVat) {
		$value = (float)$priceRowEurVat;
		if ($value < 0) {
			throw new \InvalidArgumentException('expected float greater or equals zero');
		}
		$this->priceRowEurVat = $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLang() {
		return $this->lang;
	}

	/**
	 * @param string $lang
	 * @return Order
	 */
	public function setLang($lang) {
		$this->lang = Vars::substr($lang, 0, 2);
		return $this;
	}

	/**
	 * calculate price of item without VAT (price of unit*quantity) in CZK
	 * @param boolean $withVat = true cal price with VAT
	 * @return float
	 */
	public function calcPriceRow($withVat = false) {
		if ($withVat) {
			$this->_priceRowVat = $this->priceUnitVat() * $this->quantity();
			return $this->priceRowVat();
		}
		$this->_priceRow = $this->priceUnit() * $this->quantity();
		return $this->priceRow();
	}

	/**
	 * calculate price of item without VAT (price of unit*quantity) in CZK
	 * @param boolean $withVat = true cal price with VAT
	 * @return float
	 */
	public function calcPriceRowEur($withVat = false) {
		if ($withVat) {
			$this->priceRowEurVat = $this->getPriceUnitEurVat() * $this->quantity();
			return $this->getPriceRowEurVat();
		}
		$this->priceRowEur = $this->getPriceUnitEur() * $this->quantity();
		return $this->getPriceRowEur();
	}

	public function calcPriceRowByLang($withVat = FALSE) {
		return ($this->lang === 'en' ? $this->calcPriceRowEur($withVat) : $this->calcPriceRow($withVat));
	}

	/**
	 * @return float
	 */
	public function getPriceUnitByLang() {
		return ($this->lang === 'en' ? $this->getPriceUnitEur() : $this->priceUnit());
	}

	/**
	 * @return float
	 */
	public function getPriceUnitVatByLang() {
		return ($this->lang === 'en' ? $this->getPriceUnitEurVat() : $this->priceUnitVat());
	}

	/**
	 * @return float
	 */
	public function getPriceRowByLang() {
		return ($this->lang === 'en' ? $this->getPriceRowEur() : $this->priceRow());
	}

	/**
	 * @return float
	 */
	public function getPriceRowVatByLang() {
		return ($this->lang === 'en' ? $this->getPriceRowEurVat() : $this->priceRowVat());
	}

}
