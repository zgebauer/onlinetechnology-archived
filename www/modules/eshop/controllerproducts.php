<?php
/**
 * @package    Ergo
 * @subpackage Eshop
 */

namespace Eshop;

use Ergo\ApplicationInterface;
use Eshop\Products\Models3d\Service as Service3d;
use GstLib\Template;
use GstLib\Html;
use Ergo\Request;
use Ergo\Pagination;
use Eshop\Products\Image;
use Eshop\Products\GalleryImage;

/**
 * controller for products
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class ControllerProducts
{

	/**
	 * @var Service3d
	 */
	private $service3d;

	/**
	 * @param ApplicationInterface $application current application
	 * @param Service3d $service3d
	 */
	public function __construct(ApplicationInterface $application, Service3d $service3d) {
		$this->_app = $application;
		$this->service3d = $service3d;
	}

	/**
	 * returns box with with tree of categories
	 * @return string
	 */
	public function renderBoxCategories()
	{
		$tpl = new Template($this->_app->getTemplatePath('box_categories.htm', Module::NAME), true);
		$cnt = $this->_getTree(0);
		if ($cnt === '') {
			return '';
		}
		$tpl->replaceSub('LEVEL', $cnt);
		return $tpl->get();
	}

	/**
	 * recursive function, create tree of items
	 *
	 * @param int $categoryId id of current tree node
	 * @return string
	 */
	private function _getTree($categoryId = 0)
	{
		static $levelTemplate;
		static $itemTemplate;

		$tplLevel = new Template();
		$tplItem = new Template();

		if (!isset($levelTemplate) || !isset($itemTemplate)) {
			$tpl = new Template($this->_app->getTemplatePath('box_categories.htm', Module::NAME), true);
			$levelTemplate = $tpl->getSub('LEVEL');
			$itemTemplate = $tpl->getSub('ITEM');
		}

		static $categoriesList;
		if (!isset($categoriesList) || defined('ERGO_TESTING')) {
			$categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');
		}

		$cnt = '';
		$tplLevel->set($levelTemplate);
		$counter = 0;
		foreach ($categoriesList->getCategories($categoryId) as $item) {
			if (!$item->published()) {
				continue;
			}
			$tplItem->set($itemTemplate);
			$tplItem->replace('TXT_ITEM', $item->getNameByLang($this->_app->currentLanguage()));
			$tplItem->replace('TXT_ITEM_ID', $item->instanceId());
			$url = $this->_app->getUrl(Module::NAME, 'kategorie/'.$item->seo());
			$tplItem->replace('URL_ITEM', $url);
			$cssSelected = $this->_app->getCurrentUrl() === $url ? ' selected' : '';
			$tplItem->replace('CSS_SELECTED', $cssSelected);
			$tplItem->replace('INC_SUBLEVEL', $this->_getTree($item->instanceId()), 'raw');
			$cnt .= $tplItem->get();
			$counter++;
		}
		if ($counter === 0) {
			return '';
		}
		$tplLevel->replaceSub('ITEM', $cnt);
		return $tplLevel->get();
	}

	/**
	 * render pagewith products in category
	 * @param string $seo category seo
	 * @return void
	 */
	public function renderCategory($seo)
	{
		$record = $this->_app->dice()->create('\Eshop\Products\DataCategory')->getRecordBySeo($seo);
		if (is_null($record) || !$record->published()) {
			$this->_app->render404();
			return;
		}

		$lang = $this->_app->currentLanguage();
		$pos = $this->_app->request()->getValue(Pagination::POS_VARIABLE, Request::GET, 'int');
		$pos = ($pos < 1 ? 1 : $pos);
		$rowsPage = 9;

		$poradi = $this->_app->request()->getValue('poradi', Request::GET);
		if (!in_array($poradi, array('nazev', 'cena'))) {
			$poradi = 'nazev';
		}
		$orderBy = ($poradi === 'cena' ? 'price' : 'name');
		$desc = $this->_app->request()->getValue('sest', Request::GET, 'bool');

		$options['filterCategory'] = $record->instanceId();

		$criteria = array();
		$criteria[] = array('publish', 1);

		$tmp = array('category_id='.$record->instanceId());
		$categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');
		foreach ($categoriesList->getChildrenIds($record->instanceId()) as $catId) {
			$tmp[] = 'category_id='.$catId;
		}
		$criteria[] = array('('.join(' OR ', $tmp).')');

		$filterIcons = $this->_app->request()->getValue('funkce', Request::GET, 'array');
		$filterIcons = array_map('intval', $filterIcons);
		if (isset($filterIcons[0])) {
			$tmp = array();
			foreach ($filterIcons as $iconId) {
				$tmp[] = 'ri.icon_id='.$iconId;
			}
			$criteria[] = array('('.join(' OR ', $tmp).')');
		}

		$linkSelf = $this->_app->getUrl(Module::NAME, 'kategorie/'.$record->seo());

		$queryParams['pos'] = ($pos > 1 ? $pos : NULL);
		$queryParams['poradi'] = ($poradi === 'nazev' ? NULL : $poradi);
		$queryParams['sest'] = ($desc === 1 ? $desc : 0);
		$queryParams['funkce'] = (isset($filterIcons[0]) ? $filterIcons : NULL);

		$linkPos = Html::queryUrl($linkSelf, $queryParams, array(Pagination::POS_VARIABLE=>null));

		$tpl = new Template($this->_app->getTemplatePath('category.htm', Module::NAME), true);
		$tpl->replace('INC_BREADCRUMBS', $this->_renderBreadcrumbsCategory($record), 'raw');
		$tpl->replace('TXT_NAME', $record->getNameByLang($this->_app->currentLanguage()));
		$tpl->replace('TXT_DESCRIPTION', Html::urlToAbs($record->text(), $this->_app->baseUrl()), 'raw');

		$tpl->replace('URL_FORM', Html::queryUrl($linkPos, array('funkce[]'=>NULL)));

		// icons
		$cnt = '';
		$rowTemplate = $tpl->getSub('ITEMS_ICONS');
		$tplRow = new Template();
		$icons = $this->_app->dice()->create('\Eshop\Products\DataIcon', array($this->_app))
			->getNonEmptyIcons($options['filterCategory']);
		foreach ($icons as $icon) {
			$tplRow->set($rowTemplate);
			$tplRow->replace('TXT_ICON_NAME', $icon->getNameBylang($lang));
			$tplRow->replace('INPVAL_ICON_ID', $icon->instanceId());
			$tplRow->replace('INPCHECK_ICON', Html::isChecked(in_array($icon->instanceId(), $filterIcons)), 'raw');
			$url = $icon->getMainImage()->getAttachmentUrl(Products\IconImage::IMG_ORIG);
			$tplRow->replace('URL_ITEM_IMG', $url);
			$cnt .= $tplRow->get();
		}
		unset($tplRow);
		$tpl->replaceSub('ITEMS_ICONS', $cnt);
		$tpl->showSub('FILTER', $cnt !== '');

		$tmp = (isset($filterIcons[0]) ? $filterIcons : NULL);
		$tpl->replace(
			'URL_ORDER_NAME_ASC', Html::queryUrl($linkSelf, array('funkce'=>$tmp, 'poradi'=>'nazev', 'sest'=>0)), 'raw'
		);
		$tpl->replace(
			'URL_ORDER_NAME_DESC', Html::queryUrl($linkSelf, array('funkce'=>$tmp, 'poradi'=>'nazev', 'sest'=>1)), 'raw'
		);
		$tpl->replace(
			'URL_ORDER_PRICE_ASC', Html::queryUrl($linkSelf, array('funkce'=>$tmp, 'poradi'=>'cena', 'sest'=>0)), 'raw'
		);
		$tpl->replace(
			'URL_ORDER_PRICE_DESC', Html::queryUrl($linkSelf, array('funkce'=>$tmp, 'poradi'=>'cena', 'sest'=>1)), 'raw'
		);

		$products = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app))
				->getRecords($pos-1, $rowsPage, $orderBy, $desc, $criteria);
		$tpl->replace('INC_PRODUCTS', $this->_renderList($products), 'raw');

		$sum = $this->_app->dice()->create('\Eshop\Products\DataProduct')->count($criteria);
		$tpl->replace('INC_PAGES', $this->_app->renderPagination($sum, $pos, $linkPos, $rowsPage), 'raw');

		$this->_app->response()->pageTitle($record->name().' | [%LG_SITENAME%]');
		$this->_app->response()->metaDescription($record->metaDescription());
		$this->_app->response()->metaKeywords($record->metaKeywords());
		$this->_app->response()->canonicalUrl($this->_app->getUrl(Module::NAME, 'kategorie/'.$record->seo()));
		$this->_app->response()->pageContent($tpl->get());
	}

	private function _renderList(array $products)
	{
		$cnt = '';
		foreach ($products as $record) {
			$cnt .= $this->_renderListItem($record);
		}
		return $cnt;
	}

	private function _renderListItem (\Eshop\Products\Product $record)
	{
		static $tplRow, $tplRowIcons;
		static $rowTemplate, $rowTemplateIcons;

		$lang = $this->_app->currentLanguage();

		if (!isset($tplRow)) {
			$tplRow = new Template($this->_app->getTemplatePath('list_item.htm', Module::NAME), true);
			$rowTemplate = $tplRow->get();
			$tplRowIcons = new Template();
			$rowTemplateIcons = $tplRow->getSub('ITEMS_ICONS');
		}

		$tplRow = new Template();
		$tplRow->set($rowTemplate);

		$tplRow->set($rowTemplate);
		$tplRow->replace('TXT_ITEM', $record->getNameByLang($lang));
		$tplRow->replace('URL_ITEM', Html::queryUrl($this->_app->getUrl(Module::NAME, 'zbozi/'.$record->seo())));
		$tplRow->replace('INPVAL_ITEM_ID', $record->instanceId());
		$tplRow->replace('TXT_CATEGORY_ID', $record->categoryId());
		$tplRow->replace('TXT_ITEM_PRICE_VAT', \GstLib\Vars::formatCurrency($record->getPriceVatByLang($lang), $this->_app->getCurrency(), 0), 'raw');
		$tplRow->replace('TXT_ITEM_PRICE_VAT_U', $record->getPriceVatByLang($lang));
		$tplRow->replace('INP_CURRENCY', $this->_app->getCurrency());

		// img
		$tplRow->replace('TXT_FACADE', $this->_app->currentFacade());
		$url = $record->getMainImage()->getAttachmentUrl(Image::IMG_THUMB);
		$tplRow->replace('URL_ITEM_IMG', $url);
		$tplRow->showSub('IMG', $url !== '');
		$tplRow->showSub('IMG_DEFAULT', $url === '');

		// icons
		$cnt = '';
		foreach ($record->getIcons() as $icon) {
			$tplRowIcons->set($rowTemplateIcons);
			$url = $icon->getMainImage()->getAttachmentUrl(Products\IconImage::IMG_ORIG);
			$tplRowIcons->replace('TXT_ITEM_ICON', $icon->getNameByLang($lang));
			$tplRowIcons->replace('URL_ITEM_IMG', $url);
			$cnt .= $tplRowIcons->get();
		}
		$tplRow->replaceSub('ITEMS_ICONS', $cnt);

		$model3d = $this->service3d->getFile($record->instanceId());
		$tplRow->showSub('ICON_3D', $model3d->getUrl() !== '');
		return $tplRow->get();
	}

	/**
	 * render page with detail of product
	 * @param string $seo od product
	 * @return void
	 */
	public function renderProduct($seo)
	{
		/** @var \Eshop\Products\Product */
		$record = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app))->getRecordBySeo($seo);
		if (is_null($record) || !$record->published()) {
			$this->_app->render404();
			return;
		}

		$model3d = $this->service3d->getFile($record->instanceId());

		$lang = $this->_app->currentLanguage();

		$tpl = new Template($this->_app->getTemplatePath('product.htm', Module::NAME), true);

		$tpl->replace('INC_BREADCRUMBS', $this->_renderBreadcrumbs($record), 'raw');

		$tpl->replace('TXT_NAME', $record->getNameByLang($lang));
		$tpl->replace('TXT_DESCRIPTION', Html::urlToAbs($record->getTextByLang($lang), $this->_app->baseUrl()), 'raw');
		$tpl->replace('TXT_PRICE', \GstLib\Vars::formatNumberHuman($record->getPriceByLang($lang), 2), 'raw');
		$tpl->replace('TXT_PRICE_VAT', \GstLib\Vars::formatNumberHuman($record->getPriceVatByLang($lang), 2), 'raw');
		$tpl->replace('TXT_PRICE_VAT_U', $record->getPriceVatByLang($lang));
		$tpl->replace('TXT_CATEGORY_ID', $record->categoryId());
		$tpl->replace('INP_CURRENCY', $this->_app->getCurrency());

		$tpl->replace(
			'TXT_POPIS_MODULU', Html::urlToAbs($record->getPopisModuluByLang($lang), $this->_app->baseUrl()), 'raw'
		);
		$tpl->replace('TXT_ZAPOJENI', Html::urlToAbs($record->getZapojeniByLang($lang), $this->_app->baseUrl()), 'raw');
		$tpl->replace(
			'TXT_DOKUMENTACE', Html::urlToAbs($record->getDokumentaceByLang($lang), $this->_app->baseUrl()), 'raw'
		);
		$tpl->replace(
			'TXT_FIRMWARE', Html::urlToAbs($record->getFirmwareByLang($lang), $this->_app->baseUrl()), 'raw'
		);
		$tpl->replace('TXT_PROGRAMY', Html::urlToAbs($record->getProgramyByLang($lang), $this->_app->baseUrl()), 'raw');
		$tpl->replace('TXT_STOCK_INFO', $record->getStockInfoByLang($lang));

		// cart
		$tpl->replace('INPVAL_ID', $record->instanceId());

		// img
		$tpl->replace('TXT_FACADE', $this->_app->currentFacade());
		$url = $record->getMainImage()->getAttachmentUrl(\Eshop\Products\Image::IMG_THUMB2);
		$urlLarge = $record->getMainImage()->getAttachmentUrl(\Eshop\Products\Image::IMG_ORIG);
		$tpl->replace('URL_IMG_LARGE', $urlLarge);
		$tpl->replace('URL_IMG', $url);
		$tpl->showSub('IMG', $url !== '');
		$tpl->showSub('IMG_DEFAULT', $url === '');

		// icons
		$cnt = '';
		$rowTemplate = $tpl->getSub('ITEMS_ICONS');
		$tplRow = new Template();
		foreach ($record->getIcons() as $icon) {
			$tplRow->set($rowTemplate);
			$url = $icon->getMainImage()->getAttachmentUrl(Products\IconImage::IMG_ORIG);
			$tplRow->replace('TXT_ITEM', $icon->getNameByLang($lang));
			$tplRow->replace('URL_ITEM_IMG', $url);
			$tplRow->replace('URL_ITEM', $this->_app->getUrl(Module::NAME, 'ikona/'.$icon->instanceId()), 'raw');
			$cnt .= $tplRow->get();
		}
		$tpl->replaceSub('ITEMS_ICONS', $cnt);

		// gallery images
		$cnt = '';
		$rowTemplate = $tpl->getSub('ITEMS_IMAGES');
		foreach ($record->getImages() as $image) {
			$tplRow->set($rowTemplate);
			$url = $image->getAttachmentUrl(GalleryImage::IMG_THUMB);
			$urlLarge = $image->getAttachmentUrl(GalleryImage::IMG_ORIG);

			if ($url === '' /*|| $urlLarge === ''*/) {
				continue;
			}
			$tplRow->replace('TXT_ITEM', $image->description());
			$tplRow->replace('URL_ITEM_IMG', $url);
			$tplRow->replace('URL_ITEM_IMG_LARGE', $urlLarge);
			$cnt .= $tplRow->get();
		}
		unset($tplRow);
		$tpl->replaceSub('ITEMS_IMAGES', $cnt);

		// variants  products
		$cnt = '';
		foreach ($record->getVariantProducts() as $product) {
			$cnt .= '<li>'.$this->_renderListItem($product).'</li>';
		}
		$cnt = ($cnt === '' ? '' : '<ul id="variants">'.$cnt.'</ul>');
		$tpl->replace('INC_VARIANTS', $cnt, 'raw');
		$tpl->showSub('VARIANTS', $cnt !== '');

		// cross-sell products
		$cnt = '';
		foreach ($record->getCrossSellProducts() as $product) {
			$cnt .= '<li>'.$this->_renderListItem($product).'</li>';
		}
		$cnt = ($cnt === '' ? '' : '<ul id="crossSell">'.$cnt.'</ul>');
		$tpl->replace('INC_CROSS_SELL', $cnt, 'raw');
		$tpl->showSub('CROSS_SELL', $cnt !== '');

		$tpl->replace('URL_3D', $model3d->getUrl());
		$tpl->showSub('3D', $model3d->getUrl() !== '');

		$this->_app->response()->pageTitle($record->name().' | [%LG_SITENAME%]');
		$this->_app->response()->metaDescription($record->metaDescription());
		$this->_app->response()->metaKeywords($record->metaKeywords());
		$this->_app->response()->canonicalUrl($this->_app->getUrl(Module::NAME, 'zbozi/'.$record->seo()));
		$this->_app->response()->pageContent($tpl->get());
	}

	private function _renderBreadcrumbs(\Eshop\Products\Product $record)
	{
		$items = array();
		$categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');

		$parents = $categoriesList->getParentCategories($record->categoryId());

		foreach ($parents as $category) {
			$items[] = '<a href="'.
				$this->_app->getUrl(Module::NAME, 'kategorie/'.$category->seo()).
				'">'.Html::escape($category->getNameByLang($this->_app->currentLanguage())).'</a>';
		}

		$category = $this->_app->dice()->create('\Eshop\Products\Category', array($record->categoryId()));
		$items[] = '<a href="'.
			$this->_app->getUrl(Module::NAME, 'kategorie/'.$category->seo()).
			'">'.Html::escape($category->getNameByLang($this->_app->currentLanguage())).'</a>';

		$items[] = $record->getNameByLang($this->_app->currentLanguage());
		return join(' / ', $items);
	}

	/**
	 * render breadcrumb in category page
	 * @staticvar \Eshop\Products\Categories $categoriesList
	 * @param \Eshop\Products\Category $record
	 * @return string
	 */
	private function _renderBreadcrumbsCategory(\Eshop\Products\Category $record)
	{
		$items = array();

		$categoriesList = $this->_app->dice()->create('\Eshop\Products\Categories');

		$parents = $categoriesList->getParentCategories($record->instanceId());
		foreach ($parents as $category) {
			$items[] = '<a href="'.
				$this->_app->getUrl(Module::NAME, 'kategorie/'.$category->seo()).
				'">'.Html::escape($category->getNameByLang($this->_app->currentLanguage())).'</a>';
		}
	   $items[] = $record->getNameByLang($this->_app->currentLanguage());
	   return join(' / ', $items);
	}

	/**
	 * render box with products on home
	 * @return string
	 */
	public function renderBoxHome()
	{
		$criteria = array();
		$criteria[] = array('publish', 1);
		$criteria[] = array('bestseller', 1);

		$products = $this->_app->dice()->create('\Eshop\Products\DataProduct', array($this->_app))
				->getRecords(0, 12, 'name', FALSE, $criteria);
		return $this->_renderList($products);
	}

	/**
	 * render page with detail of product icon
	 * @param int $iconId
	 * @return void
	 */
	public function renderIcon($iconId)
	{
		/** @var \Eshop\Products\Product */
		$record = $this->_app->dice()->create('\Eshop\Products\Icon', array($iconId));
		if (is_null($record)) {
			$this->_app->render404();
			return;
		}

		$lang = $this->_app->currentLanguage();
		$tpl = new Template($this->_app->getTemplatePath('icon.htm', Module::NAME), true);

		$tpl->replace('TXT_NAME', $record->getNameByLang($lang));
		$tpl->replace('TXT_DESCRIPTION', Html::urlToAbs($record->getTextByLang($lang), $this->_app->baseUrl()), 'raw');

		$this->_app->response()->pageTitle($record->name().' | [%LG_SITENAME%]');
		$this->_app->response()->metaDescription($record->name());
		$this->_app->response()->canonicalUrl($this->_app->getUrl(Module::NAME, 'ikona/'.$record->instanceId()));
		$this->_app->response()->pageContent($tpl->get());
	}

}