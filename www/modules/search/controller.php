<?php
/**
 * @package    Ergo
 * @subpackage Search
 */

namespace Search;

use Ergo\Request;
use Ergo\Response;
use GstLib\Vars;
use GstLib\Html;
use GstLib\Template;
use Ergo\Pagination;

/**
 * controller
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Eshop
 */
class Controller
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * returns box with small search form
     * @return string
     */
    public function renderBox()
    {
        $tpl = new Template($this->_app->getTemplatePath('box.htm', Module::NAME), true);
        $tpl->replace('URL_SUGGEST', Html::queryUrl($this->_app->getUrl(Module::NAME, 'ajax-suggest')), 'raw');
        return $tpl->get();
    }

    /**
     * returns list of records
     */
    public function renderList()
    {
        $query = $this->_app->request()->getValue('q', Request::GET);
        if ($query === '') {
            $this->_app->response()->redirect($this->_app->baseUrl());
            return;
        }

        $pos = $this->_app->request()->getValue(Pagination::POS_VARIABLE, 'get', 'int');
        $pos = ($pos < 1 ? 1 : $pos);
        $rowsPage = 8;

        $queryParams['q'] = $query;
        $linkSelf = Html::queryURL($this->_app->getUrl(Module::NAME), $queryParams);
        $linkPos = Html::queryURL($linkSelf, array(Pagination::POS_VARIABLE=>null));

        $criteria[] = array('text', $query, 'like');

        $tpl = new Template($this->_app->getTemplatePath('list.htm', Module::NAME), true);
        $tpl->replace(
            'INC_PAGES', $this->_app->renderPagination(
                $this->_app->dice()->create('\Search\Data')->count($criteria), $pos, $linkPos, $rowsPage
            ), 'raw'
        );
        $tpl->replace('INPVAL_SEARCH', $query);

        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('ITEMS');
        $queryRegexp = "/(".preg_quote($query, '/').")/Ui";
        foreach ($this->_app->dice()->create('\Search\Data')
                ->getResults($pos-1, $rowsPage, 'name', false, $criteria) as $row) {
            $tplRow->set($rowTemplate);
            $row['name'] = preg_replace($queryRegexp, "<span class=\"highlight\">$1</span>", $row['name']);
            $tplRow->replace('TXT_ITEM', $row['name'], 'raw');
            $tplRow->replace('URL_ITEM', $row['url']);

            // popis je ukazka textu
            $pos = mb_strpos($row['text'], $query);
            $start = $pos - 100;
            if ($start < 0) {
                $start = 0;
            }

            $length = mb_strlen($query) + 200;
            $perex = ($start > 0 ? '...' : '');
            $perex .= mb_substr($row['text'], $start, $length, 'UTF-8');
            if (($pos + $length) < mb_strlen($row['text'])) {
                $perex .= '...';
            }
            // zvyrazneni hledaneho slova
            $perex = preg_replace($queryRegexp, "<span class=\"highlight\">$1</span>", $perex);
            $tplRow->replace('TXT_PEREX', $perex, 'raw');

            $cnt .= $tplRow->get();
        }
        unset($tplRow);
        $tpl->replaceSub('ITEMS', $cnt);
        $tpl->showSub('NOT_FOUND', $cnt == '');


        $this->_app->response()->pageContent($tpl->get());
        $this->_app->response()->pageTitle('Vyhledávání | [%LG_SITENAME%]');
    }

    /**
     * render product names for autocomplete. Expacts
     * $_GET[query] - part od product name
     */
    public function handleSuggest()
    {
        $term = $this->_app->request()->getQuery('query');
        $ret = array();
        if ($term !== '') {
            $ret['query'] = $term;
            $ret['suggestions'] = $this->_app->dice()->create('\Search\Data')->getSuggest($term);
        }
        $this->_app->response()->setOutputJson($ret);
    }

}