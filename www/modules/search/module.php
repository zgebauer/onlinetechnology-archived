<?php
/**
 * @package    Ergo
 * @subpackage Search
 */

namespace Search;


/**
 * main class of module Search
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Search
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'search';

    public function __construct(\Ergo\ApplicationInterface $application)
    {
        parent::__construct($application);

        $this->_app->setShared('\Eshop\Search\Data');
    }

    public function output($parameter = '')
    {
        $map = array(
            '_box_' => array('\Search\Controller', 'renderBox'),
            '' => array('\Search\Controller', 'renderList'),
            'ajax-suggest' => array('\Search\Controller', 'handleSuggest'),
        );

        if (isset($map[$parameter])) {
            return call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
        }

        if (substr($parameter, 0, 5) === '_url_') {
            return $this->_app->getUrl(self::NAME, substr($parameter, 5));
        }

//        case '':
//            $this->_renderList();
//            return;
////                $query = Gst_Html::getGlobal('q', 'get');
////                $this->_meta['title'] = '[]$record->name);
////                    $this->_meta['description'] =
////                        array($record->metaDescription);
////                    $this->_meta['keywords'] = array($record->metaKeywords);
////                    $this->_currentArticleId = $record->id;
////                return Ergo_Core::factory('Search_Gui')
////                    ->renderResults($query, Ergo_Core::getCurrentLanguage());
//        case 'ajax-suggest':
//            $this->_renderSuggest();
//            return;
//        }
        return parent::output($parameter);
    }

    public function name()
    {
        return 'Search';
    }

    public function cron()
    {
        // once a day refresh search index
//        $fullPath = ADIR_ROOT . 'log/search_cron.lastrun';
//        $lastRun = '';
//        if (is_file ( $fullPath )) {
//            $lastRun = trim ( file_get_contents ( $fullPath ) );
//        }
//        if ($lastRun != date ( 'Y-m-d' )) {
//            if (CDataModuleSearch::refreshIndex()) {
//                file_put_contents ( $fullPath, date ( 'Y-m-d' ) );
//            }
//        }

        // once a day refresh search index
        $fullPath = $this->_app->config()->baseDirLogCron().self::NAME.'_search.lastrun';
        $lastRun = '';
        if (is_file($fullPath)) {
            $lastRun = trim(file_get_contents($fullPath));
        }
        if ($lastRun != date('Y-m-d')) {
            $this->_app->dice()->create('\Search\Data')->refreshSearchIndex();
            file_put_contents($fullPath, date('Y-m-d'));
        }
    }

}
