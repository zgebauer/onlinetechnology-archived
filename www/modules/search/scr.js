'use strict';

$(document).ready(function(){

    $('#frmQ, #frmSearch').submit(function() {
        return ($.trim($(this).find('input[type="text"]').val()) !== '');
    });

    $('#frmQ input:text, #frmSearch input:text').autocomplete({
        serviceUrl: ERGO_ROOT_URL+'search/ajax-suggest',
        minChars: 2,
        onSelect: function (suggestion) {
            $(this).closest('form').submit();
        }
    });
});