<?php
/**
 * @package    Ergo
 * @subpackage Search
 */

namespace Search;

/**
 * data tier for module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Search
 */
class Data
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%core_search_index%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * returns results
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getResults($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT url,name,text FROM '.self::$_tableName
            .$this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        return $this->_con->fetchArrayAll($sql);
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array())
    {
        return $this->_con->count('SELECT COUNT(*) FROM '.self::$_tableName.$this->_con->where($criteria));
    }

    /**
     * read url from search index and parse text content from pages
     */
    public function refreshSearchIndex()
    {
        $sql = 'SELECT url FROM '.self::$_tableName.' ORDER BY last_index ASC LIMIT 50';
        $urls = $this->_con->fetchColAll('url', $sql);
        foreach ($urls as $url) {
            $url = str_replace('&amp;', '&', $url);
            $cnt = @file_get_contents($url);
            $text = '';
            if ($cnt !== false) {
                $text = self::_parseText($cnt);
            }

            $date = new \DateTime('now', new \DateTimeZone('UTC'));
            $sql = 'UPDATE '.self::$_tableName.'
               SET text='.$this->_con->escape($text).',
               last_index='.$this->_con->escape($date->format('Y-m-d H:i:s'))
               .' WHERE url='.$this->_con->escape($url);
            $this->_con->query($sql);
        }
    }

    private static function _parseText($page)
    {
        $tmp = array();
        $ret = '';

        //$customTag = Ergo_Module_Search::getConfig()->getIndexMark();
        $customTag = 'FULLTEXT';
        //$customTag = '';

        if ($customTag == '') {
            $regexp = '/<body>(.*)<\/body>/sm';
            if (preg_match($regexp, $page, $tmp)) {
                $ret = $tmp[1];
            }
        } else {
            $regexp = '/<!--<'.$customTag.'>-->(.*)<!--<\/'.$customTag.'>-->/smU';
            $occurences = array();
            preg_match_all($regexp, $page, $occurences, PREG_PATTERN_ORDER);
            $ret = join(' ', $occurences[0]);
        }

        if ($ret != '') {
            $ret = strip_tags($ret);
            $ret = str_replace('&nbsp;', ' ', $ret);
            $ret = str_replace("\n", ' ', $ret);
            $ret = str_replace("\r", ' ', $ret);
            $ret = str_replace("\t", ' ', $ret);

            $ret = strip_tags($ret);
            while (strpos($ret, '  ') !== false) {
                $ret = str_replace('  ', ' ', $ret); // remove double spaces
            }
            $ret = trim($ret);
        }
        return $ret;
    }

     /**
     * return suggestions for ajax suggest
     * @param string $text string from search box
     * @return string
     */
    public function getSuggest($text)
    {
        $sql = 'SELECT name  FROM [%eshop_products%]
            WHERE name LIKE '.$this->_con->escape('%'.$text.'%').' AND publish='.$this->_con->boolToSql(true)
            .' ORDER BY name '.$this->_con->limit(0, 10);
        return $this->_con->fetchColAll('name', $sql);
    }
}