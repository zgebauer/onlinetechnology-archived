<?php
/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

require_once 'data_sitemap.php';

/**
 * main class of module Staticarticles
 * Management of static articles
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'staticarticles';

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        parent::__construct($application);

        $this->_app->setShared('\Staticarticles\Config');
        $this->_app->setShared('\Staticarticles\Data');
        $this->_app->setShared('\Staticarticles\DataSitemap');
    }

    public function menuItems()
    {
        $user = $this->_app->getUser();
        if (is_null($user) || !$user->hasPermission(self::NAME)) {
            return null;
        }
        return array(
            array(
               'name' => $this->name(),
               'url' => $this->_app->getUrl(self::NAME),
               'title' => 'Seznam statických článků',
               'highlight' => ($this->_app->getCurrentModule() == self::NAME)
            )
        );
    }

    public function permissions()
    {
        return array();
    }

    public function output($parameter = '')
    {
        $seo = Article::seoStrings();

        switch($parameter) {
        default:
            if (substr($parameter, 0, 8) === '_urlseo_') {
                $articleId = trim(substr($parameter, 8));
                return (isset($seo[$articleId]) ? $this->_app->getUrl(self::NAME, $seo[$articleId])  : '');
            }
            if (substr($parameter, 0, 5) === '_box_') {
                return $this->_renderArticleAsBox(intval(substr($parameter, 5)));
            }
            if ($parameter != '') {
                $tmp = array_flip($seo);
                if (isset($tmp[$parameter])) {
                    $this->_renderArticle($tmp[$parameter], $parameter);
                    return;
                }
            }
        }
        return parent::output($parameter);
    }

    public function outputAdmin($parameter = '')
    {
        $map = array(
            '' => array('\Staticarticles\Admin\Controller', 'renderList'),
            'ajax-list' => array('\Staticarticles\Admin\Controller', 'handleList'),
            'ajax-edit' => array('\Staticarticles\Admin\Controller', 'handleEdit'),
            'ajax-save' => array('\Staticarticles\Admin\Controller', 'handleSave')
        );

        if (isset($map[$parameter])) {
            return call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
        }
        return parent::outputAdmin($parameter);
    }

    public function name()
    {
        return 'Statické články';
    }

    public function refreshSitemapLinks()
    {
        $this->_app->dice()->create('\Staticarticles\DataSitemap', array($this->_app))->refreshLinks();
    }

    private function _renderArticle($articleId, $seo)
    {
         $record = $this->_app->dice()->create('\StaticArticles\Article', array($articleId));

        $this->_app->response()->pageTitle($record->name().' | [%LG_SITENAME%]');
        $this->_app->response()->metaDescription($record->metaDescription());
        $this->_app->response()->metaKeywords($record->metaKeywords());
        $this->_app->response()->canonicalUrl($this->_app->getUrl(self::NAME, $seo));
        $this->_app->response()->setMetaProperty('og:type', 'article');

        $gui = new Gui($this->_app);
        $this->_app->response()->pageContent($gui->renderArticle($record));
    }

    private function _renderArticleAsBox($articleId)
    {
        $record = $this->_app->dice()->create('\StaticArticles\Article', array($articleId));
        $gui = new Gui($this->_app);
        return $gui->renderArticleAsBox($record);
    }

}