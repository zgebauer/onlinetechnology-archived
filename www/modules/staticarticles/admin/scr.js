'use strict';

Ergo.Staticarticles = Ergo.Staticarticles || {};

Ergo.Staticarticles.Admin = function() {
    var nodeList = $('#staticarticlesList');
    var nodeDialog = $('#dialog');
    var editRecord;
    var nodeMsg;
    var editorInstances;
    var selectedTab = 0;

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/staticarticles/ajax-list';
    recordsList.itemTemplate = '<tr><td>%name%</td><td>%note%</td>'
        + '<td data-item-edit="%id%" class="clickable edit">Upravit</td></tr>';
    recordsList.orderBy = 'name';
    recordsList.orderDesc = 0;
    recordsList.callbackLoaded = function(list, data) {
        pagination.setPages(list.pages);
        positions.update(list.pages);
    };
    recordsList.load();

    var pagination = new Ergo.RecordsList.Pagination($('[data-pos]'), recordsList);
    var positions = new Ergo.RecordsList.Positions(recordsList, $('[data-pos-start]'), $('[data-pos-end]'), $('[data-pos-total]'));
    var rowsPage = new Ergo.RecordsList.RowsPage($('.pagination [data-rows-page]'), recordsList);
    var formFilter = new Ergo.RecordsList.Filter($('.frmFilter'), recordsList);

    var renderEditForm = function() {

        nodeDialog.html($('#detailEdit').html());
        nodeMsg = nodeDialog.find('.msg');

       //if (record !== undefined) {
            nodeDialog.find('input[name="id"]').val(editRecord.id);
            nodeDialog.find('input[name="name"]').val(editRecord.name);
            nodeDialog.find('input[name="name_en"]').val(editRecord.name_en);
            nodeDialog.find('textarea[name="text"]').val(editRecord.text);
            nodeDialog.find('textarea[name="text_en"]').val(editRecord.text_en);
            nodeDialog.find('input[name="meta_desc"]').val(editRecord.meta_desc);
            nodeDialog.find('input[name="meta_keys"]').val(editRecord.meta_keys);
        //}

        nodeDialog.find('input[id], textarea[id]').each(function() {
            $(this).prop('id', 'edit_'+$(this).prop('id'));
        });
        nodeDialog.find('label[for]').each(function() {
            $(this).prop('for', 'edit_'+$(this).prop('for'));
        });

	var editorNames = ['text', 'text_en'];
        editorInstances = [];
        for (var i=0; i<editorNames.length; i++) {
			var editorId = 'editor'+Math.random(); // tinymce requires unique id on textarea
            nodeDialog.find('textarea[name="'+editorNames[i]+'"]').prop('id', editorId);

			editorInstances[i] = new tinymce.Editor(editorId, {
				language : 'cs',
				plugins: "autolink charmap code contextmenu hr image link lists paste table textcolor anchor",
				toolbar: "undo redo | styleselect bullist numlist | link image | charmap forecolor | preview",
				contextmenu: "link image inserttable | cell row column deletetable",
				paste_as_text: true,
				image_advtab: true,
				entity_encoding: 'raw',
				relative_urls: false,
				remove_script_host: false,
				height : 600,
				file_browser_callback: function(field_name, url, type, win) {
					tinyMCE.activeEditor.windowManager.open({
						file : ERGO_ROOT_URL+'other/filebrowser/browser.htm',
						title : 'Gst File Browser',
						width : 800,
						height : 500,
						resizable : "yes",
						inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
						close_previous : "yes"
					}, {
						window : win,
						input : field_name
					});
				}
			}, tinymce.EditorManager);
			editorInstances[i].render();
		}

        nodeDialog.find('.tabs').tabs({
            active: selectedTab,
            activate: function(event, ui ) {
                selectedTab = ui.newTab.index();
            }
        });

        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');

    };

    var saveItem = function(closeAfterSave) {
        for (var i=0; i<editorInstances.length; i++) {
            editorInstances[i].save();
        }

        var postdata = nodeDialog.find('form').serialize();
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/staticarticles/ajax-save',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            nodeDialog.find('.msg').html('');
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            } else {
                editItem(data.id);
            }
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var addItem = function() {
        editRecord = undefined;
        renderEditForm();
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/staticarticles/ajax-edit?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            editRecord = data.record;
            renderEditForm();
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };



    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); });

    // Prevent jQuery UI dialog from blocking focusin
    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });

};