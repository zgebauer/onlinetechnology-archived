<?php
/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

use GstLib\Sitemap;

/**
 * data tier for module - sitemap
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 */
class DataSitemap
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    /** @var string name of table with records */
    private static $_tableName = '[%staticarticles%]';
    /** @var string name of table with sitemap index */
    private static $_tableNameIndex = '[%core_sitemap%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_app = $application;
    }

    /**
     * refresh urls of module in table sitemap
     */
    public function refreshLinks()
    {
        $date =  new \DateTime('now', new \DateTimeZone('UTC'));
        $refresh = $date->format('Y-m-d H:i:s');

        // records
        $url = $this->_app->getUrl(Module::NAME, false);
        $sql = 'INSERT INTO '.self::$_tableNameIndex.' (module,last_refresh,url,change_freq,priority,last_mod)
        SELECT '.$this->_con->escape(Module::NAME).','.$this->_con->escape($refresh).
        ",CONCAT('".$url."',seo),".$this->_con->escape(Sitemap::CHANGE_WEEKLY).',0.3,last_change
        FROM '.self::$_tableName;
        $this->_con->query($sql);

        $sql = 'DELETE FROM '.self::$_tableNameIndex.'
        WHERE module='.$this->_con->escape(Module::NAME).' AND last_refresh<>'.$this->_con->escape($refresh);
        $this->_con->query($sql);
    }

}