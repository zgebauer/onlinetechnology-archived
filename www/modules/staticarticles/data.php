<?php
/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

/**
 * data tier for module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 */
class Data
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%staticarticles%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * fill given instance from database
     * @param \Staticarticles\Article $record
     * @return bool false on error
     */
    public function load(Article $record)
    {
        $sql = 'SELECT name,last_change,text,meta_desc,meta_keys,`name_en`,`text_en`
        FROM '.self::$_tableName.' WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->text($row['text']);
        $record->metaDescription($row['meta_desc']);
        $record->metaKeywords($row['meta_keys']);
        $record->setNameEn($row['name_en'])
			->setTextEn($row['text_en']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Staticarticles\Article $record
     * @return bool false on error
     */
    public function save(Article $record)
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $items = array();
        $items[] = 'name='.$this->_con->escape($record->name());
        $items[] = 'text='.$this->_con->escape($record->text());
        $items[] = 'last_change='.$this->_con->escape($date->format('Y-m-d H:i:s'));
        $items[] = 'meta_desc='.$this->_con->escape($record->metaDescription());
        $items[] = 'meta_keys='.$this->_con->escape($record->metaKeywords());
        $items[] = '`name_en`='.$this->_con->escape($record->getNameEn());
        $items[] = '`text_en`='.$this->_con->escape($record->getTextEn());
        $sql = 'UPDATE '.self::$_tableName.' SET '.join(',', $items).' WHERE id='.$record->instanceId();
        return $this->_con->query($sql);
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,name FROM '.self::$_tableName.
            $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Article($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array())
    {
        return $this->_con->count('SELECT COUNT(*) FROM '.self::$_tableName.$this->_con->where($criteria));
    }

}