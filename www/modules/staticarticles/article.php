<?php

/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

use GstLib\Vars;

/**
 * static article with fixed position on web
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 */
class Article {

	/** @var id of article "sitemap" */
	const ID_SITEMAP = 3;

	/** @var id of article "obchodni podminky" */
	const ID_RULES = 4;

	/** @var string last error message */
	protected $_err;

	/** @var int instance identifier */
	protected $_id;

	/** @var bool flag "loaded from data tier" */
	protected $_loaded;

	/** @var string name */
	protected $_name;

	/** @var date date of last change of record */
	protected $_lastChange;

	/** @var string text of record */
	protected $_text;

	/** @var string content for meta tag description */
	protected $_metaDescription;

	/** @var string content for meta tag keywords */
	protected $_metaKeywords;

	/** @var \Staticarticles\Data data layer */
	protected $_dataLayer;

	/** @var string */
	private $nameEn;

	/** @var string */
	private $textEn;

	/**
	 * create instance and fill it with data by given identifier
	 * @param \Staticarticles\Data $dataLayer
	 * @param int $instanceId identifier of instance
	 */
	public function __construct(Data $dataLayer, $instanceId = 0) {
		$this->_err = '';
		$this->_id = 0;
		$this->_loaded = false;
		$this->_name = '';
		$this->_seo = '';
		$this->_lastChange = null;
		$this->_text = '';
		$this->_metaDescription = $this->_metaKeywords = '';

		$this->_dataLayer = $dataLayer;

		$this->instanceId($instanceId);
		if ($this->_id != 0) {
			$this->load();
		}
	}

	/**
	 * returns last error message
	 * @return string
	 */
	public function err() {
		return $this->_err;
	}

	/**
	 * returns and sets instance identifier
	 * @param int $value
	 * @return int
	 */
	public function instanceId($value = null) {
		if (!is_null($value)) {
			$value = (int)$value;
			if ($value > 0) {
				$this->_id = $value;
			}
		}
		return $this->_id;
	}

	/**
	 * returns flag "record loaded"
	 * @return bool
	 */
	public function loaded() {
		return $this->_loaded;
	}

	/**
	 * returns and sets name
	 * @param string $value
	 * @return string
	 */
	public function name($value = null) {
		if (!is_null($value)) {
			$this->_name = Vars::substr($value, 0, 70);
		}
		return $this->_name;
	}

	/**
	 * returns and sets text of article
	 * @param string $value
	 * @return string text
	 */
	public function text($value = null) {
		if (!is_null($value)) {
			$this->_text = $value;
		}
		return $this->_text;
	}

	/**
	 * returns and sets meta description
	 * @param string $value
	 * @return string
	 */
	public function metaDescription($value = null) {
		if (!is_null($value)) {
			$this->_metaDescription = Vars::substr($value, 0, 255);
		}
		return $this->_metaDescription;
	}

	/**
	 * returns and sets meta keywords
	 * @param string $value
	 * @return string
	 */
	public function metaKeywords($value = null) {
		if (!is_null($value)) {
			$this->_metaKeywords = Vars::substr($value, 0, 255);
		}
		return $this->_metaKeywords;
	}

	/**
	 * @return string
	 */
	public function getNameEn() {
		return $this->nameEn;
	}

	/**
	 *
	 * @param string$nameEn
	 * @return Article
	 */
	public function setNameEn($nameEn) {
		$this->nameEn = Vars::substr($nameEn, 0, 250);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTextEn() {
		return $this->textEn;
	}

	/**
	 * @param string $textEn
	 * @return Article
	 */
	public function setTextEn($textEn) {
		$this->textEn = $textEn;
		return $this;
	}

	/**
	 * fill instance from data tier
	 * @return bool false on error
	 */
	public function load() {
		$this->_loaded = $this->_dataLayer->load($this);
		return $this->_loaded;
	}

	/**
	 * save instance to data tier
	 * @return bool false on error
	 */
	public function save() {
		$this->_err = '';
		$required = array();
		if ($this->_name === '') {
			$required[] = '[%LG_NAME%] (CS)';
		}
		if ($this->getNameEn() === '') {
			$required[] = '[%LG_NAME%] (EN)';
		}
		if ($this->_text === '') {
			$required[] = '[%LG_TEXT%] (CS)';
		}
		if ($this->getTextEn() === '') {
			$required[] = '[%LG_TEXT%] (EN)';
		}
		if (isset($required[0])) {
			$this->_err = '[%LG_ERR_MISSING_VALUES%] ' . join(', ', $required);
			return false;
		}

		if ($this->_metaDescription === '') {
			$this->metaDescription($this->_name);
		}
		if ($this->_metaKeywords === '') {
			$this->metaKeywords(join(', ', Vars::parseKeywords($this->_name)));
		}

		return $this->_dataLayer->save($this);
	}

	/**
	 * return note about article
	 * @return string
	 */
	public function note() {
		$notes = array(
			self::ID_SITEMAP => 'Mapa stránek',
			self::ID_RULES => 'Obchodní podmínky'
		);
		return (isset($notes[$this->_id]) ? $notes[$this->_id] : '');
	}

	/**
	 * returns seo strings for all articles
	 * @return array
	 */
	public static function seoStrings() {
		return array(
			self::ID_SITEMAP => 'mapa-stranek',
			self::ID_RULES => 'obchodni-podminky'
		);
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public function getNameByLang($lang) {
		return $lang === 'en' ? $this->getNameEn() : $this->name();
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	public function getTextByLang($lang) {
		return $lang === 'en' ? $this->getTextEn() : $this->text();
	}
}
