<?php
/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

/**
 * configuration of module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 */
class Config
{
  

    /**
     * returns absolute path to directory for files uploaded via tinymce
     * @return string
     */
    public function dirUserFiles()
    {
        return ERGO_DATA_DIR.'soubory/';
    }


}