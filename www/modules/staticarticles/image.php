<?php
/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

/**
 * main image assigned to article
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 * @see        \Staticarticles\Article
 */
class Image
{
    /** @var type of image - original image */
    const IMG_ORIG = 0;
    /** @var type of image - thumbnail */
    const IMG_THUMB = 1;
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var \Staticarticles\DataImage data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Staticarticles\DataImage $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(DataImage $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * save instance to data tier
     * @param string name of input with uploaded file
     * @return bool false on error
     */
    public function save($field)
    {
        $this->_err = '';
        return $this->_dataLayer->save($this, $field);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        $this->_err = '';
//        $this->_dataLayer->delete($this);
//die();
        return $this->_dataLayer->delete($this);
    }

    /**
     * return url of attached file or empty string if file not found
     * @param enum $type type of attached file
     * @return string
     */
    public function getAttachmentUrl($type)
    {
        return $this->_dataLayer->getAttachmentUrl($this, $type);
    }

    /**
     * returns array with width and height of attached image
     * @param enum $type type of attached file
     * @return array with indices 'width' and 'height'
     */
 /*   public function getAttachmentImgSize($type = null)
    {
        $width = $height = null;
        if ($type === self::IMG_THUMB) {
            $width = Ergo_Module_News::getConfig()->getWidthImageThumb();
            $height = Ergo_Module_News::getConfig()->getHeightImageThumb();
        }
        if ($type === self::IMG_THUMB_DETAIL) {
            $width = Ergo_Module_News::getConfig()->getWidthImageThumbDetail();
            $height = Ergo_Module_News::getConfig()->getHeightImageThumbDetail();
        }
        $tmp = getimagesize(Ergo_Core::factory('NewsDataMainImage')->getAttachmentFullPath($this, $type));
        if (is_array($tmp)) {
            if (!is_null($width) && $tmp[0] <= $width) {
                $width = $tmp[0];
            }
            if (!is_null($height) && $tmp[1] <= $height) {
                $height = $tmp[1];
            }
        }
        return array($width, $height);
    }*/

}