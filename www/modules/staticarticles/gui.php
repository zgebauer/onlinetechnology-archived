<?php

/**
 * @package    Ergo
 * @subpackage Staticarticles
 */

namespace Staticarticles;

use GstLib\Html;
use GstLib\Template;

/**
 * presentation tier for public web of module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Staticarticles
 */
class Gui extends \Ergo\GuiAbstract {

	/**
	 * render page with article
	 * @param \Staticarticles\Article $record
	 * @return string
	 */
	public function renderArticle(Article $record) {
		$lang = $this->_app->currentLanguage();
		$tpl = new Template($this->_app->getTemplatePath('article.htm', Module::NAME), true);
		$tpl->replace('TXT_NAME', $record->getNameByLang($lang));
		$tpl->replace('TXT_TEXT', Html::urlToAbs($record->getTextByLang($lang), $this->_app->baseUrl()), 'raw');
		return $tpl->get();
	}

	/**
	 * render box with article
	 * @param \Staticarticles\Article $record
	 * @return string
	 */
	public function renderArticleAsBox(Article $record) {
		$tpl = new Template($this->_app->getTemplatePath('box.htm', Module::NAME), true);
		$tpl->replace('TXT_TEXT', Html::urlToAbs($record->getTextByLang($this->_app->currentLanguage()), $this->_app->baseUrl()), 'raw');
		return $tpl->get();
	}

}
