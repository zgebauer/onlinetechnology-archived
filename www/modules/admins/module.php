<?php
/**
 * @package    Ergo
 * @subpackage Admins
 */

namespace Admins;

require_once 'user.php';
require_once 'data.php';

/**
 * main class of module Admins
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Admins
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'admins';
    /** @var name of session variable to store id of logged user */
    const SESSION_USER_ID = 'uid';
    /** @var \Admins\User current logged user */
    private $_user;

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        parent::__construct($application);
        $this->_user = null;
        $this->_app->setShared('\Admins\Data');
    }

    public function menuItems()
    {
        $user = $this->getUser();
        if (is_null($user) || !$user->hasPermission(self::NAME)) {
            return null;
        }
        return array(
            array(
               'name' => $this->name(),
               'url' => $this->_app->getUrl(self::NAME),
               'title' => 'Seznam správců',
               'highlight' => ($this->_app->getCurrentModule() == self::NAME)
            )
        );
    }

    public function permissions()
    {
        return array();
    }

    public function outputAdmin($parameter = '')
    {
        $map = array(
            '' => array('\Admins\Admin\Controller', 'renderList'),
            'ajax-list' => array('\Admins\Admin\Controller', 'handleList'),
            'ajax-edit' => array('\Admins\Admin\Controller', 'handleEdit'),
            'ajax-save' => array('\Admins\Admin\Controller', 'handleSave'),
            'ajax-delete' => array('\Admins\Admin\Controller', 'handleDelete'),
            '_box_login_' => array('\Admins\Admin\ControllerAuth', 'renderBoxLogin'),
            '_box_current_user_' => array('\Admins\Admin\ControllerAuth', 'renderBoxUser'),
            'logout' => array('\Admins\Admin\ControllerAuth', 'handleLogout')
        );

        if (isset($map[$parameter])) {
            return call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
        }
        return parent::outputAdmin($parameter);
    }

    public function name()
    {
        return 'Správci';
    }

    /**
     * returns current user or null
     * @return \Admins\User|null
     */
    public function getUser()
    {
        if (is_null($this->_user) || ERGO_TESTING) {
            $userId = $this->_app->request()->getValue(self::SESSION_USER_ID, 'sess', 'int');
            if ($userId > 0) {
                $this->_user = new \Admins\User($this->_app->dice()->create('\Admins\Data'), $userId);
                if (!$this->_user->loaded()) {
                    $this->_user = null;
                }
            }
        }
        return $this->_user;
    }

    /**
     * returns hash of salted password
     * @param string $password
     * @param string $salt
     * @return string
     */
    public static function passwordHash($password, $salt = null)
    {
        $password = urlencode($password);
        if (is_null($salt)) {
            // '$2a$07$' - blowfish
            $salt = '$2a$07$'.\GstLib\Vars::randomString(22);
        }
        return crypt($password, $salt);
    }

}