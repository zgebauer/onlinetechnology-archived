<?php
/**
 * @package    Ergo
 * @subpackage Admins
 */

namespace Admins\Admin;

use GstLib\Template;
use Admins\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Admins
 */
class ControllerAuth
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render login form
     * @return string
     */
    public function renderBoxLogin()
    {
        if (!is_null($this->_app->getUser())) {
            return '';
        }
        $err = '';
        if ($this->_app->request()->isPost() && $this->_app->csrf()->checkCsrf()) {
            $dataLayer = $this->_app->dice()->create('\Admins\Data');
            $login = $this->_app->request()->getPost('login');
            $password = $this->_app->request()->getPost('pwd');
            if ($login === '' || $password === '') {
                $err = 'Vyplňte uživatelské jméno a heslo';
            }
            if ($err === '') {
                $result = $dataLayer->login($login, $password);
                if (!$result) {
                    $err = 'Uživatelské jméno nebo heslo není platné';
                }
            }
            if ($err === '') {
                $this->_app->response()->redirect($this->_app->targetUrlAfterLogin(), 303);
            }
        }

        $tpl = new Template($this->_app->getTemplatePath('box_login.htm', Module::NAME, true), true);
        $tpl->replace('URL_FORM', $this->_app->baseUrl(true), 'raw');
        $tpl->replace('INC_CSRF', $this->_app->csrf()->renderCsrfHidden(), 'raw');
        $tpl->replace('TXT_ERROR', $err);
        return $tpl->get();
    }

    /**
     * handle logout user
     */
    public function handleLogout()
    {
        $this->_app->dice()->create('\Admins\Data')->logout();
        $this->_app->response()->redirect($this->_app->baseUrl(true));
    }

    /**
     * render box with logged user
     * @return string
     */
    public function renderBoxUser()
    {
        if (is_null($this->_app->getUser())) {
            return '';
        }
        $tpl = new Template($this->_app->getTemplatePath('box_current_user.htm', Module::NAME, true), true);
        $tpl->replace('TXT_NAME', $this->_app->getUser()->name());
        $tpl->replace('URL_LOGOUT', $this->_app->getUrl(Module::NAME, 'logout'));
        return $tpl->get();
    }

}