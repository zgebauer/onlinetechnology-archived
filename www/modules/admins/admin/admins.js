'use strict';

Ergo.Admins = Ergo.Admins || {};

Ergo.Admins.Admin = function() {
    var nodeList = $('#adminsList');
    var nodeDialog = $('#dialog');

    var recordsList = new Ergo.RecordsList(nodeList);
    recordsList.url = ERGO_ROOT_URL+'admin/admins/ajax-list';
    recordsList.itemTemplate = '<tr><td>%login%</td><td>%name%</td><td>%last_log%</td><td>%blocked%</td>'
        + '<td data-item-edit="%id%" class="clickable edit">Upravit</td><td data-item-del="%id%" class="clickable del">Smazat</td></tr>';
    recordsList.orderBy = 'login';
    recordsList.callbackLoaded = function(list) {
        pagination.setPages(list.pages);
        positions.update(list.pages);
    };
    recordsList.load();

    var pagination = new Ergo.RecordsList.Pagination($('[data-pos]'), recordsList);
    var positions = new Ergo.RecordsList.Positions(recordsList, $('[data-pos-start]'), $('[data-pos-end]'), $('[data-pos-total]'));
    var rowsPage = new Ergo.RecordsList.RowsPage($('.pagination [data-rows-page]'), recordsList);
    var formFilter = new Ergo.RecordsList.Filter($('.frmFilter'), recordsList);


    var renderEditForm = function(record) {
        if (record === undefined) {
            nodeDialog.html($('#detailEditNew').html());
        } else {
            var html = $('#detailEdit').html();
            html = html.replace(/%login%/g, record.login);
            html = html.replace(/%last_log%/g, record.last_log);
            nodeDialog.html(html);
            nodeDialog.find('input[name="id"]').val(record.id);
            nodeDialog.find('input[name="name"]').val(record.name);
            nodeDialog.find('input[name="block"]').prop('checked', record.blocked);
            for (var key in record.permissions) {
                nodeDialog.find('input[name=module\\[\\]][value="'+key+'"]').prop('checked', true);
            }
            nodeDialog.find('input[name="change_password"]').change(function() {
                if ($(this).is(':checked')) {
                    $('#dialog').find('.newpassword').removeClass('hidden');
                } else {
                    $('#dialog').find('.newpassword').addClass('hidden');
                }
            });
        };
        nodeDialog.dialog({
            buttons : [
                { text: "Uložit", click: function() { saveItem(); } },
                { text: "Uložit a zavřít", click: function() { saveItem(true); } },
                { text: "Storno",  click: function() { $(this).dialog('close'); } }
            ],
            width: '95%'
        }).dialog('open');
    };

    var saveItem = function(closeAfterSave) {
        var postdata = nodeDialog.find('form').serialize();
        nodeDialog.find('.msg').html('Probíhá zpracování...');
        $.ajax({
            url: ERGO_ROOT_URL+'admin/admins/ajax-save',
            method:'post',
            data: postdata,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseSaveItem(data, closeAfterSave);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseSaveItem( { msg: errorThrown } );
        });
    };
    var handleResponseSaveItem = function(data, closeDialog) {
        if (data.status === 'OK') {
            recordsList.load();
            if (closeDialog) {
                nodeDialog.dialog('close');
            }
            nodeDialog.find('.msg').html('');
        } else {
            nodeDialog.find('.msg').html(data.msg);
        }
    };

    var editItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/admins/ajax-edit?id='+id,
            dataType: 'json',
            cache: false
        }).done(function(data, textStatus, jqXHR) {
            handleResponseEditItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseEditItem( { msg: errorThrown } );
        });
    };
    var handleResponseEditItem = function(data) {
        if (data.status === 'OK') {
            renderEditForm(data.record);
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

//    $('#adminsList').on('click', '[data-item-add]', function() {
//        var html = $('#detailNew').html();
//        $('#dialog').html(html).dialog(
//            'option', 'buttons', [
//                { text: "Ok",
//                  click: function() {
//                        var isValid = $('#dialog form').valid();
//                        if (isValid) {
//                            var postdata = $('#dialog form').serialize();
//                            $('#dialog form').find('.msg').html('Probíhá zpracování...');
//                            $.ajax({
//                                url: ERGO_ROOT_URL+'admin/admins/ajax-save',
//                                method:'post',
//                                data: postdata,
//                                dataType: 'json',
//                                context:this,
//                                success: function(data) {
//                                    if (data.status === 'OK') {
//                                        recordsList.load();
//                                        $(this).dialog('close');
//                                    }
//                                    $(this).find('.msg').html(data.msg);
//                                }
//                            });
//                        }
//                  }
//                },
//                { text: "Storno",
//                  click: function() {
//                    $(this).dialog('close');
//                  }
//                }
//            ]
//        ).dialog('option', 'width', 400)
//        .dialog('open');
//    });

//    $('#adminsList').on('mousedown', '[data-item-edit]', function() {
//        var id = $(this).data('item-edit');
//        $.ajax({
//            url: ERGO_ROOT_URL+'admin/admins/ajax-edit?id='+id,
//            context:this,
//            success: function(data) {
//                if (data.status === 'OK') {
//                    var html = $('#detailEdit').html();
//                    html = html.replace(/%login%/g, data.record.login);
//                    html = html.replace(/%last_log%/g, data.record.last_log);
//
//                    $('#dialog').html(html).dialog(
//                        'option', 'buttons', [
//                            { text: "Ok",
//                              click: function() {
//                                   var isValid = $('#dialog form').valid();
//                                   if (isValid) {
//                                       var postdata = $('#dialog form').serialize();
//                                       $('#dialog form').find('.msg').html('Probíhá zpracování...');
//                                       $.ajax({
//                                           url: ERGO_ROOT_URL+'admin/admins/ajax-save?id='+id,
//                                           method:'post',
//                                           data: postdata,
//                                           dataType: 'json',
//                                           context:this,
//                                           success: function(data) {
//                                               if (data.status === 'OK') {
//                                                   recordsList.load();
//                                                   $(this).dialog('close');
//                                               }
//                                               $(this).find('.msg').html(data.msg);
//                                           }
//                                       });
//                                   }
//                             }
//                            },
//                            { text: "Storno",
//                              click: function() {
//                                $(this).dialog('close');
//                              }
//                            }
//                        ]
//                    ).dialog('option', 'width', '90%')
//                    .dialog('open');
//
//                    $('#dialog').find('input[name="name"]').val(data.record.name);
//                    $('#dialog').find('input[name="block"]').prop('checked', data.record.blocked);
//                    for (var key in data.record.permissions) {
//                        $('#dialog').find('input[name=module\\[\\]][value="'+key+'"]').prop('checked', true);
//                    }
//                    $('#dialog').find('input[name="change_password"]').change(function() {
//                        if ($(this).is(':checked')) {
//                            $('#dialog').find('.newpassword').removeClass('hidden');
//                        } else {
//                            $('#dialog').find('.newpassword').addClass('hidden');
//                        }
//                    });
//                }
//
//            }
//        });
//    });

    var confirmDeleteItem = function(id, name) {
        nodeDialog.html('<p>Opravdu chcete vymazat '+name+'?</p>').dialog({
            buttons: [
                { text: "Ok", click: function() { deleteItem(id) } },
                { text: "Storno", click: function() { nodeDialog.dialog('close') } }
            ],
            width: '40%'
            }).dialog('open');
    };

    var deleteItem = function(id) {
        $.ajax({
            url: ERGO_ROOT_URL+'admin/admins/ajax-delete?id='+id,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            handleResponseDeleteItem(data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            handleResponseDeleteItem( { msg: errorThrown } );
        });
    };
    var handleResponseDeleteItem = function(data) {
        if (data.status === 'OK') {
            recordsList.load();
            nodeDialog.dialog('close');
        } else {
            nodeDialog.html('<p>'+data.msg+'</p>')
                .dialog('option', 'buttons', [{ text: "Ok", click: function() { $(this).dialog('close'); } }]);
        }
    };

    nodeList.on('mousedown', '[data-item-edit]', function() { editItem($(this).data('item-edit')); })
        .on('mousedown', '[data-item-add]', function() { renderEditForm(); })
        .on('mousedown', '[data-item-del]', function() {
            confirmDeleteItem($(this).data('item-del'), $(this).closest('tr').find('td').eq(0).html());
        });

};
