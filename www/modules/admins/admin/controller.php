<?php
/**
 * @package    Ergo
 * @subpackage Admins
 */

namespace Admins\Admin;

use GstLib\Html;
use GstLib\Template;
use Ergo\Response;
use Ergo\Pagination;
use Admins\Module;

/**
 * controller for admin
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Admins
 */
class Controller
{
    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * render page with list of records
     */
    public function renderList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->rememberRedirectLogin();
            $this->_app->response()->redirect($this->_app->baseUrl(true));
            return;
        }

        $this->_app->response()->pageTitle('Správci');
        $tpl = new Template($this->_app->getTemplatePath('list.htm', Module::NAME, true), true);
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $tpl->replace('INPVAL_ROWS', Html::getOptions($options), 'raw');

        // permissions for modules
        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('ITEMS_MODULES');
        $tplPermission = new Template();
        $permissionTemplate = $tpl->getSub('ITEMS_PERMISSIONS');
        foreach ($this->_app->getModules() as $moduleName) {
            $module = $this->_app->getModule($moduleName);
            $permissions = $module->permissions();
            if (is_null($permissions)) {
                continue;  // no admin
            }
            $tplRow->set($rowTemplate);
            $tplRow->replace('TXT_MODULE', $module->name());
            $tplRow->replace('INPVAL_MODULE', $moduleName);
            // permissions
            $cntPermissions = '';
            foreach ($permissions as $key => $value) {
                $tplPermission->set($permissionTemplate);
                $tplPermission->replace('INPVAL_MODULE', $moduleName);
                $tplPermission->replace('INPVAL_PERMISSION', $key);
                $tplPermission->replace('TXT_PERMISSION', $value);
                $cntPermissions .= $tplPermission->get();
            }
            $tplRow->replaceSub('ITEMS_PERMISSIONS', $cntPermissions);
            $tplRow->showSub('PERMISSIONS', $cntPermissions !== '');
            $cnt .= $tplRow->get();
        }
        unset($tplRow);
        $tpl->replaceSub('ITEMS_MODULES', $cnt);
        $tpl->replace('INC_MODULES_UPDATE', $cnt, 'raw');

        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * render json with list of records
     */
    public function handleList()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $dataLayer = $this->_app->dice()->create('\Admins\Data');
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $pos = max(array(1, $request->getQuery(Pagination::POS_VARIABLE, 'int')));
        $order = array_intersect(array('login', 'name'), array($request->getQuery('order')));
        $order = (isset($order[0]) ? $order[0] : 'login');
        $desc = $request->getQuery('desc', 'bool');
        $rowsPage = max(array(10, $request->getQuery('rows_page', 'int')));

        $filterLogin = $request->getQuery('f_login');
        $filterName = $request->getQuery('f_name');

        $criteria = array();
        if ($filterLogin !== '') {
            $criteria[] = array('login', $filterLogin, 'like');
        }
        if ($filterName !== '') {
            $criteria[] = array('name', $filterName, 'like');
        }

        $items = array();
        /** @var $record \Admins\User */
        foreach ($dataLayer->getRecords($pos-1, $rowsPage, $order, $desc, $criteria) as $record) {
            $items[] = array(
                'id' => $record->instanceId(),
                'login' => $record->login(),
                'name' => $record->name(),
                'last_log' => (is_null($record->lastLog()) ? '' : $record->lastLog()->format('j.n.Y H:i')),
                'blocked' => ($record->blocked() ? 'Ano' : '')
            );
        }

        $ret = new \stdClass();
        $ret->items = $items;
        $ret->total = $dataLayer->count($criteria);

        $pager = new Pagination($ret->total, $pos, $rowsPage);
        $ret->pages = $pager->getPages();

        $tmp = $pager->getRecords();
        $ret->pos = $tmp[0];
        $ret->posEnd = $tmp[1];
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * render json with single record
     */
    public function handleEdit()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $record = $this->_app->dice()->create('\Admins\User', array($this->_app->request()->getQuery('id', 'int')));

        $ret = new \stdClass();
        if ($record->loaded()) {
            $ret->record = (object) array(
                'id' => $record->instanceId(),
                'login' => $record->login(),
                'name' => $record->name(),
                'blocked' => $record->blocked(),
                'last_log' => is_null($record->lastLog()) ? '' : $record->lastLog()->format('j.n.Y H:i'),
                'permissions' => $record->getPermissions()
            );
        }

        $ret->status = ($record->loaded() ? 'OK' : 'ERR');
        $ret->msg = ($record->loaded() ? '' : 'Record not found');
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle save record
     */
    public function handleSave()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Admins\User', array($request->getPost('id', 'int')));
        $changePassword = isset($_POST['change_password']);
        if ($record->instanceId() === 0 || $changePassword) {
            $record->login($request->getPost('login'));
            $record->setPassword($request->getPost('password'), $request->getPost('confirm'));
        }
        $record->name($request->getPost('name'));
        $record->blocked($request->getPost('block', 'bool'));
        $permissions = array();
        if (isset($_POST['module']) && is_array($_POST['module'])) {
            foreach ($_POST['module'] as $moduleName) {
                $permissions[$moduleName] = array();
            }
        }
        if (isset($_POST['permissions']) && is_array($_POST['permissions'])) {
            foreach ($_POST['permissions'] as $moduleName=>$tmp) {
                array_walk($tmp, 'intval');
                $permissions[$moduleName] = $tmp;
            }
        }
        $record->setPermissions($permissions);

        if (!$record->save($changePassword)) {
            $err = ($record->err() === '' ? 'something wrong' : $record->err());
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $ret->msg = $err;
        $this->_app->response()->setOutputJson($ret);
    }

    /**
     * handle delete record
     */
    public function handleDelete()
    {
        if (is_null($this->_app->getUser()) || !$this->_app->getUser()->hasPermission(Module::NAME)) {
            $this->_app->response()->setOutputNone(Response::HTTP_FORBIDDEN);
            return;
        }

        $err = '';
        /** @var $request \Ergo\Request */
        $request = $this->_app->request();

        $record = $this->_app->dice()->create('\Admins\User', array($request->getQuery('id', 'int')));

        if (!$record->delete()) {
            $err = ($record->err() == '' ? 'something wrong' : $record->err());
        }
        if ($err === '') {
            $this->_app->pageCache()->emptyCache(array('pattern'=> Module::NAME));
        }

        $ret = new \stdClass();
        $ret->status = ($err === '' ? 'OK' : 'ERR');
        $this->_app->response()->setOutputJson($ret);
    }

}