<?php
/**
 * @package    Ergo
 * @subpackage Admins
 */

namespace Admins;

use GstLib\Vars;

/**
 * user - administator.
 * Administrator can have different permissions per modules
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Admins
 */
class User
{
    /** @var string last error message */
    protected $_err;
    /** @var int instance identifier */
    protected $_id;
    /** @var bool flag loaded from data tier */
    protected $_loaded;
    /** @var string username */
    protected $_login;
    /** @var string password */
    protected $_password;
    /** @var string confirm password */
    protected $_confirm;
    /** @var string name */
    protected $_name;
    /** @var \DateTime date of last login */
    protected $_lastLog;
    /** @var bool flag "access blocked" */
    protected $_block;
    /**
     * associative array of granted permissions for modules.
     * Each item contains array of permissions
     * @var array
     */
    protected $_permissions;
    /** @var \Admins\Data $dataLayer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param \Admins\Data $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(Data $dataLayer, $instanceId = 0)
    {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_login = '';
        $this->_password = '';
        $this->_confirm = '';
        $this->_name = '';
        $this->_lastLog = null;
        $this->_block = false;
        $this->_permissions = null;
        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns and sets last error message
     * @param string $value
     * @return string
     */
    public function err($value = null)
    {
        if (!is_null($value)) {
            $this->_err = $value;
        }
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null)
    {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded()
    {
        return $this->_loaded;
    }

    /**
     * returns and sets login
     * @param string $value
     * @return string
     */
    public function login($value = null)
    {
        if (!is_null($value)) {
            $this->_login = Vars::substr($value, 0, 10);
        }
        return $this->_login;
    }

    /**
     * returns password
     * @return string
     */
    public function password()
    {
        return $this->_password;
    }

    /**
     * sets password
     * @param string $value
     * @param string $confirm confirmation of password
     */
    public function setPassword($value, $confirm)
    {
        $this->_password = $value;
        $this->_confirm = $confirm;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null)
    {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 50);
        }
        return $this->_name;
    }

    /**
     * returns and sets date of last login
     * @param string $vvalue
     * @return Datetime|null
     */
    public function lastLog($value = null)
    {
        if (!is_null($value) && (strpos($value, '0000') === false)) {
            try {
                $date = new \DateTime($value, new \DateTimeZone('UTC'));
                $this->_lastLog = $date;
            } catch (Exception $e) {
            }
        }
        return $this->_lastLog;
    }

    /**
     * returns and sets flag "access blocked"
     * @param bool $value
     * @return bool
     */
    public function blocked($value = null)
    {
        if (!is_null($value)) {
            $this->_block = (bool) $value;
        }
        return $this->_block;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load()
    {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @param bool $changePassword
     * @return bool false on error
     */
    public function save($changePassword = false)
    {
        $this->_err = '';

        if ($this->_id === 0 || $changePassword) {
            $required = array();
            if ($this->_id === 0 && $this->_login === '') {
                $required[] = 'uživatelské jméno';
            }
            if ($this->_password === '') {
                $required[] = 'heslo';
            }
            if ($this->_confirm == '') {
                $required [] = 'potvzení hesla';
            }
            if (isset($required[0])) {
                $this->_err = 'Chybí údaje: '.join(', ', $required);
                return false;
            }
            if ($this->_password != $this->_confirm) {
                $this->_err = 'Heslo a potvrzení hesla není stejné!';
                return false;
            }
        }
        return $this->_dataLayer->save($this, $changePassword);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete()
    {
        return $this->_dataLayer->delete($this);
    }

    /**
     * return permissions for modules
     * @return array
     */
    public function getPermissions()
    {
        if (is_null($this->_permissions)) {
            return array();
        }
        return $this->_permissions;
    }

    /**
     * check if user has permision for specified module
     * @param string $module
     * @param mixed $permission
     * @return bool
     */
    public function hasPermission($module, $permission = null)
    {
        if (!isset($this->_permissions[$module])) {
            return false;
        }
        return (is_null($permission) ? true : in_array($permission, $this->_permissions[$module]));
    }

    /**
     * sets permission of user. Expect array of arrays in format
     * [modulename] => array(1, 2) // permissions
     * @param array $permissions
     */
    public function setPermissions(array $permissions)
    {
        $this->_permissions = null;
        foreach ($permissions as $module => $modulePermissions) {
            $tmp = array();
            if (is_array($modulePermissions)) {
                foreach ($modulePermissions as $item) {
                    $item = (int) $item;
                    if ($item >= 0) {
                        $tmp[] = $item;
                    }
                }
            }
            $this->_permissions[$module] = $tmp;
        }
    }

}