<?php
/**
 * class AdminsData
 *
 * @package    Ergo
 * @subpackage Admins
 */

namespace Admins;

/**
 * data tier for module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Admins
 */
class Data
{
    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection)
    {
        $this->_con = $connection;
    }

    /**
     * fill given instance from database
     * @param \Admins\User $record
     * @return bool false on error
     */
    public function load(User $record)
    {
        $sql = 'SELECT login,name,last_log,block FROM [%core_users%] WHERE id='.$record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->login($row['login']);
        $record->name($row['name']);
        $record->lastLog($row['last_log']);
        $record->blocked($row['block']);

        $sql = 'SELECT module,permission FROM [%core_permissions%] WHERE user_id='.$record->instanceId();
        $permissions = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $permissions[$row['module']] = explode(',', $row['permission']);
        }
        $record->setPermissions($permissions);

        return true;
    }

    /**
     * save given instance to database
     * @param \Admins\User $record
     * @param bool $updatePassword
     * @return bool false on error
     */
    public function save(User $record, $updatePassword = false)
    {
        $items = array();
        $userId = $record->instanceId();
        if ($userId === 0) {
            $sql = 'SELECT COUNT(*) FROM [%core_users%] WHERE login='.$this->_con->escape($record->login());
            if ($this->_con->count($sql) !== 0) {
                $record->err('Uživatelské jméno už existuje, použijte jiné');
                return false;
            }
            if (!$this->_con->beginTrans()) {
                return false;
            }
            $items['login'] = $this->_con->escape($record->login());
            $items['pwd'] = $this->_con->escape(\Admins\Module::passwordHash($record->password()));
            $items['name'] = $this->_con->escape($record->name());
            $items['block'] = $this->_con->boolToSql($record->blocked());
            $userId = $this->_con->insert(
                'INSERT INTO [%core_users%] ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')'
            );
            if (!$userId) {
                $this->_con->rollback();
                return false;
            }
        } else {
            $items[] = 'name='.$this->_con->escape($record->name());
            $items[] = 'block='.$this->_con->boolToSql($record->blocked());
            if ($updatePassword) {
                $items[] = 'pwd='.$this->_con->escape(\Admins\Module::passwordHash($record->password()));
            }
            $sql = 'UPDATE [%core_users%] SET '.join(',', $items).' WHERE id='.$userId;
            if (!$this->_con->query($sql)) {
                $this->_con->rollback();
                return false;
            }
        }

        // save permissions
        $sql = 'DELETE FROM [%core_permissions%] WHERE user_id='.$userId;
        if (!$this->_con->query($sql)) {
            $this->_con->rollback();
            return false;
        }
        foreach ($record->getPermissions() as $module=>$permissions) {
            $sql = 'INSERT INTO [%core_permissions%] (user_id,module,permission)
            VALUES ('.$userId.','.$this->_con->escape($module).','.$this->_con->escape(join(',', $permissions)).')';
            if (!$this->_con->query($sql)) {
                $this->_con->rollback();
                return false;
            }
        }
        if (!$this->_con->commit()) {
            return false;
        }
        $record->instanceId($userId);
        return true;
    }

    /**
     * delete given instance from database
     * @param \Admins\User $record
     * @return bool false on error
     */
    public function delete(User $record)
    {
        $sql = array(
            'DELETE FROM [%core_permissions%] WHERE user_id='.$record->instanceId(),
            'DELETE FROM [%core_users%] WHERE id='.$record->instanceId()
        );
        return $this->_con->transaction($sql);
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array())
    {
        $sql = 'SELECT id,login,name,last_log,block FROM [%core_users%] '.
        $this->_con->where($criteria).$this->_con->orderBy($order, $desc).$this->_con->limit($start, $rows);
        $this->_con->setType('last_log', 'iso-datetime');
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new User($this);
            $tmp->instanceId($row['id']);
            $tmp->login($row['login']);
            $tmp->name($row['name']);
            $tmp->lastLog($row['last_log']);
            $tmp->blocked($row['block']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array())
    {
        return $this->_con->count('SELECT COUNT(*) FROM [%core_users%]'.$this->_con->where($criteria));
    }

    /**
     * atempts login user with email and password
     * returns logged in profile or null
     *
     * @param string $email
     * @param string $password
     * @return boolean false on invalid data
     */
    public function login($login, $password)
    {
        // profile exists in db?
        $sql = 'SELECT id,pwd FROM [%core_users%] WHERE login='.$this->_con->escape($login).' AND block=0';
        $row = $this->_con->fetchArray($sql);
        if (!$row) {
            return false;
        }
        if ($row['pwd'] !== Module::passwordHash($password, $row['pwd'])) {
            return false;
        }

        $sql = 'UPDATE [%core_users%] SET last_log='.$this->_con->escape(gmdate('Y-m-d H:i:s')).' WHERE id='.$row['id'];
        $this->_con->query($sql);

        $_SESSION[Module::SESSION_USER_ID] = $row['id'];
        return true;
    }

    /**
     * logout current admin user and destroy current session
     */
    public function logout()
    {
        unset($_SESSION[Module::SESSION_USER_ID]);
        session_unset();
        @session_destroy();
    }

}