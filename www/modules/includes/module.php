<?php
/**
 * @package    Ergo
 * @subpackage Includes
 */

namespace Includes;

require_once 'gui.php';

/**
 * main class of module Includes
 * includes html fragments in page
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Includes
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'includes';

    public function __construct(\Ergo\ApplicationInterface $application)
    {
        parent::__construct($application);

        $this->_app->setShared('\Includes\Gui');
    }

    public function output($parameter = '')
    {
        switch($parameter) {
        case '_js_':
            $url = $this->_app->baseUrl();
            $facade = $this->_app->currentFacade();
            /*if ($this->_app->isProductionMode()) {
                return '<script src="'.$url.'facades/'.$facade.'/default.min.js?150916"></script>';
            }*/
            return '<script src="'.$url.'other/jquery/jquery.colorbox-min.js"></script>'
            .'<script src="'.$url.'other/jquery/easySlider1.7.js"></script>'
            .'<script src="'.$url.'other/jquery/jquery.bxslider.min.js"></script>'
            .'<script src="'.$url.'other/jquery/jquery.animate_from_to-1.0-custom.js"></script>'
            .'<script src="'.$url.'other/jquery/jquery.autocomplete.min.js"></script>'
            .'<script src="'.$url.'lang/' . $this->_app->currentLanguage() . '.js?v2"></script>'
            .'<script src="'.$url.'core/core.js"></script>'
            .'<script src="'.$url.'modules/eshop/scr.js"></script>'
            .'<script src="'.$url.'modules/search/scr.js"></script>'
            .'<script src="'.$url.'facades/'.$facade.'/scr.js"></script>'
            ;
        case '_ga':
            if (!$this->_app->isProductionMode()) {
                return '';
            }
            break;
        case '_slider':
            // slider nezobrazovat v detailu produktu
            if ($this->_app->getCurrentModule() == \Eshop\Module::NAME
                    && strpos($this->_app->currentModuleParameter(), 'zbozi/') !== FALSE) {
                return '';
            }
            break;
        }

        if ($parameter !== '') {
            return $this->_app->dice()->create('\Includes\Gui', array($this->_app))->renderContainer($parameter);
        }
        return parent::output($parameter);
    }

    public function name()
    {
        return 'Includes';
    }

}