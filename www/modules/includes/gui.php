<?php
/**
 * @package    Ergo
 * @subpackage Includes
 */

namespace Includes;

use GstLib\Html;
use GstLib\Template;


/**
 * presentation tier for public web of module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Includes
 */
class Gui extends \Ergo\GuiAbstract
{
    /**
     * returns content of container by name
     * @param string name of html template file without extension
     * @return string
     */
    public function renderContainer($name)
    {
        if (substr($name, 0, 1) === '_') {
            $name = substr($name, 1);
        }
        $template = $this->_app->getTemplatePath($name.'.htm', Module::NAME);
        if (!is_file($template)) {
            return '';
        }

        $tpl = new Template($template, true);
        return $tpl->get();
    }

}
