<?php

/**
 * @package    Ergo
 * @subpackage Onlinetechnology
 */

namespace Onlinetechnology;

require_once dirname(__DIR__) . '/core/GstLib/Db/Mysqli.php';

use Dice\Dice;
use Ergo\Tracy\ApplicationPanel;
use GstLib\Template;
use Ergo\Pagination;
use Tracy\Debugger;

/**
 * application
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Onlinetechnology
 */
class Application extends \Ergo\Application {

    public function init(/* \Ergo\ConfigInterface */ $config, $currentUrl = null, $admin = false) {
        parent::init($config, $currentUrl, $admin);

		$this->dic = new Dice();
		$this->dic->addRule('*', ['substitutions' => ['Ergo\\ApplicationInterface' => $this], 'shared' => true]);

		$ruleDb = [
			'constructParams' => [[
				'server' => ERGO_DB_SERVER,
				'user' => ERGO_DB_USER,
				'password' => ERGO_DB_PWD,
				'database' => ERGO_DB_NAME,
				'persistent' => ERGO_DB_PERSISTENT,
				'table_prefix' => ERGO_DB_PREFIX
			]],
			'shared' => true
		];
		if (Debugger::isEnabled()) {
			ApplicationPanel::setApplication($this);
			$ruleDb['call'] = [
				['setQueryLogger', [['\Ergo\Tracy\DatabasePanel', 'logQuery']]]
			];
		}

		$this->dic->addRule('GstLib\Db\DriverMysqli', $ruleDb);

    }

    /**
     * returns current logged admin user or null
     * @return \Admins\User|null
     */
    public function getUser() {
        return $this->getModule('admins')->getUser();
    }

    /**
     * returns current logged user or null
     * @return \Eshop\Customers\Customer|null
     */
    public function getVisitor() {
        return $this->getModule('eshop')->getCustomer();
    }

    /**
     * overide method cron, generate sitemap xml
     * @param int $timeout minimal timeout between run
     */
    public function cron($timeout = 600) {
        parent::cron($timeout);

        // generate sitemap
        $batch = 0;
        $sitemapFiles = array();
        $dataDir = ERGO_DATA_DIR . 'core';
        if (!is_dir($dataDir)) {
            \GstLib\Filesystem::mkDir($dataDir, 0777);
        }
        if (!is_dir($dataDir)) {
            user_error('dir ' . $dataDir . ' not found', E_USER_WARNING);
            return;
        }

        $con = $this->dice()->create('GstLib\Db\DriverMysqli');

        $count = $con->count('SELECT COUNT(*) FROM [%core_sitemap%]');
        // sitemap can contains max 50 000 urls
        while ($batch == 0 || ($batch * 50000) < $count) {
            if (!ini_get('safe_mode')) {
                @set_time_limit(60);
            }
            $sql = 'SELECT url,last_mod,change_freq,priority
            FROM [%core_sitemap%] ORDER BY module,last_refresh LIMIT ' . ($batch * 50000) . ',50000';
            $tmpFile = ERGO_ROOT_DIR . 'cache/' . md5(uniqid('sitemap'));
            file_put_contents(
                    $tmpFile, '<?xml version="1.0" encoding="UTF-8"?>' .
                    '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
            );
            foreach ($con->fetchArrayAll($sql) as $row) {
                $row['last_mod'] = str_replace(' ', 'T', $row['last_mod']);
                $item = new \GstLib\SitemapItem($row['url'], $row['last_mod'], $row['change_freq'], $row['priority']);
                if ($item->isValid()) {
                    file_put_contents($tmpFile, $item->getXml(), FILE_APPEND);
                }
            }
            file_put_contents($tmpFile, '</urlset>', FILE_APPEND);

            $file = $dataDir . '/sitemap' . ($batch > 0 ? $batch : '') . '.xml';
            if (is_file($file)) {
                unlink($file);
            }

            \GstLib\Filesystem::gzipFile($tmpFile, $file . '.gz');
            unlink($tmpFile);
            $sitemapFiles[] = $file . '.gz';

            $batch++;
        }

        if (count($sitemapFiles) > 1) {
            $sitemap = new Gst_Sitemap();
            foreach ($sitemapFiles as $file) {
                $sitemap->addSitemap(
                        ERGO_ROOT_URL . 'data/core/' . basename($file), Gst_Date::unix2Iso(filemtime($file), true)
                );
            }
            $tmpFile = ERGO_ROOT_DIR . 'cache/' . md5(uniqid('sitemap_index'));
            $file = $dataDir . '/sitemap_index.xml';
            if (file_put_contents($tmpFile, $sitemap->getXmlSitemap()) !== false) {
                if (is_file($file)) {
                    unlink($file);
                }
                rename($tmpFile, $file);
            }
        }
    }

    /**
     * returns list of countries in format ISO 3166-1 code => name
     * @return array
     */
    public function countries() {
        return array(
            'CZ' => '[%LG_COUNTRY_CZ%]',
            'SK' => '[%LG_COUNTRY_SK%]',
            'PL' => '[%LG_COUNTRY_PL%]',
            'DE' => '[%LG_COUNTRY_DE%]',
            'AT' => '[%LG_COUNTRY_AT%]',
            'FR' => '[%LG_COUNTRY_FR%]',
            'CH' => '[%LG_COUNTRY_CH%]',
            'PT' => '[%LG_COUNTRY_PT%]',
            'ES' => '[%LG_COUNTRY_ES%]',
            'GB' => '[%LG_COUNTRY_GB%]'
        );
    }

    /**
     * returns name of country by code
     * @param string $countryCode
     * @return string
     */
    public function countryName($countryCode) {
        $options = $this->countries();
        if (isset($options[$countryCode])) {
            return $options[$countryCode];
        }
        return '';
    }

    /**
     * returns html code with pagination for administration
     * @param int $sumRows number of all records in list
     * @param int $pos current position in list
     * @param string $url url of page
     * @param int $rowsPage number of records per one page
     * @param string $posVar name of variable holding position
     * @return string
     */
    public function renderPagination($sumRows, $pos, $url, $rowsPage, $posVar = Pagination::POS_VARIABLE) {
        $pager = new Pagination($sumRows, $pos, $rowsPage);

        $tpl = new Template($this->getTemplatePath('pagination.htm'), true);

        $tmp = $pager->getUrlFirst($url, $posVar);
        $tpl->replace('URL_FIRST', $tmp, 'raw');
        $tpl->showSub('FIRST', $tmp != '');
        $tmp = $pager->getUrlPrev($url, $posVar);
        $tpl->replace('URL_PREV', $tmp, 'raw');
        $tpl->showSub('PREV', $tmp != '');
        $tmp = $pager->getUrlNext($url, $posVar);
        $tpl->replace('URL_NEXT', $tmp, 'raw');
        $tpl->showSub('NEXT', $tmp != '');
        $tmp = $pager->getUrlLast($url, $posVar);
        $tpl->replace('URL_LAST', $tmp, 'raw');
        $tpl->showSub('LAST', $tmp != '');

        $tmp = $pager->getOptions($url, $posVar, 100);
        //$tpl->replace('INPVAL_PAGES', $tmp, array('output' => 'raw', 'quiet' => true));
        $tpl->showSub('PAGES', count($pager->getPages()) > 1);
        $options['cssSelected'] = 'selected';
        $options['descPageNum'] = true;
        $tmp = $pager->getLinks($url, $posVar, 20, $options);
        $tpl->replace('LINKS_PAGES', $tmp, array('output' => 'raw', 'quiet' => true));

        $tmp = $pager->getRecords();
        $tpl->replace('TXT_CURRENT_POS', $tmp[0]);
        $tpl->replace('TXT_END_POS', $tmp[1]);
        $tpl->replace('TXT_SUM', $pager->getTotalRows());
        unset($pager);

        return $tpl->get();
    }

    /**
     * @return string currency code by current language
     */
    public function getCurrency() {
        return $this->currentLanguage() === 'en' ? 'EUR' : 'CZK';
    }

}
