<?php
/**
 * @package    Ergo
 * @subpackage Core
 */

namespace Core;

use GstLib\Template;

/**
 * GUI for module Core
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Core
 */
class Gui extends \Ergo\GuiAbstract
{
    /**
     * returns page for 404 error
     * @return string
     */
    public function render404()
    {
        $tpl = new Template($this->_app->getTemplatePath('404.htm'), true);
        return $tpl->get();
    }
}
