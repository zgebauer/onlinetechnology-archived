<?php

namespace Ergo\Tracy;

use Tracy\IBarPanel;

/**
 * tracy panel with database query statistics
 */
final class DatabasePanel implements IBarPanel {

	/**
	 * @var array
	 */
	private static $queryData = [];

	/**
	 * @inheritdoc
	 */
	public function getTab() {
		$totalQueries = count(self::$queryData);
		$totalTime = array_sum(array_column(self::$queryData, 'time'));

		// @codingStandardsIgnoreStart
		return '<span title="Database"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEYSURBVBgZBcHPio5hGAfg6/2+R980k6wmJgsJ5U/ZOAqbSc2GnXOwUg7BESgLUeIQ1GSjLFnMwsKGGg1qxJRmPM97/1zXFAAAAEADdlfZzr26miup2svnelq7d2aYgt3rebl585wN6+K3I1/9fJe7O/uIePP2SypJkiRJ0vMhr55FLCA3zgIAOK9uQ4MS361ZOSX+OrTvkgINSjS/HIvhjxNNFGgQsbSmabohKDNoUGLohsls6BaiQIMSs2FYmnXdUsygQYmumy3Nhi6igwalDEOJEjPKP7CA2aFNK8Bkyy3fdNCg7r9/fW3jgpVJbDmy5+PB2IYp4MXFelQ7izPrhkPHB+P5/PjhD5gCgCenx+VR/dODEwD+A3T7nqbxwf1HAAAAAElFTkSuQmCC" />queries:'
		// @codingStandardsIgnoreEnd
			. $totalQueries . ' / time: ' . round(($totalTime * 1000), 2) . '&nbsp;ms</span>';
	}

	/**
	 * @inheritdoc
	 */
	public function getPanel() {
		$occurs = [];
		foreach (self::$queryData as $key => $query) {
			$sqlHash = md5($query['sql']);
			self::$queryData[$key]['hash'] = $sqlHash;
			if (!array_key_exists($sqlHash, $occurs)) {
				$occurs[$sqlHash] = 0;
			}
			$occurs[$sqlHash]++;
		}

		$html = '';
		foreach (self::$queryData as $query) {
			$html .= '<tr><td>' . round(($query['time'] * 1000), 5)
				. '</td><td>' . $query['rows'] . '</td><td>' . $occurs[$query['hash']] . '</td><td>'
				. $query['sql'] . '</td></tr>';
		}

		// @codingStandardsIgnoreStart
		return '<h1>Database</h1><div class="nette-inner"><table><tr><th>Time&nbsp;ms</th><th>Rows</th><th>Occurs</th><th>SQL Statement</th></tr>'
		// @codingStandardsIgnoreEnd
			. $html . '</table></div>';
	}

	/**
	 * @param string $sql
	 * @param float $time
	 * @param int $affectedRows
	 */
	public static function logQuery($sql, $time, $affectedRows) {
		self::$queryData[] = ['sql' => $sql, 'time' => $time, 'rows' => $affectedRows];
	}

}
