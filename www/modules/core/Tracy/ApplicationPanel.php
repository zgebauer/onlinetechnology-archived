<?php

namespace Ergo\Tracy;

use Ergo\ApplicationInterface;
use Ergo\IApplication;
use Tracy\Debugger;
use Tracy\IBarPanel;

/**
 * tracy panel with application information
 */
final class ApplicationPanel implements IBarPanel {

	/**
	 * @var ApplicationInterface
	 */
	private static $application;

	/**
	 * @inheritdoc
	 */
	public function getTab() {
		return '<span title="Ergo">Ergo</span>';
	}

	/**
	 * @inheritdoc
	 */
	public function getPanel() {
		$html = '<h1>Ergo application</h1><div class="nette-inner"><table>';
		$html .= '<tr><td>Error: e-mail</td><td>' . Debugger::$email . '</td></tr><tr><td>Error: allowed IP</td><td>'
			. ERGO_DEVEL_ADDRESS . '</td></tr></table></div>';
		return $html;
	}

	public static function setApplication(ApplicationInterface $application) {
		self::$application = $application;
	}
}
