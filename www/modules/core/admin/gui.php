<?php
/**
 * @package    Ergo
 * @subpackage Core
 */

namespace Core;

use GstLib\Template;

/**
 * GUI for module Core
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Core
 */
class GuiAdmin
{
    /** @var \Ergo\ApplicationInterface application */
    protected $_app;

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application application object
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        $this->_app = $application;
    }

    /**
     * returns box with menu
     * expects menu items collected from method menuItems() from each module
     *
     * @param array $items menu items
     * @return string
     */
    public function renderBoxMenu(array $items)
    {
        $tpl = new Template($this->_app->getTemplatePath('box_menu.htm', Module::NAME, true), true);
        $cnt = '';
        $tplRow = new Template();
        $rowTemplate = $tpl->getSub('ITEMS');
        foreach ($items as $moduleLinks) {
            foreach ($moduleLinks as $link) {
                $tplRow->set($rowTemplate);
                $tplRow->replace('TXT_MENU', $link['name']);
                $tplRow->replace('URL_MENU', $link['url']);
                $tplRow->replace('TXT_TITLE', $link['title']);
                $css = (isset($link['css']) && $link['css'] !== '' ? ' class="'.$link['css'].'"' : '');
                $tplRow->replace('CSS', $css, 'raw');
                $cnt .= $tplRow->get();
            }
        }
        unset($tplRow);
        if ($cnt === '') {
            return '';
        }
        $tpl->replaceSub('ITEMS', $cnt);
        return $tpl->get();
    }

}