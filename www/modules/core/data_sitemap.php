<?php
/**
 * @package    Ergo
 * @subpackage Core
 */

namespace Core;

use GstLib\Sitemap;

/**
 * data tier for module - sitemap
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Core
 */
class DataSitemap
{
    /** @var \GstLib\Db\DriverMysqli $connection */
    private $_con;

    /** @var string name of table with sitemap index */
    private static $_tableNameIndex = '[%core_sitemap%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection, \Ergo\ApplicationInterface $application)
    {
        $this->_con = $connection;
        $this->_app = $application;
    }

    /**
     * refresh urls of module in table sitemap
     */
    public function refreshLinks()
    {
        $date =  new \DateTime('now', new \DateTimeZone('UTC'));
        $refresh = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO [%core_sitemap%] (module,last_refresh,url,
        change_freq,priority) VALUES (".$this->_con->escape(Module::NAME).
        ",'".$refresh."','".ERGO_ROOT_URL."','daily',1)";
        $this->_con->query($sql);

        $sql = 'DELETE FROM '.self::$_tableNameIndex.'
        WHERE module='.$this->_con->escape(Module::NAME).' AND last_refresh<>'.$this->_con->escape($refresh);
        $this->_con->query($sql);
    }

}