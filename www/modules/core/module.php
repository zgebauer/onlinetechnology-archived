<?php
/**
 * @package    Ergo
 * @subpackage Core
 */

namespace Core;

require_once __DIR__.'/gui.php';
require_once __DIR__.'/admin/gui.php';
require_once 'data_sitemap.php';

use Ergo\Response;
use GstLib\Template;

/**
 * main class of module Core
 * base module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Core
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'core';

    /**
     * returns output of module by given parameter:<br />
     * '' - homepage
     * default 404 page
     */
    public function output($parameter = '')
    {
        switch($parameter) {
        case '':
            $this->_renderHomepage();
            return;
        }
        return $this->_render404();
    }

    /**
     * returns output of module by given parameter:<br />
     * '' - homepage
     * '_box_menu_' - administartion menu
     * default 404 page
     */
    public function outputAdmin($parameter = '')
    {
        switch($parameter) {
        case '_box_menu_':
            return $this->_renderBoxMenu();
        case '':
            return $this->_renderHomepageAdmin();
        }
        return $this->_render404();
    }

    public function name()
    {
        return 'Core';
    }

    public function refreshSitemapLinks()
    {
        $this->_app->dice()->create('\Core\DataSitemap', array($this->_app))->refreshLinks();
    }

    /**
     * render default homepage
     */
    private function _renderHomepage()
    {
        $this->_app->response()->pageTitle('[%LG_DEFAULT_META_TITLE%]');
        $this->_app->response()->metaDescription('[%LG_DEFAULT_META_DESCRIPTION%]');
        $this->_app->response()->metaKeywords('[%LG_DEFAULT_META_KEYWORDS%]');
        $this->_app->response()->canonicalUrl($this->_app->baseUrl());
        $tpl = new Template($this->_app->getTemplatePath('home.htm', ''), true);
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * render default homepage
     */
    private function _renderHomepageAdmin()
    {
        $this->_app->response()->pageTitle('[%LG_DEFAULT_META_TITLE%]');
        $tpl = new Template($this->_app->getTemplatePath('home.htm', '', true), true);
        $this->_app->response()->pageContent($tpl->get());
    }

    /**
     * returns page for 404 error
     * @return string
     */
    private function _render404()
    {
        $this->_app->response()->httpCode(Response::HTTP_NOT_FOUND);
        $this->_app->response()->pageTitle('[%LG_DEFAULT_META_TITLE%]');
        $gui = new Gui($this->_app);
        $this->_app->response()->pageContent($gui->render404());
    }

    private function _renderBoxMenu()
    {
        if (is_null($this->_app->getUser())) {
            return '';
        }

        $menuItems = array();
        foreach ($this->_app->getModules() as $moduleName) {
            $module = $this->_app->getModule($moduleName);
            $items = $module->menuItems();
            if (is_null($items)) {
                continue;
            }
            $menuItems[$moduleName] = $items;
        }

        $gui = new GuiAdmin($this->_app);
        return $gui->renderBoxMenu($menuItems);
    }

}