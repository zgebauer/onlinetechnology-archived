<?php

namespace Articles;

use GstLib\Vars;
use GstLib\Html;

/**
 * static article with fixed position on web
 *
 * @package    Ergo
 * @subpackage Articles
 */
class Article {

    /** @var string last error message */
    protected $_err;

    /** @var int instance identifier */
    protected $_id;

    /** @var bool flag "loaded from data tier" */
    protected $_loaded;

    /** @var string name */
    protected $_name;

    /** @var string identifier for url */
    protected $_seo;

    /** @var date date of last change of record */
    protected $_lastChange;

    /** @var string text of record */
    protected $_text;

    /** @var string content for meta tag description */
    protected $_metaDescription;

    /** @var string content for meta tag keywords */
    protected $_metaKeywords;

    /** @var string language code of article */
    private $lang;

    /** @var \Articles\Data data layer */
    protected $_dataLayer;

    /**
     * create instance and fill it with data by given identifier
     * @param Data $dataLayer
     * @param int $instanceId identifier of instance
     */
    public function __construct(Data $dataLayer, $instanceId = 0) {
        $this->_err = '';
        $this->_id = 0;
        $this->_loaded = false;
        $this->_name = $this->_seo = '';
        $this->_lastChange = null;
        $this->_text = '';
        $this->_metaDescription = $this->_metaKeywords = '';
        $this->lang = 'cs';

        $this->_dataLayer = $dataLayer;

        $this->instanceId($instanceId);
        if ($this->_id != 0) {
            $this->load();
        }
    }

    /**
     * returns last error message
     * @return string
     */
    public function err() {
        return $this->_err;
    }

    /**
     * returns and sets instance identifier
     * @param int $value
     * @return int
     */
    public function instanceId($value = null) {
        if (!is_null($value)) {
            $value = (int) $value;
            if ($value > 0) {
                $this->_id = $value;
            }
        }
        return $this->_id;
    }

    /**
     * returns flag "record loaded"
     * @return bool
     */
    public function loaded() {
        return $this->_loaded;
    }

    /**
     * returns and sets name
     * @param string $value
     * @return string
     */
    public function name($value = null) {
        if (!is_null($value)) {
            $this->_name = Vars::substr($value, 0, 70);
        }
        return $this->_name;
    }

    /**
     * returns and sets seo
     * @param string $value
     * @return string
     */
    public function seo($value = null) {
        if (!is_null($value)) {
            $this->_seo = Vars::substr(Html::fixForUrl($value), 0, 72);
        }
        return $this->_seo;
    }

    /**
     * returns and sets text of article
     * @param string $value
     * @return string text
     */
    public function text($value = null) {
        if (!is_null($value)) {
            $this->_text = $value;
        }
        return $this->_text;
    }

    /**
     * returns and sets meta description
     * @param string $value
     * @return string
     */
    public function metaDescription($value = null) {
        if (!is_null($value)) {
            $this->_metaDescription = Vars::substr($value, 0, 255);
        }
        return $this->_metaDescription;
    }

    /**
     * returns and sets meta keywords
     * @param string $value
     * @return string
     */
    public function metaKeywords($value = null) {
        if (!is_null($value)) {
            $this->_metaKeywords = Vars::substr($value, 0, 255);
        }
        return $this->_metaKeywords;
    }

    /**
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return Article
     */
    public function setLang($lang) {
        $this->lang = Vars::substr($lang, 0, 2);
        return $this;
    }

    /**
     * fill instance from data tier
     * @return bool false on error
     */
    public function load() {
        $this->_loaded = $this->_dataLayer->load($this);
        return $this->_loaded;
    }

    /**
     * save instance to data tier
     * @return bool false on error
     */
    public function save() {
        $this->_err = '';
        $required = array();
        if ($this->_name === '') {
            $required[] = '[%LG_NAME%]';
        }
        if ($this->_text === '') {
            $required[] = '[%LG_TEXT%]';
        }
        if (isset($required[0])) {
            $this->_err = '[%LG_ERR_MISSING_VALUES%] ' . join(', ', $required);
            return false;
        }
        if ($this->_seo === '') {
            $this->seo($this->_name);
        }
        if ($this->_metaDescription === '') {
            $this->metaDescription($this->_name);
        }
        if ($this->_metaKeywords === '') {
            $this->metaKeywords(join(', ', Vars::parseKeywords($this->_name)));
        }

        return $this->_dataLayer->save($this);
    }

    /**
     * delete instance from data tier
     * @return bool false on error
     */
    public function delete() {
        return $this->_dataLayer->delete($this);
    }

}
