<?php
/**
 * @package    Ergo
 * @subpackage Articles
 */

namespace Articles;

/**
 * configuration of module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Articles
 */
class Config
{
  

    /**
     * returns absolute path to directory for files uploaded via tinymce
     * @return string
     */
    public function dirUserFiles()
    {
        return ERGO_DATA_DIR.'soubory/';
    }


}