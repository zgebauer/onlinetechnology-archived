<?php
/**
 * @package    Ergo
 * @subpackage Articles
 */

namespace Articles;

require_once 'data_sitemap.php';

/**
 * main class of module Articles
 * Management of static articles
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Articles
 */
class Module extends \Ergo\ModuleAbstract
{
    /** @var module name */
    const NAME = 'articles';

    /**
     * constructor
     * @param \Ergo\ApplicationInterface $application current application
     */
    public function __construct(\Ergo\ApplicationInterface $application)
    {
        parent::__construct($application);

        $this->_app->setShared('\Articles\Config');
        $this->_app->setShared('\Articles\Data');
        $this->_app->setShared('\Articles\DataSitemap');
    }

    public function menuItems()
    {
        $user = $this->_app->getUser();
        if (is_null($user) || !$user->hasPermission(self::NAME)) {
            return null;
        }
        return array(
            array(
               'name' => $this->name(),
               'url' => $this->_app->getUrl(self::NAME),
               'title' => 'Seznam článků',
               'highlight' => ($this->_app->getCurrentModule() == self::NAME)
            )
        );
    }

    public function permissions()
    {
        return array();
    }

    public function output($parameter = '')
    {
        $seo = $this->seoStrings();
        switch($parameter) {
        default:
            if (substr($parameter, 0, 8) === '_urlseo_') {
                $articleId = trim(substr($parameter, 8));
                return (isset($seo[$articleId]) ? $this->_app->getUrl(self::NAME, $seo[$articleId])  : '');
            }
            if (substr($parameter, 0, 5) === '_box_') {
                return $this->_renderArticleAsBox(intval(substr($parameter, 5)));
            }
            if ($parameter != '') {
                $tmp = array_flip($seo);
                if (isset($tmp[$parameter])) {
                    $this->_renderArticle($tmp[$parameter], $parameter);
                    return;
                }
            }
        }
        return parent::output($parameter);
    }

    public function outputAdmin($parameter = '')
    {
        $map = array(
            '' => array('\Articles\Admin\Controller', 'renderList'),
            'ajax-list' => array('\Articles\Admin\Controller', 'handleList'),
            'ajax-edit' => array('\Articles\Admin\Controller', 'handleEdit'),
            'ajax-save' => array('\Articles\Admin\Controller', 'handleSave'),
            'ajax-delete' => array('\Articles\Admin\Controller', 'handleDelete')
        );

        if (isset($map[$parameter])) {
            return call_user_func(array($this->_app->dice()->create($map[$parameter][0]), $map[$parameter][1]));
        }
        return parent::outputAdmin($parameter);
    }

    public function name()
    {
        return 'Články';
    }

    public function refreshSitemapLinks()
    {
        $this->_app->dice()->create('\Articles\DataSitemap', array($this->_app))->refreshLinks();
    }

    private function _renderArticle($articleId, $seo)
    {
         $record = $this->_app->dice()->create('\Articles\Article', array($articleId));

        $this->_app->response()->pageTitle($record->name().' | [%LG_SITENAME%]');
        $this->_app->response()->metaDescription($record->metaDescription());
        $this->_app->response()->metaKeywords($record->metaKeywords());
        $this->_app->response()->canonicalUrl($this->_app->getUrl(self::NAME, $seo));
        $this->_app->response()->setMetaProperty('og:type', 'article');

        $gui = new Gui($this->_app);
        $this->_app->response()->pageContent($gui->renderArticle($record));
    }

    private function _renderArticleAsBox($articleId)
    {
        $record = $this->_app->dice()->create('\Articles\Article', array($articleId));
        $gui = new Gui($this->_app);
        return $gui->renderArticleAsBox($record);
    }

    public function navigationLinks()
    {
        return $this->_app->dice()->create('\Articles\Data', array($this->_app))->getMenuLinks();
    }

    /**
     * returns seo strings for all articles
     * @return array
     */
    public function seoStrings()
    {
        static $seoStrings;
        if (!isset($seoStrings)) {
            $seoStrings = array();
            foreach ($this->_app->dice()->create('\Articles\Data', array($this->_app))->getRecords() as $record) {
                $seoStrings[$record->instanceId()] = $record->seo();
            }
        }
        return $seoStrings;
    }

}