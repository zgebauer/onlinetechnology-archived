<?php
/**
 * @package    Ergo
 * @subpackage Articles
 */

namespace Articles;

use GstLib\Html;
use GstLib\Template;


/**
 * presentation tier for public web of module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Articles
 */
class Gui extends \Ergo\GuiAbstract
{
    /**
     * render page with article
     * @param \Articles\Article $record
     * @return string
     */
    public function renderArticle(Article $record)
    {
        $tpl = new Template($this->_app->getTemplatePath('article.htm', Module::NAME), true);
        $tpl->replace('TXT_NAME', $record->name());
        $tpl->replace('TXT_TEXT', Html::urlToAbs($record->text(), $this->_app->baseUrl()), 'raw');
        return $tpl->get();
    }

    /**
     * render box with article
     * @param \Articles\Article $record
     * @return string
     */
    public function renderArticleAsBox(Article $record)
    {
        $tpl = new Template($this->_app->getTemplatePath('box.htm', Module::NAME), true);
        $tpl->replace('TXT_TEXT', Html::urlToAbs($record->text(), $this->_app->baseUrl()), 'raw');
        return $tpl->get();
    }

}
