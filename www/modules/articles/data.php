<?php

/**
 * @package    Ergo
 * @subpackage Articles
 */

namespace Articles;

/**
 * data tier for module
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage Articles
 */
class Data {

    /** @var \GstLib\Db\DriverAbstract $connection */
    private $_con;
    private static $_tableName = '[%articles%]';

    /**
     * constructor
     * @param \GstLib\Db\DriverMysqli $connection
     */
    public function __construct(\GstLib\Db\DriverMysqli $connection) {
        $this->_con = $connection;
    }

    /**
     * fill given instance from database
     * @param \Articles\Article $record
     * @return bool false on error
     */
    public function load(Article $record) {
        $sql = 'SELECT name,seo,last_change,text,meta_desc,meta_keys,`lang`
        FROM ' . self::$_tableName . ' WHERE id=' . $record->instanceId();
        $row = $this->_con->fetchArray($sql);
        $this->_con->free();
        if (!$row) {
            return false;
        }
        $record->name($row['name']);
        $record->seo($row['seo']);
        $record->text($row['text']);
        $record->metaDescription($row['meta_desc']);
        $record->metaKeywords($row['meta_keys']);
        $record->setLang($row['lang']);
        return true;
    }

    /**
     * save given instance to database
     * @param \Articles\Article $record
     * @return bool false on error
     */
    public function save(Article $record) {
        // seo have to be unique
        $uniq = false;
        $count = 0;
        $uniqSeo = '';
        do {
            $sql = 'SELECT COUNT(*) FROM ' . self::$_tableName . '
            WHERE seo=' . $this->_con->escape($record->seo() . $uniqSeo) . ' AND id<>' . $record->instanceId();
            if ($this->_con->count($sql) == 0) {
                $uniq = true;
            } else {
                $uniqSeo = '-' . ($count++);
            }
        } while (!$uniq);
        $record->seo($record->seo() . $uniqSeo);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $items = array();
        if ($record->instanceId() === 0) {
            $items['name'] = $this->_con->escape($record->name());
            $items['seo'] = $this->_con->escape($record->seo());
            $items['text'] = $this->_con->escape($record->text());
            $items['meta_desc'] = $this->_con->escape($record->metaDescription());
            $items['meta_keys'] = $this->_con->escape($record->metaKeywords());
            $items['last_change'] = $this->_con->escape($date->format('Y-m-d H:i:s'));
            $items['`lang`'] = $this->_con->escape($record->getLang());
            $sql = 'INSERT INTO ' . self::$_tableName . ' ('
                    . implode(',', array_keys($items)) . ') VALUES (' . implode(',', $items) . ')';
            try {
                $record->instanceId($this->_con->insert($sql));
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        } else {
            $items[] = 'name=' . $this->_con->escape($record->name());
            $items[] = 'seo=' . $this->_con->escape($record->seo());
            $items[] = 'text=' . $this->_con->escape($record->text());
            $items[] = 'last_change=' . $this->_con->escape($date->format('Y-m-d H:i:s'));
            $items[] = 'meta_desc=' . $this->_con->escape($record->metaDescription());
            $items[] = 'meta_keys=' . $this->_con->escape($record->metaKeywords());
            $items[] = '`lang`=' . $this->_con->escape($record->getLang());
            $sql = 'UPDATE ' . self::$_tableName . ' SET ' . join(',', $items) . ' WHERE id=' . $record->instanceId();
            try {
                $this->_con->query($sql);
            } catch (\GstLib\Db\DatabaseErrorException $exception) {
                $record->err('[%LG_ERR_SAVE_FAILED%]');
                \Ergo\Application::logException($exception);
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * delete given instance from database
     * @param \Articles\Article $record
     * @return bool false on error
     */
    public function delete(Article $record) {
        try {
            $sql = 'DELETE FROM ' . self::$_tableName . ' WHERE id=' . $record->instanceId();
            $this->_con->query($sql);
        } catch (\GstLib\Db\DatabaseErrorException $exception) {
            $record->err('[%LG_ERR_DELETE_FAILED%]');
            \Ergo\Application::logException($exception);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * returns records as array of instances
     * @param int $start start position
     * @param int $rows number of returned instances (null = all)
     * @param string $order name of column for sorting
     * @param bool $desc true/false sort descending/ascending
     * @param array $criteria array of parts of WHERE clause
     * @return array
     */
    public function getRecords($start = 0, $rows = null, $order = '', $desc = false, array $criteria = array()) {
        $sql = 'SELECT id,name,seo,`lang` FROM ' . self::$_tableName .
                $this->_con->where($criteria) . $this->_con->orderBy($order, $desc) . $this->_con->limit($start, $rows);
        $ret = array();
        foreach ($this->_con->fetchArrayAll($sql) as $row) {
            $tmp = new Article($this);
            $tmp->instanceId($row['id']);
            $tmp->name($row['name']);
            $tmp->seo($row['seo']);
            $tmp->setLang($row['lang']);
            $ret[] = $tmp;
        }
        return $ret;
    }

    /**
     * returns number of records matching given criteria
     * @param array $criteria array of parts of WHERE clause
     * @return int
     */
    public function count(array $criteria = array()) {
        return $this->_con->count('SELECT COUNT(*) FROM ' . self::$_tableName . $this->_con->where($criteria));
    }

    /**
     * returns navigation links
     * @param array $criteria
     * @return array
     */
    public function getMenuLinks(array $criteria = array()) {
        $ret = array();
        foreach ($this->getRecords(0, NULL, 'name', FALSE, $criteria) as $record) {
            $ret['_urlseo_' . $record->instanceId()] = $record->name() . ' (' . $record->getLang() . ')';
        }
        return $ret;
    }

}
