<?php

/**
 * run scheduled tasks
 *
 * @package Ergo
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 */
require_once dirname(__DIR__) . '/config/config.php';
require_once ERGO_ROOT_DIR . 'vendor/autoload.php';
require_once ERGO_ROOT_DIR.'core/base.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';

ergoSetEnvironment();
ergoSetErrHandler();

$config = new Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
$config->setUrlRewriteEnabled(ERGO_REWRITE_ENABLED);
$config->mode(ERGO_RUN_MODE);
$config->defaultLanguage('cs');
$config->facade('onlinetechnology');
$config->isAdmin(false);
$config->adminPath('admin');
$config->translate(true);
$config->moduleAliases(
    array(
        'articles' => 'clanky',
        'staticarticles' => 'stranky'
    )
);

$app = new Onlinetechnology\Application();
$app->init($config);
$app->cron(600);
echo date('c');