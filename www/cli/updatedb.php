<?php
/**
 * process SQL to update database
 *
 * Expects sql files in directory DIR_SQL with names in format currentversion-targetversion.sql
 * (eg. 0-1.sql or 2-1.sql). Only integer are allowed as database version numbers. Numbering sql files
 * for "forward updates" must be continuous (0-1, 1-2, 2-3 etc). Sql files for "revert updates" are optional.
 *
 * Each sql command in sql files must ends with semicolon and EOL (LF, not CRLF).
 *
 * Target database must exists before run this script, because script create new table with version number.
 *
 * @package Ergo
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 */

require_once dirname(__DIR__).'/config/config.php';

/** required database version */
define('TARGET_VERSION', 7);
/** directory with sql files */
define('DIR_SQL', ERGO_ROOT_DIR.'db/');
/** log file */
define('LOGFILE', ERGO_LOG_DIR.'updatedb.log');
/** name of table with database version */
define('TABLE_VERSION', 'db_version');

file_put_contents(LOGFILE, date('Y-m-d H:i:s').' update to '.TARGET_VERSION."\n", FILE_APPEND);

$conn = mysqli_connect(ERGO_DB_SERVER, ERGO_DB_USER, ERGO_DB_PWD, ERGO_DB_NAME);
if (!$conn) {
    logMsg(mysqli_connect_errno().':'.mysqli_connect_error());
}
if (!mysqli_query($conn, 'SET CHARACTER SET '.ERGO_DB_CHARSET)) {
    processError();
}
if (!mysqli_query($conn, 'SET NAMES '.ERGO_DB_CHARSET)) {
    processError();
}

// get current version from db
$currentVersion = 0;
$sql = "SHOW TABLES LIKE '".TABLE_VERSION."'";
if (($result = mysqli_query($conn, $sql)) === false) {
    processError();
}
if (mysqli_num_rows($result) === 0) {
    runQuery("CREATE TABLE `".TABLE_VERSION."` (`version` INT(10) UNSIGNED NOT NULL) ENGINE=MyISAM");
    runQuery("INSERT INTO `".TABLE_VERSION."` (`version`) VALUES(0)");
} else {
    $sql = "SELECT version FROM `".TABLE_VERSION."`";
    if (($result = mysqli_query($conn, $sql)) === false) {
        processError();
    }
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $currentVersion = (int) $row['version'];
}
file_put_contents(LOGFILE, 'Current version '.$currentVersion."\n", FILE_APPEND);

// forward update
if (TARGET_VERSION > $currentVersion) {
    while ($currentVersion < TARGET_VERSION) {
        $nextVersion = $currentVersion+1;
        $fileDelta = DIR_SQL.$currentVersion.'-'.$nextVersion.'.sql';
        if (is_file($fileDelta)) {
            processSql($fileDelta);
            runQuery("UPDATE `".TABLE_VERSION."` SET `version`=".$nextVersion);
            $currentVersion = $nextVersion;
        } else {
            logMsg('File '.$fileDelta.' not found');
        }
    }
} elseif (TARGET_VERSION < $currentVersion) {
    // revert
    while ($currentVersion > TARGET_VERSION) {
        $prevVersion = $currentVersion-1;
        $fileDelta = DIR_SQL.$currentVersion.'-'.$prevVersion.'.sql';
        if (is_file($fileDelta)) {
            processSql($fileDelta);
            runQuery("UPDATE `".TABLE_VERSION."` SET `version`=".$prevVersion);
        }
        $currentVersion = $prevVersion;
    }
}

echo date('Y-m-d H:i:s');

/**
 * run sql command
 * @global mysqli $conn
 * @param string $sql
 * @return boolean
 */
function runQuery($sql)
{
    global $conn;
    if (!mysqli_query($conn, $sql)) {
        processError();
    }
    return true;
}

/**
 * proccess sql file
 * @param string $file
 * @return boolean
 */
function processSql($file)
{
    $currentDelimiter = ';';
    $sqlCommands = array();
    $lines = file($file);
    $query = '';

    foreach ($lines as $line) {
        $line = trim($line);
        if (substr($line, 0, 2) === '--') { // ignore comments
            continue;
        }
        if (strtoupper(substr($line, 0, 9)) === 'DELIMITER') {
            $currentDelimiter = trim(str_ireplace('DELIMITER', '', $line));
            continue;
        }
        if ($line === $currentDelimiter) {
            $sqlCommands[] = $query;
            $query = '';
            continue;
        }

        $query .= $line."\n";
        if (substr($line, -1*strlen($currentDelimiter)) === $currentDelimiter) {
            $sqlCommands[] = trim($query);
            $query = '';
        }
    }

    foreach ($sqlCommands as $sql) {
        runQuery($sql);
    }
    return true;
}

/**
 * log message to file and stdout and stop execution
 * @param string $msg
 */
function logMsg($msg)
{
    file_put_contents(LOGFILE, $msg."\n", FILE_APPEND);
    die($msg);
}

/**
 * pricess mysqli error
 * @global mysqli $conn
 */
function processError()
{
    global $conn;
    $err = mysqli_error($conn);
    logMsg($err);
}
