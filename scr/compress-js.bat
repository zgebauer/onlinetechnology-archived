rem vytvoreni minifikovane verze js

set java="c:\ProgramData\Oracle\Java\javapath\java.exe" -jar
set yuc=%java% c:\bin\yuicompressor\yuicompressor-2.4.7.jar
set folder=d:\projekty\onlinetechnology\www\
set tmpjs=%folder%tmp.js

rem public web
set targetjs=%folder%facades\onlinetech\default.min.js
copy/b ^
    %folder%other\jquery\jquery.colorbox-min.js + ^
    %folder%other\jquery\easySlider1.7.js + ^
    %folder%other\jquery\jquery.bxslider.min.js + ^
    %folder%other\jquery\jquery.animate_from_to-1.0-custom.js + ^
    %folder%other\jquery\jquery.autocomplete.min.js + ^
    %folder%lang\cs.js + ^
    %folder%core\core.js + ^
    %folder%modules\eshop\scr.js + ^
    %folder%modules\search\scr.js + ^
    %folder%facades\onlinetech\scr.js ^
    %tmpjs%

%yuc% -v -o %targetjs% --charset utf-8 %tmpjs%
del %tmpjs%
