<?php
/**
 * @package    Ergo
 * @subpackage OnlinetechnologyTest
 */

/**
 * overide \Onlinetechnology\Application
 * provide additional functions for testing
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage OnlinetechnologyTest
 */
class TestApplication extends \Onlinetechnology\Application
{
    const TEST_USER_ID = 1;
    const TEST_VISITOR_ID = 1;
    const FACADE = 'onlinetechnology';
    const ADMIN_PATH = 'admin';
    const SESSION_UID = 'uid'; // same as \Admins\Module::SESSION_USER_ID
    const SESSION_VID = 'vid'; // same as \Users\Users::SESSION_USER_ID

    private $_dbTest;

    public function createBaseTables()
    {
        $this->_dbTest = $this->dice()->create('GstLib\Db\DriverMysqli');
        $this->_dbTest->query('DROP TABLE IF EXISTS core_users');
        $this->_dbTest->query('DROP TABLE IF EXISTS core_permissions');
        $this->_dbTest->query('DROP TABLE IF EXISTS core_sitemap');
        $this->_dbTest->query('DROP TABLE IF EXISTS core_search_index');
        $this->_dbTest->query('CREATE TABLE core_users LIKE '.ERGO_DB_NAME_PHPUNIT.'.core_users');
        $this->_dbTest->query('CREATE TABLE core_permissions LIKE '.ERGO_DB_NAME_PHPUNIT.'.core_permissions');
        $this->_dbTest->query('CREATE TABLE core_sitemap LIKE '.ERGO_DB_NAME_PHPUNIT.'.core_sitemap');
        $this->_dbTest->query('CREATE TABLE core_search_index LIKE '.ERGO_DB_NAME_PHPUNIT.'.core_search_index');

    }

    public function tearDown()
    {
        if (isset($this->_dbTest)) {
            $this->_dbTest->query('DROP TABLE IF EXISTS core_users');
            $this->_dbTest->query('DROP TABLE IF EXISTS core_permissions');
            $this->_dbTest->query('DROP TABLE IF EXISTS core_sitemap');
            //$this->_dbTest->query('DROP TABLE IF EXISTS users');
            $this->_dbTest->close();
        }
        if (isset($_SESSION[self::SESSION_UID])) {
            unset($_SESSION[self::SESSION_UID]);
        }
        if (isset($_SESSION[self::SESSION_VID])) {
            unset($_SESSION[self::SESSION_VID]);
        }
        unset($_GET);
        unset($_POST);
        unset($_SESSION);
        \GstLib\Filesystem::rmDir(ERGO_DATA_DIR);
    }

    public function logTestUser()
    {
        $this->_dbTest->query('TRUNCATE TABLE core_users');
        $this->_dbTest->query("INSERT INTO core_users (id,login,pwd,name)
        VALUES(".self::TEST_USER_ID.",'login',"
            .$this->_dbTest->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').",'name')");
        $_SESSION[self::SESSION_UID] = self::TEST_USER_ID;
    }

    public function logTestVisitor()
    {
        $this->_dbTest->query('TRUNCATE TABLE users');
        $this->_dbTest->query("INSERT INTO users (id,email,pwd,name,register_date)
        VALUES(".self::TEST_USER_ID.",'test@example.org',"
            .$this->_dbTest->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').",'name',NOW())");
        $_SESSION[self::SESSION_VID] = self::TEST_VISITOR_ID;
    }

    public function addPermission($userId, $module, $permission = '')
    {
        $this->_dbTest->query("INSERT INTO core_permissions (user_id,module,permission)
            VALUES(".$userId.",".$this->_dbTest->escape($module).",".$this->_dbTest->escape($permission).")");
    }

    public function logoutTestUser()
    {
        $this->_dbTest->query('TRUNCATE TABLE core_users');
        $this->_dbTest->query('TRUNCATE TABLE core_permissions');
        unset($_SESSION[self::SESSION_UID]);
    }

}
