<?php
/**
 * @package    Ergo
 * @subpackage EshopTest
 */

namespace Eshop;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'core/GstLib/Tree.php';
require_once ERGO_ROOT_DIR.'core/GstLib/UserFile.php';
require_once ERGO_ROOT_DIR.'core/GstLib/Resizer.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/eshop/module.php';
require_once ERGO_ROOT_DIR.'modules/eshop/config.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/category.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/categories.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/product.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/dataproduct.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/datacategory.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/dataicon.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/dataimage.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/datagalleryimage.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/galleryimage.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/image.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/icon.php';
require_once ERGO_ROOT_DIR.'modules/eshop/dataconfig.php';
require_once ERGO_ROOT_DIR.'modules/eshop/admin/controllerproducts.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

use GstLib\Html;
use Ergo\Response;


/**
 * test class for module Eshop
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage EshopTest
 */
class AdminProductsTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Eshop\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $config->defaultLanguage('cs');
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Eshop\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
        $this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_icon');
        $this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_cross');
        $this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_variant');
        $this->_db->query('DROP TABLE IF EXISTS eshop_carts');
        $this->_db->query('DROP TABLE IF EXISTS eshop_products');
        $this->_db->query('DROP TABLE IF EXISTS eshop_categories');
        $this->_db->query('DROP TABLE IF EXISTS eshop_products_images');
        $this->_db->query('DROP TABLE IF EXISTS eshop_products_icons');
        $this->_db->query('DROP TABLE IF EXISTS eshop_config');

        $this->_db->query('CREATE TABLE eshop_config LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_config');
        $this->_db->query('CREATE TABLE eshop_products LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products');
        $this->_db->query('CREATE TABLE eshop_categories LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_categories');
        $this->_db->query('CREATE TABLE eshop_rel_product_variant LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_variant');
        $this->_db->query('ALTER TABLE eshop_rel_product_variant ADD CONSTRAINT `FK_eshop_rel_product_variant_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->_db->query('ALTER TABLE eshop_rel_product_variant ADD CONSTRAINT `FK_eshop_rel_product_variant_eshop_products_2` FOREIGN KEY (`variant_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->_db->query('CREATE TABLE eshop_products_images LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products_images');
        $this->_db->query('CREATE TABLE eshop_products_icons LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products_icons');
        $this->_db->query('CREATE TABLE eshop_rel_product_icon LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_icon');
        $this->_db->query('ALTER TABLE `eshop_rel_product_icon` ADD CONSTRAINT `FK_eshop_rel_product_icon_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->_db->query('CREATE TABLE eshop_rel_product_cross LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_cross');
        $this->_db->query('ALTER TABLE eshop_rel_product_cross ADD CONSTRAINT `FK_eshop_rel_product_cross_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->_db->query('ALTER TABLE eshop_rel_product_cross ADD CONSTRAINT `FK_eshop_rel_product_cross_eshop_products_2` FOREIGN KEY (`cross_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->_db->query('CREATE TABLE eshop_carts LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_carts');
        $this->_db->query('ALTER TABLE eshop_carts ADD CONSTRAINT `FK_eshop_carts_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
    }

    protected function tearDown()
    {
        $this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_icon');
        $this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_cross');
        $this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_variant');
        $this->_db->query('DROP TABLE IF EXISTS eshop_carts');
        $this->_db->query('DROP TABLE IF EXISTS eshop_products');
        $this->_db->query('DROP TABLE IF EXISTS eshop_categories');
        $this->_db->query('DROP TABLE IF EXISTS eshop_products_images');
        $this->_db->query('DROP TABLE IF EXISTS eshop_products_icons');
        $this->_db->query('DROP TABLE IF EXISTS eshop_config');
        $this->_db->close();
        $this->_app->tearDown();
    }

    public function testListAsNotLogged()
    {
        $this->_object->outputAdmin('products');
        $this->assertEquals(302, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
    }

    public function testListAsLogged()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 0;
        $items['name'] = $this->_db->escape('category1');
        $items['seo'] = $this->_db->escape('seo1');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['id'] = 2;
        $items['parent_id'] = 1;
        $items['name'] = $this->_db->escape('category2');
        $items['seo'] = $this->_db->escape('seo2');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        for ($i = 1; $i < 22; $i++) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $items = array();
            $items['id'] = $i;
            $items['name'] = $this->_db->escape('icon'.$tmp);
            $items['text'] = $this->_db->escape('text'.$i);
            $sql = 'INSERT INTO eshop_products_icons ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            $this->_db->query($sql);
        }

        $this->_object->outputAdmin('products');
        $output = $this->_app->response()->pageContent();
        $this->assertContains('<h1>Zboží</h1>', $output);
        // filter
        $this->assertContains('<form method="post" class="frmFilter">', $output);
        $this->assertContains('<input type="text" name="f_name" id="fldFilterName" maxlength="20" />', $output);
        // pagination
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $this->assertContains('<select data-rows-page="" id="fldRows">'.Html::getOptions($options).'</select>', $output);
        // orders
        $this->assertContains('<span data-orderby="name"', $output);
        $this->assertContains('<span data-orderby="name" data-orderdesc="1"', $output);
        // positions
        $this->assertContains('<select data-pos=""></select>', $output);
        $this->assertContains('<span data-pos-start=""></span>', $output);
        $this->assertContains('<span data-pos-end=""></span>', $output);
        $this->assertContains('<span data-total=""></span>', $output);

        // edit form
        $this->assertContains('<input type="hidden" name="id" value="0" />', $output);
        $this->assertContains('<input type="text" name="name" id="fldName" maxlength="50" required />', $output);
        $this->assertContains('<input type="text" name="seo" id="fldSeo" maxlength="52" />', $output);
        $this->assertContains('<input type="checkbox" name="publish" id="fldPublish" />', $output);
        $this->assertContains('<input type="checkbox" name="bestseller" id="fldBestseller" />', $output);
        $this->assertContains('<input type="checkbox" name="featured" id="fldFeatured" />', $output);
        $this->assertContains('<input type="text" id="fldPrice" name="price" class="float" />', $output);
        $this->assertContains('<input type="text" name="vat" id="fldVat" maxlength="4" class="float" />', $output);
        $this->assertContains('<input type="text" id="fldPriceVat" name="price_vat" class="float" readonly="readonly" />', $output);
        $this->assertContains('<textarea name="text" cols="50" rows="5"></textarea>', $output);
        $this->assertContains('<input type="text" name="meta_desc" maxlength="255" />', $output);
        $this->assertContains('<input type="text" name="meta_keys" maxlength="255" />', $output);

        //icons
        $this->assertContains('<input type="checkbox" name="icons[]" value="1" id="fldIcon1" />', $output);
        $this->assertContains('<input type="checkbox" name="icons[]" value="2" id="fldIcon2" />', $output);
        // categories
        $this->assertContains('<select name="category_id" id="fldCategory"><option value="1">category1</option><option value="2">&nbsp;&nbsp;category2</option></select>', $output);

        $this->assertEquals('Zboží | [%LG_SITENAME%]', $this->_app->response()->pageTitle());
    }

    public function testHandleListAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-listproducts');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleList()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        for ($i = 1; $i < 22; $i++) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $items = array();
            $items['id'] = $i;
            $items['name'] = $this->_db->escape('name'.$tmp);
            $items['seo'] = $this->_db->escape('seo'.$i);
            $items['publish'] = '1';
            $items['text'] = $this->_db->escape('text'.$i);
            $items['category_id'] = 1;
            $items['popis_modulu'] = $this->_db->escape('modul'.$i);
            $items['zapojeni'] = $this->_db->escape('zapojeni'.$i);
            $items['dokumentace'] = $this->_db->escape('dokumentace'.$i);
            $items['firmware'] = $this->_db->escape('firmware'.$i);
            $items['programy'] = $this->_db->escape('programy'.$i);
            $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            $this->_db->query($sql);
        }

        $this->_object->outputAdmin('ajax-listproducts');
        $output = json_decode($this->_app->response()->pageContent());

        $expect = array();
        for ($i = 1; $i < 11; $i++) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $expect[] = (object) array(
                'id'=>$i,
                'name'=> 'name'.$tmp,
                'publish'=> 'Ano'
            );
        }
        $this->assertEquals($expect, $output->items);
        $this->assertEquals(array(1, 11, 21), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(10, $output->posEnd);
        $this->assertEquals(21, $output->total);

        // filtered list
        unset($_GET);
        $_GET['f_name'] = 'name21';
        $expect = array((object) array(
            'id'=>21,
            'name'=> 'name21',
            'publish'=> 'Ano'
        ));
        $this->_object->outputAdmin('ajax-listproducts');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($expect, $output->items);
        $this->assertEquals(array(1,), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(1, $output->posEnd);
        $this->assertEquals(1, $output->total);
    }

    public function testHandleEditAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-editproduct');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleEditWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Record not found';
        $this->_object->outputAdmin('ajax-editproduct');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleEdit()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name a');
        $items['seo'] = $this->_db->escape('seo-a');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $items['bestseller'] = '1';
        $items['featured'] = '1';
        $items['price'] = '10';
        $items['price_vat'] = '12';
        $items['vat'] = '20';
        $items['category_id'] = '1';
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name1');
        $items['seo'] = $this->_db->escape('seo1');
        $items['text'] = $this->_db->escape('text1');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['name'] = $this->_db->escape('name2');
        $items['seo'] = $this->_db->escape('seo2');
        $items['text'] = $this->_db->escape('text2');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        //icons
        $items = array();
        $items['id'] = 3;
        $items['name'] = $this->_db->escape('name1');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_products_icons ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['id'] = 4;
        $items['name'] = $this->_db->escape('name2');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_products_icons ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['product_id'] = 1;
        $items['icon_id'] = 3;
        $sql = 'INSERT INTO eshop_rel_product_icon ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        //cross-sell
        $items = array();
        $items['id'] = 10;
        $items['name'] = $this->_db->escape('name10');
        $items['seo'] = $this->_db->escape('seo10');
        $items['text'] = $this->_db->escape('');
        $items['category_id'] = '1';
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['product_id'] = 1;
        $items['cross_id'] = 10;
        $sql = 'INSERT INTO eshop_rel_product_cross ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        //variant
        $items = array();
        $items['id'] = 11;
        $items['name'] = $this->_db->escape('name11');
        $items['seo'] = $this->_db->escape('se11');
        $items['text'] = $this->_db->escape('');
        $items['category_id'] = '1';
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['product_id'] = 1;
        $items['variant_id'] = 11;
        $sql = 'INSERT INTO eshop_rel_product_variant ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        //gallery images
        $items = array();
        $items['id'] = 12;
        $items['name'] = $this->_db->escape('name12');
        $items['description'] = $this->_db->escape('desc12');
        $items['parent_id'] = 1;
        $sql = 'INSERT INTO eshop_products_images ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $dataDirOrig = ERGO_DATA_DIR.'eshop/gallery/orig';
        \GstLib\Filesystem::mkDir($dataDirOrig);
        copy (dirname(ERGO_ROOT_DIR).'/test/phpunit.jpg', $dataDirOrig.'/12_phpunit.jpg');
        $timestamp = filemtime($dataDirOrig.'/12_phpunit.jpg');

        $_GET['id'] = 1;
        $this->_object->outputAdmin('ajax-editproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);
        $expect = (object) array(
            'id' => 1,
            'name' => 'name a',
            'seo' => 'seo-a',
            'publish' => true,
            'meta_desc' => 'meta a',
            'meta_keys' => 'keys a',
            'text' => 'text a',
            'bestseller' => true,
            'featured' => true,
            'price' => 10,
            'price_vat' => 12,
            'vat' => 20,
            'category_id' => 1,
            'imageUrl' => '',
            'images' => array((object) array('id'=>12, 'description'=>'desc12', 'url'=>ERGO_ROOT_URL.'phpunitdata/eshop/gallery/thumb/12_'.$timestamp.'_phpunit.jpg')),
            'variants' => array((object) array('id'=>11, 'name'=>'name11')),
            'cross' => array((object) array('id'=>10, 'name'=>'name10')),
            'icons' => array(3),
            'popis_modulu' => 'modul',
            'zapojeni' => 'zapojeni',
            'dokumentace' => 'dokumentace',
            'firmware' => 'firmware',
            'programy' => 'programy'
        );
        $this->assertEquals($expect, $output->record);
    }

    public function testHandleSaveAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-saveproduct');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleSaveEmptyData()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Chybí údaje: Název';
        $expect->id = 0;
        unset($_POST);
        $this->_object->outputAdmin('ajax-saveproduct');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveInsert()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        unset($_GET);
        unset($_POST);
        $_POST['name'] = 'name';
        $_POST['text'] = ' <p>text</p>';
        $_POST['publish'] = 'on';
        $_POST['meta_desc'] = ' meta a';
        $_POST['meta_keys'] = 'keys a';
        $_POST['bestseller'] = 'on';
        $_POST['featured'] = 'on';
        $_POST['price'] = '10';
        $_POST['vat'] = '20';
        $_POST['category_id'] = '1';
        $this->_object->outputAdmin('ajax-saveproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('', $output->msg);
        $this->assertEquals('OK', $output->status);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $expect = array(
            'id'=>'1',
            'name'=>'name',
            'seo'=>'name',
            'publish'=>'1',
            'text'=>'<p>text</p>',
            'meta_desc'=>'meta a',
            'meta_keys'=>'keys a',
            'bestseller'=>'1',
            'featured'=>'1',
            'price'=>'10.00',
            'price_vat'=>'12.00',
            'vat'=>'20.00',
            'last_change'=>$date->format('Y-m-d H:i:'),
            'category_id'=>'1',
            'popis_modulu'=>'',
            'zapojeni'=>'',
            'dokumentace'=>'',
            'firmware'=>'',
            'programy'=>''
        );
        $row = $this->_db->fetchArray('SELECT * FROM eshop_products');
        $row['last_change'] = substr($row['last_change'], 0, -2);
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveUpdate()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name');
        $items['seo'] = $this->_db->escape('seo');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $items['bestseller'] = '1';
        $items['featured'] = '1';
        $items['price'] = '10';
        $items['price_vat'] = '12';
        $items['vat'] = '20';
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        unset($_POST);
        $_POST['id'] = '1';
        $_POST['name'] = 'name2';
        $_POST['seo'] = 'seo2';
        $_POST['text'] = ' <p>text2</p>';
        $_POST['parent'] = '3';
        $_POST['meta_desc'] = ' meta a2';
        $_POST['meta_keys'] = 'keys a2';
        $_POST['price'] = '100';
        $_POST['vat'] = '10';
        $_POST['price_vat'] = '110';
        $this->_object->outputAdmin('ajax-saveproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $expect = array(
            'id'=>'1',
            'name'=>'name2',
            'seo'=>'seo2',
            'publish'=>'0',
            'text'=>'<p>text2</p>',
            'meta_desc'=>'meta a2',
            'meta_keys'=>'keys a2',
            'bestseller'=>'0',
            'featured'=>'0',
            'price'=>'100.00',
            'price_vat'=>'110.00',
            'vat'=>'10.0',
            'last_change'=>$date->format('Y-m-d H:i:'),
            'category_id' => '1',
            'popis_modulu' => '',
            'zapojeni' => '',
            'dokumentace' => '',
            'firmware' => '',
            'programy' => ''
        );
        $row = $this->_db->fetchArray('SELECT * FROM eshop_products');
        $row['last_change'] = substr($row['last_change'], 0, -2);
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveNotUniqueSeo()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name');
        $items['seo'] = $this->_db->escape('seo');
        $items['text'] = $this->_db->escape('text');
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_GET);
        unset($_POST);
        $_POST['name'] = 'name2';
        $_POST['seo'] = 'seo';
        $this->_object->outputAdmin('ajax-saveproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);

        $expect = array(
            'id'=>'2',
            'seo'=>'seo-1'
        );
        $row = $this->_db->fetchArray('SELECT id,seo FROM eshop_products WHERE id=2');
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveInsertFail()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $this->_db->query('ALTER TABLE eshop_products CHANGE COLUMN `name` `name` VARCHAR(10) NOT NULL');

        unset($_GET);
        unset($_POST);
        $_POST['name'] = 'namenamenamename';

        $this->_object->outputAdmin('ajax-saveproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('ERR', $output->status);
        $this->assertEquals('Uložení se nezdařilo', $output->msg);

        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_products'));
    }

    public function testHandleSaveUpdateFail()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name');
        $items['seo'] = $this->_db->escape('seo');
        $items['text'] = $this->_db->escape('text');
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $this->_db->query('ALTER TABLE eshop_products CHANGE COLUMN `name` `name` VARCHAR(10) NOT NULL');

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = '[%LG_ERR_SAVE_FAILED%]';
        unset($_GET);
        unset($_POST);
        $_POST['id'] = '1';
        $_POST['name'] = 'namenamenamenamename';
        $this->_object->outputAdmin('ajax-saveproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('ERR', $output->status);
        $this->assertEquals('Uložení se nezdařilo', $output->msg);
    }

    public function testHandleDeleteAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-deleteproduct');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleDeletetWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;
        $this->_object->outputAdmin('ajax-deleteproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('ERR', $output->status);
        $this->assertEquals('Record not found', $output->msg);
    }

    public function testHandleDelete()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name');
        $items['seo'] = $this->_db->escape('seo');
        $items['text'] = $this->_db->escape('text');
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name1');
        $items['seo'] = $this->_db->escape('seo1');
        $items['text'] = $this->_db->escape('text1');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        //gallery images
        $items = array();
        $items['id'] = 12;
        $items['name'] = $this->_db->escape('name12');
        $items['description'] = $this->_db->escape('desc12');
        $items['parent_id'] = 1;
        $sql = 'INSERT INTO eshop_products_images ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $dataDirOrig = ERGO_DATA_DIR.'eshop/gallery/orig';
        \GstLib\Filesystem::mkDir($dataDirOrig);
        \GstLib\Filesystem::mkDir(ERGO_DATA_DIR.'eshop/gallery/thumb');
        copy (dirname(ERGO_ROOT_DIR).'/test/phpunit.jpg', $dataDirOrig.'/12_phpunit.jpg');
        $timestamp = filemtime($dataDirOrig.'/12_phpunit.jpg');
        copy (dirname(ERGO_ROOT_DIR).'/test/phpunit.jpg', ERGO_DATA_DIR.'eshop/gallery/thumb/12_'.$timestamp.'_phpunit.jpg');

        //icons
        $items = array();
        $items['product_id'] = 1;
        $items['icon_id'] = 3;
        $sql = 'INSERT INTO eshop_rel_product_icon ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        // cross-sell
        $items = array();
        $items['id'] = 3;
        $items['name'] = $this->_db->escape('name3');
        $items['seo'] = $this->_db->escape('seo3');
        $items['text'] = $this->_db->escape('');
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['product_id'] = 1;
        $items['cross_id'] = 3;
        $sql = 'INSERT INTO eshop_rel_product_cross ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        //variant
        $items = array();
        $items['product_id'] = 1;
        $items['variant_id'] = 3;
        $sql = 'INSERT INTO eshop_rel_product_variant ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        // cart
        $items = array();
        $items['id'] = $this->_db->escape('someid');
        $items['date'] = $this->_db->escape(date('Y-m-d H:i:s'));
        $items['product_id'] = 1;
        $items['quantity'] = 3;
        $sql = 'INSERT INTO eshop_carts ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['id'] = $this->_db->escape('someid');
        $items['date'] = $this->_db->escape(date('Y-m-d H:i:s'));
        $items['product_id'] = 3;
        $items['quantity'] = 2;
        $sql = 'INSERT INTO eshop_carts ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $_GET['id'] = 1;
        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-deleteproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);

        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_products WHERE id=1'));
        $this->assertEquals(1, $this->_db->count('SELECT COUNT(*) FROM eshop_products WHERE id=3'));

        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_products_images'));
        $this->assertFileNotExists($dataDirOrig.'/12_phpunit.jpg');
        $this->assertFileNotExists(ERGO_DATA_DIR.'eshop/gallery/thumb/12_'.$timestamp.'_phpunit.jpg');

        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_rel_product_icon'));
        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_rel_product_cross'));
        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_rel_product_variant'));
        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_carts WHERE product_id=1'));
        $this->assertEquals(1, $this->_db->count('SELECT COUNT(*) FROM eshop_carts WHERE product_id=3'));
    }

//    public function testHandleDeleteFail()
//    {
//    // TODO pujde nejak zpusobit vyhozeni vyjimky pomoci manipulace s db?
//        $this->_app->logTestUser();
//        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);
//
//        $items = array();
//        $items['id'] = 1;
//        $items['parent_id'] = 5;
//        $items['ordering'] = 1;
//        $items['name'] = $this->_db->escape('A');
//        $items['seo'] = $this->_db->escape('seo');
//        $items['publish'] = '1';
//        $items['meta_desc'] = $this->_db->escape('meta a');
//        $items['meta_keys'] = $this->_db->escape('keys a');
//        $items['text'] = $this->_db->escape('text a');
//        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
//        $this->_db->query($sql);
//
//        //$this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_category');
//        $this->_db->query('DROP TABLE eshop_rel_product_category');
//        //$this->_db->query('DROP TABLE eshop_categories');
//         //$this->_db->query('ALTER TABLE eshop_categories CHANGE COLUMN `parent_id` `dummy` VARCHAR(10) NOT NULL');
//
//        $_GET['id'] = 1;
//        $expect = new \stdClass();
//        $expect->status = 'ERR';
//        $expect->msg = '[%LG_ERR_DELETE_FAILED%]';
//        $this->_object->outputAdmin('ajax-deletecategory');
//        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
//
//        $this->assertEquals(1, $this->_db->count('SELECT COUNT(*) FROM eshop_categories'));
//    }

    public function testHandlePublishAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-publishproduct');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandlePublish()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name');
        $items['seo'] = $this->_db->escape('seo');
        $items['text'] = $this->_db->escape('text');
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        unset($_POST);
        $_GET['id'] = '1';
        $this->_object->outputAdmin('ajax-publishproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('1', $output->id);
        $this->assertEquals('Ano', $output->publish);

        $row = $this->_db->fetchArray('SELECT publish FROM eshop_products WHERE id=1');
        $this->assertEquals('1', $row['publish']);

        unset($_GET);
        $_GET['id'] = '1';
        $this->_object->outputAdmin('ajax-publishproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('1', $output->id);
        $this->assertEquals('Ne', $output->publish);

        $row = $this->_db->fetchArray('SELECT publish FROM eshop_products WHERE id=1');
        $this->assertEquals('0', $row['publish']);
    }

    public function testHandleCopy()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['name'] = $this->_db->escape('name');
        $items['seo'] = $this->_db->escape('seo');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $items['bestseller'] = '1';
        $items['featured'] = '1';
        $items['price'] = '10';
        $items['price_vat'] = '12';
        $items['vat'] = '20';
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul');
        $items['zapojeni'] = $this->_db->escape('zapojeni');
        $items['dokumentace'] = $this->_db->escape('dokumentace');
        $items['firmware'] = $this->_db->escape('firmware');
        $items['programy'] = $this->_db->escape('programy');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['name'] = $this->_db->escape('name2');
        $items['seo'] = $this->_db->escape('seo2');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta2');
        $items['meta_keys'] = $this->_db->escape('keys2');
        $items['text'] = $this->_db->escape('text2');
        $items['bestseller'] = '1';
        $items['featured'] = '1';
        $items['price'] = '10';
        $items['price_vat'] = '12';
        $items['vat'] = '20';
        $items['category_id'] = 1;
        $items['popis_modulu'] = $this->_db->escape('modul2');
        $items['zapojeni'] = $this->_db->escape('zapojeni2');
        $items['dokumentace'] = $this->_db->escape('dokumentace2');
        $items['firmware'] = $this->_db->escape('firmware2');
        $items['programy'] = $this->_db->escape('programy2');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 3;
        $items['name'] = $this->_db->escape('name3');
        $items['seo'] = $this->_db->escape('seo3');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta2');
        $items['meta_keys'] = $this->_db->escape('keys2');
        $items['text'] = $this->_db->escape('text2');
        $items['bestseller'] = '1';
        $items['featured'] = '1';
        $items['price'] = '10';
        $items['price_vat'] = '12';
        $items['vat'] = '20';
        $items['category_id'] = 2;
        $items['popis_modulu'] = $this->_db->escape('modul2');
        $items['zapojeni'] = $this->_db->escape('zapojeni2');
        $items['dokumentace'] = $this->_db->escape('dokumentace2');
        $items['firmware'] = $this->_db->escape('firmware2');
        $items['programy'] = $this->_db->escape('programy2');
        $sql = 'INSERT INTO eshop_products ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['product_id'] = 1;
        $items['cross_id'] = 2;
        $sql = 'INSERT INTO eshop_rel_product_cross ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['product_id'] = 1;
        $items['cross_id'] = 3;
        $sql = 'INSERT INTO eshop_rel_product_cross ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['product_id'] = 1;
        $items['variant_id'] = 3;
        $sql = 'INSERT INTO eshop_rel_product_variant ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['product_id'] = 1;
        $items['icon_id'] = 1;
        $sql = 'INSERT INTO eshop_rel_product_icon ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $items = array();
        $items['product_id'] = 1;
        $items['icon_id'] = 2;
        $sql = 'INSERT INTO eshop_rel_product_icon ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $dataDirOrig = ERGO_DATA_DIR.'eshop/orig';
        \GstLib\Filesystem::mkDir($dataDirOrig);
        copy (dirname(ERGO_ROOT_DIR).'/test/phpunit.jpg', $dataDirOrig.'/1_phpunit.jpg');

        //gallery images
        $items = array();
        $items['id'] = 12;
        $items['name'] = $this->_db->escape('name12');
        $items['description'] = $this->_db->escape('desc12');
        $items['parent_id'] = 1;
        $items['publish'] = 1;
        $sql = 'INSERT INTO eshop_products_images ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);
        $dataDirGalleryOrig = ERGO_DATA_DIR.'eshop/gallery/orig';
        \GstLib\Filesystem::mkDir($dataDirGalleryOrig);
        copy (dirname(ERGO_ROOT_DIR).'/test/phpunit.jpg', $dataDirGalleryOrig.'/12_phpunit.jpg');


        $_POST['id'] = '1';
        $this->_object->outputAdmin('ajax-copyproduct');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);
        $this->assertEquals('4', $output->id);

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $expect = array(
            'id'=>'4',
            'name'=>'name KOPIE',
            'seo'=>'seo-1',
            'publish'=>'0',
            'text'=>'text a',
            'meta_desc'=>'meta a',
            'meta_keys'=>'keys a',
            'bestseller'=>'1',
            'featured'=>'1',
            'price'=>'10.00',
            'price_vat'=>'12.00',
            'vat'=>'20.0',
            'last_change'=>$date->format('Y-m-d H:i:'),
            'category_id' => '1',
            'popis_modulu' => 'modul',
            'zapojeni' => 'zapojeni',
            'dokumentace' => 'dokumentace',
            'firmware' => 'firmware',
            'programy' => 'programy'
        );
        $row = $this->_db->fetchArray('SELECT * FROM eshop_products WHERE id=4');
        $row['last_change'] = substr($row['last_change'], 0, -2);
        $this->assertEquals($expect, $row);

        $expect = array(
            array('product_id'=>'1', 'cross_id'=>'2'),
            array('product_id'=>'1', 'cross_id'=>'3'),
            array('product_id'=>'4', 'cross_id'=>'2'),
            array('product_id'=>'4', 'cross_id'=>'3')
        );
        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_rel_product_cross ORDER BY product_id,cross_id');
        $this->assertEquals($expect, $rows);

        $expect = array(
            array('product_id'=>'1', 'variant_id'=>'3'),
            array('product_id'=>'4', 'variant_id'=>'3')
        );
        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_rel_product_variant ORDER BY product_id,variant_id');
        $this->assertEquals($expect, $rows);

        $expect = array(
            array('product_id'=>'1', 'icon_id'=>'1'),
            array('product_id'=>'1', 'icon_id'=>'2'),
            array('product_id'=>'4', 'icon_id'=>'1'),
            array('product_id'=>'4', 'icon_id'=>'2')
        );
        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_rel_product_icon ORDER BY product_id,icon_id');
        $this->assertEquals($expect, $rows);
        // main image
        $this->assertFileExists($dataDirOrig.'/1_phpunit.jpg');
        $this->assertFileExists($dataDirOrig.'/4_phpunit.jpg');

        // gallery
        $expect = array(
            array(
                'id'=>'12',
                'name'=>'name12',
                'description'=>'desc12',
                'parent_id'=>'1',
                'publish'=>'1',
                'ordering'=>'0'
            ),
            array(
                'id'=>'13',
                'name'=>'name12',
                'description'=>'desc12',
                'parent_id'=>'4',
                'publish'=>'1',
                'ordering'=>'1'
            )
        );
        $rows = $this->_db->fetchArrayAll('SELECT id,name,description,parent_id,publish,ordering FROM eshop_products_images ORDER BY parent_id,ordering');
        $this->assertEquals($expect, $rows);

        $this->assertFileExists($dataDirGalleryOrig.'/12_phpunit.jpg');
        $this->assertFileExists($dataDirGalleryOrig.'/13_phpunit.jpg');
    }

}