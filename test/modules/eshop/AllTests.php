<?php
/**
 * @package    Ergo
 * @subpackage EshopTest
 */

namespace Eshop;

require_once 'ModuleTest.php';
require_once 'AdminConfigTest.php';
require_once 'AdminCategoriesTest.php';
require_once 'AdminProductsTest.php';
require_once 'AdminOrdersTest.php';

/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage IncludesTest
 */
class Module_AllTests
{
    /**
     * run all tests of module
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('ModuleEshop');
        $suite->addTestSuite('\Eshop\ModuleTest');
        $suite->addTestSuite('\Eshop\AdminConfigTest');
        $suite->addTestSuite('\Eshop\AdminCategoriesTest');
        $suite->addTestSuite('\Eshop\AdminProductsTest');
        $suite->addTestSuite('\Eshop\AdminOrdersTest');
        return $suite;
    }
}