<?php
/**
 * @package    Ergo
 * @subpackage EshopTest
 */

namespace Eshop;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/eshop/module.php';
require_once ERGO_ROOT_DIR.'modules/eshop/config.php';
require_once ERGO_ROOT_DIR.'modules/eshop/dataconfig.php';
require_once ERGO_ROOT_DIR.'modules/eshop/admin/controllerconfig.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/delivererslist.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/paymentslist.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/datadeliverer.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/datapayment.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

use Ergo\Response;

/**
 * test class for module Eshop
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage EshopTest
 */
class AdminConfigTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Eshop\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $config->defaultLanguage('cs');
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Eshop\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
        $this->_db->query('DROP TABLE IF EXISTS eshop_config');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_deliverers');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_payments');

        $this->_db->query('CREATE TABLE eshop_config LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_config');
        $this->_db->query('CREATE TABLE eshop_orders_deliverers LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_orders_deliverers');
        $this->_db->query('CREATE TABLE eshop_orders_payments LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_orders_payments');
    }

    protected function tearDown()
    {
        $this->_db->query('DROP TABLE IF EXISTS eshop_config');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_deliverers');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_payments');
        $this->_db->close();
        $this->_app->tearDown();
    }

    public function testRenderFormAsNotLogged()
    {
        $this->_object->outputAdmin('config');
        $this->assertEquals(302, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
    }

    public function testRenderForm()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['param'] = $this->_db->escape('email_order');
        $items['value'] = $this->_db->escape('test@example.org');
        $sql = 'INSERT INTO eshop_config ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 1;
        $items['price'] = 10;
        $items['free_from'] = 11;
        $sql = 'INSERT INTO eshop_orders_deliverers ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['price'] = 20;
        $items['free_from'] = 21;
        $sql = 'INSERT INTO eshop_orders_deliverers ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);


        $this->_object->outputAdmin('config');
        $output = $this->_app->response()->pageContent();
        $this->assertContains('<h1>[%LG_SETTINGS%]</h1>', $output);
        $this->assertContains('<input type="text" name="email" id="fldEmail" value="test@example.org" maxlength="255" />', $output);

        $this->assertContains('<input type="text" name="price_delivery[1]" value="10" />', $output);
        $this->assertContains('<input type="text" name="free_delivery[1]" value="11" />', $output);
        $this->assertContains('<input type="text" name="price_delivery[2]" value="20" />', $output);
        $this->assertContains('<input type="text" name="free_delivery[2]" value="21" />', $output);
        $this->assertContains('<input type="text" name="price_delivery[3]" value="0" />', $output);
        $this->assertContains('<input type="text" name="free_delivery[3]" value="0" />', $output);
        $this->assertContains('<input type="text" name="price_delivery[5]" value="0" />', $output);
        $this->assertContains('<input type="text" name="free_delivery[5]" value="0" />', $output);

        $this->assertEquals('Nastavení e-shopu | [%LG_SITENAME%]', $this->_app->response()->pageTitle());
    }

    public function testHandleSaveAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-saveconfig');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleSaveInsert()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        unset($_GET);
        unset($_POST);
        $_POST['email'] = 'test@example.org';
        $_POST['price_delivery'] = array(1=>10, 2 => 20);
        $_POST['free_delivery'] = array(1=>11, 2 => 21);
        $_POST['price_payment'] = array(1=>100, 2 => 200);
        $this->_object->outputAdmin('ajax-saveconfig');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('Nastavení bylo uloženo', $output->msg);

        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_config');
        $expect = array('param'=>'email_order', 'value'=>'test@example.org');
        $this->assertContains($expect, $rows);

        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_orders_deliverers');
        $expect = array(
            array('id'=>'1', 'price'=>'10', 'free_from'=>'11'),
            array('id'=>'2', 'price'=>'20', 'free_from'=>'21'),
            array('id'=>'3', 'price'=>'0', 'free_from'=>'0'),
            array('id'=>'4', 'price'=>'0', 'free_from'=>'0'),
            array('id'=>'5', 'price'=>'0', 'free_from'=>'0'),
        );
        $this->assertEquals($expect, $rows);

        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_orders_payments');
        $expect = array(
            array('id'=>'1', 'price'=>'100'),
            array('id'=>'2', 'price'=>'200'),
            array('id'=>'3', 'price'=>'0'),
            array('id'=>'4', 'price'=>'0'),
        );
        $this->assertEquals($expect, $rows);
    }

    public function testHandleSaveUpdate()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

         $items = array();
        $items['param'] = $this->_db->escape('email_order');
        $items['value'] = $this->_db->escape('test@example.org');
        $sql = 'INSERT INTO eshop_config ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 1;
        $items['price'] = 1000;
        $items['free_from'] = 5000;
        $sql = 'INSERT INTO eshop_orders_deliverers ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['price'] = 2000;
        $sql = 'INSERT INTO eshop_orders_payments ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        unset($_POST);
        $_POST['email'] = 'test2@example.org';
        $_POST['price_delivery'] = array(1=>10, 2 => 20);
        $_POST['free_delivery'] = array(1=>11, 2 => 21);
        $_POST['price_payment'] = array(1=>100, 2 => 200);
        $this->_object->outputAdmin('ajax-saveconfig');

        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('Nastavení bylo uloženo', $output->msg);

        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_config');
        $expect = array('param'=>'email_order', 'value'=>'test2@example.org');
        $this->assertContains($expect, $rows);

        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_orders_deliverers');
        $expect = array(
            array('id'=>'1', 'price'=>'10', 'free_from'=>'11'),
            array('id'=>'2', 'price'=>'20', 'free_from'=>'21'),
            array('id'=>'3', 'price'=>'0', 'free_from'=>'0'),
            array('id'=>'4', 'price'=>'0', 'free_from'=>'0'),
            array('id'=>'5', 'price'=>'0', 'free_from'=>'0'),
        );
        $this->assertEquals($expect, $rows);

        $rows = $this->_db->fetchArrayAll('SELECT * FROM eshop_orders_payments');
        $expect = array(
            array('id'=>'1', 'price'=>'100'),
            array('id'=>'2', 'price'=>'200'),
            array('id'=>'3', 'price'=>'0'),
            array('id'=>'4', 'price'=>'0'),
        );
        $this->assertEquals($expect, $rows);
    }

    public function testHandleSaveInsertFail()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $this->_db->query('ALTER TABLE eshop_config CHANGE COLUMN `value` `value` CHAR(1) NOT NULL');

        unset($_GET);
        unset($_POST);
        $_POST['email'] = 'test2@example.org';
        $_POST['price_delivery'] = '99.5';
        $_POST['free_delivery'] = '1000.5';
        $_POST['price_delivery_express'] = '129.5';
        $_POST['free_delivery_express'] = '1200.5';
        $this->_object->outputAdmin('ajax-saveconfig');

        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('ERR', $output->status);
        $this->assertEquals('Uložení se nezdařilo', $output->msg);
    }

}