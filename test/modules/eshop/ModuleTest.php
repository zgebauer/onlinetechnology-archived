<?php
/**
 * @package    Ergo
 * @subpackage EshopTest
 */

namespace Eshop;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/eshop/module.php';
require_once ERGO_ROOT_DIR.'modules/eshop/datasearch.php';

require_once dirname(dirname(__DIR__)).'/test_app.php';

/**
 * test class for module Eshop
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage IncludesTest
 */
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Eshop\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Eshop\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
        $this->_db->query('DROP TABLE IF EXISTS eshop_products');
        $this->_db->query('CREATE TABLE eshop_products LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products');
    }

    protected function tearDown()
    {
        $this->_db->query('DROP TABLE IF EXISTS eshop_products');
        $this->_db->close();
        $this->_app->tearDown();
    }

    public function testDefaultMethods()
    {
        $this->assertEquals('eshop', Module::NAME);
        $this->assertNull($this->_object->menuItems());
        $this->assertEquals(array(), $this->_object->permissions());
        $this->assertEquals('E-shop', $this->_object->name());
        $this->assertNull($this->_object->cron());
    }

    public function testMenuItemsAsLogged()
    {
        $this->_app->logTestUser();
        // without permissions
        $this->assertNull($this->_object->menuItems());
    }

    public function testMenuItemsAsLoggedWithPermissions()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $output = $this->_object->menuItems();
        $expect = array(
            'name' => 'E-shop: Zboží',
            'url' => $this->_app->getUrl(Module::NAME, 'products'),
            'title' => 'Seznam zboží',
            'highlight' => false
        );
        $this->assertContains($expect, $output);

        $expect = array(
            'name' => 'E-shop: Kategorie zboží',
            'url' => $this->_app->getUrl(Module::NAME, 'categories'),
            'title' => 'Seznam kategorií zboží',
            'highlight' => false
        );
        $this->assertContains($expect, $output);
    }

    public function testUndefinedParameter()
    {
        $this->_object->output('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());

        $this->_object->outputAdmin('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());
    }

}