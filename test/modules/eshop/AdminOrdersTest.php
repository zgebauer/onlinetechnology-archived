<?php
/**
 * @package    Ergo
 * @subpackage EshopTest
 */

namespace Eshop;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
//require_once ERGO_ROOT_DIR.'core/GstLib/Tree.php';
//require_once ERGO_ROOT_DIR.'core/GstLib/UserFile.php';
//require_once ERGO_ROOT_DIR.'core/GstLib/Resizer.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/eshop/module.php';
require_once ERGO_ROOT_DIR.'modules/eshop/config.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/order.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/dataorder.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/categories.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/product.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/dataproduct.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/datacategory.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/dataicon.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/dataimage.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/datagalleryimage.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/galleryimage.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/image.php';
//require_once ERGO_ROOT_DIR.'modules/eshop/products/icon.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/delivererslist.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/datadeliverer.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/paymentslist.php';
require_once ERGO_ROOT_DIR.'modules/eshop/orders/datapayment.php';
require_once ERGO_ROOT_DIR.'modules/eshop/admin/controllerorders.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

use GstLib\Html;
use Ergo\Response;


/**
 * test class for module Eshop
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage EshopTest
 */
class AdminOrdersTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Eshop\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $config->defaultLanguage('cs');
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Eshop\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_items');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_payments');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_deliverers');

        $this->_db->query('CREATE TABLE eshop_orders LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_orders');
        $this->_db->query('CREATE TABLE eshop_orders_items LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_orders_items');
        $this->_db->query('CREATE TABLE eshop_orders_deliverers LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_orders_deliverers');
        $this->_db->query('CREATE TABLE eshop_orders_payments LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_orders_payments');
//        $this->_db->query('CREATE TABLE eshop_config LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_config');
//        $this->_db->query('CREATE TABLE eshop_products LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products');
//        $this->_db->query('CREATE TABLE eshop_categories LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_categories');
//        $this->_db->query('CREATE TABLE eshop_rel_product_category LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_category');
//        $this->_db->query('ALTER TABLE `eshop_rel_product_category` ADD CONSTRAINT `FK_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('ALTER TABLE `eshop_rel_product_category` ADD CONSTRAINT `FK_categories` FOREIGN KEY (`category_id`) REFERENCES `eshop_categories` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('CREATE TABLE eshop_rel_product_variant LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_variant');
//        $this->_db->query('ALTER TABLE eshop_rel_product_variant ADD CONSTRAINT `FK_eshop_rel_product_variant_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('ALTER TABLE eshop_rel_product_variant ADD CONSTRAINT `FK_eshop_rel_product_variant_eshop_products_2` FOREIGN KEY (`variant_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('CREATE TABLE eshop_products_images LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products_images');
//        $this->_db->query('CREATE TABLE eshop_products_icons LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_products_icons');
//        $this->_db->query('CREATE TABLE eshop_rel_product_icon LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_icon');
//        $this->_db->query('ALTER TABLE `eshop_rel_product_icon` ADD CONSTRAINT `FK_eshop_rel_product_icon_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('CREATE TABLE eshop_rel_product_cross LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_rel_product_cross');
//        $this->_db->query('ALTER TABLE eshop_rel_product_cross ADD CONSTRAINT `FK_eshop_rel_product_cross_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('ALTER TABLE eshop_rel_product_cross ADD CONSTRAINT `FK_eshop_rel_product_cross_eshop_products_2` FOREIGN KEY (`cross_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
//        $this->_db->query('CREATE TABLE eshop_carts LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_carts');
//        $this->_db->query('ALTER TABLE eshop_carts ADD CONSTRAINT `FK_eshop_carts_eshop_products` FOREIGN KEY (`product_id`) REFERENCES `eshop_products` (`id`) ON UPDATE CASCADE ON DELETE CASCADE');
    }

    protected function tearDown()
    {
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_items');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_deliverers');
        $this->_db->query('DROP TABLE IF EXISTS eshop_orders_payments');
        $this->_db->close();
        $this->_app->tearDown();
    }

    public function testListAsNotLogged()
    {
        $this->_object->outputAdmin('orders');
        $this->assertEquals(302, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
    }

    public function testListAsLogged()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $this->_object->outputAdmin('orders');
        $output = $this->_app->response()->pageContent();
        $this->assertContains('<h1>Objednávky</h1>', $output);
        // filter
        $this->assertContains('<form method="post" class="frmFilter">', $output);
        $this->assertContains('<input type="text" name="f_id" id="fldNumber" class="integer" />', $output);
        $this->assertContains('<input type="text" name="f_start" id="fldStart" maxlength="13" class="datePick" />', $output);
        $this->assertContains('<input type="text" name="f_end" id="fldEnd" maxlength="13" class="datePick" />', $output);
        // pagination
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $this->assertContains('<select data-rows-page="" id="fldRows">'.Html::getOptions($options).'</select>', $output);
        // orders
        $this->assertContains('<span data-orderby="id"', $output);
        $this->assertContains('<span data-orderby="id" data-orderdesc="1"', $output);
        // positions
        $this->assertContains('<select data-pos=""></select>', $output);
        $this->assertContains('<span data-pos-start=""></span>', $output);
        $this->assertContains('<span data-pos-end=""></span>', $output);
        $this->assertContains('<span data-total=""></span>', $output);

        // edit form
        $this->assertContains('<input type="hidden" name="id" />', $output);
        $this->assertContains('<input type="text" name="parcel" id="fldParcel" maxlength="20" />', $output);
        $this->assertContains('<select name="status" id="fldStatus">', $output);

        $this->assertEquals('Objednávky | [%LG_SITENAME%]', $this->_app->response()->pageTitle());
    }

    public function testHandleListAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-listorders');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleList()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        for ($i = 1; $i < 22; $i++) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $items = array();
            $items['id'] = $i;
            $items['date'] = $this->_db->escape('2013-08-'.$tmp.' 10:20:30');
            $items['first_name'] = $this->_db->escape('first'.$tmp);
            $items['last_name'] = $this->_db->escape('last'.$tmp);
            $items['email'] = $this->_db->escape('');
            $items['phone'] = $this->_db->escape('');
            $items['address'] = $this->_db->escape('');
            $items['city'] = $this->_db->escape('');
            $items['post_code'] = $this->_db->escape('');
            $items['subtotal'] = 0;
            $items['subtotal_vat'] = 0;
            $items['price_delivery'] = 0;
            $items['price_payment'] = 0;
            $items['grandtotal'] = 0;
            $items['grandtotal_vat'] = 0;
            $sql = 'INSERT INTO eshop_orders ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
            $this->_db->query($sql);
        }

        $_GET['desc'] = '1';
        $this->_object->outputAdmin('ajax-listorders');
        $output = json_decode($this->_app->response()->pageContent());

        $expect = array();
        for ($i = 21; $i > 11; $i--) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $expect[] = (object) array(
                'id'=>$i,
                'date'=> $i.'.8.2013',
                'name'=> 'last'.$tmp.' first'.$tmp,
                'status'=> 'Objednávka přijata',
                'status_id'=> '0'
            );
        }
        $this->assertEquals($expect, $output->items);
        $this->assertEquals(array(1, 11, 21), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(10, $output->posEnd);
        $this->assertEquals(21, $output->total);

        // filtered list
        unset($_GET);
        $_GET['f_id'] = '21';
        $expect = array((object) array(
            'id'=>21,
            'date'=> '21.8.2013',
            'name'=> 'last21 first21',
            'status'=> 'Objednávka přijata',
            'status_id'=> '0'
        ));
        $this->_object->outputAdmin('ajax-listorders');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($expect, $output->items);
        $this->assertEquals(array(1), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(1, $output->posEnd);
        $this->assertEquals(1, $output->total);

        unset($_GET);
        $_GET['desc'] = '1';
        $_GET['f_start'] = '10.8.2013';
        $_GET['f_end'] = '12.8.2013';
        $expect = array();
        $expect[] = (object) array(
            'id'=>12,
            'date'=> '12.8.2013',
            'name'=> 'last12 first12',
            'status'=> 'Objednávka přijata',
            'status_id'=> '0'
        );
        $expect[] = (object) array(
            'id'=>11,
            'date'=> '11.8.2013',
            'name'=> 'last11 first11',
            'status'=> 'Objednávka přijata',
            'status_id'=> '0'
        );
        $expect[] = (object) array(
            'id'=>10,
            'date'=> '10.8.2013',
            'name'=> 'last10 first10',
            'status'=> 'Objednávka přijata',
            'status_id'=> '0'
        );
        $this->_object->outputAdmin('ajax-listorders');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($expect, $output->items);
        $this->assertEquals(array(1), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(3, $output->posEnd);
        $this->assertEquals(3, $output->total);
    }

    public function testHandleEditAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-editorder');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleEditWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Record not found';
        $this->_object->outputAdmin('ajax-editorder');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleEdit()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['date'] = $this->_db->escape('2013-08-07 11:22:33');
        $items['customer_id'] = 0;
        $items['first_name'] = $this->_db->escape('first');
        $items['last_name'] = $this->_db->escape('last');
        $items['company'] = $this->_db->escape('company');
        $items['address'] = $this->_db->escape('address');
        $items['city'] = $this->_db->escape('city');
        $items['post_code'] = $this->_db->escape('123');
        //$items['country'] = $this->_db->escape('CS');
        $items['del_address'] = $this->_db->escape('deliveryAddress');
        $items['del_city'] = $this->_db->escape('deliveryCity');
        $items['del_post_code'] = $this->_db->escape('456');
        //$items['del_country'] = $this->_db->escape('EN');
        $items['ic'] = $this->_db->escape('ic');
        $items['dic'] = $this->_db->escape('dic');
        $items['phone'] = $this->_db->escape('phone');
        $items['email'] = $this->_db->escape('test@example.org');
        $items['note'] = $this->_db->escape("note\nsecond line");
        $items['deliverer_id'] = 1;
        $items['payment_id'] = 1;
        $items['subtotal'] = 10;
        $items['subtotal_vat'] = 12;
        $items['price_delivery'] = 100;
        $items['price_payment'] = 20;
        $items['grandtotal'] = 110;
        $items['grandtotal_vat'] = 120;
        $items['parcel_number'] = $this->_db->escape('parcel');
        $sql = 'INSERT INTO eshop_orders ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 1;
        $items['order_id'] = 1;
        $items['product_id'] = 1;
        $items['product'] = $this->_db->escape('product1');
        $items['quantity'] = 3;
        $items['price_unit'] = 10;
        $items['price_unit_vat'] = 12;
        $items['vat'] = 20;
        $items['price_row'] = 30;
        $items['price_row_vat'] = 36;
        $sql = 'INSERT INTO eshop_orders_items ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['order_id'] = 1;
        $items['product_id'] = 2;
        $items['product'] = $this->_db->escape('product2');
        $items['quantity'] = 1;
        $items['price_unit'] = 100;
        $items['price_unit_vat'] = 110;
        $items['vat'] = 10;
        $items['price_row'] = 100;
        $items['price_row_vat'] = 110;
        $sql = 'INSERT INTO eshop_orders_items ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $_GET['id'] = 1;
        $this->_object->outputAdmin('ajax-editorder');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);

        $this->assertEquals(1, $output->record->id);
        $this->assertEquals('2013-08-07T11:22:33Z', $output->record->date);
        $this->assertEquals('first', $output->record->first_name);
        $this->assertEquals('last', $output->record->last_name);
        $this->assertEquals('address', $output->record->address);
        $this->assertEquals('city', $output->record->city);
        $this->assertEquals('123', $output->record->post_code);
        $this->assertEquals('deliveryAddress', $output->record->delivery_address);
        $this->assertEquals('deliveryCity', $output->record->delivery_city);
        $this->assertEquals('456', $output->record->delivery_post_code);
        $this->assertEquals('test@example.org', $output->record->email);
        $this->assertEquals('phone', $output->record->phone);
        $this->assertEquals("note\nsecond line", $output->record->note);
        $this->assertEquals('company', $output->record->company);
        $this->assertEquals('ic', $output->record->ic);
        $this->assertEquals('dic', $output->record->dic);
        $this->assertEquals('Česká pošta - Balík do ruky', $output->record->deliverer);
        $this->assertEquals('parcel', $output->record->parcel);
        $this->assertEquals('10.00 Kč', $output->record->subtotal);
        $this->assertEquals('12.00 Kč', $output->record->subtotal_vat);

        $expect = (object) array(
            'name' => 'product1',
            'quantity' => '3',
            'price_unit' => '10.00 Kč',
            'price_unit_vat' => '12.00 Kč',
            'vat' => '20',
            'price_row' => '30.00 Kč',
            'price_row_vat' => '36.00 Kč'
        );
        $this->assertEquals($expect, $output->record->items[0]);
        $expect = (object) array(
            'name' => 'product2',
            'quantity' => '1',
            'price_unit' => '100.00 Kč',
            'price_unit_vat' => '110.00 Kč',
            'vat' => '10',
            'price_row' => '100.00 Kč',
            'price_row_vat' => '110.00 Kč'
        );
        $this->assertEquals($expect, $output->record->items[1]);

    }

    public function testHandleSaveAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-saveorder');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleSaveWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_POST['id'] = 10;

        $this->_object->outputAdmin('ajax-saveorder');
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->_app->response()->httpCode());
    }

    public function testHandleSaveUpdate()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['date'] = $this->_db->escape('2013-08-07 11:22:33');
        $items['customer_id'] = 0;
        $items['first_name'] = $this->_db->escape('first');
        $items['last_name'] = $this->_db->escape('last');
        $items['company'] = $this->_db->escape('company');
        $items['address'] = $this->_db->escape('address');
        $items['city'] = $this->_db->escape('city');
        $items['post_code'] = $this->_db->escape('123');
        $items['del_address'] = $this->_db->escape('deliveryAddress');
        $items['del_city'] = $this->_db->escape('deliveryCity');
        $items['del_post_code'] = $this->_db->escape('456');
        $items['ic'] = $this->_db->escape('ic');
        $items['dic'] = $this->_db->escape('dic');
        $items['phone'] = $this->_db->escape('phone');
        $items['email'] = $this->_db->escape('test@example.org');
        $items['note'] = $this->_db->escape("note\nsecond line");
        $items['deliverer_id'] = 1;
        $items['subtotal'] = 10;
        $items['subtotal_vat'] = 12;
        $items['price_delivery'] = 100;
        $items['price_payment'] = 20;
        $items['grandtotal'] = 110;
        $items['grandtotal_vat'] = 120;
        $items['parcel_number'] = $this->_db->escape('parcel');
        $sql = 'INSERT INTO eshop_orders ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        unset($_POST);
        $_POST['id'] = '1';
        $_POST['parcel'] = 'newparcel';
        $_POST['status'] = '2';
        $this->_object->outputAdmin('ajax-saveorder');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('OK', $output->status);
        $this->assertEquals('', $output->msg);

         $row = $this->_db->fetchArray('SELECT * FROM eshop_orders');

        $this->assertEquals(1, $row['id']);
        $this->assertEquals('newparcel', $row['parcel_number']);
        $this->assertEquals('2', $row['status']);
    }

    public function testHandleSaveUpdateFail()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['date'] = $this->_db->escape('2013-08-07 11:22:33');
        $items['customer_id'] = 0;
        $items['first_name'] = $this->_db->escape('first');
        $items['last_name'] = $this->_db->escape('last');
        $items['company'] = $this->_db->escape('company');
        $items['address'] = $this->_db->escape('address');
        $items['city'] = $this->_db->escape('city');
        $items['post_code'] = $this->_db->escape('123');
        $items['del_address'] = $this->_db->escape('deliveryAddress');
        $items['del_city'] = $this->_db->escape('deliveryCity');
        $items['del_post_code'] = $this->_db->escape('456');
        $items['ic'] = $this->_db->escape('ic');
        $items['dic'] = $this->_db->escape('dic');
        $items['phone'] = $this->_db->escape('phone');
        $items['email'] = $this->_db->escape('test@example.org');
        $items['note'] = $this->_db->escape("note\nsecond line");
        $items['deliverer_id'] = 1;
        $items['subtotal'] = 10;
        $items['subtotal_vat'] = 12;
        $items['price_delivery'] = 100;
        $items['price_payment'] = 20;
        $items['grandtotal'] = 110;
        $items['grandtotal_vat'] = 120;
        //$items['parcel_number'] = $this->_db->escape('parcel');
        $sql = 'INSERT INTO eshop_orders ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $this->_db->query('ALTER TABLE eshop_orders CHANGE COLUMN `parcel_number` `parcel_number` CHAR(1) NOT NULL');

        unset($_GET);
        unset($_POST);
        $_POST['id'] = '1';
        $_POST['parcel'] = 'newparcel';
        $_POST['status'] = '2';
        $this->_object->outputAdmin('ajax-saveorder');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals('ERR', $output->status);
        $this->assertEquals('Uložení se nezdařilo', $output->msg);
    }

}