<?php
/**
 * @package    Ergo
 * @subpackage EshopTest
 */

namespace Eshop;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'core/GstLib/Tree.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/eshop/module.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/categories.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/category.php';
require_once ERGO_ROOT_DIR.'modules/eshop/products/datacategory.php';
require_once ERGO_ROOT_DIR.'modules/eshop/admin/controllercategories.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

use Ergo\Response;

/**
 * test class for module Eshop
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage EshopTest
 */
class AdminCategoriesTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Eshop\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Eshop\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
        $this->_db->query('DROP TABLE IF EXISTS eshop_categories');
        $this->_db->query('CREATE TABLE eshop_categories LIKE '.ERGO_DB_NAME_PHPUNIT.'.eshop_categories');
    }

    protected function tearDown()
    {
        $this->_db->query('DROP TABLE IF EXISTS eshop_categories');
        $this->_db->close();
        $this->_app->tearDown();
    }

    public function testListAsNotLogged()
    {
        $this->_object->outputAdmin('categories');
        $this->assertEquals(302, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
    }

    public function testListAsLogged()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $this->_object->outputAdmin('categories');
        $output = $this->_app->response()->pageContent();
        $this->assertContains('<h1>Kategorie zboží</h1>', $output);

        $this->assertEquals('Kategorie zboží | [%LG_SITENAME%]', $this->_app->response()->pageTitle());
    }

    public function testHandleListAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-listcategories');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleList()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 0;
        $items['ordering'] = 0;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo-a');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['parent_id'] = 1;
        $items['ordering'] = 0;
        $items['name'] = $this->_db->escape('B');
        $items['seo'] = $this->_db->escape('seo-b');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta b');
        $items['meta_keys'] = $this->_db->escape('keys b');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items[] = (object) array(
            'id'=>1,
            'name'=>'A',
            'parent_id'=>0,
            'level'=>0
        );
        $items[] = (object) array(
            'id'=>2,
            'name'=>'B',
            'parent_id'=>1,
            'level'=> 1
        );

        $this->_object->outputAdmin('ajax-listcategories');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($items, $output->items);
    }

    public function testHandleEditAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-editcategory');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleEditWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Record not found';
        $this->_object->outputAdmin('ajax-editcategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleEdit()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 5;
        $items['ordering'] = 0;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo-a');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $_GET['id'] = 1;
        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->record = (object) array(
            'id' => 1,
            'parent_id' => 5,
            'name' => 'A',
            'seo' => 'seo-a',
            'publish' => true,
            'meta_desc' => 'meta a',
            'meta_keys' => 'keys a',
            'text' => 'text a'
        );
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-editcategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleSaveEmptyData()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = '[%LG_ERR_MISSING_VALUES%] [%LG_NAME%]';
        unset($_POST);
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveInsert()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_GET);
        unset($_POST);
        $_POST['name'] = 'name';
        $_POST['text'] = ' <p>text</p>';
        $_POST['parent'] = '5';
        $_POST['publish'] = 'on';
        $_POST['meta_desc'] = ' meta a';
        $_POST['meta_keys'] = 'keys a';
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array(
            'id'=>'1',
            'name'=>'name',
            'seo'=>'name',
            'publish'=>'1',
            'parent_id'=>'5',
            'ordering'=>'1',
            'text'=>'<p>text</p>',
            'meta_desc'=>'meta a',
            'meta_keys'=>'keys a'
        );
        $row = $this->_db->fetchArray('SELECT * FROM eshop_categories');
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveUpdate()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 5;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_GET);
        unset($_POST);
        $_POST['id'] = '1';
        $_POST['name'] = 'name2';
        $_POST['seo'] = 'seo';
        $_POST['text'] = ' <p>text2</p>';
        $_POST['parent'] = '3';
        $_POST['meta_desc'] = ' meta a2';
        $_POST['meta_keys'] = 'keys a2';
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array(
            'id'=>'1',
            'name'=>'name2',
            'seo'=>'seo',
            'publish'=>'0',
            'parent_id'=>'3',
            'ordering'=>'1',
            'text'=>'<p>text2</p>',
            'meta_desc'=>'meta a2',
            'meta_keys'=>'keys a2'
        );
        $row = $this->_db->fetchArray('SELECT * FROM eshop_categories');
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveNotUniqueSeo()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 5;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_GET);
        unset($_POST);
        $_POST['name'] = 'name2';
        $_POST['seo'] = 'seo';
        $_POST['text'] = ' <p>text2</p>';
        $_POST['parent'] = '3';
        $_POST['meta_desc'] = ' meta a2';
        $_POST['meta_keys'] = 'keys a2';
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array(
            'id'=>'2',
            'seo'=>'seo-1'
        );
        $row = $this->_db->fetchArray('SELECT id,seo FROM eshop_categories WHERE id=2');
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveNotUniqueOrdering()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 5;
        $items['ordering'] = 0;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['parent_id'] = 5;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('B');
        $items['seo'] = $this->_db->escape('seob');
        $items['text'] = $this->_db->escape('text b');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 3;
        $items['parent_id'] = 1;
        $items['ordering'] = 0;
        $items['name'] = $this->_db->escape('C');
        $items['seo'] = $this->_db->escape('seoc');
        $items['text'] = $this->_db->escape('text c');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_GET);
        unset($_POST);
        $_POST['id'] = '3';
        $_POST['name'] = 'C';
        $_POST['seo'] = 'seoc';
        $_POST['text'] = '';
        $_POST['parent'] = '5';
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array('0', '1', '2');
        $rows = $this->_db->fetchColAll('ordering', 'SELECT ordering FROM eshop_categories WHERE parent_id=5 ORDER BY ordering');
        $this->assertEquals($expect, $rows);
    }

    public function testHandleSaveInsertFail()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $this->_db->query('ALTER TABLE eshop_categories CHANGE COLUMN `name` `name` VARCHAR(10) NOT NULL');

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = '[%LG_ERR_SAVE_FAILED%]';
        unset($_GET);
        unset($_POST);
        $_POST['name'] = 'namenamenamename';
        $_POST['text'] = ' <p>text</p>';

        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $this->assertFalse($this->_db->fetchArray('SELECT * FROM eshop_categories'));
    }

    public function testHandleSaveUpdateFail()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 5;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $this->_db->query('ALTER TABLE eshop_categories CHANGE COLUMN `name` `name` VARCHAR(10) NOT NULL');

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = '[%LG_ERR_SAVE_FAILED%]';
        unset($_GET);
        unset($_POST);
        $_POST['id'] = '1';
        $_POST['name'] = 'namenamenamenamename';
        $_POST['text'] = ' <p>text2</p>';
        $this->_object->outputAdmin('ajax-savecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleDeleteAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-deletecategory');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleDeletetWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;
        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Record not found';
        $this->_object->outputAdmin('ajax-deletecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleDelete()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['parent_id'] = 5;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('seo');
        $items['publish'] = '1';
        $items['meta_desc'] = $this->_db->escape('meta a');
        $items['meta_keys'] = $this->_db->escape('keys a');
        $items['text'] = $this->_db->escape('text a');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $_GET['id'] = 1;
        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-deletecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
        $row = $this->_db->fetchArray('SELECT * FROM eshop_categories');
        $this->assertFalse($row);
    }

//    public function testHandleDeleteFail()
//    {
//    // TODO pujde nejak zpusobit vyhozeni vyjimky pomoci manipulace s db?
//        $this->_app->logTestUser();
//        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);
//
//        $items = array();
//        $items['id'] = 1;
//        $items['parent_id'] = 5;
//        $items['ordering'] = 1;
//        $items['name'] = $this->_db->escape('A');
//        $items['seo'] = $this->_db->escape('seo');
//        $items['publish'] = '1';
//        $items['meta_desc'] = $this->_db->escape('meta a');
//        $items['meta_keys'] = $this->_db->escape('keys a');
//        $items['text'] = $this->_db->escape('text a');
//        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
//        $this->_db->query($sql);
//
//        //$this->_db->query('DROP TABLE IF EXISTS eshop_rel_product_category');
//        $this->_db->query('DROP TABLE eshop_rel_product_category');
//        //$this->_db->query('DROP TABLE eshop_categories');
//         //$this->_db->query('ALTER TABLE eshop_categories CHANGE COLUMN `parent_id` `dummy` VARCHAR(10) NOT NULL');
//
//        $_GET['id'] = 1;
//        $expect = new \stdClass();
//        $expect->status = 'ERR';
//        $expect->msg = '[%LG_ERR_DELETE_FAILED%]';
//        $this->_object->outputAdmin('ajax-deletecategory');
//        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
//
//        $this->assertEquals(1, $this->_db->count('SELECT COUNT(*) FROM eshop_categories'));
//    }

    public function testHandleMoveAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-movecategory');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleMoveWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;
        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Record not found';
        $this->_object->outputAdmin('ajax-movecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testMoveUp()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('a');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['ordering'] = 2;
        $items['name'] = $this->_db->escape('B');
        $items['seo'] = $this->_db->escape('b');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        $_GET['id'] = 2;
        $_GET['direction'] = 'up';
        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-movecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $rows = $this->_db->fetchColAll('id', 'SELECT id FROM eshop_categories ORDER BY ordering');
        $expect = array(2, 1);
        $this->assertEquals($expect, $rows);
    }

    public function testMoveDown()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['ordering'] = 2;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('a');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('B');
        $items['seo'] = $this->_db->escape('b');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        $_GET['id'] = 2;
        $_GET['direction'] = 'down';
        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-movecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $rows = $this->_db->fetchColAll('id', 'SELECT id FROM eshop_categories ORDER BY ordering');
        $expect = array(1, 2);
        $this->assertEquals($expect, $rows);
    }

    public function testMoveDownFirst()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $items = array();
        $items['id'] = 1;
        $items['ordering'] = 1;
        $items['name'] = $this->_db->escape('A');
        $items['seo'] = $this->_db->escape('a');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        $items = array();
        $items['id'] = 2;
        $items['ordering'] = 2;
        $items['name'] = $this->_db->escape('B');
        $items['seo'] = $this->_db->escape('b');
        $items['text'] = $this->_db->escape('');
        $sql = 'INSERT INTO eshop_categories ('.implode(',', array_keys($items)).') VALUES ('.implode(',', $items).')';
        $this->_db->query($sql);

        unset($_GET);
        $_GET['id'] = 1;
        $_GET['direction'] = 'up';
        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-movecategory');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $rows = $this->_db->fetchColAll('id', 'SELECT id FROM eshop_categories ORDER BY ordering');
        $expect = array(1, 2);
        $this->assertEquals($expect, $rows);
    }

}