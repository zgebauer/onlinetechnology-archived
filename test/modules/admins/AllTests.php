<?php
/**
 * @package    Ergo
 * @subpackage AdminsTest
 */

namespace Admins;

require_once 'ModuleTest.php';
require_once 'OutputLoginTest.php';
require_once 'OutputAdminTest.php';

/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage AdminsTest
 */
class Module_AllTests
{
    /**
     * run all tests of module
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('ModuleAdmins');
        $suite->addTestSuite('\Admins\ModuleTest');
        $suite->addTestSuite('\Admins\OutputLoginTest');
        $suite->addTestSuite('\Admins\OutputAdminTest');
        return $suite;
    }
}