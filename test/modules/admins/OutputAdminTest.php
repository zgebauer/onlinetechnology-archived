<?php
/**
 * @package    Ergo
 * @subpackage AdminsTest
 */

namespace Admins;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/admins/module.php';
require_once ERGO_ROOT_DIR.'modules/admins/user.php';
require_once ERGO_ROOT_DIR.'modules/admins/data.php';
require_once ERGO_ROOT_DIR.'modules/admins/admin/controller.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

use GstLib\Html;
use Ergo\Response;

/**
 * test class for module Admins
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage AdminsTest
 */
class OutputAdminTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Admins\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Admins\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
    }

    protected function tearDown()
    {
        $this->_app->tearDown();
    }

    public function testListAsNotLogged()
    {
        $this->_object->outputAdmin('');
        $this->assertEquals(302, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
    }

    public function testListAsLogged()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $this->_object->outputAdmin('');
        $output = $this->_app->response()->pageContent();
        $this->assertContains('<h1>Správci</h1>', $output);
        // filter
        $this->assertContains('<form action="#" method="post" class="frmFilter">', $output);
        $this->assertContains('<input type="text" name="f_login" id="fldLogin" maxlength="10" />', $output);
        $this->assertContains('<input type="text" name="f_name" id="fldName" maxlength="20" />', $output);
        // pagination
        $options = array(10=>'10', 20=>'20', 50=>'50', 100=>'100');
        $this->assertContains('<select data-rows-page="" id="fldRows">'.Html::getOptions($options).'</select>', $output);
        // orders
        $this->assertContains('<span data-orderby="login"', $output);
        $this->assertContains('<span data-orderby="login" data-orderdesc="1"', $output);
        $this->assertContains('<span data-orderby="name"', $output);
        $this->assertContains('<span data-orderby="name" data-orderdesc="1"', $output);
        // positions
        $this->assertContains('<select data-pos=""></select>', $output);
        $this->assertContains('<span data-pos-start=""></span>', $output);
        $this->assertContains('<span data-pos-end=""></span>', $output);
        $this->assertContains('<span data-total=""></span>', $output);

        $this->assertEquals('Správci', $this->_app->response()->pageTitle());
    }

    public function testHandleListAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-list');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleList()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        for ($i = 2; $i < 22; $i++) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $this->_db->query(
                "INSERT INTO [%core_users%] (id,login,name,block,last_log)
                VALUES(".$tmp.",'login".$tmp."','name".$tmp."',0,'2012-12-31')"
            );
        }

        $items = array();
        $items[] = (object) array(
            'id'=>1,
            'login'=> 'login',
            'name'=> 'name',
            'last_log'=> '',
            'blocked'=> ''
        );
        for ($i = 2; $i < 11; $i++) {
            $tmp = str_pad($i, 2, '0', STR_PAD_LEFT);
            $items[] = (object) array(
                'id'=>$i,
                'login'=> 'login'.$tmp,
                'name'=> 'name'.$tmp,
                'last_log'=> '31.12.2012 00:00',
                'blocked'=> ''
            );
        }

        $this->_object->outputAdmin('ajax-list');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($items, $output->items);
        $this->assertEquals(array(1, 11, 21), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(10, $output->posEnd);
        $this->assertEquals(21, $output->total);

        // filtered list
        unset($_GET);
        $_GET['f_name'] = 'name21';
        $items = array((object) array(
            'id'=>21,
            'login'=> 'login21',
            'name'=> 'name21',
            'last_log'=> '31.12.2012 00:00',
            'blocked'=> ''
        ));
        $this->_object->outputAdmin('ajax-list');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($items, $output->items);
        $this->assertEquals(array(1,), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(1, $output->posEnd);
        $this->assertEquals(1, $output->total);

        unset($_GET);
        $_GET['f_login'] = 'login10';
        $items = array((object) array(
            'id'=>10,
            'login'=> 'login10',
            'name'=> 'name10',
            'last_log'=> '31.12.2012 00:00',
            'blocked'=> ''
        ));
        $this->_object->outputAdmin('ajax-list');
        $output = json_decode($this->_app->response()->pageContent());
        $this->assertEquals($items, $output->items);
        $this->assertEquals(array(1,), $output->pages);
        $this->assertEquals(1, $output->pos);
        $this->assertEquals(1, $output->posEnd);
        $this->assertEquals(1, $output->total);
    }

    public function testHandleEditAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-edit');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleEditWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Record not found';
        $this->_object->outputAdmin('ajax-edit');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleEdit()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 1;

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->record = (object) array(
            'id' => 1,
            'login' => 'login',
            'name' => 'name',
            'blocked' => false,
            'last_log' => '',
            'permissions' => (object) array('admins'=>array(0))
        );
        $expect->msg = '';
        $this->_object->outputAdmin('ajax-edit');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleSaveEmptyData()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Chybí údaje: uživatelské jméno, heslo, potvzení hesla';
        unset($_POST);
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveInvalidData()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Heslo a potvrzení hesla není stejné!';
        unset($_POST);
        $_POST['login'] = 'login';
        $_POST['password'] = 'password';
        $_POST['confirm'] = 'different';
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveNotUniqueLogin()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'ERR';
        $expect->msg = 'Uživatelské jméno už existuje, použijte jiné';
        unset($_POST);
        $_POST['login'] = 'login';
        $_POST['password'] = 'password';
        $_POST['confirm'] = 'password';
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testHandleSaveInsert()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_GET);
        unset($_POST);
        $_POST['login'] = 'newlogin';
        $_POST['password'] = 'password';
        $_POST['confirm'] = 'password';
        $_POST['name'] = 'name';
        $_POST['block'] = 'on';
        $_POST['module'] = array(Module::NAME); // TODO other modules
        $_POST['permissions'][Module::NAME] = array('1');
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array(
            'id'=>'2',
            'login'=>'newlogin',
            'name'=>'name',
            'block'=>'1',
            'last_log'=>'0000-00-00 00:00:00'
        );
        $row = $this->_db->fetchArray('SELECT id,login,name,block,last_log FROM core_users WHERE id=2');
        $this->assertEquals($expect, $row);

        $expect = array('admins');
        $row = $this->_db->fetchColAll('module', 'SELECT module FROM core_permissions WHERE user_id=2');
        $this->assertEquals($expect, $row);
    }

    public function testHandleSaveUpdate()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        // edit record
        $this->_db->query("INSERT INTO core_users (id,login,pwd,name,last_log,block)
            VALUES(2,'login2','password','name2','2012-12-31 10:20:30',1)");
        $this->_app->addPermission(2, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_POST);
        $_POST['id'] = 2;
        $_POST['name'] = 'newname';
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array(
            'id'=>'2',
            'login'=>'login2',
            'pwd'=>'password',
            'name'=>'newname',
            'block'=>'0',
            'last_log'=>'2012-12-31 10:20:30'
        );
        $row = $this->_db->fetchArray('SELECT id,login,pwd,name,block,last_log FROM core_users WHERE id=2');
        $this->assertEquals($expect, $row);

        $row = $this->_db->fetchColAll('module', 'SELECT module FROM core_permissions WHERE user_id=2');
        $this->assertEquals(array(), $row);
    }

    public function testHandleSaveChangePassword()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        // edit record
        $this->_db->query("INSERT INTO core_users (id,login,pwd,name,last_log,block)
            VALUES(2,'login2','password','name2','2012-12-31 10:20:30',1)");
        $this->_app->addPermission(2, Module::NAME);

        $expect = new \stdClass();
        $expect->status = 'OK';
        $expect->msg = '';
        unset($_POST);
        $_POST['id'] = 2;
        $_POST['name'] = 'newname';
        $_POST['change_password'] = 'on';
        $_POST['password'] = 'newpassword';
        $_POST['confirm'] = 'newpassword';
        $this->_object->outputAdmin('ajax-save');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $expect = array(
            'id'=>'2',
            'login'=>'login2',
            'name'=>'newname',
            'block'=>'0',
            'last_log'=>'2012-12-31 10:20:30'
        );
        $row = $this->_db->fetchArray('SELECT id,login,name,block,last_log FROM core_users WHERE id=2');
        $this->assertEquals($expect, $row);
        $row = $this->_db->fetchArray('SELECT pwd FROM core_users WHERE id=2');
        $this->assertNotEquals('password', $row['pwd']);
    }

    public function testHandleDeleteAsNotLogged()
    {
        $this->_object->outputAdmin('ajax-delete');
        $this->assertEquals(Response::HTTP_FORBIDDEN, $this->_app->response()->httpCode());
    }

    public function testHandleDeletetWithInvalidId()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $_GET['id'] = 10;
        $expect = new \stdClass();
        $expect->status = 'OK';
        $this->_object->outputAdmin('ajax-delete');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));
    }

    public function testDelete()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        // edit record
        $this->_db->query("INSERT INTO core_users (id,login,pwd,name,last_log,block)
            VALUES(2,'login2','password','name2','2012-12-31 10:20:30',1)");
        $this->_app->addPermission(2, Module::NAME);

        $_GET['id'] = 2;
        $expect = new \stdClass();
        $expect->status = 'OK';
        $this->_object->outputAdmin('ajax-delete');
        $this->assertEquals($expect, json_decode($this->_app->response()->pageContent()));

        $row = $this->_db->fetchArray('SELECT * FROM [%core_users%] WHERE id=2');
        $this->assertFalse($row);
        $row = $this->_db->fetchArray('SELECT * FROM [%core_permissions%] WHERE user_id=2');
        $this->assertFalse($row);
        $count = $this->_db->count('SELECT COUNT(*) FROM [%core_users%] WHERE id=1');
        $this->assertEquals(1, $count);
    }

}