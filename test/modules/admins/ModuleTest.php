<?php
/**
 * @package    Ergo
 * @subpackage AdminsTest
 */

namespace Admins;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/admins/module.php';
require_once ERGO_ROOT_DIR.'modules/admins/user.php';
require_once ERGO_ROOT_DIR.'modules/admins/data.php';

require_once dirname(dirname(__DIR__)).'/test_app.php';

/**
 * test class for module Admins
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage AdminsTest
 */
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Admins\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Admins\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_db->query('USE '.ERGO_DB_NAME);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        $this->_app->tearDown();
    }

    public function testDefaultMethods()
    {
        $this->assertEquals('admins', Module::NAME);
        $this->assertNull($this->_object->menuItems());
        $this->assertEquals(array(), $this->_object->permissions());
        $this->assertEquals('Správci', $this->_object->name());
        $this->assertNull($this->_object->cron());
    }

    public function testMenuItemsAsLogged()
    {
        $this->_app->createBaseTables();
        $this->_app->logTestUser();
        $this->assertNull($this->_object->menuItems());
    }

    public function testMenuItemsAsLoggedWithPermissions()
    {
        $this->_app->createBaseTables();
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, Module::NAME);

        $expect = array(array(
            'name' => $this->_object->name(),
            'url' => $this->_app->getUrl(Module::NAME),
            'title' => 'Seznam správců',
            'highlight' => false
        ));
        $this->assertEquals($expect, $this->_object->menuItems());
    }

    public function testUndefinedParameter()
    {
        $this->_object->output('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());

        $this->_object->outputAdmin('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());
    }

}