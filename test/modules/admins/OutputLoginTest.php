<?php
/**
 * @package    Ergo
 * @subpackage AdminsTest
 */

namespace Admins;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';

require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/admins/module.php';
require_once ERGO_ROOT_DIR.'modules/admins/user.php';
require_once ERGO_ROOT_DIR.'modules/admins/data.php';
require_once ERGO_ROOT_DIR.'modules/admins/admin/controllerauth.php';

require_once dirname(dirname(__DIR__)).'/test_app.php';

/**
 * test class for module Admins login, logout
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage AdminsTest
 */
class OutputLoginTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Admins\Module tested object */
    protected $_object;
    /** @var Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Admins\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_db->query('USE '.ERGO_DB_NAME);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        $this->_app->tearDown();
    }

    /**
     * test show login form
     */
    public function testBoxLoginNotLogged()
    {
        $output = $this->_object->outputAdmin('_box_login_');
        $this->assertContains('<form action="'.ERGO_ROOT_URL.\TestApplication::ADMIN_PATH.'/" method="post" id="frmLogin">', $output);
        $this->assertContains('<p class="msg"></p>', $output);
        $this->assertContains('<input type="text" name="login" id="fldLogin" maxlength="10" autofocus="autofocus" required />', $output);
        $this->assertContains('<input type="password" name="pwd" id="fldPwd" required />', $output);
        $this->assertRegexp('<input type="hidden" name="ergo_csrf" value="(.*)" />', $output);
        $this->assertContains('<input type="submit" value="[%LG_BUTTON_LOGIN%]" />', $output);
    }

    /**
     * test submit empty form
     */
    public function testLoginWithEmptyData()
    {
        $this->_app->createBaseTables();

        unset($_POST);
        $_POST['login'] = '';
        $_POST['pwd'] = '';
        $_POST[\Ergo\Csrf::CSRF_VAR] = $this->_app->csrf()->getCsrf();

        $output = $this->_object->outputAdmin('_box_login_');
        $this->assertContains('<p class="msg">Vyplňte uživatelské jméno a heslo</p>', $output);
    }

    /**
     * test submit form with invalid data
     */
    public function testLoginWithInvalidData()
    {
        $this->_app->createBaseTables();

        unset($_POST);
        $_POST['login'] = 'invalid';
        $_POST['pwd'] = 'invalid';
        $_POST[\Ergo\Csrf::CSRF_VAR] = $this->_app->csrf()->getCsrf();

        $output = $this->_object->outputAdmin('_box_login_');
        $this->assertContains('<p class="msg">Uživatelské jméno nebo heslo není platné</p>', $output);
    }

    public function testLoginAsValidUserWithInvalidPassword()
    {
        $this->_app->createBaseTables();
        $this->_db->query("INSERT INTO core_users (id,login,pwd)
            VALUES(1,'admin',"
            .$this->_db->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').")");

        unset($_POST);
        $_POST['login'] = 'admin';
        $_POST['pwd'] = 'invalid';
        $_POST[\Ergo\Csrf::CSRF_VAR] = $this->_app->csrf()->getCsrf();

        $output = $this->_object->outputAdmin('_box_login_');
        $this->assertContains('<p class="msg">Uživatelské jméno nebo heslo není platné</p>', $output);
    }

    /**
     * test login as valid user
     */
    public function testLoginAsValidUser()
    {
        $this->_app->createBaseTables();
        $this->_db->query("INSERT INTO core_users (id,login,pwd)
            VALUES(1,'admin',"
            .$this->_db->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').")");

        unset($_POST);
        $_POST['login'] = 'admin';
        $_POST['pwd'] = 'ergo';
        $_POST[\Ergo\Csrf::CSRF_VAR] = $this->_app->csrf()->getCsrf();

        $this->_object->outputAdmin('_box_login_');
        // redirect to root url?
        $this->assertEquals(303, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
        // last login time?
        $row = $this->_db->fetchArray('SELECT last_log FROM core_users WHERE id=1');
        $this->assertEquals(gmdate('Y-m-d H:i:'), substr($row['last_log'], 0, -2));

        $this->assertEquals($_SESSION[Module::SESSION_USER_ID], 1);
    }

    /**
     * test logout()
     */
    public function testLogout()
    {
        $_SESSION[Module::SESSION_USER_ID] = 10;
        $this->_object->outputAdmin('logout');
        $this->assertEquals(302, $this->_app->response()->httpCode());
        $this->assertEquals(ERGO_ROOT_URL.'admin/', $this->_app->response()->redirectUrl());
        $this->assertFalse(isset($_SESSION[Module::SESSION_USER_ID]));
    }

    public function testBoxLoginAsLogged()
    {
        $this->_app->createBaseTables();
        $this->_db->query("INSERT INTO core_users (id,login,pwd)
            VALUES(1,'admin',"
            .$this->_db->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').")");
        $_SESSION[Module::SESSION_USER_ID] = 1;
        $this->assertEquals('', $this->_object->outputAdmin('_box_login_'));
    }

    /**
     * test output('_box_current_user_') as not logged user
     */
    public function testBoxUserAsNotLogged ()
    {
        $this->assertEquals('', $this->_object->outputAdmin('_box_current_user_'));
    }

    public function testBoxUserAsLoggedWithInvalidId()
    {
        $this->_app->createBaseTables();
        $this->_db->query("INSERT INTO core_users (id,login,pwd)
            VALUES(1,'admin',"
            .$this->_db->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').")");

        $_SESSION[Module::SESSION_USER_ID] = 999;
        $this->assertEquals('', $this->_object->outputAdmin('_box_current_user_'));
    }

    /**
     * test output('_box_current_user_') as logged user
     */
    public function testBoxUserAsLogged ()
    {
        $this->_app->createBaseTables();
        $this->_db->query("INSERT INTO core_users (id,login,name,pwd)
            VALUES(1,'admin', 'name of user',"
            .$this->_db->escape('$2a$07$dsvJZ9eeKv536dvLfL82$.rdPF99QTpyFOKDvqFrAiFKXSkfYdzVm').")");
        $_SESSION[Module::SESSION_USER_ID] = 1;

        $output = $this->_object->outputAdmin('_box_current_user_');
        $this->assertContains('name of user', $output);
        $this->assertContains('<a href="'.$this->_app->getUrl(Module::NAME, 'logout').'"', $output);
    }
}