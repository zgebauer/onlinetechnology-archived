<?php
/**
 * @package    Ergo
 * @subpackage IncludesTest
 */

namespace Includes;

require_once 'ModuleTest.php';
require_once 'OutputWebTest.php';
require_once 'OutputAdminTest.php';

/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage IncludesTest
 */
class Module_AllTests
{
    /**
     * run all tests of module
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('ModuleIncludes');
        $suite->addTestSuite('\Includes\ModuleTest');
        $suite->addTestSuite('\Includes\OutputWebTest');
        $suite->addTestSuite('\Includes\OutputAdminTest');
        return $suite;
    }
}