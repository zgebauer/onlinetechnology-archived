<?php
/**
 * @package    Ergo
 * @subpackage IncludesTest
 */

namespace Includes;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/includes/module.php';

/**
 * test class for module Includes
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage IncludesTest
 */
class OutputWebTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Includes\Module tested object */
    protected $_object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $this->_app = new \Ergo\Application();
        $this->_app->init($config);
        $this->_object = new Module($this->_app);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    public function testOutputUndefined()
    {
        $this->assertEquals('', $this->_object->output('not-exists'));
    }

    public function testGa()
    {
        $this->assertEquals('', $this->_object->output('_ga'));
    }

    public function testGaProductionMode()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->mode(\Ergo\Config::MODE_PRODUCTION);
        $app = new \Ergo\Application();
        $app->init($config);
        $object = new Module($app);
        $this->assertContains('UA-23910579-2', $object->output('_ga'));
    }


}