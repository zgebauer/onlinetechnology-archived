<?php
/**
 * @package    Ergo
 * @subpackage IncludesTest
 */

namespace Includes;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/includes/module.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

/**
 * test class for module Includes
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage IncludesTest
 */
class OutputAdminTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Includes\Module tested object */
    protected $_object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_db->query('USE '.ERGO_DB_NAME);
        $this->_db->query('DROP TABLE IF EXISTS core_users');
        $this->_db->query('DROP TABLE IF EXISTS core_permissions');
        $this->_db->query('CREATE TABLE core_users LIKE '.ERGO_DB_NAME_PHPUNIT.'.core_users');
        $this->_db->query('CREATE TABLE core_permissions LIKE '.ERGO_DB_NAME_PHPUNIT.'.core_permissions');
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        $this->_db->query('USE '.ERGO_DB_NAME);
        $this->_db->query('DROP TABLE IF EXISTS core_users');
        $this->_db->query('DROP TABLE IF EXISTS core_permissions');
    }

    public function testOutputUndefined()
    {
        $this->assertEquals('', $this->_object->outputAdmin('not-exists'));
    }

    /** test ooutput('_js') */
    /*public function testJs()
    {
        $output = $this->_object->outputAdmin('_js_');
        $this->assertContains('<script src="'.$this->_app->baseUrl().'modules/admins/admin/scr.js', $output);
        $this->assertContains('<script src="'.$this->_app->baseUrl().'facades/'.\TestApplication::FACADE.'/admin/scr.js', $output);
    }*/

    /** test ooutput('_js') in production mode */
    /*public function testJsInProduction()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->mode(\Ergo\Config::MODE_PRODUCTION);
        $config->facade(\TestApplication::FACADE);
        $app = new \Ergo\Application();
        $app->init($config);
        $object = new \Includes\Module($app);
        $output = $object->outputAdmin('_js_');
        $this->assertNotContains('/scr.js"></script>', $output);
        $this->assertContains('<script src="'.$this->_app->baseUrl().'facades/'.\TestApplication::FACADE.'/admin/default.min.js"></script>', $output);
    }*/

}