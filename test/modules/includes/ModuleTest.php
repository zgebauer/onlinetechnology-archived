<?php
/**
 * @package    Ergo
 * @subpackage IncludesTest
 */

namespace Includes;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/includes/module.php';

/**
 * test class for module Includes
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage IncludesTest
 */
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Includes\Module tested object */
    protected $_object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $this->_app = new \Ergo\Application();
        $this->_app->init($config);
        $this->_object = new \Includes\Module($this->_app);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    public function testDefaultMethods()
    {
        $this->assertEquals('includes', Module::NAME);
        $this->assertNull($this->_object->menuItems());
        $this->assertNull($this->_object->permissions());
        $this->assertEquals('Includes', $this->_object->name());
        $this->assertNull($this->_object->cron());
    }

    public function testUndefinedParameter()
    {
        $this->_object->output('undefined');
        $this->assertEquals('', $this->_app->response()->pageContent());

        $this->_object->output('');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());

        $this->_object->outputAdmin('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());
    }
}