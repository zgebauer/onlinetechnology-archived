<?php
/**
 * @package    Ergo
 * @subpackage CoreTest
 */

namespace Core;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/core/module.php';

/**
 * test class for module Core
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage CoreTest
 */
class OutputWebTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Core\Module tested object */
    protected $_object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $this->_app = new \Ergo\Application();
        $this->_app->init($config);
        $this->_object = new \Core\Module($this->_app);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test of homepage() */
    public function testHomepage()
    {
        $this->assertNull($this->_object->output(''));
        $output = $this->_app->response()->pageContent();
        $this->assertContains('Ergo', $output);
        $this->assertContains('Default homepage', $output);

        $this->assertEquals('[%LG_DEFAULT_META_TITLE%]', $this->_app->response()->pageTitle());
        $this->assertEquals('[%LG_DEFAULT_META_DESCRIPTION%]', $this->_app->response()->metaDescription());
        $this->assertEquals('[%LG_DEFAULT_META_KEYWORDS%]', $this->_app->response()->metaKeywords());
        $this->assertEquals(ERGO_ROOT_URL, $this->_app->response()->canonicalUrl());
    }

    /** test of rende 404 page () */
/*    public function test404()
    {
        $output = $this->_object->output('404');
        $this->assertContains('HTTP 404', $output);

        $this->assertEquals('404 | [%LG_DEFAULT_META_TITLE%]', $this->_object->metaTitle());
        $this->assertEquals('', $this->_object->metaDescription());
        $this->assertEquals('', $this->_object->metaKeywords());
        $this->assertEquals('', $this->_object->metaLinks());
    }*/

}