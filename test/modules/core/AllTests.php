<?php
/**
 * @package    Ergo
 * @subpackage CoreTest
 */

namespace Core;

require_once 'ModuleTest.php';
require_once 'OutputWebTest.php';
require_once 'OutputAdminTest.php';
require_once 'SitemapTest.php';

/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage CoreTest
 */
class Module_AllTests
{
    /**
     * run all tests of module
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('ModuleCore');
        $suite->addTestSuite('\Core\ModuleTest');
        $suite->addTestSuite('\Core\OutputWebTest');
        $suite->addTestSuite('\Core\OutputAdminTest');
        $suite->addTestSuite('\Core\SitemapTest');
        return $suite;
    }
}