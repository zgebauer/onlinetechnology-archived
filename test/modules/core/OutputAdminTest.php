<?php
/**
 * @package    Ergo
 * @subpackage CoreTest
 */

namespace Core;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/core/module.php';
require_once ERGO_ROOT_DIR.'modules/admins/module.php';
require_once ERGO_ROOT_DIR.'modules/admins/user.php';
require_once ERGO_ROOT_DIR.'modules/admins/data.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

/**
 * test class for module Core
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage CoreTest
 */
class OutputAdminTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Core\Module tested object */
    protected $_object;
    /** @var \Ergo\ApplicationInterface application context */
    protected $_app;
    /** @var \GstLib\Db\DriverMysqli connection to db */
    protected $_db;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->isAdmin(true);
        $config->adminPath(\TestApplication::ADMIN_PATH);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Core\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        $this->_app->tearDown();
    }

    /** test of homepage() */
    public function testHomepage()
    {
        $this->assertNull($this->_object->outputAdmin(''));
        $output = $this->_app->response()->pageContent();
        $this->assertContains('Ergo: admin', $output);

        $this->assertEquals('[%LG_DEFAULT_META_TITLE%]', $this->_app->response()->pageTitle());
        $this->assertEquals('', $this->_app->response()->metaDescription());
        $this->assertEquals('', $this->_app->response()->metaKeywords());
        $this->assertEquals('', $this->_app->response()->canonicalUrl());
    }

    /** test of output('_box_menu_') as not logged user */
    public function testBoxMenuAsNotLogged()
    {
        class_alias('\Ergo\Application', 'ErgoApplication');
        $mockApp = $this->getMock('ErgoApplication', array('getUser'));
        $mockApp->expects($this->any())
            ->method('getUser')
            ->will($this->returnValue(null));
        $object = new \Core\Module($mockApp);
        $this->assertEquals('', $object->outputAdmin('_box_menu_'));
    }

    public function testBoxMenuAsLogged()
    {
        $this->_app->logTestUser();
        $this->_app->addPermission(\TestApplication::TEST_USER_ID, 'admins');

        $output = $this->_object->outputAdmin('_box_menu_');
        $this->assertContains('<a href="'.ERGO_ROOT_URL.'admin/admins" title="Seznam správců">Správci</a>', $output);
    }

    public function testBoxMenuAsLoggedWithoutPermissions()
    {
        $this->_app->logTestUser();
        $this->assertEquals('', $this->_object->outputAdmin('_box_menu_'));
    }

}