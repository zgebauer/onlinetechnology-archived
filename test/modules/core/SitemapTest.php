<?php
/**
 * @package    Ergo
 * @subpackage CoreTest
 */

namespace Core;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/app.php';
require_once ERGO_ROOT_DIR.'modules/core/module.php';
require_once dirname(dirname(__DIR__)).'/test_app.php';

/**
 * test class for module Core
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage CoreTest
 */
class SitemapTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Core\Module tested object */
    protected $_object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $config->facade(\TestApplication::FACADE);
        $this->_app = new \TestApplication();
        $this->_app->init($config);
        $this->_object = new \Core\Module($this->_app);

        $this->_db = new \GstLib\Db\DriverMysqli(array(
            'server' => ERGO_DB_SERVER,
            'user' => ERGO_DB_USER,
            'password' => ERGO_DB_PWD,
            'database' => ERGO_DB_NAME
        ));
        $this->_app->createBaseTables();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        //$this->_db->query('DROP TABLE IF EXISTS users_registrations');
        $this->_db->close();
        $this->_app->tearDown();
    }

    public function testRefreshSitemapLinks()
    {
        $this->assertNull($this->_object->refreshSitemapLinks());

        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $expect = array(
          'module' => Module::NAME,
          'last_refresh' => $date->format('Y-m-d H:i:'),
          'url' => ERGO_ROOT_URL,
          'last_mod' => '0000-00-00 00:00:00',
          'change_freq' => 'daily',
          'priority' => '1.0'
        );

        $sql = "SELECT COUNT(*) FROM [%core_sitemap%] WHERE module='".Module::NAME."'";
        $this->assertEquals(1, $this->_db->count($sql));

        $sql = "SELECT * FROM [%core_sitemap%] WHERE module='".Module::NAME."'";
        $row = $this->_db->fetchArray($sql);
        $row['last_refresh'] = substr($row['last_refresh'], 0, -2);
        $this->assertEquals($expect, $row);
    }

}