<?php
/**
 * @package    Ergo
 * @subpackage CoreTest
 */

namespace Core;

require_once dirname(dirname(dirname(__DIR__))).'/test/config.php';
require_once ERGO_ROOT_DIR.'core/config.php';
require_once ERGO_ROOT_DIR.'core/app.php';
require_once ERGO_ROOT_DIR.'modules/core/module.php';

/**
 * test class for module Core
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage CoreTest
 */
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Core\Module tested object */
    protected $_object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $config = new \Ergo\Config(ERGO_ROOT_URL, ERGO_ROOT_DIR);
        $this->_app = new \Ergo\Application();
        $this->_app->init($config);
        $this->_object = new \Core\Module($this->_app);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    public function testDefaultMethods()
    {
        $this->assertEquals('core', Module::NAME);
        $this->assertNull($this->_object->menuItems());
        $this->assertNull($this->_object->permissions());
        $this->assertEquals('Core', $this->_object->name());
    }

    public function testUndefinedParameter()
    {
        $this->_object->output('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());

        $this->_object->outputAdmin('undefined');
        $this->assertContains('HTTP 404', $this->_app->response()->pageContent());
    }

}