<?php
/**
 * @package    Ergo
 * @subpackage ErgoTest
 */

namespace Ergo;

require_once 'core/GstLib/AllTests.php';
require_once 'core/AllTests.php';
require_once 'modules/core/AllTests.php';
require_once 'modules/includes/AllTests.php';
require_once 'modules/admins/AllTests.php';
require_once 'modules/eshop/AllTests.php';


/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage ErgoTest
 */
class Ergo_AllTests extends \PHPUnit_Framework_TestSuite
{
    /**
     * run all tests
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('Strips');
        $suite->addTestSuite('\GstLib\AllTests');
        $suite->addTestSuite('\Ergo\CoreAllTests');
        $suite->addTestSuite('\Core\Module_AllTests');
        $suite->addTestSuite('\Includes\Module_AllTests');
        $suite->addTestSuite('\Admins\Module_AllTests');
        $suite->addTestSuite('\Eshop\Module_AllTests');
        return $suite;
    }
}