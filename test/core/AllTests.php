<?php
/**
 * @package    Ergo
 * @subpackage ErgoTest
 */

namespace Ergo;

require_once 'CsrfTest.php';
require_once 'RouterTest.php';

/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage ErgoTest
 */
class CoreAllTests extends \PHPUnit_Framework_TestSuite
{
    /**
     * runs all tests of core
     * @return \PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('\Ergo');
        $suite->addTestSuite('\Ergo\CsrfTest');
        $suite->addTestSuite('\Ergo\RouterTest');
        return $suite;
    }
}