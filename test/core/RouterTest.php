<?php
/**
 * class Ergo_RouterTest
 *
 * @package    Ergo
 * @subpackage ErgoTest
 */

namespace Ergo;

/** */
require_once dirname(dirname(__DIR__)).'/www/core/router.php';

/**
 * test class for Ergo_Router
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage ErgoTest
 */
class RouterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    public function testParseWithoutAllowedModules()
    {
        $obj = new Router('http://example.org/');
        $obj->parseUrl('http://example.org/module/parameter');
        $this->assertEquals('', $obj->getModule());
        $this->assertEquals('module/parameter', $obj->getParameter());
    }

    public function testParseWithAllowedModules()
    {
        $obj = new Router('http://example.org/');
        $obj->setModules(array('module'), 'module');
        $obj->parseUrl('http://example.org/module/parameter');
        $this->assertEquals('module', $obj->getModule());
        $this->assertEquals('parameter', $obj->getParameter());
    }

    public function testGetUrl()
    {
        $obj = new Router('http://example.org/');
        $expect = 'http://example.com/module/parameter';
        $this->assertEquals($expect, $obj->createUrl('http://example.com/', 'module', 'parameter'));
    }

}