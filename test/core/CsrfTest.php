<?php
/**
 * @package    Ergo
 * @subpackage ErgoTest
 */

namespace Ergo;

require_once dirname(dirname(__DIR__)).'/www/core/csrf.php';
require_once dirname(dirname(__DIR__)).'/www/core/app.php';

/**
 * test class for Csrf
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    Ergo
 * @subpackage ErgoTest
 */
class CsrfTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test of init */
    public function testRefreshCsrfHidden()
    {
        $mockApp = $this->getMock('\Ergo\ApplicationInterface');

        $obj = new Csrf($mockApp);
        $output = $obj->renderCsrfHidden();
        $this->assertRegExp('/<input type="hidden" name="'.Csrf::CSRF_VAR.'" value="(.*)" \/>/', $output);
        preg_match('/value="(.*)"/i', $output, $result);
        $this->assertEquals($result[1], $_SESSION[Csrf::CSRF_VAR]);

        $output = $obj->renderCsrfHidden('token');
        $this->assertEquals('<input type="hidden" name="'.Csrf::CSRF_VAR.'" value="token" />', $output);
        // token in session remains unchanged
        $this->assertEquals($result[1], $_SESSION[Csrf::CSRF_VAR]);
        unset($_SESSION[Csrf::CSRF_VAR]);
    }

    public function testCheckCsrf()
    {
        class_alias('\Ergo\Request', 'Request');
        $mockRequest = $this->getMock('Request');
        $mockRequest->expects($this->any())
            ->method('getValue')
            ->will($this->returnValue('token'));

        class_alias('\Ergo\Application', 'Application');
        $mockApp = $this->getMock('Application');
        $mockApp->expects($this->any())
            ->method('request')
            ->will($this->returnValue($mockRequest));


        $obj = new Csrf($mockApp);

        $this->assertFalse($obj->checkCsrf());

        $_SESSION[Csrf::CSRF_VAR] = 'token';
        $this->assertTrue($obj->checkCsrf());
    }
}