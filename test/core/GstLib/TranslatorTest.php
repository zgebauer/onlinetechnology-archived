<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Translator.php';

/**
 * test class for Translator
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class TranslatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test __construct() */
    public function testConstructor()
    {
        try {
            $obj = @new Translator('csc', array());
        } catch (Exception $e) {
            $this->assertEquals('lang have to be valid ISO 639-1 code', $e->getMessage());
        }
        try {
            $obj = @new Translator('cs', array('file'));
        } catch (Exception $e) {
            $this->assertEquals('cannot read file file', $e->getMessage());
        }
    }

    /** test of getText() */
    public function testGetText()
    {
        $files[0] = sys_get_temp_dir().'file.txt';
        file_put_contents($files[0], "#comment\n\nTOKEN_1;czech token1\nTOKEN_2;czech token2");
        $files[1] = sys_get_temp_dir().'anotherfile.txt';
        file_put_contents($files[1], "TOKEN_3;czech token3\nINVALID");
        $obj = new Translator('cs', $files);

        try {
            $obj->lang = 'invalid';
        } catch (Exception $e) {
            $this->assertEquals('property lang have to be valid ISO 639-1 code', $e->getMessage());
        }

        $this->assertEquals('', $obj->getText(''));
        $this->assertEquals('czech token1', $obj->getText('TOKEN_1'));
        $this->assertEquals('czech token2', $obj->getText('TOKEN_2'));
        $this->assertEquals('czech token3', $obj->getText('TOKEN_3'));
        try {
            @$this->assertEquals('UNDEFINED', $obj->getText('UNDEFINED'));
        } catch (Exception $e) {
            $this->assertEquals('translation not found: UNDEFINED (lang: cs)', $e->getMessage());
        }
        unlink($files[0]);
        unlink($files[1]);
    }

    /** test translate() */
    public function testTranslate()
    {
        $files[0] = sys_get_temp_dir().'file.txt';
        file_put_contents($files[0], "#comment\n\nTOKEN_1;czech token1\nTOKEN_2;czech token2");
        $obj = new Translator('cs', $files);

        $input = 'a [%LG_TOKEN_1%] b [%LG_TOKEN_2%] c [%LG_TOKEN_2%]';
        $expect = 'a czech token1 b czech token2 c czech token2';
        $this->assertEquals($expect, $obj->translate($input));

        $expect = array('TOKEN_1'=>'czech token1', 'TOKEN_2'=>'czech token2', 'LANG'=>'cs');
        $this->assertEquals($expect, $obj->getStrings());

        unlink($files[0]);
    }

}