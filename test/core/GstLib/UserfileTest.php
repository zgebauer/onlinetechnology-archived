<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/UserFile.php';

/**
 * test class for UserFile
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class UserFileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->_dir = sys_get_temp_dir().'/gstuserfile';
        Filesystem::rmDir($this->_dir);
        @mkdir($this->_dir);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        Filesystem::rmDir($this->_dir);
    }

    /** test cof constants */
    public function testConstants()
    {
        $this->assertEquals(-1, UserFile::FILES_PER_DIR_UNKNOWN);
        $this->assertEquals(0, UserFile::FILES_PER_DIR_UNLIMITED);
        $this->assertEquals(1, UserFile::FILES_PER_DIR_10);
        $this->assertEquals(2, UserFile::FILES_PER_DIR_100);
        $this->assertEquals(3, UserFile::FILES_PER_DIR_1000);
    }

    /** test of createFileName */
    public function testCreateFileName()
    {
        $this->assertEquals('1_file.jpg', UserFile::createFileName(1, 'file.jpg'));
        $this->assertEquals('0_file.jpg', UserFile::createFileName(0, 'file.jpg'));
        $this->assertEquals(
            '1_prilis-zlutoucky-kun.tar.gz', UserFile::createFileName(1, 'Příliš žluťoučký kůň.tar.gz')
        );
        $this->assertEquals('1_', UserFile::createFileName(1, ''));
    }

    /** test of stripId */
    public function testStripId()
    {
        $this->assertEquals('file.jpg', UserFile::stripId('file.jpg'));
        $this->assertEquals('file.jpg', UserFile::stripId('0_file.jpg'));
        $this->assertEquals('file.tar.gz', UserFile::stripId('22_file.tar.gz'));
    }


    /** test of idToDir */
    public function testIdToDir()
    {
        $this->assertEquals('', UserFile::idToDir(0));
        $this->assertEquals('', UserFile::idToDir(123456, UserFile::FILES_PER_DIR_UNLIMITED));
        $this->assertEquals('1/2/3/', UserFile::idToDir(123456, UserFile::FILES_PER_DIR_1000));
        $this->assertEquals('1/2/3/4/', UserFile::idToDir(123456, UserFile::FILES_PER_DIR_100));
        $this->assertEquals('1/2/3/4/5/', UserFile::idToDir(123456, UserFile::FILES_PER_DIR_10));
    }

    /** test of humanFileSize */
    public function testHumanFileSize()
    {
        $this->assertEquals('0 B', UserFile::humanFilesize(0, false, 0));
        $this->assertEquals('0.00 B', UserFile::humanFilesize(0, true));
        $this->assertEquals('0.00 B', UserFile::humanFilesize(-10));
        $this->assertEquals('0,0 B', UserFile::humanFilesize(-10, true, 1, ','));
        $this->assertEquals('1000 B', UserFile::humanFilesize(1000, false, 0));
        $this->assertEquals('1 KiB', UserFile::humanFilesize(1024, false, 0));
        $this->assertEquals('1 kB', UserFile::humanFilesize(1024, true, 0));
        $this->assertEquals('1.00 MiB', UserFile::humanFilesize(1024*1024, false, 2));
        $this->assertEquals('1.00 MB', UserFile::humanFilesize(1000*1000, true, 2));
        $this->assertEquals('1.00 GiB', UserFile::humanFilesize(1024*1024*1024, false, 2));
        $this->assertEquals('1.00 GB', UserFile::humanFilesize(1000*1000*1000, true, 2));
    }

    /** test of bytes() */
    public function testBytes()
    {
        $this->assertEquals(0, UserFile::bytes('0'));
        $this->assertEquals(0, UserFile::bytes('0 K'));
        $this->assertEquals(0, UserFile::bytes('0 M'));
        $this->assertEquals(0, UserFile::bytes('0 G'));

        $this->assertEquals(1024, UserFile::bytes('1 K'));
        $this->assertEquals(1024*1024, UserFile::bytes('1 M'));
        $this->assertEquals(1024*1024*1024, UserFile::bytes('1 G'));
    }

    /** test of getMime() */
    public function testGetMime()
    {
        $this->assertEquals('application/ogg', UserFile::getMime('file.ogg'));
        $this->assertEquals('application/pdf', UserFile::getMime('file.pdf'));
        $this->assertEquals('application/rtf', UserFile::getMime('file.rtf'));
        $this->assertEquals('application/zip', UserFile::getMime('file.zip'));
        $this->assertEquals('application/zip', UserFile::getMime('FILE.ZIP'));
        $this->assertEquals('application/postscript', UserFile::getMime('file.ai'));
        $this->assertEquals('application/postscript', UserFile::getMime('file.eps'));
        $this->assertEquals('application/postscript', UserFile::getMime('file.ps'));
        $this->assertEquals('application/vnd.ms-excel', UserFile::getMime('file.xls'));
        $this->assertEquals('application/vnd.ms-powerpoint', UserFile::getMime('file.ppt'));
        $this->assertEquals('application/vnd.sun.xml.writer', UserFile::getMime('file.sxw'));
        $this->assertEquals('application/vnd.sun.xml.calc', UserFile::getMime('file.sxc'));
        $this->assertEquals('application/x-bittorrent', UserFile::getMime('file.torrent'));
        $this->assertEquals('application/x-gzip', UserFile::getMime('file.gz'));
        $this->assertEquals('application/x-gzip', UserFile::getMime('file.tar.gz'));
        $this->assertEquals('application/x-javascript', UserFile::getMime('file.js'));
        $this->assertEquals('application/x-shockwave-flash', UserFile::getMime('file.swf'));
        $this->assertEquals('application/x-rpm', UserFile::getMime('file.rpm'));
        $this->assertEquals('application/xhtml+xml', UserFile::getMime('file.xhtml'));
        $this->assertEquals('application/xhtml+xml', UserFile::getMime('file.xht'));
        $this->assertEquals('audio/midi', UserFile::getMime('file.midi'));
        $this->assertEquals('audio/midi', UserFile::getMime('file.mid'));
        $this->assertEquals('audio/mpeg', UserFile::getMime('file.mp2'));
        $this->assertEquals('audio/mpeg', UserFile::getMime('file.mp3'));
        $this->assertEquals('audio/x-pn-realaudio', UserFile::getMime('file.ram'));
        $this->assertEquals('audio/x-pn-realaudio', UserFile::getMime('file.rm'));
        $this->assertEquals('audio/x-realaudio', UserFile::getMime('file.ra'));
        $this->assertEquals('audio/x-wav', UserFile::getMime('file.wav'));
        $this->assertEquals('image/bmp', UserFile::getMime('file.bmp'));
        $this->assertEquals('image/gif', UserFile::getMime('file.gif'));
        $this->assertEquals('image/jpeg', UserFile::getMime('file.jpeg'));
        $this->assertEquals('image/jpeg', UserFile::getMime('file.jpg'));
        $this->assertEquals('image/jpeg', UserFile::getMime('file.jpe'));
        $this->assertEquals('image/png', UserFile::getMime('file.png'));
        $this->assertEquals('image/wmf', UserFile::getMime('file.wmf'));
        $this->assertEquals('image/tiff', UserFile::getMime('file.tiff'));
        $this->assertEquals('image/tiff', UserFile::getMime('file.tif'));
        $this->assertEquals('model/vrml', UserFile::getMime('file.wrl'));
        $this->assertEquals('model/vrml', UserFile::getMime('file.vrml'));
        $this->assertEquals('text/css', UserFile::getMime('file.css'));
        $this->assertEquals('text/html', UserFile::getMime('file.html'));
        $this->assertEquals('text/html', UserFile::getMime('file.htm'));
        $this->assertEquals('text/xml', UserFile::getMime('file.xml'));
        $this->assertEquals('text/xml', UserFile::getMime('file.xsl'));
        $this->assertEquals('text/plain', UserFile::getMime('file.asc'));
        $this->assertEquals('text/plain', UserFile::getMime('file.txt'));
        $this->assertEquals('video/mpeg', UserFile::getMime('file.mpeg'));
        $this->assertEquals('video/mpeg', UserFile::getMime('file.mpg'));
        $this->assertEquals('video/mpeg', UserFile::getMime('file.mpe'));
        $this->assertEquals('video/quicktime', UserFile::getMime('file.qt'));
        $this->assertEquals('video/quicktime', UserFile::getMime('file.mov'));
        $this->assertEquals('video/x-ms-wmv', UserFile::getMime('file.wmv'));
        $this->assertEquals('video/x-msvideo', UserFile::getMime('file.avi'));
        $this->assertEquals('application/octet-stream', UserFile::getMime('file.unknown'));
        $this->assertEquals('application/octet-stream', UserFile::getMime('file'));
    }

    /** test of getFullPath() */
    public function testGetFullPath()
    {
        $baseDir = $this->_dir.'/';

        $this->assertEquals('', UserFile::getFullPath(0, $baseDir));

        file_put_contents($baseDir.'/1_file.jpg', '');
        $expect = self::_normalizeDirDelimiters($baseDir.'1_file.jpg');
        $output = self::_normalizeDirDelimiters(UserFile::getFullPath(1, $baseDir));
        $this->assertEquals($expect, $output);
        $this->assertEquals('', UserFile::getFullPath(11, $baseDir));

        Filesystem::mkDir($baseDir.'1/2/');
        file_put_contents($baseDir.'1/2/123_file.jpg', '');
        $expect = self::_normalizeDirDelimiters($baseDir.'/1/2/123_file.jpg');
        $output = self::_normalizeDirDelimiters(
            UserFile::getFullPath(123, $baseDir, UserFile::FILES_PER_DIR_10)
        );
        $this->assertEquals($expect, $output);
        $output = self::_normalizeDirDelimiters(
            UserFile::getFullPath(123, $baseDir, UserFile::FILES_PER_DIR_UNKNOWN)
        );
        $this->assertEquals($expect, $output);
        $this->assertEquals('', UserFile::getFullPath(12, $baseDir));

        Filesystem::mkDir($baseDir.'1/2/3/');
        file_put_contents($baseDir.'1/2/3/123456_file.jpg', '');
        $expect = self::_normalizeDirDelimiters($baseDir.'/1/2/3/123456_file.jpg');
        $output = self::_normalizeDirDelimiters(
            UserFile::getFullPath(123456, $baseDir, UserFile::FILES_PER_DIR_1000)
        );
        $this->assertEquals($expect, $output);
        $output = self::_normalizeDirDelimiters(
            UserFile::getFullPath(123456, $baseDir, UserFile::FILES_PER_DIR_UNKNOWN)
        );
        $this->assertEquals($expect, $output);
    }

    /**
     * normalize windows direcory delimiters
     * @param string $path
     * @return string
     */
    private static function _normalizeDirDelimiters($path)
    {
        $path = str_replace("\\", "/", $path);
        return str_replace("//", "/", $path);
    }

    /** test of delete */
    public function testDelete()
    {
        $baseDir = $this->_dir.'/';

        file_put_contents($baseDir.'/1_file.jpg', '');
        Filesystem::mkDir($baseDir.'1/2/');
        file_put_contents($baseDir.'1/2/123_file.jpg', '');
        Filesystem::mkDir($baseDir.'1/2/3/');
        file_put_contents($baseDir.'1/2/3/123456_file.jpg', '');
        file_put_contents($baseDir.'1/2/3/123456_file.gif', '');
        $this->assertTrue(UserFile::delete(123456, $baseDir, '123456_file.gif', UserFile::FILES_PER_DIR_1000));
        $this->assertFileExists($baseDir.'/1_file.jpg');
        $this->assertFileExists($baseDir.'1/2/123_file.jpg');
        $this->assertFileNotExists($baseDir.'1/2/3/123456_file.jpg');
        $this->assertFileExists($baseDir.'1/2/3/123456_file.gif');
        file_put_contents($baseDir.'1/2/3/123456_file.png', '');
        $this->assertTrue(UserFile::delete(123456, $baseDir, '', UserFile::FILES_PER_DIR_1000));
        $this->assertFileNotExists($baseDir.'1/2/3/123456_file.gif');
        $this->assertFileNotExists($baseDir.'1/2/3/123456_file.png');

        $this->assertTrue(UserFile::delete(123, $baseDir, '', UserFile::FILES_PER_DIR_UNKNOWN));
        $this->assertFileNotExists($baseDir.'1/2/123_file.jpg');
    }

    /** test of download(), do not test headers */
/*    public function testDownload()
    {
        try {
            @UserFile::download($this->_dir.'/some file.jpg', 'image/jpeg');
        } catch (Exception $e) {
            $this->assertEquals('file_not_found', $e->getMessage());
        }
    }*/

}