<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Filesystem.php';

/**
 * test class for Filesystem
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class FilesystemTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->_dir = sys_get_temp_dir().'/gstfilesystem';
        Filesystem::rmDir($this->_dir);
        @mkdir($this->_dir);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        Filesystem::rmDir($this->_dir);
    }

    /** test of mkDir() */
    public function testMkDir()
    {
        Filesystem::mkDir($this->_dir.'/dir1/dir2');
        $this->assertTrue(is_dir($this->_dir.'/dir1/dir2'));
    }

    /**
     * fail on incorrect directory name
     * @expectedException PHPUnit_Framework_Error
     */
    public function testMkDirFail()
    {
        Filesystem::mkDir($this->_dir.'/dir1/dir2/dir:');
    }

    /** test of globr() */
    public function testGlobr()
    {
        mkdir($this->_dir.'/dir1');
        mkdir($this->_dir.'/dir1/dir2');

        $files = Filesystem::globr($this->_dir, '*');
        $expect = array($this->_dir.'/dir1', $this->_dir.'/dir1/dir2');
        $this->assertEquals($expect, $files);

        file_put_contents($this->_dir.'/dir1/file.txt', ' ');
        file_put_contents($this->_dir.'/dir1/dir2/file.aaa', ' ');
        file_put_contents($this->_dir.'/dir1/dir2/file3.aaa', ' ');

        $files = Filesystem::globr($this->_dir, '*.txt');
        $expect = array($this->_dir.'/dir1/file.txt');
        $this->assertEquals($expect, $files);

        $files = Filesystem::globr($this->_dir, 'file.*');
        $expect = array($this->_dir.'/dir1/file.txt', $this->_dir.'/dir1/dir2/file.aaa');
        $this->assertEquals($expect, $files);
    }

    /** test of chmodr() */
    public function testChmodr()
    {
        mkdir($this->_dir.'/dir1');
        mkdir($this->_dir.'/dir1/dir2');
        file_put_contents($this->_dir.'/dir1/file.txt', ' ');
        $this->assertNull(Filesystem::chmodr($this->_dir, 0777, 0666));
    }

    /** test of getFileExtension() */
    public function testGetFileExtension()
    {
        $this->assertEquals('', Filesystem::getFileExtension('file'));
        $this->assertEquals('ext', Filesystem::getFileExtension('file.ext'));
        $this->assertEquals('tar.gz', Filesystem::getFileExtension('file.tar.gz'));
    }

    /** test of getFileExtension() */
    public function testGetFilenameWoExtension()
    {
        $this->assertEquals('file', Filesystem::getFilenameWoExtension('file'));
        $this->assertEquals('file', Filesystem::getFilenameWoExtension('file.ext'));
        $this->assertEquals('file', Filesystem::getFilenameWoExtension('file.tar.gz'));
    }

    /** test of gZipFile() */
    public function testGZipFile()
    {
        $src = $this->_dir.'/file.txt';
        file_put_contents($src, 'lorem ipsum dolor sit amet');

        $dest = $this->_dir.'/file.gz';
        $this->assertNull(Filesystem::gZipFile($src, $dest));
        $this->assertFileExists($dest);
        $this->assertGreaterThan(filesize($src), filesize($dest));
    }

    /** test of logrotate() */
    public function testLogrotate()
    {
        $src = $this->_dir.'/file.log';
        file_put_contents($src, '   ');

        $this->assertNull(Filesystem::logrotate($src, 3));
        clearstatcache();
        $this->assertEquals(0, filesize($src));
        $this->assertFileExists($src.'.'.date('Y-m-d-H-i-s'));
        $this->assertEquals(3, filesize($src.'.'.date('Y-m-d-H-i-s')));
        sleep(1);
        $this->assertNull(Filesystem::logrotate($src, 3));
        sleep(1);
        $this->assertNull(Filesystem::logrotate($src, 3));
        sleep(1);
        $this->assertNull(Filesystem::logrotate($src, 3));
        $files = Filesystem::globr($this->_dir, 'file.log.*');
        $this->assertEquals(3, count($files));

        $this->assertNull(Filesystem::logrotate($src = $this->_dir.'/notexists.log', 3));
    }

}