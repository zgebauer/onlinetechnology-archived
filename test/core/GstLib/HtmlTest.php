<?php
/**
 * class HtmlTest
 *
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Html.php';

/**
 * test class for Html
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class HtmlTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test of escapeForJs() */
    public function testEscapeForJs()
    {
        $this->assertEquals('string', Html::escapeForJs('string'));
        $this->assertEquals("st\'ring", Html::escapeForJs("st'ring"));
        $this->assertEquals('string', Html::escapeForJs('st"ring'));
        $this->assertEquals('string', Html::escapeForJs("st\tring"));
    }

    /** test of escapeForHtml() */
    public function testEscapeForHtml()
    {
        $this->assertEquals('1&amp;2&quot;3&#039;4&lt;5&gt;6', Html::escapeForHtml("1&2\"3'4<5>6"));
    }

    /** test of getEmailLink() */
    public function testGetEmailLink()
    {
        $email = 'email@address.com';
        $this->assertEquals('<a href="mailto:'.$email.'">'.$email.'</a>', Html::getEmailLink($email));
        $this->assertEquals('<a href="mailto:'.$email.'">contact</a>', Html::getEmailLink($email, 'contact'));
        $this->assertRegExp('/<a href="(.+)">contact<\/a>/', Html::getEmailLink($email, 'contact', true));
        $this->assertRegExp('/<a href="(.+)">(.+)<\/a>/', Html::getEmailLink($email, '', true));
    }

    /** test of testGetOptions() */
    public function testGetOptions()
    {
        $values = array('a'=>'A', 'b'=>'B');
        $expect = '<option value="a">A</option><option value="b">B</option>';
        $this->assertEquals($expect, Html::getOptions($values));
        $selected = 'b';
        $expect = '<option value="a">A</option><option value="b" selected="selected">B</option>';
        $this->assertEquals($expect, Html::getOptions($values, $selected));
        $selected = array('b', 'a');
        $expect = '<option value="a" selected="selected">A</option><option value="b" selected="selected">B</option>';
        $this->assertEquals($expect, Html::getOptions($values, $selected));
    }

    /** test of getLink() */
    public function testGetLink()
    {
        $this->assertEquals('<a href="http://example.org">example.org</a>', Html::getLink('example.org'));
        $this->assertEquals(
            '<a href="http://example.org">description</a>', Html::getLink('example.org', 'description')
        );
        $expect = '<a href="http://example.org" title="title">description</a>';
        $this->assertEquals($expect, Html::getLink('example.org', 'description', 'title'));
    }

    /** test of fixForUrl() */
    public function testFixForUrl()
    {
        $this->assertEquals('escrzyaie', Html::fixForUrl('ěščřžýáíé'));
        $this->assertEquals('escrzyaie', Html::fixForUrl('ĚŠČŘŽÝÁÍÉ'));
        $this->assertEquals('a-b', Html::fixForUrl('A  B'));
        $this->assertEquals('a-b', Html::fixForUrl('A--B'));
    }

    /** test of Html::queryUrl() */
    public function testQueryUrl()
    {
        $url = 'example.org';
        $values = array('a'=>'A', 'b'=>'B');
        $this->assertEquals('http://example.org', Html::queryUrl($url));
        $this->assertEquals('http://example.org?a=A&amp;b=B', Html::queryUrl($url, $values));
        $values = array('a'=>'A', 'b'=>'B', 'c', 1=>2, 'd'=>null);
        $this->assertEquals('http://example.org?a=A&amp;b=B', Html::queryUrl($url, $values));
        $values = array('a'=>'A', 'b'=>'B', 'c', 1=>2, 'd'=>null);
        $this->assertEquals('http://example.org?a=A&amp;b=B', Html::queryUrl($url, $values, 'notarray'));
    }

    /** test of isChecked() */
    public function testIsChecked()
    {
        $this->assertEquals(' checked="checked"', Html::isChecked(true));
        $this->assertEquals('', Html::isChecked(false));
    }

    /** test of isSelected() */
    public function testIsSelected()
    {
        $this->assertEquals(' selected="selected"', Html::isSelected(true));
        $this->assertEquals('', Html::isSelected(false));
    }

    /** test of Html::isEmail() */
    public function testIsEmail()
    {
        $this->assertFalse(Html::isEmail(''));
        $this->assertFalse(Html::isEmail(array('address@email.com')));
        $this->assertTrue(Html::isEmail('address@email.com'));
        $this->assertTrue(Html:: isEmail('other.address@email.somewhere.com'));
        $this->assertTrue(Html::isEmail('address@i.com'));
        $this->assertFalse(Html::isEmail('address..@email.com'));
        $this->assertFalse(Html::isEmail('@email.somewhere.com'));
        $this->assertFalse(Html::isEmail('@email.somewhere.comcom'));
        $this->assertFalse(Html::isEmail('_address@email.com'));
        $this->assertFalse(Html::isEmail('.address@email.com'));
        $this->assertFalse(Html::isEmail('other..adr@email.somewhere.com'));
        $this->assertFalse(Html::isEmail('other:adr@email.somewhere.com'));
        $this->assertFalse(Html::isEmail('other,adr@email.somewhere.com'));
    }

    /** test of fixAmp() */
    public function testFixAmp()
    {
        $this->assertEquals('example.org?a=a&b=b', Html::fixAmp('example.org?a=a&amp;b=b'));
    }

    /** test of stripHtml() */
    public function testStripHtml()
    {
        $html = '<HTML>aaa<script src=""> ss';
        $expect = 'aaa ss';
        $this->assertEquals($expect, Html::stripHtml($html));
        $html = '<HTML>aaa  ss';
        $expect = 'aaa ss';
        $this->assertEquals($expect, Html::stripHtml($html, true));
    }

    /** test of Html::obfuscate */
    public function testObfuscate()
    {
        $this->assertContains('&#', Html::obfuscate('obfuscate'));
    }

    /** test of Html::stripTags */
    public function testStripTags()
    {
        $expect = 'test1 test2 test3';
        $text = '<p>test1<br>test2<br />test3</p>';
        $this->assertEquals($expect, Html::stripTags($text));
    }

    /** test of urlToAbs */
    public function testUrlToAbs()
    {
        $html = '<a href="dir/file.htm">link</a> <img src="dir/file/image" />';
        $expect = '<a href="http://example.org/dir/file.htm">link</a> <img src="http://example.org/dir/file/image" />';
        $this->assertEquals($expect, Html::urlToAbs($html, 'http://example.org/'));
    }

    /** test of minifyHtml */
    public function testMinifyHtml()
    {
        // @codingStandardsIgnoreStart
        $html = " <h1 class=\"class\" style=\"color:green;\">text</h1> \n    <p style=\"color:green\">text</p> \n \n\n  <p>text</p><p>text</p> ";
        $expect = "<h1 class=\"class\" style=\"color:green;\">text</h1> <p style=\"color:green\">text</p> <p>text</p><p>text</p>";
        // @codingStandardsIgnoreEnd
        $this->assertEquals($expect, Html::minifyHtml($html));
    }

}