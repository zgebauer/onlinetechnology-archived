<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib\Db;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Db/Mysqli.php';

/**
 * test class for Gst_Db
 * expects running mysql
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class DbMysqliTest extends \PHPUnit_Framework_TestCase
{
    /** @var array configuration for test database */
    private static $_testConfig = array('user'=>'root', 'password'=>'', 'charset'=>'utf8');
    /** @var string sql query for create test teabel */
    private static $_testTable = "CREATE TABLE IF NOT EXISTS `gstdb_test` (
        `id` INT(10) UNSIGNED NULL AUTO_INCREMENT,
        `name` VARCHAR(50) NULL DEFAULT '',
        `date` DATETIME NULL DEFAULT NULL,
        `bool` TINYINT(3) UNSIGNED NULL DEFAULT '0',
        `number` DECIMAL(10,2) UNSIGNED NULL DEFAULT '0',
        PRIMARY KEY (`id`)
    ) COLLATE='utf8_general_ci' ENGINE=InnoDB;";

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test of query() */
    public function testQuery()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $this->setExpectedException(
          'GstLib\Db\DatabaseErrorException',
          "Table 'test.sometable' doesn't exist:SELECT * FROM sometable"
        );
        $obj->query('SELECT * FROM sometable');

        $this->assertTrue($obj->query(self::$_testTable));
        $row = $obj->fetchArray("SHOW TABLES LIKE 'gstdb_test'");
        $this->assertEquals('gstdb_test', reset($row));
        $this->assertEquals('', $obj->Err());

        $this->assertTrue($obj->query("DROP TABLE IF EXISTS `gstdb_test`"));
        $this->assertEquals('', $obj->getErr());

        $obj->close();
    }

    /** test methods for format values to sql */
    public function testFormatValues()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $this->assertEquals("'va\'lue'", $obj->escape("va'lue"));
        $this->assertEquals("'2012-12-31 10:20:30'", $obj->datetimeToSql('2012-12-31T10:20:30'));
        $this->assertEquals(1, $obj->boolToSql(true));
        $this->assertEquals(0, $obj->boolToSql(false));
        $obj->close();
    }

    /** test where() */
    public function testWhere()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $this->assertEquals(" WHERE column='value'", $obj->where(array(array('column', 'value'))));
        $this->assertEquals(" WHERE column='1'", $obj->where(array(array('column', 1, '='))));
        $this->assertEquals(" WHERE column>'1'", $obj->where(array(array('column', 1, '>'))));
        $this->assertEquals(" WHERE column<='1'", $obj->where(array(array('column', 1, '<='))));
        $this->assertEquals(" WHERE column LIKE '%value%'", $obj->where(array(array('column', 'value', 'like'))));
        $this->assertEquals(" WHERE column LIKE 'value%'", $obj->where(array(array('column', 'value', 'like%'))));
        $this->assertEquals(" WHERE column LIKE '%value'", $obj->where(array(array('column', 'value', '%like'))));
        $this->assertEquals(" WHERE column IN ('1','2')", $obj->where(array(array('column', '1, 2', 'in'))));
        $this->assertEquals(" WHERE column<NOW()", $obj->where(array(array('column<NOW()', null))));

        $obj->close();
    }

    /** test of orderBy() */
    public function testorderBy()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $this->assertEquals('', $obj->orderBy(''));
        $this->assertEquals(' ORDER BY name ASC', $obj->orderBy('name'));
        $this->assertEquals(' ORDER BY name ASC', $obj->orderBy('name', false));
        $this->assertEquals(' ORDER BY name DESC', $obj->orderBy('name', true));

        $obj->close();
    }

    /** test of limit() */
    public function testlimit()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $this->assertEquals('', $obj->limit(10));
        $this->assertEquals(' LIMIT 10,20', $obj->limit(10, 20));
        $obj->close();
    }

    /** test of showSql() */
    public function testShowSql()
    {
        $obj = new DriverMysqli(array_merge(self::$_testConfig, array('table_prefix' => 'prefix_')));

        $this->assertEquals("SELECT * FROM `prefix_sometable`", $obj->showSql('SELECT * FROM [%sometable%]'));

        $obj->close();
    }

    /** test insert and getch rows */
    public function testFetch()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $this->assertTrue($obj->query("DROP TABLE IF EXISTS `gstdb_test`"));
        $this->assertTrue($obj->query(self::$_testTable));
        // @codingStandardsIgnoreStart
        $this->assertEquals(1, $obj->insert("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name1').",'0000-00-00',0,10)"));
        $this->assertEquals(2, $obj->insert("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name2').",'2012-12-31 10:20:30',1,11.22)"));
        $this->assertEquals(3, $obj->insert("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name3').",'0000-00-00',0,20)"));
        // @codingStandardsIgnoreEnd

        $this->setExpectedException(
          'GstLib\Db\DatabaseErrorException',
          "Table 'test.notexists' doesn't exist:INSERT INTO notexists (notexists)"
        );
        $this->assertFalse($obj->insert("INSERT INTO notexists (notexists) VALUES(0)"));

        // rows
        $row = $obj->fetchArray("SELECT * FROM gstdb_test WHERE id=2");
        $expect = array('id'=>2, 'name'=>'name2', 'date'=>'2012-12-31 10:20:30', 'bool'=>1, 'number'=>11.22);
        $this->assertEquals($expect, $row);

        // empty result
        $this->assertFalse($obj->fetchArray("SELECT * FROM gstdb_test WHERE id=-1"));

        $this->setExpectedException(
          'GstLib\Db\DatabaseErrorException',
          "cannot execute: INSERT INTO notexists (notexists)"
        );
        $this->assertFalse($obj->fetchArray("SELECT notexists FROM gstdb_test"));

        $obj->setType('date', 'iso-datetime');
        $obj->setType('bool', 'bool');
        $obj->setType('number', 'float');
        $rows = $obj->fetchArrayAll("SELECT * FROM gstdb_test WHERE id>1 ORDER BY id");
        $expect = array(
            array('id'=>2, 'name'=>'name2', 'date'=>'2012-12-31T10:20:30', 'bool'=>true, 'number'=>11.22),
            array('id'=>3, 'name'=>'name3', 'date'=>'0000-00-00T00:00:00', 'bool'=>false, 'number'=>20)
        );
        $this->assertEquals($expect, $rows);

        $obj->setType('date', 'date');
        $obj->setType('number', 'int');
        $row = $obj->etchArray("SELECT * FROM gstdb_test WHERE id=2");
        $expect = array('id'=>2, 'name'=>'name2', 'date'=>'2012-12-31', 'bool'=>true, 'number'=>11);
        $this->assertEquals($expect, $row);

        $this->setType('date', 'time');
        $row = $this->fetchArray("SELECT * FROM gstdb_test WHERE id=2");
        $expect = array('id'=>2, 'name'=>'name2', 'date'=>'10:20:30', 'bool'=>1, 'number'=>11.22);
        $this->assertEquals($expect, $row);

        // count
        $rows = $obj->count("SELECT COUNT(*) FROM gstdb_test WHERE id>1");
        $this->assertEquals(2, $rows);

        $this->assertTrue($obj->free());
        $this->assertTrue($obj->query("DROP TABLE IF EXISTS `gstdb_test`"));
        $obj->close();
    }

    /** test of transaction() */
    public function testTransaction()
    {
        $obj = new DriverMysqli(self::$_testConfig);

        $sql = array(
            "INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name1').",'0000-00-00',0,10)",
            // @codingStandardsIgnoreStart
            "INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name2').",'2012-12-31 10:20:30',1,11.22)"
            // @codingStandardsIgnoreEnd
        );

        $this->assertTrue($obj->query("DROP TABLE IF EXISTS `gstdb_test`"));
        $this->assertTrue($obj->query(self::$_testTable));

        $this->assertTrue($obj->transaction($sql));
        $rows = $obj->fetchArrayAll("SELECT * FROM gstdb_test");
        $expect = array(
            array('id'=>1, 'name'=>'name1', 'date'=>'0000-00-00 00:00:00', 'bool'=>0, 'number'=>10),
            array('id'=>2, 'name'=>'name2', 'date'=>'2012-12-31 10:20:30', 'bool'=>1, 'number'=>11.22)
        );
        $this->assertEquals($expect, $rows);
        $this->assertTrue($obj->query("TRUNCATE TABLE gstdb_test"));

        // invalid sql
        $sql = array("INSERT INTO gstdb_test (notexists) VALUES(0)");

        $this->setExpectedException(
          'GstLib\Db\DatabaseErrorException', "Unknown column 'notexists' in 'field list"
        );
        $this->assertFalse($obj->transaction($sql));
        $this->assertEquals(array(), $obj->fetchArrayAll("SELECT * FROM gstdb_test"));

        $this->assertTrue($obj->query("DROP TABLE `gstdb_test`"));
        $obj->close();
    }

    /** test of manual commit transaction */
    public function testTransactionCommit()
    {
        $obj = new DriverMysqli(self::$_testConfig);
        $this->assertTrue($obj->query("DROP TABLE IF EXISTS `gstdb_test`"));
        $this->assertTrue($obj->query(self::$_testTable));

        $this->assertTrue($obj->beginTrans());
        // @codingStandardsIgnoreStart
        $this->assertTrue($obj->query("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name1').",'0000-00-00',0,0)"));
        $this->assertTrue($obj->query("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name2').",'0000-00-00',0,0)"));
        // @codingStandardsIgnoreEnd
        $this->assertTrue($obj->commit());
        $rows = $obj->fetchArrayAll("SELECT * FROM gstdb_test");
        $expect = array(
            array('id'=>1, 'name'=>'name1', 'date'=>'0000-00-00 00:00:00', 'bool'=>0, 'number'=>0),
            array('id'=>2, 'name'=>'name2', 'date'=>'0000-00-00 00:00:00', 'bool'=>0, 'number'=>0)
        );
        $this->assertEquals($expect, $rows);

        $this->assertTrue($obj->query("DROP TABLE `gstdb_test`"));
        $obj->close();
    }

    /** test of manual rollback transaction */
    public function testTransactionRollback()
    {
        $obj = new DriverMysqli(self::$_testConfig);
        $this->assertTrue($obj->query("DROP TABLE IF EXISTS `gstdb_test`"));
        $this->assertTrue($obj->query(self::$_testTable));

        $this->assertTrue($obj->beginTrans());

        // @codingStandardsIgnoreStart
        $this->assertTrue($obj->query("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name1').",'0000-00-00',0,0)"));
        $this->assertTrue($obj->query("INSERT INTO gstdb_test (name,date,bool,number) VALUES(".$obj->escape('name2').",'0000-00-00',0,0)"));
        // @codingStandardsIgnoreEnd
        $this->assertTrue($obj->rollback());

        $this->assertEquals(array(), $obj->fetchArrayAll("SELECT * FROM gstdb_test"));

        $this->assertTrue($obj->query("DROP TABLE `gstdb_test`"));
        $obj->close();
    }

}