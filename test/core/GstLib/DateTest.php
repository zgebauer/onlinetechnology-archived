<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Date.php';

/**
 * test class for Date
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class DateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test of constants */
    public function testConst()
    {
         $this->assertEquals('0000-00-00', Date::EMPTY_DATE);
         $this->assertEquals('0000-00-00T00:00:00', Date::EMPTY_DATETIME);
    }

    /** test of dateTime2Iso() */
    public function testGetDateFromIso()
    {
        $expect = array(
            'seconds'=>0,
            'minutes'=>0,
            'hours'=>0,
            'mday'=>18,
            'mon'=>1,
            'year'=>2010
        );
        $this->assertEquals($expect, Date::getDateFromIso('2010-01-18'));
        $expect = array(
            'seconds'=>30,
            'minutes'=>20,
            'hours'=>10,
            'mday'=>18,
            'mon'=>1,
            'year'=>2010
        );
        $this->assertEquals($expect, Date::getDateFromIso('2010-01-18T10:20:30'));
        $expect = array(
            'seconds'=>30,
            'minutes'=>20,
            'hours'=>10,
            'mday'=>18,
            'mon'=>1,
            'year'=>2010
        );
        $this->assertEquals($expect, Date::getDateFromIso('2010-01-18T10:20:30Z02:00'));
        $this->assertFalse(Date::getDateFromIso('2010-24-24'));
    }

    /** test of Date::isIsoDate() */
    public function testIsIsoDate()
    {
        $this->assertTrue(Date::isIsoDate('2010-01-18T10:20:30'));
        $this->assertTrue(Date::isIsoDate('2010-01-18'));
        $this->assertFalse(Date::isIsoDate('10-01-18'));
        $this->assertFalse(Date::isIsoDate('2010-24-18'));
        $this->assertFalse(Date::isIsoDate('2010-10-18T24:00:00'));
        $this->assertFalse(Date::isIsoDate('2010-10-18T23:60:00'));
        $this->assertFalse(Date::isIsoDate('2010-10-18T23:59:60'));
        $this->assertTrue(Date::isIsoDate('2010-01-18T10:20:30Z02:00'));
        $this->assertTrue(Date::isIsoDate('2010-01-18T10:20:30+02:00'));
        $this->assertTrue(Date::isIsoDate('2010-01-18T10:20:30+02'));
    }

    /** test of dateTime2Iso() */
    public function testDateTime2Iso()
    {
        $this->assertEquals('0000-00-00', Date::dateTime2Iso(''));
        $this->assertEquals('0000-00-00', Date::dateTime2Iso('0000-00-00'));
        $this->assertEquals('0000-00-00T00:00:00', Date::dateTime2Iso('', true));
        $this->assertEquals('2010-01-18', Date::dateTime2Iso('2010-01-18'));
        $this->assertEquals('2010-01-18', Date::dateTime2Iso('18.1.2010'));
        $this->assertEquals('2010-01-18', Date::dateTime2Iso('18.1.10'));
        $this->assertEquals('1990-01-18', Date::dateTime2Iso('18.1.90'));
        $this->assertEquals('2010-01-18T20:19:00', Date::dateTime2Iso('18.1.2010 20:19', true));
        $this->assertEquals('2010-01-18T20:19:10', Date::dateTime2Iso('18.1.2010 20:19:10', true));
        $this->assertEquals('2010-01-18T'.date('H:i:s'), Date::dateTime2Iso('18.1.2010', true, true));
        $this->assertFalse(Date::dateTime2Iso('18.50.2010'));
        $this->assertFalse(Date::dateTime2Iso('18.1.2010 24:00:00', true));
        $this->assertFalse(Date::dateTime2Iso('18.1.2010 23:60:00', true));
        $this->assertFalse(Date::dateTime2Iso('18.1.2010 23:59:60', true));
        $this->assertFalse(Date::dateTime2Iso('FFFFF', true));
    }

    /** test of dateDiff() */
    public function testDateDiff()
    {
        $this->assertEquals(10, Date::dateDiff('2010-01-18T20:10:30', '2010-01-18T20:10:40'));
        $this->assertEquals(5, Date::dateDiff('2010-01-18T20:10:30', '2010-01-18T20:15:40', 'i'));
        $this->assertEquals(2, Date::dateDiff('2010-01-18T20:10:30', '2010-01-18T22:11:40', 'h'));
        $this->assertEquals(1, Date::dateDiff('2010-01-18T20:10:30', '2010-01-19T20:10:40', 'd'));
        $this->assertEquals(1, Date::dateDiff('2010-01-18T20:10:30', '2010-02-19T20:10:40', 'm'));
        $this->assertEquals(0, Date::dateDiff('2010-01-18T20:10:30', '2010-01-19T20:10:20', 'd'));
        $this->assertEquals(-2, Date::dateDiff('2010-01-18T20:10:30', '2010-01-17T20:10:20', 'd'));
    }

    /** test of formatDateTime() */
    public function testFormatDateTime()
    {
        $this->assertEquals('18. 1. 2010', Date::formatDateTime('2010-01-18'));
        $this->assertEquals('18. 1. 2010', Date::formatDateTime('2010-01-18T10:21'));
        $this->assertEquals('01.18.2010', Date::formatDateTime('2010-01-18', 'm.d.Y'));
        $this->assertEquals('2010/01/18', Date::formatDateTime('2010-01-18', 'Y/m/d'));
        $this->assertEquals('18. 1. 2010 10:21', Date::formatDateTime('2010-01-18T10:21:34', 'cz-datetime'));
        $this->assertEquals('18. 1. 2010 10:21:34', Date::formatDateTime('2010-01-18T10:21:34', 'cz-datetimesec'));
        $this->assertEquals('10:21', Date::formatDateTime('2010-01-18T10:21:34', 'time'));
        $this->assertEquals('', Date::formatDateTime('0000-00-00'));
        $this->assertEquals(time(), Date::formatDateTime(date('Y-m-d\TH:i:s'), 'timestamp'));
        // assume timezone Europe/Prague
        $this->assertEquals('Mon, 18 Jan 2010 10:21:34 +0100', Date::formatDateTime('2010-01-18T10:21:34', 'rss'));
        $this->assertEquals('2010-01-18T10:21:34+01:00', Date::formatDateTime('2010-01-18T10:21:34', 'atom'));
    }

    /** test of formatDateTime() fail */
    public function testFormatDateTimeFail()
    {
        try {
            $this->assertEquals('', @Date::formatDateTime('invaliddate'));
        } catch (Exception $e) {
            $this->assertEquals('invalid date invaliddate', $e->getMessage());
        }

        try {
            $this->assertEquals('', @Date::formatDateTime('2010-24-24'));
        } catch (Exception $e) {
            $this->assertEquals('invalid date 2010-24-24', $e->getMessage());
        }

        try {
            $this->assertEquals('', @Date::formatDateTime('2010-10-10T24:00:00'));
        } catch (Exception $e) {
            $this->assertEquals('invalid date 2010-10-10T24:00:00', $e->getMessage());
        }

        try {
            $this->assertEquals('', @Date::formatDateTime('2010-10-10T23:60:00'));
        } catch (Exception $e) {
            $this->assertEquals('invalid date 2010-10-10T23:60:00', $e->getMessage());
        }

        try {
            $this->assertEquals('', @Date::formatDateTime('2010-10-10T23:59:60'));
        } catch (Exception $e) {
            $this->assertEquals('invalid date 2010-10-10T23:59:60', $e->getMessage());
        }
    }

    /** test of currentDate() */
    public function testUnix2Iso()
    {
        $this->assertEquals(date('Y-m-d'), Date::unix2Iso(time()));
        $this->assertEquals(date('Y-m-d\TH:i:s'), Date::unix2Iso(time(), true));
    }

    /** test of currentDate(), currentDateTime() */
    public function testCurrentDate()
    {
         $this->assertEquals(date('Y-m-d'), Date::currentDate());
         $this->assertEquals(date('Y-m-d\TH:i:s'), Date::currentDateTime());
    }

    public function testHetMonthsRange()
    {
        $expect = array(2013=>array('01', '02'));
        $this->assertEquals($expect, Date::getMonthsRange('10.1.2013', '10.3.2013'));
        $expect = array(
            2012=>array('09', '10', '11', '12'),
            2013=>array('01', '02',)
            );
        $this->assertEquals($expect, Date::getMonthsRange('10.9.2012', '10.3.2013'));
    }
}