<?php
/**
 * class GstLib_AllTests
 *
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once 'VarsTest.php';
require_once 'DateTest.php';
require_once 'FilesystemTest.php';
require_once 'HtmlTest.php';
require_once 'TemplateTest.php';
require_once 'TranslatorTest.php';
require_once 'DbMysqliTest.php';
require_once 'UserFileTest.php';

/**
 * run all tests
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class AllTests extends \PHPUnit_Framework_TestSuite
{
    /**
     * runs all tests of GstLib
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new \PHPUnit_Framework_TestSuite('GstLib');
        $suite->addTestSuite('\GstLib\VarsTest');
        $suite->addTestSuite('\GstLib\DateTest');
        $suite->addTestSuite('\GstLib\FilesystemTest');
        $suite->addTestSuite('\GstLib\HtmlTest');
        $suite->addTestSuite('\GstLib\TemplateTest');
        $suite->addTestSuite('\GstLib\TranslatorTest');
        $suite->addTestSuite('\GstLib\Db\DbMysqliTest');
        $suite->addTestSuite('\GstLib\UserFileTest');
        return $suite;
    }
}