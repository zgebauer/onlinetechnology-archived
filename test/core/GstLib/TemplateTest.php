<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Template.php';

/**
 * test class for Template
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class TemplateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->_file = tempnam(sys_get_temp_dir(), 'tmp');
        $this->_cnt = '<html><head></head><body>[%VALUE%]'.
            '<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> --></body></html>';

        if (is_file($this->_file)) {
            unlink($this->_file);
        }
        if (!file_put_contents($this->_file, $this->_cnt)) {
            user_error('cannot write test file '.$this->_file);
        }
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        @unlink($this->_file);
    }

    /** test of __construct() */
    public function testConstructor()
    {
        $obj = new Template();
        if (defined('ERGO_TEMPLATE_CHECKSUB') && !ERGO_TEMPLATE_CHECKSUB) {
            $this->assertFalse($obj->checkSub);
        }
        $this->assertEquals('', $obj->get());

        if (defined('ERGO_TEMPLATE_CHECKSUB') && !ERGO_TEMPLATE_CHECKSUB) {
            $this->assertFalse($obj->checkSub);
        }

        if (defined('ERGO_TEMPLATE_CHECKSUB') && !ERGO_TEMPLATE_CHECKSUB) {
            $obj = new Template();
            $this->assertTrue($obj->checkSub);
        }

        $obj = new Template($this->_file);
        $this->assertEquals($this->_cnt, $obj->get());

        $obj = new Template($this->_file, true);
        $expect = '[%VALUE%]<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> -->';
        $this->assertEquals($expect, $obj->get());
    }

    /**
     * test of __construct(), readFile()
     * @expectedException PHPUnit_Framework_Error
     */
    public function testConstructorFail()
    {
        $obj = new Template('FILE_NOT_EXISTS');
        $this->assertEquals('', $obj->get());
    }

    /** test of __set() */
    public function testSetMagic()
    {
        $obj = new Template();
        $obj->checkSub = true;
        $this->assertTrue($obj->checkSub);
        $obj->checkSub = false;
        $this->assertFalse($obj->checkSub);
    }

    /** test of set() */
    public function testSet()
    {
        $cnt = 'lorem ipsum dolor sit amet';
        $obj = new Template();
        $obj->set($cnt);
        $this->assertEquals($cnt, $obj->get());
    }

    /** test of replace() */
    public function testReplace()
    {
        $obj = new Template($this->_file);
        $obj->replace('VALUE', 'replaced');
        $expect = '<html><head></head><body>replaced<!-- <SUB_TEMPLATE> -->'.
            'content of subtemplate<!-- </SUB_TEMPLATE> --></body></html>';
        $this->assertEquals($expect, $obj->get());

        $obj = new Template($this->_file);
        $obj->replace('VALUE', '<p>replaced</p>');
        $expect = '<html><head></head><body>&lt;p&gt;replaced&lt;/p&gt;<!-- <SUB_TEMPLATE> -->'.
            'content of subtemplate<!-- </SUB_TEMPLATE> --></body></html>';
        $this->assertEquals($expect, $obj->get());

        $obj = new Template($this->_file);
        $obj->replace('VALUE', '<p>replaced</p>', 'raw');
        $expect = '<html><head></head><body><p>replaced</p><!-- <SUB_TEMPLATE> -->'.
            'content of subtemplate<!-- </SUB_TEMPLATE> --></body></html>';
        $this->assertEquals($expect, $obj->get());

        $obj->replace('VALUE_NOT_EXISTS', 'replaced', array('quiet' => true));
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function testReplaceNotExists()
    {
        $obj = new Template($this->_file);

        try {
            $obj->replace('VALUE_NOT_EXISTS', 'replaced');
        } catch (Exception $e) {
            $this->assertEquals('pseudotag VALUE_NOT_EXISTS not found', $e->getMessage());
        }
    }

    /** test of getSub() */
    public function testGetSub()
    {
        $obj = new Template();
        $obj->set($this->_cnt);
        $this->assertEquals('content of subtemplate', $obj->getSub('TEMPLATE'));

        $obj->checkSub = false;
        $this->assertEquals('', $obj->getSub('TEMPLATE_NOT_EXISTS'));
    }

    /** test of getSub() */
    public function testGetSub_Fail()
    {
        $obj = new Template();
        $obj->set($this->_cnt);
        $obj->checkSub = true;
        try {
            $this->assertEquals('', $obj->getSub('TEMPLATE_NOT_EXISTS'));
        } catch(Exception $e) {
            $this->assertEquals('subtemplate TEMPLATE_NOT_EXISTS not found', $e->getMessage());
        }
        $this->assertEquals($this->_cnt, $obj->get());
    }

    /** test of delSub() */
    public function testDelSub()
    {
        $obj = new Template();
        $obj->checkSub = false;
        $obj->set($this->_cnt);
        $obj->delSub('TEMPLATE');
        $this->assertEquals('<html><head></head><body>[%VALUE%]</body></html>', $obj->get());

        $obj->checkSub = true;
        $obj->set($this->_cnt);
        try {
            $obj->delSub('TEMPLATE_NOT_EXISTS');
        } catch(Exception $e) {
            $this->assertEquals('subtemplate TEMPLATE_NOT_EXISTS not found', $e->getMessage());
        }
        $this->assertEquals($this->_cnt, $obj->get());
    }

    /** test of replaceSub() */
    public function testReplaceSub()
    {
        $obj = new Template();

        $obj->checkSub = false;
        $obj->set($this->_cnt);
        $obj->replaceSub('TEMPLATE', 'replaced content');
        $expect = '<html><head></head><body>[%VALUE%]replaced content</body></html>';
        $this->assertEquals($expect, $obj->get());

        $obj->checkSub = true;
        $obj->set($this->_cnt);
        try {
            $obj->replaceSub('TEMPLATE_NOT_EXISTS', 'replaced content');
        } catch(Exception $e) {
            $this->assertEquals('subtemplate TEMPLATE_NOT_EXISTS not found', $e->getMessage());
        }
        $this->assertEquals($this->_cnt, $obj->get());
    }

    /** test of stripSub() */
    public function testStripSub()
    {
        $obj = new Template();

        $obj->checkSub = false;
        $obj->set($this->_cnt);
        $obj->stripSub('TEMPLATE');
        $expect = '<html><head></head><body>[%VALUE%]content of subtemplate</body></html>';
        $this->assertEquals($expect, $obj->get());

        $obj->checkSub = true;
        $obj->set($this->_cnt);
        try {
            $obj->stripSub('TEMPLATE_NOT_EXISTS');
        } catch(Exception $e) {
            $this->assertEquals('subtemplate TEMPLATE_NOT_EXISTS not found', $e->getMessage());
        }
        $this->assertEquals($this->_cnt, $obj->get());
    }

    /** test of showSub() */
    public function testShowSub()
    {
        $obj = new Template();

        $obj->checkSub = false;
        $obj->set($this->_cnt);
        $obj->showSub('TEMPLATE', true);
        $expect = '<html><head></head><body>[%VALUE%]content of subtemplate';
        $expect .= '</body></html>';
        $this->assertEquals($expect, $obj->get());
        $obj->set($this->_cnt);
        $obj->showSub('TEMPLATE', false);
        $this->assertEquals('<html><head></head><body>[%VALUE%]</body></html>', $obj->get());

        $obj->checkSub = true;
        $obj->set($this->_cnt);
        try {
            $obj->showSub('TEMPLATE_NOT_EXISTS', true);
        } catch(Exception $e) {
            $this->assertEquals('subtemplate TEMPLATE_NOT_EXISTS not found', $e->getMessage());
        }
        $this->assertEquals($this->_cnt, $obj->get());
    }

    /** test of display() */
    public function testDisplay()
    {
        $obj = new Template();
        $obj->set($this->_cnt);
        $this->expectOutputString($this->_cnt);
        $obj->display();
    }

    /** test of processSub() */
    public function testProcessSub()
    {
        $template = '<div><!-- <SUB_PART> --><p>[%TXT_VALUE1%],[%TXT_VALUE2%]</p><!-- </SUB_PART> --></div>';
        $values = array(array('value1', 'value2'));
        $expect = '<div><p>value1,value2</p></div>';
        $obj = new Template();
        $obj->set($template);
        $obj->processSub('PART', $values);
        $this->assertEquals($expect, $obj->get());

        $expect = '<div><p>value2,value1</p></div>';
        $map = array('TXT_VALUE2'=>0,'TXT_VALUE1'=>1);
        $obj->set($template);
        $obj->processSub('PART', $values, $map);
        $this->assertEquals($expect, $obj->get());
    }

    /** test of processSub() */
    public function testProcessSub_Map()
    {
        $template = '<div><!-- <SUB_PART> --><p>[%TXT_VALUE1%],[%TXT_VALUE2%]';
        $template .= '</p><!-- </SUB_PART> --></div>';
        $values = array(array('value1', 'value2'));
        $expect = '<div><p>value2,value1</p></div>';
        $map = array('TXT_VALUE2'=>0,'TXT_VALUE1'=>1);
        $obj = new Template();
        $obj->set($template);
        $obj->processSub('PART', $values, $map);
        $this->assertEquals($expect, $obj->get());
    }

    /** test of processSub() fail */
    public function testProcessSub_Fail()
    {
        $template = '<div><!-- <SUB_PART> --><p>[%TXT_VALUE1%],[%TXT_VALUE2%]</p><!-- </SUB_PART> --></div>';
        $values = array(array('value1', 'value2'));
        $obj = new Template();
        $obj->checkSub = true;
        $obj->set($template);
        try {
            $obj->processSub('PART_NOT_EXISTS', $values);
        } catch(Exception $e) {
            $this->assertEquals('subtemplate PART_NOT_EXISTS not found', $e->getMessage());
            $this->assertEquals($template, $obj->get());
        }
    }

    /** test of processSub() fail
     * @expectedException PHPUnit_Framework_Error
     */
    public function testProcessSub_Map_Fail()
    {
        $template = '<div><!-- <SUB_PART> --><p>[%TXT_VALUE1%],[%TXT_VALUE2%]';
        $template .= '</p><!-- </SUB_PART> --></div>';
        $values = array(array('value1', 'value2'));
        $map = array('TXT_VALUE2'=>0,'TXT_VALUE1'=>10);
        $obj = new Template();
        $obj->checkSub = true;
        $obj->set($template);
        $obj->processSub('PART', $values, $map);
    }
}
