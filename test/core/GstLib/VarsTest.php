<?php
/**
 * @package    GstLib
 * @subpackage GstLibTest
 */

namespace GstLib;

require_once dirname(dirname(dirname(__DIR__))).'/www/core/GstLib/Vars.php';

/**
 * test class for Vars
 *
 * @author     Zdenek Gebauer <zdenek.gebauer@gmail.com>
 * @package    GstLib
 * @subpackage GstLibTest
 */
class VarsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /** test of int2Bool(), bool2Int() */
    function testBool()
    {
        $this->assertTrue(Vars::int2bool(1));
        $this->assertFalse(Vars::int2bool(0));
        $this->assertTrue(Vars::int2bool(-1));

        $this->assertTrue(Vars::bool2int(true) == 1);
        $this->assertTrue(Vars::bool2int(false) == 0);

        $this->assertTrue(Vars::int2bool(Vars::bool2int(true)));
        $this->assertFalse(Vars::int2bool(Vars::bool2int(false)));
    }

    /** test of formatNumberHuman() */
    function testFormatNumberHuman()
    {
        $this->assertEquals(123, Vars::formatNumberHuman(123, 0));
        $this->assertEquals(123.00, Vars::formatNumberHuman(123, 2));
        $this->assertEquals('123,00', Vars::formatNumberHuman(123, 2, ','));
        $this->assertEquals('1 234,56', Vars::formatNumberHuman(1234.56, 2, ',', ' '));
        $this->assertEquals('1234,56', Vars::formatNumberHuman(1234.56, 2, ',', ''));
        $this->assertEquals('1234.56', Vars::formatNumberHuman(1234.56, 2, '.', ''));
    }

    /** test of floatval() */
    function testFloatval()
    {
        $this->assertEquals(0, Vars::floatval(''));
        $this->assertEquals(123.45, Vars::floatval('1 2 3,4 5'));
        $this->assertEquals(-123.45, Vars::floatval('- 1 2 3,4 5'));
    }

    /** test of method strPadCut() */
    function testStrPadCut()
    {
        $this->assertEquals(' 123', Vars::strPadCut('123', 4, ' ', STR_PAD_LEFT));
        $this->assertEquals('12', Vars::strPadCut('123', 2, ' ', STR_PAD_LEFT));
        $this->assertEquals('123 ', Vars::strPadCut('123', 4, ' ', STR_PAD_RIGHT));
        $this->assertEquals('12', Vars::strPadCut('123', 2, ' ', STR_PAD_RIGHT));
    }

    /** test of randomString() */
    function testRandomString()
    {
        $this->assertEquals(3, strlen(Vars::randomString(3)));
    }

    /** test of substr() */
    function testSubstr()
    {
        $str = 'escrzyaie';
        $strB = 'ěščřžýáíé';
        $this->assertEquals('es', Vars::substr($str, 0, 2));
        $this->assertEquals('ěš', Vars::substr($strB, 0, 2, 'UTF-8'));
        $this->assertEquals('čř', Vars::substr($strB, 2, 2, 'UTF-8'));
        $this->assertEquals('ie', Vars::substr($str, -2));
        $this->assertEquals('íé', Vars::substr($strB, -2, null, 'UTF-8'));
    }

    /** test of perex() */
    function testPerex()
    {
        $str = 'escrzyaie';
        $this->assertEquals('escrzyaie', Vars::perex($str, 2));
        $this->assertEquals('escr', Vars::perex($str, 2, 2));

        $add = '...';
        $this->assertEquals('escr'.$add, Vars::perex($str, 2, 2, array('add'=>$add)));
    }

    /** test of formatCurrency() */
    function testFormatCurrency()
    {
        $this->assertEquals('10,45 Kč', Vars::formatCurrency(10.450, 'CZK', 2, ','));
        $this->assertEquals('10 000.450 Kč', Vars::formatCurrency(10000.4501, 'CZK', 3, '.', ' '));
        $this->assertEquals('$ 10 000.450', Vars::formatCurrency(10000.4501, 'USD', 3));
        $this->assertEquals('&euro;10 000.4501', Vars::formatCurrency(10000.4501, 'EUR', 4));
    }

    /** test of cyrilic2Latin() */
    public function testCyrilic2Latin()
    {
        $input = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'.
                 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ';
        $output = 'abvgdejozhzijjklmnoprstufkhcchshshhyehjuja'.
                  'ABVGDEJOZHZIJJKLMNOPRSTUFKHCCHSHSHHYEHJUJA';
        $this->assertEquals($output, Vars::cyrilic2Latin($input));
    }

    /** test of parseKewwords() */
    public function testParseKeywords()
    {
        $this->assertEquals(array(), Vars::parseKeywords(''));
        $this->assertEquals(array('aaaaa', 'bbbbbb'), Vars::parseKeywords('aa aaaaa bbbbbb'));
        $this->assertEquals(array('bbbbbb'), Vars::parseKeywords('aa aaaaa bbbbbb', 6));
    }

    public function testHashPassword()
    {
        $this->assertEquals(hash('sha256', 'password'), Vars::hashPassword('', 'password'));
        $this->assertEquals(hash('sha256', '11password'), Vars::hashPassword(1, 'password'));
    }

}