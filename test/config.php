<?php
/**
 * main configuration file for phpunit tests
 *
 * @package Ergo
 * @author  Zdenek Gebauer <zdenek.gebauer@gmail.com>
 */

/** database type - null=no database/mysqli */
define('ERGO_DB_TYPE', 'mysqli');
/** prefix of table names */
define('ERGO_DB_PREFIX', '');
/** character set of database*/
define('ERGO_DB_CHARSET', 'utf8');

/** name or IP address of database server for write */
define('ERGO_DB_SERVER', '127.0.0.1');
/** username for database for write */
define('ERGO_DB_USER', 'root');
/** password for database */
define('ERGO_DB_PWD', '');
/** database name, path to database file */
define('ERGO_DB_NAME', 'phpunit');
/** database name used as "patern" in tests */
define('ERGO_DB_NAME_PHPUNIT', '`onlinetechnology`');
/** true|false = use persistent|normal connection */
define('ERGO_DB_PERSISTENT', false);

/** base directory of website with trailing slash */
define('ERGO_ROOT_DIR', 'd:/projekty/onlinetechnology/www/');
/** base url of website with trailing slash */
define('ERGO_ROOT_URL', 'http://dev.zdenek/onlinetechnology/');

/** base directory for users data with trailing slash */
define('ERGO_DATA_DIR', ERGO_ROOT_DIR.'phpunitdata/');

/** base directory for log files with trailing slash */
define('ERGO_LOG_DIR', ERGO_ROOT_DIR.'log/');
/** base directory for cached files files with trailing slash */
define('ERGO_CACHE_DIR', ERGO_ROOT_DIR.'cache/');

/** true = disable Ergo error and exception handlers and use default */
define('ERGO_ERR_HANDLERS_DISABLE', false);
/** true|false = show|hide error messages, false on production server */
define('ERGO_ERR_DISPLAY', true);
/** true|false = stop|continue execution on error */
define('ERGO_ERR_HALT', true);
/** e-mail address for error messages, null=do not send */
define('ERGO_ERR_MAIL', null);

/**
 * true|false = check|do not check subtemplates (false for little speed-up)
 * @see Template
 */
define('ERGO_TEMPLATE_CHECKSUB', true);
/**
 * true|false, true disable storing content to cache directory
 * @see Gst_Cache
 */
define('ERGO_CACHE_DISABLE', true);

/** true|false, true enable full page cache */
define('ERGO_PAGE_CACHE_ENABLE', false);
/** max lifetime of fullpage cache */
define('ERGO_PAGE_CACHE_EXPIRE', 36000);

/** true|false = server support|do not support mod_rewrite */
define('ERGO_REWRITE_ENABLED', true);
/** true|false = scheduled task are run by cron (/cli/cron.php)| occasionaly in script */
//define('ERGO_CRON_ENABLED', false);
/** time zone, http://php.net/manual/en/function.date-default-timezone-set.php */
define('ERGO_TIMEZONE', 'Europe/Prague');
/** gzip output with php, false if apache mod_deflate is enabled */
//define('ERGO_GZIP_OUTPUT', false);
/** use one combined js file or multiple js files  */
//define('ERGO_USE_COMBINED_JS', true);

/** sender of automatically sent e-mails */
define('ERGO_EMAIL_ROBOT', 'ergo@example.org');
/**
 * send mail via [mail|smtp]
 * @see http://phpmailer.codeworxtech.com/
 */
define('PHPMAILER_MAILER', 'smtp');
/** smtp server, if use smtp */
define('PHPMAILER_SMTP_HOST', 'smtp.gmail.com');
/** authorize to smtp server, if use smtp */
define('PHPMAILER_SMTP_AUTH', true);
/** username for smtp server, if PHPMAILER_SMTP_AUTH is true */
define('PHPMAILER_SMTP_USERNAME', 'zdenek.gebauer');
/** password for smtp server, if PHPMAILER_SMTP_AUTH is true */
define('PHPMAILER_SMTP_PASSWORD', 'ekeflfdjxtipgohx');

/** true|false = run in test(phpunit)|normal mode */
define('ERGO_TESTING', true);
